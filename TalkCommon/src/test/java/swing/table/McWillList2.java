package swing.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.xinwei.common.lookandfeel.McWillTheme;

@SuppressWarnings("serial")
public class McWillList2 extends JTable {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Object rowData[][] = { { "Row1-Column1", "Row1-Column2", "Row1-Column3" }, { "Row2-Column1", "Row2-Column2", "Row2-Column3" } };

		List<Component> components = new ArrayList<>();

		components.add(new JLabel("Row1"));
		components.add(new JLabel("Row2"));
		JLabel e = new JLabel("Row3");
		components.add(e);
		e.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				System.out.println("test");

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});

		McWillList2 table = new McWillList2(components);


		JScrollPane scrollPane = new JScrollPane(table);

		frame.add(scrollPane, BorderLayout.CENTER);
		frame.setSize(300, 150);
		frame.setVisible(true);

	}

	public McWillList2() {

	}

	public McWillList2(final List<Component> components) {
		super(new AbstractTableModel() {
			public String getColumnName(int column) {
				return null;
			}

			public int getRowCount() {
				return components.size();
			}

			public int getColumnCount() {
				return 1;
			}

			public Object getValueAt(int row, int col) {
				return components.get(row);
			}

			public boolean isCellEditable(int row, int column) {
				return true;
			}

			public void setValueAt(Object value, int row, int col) {
				components.add(row, (Component) value);
				fireTableCellUpdated(row, col);
			}
		});
		setTableHeader(null);
		setShowGrid(false);
		getColumnModel().getColumn(0).setCellRenderer(new CellRenderer());
		getColumnModel().getColumn(0).setCellEditor(new TableCell());
	}

	public McWillList2(Object[][] rowData) {
		super(rowData, new Object[] { "" });
	}
}


class CellRenderer implements TableCellRenderer {

	public CellRenderer() {
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		return (JComponent) value;
	}
}

class TableCell extends JComponent implements TableCellEditor {
	private static final long serialVersionUID = 7550152638883023357L;

	@Override
	public Object getCellEditorValue() {
		return this;
	}

	@Override
	public boolean isCellEditable(EventObject anEvent) {
		return true;
	}

	@Override
	public boolean shouldSelectCell(EventObject anEvent) {
		return true;
	}

	@Override
	public boolean stopCellEditing() {
		return false;
	}

	@Override
	public void cancelCellEditing() {

	}

	@Override
	public void addCellEditorListener(CellEditorListener l) {

	}

	@Override
	public void removeCellEditorListener(CellEditorListener l) {

	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		return (Component) value;
	}
}
