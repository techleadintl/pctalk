//package swing.table.x;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.swing.JFrame;
//import javax.swing.JScrollPane;
//
//import com.xinwei.common.lookandfeel.component.McWillEditList;
//
//public class JInteractiveTableExample2 extends JFrame {
//	public JInteractiveTableExample2() {
//		super("Interactive Table Cell Example");
//		setDefaultCloseOperation(EXIT_ON_CLOSE);
//		setSize(500, 300);
//
//		McWillEditList<RssFeed> table = new McWillEditList<RssFeed>();
//		table.setCellUIClass(RssFeedCell2.class);
//
//		table.addElement(new RssFeed("pekalicious", "http://feeds2.feedburner.com/pekalicious", new Article[] { new Article("Title1", "http://title1.com", "Content 1"),
//				new Article("Title2", "http://title2.com", "Content 2"), new Article("Title3", "http://title3.com", "Content 3"), new Article("Title4", "http://title4.com", "Content 4"), }));
//		table.addElement(new RssFeed("Various Thoughts on Photography", "http://various-photography-thoughts.blogspot.com/values/posts/default",
//				new Article[] { new Article("Title1", "http://title1.com", "Content 1"), new Article("Title2", "http://title2.com", "Content 2"),
//						new Article("Title3", "http://title3.com", "Content 3"), new Article("Title4", "http://title4.com", "Content 4"), }));
//
//		//		table.setDefaultRenderer(RssFeed.class, new RssFeedCellRenderer());
//		//		table.setDefaultEditor(RssFeed.class, new RssFeedCellEditor());
//
//		table.addElement(new RssFeed("xxxx Thoughts on Photography", "http://various-photography-thoughts.blogspot.com/values/posts/default",
//				new Article[] { new Article("Title1", "http://title1.com", "Content 1"), new Article("Title2", "http://title2.com", "Content 2"),
//						new Article("Title3", "http://title3.com", "Content 3"), new Article("Title4", "http://title4.com", "Content 4"), }));
//
//		table.setRowHeight(90);
//
//		add(table);
//	}
//
//	public static void main(String[] args) {
//		new JInteractiveTableExample2().setVisible(true);
//	}
//}