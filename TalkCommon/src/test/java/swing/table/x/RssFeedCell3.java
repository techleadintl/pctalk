package swing.table.x;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

public class RssFeedCell3 extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {
	JPanel panel;
	JLabel text;
	JButton showButton;

	RssFeed object;

	public RssFeedCell3() {
		panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		createUI(panel);
	}

	protected void createUI(JPanel panel) {
		text = new JLabel();
		showButton = new JButton("View Articles");
		showButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, "Reading " + object.name);
			}
		});

		panel.add(text);
		panel.add(showButton);
	}

	protected void updateData(JTable table, RssFeed value, boolean isSelected, int row, int column) {
		this.object = value;
		text.setText(value.name + value.url + "Articles " + value.articles.length);

		if (isSelected) {
			panel.setBackground(table.getSelectionBackground());
		} else {
			panel.setBackground(table.getSelectionForeground());
		}
	}

	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		return getTableCellComponent(table, value, true, row, column);
	}

	public Object getCellEditorValue() {
		return null;
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		return getTableCellComponent(table, value, isSelected, row, column);
	}

	public Component getTableCellComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		updateData(table, (RssFeed) value, isSelected, row, column);
		return panel;
	}
}