//package swing.table.x;
//
//import java.awt.Component;
//import java.util.EventObject;
//
//import javax.swing.JPanel;
//import javax.swing.JTable;
//import javax.swing.event.CellEditorListener;
//import javax.swing.event.ChangeEvent;
//import javax.swing.event.EventListenerList;
//import javax.swing.table.TableCellEditor;
//import javax.swing.table.TableCellRenderer;
//
//public class McWillListCell<T> extends JPanel implements TableCellEditor, TableCellRenderer {
//	protected EventListenerList listenerList = new EventListenerList();
//	transient protected ChangeEvent changeEvent = null;
//
//	protected T object;
//
//	public McWillListCell() {
//		createUI();
//	}
//
//	protected void createUI() {
//
//	}
//
//	protected void updateData(JTable table, T value, boolean isSelected, int row, int column) {
//		this.object = value;
//	}
//
//	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
//		return getTableCellComponent(table, value, true, row, column);
//	}
//
//	public Object getCellEditorValue() {
//		return null;
//	}
//
//	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
//		return getTableCellComponent(table, value, isSelected, row, column);
//	}
//
//	public Component getTableCellComponent(JTable table, Object value, boolean isSelected, int row, int column) {
//		updateData(table, (T) value, isSelected, row, column);
//		return this;
//	}
//
//	/**
//	 * Returns true.
//	 * 
//	 * @param e
//	 *            an event object
//	 * @return true
//	 */
//	public boolean isCellEditable(EventObject e) {
//		return true;
//	}
//
//	/**
//	 * Returns true.
//	 * 
//	 * @param anEvent
//	 *            an event object
//	 * @return true
//	 */
//	public boolean shouldSelectCell(EventObject anEvent) {
//		return true;
//	}
//
//	/**
//	 * Calls <code>fireEditingStopped</code> and returns true.
//	 * 
//	 * @return true
//	 */
//	public boolean stopCellEditing() {
//		fireEditingStopped();
//		return true;
//	}
//
//	/**
//	 * Calls <code>fireEditingCanceled</code>.
//	 */
//	public void cancelCellEditing() {
//		fireEditingCanceled();
//	}
//
//	/**
//	 * Adds a <code>CellEditorListener</code> to the listener list.
//	 * 
//	 * @param l
//	 *            the new listener to be added
//	 */
//	public void addCellEditorListener(CellEditorListener l) {
//		listenerList.add(CellEditorListener.class, l);
//	}
//
//	/**
//	 * Removes a <code>CellEditorListener</code> from the listener list.
//	 * 
//	 * @param l
//	 *            the listener to be removed
//	 */
//	public void removeCellEditorListener(CellEditorListener l) {
//		listenerList.remove(CellEditorListener.class, l);
//	}
//
//	/**
//	 * Returns an array of all the <code>CellEditorListener</code>s added to this AbstractCellEditor with addCellEditorListener().
//	 *
//	 * @return all of the <code>CellEditorListener</code>s added or an empty array if no listeners have been added
//	 * @since 1.4
//	 */
//	public CellEditorListener[] getCellEditorListeners() {
//		return listenerList.getListeners(CellEditorListener.class);
//	}
//
//	/**
//	 * Notifies all listeners that have registered interest for notification on this event sendOrReceive. The event instance is created lazily.
//	 *
//	 * @see EventListenerList
//	 */
//	protected void fireEditingStopped() {
//		// Guaranteed to return a non-null array
//		Object[] listeners = listenerList.getListenerList();
//		// Process the listeners last to first, notifying
//		// those that are interested in this event
//		for (int i = listeners.length - 2; i >= 0; i -= 2) {
//			if (listeners[i] == CellEditorListener.class) {
//				// Lazily create the event:
//				if (changeEvent == null)
//					changeEvent = new ChangeEvent(this);
//				((CellEditorListener) listeners[i + 1]).editingStopped(changeEvent);
//			}
//		}
//	}
//
//	/**
//	 * Notifies all listeners that have registered interest for notification on this event sendOrReceive. The event instance is created lazily.
//	 *
//	 * @see EventListenerList
//	 */
//	protected void fireEditingCanceled() {
//		// Guaranteed to return a non-null array
//		Object[] listeners = listenerList.getListenerList();
//		// Process the listeners last to first, notifying
//		// those that are interested in this event
//		for (int i = listeners.length - 2; i >= 0; i -= 2) {
//			if (listeners[i] == CellEditorListener.class) {
//				// Lazily create the event:
//				if (changeEvent == null)
//					changeEvent = new ChangeEvent(this);
//				((CellEditorListener) listeners[i + 1]).editingCanceled(changeEvent);
//			}
//		}
//	}
//}