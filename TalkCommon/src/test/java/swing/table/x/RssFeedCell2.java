//package swing.table.x;
//
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//
//import javax.swing.JButton;
//import javax.swing.JLabel;
//import javax.swing.JOptionPane;
//import javax.swing.JTable;
//
//import com.xinwei.common.lookandfeel.component.McWillListCell;
//
//public class RssFeedCell2 extends McWillListCell<RssFeed> {
//	JLabel text;
//	JButton showButton;
//
//	@Override
//	protected void createUI() {
//		text = new JLabel();
//		showButton = new JButton("View Articles");
//		showButton.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, "Reading " + object.name);
//			}
//		});
//
//		add(text);
//		add(showButton);
//	}
//
//	@Override
//	protected void updateData(JTable table, RssFeed value, boolean isSelected, int row, int column) {
//		super.updateData(table, value, isSelected, row, column);
//		text.setText(value.name + value.url + "Articles " + value.articles.length);
//
//		if (isSelected) {
//			setBackground(table.getSelectionBackground());
//		} else {
//			setBackground(table.getSelectionForeground());
//		}
//	}
//}