package swing;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;
@SuppressWarnings("serial")
public class JImagePanel extends JPanel {	
	/**
	 * 背景图片
	 */
	private Image image = null;
	/**
	 * 是否自适应
	 */
	private boolean autoResize = false;	
	/**
	 * 构造方法
	 */
	public JImagePanel() {		this(null, false);	}
	/**
	 * 构造方法
	 * @param autoResize - 图形是否根据面大大小自适应
	 */
	public JImagePanel(boolean autoResize) {
		this(null, autoResize);	}
	/**
	 * 构造方法
	 * @param image - 图片
	 */
	public JImagePanel(Image image) {		this(image, false);	}	
	/**
	 * 构造方法
	 * @param image - 图片
	 * @param autoResize - 是否自适应
	 */
	public JImagePanel(Image image, boolean autoResize) {
		super();
		this.autoResize = autoResize;
		this.image = image;
		repaint();	}	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (image != null) {
			if (autoResize) {
				g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
			} else {
				g.drawImage(image, 0, 0, null);	}
		}
	}
	/**
	 * 返回背景图片
	 * @return
	 */
	public Image getImage() {return image;	}
	/**
	 * 设置背景图片
	 * @param image - 背景图片
	 */
	public void setImage(Image image) {
		this.image = image;
		repaint();	}
	/**
	 * 返回图片是否自适应
	 * @return
	 */
	public boolean isAutoResize() {		return autoResize;	}
	/**
	 * 设置图片是否自适应
	 * @param autoResize
	 */
	public void setAutoResize(boolean autoResize) {
		this.autoResize = autoResize;
		repaint();	}
}
