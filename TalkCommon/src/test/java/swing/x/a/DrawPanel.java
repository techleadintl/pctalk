package swing.x.a;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

public class DrawPanel extends JPanel {
	private final int r = 10;
	Point p, p1, p2;
	int l1 = -1, l2 = -1;
	int a, b, c, d, g, h;
	ArrayList<Point> l = new ArrayList<Point>();
	ArrayList<line> al = new ArrayList<line>();

	//用ArrayList进行存储
	public DrawPanel() {
		DrawListener listen = new DrawListener();
		addMouseListener(listen);
		addMouseMotionListener(listen);
		setBackground(Color.black);
		setPreferredSize(new Dimension(400, 700));
	}

	public void paintComponent(Graphics page) {
		super.paintComponent(page);
		page.setColor(Color.yellow);

		for (Point po : l) {
			if (po != null)
				page.drawOval(po.x - r, po.y - r, 2 * r, 2 * r);
		}
		for (line n : al) { //核心算法，这里我用了相似  程序中15和5分别控制箭头的大小
			if (n != null) {

				int x1 = n.a.x;
				int y1 = n.a.y;
				int x2 = n.b.x;
				int y2 = n.b.y;

				a = (int) (r * (x2 - x1) / Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
				b = (int) (r * (y2 - y1) / Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
				c = (int) ((r + 15) * (x2 - x1) / Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
				d = (int) ((r + 15) * (y2 - y1) / Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
				g = 5 * b / r;
				h = 5 * a / r;
				int Xpoint[] = new int[3];
				int Ypoint[] = new int[3];
				Xpoint[0] = x2 - c - g;
				Xpoint[1] = x2 - c + g;
				Xpoint[2] = x2 - a;
				Ypoint[0] = y2 - d + h;
				Ypoint[1] = y2 - d - h;
				Ypoint[2] = y2 - b;
				page.drawLine(x1 + a, y1 + b, x2 - a, y2 - b);
				page.fillPolygon(Xpoint, Ypoint, 3);
			}
		}
	}

	class line {
		public Point a, b;

		line(Point p1, Point p2) {
			a = p1;
			b = p2;
		}
	}

	public class DrawListener implements MouseListener, MouseMotionListener {

		public void mousePressed(MouseEvent event) {
			l2 = -1;
			l1 = -1;
			p1 = event.getPoint();
			for (int i = 0; i < l.size(); i++) {
				if (Math.sqrt((l.get(i).x - p1.x) * (l.get(i).x - p1.x) + (l.get(i).y - p1.y) * (l.get(i).y - p1.y)) < r)
					l1 = i;
			}
			repaint();
		}

		public void mouseClicked(MouseEvent event) {
			p = event.getPoint();
			int f = 1;
			if (l.size() == 0) {
				repaint();
				l.add(p);
			} else {
				for (int i = 0; i < l.size(); i++) {
					if (Math.sqrt((l.get(i).x - p.x) * (l.get(i).x - p.x) + (l.get(i).y - p.y) * (l.get(i).y - p.y)) < 2 * r)
						f = 0;
				}
				if (f == 1) {
					repaint();
					l.add(p);
				}
			}
		}

		public void mouseReleased(MouseEvent event) {
			p2 = event.getPoint();
			for (int i = 0; i < l.size(); i++) {
				if (Math.sqrt((l.get(i).x - p2.x) * (l.get(i).x - p2.x) + (l.get(i).y - p2.y) * (l.get(i).y - p2.y)) < r)
					l2 = i;
			}
			repaint();
			if (l1 != -1 && l2 != -1) {
				al.add(new line(l.get(l1), l.get(l2)));
			}
		}

		public void mouseEntered(MouseEvent event) {
		}

		public void mouseExited(MouseEvent event) {
		}

		public void mouseMoved(MouseEvent event) {
		}

		public void mouseDragged(MouseEvent event) {
			p2 = event.getPoint();
		}

	}
}