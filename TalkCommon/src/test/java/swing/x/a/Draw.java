package swing.x.a;

import javax.swing.JFrame;

public class Draw {
	public static void main(String[] args) {
		JFrame frame = new JFrame("画圆");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.add(new DrawPanel());

		frame.pack();
		frame.setVisible(true);
	}

}
