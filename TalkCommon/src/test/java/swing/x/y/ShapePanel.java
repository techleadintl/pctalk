package swing.x.y;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;

import javax.swing.JPanel;

public class ShapePanel extends JPanel {
	
	private int width;
	private int height;
	
	public ShapePanel(int width, int height) {
		this.width = width;
		this.height = height;
		this.setSize(new Dimension(width,height));
	}
	
	public void setPanelSize(Dimension d) {
		this.setSize(d);
	}
	
	public void paintComponent(Graphics g) {
		Graphics2D gTemp = (Graphics2D)g.create();
		
		GeneralPath path = new GeneralPath();
		path.moveTo(0.0,0.0);
		path.lineTo(0, this.getHeight());
		path.lineTo(this.getWidth(), this.getHeight()/2);
		path.closePath();
		
		gTemp.setColor(Color.BLACK);
		gTemp.fill(path);
		
//		gTemp.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		gTemp.dispose();
	}
}
