package swing.x.y;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class CasePanel extends JPanel {

	private int width;
	private int height;
	private String text;
	
	public CasePanel(int width, int height, String text) {
		this.width = width;
		this.height = height;
		this.text = text;
		this.setSize(new Dimension(width,height));
	}
	
	public void paintComponent(Graphics g) {
		
		Graphics2D gTemp = (Graphics2D)g.create();
		
		GradientPaint background = new GradientPaint(0f,0f,Color.GRAY.brighter(),0f,(float)getHeight()/2,Color.WHITE);
		gTemp.setPaint(background);
		gTemp.fillRect(0, 0, getWidth(), getHeight() / 2);
		
		background = new GradientPaint(0f,getHeight() / 2, Color.WHITE, 0f, (float)getHeight(), Color.GRAY.brighter());
		gTemp.setPaint(background);
		gTemp.fillRect(0, getHeight() / 2, getWidth(), getHeight()/2);
		
		gTemp.setColor(Color.BLACK);
		gTemp.setFont(new Font("",Font.BOLD,15));
		gTemp.drawString(text, width/4, height/2);
		
		gTemp.dispose();
	}
	
}
