package swing.x.y;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Direct3MainFrame extends JFrame{
	
	public static int casePanelWidth = 200;
	public static int casePanelHeight = 100;
	
	public static int casePanelWidth_small = casePanelWidth;
	public static int casePanelHeight_small = casePanelHeight/3;
	
	
	public static int interval = 20;
	
	public static int initPanelX = 10;
	public static int initPanelY = 100;
	
	private JPanel contentPanel;
	
	public static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	
	public Direct3MainFrame() {
		this.setTitle("AAAA");
		this.setSize(screen.width, screen.height);
		this.setLayout(new BorderLayout());
		this.setLocationRelativeTo(null);
		
		contentPanel = new JPanel();
		contentPanel.setLayout(null);
		
//		contentPanel.add
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(contentPanel);
		this.add(scrollPane,BorderLayout.CENTER);
		
		int huoqujianyichuliquxianX = initPanelX;
		int huoqujianyichuliquxianY = initPanelY;
		JPanel panel1 = new CasePanel(casePanelWidth_small, casePanelHeight_small,"BBBB");
		contentPanel.add(panel1);
		panel1.setBounds(huoqujianyichuliquxianX, huoqujianyichuliquxianY, panel1.getWidth(), panel1.getHeight());
		
		int line1X = huoqujianyichuliquxianX + casePanelWidth;
		int line1Y = huoqujianyichuliquxianY + casePanelHeight_small/2;
		LinePanel line1 = new LinePanel(interval, casePanelHeight_small/2);
		line1.setDrawLine(new Point(0,line1.getHeight()/2), new Point(line1.getWidth(),line1.getHeight()/2));
		contentPanel.add(line1);
		line1.setBounds(line1X, line1Y, line1.getWidth(), line1.getHeight());
		
		int huoqujiaoyixinxiX = huoqujianyichuliquxianX;
		int huoqujiaoyixinxiY = huoqujianyichuliquxianY + casePanelHeight_small + interval;
		JPanel panel3 = new CasePanel(casePanelWidth_small, casePanelHeight_small, "CCCC");
		contentPanel.add(panel3);
		panel3.setBounds(huoqujiaoyixinxiX, huoqujiaoyixinxiY, panel3.getWidth(), panel3.getHeight());
		
		int line2X = huoqujiaoyixinxiX + casePanelWidth;
		int line2Y = huoqujiaoyixinxiY;
		LinePanel line2 = new LinePanel(interval, casePanelHeight_small);
		line2.setDrawLine(new Point(0,line2.getHeight()/2), new Point(line2.getWidth(),line2.getHeight()/2));
		contentPanel.add(line2);
		line2.setBounds(line2X, line2Y, line2.getWidth(), line2.getHeight());
		
		
		int huoqutingdianxinxiX = huoqujianyichuliquxianX;
		int huoqutingdianxinxiY = huoqujianyichuliquxianY + 2 * (casePanelHeight_small + interval);
		JPanel panel4 = new CasePanel(casePanelWidth_small, casePanelHeight_small, "DDDD");
		contentPanel.add(panel4);
		panel4.setBounds(huoqutingdianxinxiX, huoqutingdianxinxiY, panel4.getWidth(), panel4.getHeight());
		
		int line3X = huoqutingdianxinxiX + casePanelWidth;
		int line3Y = huoqutingdianxinxiY;
		LinePanel line3 = new LinePanel(interval, casePanelHeight_small/2);
		line3.setDrawLine(new Point(0,line3.getHeight()/2), new Point(line3.getWidth(),line3.getHeight()/2));
		contentPanel.add(line3);
		line3.setBounds(line3X, line3Y, line3.getWidth(), line3.getHeight());
		
		int dianxingrixinxibiduiX = huoqujianyichuliquxianX + casePanelWidth + interval;
		int dianxingrixinxibiduiY = huoqujianyichuliquxianY + casePanelHeight/5;
		JPanel panel5 = new CasePanel(casePanelWidth, casePanelHeight, "EEEE");
		contentPanel.add(panel5);
		panel5.setBounds(dianxingrixinxibiduiX, dianxingrixinxibiduiY, panel5.getWidth(), panel5.getHeight());
		
		int line4X = dianxingrixinxibiduiX + casePanelWidth;
		int line4Y = dianxingrixinxibiduiY;
		LinePanel line4 = new LinePanel(interval, casePanelHeight);
		line4.setDrawLine(new Point(0,line4.getHeight()/2), new Point(line4.getWidth(),line4.getHeight()/2));
		contentPanel.add(line4);
		line4.setBounds(line4X, line4Y, line4.getWidth(), line4.getHeight());
		
		int jizuzuheX = dianxingrixinxibiduiX + casePanelWidth + interval;
		int jizuzuheY = dianxingrixinxibiduiY;
		JPanel panel2 = new CasePanel(casePanelWidth, casePanelHeight, "FFFF");
		contentPanel.add(panel2);
		panel2.setBounds(jizuzuheX , jizuzuheY, panel2.getWidth(), panel2.getHeight());
		
		int line5X = jizuzuheX + casePanelWidth;
		int line5Y = jizuzuheY;
		LinePanel line5 = new LinePanel(interval, casePanelHeight);
		line5.setDrawLine(new Point(0,line5.getHeight()/2), new Point(line5.getWidth(),line5.getHeight()/2));
		contentPanel.add(line5);
		line5.setBounds(line5X, line5Y, line5.getWidth(), line5.getHeight());
		
		int zidongdianlifenpeiX = jizuzuheX + casePanelWidth + interval;
		int zidongdianlifenpeiY = dianxingrixinxibiduiY;
		JPanel panel6 = new CasePanel(casePanelWidth, casePanelHeight, "GGGG");
		contentPanel.add(panel6);
		panel6.setBounds(zidongdianlifenpeiX, zidongdianlifenpeiY, panel6.getWidth(), panel6.getHeight());
		
		int line6X = zidongdianlifenpeiX + casePanelWidth;
		int line6Y = zidongdianlifenpeiY;
		LinePanel line6 = new LinePanel(interval, casePanelHeight);
		line6.setDrawLine(new Point(0,line6.getHeight()/2), new Point(line6.getWidth(),line6.getHeight()/2));
		contentPanel.add(line6);
		line6.setBounds(line6X, line6Y, line6.getWidth(), line6.getHeight());
		
		
		int zidongbianzhiwanchengX = zidongdianlifenpeiX + casePanelWidth + interval;
		int zidongbianzhiwanchengY = dianxingrixinxibiduiY;
		JPanel panel7 = new CasePanel(casePanelWidth, casePanelHeight, "HHHH");
		contentPanel.add(panel7);
		panel7.setBounds(zidongbianzhiwanchengX, zidongbianzhiwanchengY, panel7.getWidth(), panel7.getHeight());
	}
	
}
