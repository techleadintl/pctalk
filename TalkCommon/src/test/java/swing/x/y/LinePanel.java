package swing.x.y;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.GeneralPath;

import javax.swing.JPanel;

public class LinePanel extends JPanel {

	private int width;
	private int height;
	
	private Point startP;
	private Point endP;
	
	private double direction = 5;
	
	public LinePanel(int width, int height) {
		this.width = width;
		this.height = height;
		this.setPanelSize(new Dimension(width,height));
	}
	
	public LinePanel(int width, int height,Point startP, Point endP) {
		this.width = width;
		this.height = height;
		this.startP = startP;
		this.endP = endP;
		this.setPanelSize(new Dimension(width,height));
	}
	
	
	public void setPanelSize(Dimension d) {
		this.setSize(d);
	}
	
	public void paintComponent(Graphics g) {
		Graphics2D gTemp = (Graphics2D)g.create();
		gTemp.setColor(Color.BLACK);
		gTemp.drawLine(startP.x,startP.y, endP.x, endP.y);
		
		GeneralPath path = new GeneralPath();
		path.moveTo(endP.getX(), endP.getY());
		path.lineTo(endP.getX()-direction, endP.getY() - direction);
		path.lineTo(endP.getX()-5, endP.getY() + 5);
		path.closePath();
		gTemp.setColor(Color.BLACK);
		gTemp.fill(path);
		
		gTemp.dispose();
	}
	
	public void setDrawLine(Point startP, Point endP) {
		this.startP = startP;
		this.endP = endP;
		this.repaint();
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Point getStartP() {
		return startP;
	}

	public void setStartP(Point startP) {
		this.startP = startP;
	}

	public Point getEndP() {
		return endP;
	}

	public void setEndP(Point endP) {
		this.endP = endP;
	}
	
}
