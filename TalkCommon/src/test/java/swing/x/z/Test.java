package swing.x.z;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.plaf.FontUIResource;

public class Test extends JFrame implements ActionListener {
	public static final Font f12 = new Font("宋体", Font.PLAIN, 12);
	private static final int FRAME_WIDTH = 700;
	private static final int FRAME_HEIGHT = 740;
	JTextField tf1, tf2, tf3, tf4, tf5, tf6;
	JRadioButton rbt;
	Container mainPane;
	JPanel northPane, centerPane, southPane;
	JButton northLineButton, northRoundButton, northSanButton, northSMButton;
	JPanel linePane, roundPane, sanPanel, SMPanel;
	JTextField lineFieldx1, lineFieldx2, lineFieldy1, lineFieldy2, lineFieldx3, lineFieldy3;
	JTextField roundField, roundField2, roundField3;
	JTextField SlineFieldx1, SlineFieldx2, SlineFieldy1, SlineFieldy2, SlineFieldx3, SlineFieldy3;

	// constructor
	public Test() {
		//setBackground(Color.pink);
		init();
	}

	private void init() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
		setTitle("画图!");
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// setResizable(false);
		initGlobalFontSetting(f12);
		mainPane = getContentPane();
		mainPane.setLayout(new BorderLayout());
		mainPane.add(createNorthPane(false), BorderLayout.NORTH);
		setVisible(true);

	}

	private void initGlobalFontSetting(Font fnt) {
		FontUIResource fontRes = new javax.swing.plaf.FontUIResource(fnt);
		Enumeration keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value instanceof javax.swing.plaf.FontUIResource)
				UIManager.put(key, fontRes);
		}
	}

	private JPanel createNorthPane(boolean force) {
		if (northPane == null || force) {
			northSanButton = new JButton("画三角形");
			northSanButton.addActionListener(this);
			northLineButton = new JButton("画直线");
			northLineButton.addActionListener(this);
			northRoundButton = new JButton("画圆");
			northRoundButton.addActionListener(this);
			northSMButton = new JButton("使用说明！");
			northSMButton.addActionListener(this);

			northPane = new JPanel();
			northPane.add(northSanButton);
			northPane.add(northLineButton);
			northPane.add(northRoundButton);
			northPane.add(northSMButton);
		}
		return northPane;
	}

	private JPanel createSMPanel(boolean force) {
		SMPanel = new JPanel();
		return SMPanel;
	}

	private JPanel createRoundPanel(boolean force) {
		if (roundPane == null || force) {
			roundField = new JTextField(4);
			roundField2 = new JTextField(4);
			roundField3 = new JTextField(4);
			JButton bt2 = new JButton("画圆");
			bt2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					DrawRound dRound = new DrawRound(Integer.parseInt(roundField.getText()), Integer.parseInt(roundField2.getText()), Integer.parseInt(roundField3.getText()));
					dRound.setSize(400, 420);
					updateMainPane(createNorthPane(false), dRound, createRoundPanel(false));
					DrawLine dLine2 = new DrawLine(10, 200, 500, 200);
				}
			});
			roundPane = new JPanel();
			roundPane.add(new JLabel("半径："));
			roundPane.add(roundField);
			roundPane.add(new JLabel("原点坐标："));
			roundPane.add(new JLabel("O("));
			roundPane.add(roundField2);
			roundPane.add(new JLabel(", "));
			roundPane.add(roundField3);
			roundPane.add(new JLabel(") "));
			roundPane.add(bt2);
		}
		return roundPane;

	}

	private JPanel createLinePane(boolean force) {
		if (linePane == null || force) {
			lineFieldx1 = new JTextField(4);
			lineFieldy1 = new JTextField(4);
			lineFieldx2 = new JTextField(4);
			lineFieldy2 = new JTextField(4);
			JButton bt = new JButton("画直线");
			bt.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					DrawLine dLine = new DrawLine(Integer.parseInt(lineFieldx1.getText()), Integer.parseInt(lineFieldy1.getText()), Integer.parseInt(lineFieldx2.getText()),
							Integer.parseInt(lineFieldy2.getText()));
					dLine.setSize(400, 420);
					updateMainPane(createNorthPane(false), dLine, createLinePane(false));
					DrawLine dLine2 = new DrawLine(10, 200, 500, 200);
				}
			});
			linePane = new JPanel();
			linePane.add(new JLabel("直线的端点坐标："));
			linePane.add(new JLabel("A("));
			linePane.add(lineFieldx1);
			linePane.add(new JLabel(", "));
			linePane.add(lineFieldy1);
			linePane.add(new JLabel(") "));
			linePane.add(new JLabel("B("));
			linePane.add(lineFieldx2);
			linePane.add(new JLabel(", "));
			linePane.add(lineFieldy2);
			linePane.add(new JLabel(")"));
			linePane.add(bt);
		}
		return linePane;
	}

	private JPanel createSanPane(boolean force) {
		if (linePane == null || force) {
			SlineFieldx1 = new JTextField(4);
			SlineFieldy1 = new JTextField(4);
			SlineFieldx2 = new JTextField(4);
			SlineFieldy2 = new JTextField(4);
			SlineFieldx3 = new JTextField(4);
			SlineFieldy3 = new JTextField(4);
			JButton bt1 = new JButton("画三角形");
			bt1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					DrawSan dSan = new DrawSan(Integer.parseInt(SlineFieldx1.getText()), Integer.parseInt(SlineFieldy1.getText()), Integer.parseInt(SlineFieldx2.getText()),
							Integer.parseInt(SlineFieldy2.getText()), Integer.parseInt(SlineFieldx3.getText()), Integer.parseInt(SlineFieldy3.getText()));
					dSan.setSize(400, 420);
					updateMainPane(createNorthPane(false), dSan, createSanPane(false));
					DrawLine dSan2 = new DrawLine(10, 200, 500, 200);
				}
			});
			sanPanel = new JPanel();
			sanPanel.add(new JLabel("三顶点坐标："));
			sanPanel.add(new JLabel("A("));
			sanPanel.add(SlineFieldx1);
			sanPanel.add(new JLabel(","));
			sanPanel.add(SlineFieldy1);
			sanPanel.add(new JLabel(") ;"));

			sanPanel.add(new JLabel("B("));
			sanPanel.add(SlineFieldx2);
			sanPanel.add(new JLabel(", "));
			sanPanel.add(SlineFieldy2);
			sanPanel.add(new JLabel(") ;"));

			sanPanel.add(new JLabel("C("));
			sanPanel.add(SlineFieldx3);
			sanPanel.add(new JLabel(","));
			sanPanel.add(SlineFieldy3);
			sanPanel.add(new JLabel(") ;"));

			sanPanel.add(bt1);
		}
		return sanPanel;
	}

	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();
		if (source == northSanButton) {
			drawSan();

		} else if (source == northLineButton) {
			drawLine();
		} else if (source == northSMButton) {
			drawSM();
		} else if (source == northRoundButton)
			drawRound();
	}

	private void drawSM() {
		updateMainPane(createNorthPane(false), new JPanelSM(), createSMPanel(true));

	}

	private void drawLine() {
		updateMainPane(createNorthPane(false), new JPanelsan(), createLinePane(true));
	}

	private void drawSan() {

		updateMainPane(createNorthPane(false), new JPanelsan(), createSanPane(true));
	}

	private void drawRound() {
		updateMainPane(createNorthPane(false), new JPanelsan(), createRoundPanel(true));
	}

	private void updateMainPane(JPanel north, JPanel center, JPanel south) {
		mainPane.removeAll();
		mainPane.add(north, BorderLayout.NORTH);
		mainPane.add(center, BorderLayout.CENTER);
		mainPane.add(south, BorderLayout.SOUTH);
		mainPane.validate();

		mainPane.repaint();

	}

	public static void main(String[] arg) {

		Test test = new Test();
	}
}