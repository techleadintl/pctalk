package swing.x.z;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class JPanelsan extends JPanel {

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.gray);
		Dimension panelSize = this.getSize();
		Location center = new Location(panelSize.width / 2, panelSize.height / 2);
		int radius = (int) ((Math.min(panelSize.width, panelSize.height) / 2) * 0.9);
		int[] x = new int[2 * radius + 1];
		int[] y = new int[2 * radius + 1];
		g.setColor(Color.black);
		// 画坐标轴
		g.drawLine(0, center.y, 2 * center.x, center.y);// x
		g.drawLine(center.x, 0, center.x, 2 * center.y);// y
		for (int i = -center.x / 10; i < center.x / 10; i++) {
			g.drawLine(center.x + 10 * i, center.y, center.x + 10 * i, center.y - 2);// x
			g.drawLine(center.x, center.y + 10 * i, center.x - 2, center.y + 10 * i);// y
		}
		g.drawLine(2 * center.x, center.y, 2 * center.x - 10, center.y - 10);
		g.drawLine(2 * center.x, center.y, 2 * center.x - 10, center.y + 10);
		g.drawLine(center.x, 0, center.x - 10, 10);
		g.drawLine(center.x, 0, center.x + 10, 10);

		g.drawPolyline(x, y, 2 * radius + 1);
		g.setColor(Color.red);
		g.setFont(new Font("ScanSerif", Font.BOLD, 12));
		g.drawString("单位长度为：10个单位", 60, 60);
		g.drawString("O", center.x - 10, center.y + 10);
		g.drawString("X/" + center.x, 2 * center.x - 30, center.y - 50);
		g.drawString("Y/" + center.y, center.x + 30, 30);

	}

}
