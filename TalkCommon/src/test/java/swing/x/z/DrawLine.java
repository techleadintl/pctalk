package swing.x.z;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;
 
public class DrawLine extends JPanel {
 
       int a, b, c, d, i;
 
       DrawLine() {
       }
 
       DrawLine(int a, int b, int c, int d) {
              this.a = a;
              this.b = b;
              this.c = c;
              this.d = d;
       }
 
       public void paintComponent(Graphics g) {
 
              super.paintComponent(g);
              setBackground(Color.gray);
              Dimension panelSize = this.getSize();
              System.out.println(panelSize.width + "   " + panelSize.height);
              Location center = new Location(panelSize.width / 2,
                            panelSize.height / 2);
              int radius = (int) ((Math.min(panelSize.width, panelSize.height) / 2) * 0.9);
 
              int[] x = new int[2 * radius + 1];
              int[] y = new int[2 * radius + 1];
 
              g.setColor(Color.black);
              // 画坐标轴
 
              g.drawLine(0, center.y, 2 * center.x, center.y);// x
              g.drawLine(center.x, 0, center.x, 2 * center.y);// y
              for (int i = -center.x / 10; i < center.x / 10; i++) {
                     g.drawLine(center.x + 10 * i, center.y, center.x + 10 * i,
                                   center.y - 2);// x
                     g.drawLine(center.x, center.y + 10 * i, center.x - 2, center.y + 10
                                   * i);// y
              }
              g.drawLine(2 * center.x, center.y, 2 * center.x - 10, center.y - 10);
              g.drawLine(2 * center.x, center.y, 2 * center.x - 10, center.y + 10);
              g.drawLine(center.x, 0, center.x - 10, 10);
              g.drawLine(center.x, 0, center.x + 10, 10);
 
              g.drawPolyline(x, y, 2 * radius + 1);
 
              g.setColor(Color.red);
              g.setFont(new Font("ScanSerif", Font.BOLD, 12));
              g.drawString("单位长度为：10个单位", 60, 60);
 
              g.drawString("A点坐标:（" + a + "," + b + ")", 60, 90);
              g.drawString("B点坐标:（" + c + "," + d + ")", 60, 120);
              g.drawString("O", center.x - 10, center.y + 10);
 
              g.drawString("X/" + center.x, 2 * center.x - 30, center.y - 50);
              g.drawString("Y/" + center.y, center.x + 30, 30);
 
              g.drawLine(a + center.x, center.y - b, center.x + c, center.y - d);
              String m = "A(" + a + "," + b + ")";
              g.drawString(m, (a + center.x) + 5, (center.y - b) + 5);
              m = "B(" + c + "," + d + ")";
              g.drawString(m, (c + center.x) + 5, (center.y - d) + 5);
       }
 
}