package swing.x.z;
import java.awt.*;
import javax.swing.*;
 
public class JPanelSM extends JPanel {
 
       public void paintComponent(Graphics g) {
              super.paintComponent(g);
              setBackground(Color.gray);
              Image img = Toolkit.getDefaultToolkit().getImage("1.gif");
              Dimension panelSize = this.getSize();
              Location center = new Location(panelSize.width / 2,
                            panelSize.height / 2);
              int radius = (int) ((Math.min(panelSize.width, panelSize.height) / 2) * 0.9);
              int[] x = new int[2 * radius + 1];
              int[] y = new int[2 * radius + 1];
              g.setColor(Color.red);
              String a = "本画图程序为一个简易的画图程序，包括画直线，画三角形。";
              g.drawString(a, 60, 60);
 
              a = "①画三角形时候，在方框中输入三个顶点的坐标A,B,C  点击“画三角形”按钮 即可在坐标轴中画出所需的三角形。";
              g.drawString(a, 60, 120);
 
              a = "②画直线的时候，在方框中输入起始点和终点的坐标 A,B 点击“画直线”按钮 即可在坐标轴中画出所需的直线。 ";
              g.drawString(a, 60, 180);
 
              a = "③画圆的时候，在方框中输入半径和圆心所在的坐标点O ，点击”画圆“按钮 即可在坐标轴中画出需要的圆。";
              g.drawString(a, 60, 240);
 
              a = "④所填的坐标必须在 面板的范围内，必要的信息会在右上角显示！";
              g.drawString(a, 60, 300);
 
 
              g.setFont(new Font("宋体", Font.BOLD, 20));
              a = "使用说明书";
              g.drawString(a, center.x - 20, 20);
 
              g.drawImage(img, 200, 200, 200, 200, this);
}
}