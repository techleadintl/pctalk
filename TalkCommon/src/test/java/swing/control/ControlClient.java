package swing.control;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ControlClient extends JFrame {
	public JPanel jp;

	public static void main(String[] args) {
		ControlClient cc = new ControlClient();
		cc.remoteConnect();
		cc.initFrame();
	}

	public void initFrame() {
		setTitle("");
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(500, 400);
		setDefaultCloseOperation(3);
		setLocationRelativeTo(null);
		setVisible(true);
		ClientRun(ClientSingle.getClientSingle().getObjectInputStream(), this);
		this.addMouseListener(ma);
		this.addKeyListener(ka);
		this.addMouseMotionListener(ma);
		this.addMouseWheelListener(ma);
	}

	MouseAdapter ma = new MouseAdapter() {
		public void mousePressed(MouseEvent e) {
			SendEvent(e);
		}

		public void mouseClicked(MouseEvent e) {
			SendEvent(e);
		}

		public void mouseReleased(MouseEvent e) {
			SendEvent(e);
		}

		public void mouseMoved(MouseEvent e) {
			SendEvent(e);
		}

		public void mouseDragged(MouseEvent e) {
			SendEvent(e);
		};

		public void mouseEntered(MouseEvent e) {
			SendEvent(e);
		};

		@Override
		public void mouseExited(MouseEvent e) {
			SendEvent(e);
		}

		public void mouseWheelMoved(java.awt.event.MouseWheelEvent e) {
			e.getWheelRotation();
			int scrollAmount = e.getScrollAmount();
			System.out.println(e.getWheelRotation()+",,,"+scrollAmount);
			SendEvent(e);
		};
	};
	KeyAdapter ka = new KeyAdapter() {
		public void keyPressed(KeyEvent e) {
			SendEvent(e);
		}

		public void keyReleased(KeyEvent e) {
			SendEvent(e);
		}
	};

	private void SendEvent(InputEvent e) {
		try {
			System.out.println(e);
			ClientSingle.getClientSingle().getObjectOutputStream().writeObject(e);
			ClientSingle.getClientSingle().getObjectOutputStream().flush();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	private void remoteConnect() {//��ȡ����
		ClientSingle.getClientSingle().getConnect();
		System.out.println("���ӷ������ɹ�......");
	}

	private void ClientRun(final ObjectInputStream ois, final JFrame jf) {
		new Thread() {
			public void run() {
				while (true) {
					try {
						Object obj = ois.readObject();
						byte[] data = (byte[]) obj;

						ByteArrayInputStream bins = new ByteArrayInputStream(data);
						BufferedImage image = ImageIO.read(bins);
						javax.swing.ImageIcon ic = new ImageIcon(image);

						Image img = ic.getImage();

						Graphics g = jf.getGraphics();

						g.drawImage(img, 0, 0, null);

						//						File file = getImage(ois);
						//						System.out.println("�ͻ��˽���ͼƬ");
						//						ImageIcon icon = new ImageIcon(file.getAbsolutePath());
						//						count ++ ;//��ȡ�������ص�ͼƬ���������������޷���ȡ�ڶ���ͼƬ
						//						g.drawImage(icon.getImage(), 0, 0, null);
						//						System.out.println("ͼƬ�Ѷ�����ƽ���...");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
		}.start();
	}
}