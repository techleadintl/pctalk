package swing.control;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ByControlServer {
	private Robot robot;

	{
		try {
			robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ByControlServer bcs = new ByControlServer();
		bcs.initServer();
	}

	public void initServer() {
		new Thread() {
			@Override
			public void run() {
				try {
					ServerSocket ss = new ServerSocket(9091);
					Socket socket = ss.accept();
					System.out.println("�пͻ��������Ϸ�����......");
					ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
					while (true) {
						Object obj = ois.readObject();
						if (obj instanceof MouseWheelEvent) {
							MouseWheelEvent me = (MouseWheelEvent) obj;
							int wheelRotation = me.getWheelRotation();
							robot.mouseWheel(me.getScrollAmount() * wheelRotation);
						} else if (obj instanceof MouseEvent) {
							MouseEvent me = (MouseEvent) obj;
							int id = me.getID();
							robot.mouseMove(me.getX(), me.getY());
							int button = getButton(me.getButton());
							if (button != -1) {
								if (id == MouseEvent.MOUSE_PRESSED) {
									robot.mousePress(button);
								} else if (id == MouseEvent.MOUSE_RELEASED) {
									robot.mouseRelease(button);
								} else if (id == MouseEvent.MOUSE_CLICKED) {
									robot.mousePress(button);
									robot.mouseRelease(button);
								} else if (id == MouseEvent.MOUSE_MOVED) {

								} else if (id == MouseEvent.MOUSE_DRAGGED) {

								}
							}
						} else if (obj instanceof KeyEvent) {
							KeyEvent ke = (KeyEvent) obj;
							int id = ke.getID();
							if (id == KeyEvent.KEY_PRESSED) {
								robot.keyPress(ke.getKeyCode());
							} else if (id == KeyEvent.KEY_RELEASED) {
								robot.keyRelease(ke.getKeyCode());
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
		try {
			ServerSocket ss = new ServerSocket(9090);
			Socket socket = ss.accept();
			System.out.println("�пͻ��������Ϸ�����......");
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			while (true) {
				getImage(oos);
				//				Thread.sleep(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getButton(int button) {
		if (button == MouseEvent.BUTTON1) {
			return InputEvent.BUTTON1_MASK;
		} else if (button == MouseEvent.BUTTON2) {
			return InputEvent.BUTTON2_MASK;
		} else if (button == MouseEvent.BUTTON3) {
			return InputEvent.BUTTON3_MASK;
		}
		return -1;
	}

	public void getImage(ObjectOutputStream oos) {
		try {
			//            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
			//            BufferedImage screenshot = (new Robot()).createScreenCapture(new Rectangle(0,0,(int)d.getWidth(),(int)d.getHeight()));
			//            String name = "C:\\Users\\asus\\Desktop\\jietu.png";
			//            File f = new File(name);
			//            ImageIO.write(screenshot, "png", f);
			//            oos.writeObject(screenshot);
			//            oos.flush();

			Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
			BufferedImage screenshot = (new Robot()).createScreenCapture(new Rectangle(0, 0, (int) d.getWidth(), (int) d.getHeight()));

			java.io.ByteArrayOutputStream temB = new ByteArrayOutputStream();
			//��ͼƬ���д���ڴ�����
			javax.imageio.ImageIO.write(screenshot, "jpeg", temB);
			//��Ϊ�ֽ����鷵��
			byte[] data = temB.toByteArray();

			oos.writeObject(data);

			oos.flush();

		} catch (Exception e) {

		}
	}
}