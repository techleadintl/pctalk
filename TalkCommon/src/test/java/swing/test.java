package swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.xinwei.common.lookandfeel.component.McWillFileDialog;
import com.xinwei.common.lookandfeel.file.filter.McWillFileFilter;

import sun.swing.FilePane;

public class test extends JFrame implements ActionListener {
	JButton jb, jb2;

	public static void main(String[] args) {
		test test = new test();

	}

	public test() {

		jb = new JButton("FileDialog");
		jb.addActionListener(this);
		jb2 = new JButton("JFileChoose");
		jb2.addActionListener(this);

		this.add(jb, BorderLayout.NORTH);
		this.add(jb2, BorderLayout.SOUTH);
		this.setVisible(true);
		this.setSize(130, 100);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	String Filename = null;

	public void Open() {
		FileDialog fd = new FileDialog(this, "FileDialog", FileDialog.SAVE);
		FilenameFilter ff = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				if (name.endsWith("jpg")) {
					return true;
				}
				return false;
			}
		};
		fd.setFilenameFilter(ff);
		fd.setVisible(true);
		if (fd != null) {
			Filename = fd.getDirectory() + fd.getFile();
			System.out.println("FileDialog---->" + Filename);
		}
	}

	public void JFile() {
		McWillFileDialog d = new McWillFileDialog();
		d.setDialogType(JFileChooser.SAVE_DIALOG);
		d.setAcceptAllFileFilterUsed(false);
		d.addChoosableFileFilter(new McWillFileFilter("png"));
		d.addChoosableFileFilter(new McWillFileFilter("jpg"));
		d.addChoosableFileFilter(new McWillFileFilter("gif"));
		d.addChoosableFileFilter(new McWillFileFilter("bmp"));
		d.setDialogTitle("JFileChooser");
		int result = d.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			Filename = d.getSelectedFile().getAbsolutePath();
			System.out.println("JFileChooser---->" + Filename);
		}

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource() == jb) {
			Open();
		} else if (arg0.getSource() == jb2) {
			JFile();
		}
	}

}