package swing.list;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class FilteredJList<E> extends JList {
	private static final long serialVersionUID = 1L;
	private FilterField filterField; // 输入用的文本框＿＿为JTextfield的子类
	private int DEFAULT_FIELD_WIDTH = 20; // 最多输入20个字符

	public FilteredJList() {
		super();
		setModel(new FilterModel<E>());
		filterField = new FilterField(DEFAULT_FIELD_WIDTH);
	}

	public void setModel(ListModel m) {
		if (!(m instanceof FilterModel)) // 仅仅能以FielterModel作为JList的模型
			throw new IllegalArgumentException();
		super.setModel(m);
	}

	private FilterModel<E> getFilterModel() {
		return (FilterModel<E>) getModel();
	}

	public void addItem(E o) {
		getFilterModel().addElement(o);
	}

	public JTextField getFilterField() {
		return filterField;
	}

	class FilterModel<E> extends AbstractListModel {
		private static final long serialVersionUID = 1L;
		ArrayList<E> items; // 原始数据
		ArrayList<E> filterItems;// 过滤后的显示数据

		public FilterModel() {
			super();
			items = new ArrayList<E>();
			filterItems = new ArrayList<E>();
		}

		public E getElementAt(int index) {
			if (index < filterItems.size())
				return filterItems.get(index);
			else
				return null;
		}

		public int getSize() {
			return filterItems.size();
		}

		public void addElement(E o) {
			items.add(o);
			refilter();
		}

		private void refilter() { //完成过滤逻辑的函数
			// public void refilter() {
			filterItems.clear(); //清空用于显示的数据容器
			String term = getFilterField().getText(); //获取输入的字串
			for (int i = 0; i < items.size(); i++) //遍历原始数据容器列表
			{
				if (items.get(i).toString().indexOf(term, 0) != -1) {
					filterItems.add(items.get(i)); //如果满足条件被放入到过滤后的数据容器中用于显示
				}
			}
			fireContentsChanged(this, 0, getSize()); //通知JList的视图更新显示数据源自filterItems
			//它会产生ListDataEvent 事件给　JList　JList重新从模型中获取数据并重绘（repaint)自己．
		}
	}

	// FilterField inner class listed below
	// inner class provides filter-by-keystroke field
	class FilterField extends JTextField implements DocumentListener {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		// 在文本框中的增　删　改　都会改变其模型中的数据并激发DocumentEvent事 .在此事件发生时
		// 唯一要做的就是对JList的FilterModel重新进行过滤，过滤后JList就会显示过滤后的数据
		public FilterField(int width) {
			super(width);
			getDocument().addDocumentListener(this);
		}

		public void changedUpdate(DocumentEvent e) {
			getFilterModel().refilter();

		}

		public void insertUpdate(DocumentEvent e) {
			getFilterModel().refilter();
		}

		public void removeUpdate(DocumentEvent e) {
			getFilterModel().refilter();
		}

	}

	public static void main(String[] args) {
		String[] listItems = { "Chris", "Joshua", "Daniel", "Michael", "Don", "Kimi", "Kelly", "Keagan" };
		JFrame frame = new JFrame("McWillFilterList");
		frame.getContentPane().setLayout(new BorderLayout());
		// populate list
		FilteredJList<String> list = new FilteredJList<String>();
		for (int i = 0; i < listItems.length; i++)
			list.addItem(listItems[i]);
		// add to gui
		JScrollPane pane = new JScrollPane(list, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		frame.getContentPane().add(pane, BorderLayout.CENTER);
		frame.getContentPane().add(list.getFilterField(), BorderLayout.NORTH);
		frame.pack();
		frame.setVisible(true);
	}
}
