package swing.list;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class MultiLineList {
	private static final long serialVersionUID = 1L;

	public static void main(final String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new MultiLineList();
			}
		});
	}

	private MultiLineList() {
		JFrame f = new JFrame("MultiLineList");
		f.setResizable(true);
		f.setVisible(true);
		f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		f.getContentPane().setLayout(new BorderLayout());

		final DefaultListModel model = new DefaultListModel();
		model.addElement("This is a short text");
		model.addElement(
				"This is a long text. This is a long text. This is a long text. This is a long text. This is a long text. This is a long text. This is a long text. This is a long text. This is a long text. This is a long text. This is a long text. This is a long text. This is a long text. ");
		model.addElement(
				"This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. This is an even longer text. ");

		final JList list = new JList(model);
		list.setCellRenderer(new MyCellRenderer());
		list.setBorder(BorderFactory.createEtchedBorder());
		f.add(list);

		f.pack();
	}

	public class MyCellRenderer implements ListCellRenderer {

		private JPanel p;
		private JPanel iconPanel;
		private JLabel l;
		private JTextArea ta;

		public MyCellRenderer() {
			p = new JPanel();
			p.setLayout(new BorderLayout());

			// icon
			iconPanel = new JPanel(new BorderLayout());
			l = new JLabel("icon"); // <-- this will be an icon instead of a
									// text
			iconPanel.add(l, BorderLayout.NORTH);
			p.add(iconPanel, BorderLayout.WEST);

			// text
			ta = new JTextArea();
			ta.setLineWrap(true);
			ta.setWrapStyleWord(true);
			p.add(ta, BorderLayout.CENTER);
		}

		@Override
		public Component getListCellRendererComponent(final JList list, final Object value, final int index, final boolean isSelected, final boolean hasFocus) {

			ta.setText((String) value);
			int width = list.getParent().getParent().getWidth();
			// this is just to lure the ta's internal sizing mechanism into action
			if (width > 0)
				ta.setSize(width, Short.MAX_VALUE);
			return p;

		}
	}
}