package swing.list;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

public class Main extends JFrame {

	public Main() {
		getContentPane().setLayout(new FlowLayout());

		Vector v = new Vector();
		for (int i = 0; i < 50; i++) {
			v.addElement(new JMenuItem("Item #" + i));
		}
		getContentPane().add(new JLabel("Double-clicked on: "));
		final JTextField dblTextField = new JTextField(15);
		getContentPane().add(dblTextField);
		getContentPane().add(new JLabel("Enter MOUSE_KEY on: "));
		final JTextField entTextField = new JTextField(15);
		getContentPane().add(entTextField);
		final JList list = new JList(v);
		
		list.setCellRenderer(new ListCellRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				// TODO Auto-generated method stub
				return null;
			}
		});

		// catch double-click events
		list.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				if (me.getClickCount() == 2) {
					dblTextField.setText("" + list.getSelectedValue());
				}
			}
		});

		// catch enter-MOUSE_KEY events
		list.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent ke) {
				if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
					entTextField.setText("" + list.getSelectedValue());
				}
			}
		});

		getContentPane().add(new JScrollPane(list));

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event) {
				System.exit(0);
			}
		});

		setSize(200, 300);
	}

	public static void main(String[] args) {
		(new Main()).show();
	}
}
