package swing.list;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;

public class SwingListExample extends JPanel {

	private CategoryList booklist = new CategoryList();

	public SwingListExample() {
		setLayout(new BorderLayout());
		JButton button = new JButton("Print");

		DefaultListModel model = new DefaultListModel();
		booklist = new CategoryList();
		booklist.setModel(model);
		for (int i = 0; i < 15; i++)
			model.addElement("Element " + i);

		JScrollPane pane = new JScrollPane(booklist);

		add(pane, BorderLayout.NORTH);
		add(button, BorderLayout.SOUTH);
	}

	public static void main(String s[]) {
		JFrame frame = new JFrame("List Example");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(new SwingListExample());
		frame.pack();
		frame.setVisible(true);
	}

}
