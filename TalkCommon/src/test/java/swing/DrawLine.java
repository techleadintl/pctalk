package swing;

import java.awt.*;

import java.awt.event.*;

import java.util.EventListener;

import java.awt.event.MouseMotionListener;

public class DrawLine extends Frame implements MouseListener, MouseMotionListener {

	int startX, startY; //定义画图的起点X和Y的坐标

	int endX, endY; //定义画图的终点X和Y的坐标

	boolean drawing = false;

	public DrawLine() {

		// TODO Auto-generated constructor stub

		super("涂鸦面板");

		this.addMouseListener(this);

		this.addMouseMotionListener(this);

		this.addWindowListener(new WindowListener() {

			public void windowActivated(WindowEvent arg0) {

				// TODO Auto-generated method stub

			}

			public void windowClosed(WindowEvent arg0) {

				// TODO Auto-generated method stub

			}

			public void windowClosing(WindowEvent arg0) {

				// TODO Auto-generated method stub

				System.exit(0);

			}

			public void windowDeactivated(WindowEvent arg0) {

				// TODO Auto-generated method stub

			}

			public void windowDeiconified(WindowEvent arg0) {

				// TODO Auto-generated method stub

			}

			public void windowIconified(WindowEvent arg0) {

				// TODO Auto-generated method stub

			}

			public void windowOpened(WindowEvent arg0) {

				// TODO Auto-generated method stub

			}

		});

		this.setSize(180, 160);

		this.setVisible(true);

	}

	public void paint(Graphics g) {

		super.paint(g);

		g.drawLine(startX, startY, endX, endY);

	}

	public void update(Graphics g) {

		this.paint(g);

	}

	/**
	 * 
	 * @param args
	 * 
	 */

	public static void main(String[] args) {

		// TODO Auto-generated method stub

		new DrawLine();

	}

	public void mouseClicked(MouseEvent e) {

		// TODO Auto-generated method stub

	}

	public void mouseEntered(MouseEvent e) {

		// TODO Auto-generated method stub

	}

	public void mouseExited(MouseEvent e) {

		// TODO Auto-generated method stub

	}

	public void mousePressed(MouseEvent e) {

		// TODO Auto-generated method stub

		this.drawing = true;

		this.startX = e.getX();

		this.startY = e.getY();

	}

	public void mouseReleased(MouseEvent e) {

		// TODO Auto-generated method stub

		this.drawing = false;

	}

	public void mouseDragged(MouseEvent e) {

		// TODO Auto-generated method stub

		if (drawing) {

			this.endX = e.getX();

			this.endY = e.getY();

			this.startX = e.getX(); //当前点作为起始点

			this.startY = e.getY();

			this.repaint();

		}

	}

	public void mouseMoved(MouseEvent e) {

		// TODO Auto-generated method stub

	}

}
