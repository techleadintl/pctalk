package swing;

import java.awt.Dimension;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.sun.awt.AWTUtilities;

public class JRoundFrame extends JFrame {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JFrame.setDefaultLookAndFeelDecorated(true);
				JRoundFrame frame = new JRoundFrame();
				frame.setSize(new Dimension(200, 300));
				/** 设置圆角 */
				AWTUtilities.setWindowShape(frame, new RoundRectangle2D.Double(0.0D, 0.0D, frame.getWidth(), frame.getHeight(), 5.0D, 5.0D));
				frame.setVisible(true);
			}

		});
	}
}