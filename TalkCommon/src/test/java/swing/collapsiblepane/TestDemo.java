package swing.collapsiblepane;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import com.xinwei.common.lookandfeel.component.McWillCollapsiblePane;
import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;

public class TestDemo {
	public static void main(String[] args) {

		// 创建没有父节点和子节点、但允许有子节点的树节点，并使用指定的用户对象对它进行初始化。
		// public DefaultMutableTreeNode(Object userObject)
		DefaultMutableTreeNode node1 = new DefaultMutableTreeNode("软件部");
		node1.add(new DefaultMutableTreeNode(new User1("小花")));
		node1.add(new DefaultMutableTreeNode(new User1("小虎")));
		node1.add(new DefaultMutableTreeNode(new User1("小龙")));

		DefaultMutableTreeNode node2 = new DefaultMutableTreeNode("销售部");
		node2.add(new DefaultMutableTreeNode(new User1("小叶")));
		node2.add(new DefaultMutableTreeNode(new User1("小雯")));
		node2.add(new DefaultMutableTreeNode(new User1("小夏")));

		DefaultMutableTreeNode top = new DefaultMutableTreeNode("职员管理");

		top.add(new DefaultMutableTreeNode(new User1("总经理")));
		top.add(node1);
		top.add(node2);
		final JTree tree = new JTree(top);

		final JPanel groupsPanel = new JPanel(new VerticalFlowLayout(VerticalFlowLayout.TOP, 0, 0, true, false));
		groupsPanel.setBackground(Color.white);

		McWillCollapsiblePane pane = new McWillCollapsiblePane("Test");
		pane.setContentPane(tree);
		
		McWillCollapsiblePane pane1 = new McWillCollapsiblePane("Test1");
		pane1.setContentPane(new JLabel("xxx"));
		
		groupsPanel.add(pane);
		groupsPanel.add(pane1);

		JFrame f = new JFrame("JTreeDemo");
		f.add(groupsPanel);
		f.setSize(300, 300);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}

class User1 {
	private String name;

	public User1(String n) {
		name = n;
	}

	// 重点在toString，节点的显示文本就是toString
	public String toString() {
		return name;
	}
}