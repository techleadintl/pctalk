package swing.datepicker;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.EventListener;
import java.util.GregorianCalendar;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.EventListenerList;

import com.xinwei.talk.common.Res;

public class DateChooser2 extends JPanel {

	protected Color weekBackgroundColor = new Color(189, 235, 238);
	protected Color weekendBtnFontColor = new Color(240, 64, 64); // color
	protected Color selectedColor = weekBackgroundColor;
	protected Font labelFont = new Font("Arial", Font.PLAIN, 12);
	protected Font headFont = new Font("Arial", Font.PLAIN, 14);
	protected Color defaultBtnFontColor = Color.BLACK;
	protected Color defaultBtnBackgroundColor = Color.WHITE;
	private Calendar cal = null;
	private Calendar todayCal = null;
	private int year;
	private int month;
	private int day;
	private JPanel navigatePanel;
	private JPanel dateHeadPanel = null;
	private JPanel dateContainerPanel = null;
	protected DateButton[][] buttonDays = null;

	protected String[] weekTitle = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
	protected int[] months = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
	private EventListenerList actionListenerList = new EventListenerList();

	public DateChooser2() {
		setLayout(new BorderLayout(0, 0));
		cal = Calendar.getInstance();

		buttonDays = new DateButton[6][7];
		todayCal = Calendar.getInstance();
		this.year = cal.get(Calendar.YEAR);
		this.month = cal.get(Calendar.MONTH);
		this.day = cal.get(Calendar.DATE);

		navigatePanel = new NavigatePanel();
		navigatePanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

		dateHeadPanel = new JPanel(new GridLayout(1, 7, 0, 0));

		dateContainerPanel = new JPanel(new GridLayout(6, 7, 0, 0));
		//		dateContainerPanel.setBorder(BorderFactory.createEtchedBorder());

		JPanel p = new JPanel(new GridLayout(2, 1, 0, 10));
		p.setBackground(weekBackgroundColor);
		p.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
		p.add(navigatePanel);
		p.add(dateHeadPanel);

		add(p, BorderLayout.NORTH);
		add(dateContainerPanel, BorderLayout.CENTER);

		createDayHeadPanel();
		createDayPanel(cal);
		setActiveDay(day);

	}

	private byte[] getImage(String fileName) {
		InputStream is = null;

		try {
			is = new BufferedInputStream(this.getClass().getClassLoader().getResourceAsStream(fileName));
			byte[] b = new byte[is.available()];
			is.read(b);
			return b;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				is.close();
			} catch (IOException e) {
			}
		}
	}

	public JPanel createDayHeadPanel() {
		for (int i = 0; i < 7; i++) {
			JLabel weekLabel = new JLabel(weekTitle[i], SwingConstants.CENTER);
			weekLabel.setFont(headFont);
			weekLabel.setOpaque(true);
			weekLabel.setBackground(weekBackgroundColor);
			dateHeadPanel.add(weekLabel);
		}
		return dateHeadPanel;
	}

	/**
	 * 创建日期组件
	 * 
	 * @param cal
	 *            void
	 */
	public void createDayPanel(Calendar cal) {
		dateContainerPanel.removeAll();
		dateContainerPanel.revalidate();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.DAY_OF_MONTH, -1);

		cal.set(Calendar.DAY_OF_MONTH, 1);
		int weekday = cal.get(Calendar.DAY_OF_WEEK);

		cal.add(Calendar.DAY_OF_MONTH, 1 - weekday);

		DayButtonActionListener dayButtonActionListener = new DayButtonActionListener();

		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				int curMonth = cal.get(Calendar.MONTH);
				DateButton button = null;
				if (curMonth != month) {
					button = new DateButton(" ");
					button.setEnabled(false);
					button.setBackground(defaultBtnBackgroundColor);
				} else {
					int currentDay = cal.get(Calendar.DAY_OF_MONTH);
					button = new DateButton(currentDay + "");
					button.setHorizontalTextPosition(SwingConstants.RIGHT);
					button.setFont(labelFont);
					button.setBackground(defaultBtnBackgroundColor);
					button.setSelectedBackground(weekBackgroundColor);
					if (currentDay == todayCal.get(Calendar.DAY_OF_MONTH) && month == todayCal.get(Calendar.MONTH) && year == todayCal.get(Calendar.YEAR)) {
						button.setBorder(BorderFactory.createLineBorder(weekendBtnFontColor));
					}
					if (cal.get(Calendar.MONTH) != month) {
						button.setForeground(Color.BLUE);
					}
					if (j == 0 || j == 6) {
						button.setForeground(weekendBtnFontColor);
					} else {
						button.setForeground(defaultBtnFontColor);
					}

				}
				button.addActionListener(dayButtonActionListener);
				buttonDays[i][j] = button;
				dateContainerPanel.add(buttonDays[i][j]);
				cal.add(Calendar.DAY_OF_MONTH, 1);
			}
		}

	}

	/**
	 * 选中一天
	 * 
	 * @param selectedDay
	 *            void
	 */
	public void setActiveDay(int selectedDay) {
		clearAllActiveDay();
		if (selectedDay <= 0) {
			day = new GregorianCalendar().get(Calendar.DAY_OF_MONTH);
		} else {
			day = selectedDay;
		}
		int leadGap = new GregorianCalendar(year, month, 1).get(Calendar.DAY_OF_WEEK) - 1;
		JButton selectedButton = buttonDays[(leadGap + selectedDay - 1) / 7][(leadGap + selectedDay - 1) % 7];
		selectedButton.setBackground(weekBackgroundColor);
	}

	/**
	 * 清除所有选择的日期
	 * 
	 */
	public void clearAllActiveDay() {
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				JButton button = buttonDays[i][j];
				if (button.getText() != null && button.getText().trim().length() > 0) {
					button.setBackground(defaultBtnBackgroundColor);
					button.revalidate();
				}
			}

		}
	}

	/**
	 * 获取选中的日期
	 * 
	 * @return String
	 */
	public Calendar getSelectedDate() {
		return new GregorianCalendar(year, month, day);
	}

	class DayButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JButton button = (JButton) e.getSource();
			if (button.getText() != null && button.getText().trim().length() > 0) {
				day = Integer.parseInt(button.getText());
				setActiveDay(day);
				fireActionPerformed(e);// fire button click event
			}
		}
	}

	public void addActionListener(ActionListener actionListener) {
		actionListenerList.add(ActionListener.class, actionListener);
	}

	public void removeActionListener(ActionListener actionListener) {
		actionListenerList.remove(ActionListener.class, actionListener);
	}

	/**
	 * 事件管理,触发事件
	 * 
	 * @param actionEvent
	 *            void
	 */
	protected void fireActionPerformed(ActionEvent actionEvent) {
		EventListener listenerList[] = actionListenerList.getListeners(ActionListener.class);
		for (int i = 0, n = listenerList.length; i < n; i++) {
			((ActionListener) listenerList[i]).actionPerformed(actionEvent);
		}
	}

	public class NavigatePanel extends JPanel implements ActionListener {
		private JButton premon;

		private JButton preyear;

		private JButton nextmon;

		private JButton nextyear;

		private JLabel lbl;

		public NavigatePanel() {
			setBackground(weekBackgroundColor);
			setLayout(new BorderLayout(0, 0));
			Dimension d = new Dimension(20, 20);
			Box box = new Box(BoxLayout.X_AXIS);
			preyear = new JButton();
			preyear.setToolTipText(Res.getMessage("pre.year", "Previous year."));
			ImageIcon icon = new ImageIcon(getImage("preyear.gif"), "<<");
			preyear.setIcon(icon);
			preyear.addActionListener(this);
			preyear.setPreferredSize(d);
			box.add(preyear);

			box.add(Box.createHorizontalStrut(3));

			premon = new JButton();
			premon.setToolTipText(Res.getMessage("pre.mon", "Previous Month"));
			icon = new ImageIcon(getImage("premon.gif"), "<");
			premon.setIcon(icon);
			premon.addActionListener(this);
			premon.setPreferredSize(d);
			box.add(premon);

			add(box, BorderLayout.WEST);

			box = new Box(BoxLayout.X_AXIS);
			nextmon = new JButton();
			nextmon.setToolTipText(Res.getMessage("next.mon", "Next month."));
			icon = new ImageIcon(getImage("nextmon.gif"), ">");
			nextmon.setIcon(icon);
			nextmon.setPreferredSize(d);
			nextmon.addActionListener(this);
			box.add(nextmon);

			box.add(Box.createHorizontalStrut(3));

			nextyear = new JButton();
			nextyear.setToolTipText(Res.getMessage("next.year", "Next year."));
			icon = new ImageIcon(getImage("nextyear.gif"), ">>");
			nextyear.setIcon(icon);
			nextyear.setPreferredSize(d);
			nextyear.addActionListener(this);
			box.add(nextyear);

			add(box, BorderLayout.EAST);

			lbl = new JLabel();
			lbl.setFont(headFont);
			lbl.setHorizontalAlignment(SwingConstants.CENTER);

			add(lbl, BorderLayout.CENTER);
			lbl.setText(Res.getMessage("month." + month, "") + " " + year);
		}

		public void actionPerformed(ActionEvent e) {
			Calendar cal = new GregorianCalendar(year, month, 1);
			if (e.getSource() == premon) {
				cal.add(Calendar.MONTH, -1);
			} else if (e.getSource() == nextmon) {
				cal.add(Calendar.MONTH, 1);
			} else if (e.getSource() == nextyear) {
				cal.add(Calendar.YEAR, 1);
			} else if (e.getSource() == preyear) {
				cal.add(Calendar.YEAR, -1);
			}
			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH);
			createDayPanel(cal);
			lbl.setText(Res.getMessage("month." + month, "") + " " + year);
		}
	}

	public static void main(String[] args) {

		JFrame frame = new JFrame("xxxxxx");
		frame.setSize(400, 300);

		final DateChooser2 datePicker = new DateChooser2();
		datePicker.addActionListener(new ActionListener() {// 事件捕获

			public void actionPerformed(ActionEvent e) {
				System.out.println(datePicker.getSelectedDate());

			}
		});
		frame.getContentPane().add(datePicker);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

}

//-------自定义按钮--------------
