package swing.textPane;

import java.io.PrintStream;

import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.View;

public class TextComponentDisplay {
	public static void displayModel(JTextComponent comp, PrintStream out) {
		Document doc = comp.getDocument();
		if (doc instanceof AbstractDocument) {
			((AbstractDocument) doc).dump(out);
		}

	}

	public static void displayViews(JTextComponent comp, PrintStream out) {
		TextUI textUI = (TextUI) comp.getUI();
		View rootView = textUI.getRootView(comp);

		displayView(rootView, 0, out);
	}

	public static void displayView(View view, int tabs, PrintStream out) {
		// Print info about this view
		for (int i = 0; i < tabs; i++) {
			out.print("\t");
		}

		out.println(view.getClass().getName());

		for (int i = 0; i < tabs; i++) {
			out.print("\t");
		}

		out.println("Start: " + view.getStartOffset() + "; end: " + view.getEndOffset());

		// Display child views, if any.
		int childViews = view.getViewCount();
		for (int i = 0; i < childViews; i++) {
			View childView = view.getView(i);
			displayView(childView, tabs + 1, out);
		}
	}
}