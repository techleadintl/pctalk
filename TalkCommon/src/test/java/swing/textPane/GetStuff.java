package swing.textPane;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
  
public class GetStuff extends JFrame implements MouseListener
{
    private JTextPane tp;
    private JTextField tf;
    private ImageIcon ii;
  
    public GetStuff()
    {
        tp = new JTextPane();
        tf = new JTextField();
        tf.setEditable( false );
        getContentPane().add( tf, BorderLayout.SOUTH );
        JLabel iconLabel = new JLabel();
        ii = new ImageIcon( "Washu.gif" );
        iconLabel.setIcon( ii );
        tp.insertComponent( new JLabel( "Number One" ) );
        tp.insertComponent( iconLabel );
        tp.insertComponent( new JLabel( "Number Two" ) );
        tp.insertComponent( new JLabel( "Number Three" ) );
        tp.insertComponent( new JLabel( "Number Four" ) );
        tp.insertComponent( new JLabel( "Number Five" ) );
        JScrollPane sp = new JScrollPane( tp );
        getContentPane().add( sp, BorderLayout.CENTER );
        tp.addMouseListener( this );
        setSize( 500, 500 );
        setVisible( true );
    }
  
    public static void main( String[] args )
    {
        GetStuff gs = new GetStuff();
        gs.addWindowListener( new WindowAdapter() {
            public void windowClosing( WindowEvent event )
            {
                System.exit( 0 );
            }
        } );
    }
  
    public void mouseClicked( MouseEvent event )
    {
    }
  
    public void mouseEntered( MouseEvent event )
    {
    }
  
    public void mouseExited( MouseEvent event )
    {
    }
  
    public void mousePressed( MouseEvent event )
    {
        int x = event.getX();
        int y = event.getY();
        JLabel selectedLabel = null;
  
        JTextPane source = ( JTextPane )event.getSource();
        Component c = source.getComponentAt( x, y );
        if( c != null )
        {
            Component[] inner = ( ( Container )c ).getComponents();
            for( int n = 0; n < inner.length; n++ )
            {
                if( inner[ n ] instanceof JLabel )
                {
                    selectedLabel = ( JLabel )inner[ n ];
                }
            }
            if( selectedLabel != null )
            {
                if( selectedLabel.getIcon() != null )
                {
                    tf.setText( "The selected label has an icon" );
                }
                else
                {
                    tf.setText( "The selected label was " + selectedLabel.getText() );
                }
            }
        }
    }
  
    public void mouseReleased( MouseEvent event )
    {
    }
}