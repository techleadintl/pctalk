package swing.textPane;

import java.awt.Container;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;

public class forquestion extends JFrame {

	Container textAreaBox;
	LinkedList<JTextComponent> textArea;
	int nameTA;

	public forquestion() {
		int nameTA = 0;

		textArea = new LinkedList<>();

		textAreaBox = Box.createVerticalBox();
		textAreaBox.add(Box.createVerticalGlue());
		addLine();
		this.add(textAreaBox);
		this.setVisible(true);
		this.setSize(100, 100);
	}

	public static void main(String[] args) {
		forquestion app = new forquestion();
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void addLine() {

		JTextComponent temp_ta = createTextComponent();
		textArea.add(temp_ta);
		textAreaBox.add(textArea.getLast());
		textAreaBox.add(Box.createVerticalGlue());
	}

	protected JTextComponent createTextComponent() {
		JTextArea ta = new JTextArea("test");

		/*
		 * if (count%2==0) ta.setForeground(Color.red); else ta.setForeground(Color.GREEN);
		 */

		ta.setFont(new Font("Courier New", Font.PLAIN, 16));
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		ta.setName(Integer.toString(nameTA));
		nameTA += 1;

		basicKey("ENTER", enter, ta);

		ta.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent ev) {
				try {
					taMousePressed(ev);
				} catch (BadLocationException ex) {
					Logger.getLogger(forquestion.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});

		return ta;
	}

	public void basicKey(String s, Action a, JTextArea ta) {

		ta.getInputMap().put(KeyStroke.getKeyStroke(s), s);
		ta.getActionMap().put(s, a);
	}

	Action enter = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {

			addLine();
		}
	};

	private void taMousePressed(java.awt.event.MouseEvent ev) throws BadLocationException {
		int now_focus = Integer.parseInt(ev.getComponent().getName());
		int _caret;
		_caret = textArea.get(now_focus).getCaretPosition();
		System.out.println("CaretPosition:" + _caret);

		Rectangle rectangle = textArea.get(now_focus).modelToView(_caret);
		double x = rectangle.getX();
		//int xc = textArea.get(now_focus).getLocation().x;
		double y = rectangle.getY();
		//int yc = textArea.get(now_focus).getLocation().y;
		//double h = rectangle.getHeight();
		//double w = rectangle.getWidth();
		System.out.println(x);
		System.out.println(y);
		//System.out.println(xc);
		//System.out.println(yc);  
		//System.out.println(h);
		//System.out.println(w);
		System.out.println("");
	}

}