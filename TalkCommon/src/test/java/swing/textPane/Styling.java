package swing.textPane;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import com.xinwei.common.lookandfeel.component.McWillTextPane;  
   
public class Styling  
{  
    public Styling()  
    {  
        String text = "<span>To refer to locations within the sequence, the " +  
                      "coordinates used are the location between two " +  
                      "characters.\nAs the diagram below shows, a location " +  
                      "in a text document can be referred to as a position, " +  
                      "or an offset. This position is zero-based.</span>";  
   
        SimpleAttributeSet aSet = new SimpleAttributeSet();   
        StyleConstants.setForeground(aSet, Color.blue);  
        StyleConstants.setBackground(aSet, Color.orange);  
        StyleConstants.setFontFamily(aSet, "lucida bright italic");  
        StyleConstants.setFontSize(aSet, 18);  
   
        SimpleAttributeSet bSet = new SimpleAttributeSet();  
        StyleConstants.setAlignment(bSet, StyleConstants.ALIGN_CENTER);  
        StyleConstants.setUnderline(bSet, true);  
        StyleConstants.setFontFamily(bSet, "lucida typewriter bold");  
        StyleConstants.setFontSize(bSet, 24);  
   
        JTextPane textPane = new McWillTextPane();  
//        textPane.setContentType("text/html");
        textPane.setText(text);  
        StyledDocument doc = textPane.getStyledDocument();  
//        doc.setCharacterAttributes(105, doc.getLength()-105, aSet, false);  
//        doc.setParagraphAttributes(0, 104, bSet, false);  
   
        
        
        JFrame f = new JFrame();  
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        f.add(new JScrollPane(textPane));  
        f.setSize(400,400);  
        f.setLocation(200,200);  
        f.setVisible(true);  
    }  
   
    public static void main(String[] args)  
    {  
        new Styling();  
    }  
} 