package swing.textPane;

import java.awt.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.text.html.HTMLEditorKit;

public class Styling2 {
	private static String webContent = "<html>" + "<head>" + "<style type=\"text/css\">mytag {color:red}</style>" + "</head>" + "<body>"
			+ "<p style=\"text-align: right\">Hello World JEditorPane jep = new JEditorPane()f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)</p>" + "</body>" + "</html>";

	static int caretPosition1;

	public Styling2() throws BadLocationException {

//		String text = "<table style=\"width:100%;direction: rtl;\"><tr><td></td><td align=\"right\" >fdsafdagdsafasfjk;ahf;koafpojfkpajflpskajflpajflp'ajflapfjal'pfjalpfjalpfjalpfjalpfjalp'fjalpfjalpfjalpfja'lssfdafdasfdasfdsafdasfasfdas</td></tr></table>";

		
		 String text =
		  "<div style=\"text-align:right;\"> <div style=\"float:right; height:10px; width:100px; overflow:hidden; background:gray;\">1To ref coordinates used are thef two  characters.\nAs the diagram below shows, a location "
		  + "in a text document can be referred to as a position, " + "or an offset. This position is zero-based. </div></div>";
		 
		//        SimpleAttributeSet aSet = new SimpleAttributeSet();   
		//        StyleConstants.setForeground(aSet, Color.blue);  
		//        StyleConstants.setBackground(aSet, Color.orange);  
		//        StyleConstants.setFontFamily(aSet, "lucida bright italic");  
		//        StyleConstants.setFontSize(aSet, 18);  
		//   
		//        SimpleAttributeSet bSet = new SimpleAttributeSet();  
		//        StyleConstants.setAlignment(bSet, StyleConstants.ALIGN_CENTER);  
		//        StyleConstants.setUnderline(bSet, true);  
		//        StyleConstants.setFontFamily(bSet, "lucida typewriter bold");  
		//        StyleConstants.setFontSize(bSet, 24);  

		final JEditorPane jep = new JEditorPane() {
			@Override
			public void paint(Graphics g) {
				Rectangle modelToView1;
				try {
					modelToView1 = modelToView(caretPosition1);
					System.out.println("--" + modelToView1);
				} catch (BadLocationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				super.paint(g);
			}
		};

		jep.setEditorKit(new HTMLEditorKit());
		final Document doc = jep.getDocument();
		jep.setCaretPosition(0);
		int caretPosition = jep.getCaretPosition();
		Rectangle modelToView = jep.modelToView(caretPosition);
		jep.setMinimumSize(new Dimension(0, 0));

		jep.setText(text);
		jep.setCaretPosition(doc.getLength());
		caretPosition1 = jep.getCaretPosition();

		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.add(new JScrollPane(jep));
		f.setSize(400, 400);
		f.setLocation(200, 200);
		f.setVisible(true);
	}

	public static void main(String[] args) throws BadLocationException {
		new Styling2();
	}
}