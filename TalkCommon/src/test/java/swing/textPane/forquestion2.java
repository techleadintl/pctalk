package swing.textPane;

import java.awt.Container;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;

public class forquestion2 extends JFrame {

	JTextArea textArea;
	int nameTA;

	public forquestion2() {
		int nameTA = 0;

		createTextComponent();
		this.add(textArea);
		this.setVisible(true);
		this.setSize(100, 100);
	}

	public static void main(String[] args) {
		forquestion2 app = new forquestion2();
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}


	protected JTextComponent createTextComponent() {
		textArea= new JTextArea("test");

		/*
		 * if (count%2==0) ta.setForeground(Color.red); else ta.setForeground(Color.GREEN);
		 */

		textArea.setFont(new Font("Courier New", Font.PLAIN, 16));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setName(Integer.toString(nameTA));
		nameTA += 1;

		textArea.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent ev) {
				try {
					taMousePressed(ev);
				} catch (BadLocationException ex) {
					Logger.getLogger(forquestion2.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});

		return textArea;
	}


	private void taMousePressed(java.awt.event.MouseEvent ev) throws BadLocationException {
		int _caret;
		_caret = textArea.getCaretPosition();
		System.out.println("CaretPosition:" + _caret);

		Rectangle rectangle = textArea.modelToView(_caret);
		double x = rectangle.getX();
		//int xc = textArea.get(now_focus).getLocation().x;
		double y = rectangle.getY();
		//int yc = textArea.get(now_focus).getLocation().y;
		//double h = rectangle.getHeight();
		//double w = rectangle.getWidth();
		System.out.println(x);
		System.out.println(y);
		//System.out.println(xc);
		//System.out.println(yc);  
		//System.out.println(h);
		//System.out.println(w);
		System.out.println("");
	}

}