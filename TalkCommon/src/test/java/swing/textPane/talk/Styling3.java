package swing.textPane.talk;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Shape;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BoxView;
import javax.swing.text.ComponentView;
import javax.swing.text.Element;
import javax.swing.text.IconView;
import javax.swing.text.LabelView;
import javax.swing.text.ParagraphView;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import javax.swing.text.rtf.RTFEditorKit;

import com.xinwei.common.lookandfeel.component.McWillTextPane;

public class Styling3 {
	public Styling3() {
		/*
		 * String text = "<span>To refer to locations within the sequence, the " + "coordinates used are the location between two " + "characters.\nAs the diagram below shows, a location " +
		 * "in a text document can be referred to as a position, " + "or an offset. This position is zero-based.</span>";
		 */
		String text = "<span>To refer to locations within the sequence, the";

		SimpleAttributeSet aSet = new SimpleAttributeSet();
		StyleConstants.setForeground(aSet, Color.blue);
		StyleConstants.setBackground(aSet, Color.orange);
		StyleConstants.setFontFamily(aSet, "lucida bright italic");
		StyleConstants.setFontSize(aSet, 18);
		StyleConstants.setAlignment(aSet, StyleConstants.ALIGN_RIGHT);

		SimpleAttributeSet bSet = new SimpleAttributeSet();
		StyleConstants.setAlignment(bSet, StyleConstants.ALIGN_RIGHT);
		StyleConstants.setUnderline(bSet, true);
		StyleConstants.setFontFamily(bSet, "lucida typewriter bold");
		StyleConstants.setFontSize(bSet, 24);

		JTextPane textPane = new McWillTextPane() {
			@Override
			protected ParagraphView getParagraphView(Element elem) {
				return new ChatParagraphView(elem);
			}
		};

		//		textPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		//        textPane.setContentType("text/html");
		textPane.setText(text);
		StyledDocument doc = textPane.getStyledDocument();
		//		doc.setCharacterAttributes(0, doc.getLength() , aSet, false);
		doc.setParagraphAttributes(0, doc.getLength(), bSet, false);
		//
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.add(new JScrollPane(textPane));
		f.setSize(450, 400);
		f.setLocation(200, 200);
		f.setVisible(true);
	}

	public static void main(String[] args) {
		new Styling3();
	}

	class MyRTFEditorKit extends RTFEditorKit {
		ViewFactory defaultFactory = new MyRTFViewFactory();

		public ViewFactory getViewFactory() {
			return defaultFactory;
		}
	}

	class MyRTFViewFactory implements ViewFactory {
		public View create(Element elem) {
			String kind = elem.getName();
			if (kind != null)
				if (kind.equals(AbstractDocument.ContentElementName)) {
					return new WrapLabelView(elem);
				} else if (kind.equals(AbstractDocument.ParagraphElementName)) {
					return new ParagraphView(elem);
					//					return new MyParagraphView(elem);
				} else if (kind.equals(AbstractDocument.SectionElementName)) {
					return new BoxView(elem, View.Y_AXIS);
					//					return new MySectionView(elem, View.Y_AXIS);
				} else if (kind.equals(StyleConstants.ComponentElementName)) {
					return new ComponentView(elem);
				} else if (kind.equals(StyleConstants.IconElementName)) {
					return new IconView(elem);
				}
			// default to text display
			return new LabelView(elem);
		}
	}

	class WrapLabelView extends LabelView {
		public WrapLabelView(Element elem) {
			super(elem);
		}

		public float getMinimumSpan(int axis) {
			switch (axis) {
			case View.X_AXIS:
				return 0;
			case View.Y_AXIS:
				return super.getMinimumSpan(axis);
			default:
				throw new IllegalArgumentException("Invalid axis: " + axis);
			}
		}

	}

	class MySectionView extends BoxView {
		public MySectionView(Element e, int axis) {
			super(e, axis);
		}

		public void paintChild(Graphics g, Rectangle r, int n) {
			if (n > 0) {
				MyParagraphView child = (MyParagraphView) this.getView(n - 1);
				int shift = child.shift + child.childCount;

				MyParagraphView current = (MyParagraphView) this.getView(n);
				current.shift = shift;
				System.out.println("box:" + shift + "," + child.childCount);
			}
			super.paintChild(g, r, n);
		}
	}

	class MyParagraphView extends javax.swing.text.ParagraphView {
		public int childCount;
		public int shift = 0;

		public MyParagraphView(Element e) {
			super(e);
			//			short top = 0;
			//			short left = 20;
			//			short bottom = 0;
			//			short right = 0;
			//			this.setInsets(top, left, bottom, right);

		}

		public void paint(Graphics g, Shape a) {
			childCount = this.getViewCount();
			super.paint(g, a);
			int rowCountInThisParagraph = this.getViewCount(); //<----- YOU HAVE REAL ROW COUNT FOR ONE PARAGRAPH}
		}

		public void paintChild(Graphics g, Rectangle r, int n) {
			super.paintChild(g, r, n);
			System.out.println(n + "," + r);
			//			g.drawString("x" + Integer.toString(shift + n + 1), r.x - 20, r.y + r.height - 3);
		}
	}
}