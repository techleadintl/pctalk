package swing.textPane.talk;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.EditorKit;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import com.xinwei.talk.common.LAF;


/**
 */
public class RubenTest2 extends JFrame {

	public static String htmlString = "<html>\n" + "<head>\n" + "</head>\n" + "\n" + "<body>\n" + "<h1>Just a test</h1>" + "<p><input type='checkbox' value='Monday'>Monday</input></p>"
			+ "<p><input type='checkbox' value='Tuesday' checked='checked'>Tuesday</input></p>" + "<p><input type='checkbox' value='Wednesday'>Wednesday</input></p>"
			+ "<p><input type='checkbox' value='Thursday'>Thursday</input></p>" + "<p><input type='checkbox' value='Friday' checked='checked'>Friday</input></p>"
			+ "<p><input type='checkbox' value='Saturday'>Saturday</input></p>" + "<p><input type='checkbox' value='Sunday'>Sunday</input></p>" + "</body>\n" + "</html>";

	JTextPane editor = new JTextPane();
	EditorKit kit = new HTMLEditorKit();

	public static void main(String[] args) {
		RubenTest2 app = new RubenTest2();
		app.setVisible(true);
	}

	public RubenTest2() {
		super("Ruben test");
		getContentPane().setLayout(new BorderLayout(5, 5));

		editor.setEditorKit(kit);
		editor.setContentType("text/html");
		//		editor.setText(htmlString);
		editor.setEditable(true);

		JScrollPane scroll = new JScrollPane(editor);
		getContentPane().add(scroll, BorderLayout.CENTER);

		editor.insertComponent(new JLabel(LAF.getTestImageIcon()));

		ActionListener actionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				Reader reader = new StringReader(editor.getText());
				HTMLDocument htmlDoc = new HTMLDocument();
				try {
					kit.read(reader, htmlDoc, 0);
				} catch (IOException | BadLocationException e) {
					e.printStackTrace();
				}
				System.out.println(editor.getText());
				for (int i = 0; i < htmlDoc.getLength(); i++) {
					String name = htmlDoc.getCharacterElement(i).getName();
					//					System.out.println(name);
				}
				HTMLDocument.Iterator it = htmlDoc.getIterator(HTML.Tag.HTML);
				while (it.isValid()) {
					AttributeSet s = it.getAttributes();
					if ("checkbox".equals(s.getAttribute(HTML.Attribute.TYPE))) {
						Object model = s.getAttribute(StyleConstants.ModelAttribute);
						String checkboxname = (String) s.getAttribute(HTML.Attribute.VALUE);
						String value = null;
						boolean selected = false;
						if ("checkbox".equals(s.getAttribute(HTML.Attribute.TYPE))) {
							ButtonModel m = (ButtonModel) model;
							selected = m.isSelected();
						}
						//Nu nog de opzoeken in de lijst met episodes en de juiste naam op selected/niet selected zetten.
						System.out.println("checkbox " + checkboxname + " selected: " + selected);
					} else {
						System.out.println(s.getAttribute(HTML.Attribute.TYPE));
					}
					it.next();
				}
			}
		};

		JButton b = new JButton("submit");
		getContentPane().add(b, BorderLayout.SOUTH);
		b.addActionListener(actionListener);

		setSize(200, 400);
		setLocationRelativeTo(null);
	}
}