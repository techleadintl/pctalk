//package swing.textPane.talk;
//
//import java.awt.Color;
//import java.awt.Component;
//import java.awt.Graphics;
//import java.awt.Graphics2D;
//import java.awt.Insets;
//import java.awt.Rectangle;
//import java.awt.RenderingHints;
//import java.awt.Shape;
//
//import javax.swing.BorderFactory;
//import javax.swing.ImageIcon;
//import javax.swing.JFrame;
//import javax.swing.JLabel;
//import javax.swing.JScrollPane;
//import javax.swing.JTextPane;
//import javax.swing.SwingUtilities;
//import javax.swing.UIManager;
//import javax.swing.border.Border;
//import javax.swing.text.AbstractDocument;
//import javax.swing.text.AttributeSet;
//import javax.swing.text.BadLocationException;
//import javax.swing.text.BoxView;
//import javax.swing.text.ComponentView;
//import javax.swing.text.DefaultStyledDocument;
//import javax.swing.text.Document;
//import javax.swing.text.Element;
//import javax.swing.text.IconView;
//import javax.swing.text.LabelView;
//import javax.swing.text.MutableAttributeSet;
//import javax.swing.text.ParagraphView;
//import javax.swing.text.SimpleAttributeSet;
//import javax.swing.text.Style;
//import javax.swing.text.StyleConstants;
//import javax.swing.text.StyleContext;
//import javax.swing.text.StyledEditorKit;
//import javax.swing.text.View;
//import javax.swing.text.ViewFactory;
//
//import com.xinwei.talk.common.Constants;
//import com.xinwei.talk.common.LAF;
//
//public class ExtendedParagraphExample3 {
//	// Style names
//	public static final String leftMsgStyleName = "LeftStyle";
//
//	public static final String rightMsgStyleName = "RightStyle";
//
//	public static final String centerStyleName = "CenterStyle";
//
//	public static final String mainStyleName = "MainStyle";
//
//	public static final Paragraph[] content = new Paragraph[] {
//			//			//
//			new Paragraph(centerStyleName, new Run[] { new TextRun("Attributes, Styles and Style Contexts") }),
//			//			//
//			new Paragraph(leftMsgStyleName,
//					new Run[] {
//							new TextRun(" class that you saw in the previous " + "chapter is only capable of holding text. " + "The more complex text components use a more "
//									+ "sophisticated model that implements the \n"),
//					new TextRun(" that contains methods for manipulating attributes " + "that control the way in which the text in the " + "document is displayed. The Swing text package "
//							+ "contains a concrete implementation of ") }),
//
//			new Paragraph(centerStyleName, new Run[] { new ComponentRun(new JLabel("Attributes, Styles and Style Contexts")), new TextRun(" TextRun ") }),
//			//
//			new Paragraph(rightMsgStyleName, new Run[] { new TextRun(" test2 that contains methods for manipulating attributes") }),
//
//	};
//
//	public static void main(String[] args) {
//		try {
//			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
//		} catch (Exception evt) {
//		}
//
//		JFrame f = new JFrame("Extended Paragraph Example");
//
//		// Create the StyleContext, the document and the pane
//		final StyleContext sc = new StyleContext();
//		final DefaultStyledDocument doc = new DefaultStyledDocument(sc);
//		//		final JTextPane pane = new McWillTextPane(doc) {
//		//			@Override
//		//			protected ParagraphView getParagraphView(Element elem) {
//		//				return new ExtendedParagraphView1(elem);
//		//			}
//		//		};
//		final JTextPane pane = new JTextPane(doc);
//		pane.setEditorKit(new TalkStyledEditorKit());
//		pane.setBorder(BorderFactory.createLineBorder(Color.RED));
//
//		try {
//			// Add the text and apply the styles
//			SwingUtilities.invokeAndWait(new Runnable() {
//				public void run() {
//					// Build the styles
//					createDocumentStyles(sc);
//
//					// Add the text
//					addText(pane, sc, sc.getStyle(mainStyleName), content);
//				}
//			});
//
//			pane.setEditable(false);
//		} catch (Exception e) {
//			System.out.println("Exception when constructing document: " + e);
//			System.exit(1);
//		}
//
//		f.getContentPane().add(new JScrollPane(pane));
//		f.setSize(600, 500);
//		f.setVisible(true);
//	}
//
//	public static void createDocumentStyles(StyleContext sc) {
//		Style defaultStyle = sc.getStyle(StyleContext.DEFAULT_STYLE);
//
//		// Create and add the main document style
//		Style mainStyle = sc.addStyle(mainStyleName, defaultStyle);
//		StyleConstants.setFontFamily(mainStyle, "serif");
//		StyleConstants.setFontSize(mainStyle, 12);
//
//		
////		Style leftStyle = sc.addStyle(leftMsgStyleName, null);
////		StyleConstants.setFontFamily(leftStyle, "serif");
////		StyleConstants.setFontSize(leftStyle, 18);
////		StyleConstants.setAlignment(leftStyle, StyleConstants.ALIGN_LEFT);
////		StyleConstants.setLeftIndent(leftStyle, 20);
////		TalkStyleConstants.setParagraphBackground(leftStyle, LAF.getBalloonLeftColor1());
//		
//		
//		Style leftStyle = sc.addStyle(leftMsgStyleName, null);
//		StyleConstants.setFontFamily(leftStyle, "serif");
//		StyleConstants.setFontSize(leftStyle, 18);
//		StyleConstants.setAlignment(leftStyle, StyleConstants.ALIGN_LEFT);
//		StyleConstants.setLeftIndent(leftStyle, 20);
//		TalkStyleConstants.setParagraphBackground(leftStyle, LAF.getBalloonLeftColor1());
//
//		Style rightStyle = sc.addStyle(rightMsgStyleName, null);
//		StyleConstants.setFontFamily(rightStyle, "serif");
//		StyleConstants.setFontSize(rightStyle, 18);
//		StyleConstants.setAlignment(rightStyle, StyleConstants.ALIGN_RIGHT);
//		//		StyleConstants.setSpaceAbove(rightStyle, 5);
//		TalkStyleConstants.setParagraphBackground(rightStyle, LAF.getBalloonRightColor1());
//		StyleConstants.setRightIndent(rightStyle, 20);
//
//		Style centerStyle = sc.addStyle(centerStyleName, null);
//		StyleConstants.setFontFamily(centerStyle, "serif");
//		StyleConstants.setFontSize(centerStyle, 18);
//		StyleConstants.setAlignment(centerStyle, StyleConstants.ALIGN_CENTER);
//		TalkStyleConstants.setParagraphBackground(centerStyle, LAF.getBalloonRightColor1());
//
//		//		MyStyleConstants.setParagraphBorder(rightStyle, BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2),
//		//				BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder( Color.gray.brighter(),  Color.gray.darker()), BorderFactory.createEmptyBorder(4, 4, 4, 4))));
//
//	}
//
//	public static void addText(JTextPane pane, StyleContext sc, Style logicalStyle, Paragraph[] paragraphs) {
//		// The outer loop adds paragraphs, while the
//		// inner loop adds character runs.
//		int paragraphSizes = paragraphs.length;
//		Document doc = pane.getDocument();
//		for (int i = 0; i < paragraphSizes; i++) {
//			Run[] runs = paragraphs[i].content;
//
////			try {
////				doc.insertString(doc.getLength(), "李拥 2017-05-12 21:29:08\n", SimpleAttributeSet.EMPTY);
////			} catch (BadLocationException e) {
////				e.printStackTrace();
////			}
//			
//			for (int j = 0; j < runs.length; j++) {
//				AttributeSet styles = runs[j].styleName == null ? SimpleAttributeSet.EMPTY : sc.getStyle(runs[j].styleName);
//
//				if (runs[j] instanceof TextRun) {
//					int caretPosition = pane.getCaretPosition();
//					TextRun run = (TextRun) runs[j];
//					try {
//						doc.insertString(doc.getLength(), run.content, styles);
//					} catch (BadLocationException e) {
//						e.printStackTrace();
//					}
//					int caretPosition2 = pane.getCaretPosition();
//					System.out.println(caretPosition + "---TextRun---" + caretPosition2);
//				} else if (runs[j] instanceof ComponentRun) {
//					ComponentRun run = (ComponentRun) runs[j];
//					int caretPosition = pane.getCaretPosition();
//					pane.insertComponent(run.component);
//					int caretPosition2 = pane.getCaretPosition();
//					System.out.println(caretPosition + "---ComponentRun---" + caretPosition2);
//				}
//				//				pane.setCaretPosition(doc.getLength());
//			}
//
//			// At the end of the paragraph, add the logical style and
//			// any overriding paragraph style and then terminate the
//			// paragraph with a newline.
//			pane.setParagraphAttributes(SimpleAttributeSet.EMPTY, true);
//
//			if (logicalStyle != null) {
//				pane.setLogicalStyle(logicalStyle);
//			}
//			//
//			if (paragraphs[i].styleName != null) {
//				pane.setParagraphAttributes(sc.getStyle(paragraphs[i].styleName), true);
//			}
//
//			if (i != paragraphSizes - 1) {
//				pane.replaceSelection("\n");
//			}
//		}
//	}
//
//	// Inner classes used to define paragraph structure
//	public static class Run {
//		public Run(String styleName) {
//			this.styleName = styleName;
//		}
//
//		public String styleName;
//	}
//
//	public static class TextRun extends Run {
//		public TextRun(String content) {
//			this(null, content);
//		}
//
//		public TextRun(String styleName, String content) {
//			super(styleName);
//			this.content = content;
//		}
//
//		public String content;
//	}
//
//	public static class ImageRun extends Run {
//		public ImageRun(ImageIcon imageIcon) {
//			this(null, imageIcon);
//		}
//
//		public ImageRun(String styleName, ImageIcon imageIcon) {
//			super(styleName);
//			this.imageIcon = imageIcon;
//		}
//
//		public ImageIcon imageIcon;
//	}
//
//	public static class ComponentRun extends Run {
//		public ComponentRun(Component component) {
//			this(null, component);
//		}
//
//		public ComponentRun(String styleName, Component component) {
//			super(styleName);
//			this.component = component;
//		}
//
//		public Component component;
//	}
//
//	public static class Paragraph {
//		public Paragraph(String styleName, Run[] content) {
//			this.styleName = styleName;
//			this.content = content;
//		}
//
//		public String styleName;
//
//		public Run[] content;
//	}
//
//}
//
//class TalkStyleConstants {
//	public TalkStyleConstants(String name) {
//		this.name = name;
//	}
//
//	public String toString() {
//		return name;
//	}
//
//	/**
//	 * The border to be used for a paragraph. Type is javax.swing.border.Border
//	 */
//	public static final Object ParagraphBorder = TalkParagraphConstants.ParagraphBorder;
//
//	/**
//	 * The background to be used for a paragraph. Type is java.awt.Color
//	 */
//	public static final Object ParagraphBackground = TalkParagraphConstants.ParagraphBackground;
//
//	public static final Object ParagraphBorderBackground = TalkParagraphConstants.ParagraphBorderBackground;
//
//	/* Adds the border attribute */
//	public static void setParagraphBorder(MutableAttributeSet a, Border b) {
//		a.addAttribute(ParagraphBorder, b);
//	}
//
//	/* Gets the border attribute */
//	public static Border getParagraphBorder(AttributeSet a) {
//		return (Border) a.getAttribute(ParagraphBorder);
//	}
//
//	/* Adds the paragraph background attribute */
//	public static void setParagraphBackground(MutableAttributeSet a, Color c) {
//		a.addAttribute(ParagraphBackground, c);
//	}
//
//	/* Gets the paragraph background attribute */
//	public static Color getParagraphBackground(AttributeSet a) {
//		return (Color) a.getAttribute(ParagraphBackground);
//	}
//
//	/* Adds the paragraph background attribute */
//	public static void setParagraphBorderBackground(MutableAttributeSet a, Color c) {
//		a.addAttribute(ParagraphBackground, c);
//	}
//
//	/* Gets the paragraph background attribute */
//	public static Color getParagraphBorderBackground(AttributeSet a) {
//		return (Color) a.getAttribute(ParagraphBackground);
//	}
//
//	/* A typesafe collection of extended paragraph attributes */
//	public static class TalkParagraphConstants extends TalkStyleConstants implements AttributeSet.ParagraphAttribute {
//		/**
//		 * The paragraph border attribute.
//		 */
//		public static final Object ParagraphBorder = new TalkStyleConstants("ParagraphBorder");
//
//		/**
//		 * The paragraph background attribute.
//		 */
//		public static final Object ParagraphBackground = new TalkStyleConstants("ParagraphBackground");
//
//		public static final Object ParagraphBorderBackground = new TalkStyleConstants("ParagraphBorderBackground");
//
//		private TalkParagraphConstants(String name) {
//			super(name);
//		}
//	}
//
//	protected String name; // Name of an attribute
//}
//
//class TalkStyledEditorKit extends StyledEditorKit {
//	public Object clone() {
//		return new TalkStyledEditorKit();
//	}
//
//	public ViewFactory getViewFactory() {
//		return defaultFactory;
//	}
//
//	/* The extended view factory */
//	static class ExtendedStyledViewFactory implements ViewFactory {
//		public View create(Element elem) {
//			String kind = elem.getName();
//			if (kind != null) {
//				if (kind.equals(AbstractDocument.ParagraphElementName)) {
//					return new TalkParagraphView(elem);
//				}
//				if (kind.equals(AbstractDocument.ContentElementName)) {
//					return new WrapLabelView(elem);
//				} else if (kind.equals(AbstractDocument.ParagraphElementName)) {
//					return getParagraphView(elem);
//				} else if (kind.equals(AbstractDocument.SectionElementName)) {
//					return new BoxView(elem, View.Y_AXIS);
//				} else if (kind.equals(StyleConstants.ComponentElementName)) {
//					return new ComponentView(elem);
//				} else if (kind.equals(StyleConstants.IconElementName)) {
//					return new IconView(elem);
//				}
//			}
//			// Delegate others to StyledEditorKit
//			return styledEditorKitFactory.create(elem);
//		}
//
//	}
//
//	protected static View getParagraphView(Element elem) {
//		return new ParagraphView(elem);
//	}
//
//	private static final ViewFactory styledEditorKitFactory = (new StyledEditorKit()).getViewFactory();
//
//	private static final ViewFactory defaultFactory = new ExtendedStyledViewFactory();
//}
//
//class TalkParagraphView extends ParagraphView {
//	private Color borderColor;
//	protected int alignment = StyleConstants.ALIGN_CENTER;
//	// Attribute cache
//	protected Color bgColor; // Background color, or null for transparent.
//
//	protected Border border; // Border, or null for no border
//
//	protected static final Border smallBorder = BorderFactory.createEmptyBorder(0, 0, 0, 0);
//
//	public TalkParagraphView(Element elem) {
//		super(elem);
//	}
//
//	protected void setPropertiesFromAttributes() {
//		AttributeSet attr = getAttributes();
//		if (attr != null) {
//			super.setPropertiesFromAttributes();
//			Insets paraInsets = new Insets(getTopInset(), getLeftInset(), getBottomInset(), getRightInset());
//
//			border = TalkStyleConstants.getParagraphBorder(attr);
//
//			bgColor = TalkStyleConstants.getParagraphBackground(attr);
//			if (bgColor != null && border == null) {
//				// Provide a small margin if the background
//				// is being filled and there is no border
//				border = smallBorder;
//			}
//			if (border != null) {
//				Insets borderInsets = border.getBorderInsets(getContainer());
//				setInsets((short) (paraInsets.top + borderInsets.top), (short) (paraInsets.left + borderInsets.left), (short) (paraInsets.bottom + borderInsets.bottom),
//						(short) (paraInsets.right + borderInsets.right));
//			}
//			System.out.println(getTopInset());
//		}
//	}
//
//	protected void layoutMinorAxis(int targetSpan, int axis, int[] offsets, int[] spans) {
//		int n = getViewCount();
//		for (int i = 0; i < n; i++) {
//			View v = getView(i);
//			int max = (int) v.getMaximumSpan(axis);
//			if (max < targetSpan) {
//				// can't make the child this wide, align it
//				float align = v.getAlignment(axis);
//				if (i == 0) {
//					offsets[i] = (int) ((targetSpan - max) * align);
//				} else {
//					offsets[i] = offsets[0];
//				}
//				spans[i] = max;
//			} else {
//				// make it the target width, or as small as it can get.
//				int min = (int) v.getMinimumSpan(axis);
//				offsets[i] = 0;
//				spans[i] = Math.max(min, targetSpan);
//			}
//		}
//	}
//
//	public void paint(Graphics g, Shape a) {
//		AttributeSet attr = getAttributes();
//
//		//		if (border != null) {
//		//			Insets borderInsets = border.getBorderInsets(getContainer());
//		//			setInsets((short) (getTopInset() + borderInsets.top), (short) (getLeftInset() + borderInsets.left), (short) (paraInsets.bottom + borderInsets.bottom),
//		//					(short) (getRightInset() + borderInsets.right));
//		//		}
//		Object alignmentObject = attr.getAttribute(StyleConstants.Alignment);
//		if (alignmentObject instanceof Number) {
//			alignment = ((Number) alignmentObject).intValue();
//		}
//
//		if (alignment == StyleConstants.ALIGN_LEFT) {
//			borderColor = LAF.getBalloonBorderLeftColor();
//			borderColor = Color.RED;
//		} else {
//			borderColor = LAF.getBalloonBorderRightColor();
//			borderColor = Color.GREEN;
//			borderColor = LAF.getBalloonBorderLeftColor();
//			borderColor = Color.RED;
//		}
//
//		//		Container comp = getContainer();
//		Rectangle alloc = new Rectangle(a.getBounds());
//
//		alloc.x += getLeftInset();
//		alloc.y += getTopInset();
//		alloc.width -= getLeftInset() + getRightInset();
//		alloc.height -= getTopInset() + getBottomInset();
//
//		int viewCount = getViewCount();
//
//		int offset = -1;
//		int span = -1;
//
//		for (int i = 0; i < viewCount; i++) {
//			if (offset == -1) {
//				offset = getOffset(X_AXIS, i);
//			} else {
//				offset = Math.min(offset, getOffset(X_AXIS, i));
//			}
//			if (span == -1) {
//				span = getSpan(X_AXIS, i);
//			} else {
//				span = Math.max(span, getSpan(X_AXIS, i));
//			}
//		}
//
//		alloc.x += offset;
//		if (alignment == StyleConstants.ALIGN_LEFT) {
//			alloc.width = alloc.x + span;
//		} else if (alignment == StyleConstants.ALIGN_RIGHT) {
//			alloc.width -= offset;
//		} else if (alignment == StyleConstants.ALIGN_CENTER) {
//			alloc.width = span;
//		}
//
//		//		alloc.x -= getLeftInset() / 2;
//		//		alloc.y -= getTopInset() / 2;
//		//		alloc.width += getLeftInset() / 2 + getRightInset() / 2;
//		//		alloc.height += getTopInset() / 2 + getBottomInset() / 2;
//
//		Graphics2D g2D = (Graphics2D) g;
//		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // 反锯齿平滑绘制  
//
//		// 绘制额消息气泡左边小箭头  
//		int xPoints[] = new int[3];
//		int yPoints[] = new int[3];
//
//		// 绘制圆角消息气泡边框  
//		g2D.setColor(borderColor);
//		g2D.drawRoundRect(alloc.x, alloc.y, alloc.width, alloc.height, 10, 10);
//		// 绘制圆角消息气泡矩形  
//		g2D.setColor(bgColor);
//		g2D.fillRoundRect(alloc.x + 1, alloc.y + 1, alloc.width - 1, alloc.height - 1, 10, 10);
//
//		int Y = 15;
//		int H = 8;
//
//		//泡泡
//		if (alignment == StyleConstants.ALIGN_LEFT) {
//			// 消息发出者是自己，则头像靠右显示  
//			xPoints[0] = alloc.x;
//			yPoints[0] = alloc.y + Y - H / 2;
//			xPoints[1] = alloc.x - getLeftInset() / 2;
//			yPoints[1] = alloc.y + Y - 2;
//			xPoints[2] = alloc.x;
//			yPoints[2] = alloc.y + Y + H / 2;
//
//			g2D.setColor(bgColor);
//			g2D.fillPolygon(xPoints, yPoints, 3);
//			g2D.setColor(borderColor);
//			g2D.drawPolyline(xPoints, yPoints, 3);
//			g2D.setColor(bgColor);
//			g2D.drawLine(xPoints[0], yPoints[0] + 1, xPoints[2], yPoints[2] - 1);
//		} else if (alignment == StyleConstants.ALIGN_RIGHT) {
//			xPoints[0] = alloc.x + alloc.width;
//			yPoints[0] = alloc.y + Y - H / 2;
//			xPoints[1] = alloc.x + alloc.width + getRightInset() / 2;
//			yPoints[1] = alloc.y + Y - 2;
//			xPoints[2] = alloc.x + alloc.width;
//			yPoints[2] = alloc.y + Y + H / 2;
//
//			g2D.setColor(bgColor);
//			g2D.fillPolygon(xPoints, yPoints, 3);
//
//			g2D.setColor(borderColor);
//			g2D.drawPolyline(xPoints, yPoints, 3);
//
//			g2D.setColor(bgColor);
//			g2D.drawLine(xPoints[0], yPoints[0] + 1, xPoints[2], yPoints[2] - 1);
//		}
//
//		super.paint(g, a); // Note: pass ORIGINAL allocation
//	}
//
//}
//
//class WrapLabelView extends LabelView {
//	public WrapLabelView(Element elem) {
//		super(elem);
//	}
//
//	public float getMinimumSpan(int axis) {
//		switch (axis) {
//		case View.X_AXIS:
//			return 0;
//		case View.Y_AXIS:
//			return super.getMinimumSpan(axis);
//		default:
//			throw new IllegalArgumentException("Invalid axis: " + axis);
//		}
//	}
//}
