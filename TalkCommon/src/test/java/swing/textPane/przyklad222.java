package swing.textPane;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.text.rtf.RTFEditorKit;

public class przyklad222 extends JFrame implements ActionListener, FocusListener, ItemListener, CaretListener {
	String newline = "\n", all, podziel;
	StyledDocument doc, doc2, doc3;
	JTextPane textPane, textPane2;
	JToggleButton g, g2;
	int start, end;
	RTFEditorKit kit;
	DefaultEditorKit kit2;
	MutableAttributeSet in, set;
	String style[] = { "regular", "bold", "bold_off", "italic", "italic_off", "icon", "bold_italic", "boldOFF_italicON", "boldON_italicOFF" };
	int ktoryStyl = 0, ktoryStylSpr = 0, startDziel = 0, START = 0;
	ArrayList zmianaStyli = new ArrayList();
	ArrayList podzielonyString = new ArrayList();
	boolean boldON, italicON;

	public przyklad222() {
		this.setLayout(null);
		kit = new RTFEditorKit();
		kit2 = new RTFEditorKit();

		textPane = new JTextPane() {
			public void setSize(Dimension d) {

				if (d.width < getParent().getSize().width)
					d.width = getParent().getSize().width;

				super.setSize(d);
			}

			public boolean getScrollableTracksViewportWidth() {
				return false;
			}
		};
		textPane.setEditorKit(kit);
		doc = textPane.getStyledDocument();
		addStylesToDocument(doc);
		textPane.addFocusListener(this);
		textPane.addCaretListener(this);

		JScrollPane paneScrollPane = new JScrollPane(textPane);
		paneScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		paneScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		paneScrollPane.setSize(300, 40);
		paneScrollPane.setLocation(10, 410);
		this.add(paneScrollPane);

		textPane2 = new JTextPane() {
			public void setSize(Dimension d) {

				if (d.width < getParent().getSize().width) {
					d.width = getParent().getSize().width;

				}
				super.setSize(d);

			}

			public boolean getScrollableTracksViewportWidth() {
				return false;
			}
		};
		doc2 = textPane2.getStyledDocument();
		textPane2.setEditable(false);

		addStylesToDocument(doc2);

		// textPane2.setEditorKit(kit2);

		JScrollPane paneScrollPane2 = new JScrollPane(textPane2);
		paneScrollPane2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		paneScrollPane2.setSize(300, 350);
		paneScrollPane2.setLocation(10, 10);
		this.add(paneScrollPane2);

		JButton b = new JButton("img");

		b.setSize(70, 20);
		b.setLocation(150, 370);
		b.addActionListener(this);
		this.add(b);

		JButton b2 = new JButton("send");

		b2.setSize(70, 20);
		b2.setLocation(320, 410);
		b2.addActionListener(this);
		this.add(b2);

		g = new JToggleButton("B");
		g.setSize(60, 30);
		g.setLocation(10, 370);
		g.setActionCommand("bold");
		g.addItemListener(this);
		this.add(g);

		g2 = new JToggleButton("I");
		g2.setSize(60, 30);
		g2.setLocation(80, 370);
		g2.setActionCommand("italic");
		g2.addItemListener(this);
		this.add(g2);

	}

	protected void addStylesToDocument(StyledDocument doc) {
		//Initialize some styles.
		Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);

		Style regular = doc.addStyle("regular", def);
		StyleConstants.setFontFamily(def, "SansSerif");

		Style s = doc.addStyle("italic", regular);
		StyleConstants.setItalic(s, true);

		s = doc.addStyle("italic_off", regular);
		StyleConstants.setItalic(s, false);

		s = doc.addStyle("bold", regular);
		StyleConstants.setBold(s, true);

		s = doc.addStyle("bold_off", regular);
		StyleConstants.setBold(s, false);

		s = doc.addStyle("bold_italic", regular);
		StyleConstants.setBold(s, true);
		StyleConstants.setItalic(s, true);

		s = doc.addStyle("boldOFF_italicON", regular);
		StyleConstants.setBold(s, false);
		StyleConstants.setItalic(s, true);

		s = doc.addStyle("boldON_italicOFF", regular);
		StyleConstants.setBold(s, true);
		StyleConstants.setItalic(s, false);

		s = doc.addStyle("icon", null);
		StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
		ImageIcon pigIcon = createImageIcon("cross.GIF", "a cute pig");
		if (pigIcon != null) {
			StyleConstants.setIcon(s, pigIcon);

		}

	}

	protected static ImageIcon createImageIcon(String path, String description) {
		java.net.URL imgURL = null;//TextSamplerDemo.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	public void set(AttributeSet s) {
		int st = textPane.getSelectionStart();
		int fn = textPane.getSelectionEnd();
		if (!textPane.hasFocus()) {
			st = start;
			fn = end;
		}
		if (st != fn) {
			doc.setCharacterAttributes(st, fn - st, s, false);

		} else {
			in = kit.getInputAttributes();
			in.addAttributes(s);

		}
		textPane.grabFocus();
	}

	public static void main(String[] args) {
		przyklad222 f = new przyklad222();
		f.setSize(500, 500);
		f.setLocation(10, 10);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("img")) {

			ktoryStyl = 5;
			ImageIcon pigIcon = createImageIcon("cross.GIF", "a cute pig");
			//doc.setLogicalStyle(doc.getLength(),in);
			textPane.insertIcon(pigIcon);
			/*
			 * try { doc.insertString(doc.getLength(), "  :wink:  ", in); } catch (BadLocationException e1) { // TODO Auto-generated catch block e1.printStackTrace(); }
			 */

			textPane.grabFocus();
			start = textPane.getSelectionStart();
			end = textPane.getSelectionEnd();
		}

		if (e.getActionCommand().equals("send")) {
			podziel = all.substring(startDziel, doc.getLength());
			podzielonyString.add(podziel);

			textPane.grabFocus();

			String initString[] = new String[podzielonyString.size()];
			String initStyles[] = new String[zmianaStyli.size()];
			for (int i = 0; i < podzielonyString.size(); i++) {

				initString[i] = (String) podzielonyString.get(i);
				if (i == podzielonyString.size() - 1)
					initString[i] += "\n";

				initStyles[i] = (String) zmianaStyli.get(i);
				System.out.println("STYL=" + initStyles[i] + "  String=" + initString[i]);

			}
			System.out.println(initString.length + "==" + initStyles.length);
			zmianaStyli.clear();
			podzielonyString.clear();
			startDziel = 0;
			START = 0;

			try {
				doc.remove(0, doc.getLength());

				for (int i = 0; i < initString.length; i++) {

					doc2.insertString(doc2.getLength(), initString[i], doc2.getStyle(initStyles[i]));

				}
			} catch (BadLocationException ble) {
				System.err.println("Couldn't insert initial text into text pane.");
			}

		}
	}

	public void itemStateChanged(ItemEvent e) {
		g = (JToggleButton) e.getSource();
		g2 = (JToggleButton) e.getSource();
		String a = g.getActionCommand();
		String a2 = g2.getActionCommand();

		if (a.equals("bold")) {
			if (e.getStateChange() == 1) {
				System.out.println("bold_jest_on");

				boldON = true;

				ktoryStyl = 1;
				set = doc.getStyle("bold");
				set(set);
				if (italicON == true)
					ktoryStyl = 6;

			} else if (e.getStateChange() == 2) {
				System.out.println("bold_jest_off");
				ktoryStyl = 2;
				set = doc.getStyle("bold_off");
				set(set);
				boldON = false;
				if (italicON == true)
					ktoryStyl = 7;
			}
		}

		if (a2.equals("italic")) {
			if (e.getStateChange() == 1) {
				System.out.println("italc_jest_on");

				italicON = true;

				set = doc.getStyle("italic");
				set(set);
				ktoryStyl = 3;
				if (boldON == true)
					ktoryStyl = 6;

			} else if (e.getStateChange() == 2) {
				System.out.println("ilatci_jest_off");

				set = doc.getStyle("italic_off");
				set(set);
				ktoryStyl = 4;
				italicON = false;
				if (boldON == true)
					ktoryStyl = 8;
			}

		}
	}

	public void focusGained(FocusEvent arg0) {
		if (start >= 0 && end >= 0) {
			if (textPane.getCaretPosition() == start) {
				textPane.setCaretPosition(end);
				textPane.moveCaretPosition(start);
			} else {
				textPane.select(start, end);
			}
		}

	}

	public void focusLost(FocusEvent arg0) {
		start = textPane.getSelectionStart();
		end = textPane.getSelectionEnd();

	}

	public void caretUpdate(CaretEvent e) {

		try {
			all = doc.getText(0, doc.getLength());

		} catch (BadLocationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (START == 0) {
			ktoryStylSpr = ktoryStyl;
			zmianaStyli.add(style[ktoryStyl]);
			START++;
		} else {

			if (ktoryStylSpr == ktoryStyl) {
				//System.out.println("row");
			} else {
				//System.out.println();
				if (ktoryStyl == 5) {
					zmianaStyli.add(style[ktoryStyl]);

					podziel = "  :)  ";
					startDziel = e.getDot();
					podzielonyString.add(podziel);
				}
				ktoryStylSpr = ktoryStyl;
				zmianaStyli.add(style[ktoryStyl]);
				podziel = all.substring(startDziel, e.getDot());

				startDziel = e.getDot();
				podzielonyString.add(podziel);

			}
		}

	}

}