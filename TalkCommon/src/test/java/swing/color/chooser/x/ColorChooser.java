package swing.color.chooser.x;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ColorChooser extends JFrame {
	private JLabel sampleText = new JLabel("Label");
	private JButton chooseButton = new JButton("Choose Color");

	public static void main(String[] args) {
		new ColorChooser();
	}

	public ColorChooser() {
		this.setSize(300, 100);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel1 = new JPanel();
		sampleText.setBackground(null);
		panel1.add(sampleText);

		chooseButton.addActionListener(new ButtonListener());
		panel1.add(chooseButton);

		this.add(panel1);
		this.setVisible(true);
	}

	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			ColorPicker picker = new ColorPicker(false, false);
			picker.setPreferredSize(new Dimension(200, 160));
			picker.setMode(ColorPicker.BRI);
			picker.showDialog(ColorChooser.this, Color.RED);
//			hue.radioButton.setSelected(mode == HUE);
//			sat.radioButton.setSelected(mode == SAT);
//			bri.radioButton.setSelected(mode == BRI);
//			red.radioButton.setSelected(mode == RED);
//			green.radioButton.setSelected(mode == GREEN);
//			blue.radioButton.setSelected(mode == BLUE);
		}
	}
}
