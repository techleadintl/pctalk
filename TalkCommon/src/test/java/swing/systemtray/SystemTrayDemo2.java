package swing.systemtray;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.MenuItem;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.Rectangle;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

public class SystemTrayDemo2 {
	public static void main(String[] args) {
		if (!SystemTray.isSupported()) {
			return;
		}
		SystemTray tray = SystemTray.getSystemTray();

		Dimension size = tray.getTrayIconSize();
		BufferedImage bi = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
		Graphics g = bi.getGraphics();

		g.setColor(Color.blue);
		g.fillRect(0, 0, size.width, size.height);
		g.setColor(Color.yellow);
		int ovalSize = (size.width < size.height) ? size.width : size.height;
		ovalSize /= 2;
		g.fillOval(size.width / 4, size.height / 4, ovalSize, ovalSize);

		try {
			PopupMenu popup = new PopupMenu();
			MenuItem miExit = new MenuItem("Exit");
			ActionListener al;
			al = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Goodbye");
					System.exit(0);
				}
			};
			miExit.addActionListener(al);
			popup.add(miExit);

			TrayIcon ti = new TrayIcon(bi, "System Tray Demo #2", popup);

			al = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println(e.getActionCommand());
				}
			};

			ti.setActionCommand("My Icon");
			ti.addActionListener(al);

			MouseListener ml;
			ml = new MouseListener() {
				public void mouseClicked(MouseEvent e) {
					System.out.println("Tray icon: Mouse clicked");
				}

				public void mouseEntered(MouseEvent e) {
					System.out.println("Tray icon: Mouse entered");
				}

				public void mouseExited(MouseEvent e) {
					System.out.println("Tray icon: Mouse exited");
				}

				public void mousePressed(MouseEvent e) {
					System.out.println("Tray icon: Mouse pressed");
				}

				public void mouseReleased(MouseEvent e) {
					System.out.println("Tray icon: Mouse released");
				}
			};
			ti.addMouseListener(ml);

			MouseMotionListener mml;

			mml = new TrayMouseMotionListener();
			ti.addMouseMotionListener(mml);

			tray.add(ti);
		} catch (AWTException e) {
			System.out.println(e.getMessage());
			return;
		}

	}

	static class TrayMouseMotionListener extends MouseMotionAdapter {
		int x1, x2;
		int y1, y2;
		boolean isIn = false;
		boolean stop;

		public TrayMouseMotionListener() {
			Dimension trayIconSize = SystemTray.getSystemTray().getTrayIconSize();
			final int w = trayIconSize.width;
			final int h = trayIconSize.height;
			final Rectangle rec = new Rectangle();
			new Thread() {
				public void run() {
					while (!stop) {
						if (isIn) {
							rec.x = (x2 + x1) / 2 - w;
							rec.y = (y2 + y1) / 2 - h;
							rec.width = 2 * w;
							rec.height = 2 * h;
							Point location = MouseInfo.getPointerInfo().getLocation();
							if (!rec.contains(location)) {
								isIn = false;
							}
						}
						System.out.println(isIn);
						try {
							sleep(250);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				};
			}.start();
		}

		public void mouseMoved(MouseEvent e) {
			int x = e.getX();
			int y = e.getY();
			if (isIn) {
				if (x < x1) {
					x1 = x;
				}
				if (x > x2) {
					x2 = x;
				}
				if (y < y1) {
					y1 = y;
				}
				if (y > y2) {
					y2 = y;
				}
			} else {
				isIn = true;
				x1 = x2 = x;
				y1 = y2 = y;
			}
		}

		public boolean isStop() {
			return stop;
		}

		public void setStop(boolean stop) {
			this.stop = stop;
		}
	}

}