/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月15日 上午9:11:29
 * 
 ***************************************************************/
package swing.file;

import javax.swing.JFileChooser;
import javax.swing.UIManager;

public class XX {
	public static void main(String[] args) {

		//		UIManager.put("ScrollBarUI", "com.sun.java.swing.plaf.windows.WindowsScrollBarUI");// 设置滚动条样式为window风格的滚动条样式
		//
		//		// 设置文件夹在swing中所显示的图标
		//		UIManager.put("FileView.directoryIcon", FileSystemView.getFileSystemView().getSystemIcon(new File(System.getProperty("user.dir"))));
		//
		//		// 如果觉得默认的图标太丑可以自己设置文件选择对话框的一系列图标
		//		//	UIManager.put("FileChooser.newFolderIcon", newFolderIcon);
		//		//	UIManager.put("FileChooser.upFolderIcon", upFolderIcon);
		//		//	UIManager.put("FileChooser.homeFolderIcon", homeFolderIcon);
		//		//	UIManager.put("FileChooser.detailsViewIcon", detailsViewIcon);
		//		//	UIManager.put("FileChooser.listViewIcon", listViewIcon);
		//
		//		// 设置工具提示的默认样式
		//		Color toolTipColor = new Color(80, 80, 80);
		//		UIManager.put("ToolTip.border", BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(toolTipColor), BorderFactory.createEmptyBorder(2, 3, 2, 3)));
		//		UIManager.put("ToolTip.background", Color.WHITE);
		//		UIManager.put("ToolTip.foreground", toolTipColor);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println("Unable to load Windows look and feel");
		}
//		MockJFileChooser chooser = new MockJFileChooser();
		JFileChooser chooser = new JFileChooser();
		chooser.showOpenDialog(null);
	}
}
