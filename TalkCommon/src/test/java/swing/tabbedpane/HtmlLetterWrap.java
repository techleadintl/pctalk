package swing.tabbedpane;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.InlineView;
import javax.swing.text.html.ParagraphView;

public class HtmlLetterWrap {
	private static String webContent = "<html>" + "<head>" + "<style type=\"text/css\">mytag {color:red}</style>" + "</head>" + "<body>"
			+ "<p style=\"text-align: right\">Hello World JEditorPane jep = new JEditorPane()f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)</p>" + "</body>" + "</html>";

	static int caretPosition1;

	public HtmlLetterWrap() throws BadLocationException {
		final JFrame frame = new JFrame("Letter wrap test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final JTextPane jep = new JTextPane() {
			@Override
			public void paint(Graphics g) {
				Rectangle modelToView1;
				try {
					modelToView1 = modelToView(caretPosition1);
					System.out.println("--" + modelToView1);
				} catch (BadLocationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				super.paint(g);
			}
		};

		jep.setEditorKit(new HTMLEditorKit());
		final Document doc = jep.getDocument();
		jep.setCaretPosition(0);
		int caretPosition = jep.getCaretPosition();
		Rectangle modelToView = jep.modelToView(caretPosition);
		jep.setMinimumSize(new Dimension(0, 0));

		jep.setText(webContent);
		jep.setCaretPosition(doc.getLength());
		caretPosition1 = jep.getCaretPosition();

		JTextPane noHtmlTextPane = new JTextPane();

		noHtmlTextPane.setText(webContent);

		noHtmlTextPane.setMinimumSize(new Dimension(0, 0));

		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, noHtmlTextPane, jep);
		splitPane.setContinuousLayout(true);

		frame.add(splitPane);

		frame.setSize(200, 200);
		frame.setVisible(true);
		splitPane.setDividerLocation(.5);
	}


	public static void main(String[] args) throws BadLocationException {
		new HtmlLetterWrap();
	}
}