package swing.tabbedpane;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.jtattoo.plaf.texture.TextureLookAndFeel;

public class TabShapeTest extends JFrame {

	/** 
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {

		new TabShapeTest().setVisible(true);
	}

	public TabShapeTest() {
		try {
			UIManager.setLookAndFeel(new TextureLookAndFeel ());
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JTabbedPane tpShape = new JTabbedPane();
		tpShape.setUI(new XTabbedPaneUI());
		add(tpShape);
		setPreferredSize(new Dimension(280, 300));
		tpShape.addTab("first", new JPanel());
		tpShape.addTab("second?????", new JPanel());
		tpShape.addTab("third", new JPanel());
		//		tpShape.setBackgroundAt(1, Color.yellow);
		//		tpShape.setBackgroundAt(2, Color.BLUE);

		pack();
	}

}