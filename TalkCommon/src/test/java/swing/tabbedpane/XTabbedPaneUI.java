/*
 * Copyright 2012 MH-Software-Entwicklung. All rights reserved.
 * Use is subject to license terms.
 */

package swing.tabbedpane;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.lang.reflect.Method;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import javax.swing.text.View;

import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.BaseTabbedPaneUI;
import com.jtattoo.plaf.ColorHelper;
import com.jtattoo.plaf.JTattooUtilities;
import com.jtattoo.plaf.texture.TextureUtils;
import com.xinwei.common.lookandfeel.util.ColorUtil;

/**
 * @author Michael Hagen
 */
public class XTabbedPaneUI extends BaseTabbedPaneUI {

	public static ComponentUI createUI(JComponent c) {
		return new XTabbedPaneUI();
	}

	protected void paintRoundedTopTabBorder(int tabIndex, Graphics g, int x1, int y1, int x2, int y2, boolean isSelected) {
	}

	protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h) {

	}

	protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
	}

	protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
		if (isSelected || tabIndex == rolloverIndex) {
			Graphics2D g2D = (Graphics2D) g;
			Object savedRederingHint = g2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
			g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			Color borderColor = null;
			if (isSelected) {
				borderColor = getSelectedBorderColor(tabIndex);
			} else {
				borderColor = Color.BLUE;
			}
			g.setColor(borderColor);
			g.fillRoundRect(x, y, w, h, 8, 8);
			g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, savedRederingHint);

		}
	}

	protected void paintText(Graphics g, int tabPlacement, Font font, FontMetrics metrics, int tabIndex, String title, Rectangle textRect, boolean isSelected) {
		int mnemIndex = tabPane.getDisplayedMnemonicIndexAt(tabIndex);

		if (isSelected) {
			Color titleColor = AbstractLookAndFeel.getWindowTitleForegroundColor();
			if (ColorUtil.getGrayValue(titleColor) > 164)
				g.setColor(Color.black);
			else
				g.setColor(Color.white);
			g.setColor(titleColor);
		} else {
			g.setColor(tabPane.getForegroundAt(tabIndex));
		}

		drawStringUnderlineCharAt(tabPane, g, title, mnemIndex, textRect.x, textRect.y + metrics.getAscent());
	}

	public void drawStringUnderlineCharAt(JComponent c, Graphics g, String text, int underlinedIndex, int x, int y) {
		Graphics2D g2D = (Graphics2D) g;
		try {
			Class swingUtilities2Class = sun.swing.SwingUtilities2.class;
			Class classParams[] = { JComponent.class, Graphics.class, String.class, Integer.TYPE, Integer.TYPE, Integer.TYPE };
			Method m = swingUtilities2Class.getMethod("drawStringUnderlineCharAt", classParams);
			Object methodParams[] = { c, g, text, new Integer(underlinedIndex), new Integer(x), new Integer(y) };
			m.invoke(null, methodParams);
		} catch (Exception ex) {
			BasicGraphicsUtils.drawString(g, text, underlinedIndex, x, y);
		}
	}
}