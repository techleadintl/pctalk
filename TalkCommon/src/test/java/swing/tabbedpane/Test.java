package swing.tabbedpane;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Shape;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.BoxView;
import javax.swing.text.ComponentView;
import javax.swing.text.Element;
import javax.swing.text.IconView;
import javax.swing.text.LabelView;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import javax.swing.text.rtf.RTFEditorKit;

class Test extends JFrame {
	public Test() throws BadLocationException {
		super("Test");
		JTextPane edit = new JTextPane();
		edit.setEditorKit(new MyRTFEditorKit());

		SimpleAttributeSet bSet = new SimpleAttributeSet();
		StyleConstants.setAlignment(bSet, StyleConstants.ALIGN_CENTER);
		StyleConstants.setUnderline(bSet, true);
		StyleConstants.setFontFamily(bSet, "lucida typewriter bold");
		StyleConstants.setFontSize(bSet, 12);

		StyledDocument doc = edit.getStyledDocument();
		doc.setParagraphAttributes(0, doc.getLength(), bSet, false);
		//		doc.setParagraphAttributes(0, 104, bSet, false);
		edit.setText("我用JtextPane实现了一个类似qq聊天的界面，在JTextpane里面");

		edit.setEditable(true);
		JScrollPane scroll = new JScrollPane(edit);
		getContentPane().add(scroll);
		setSize(300, 300);
		setVisible(true);
	}

	public static void main(String a[]) throws BadLocationException {
		new Test();
	}

}

class MyRTFEditorKit extends RTFEditorKit {
	public ViewFactory getViewFactory() {
		return new MyRTFViewFactory();
	}
}

class MyRTFViewFactory implements ViewFactory {
	public View create(Element elem) {
		String kind = elem.getName();
		if (kind != null)
			if (kind.equals(AbstractDocument.ContentElementName)) {
				return new WrapLabelView(elem);
			} else if (kind.equals(AbstractDocument.ParagraphElementName)) {
				// return new ParagraphView(elem);
				return new MyParagraphView(elem);
			} else if (kind.equals(AbstractDocument.SectionElementName)) {
				// return new BoxView(elem, View.Y_AXIS);
				return new MySectionView(elem, View.Y_AXIS);
			} else if (kind.equals(StyleConstants.ComponentElementName)) {
				return new ComponentView(elem);
			} else if (kind.equals(StyleConstants.IconElementName)) {
				return new IconView(elem);
			}
		// default to text display
		return new LabelView(elem);
	}
}

class WrapLabelView extends LabelView {
	public WrapLabelView(Element elem) {
		super(elem);
	}

	public float getMinimumSpan(int axis) {
		switch (axis) {
		case View.X_AXIS:
			return 0;
		case View.Y_AXIS:
			return super.getMinimumSpan(axis);
		default:
			throw new IllegalArgumentException("Invalid axis: " + axis);
		}
	}

}

class MySectionView extends BoxView {
	public MySectionView(Element e, int axis) {
		super(e, axis);
	}

	public void paintChild(Graphics g, Rectangle r, int n) {
		if (n > 0) {
			MyParagraphView child = (MyParagraphView) this.getView(n - 1);
			int shift = child.shift + child.childCount;

			MyParagraphView current = (MyParagraphView) this.getView(n);
			current.shift = shift;
			System.out.println("box:" + shift + "," + child.childCount);
		}
		super.paintChild(g, r, n);
	}
}

class MyParagraphView extends javax.swing.text.ParagraphView {
	public int childCount;
	public int shift = 0;

	public MyParagraphView(Element e) {
		super(e);
		short top = 0;
		short left = 20;
		short bottom = 0;
		short right = 0;
		this.setInsets(top, left, bottom, right);

	}

	public void paint(Graphics g, Shape a) {
		childCount = this.getViewCount();
		super.paint(g, a);
		int rowCountInThisParagraph = this.getViewCount(); //<----- YOU HAVE REAL ROW COUNT FOR ONE PARAGRAPH}
	}

	public void paintChild(Graphics g, Rectangle r, int n) {
		super.paintChild(g, r, n);
		System.out.println(n + "," + r);
		g.drawString("x" + Integer.toString(shift + n + 1), r.x - 20, r.y + r.height - 3);
	}
}