package swing.tabbedpane;
import java.awt.Dimension; 
import javax.swing.*;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

@SuppressWarnings("serial")
public class jBulletins extends JPanel {

    javax.swing.JEditorPane bulletinBoard = new javax.swing.JEditorPane();

    public jBulletins() {

        Dimension d = new Dimension();
        d.setSize(800, 600);

        bulletinBoard.setPreferredSize(d);
        bulletinBoard.setContentType("text/html"); // NOI18N

        add(new JScrollPane(bulletinBoard));

        loadBulletins();
    }

    private void loadBulletins() {

        //<editor-fold defaultstate="collapsed" desc="HTML/CSS Script">
        String eBullStyles = "#divMsgs{ "
                + "height: 99px; "
                + "margin: auto; "
                + "background-color: #3399FF; "
                + "} "
                + "#divLeft{ "
                + "float: left; "
                + "width: 110px; "
                + "height: 85px; "
                + "border: 2px #6666ff outset; "
                + "padding: 5px; "
                + "background-color: #189cd8; "
                + "color: #ffffff; "
                + "font-size: 11px; "
                + "font-weight: normal; "
                + "font-family: Tahoma, Geneva, sans-serif; "
                + "font-style: normal; "
                + "text-decoration: inherit; "
                + "} "
                + "#divRight{ "
                + "float: right; "
                + "width: 50px; "
                + "height: 85px; "
                + "border: 2px #6666ff outset; "
                + "background-color: #189cd8; "
                + "padding: 5px; "
                + "color: #ffffff; "
                + "font-size: 11px; "
                + "font-weight: normal; "
                + "font-family: Tahoma, Geneva, sans-serif; "
                + "font-style: normal; "
                //+ "text-decoration: inherit; "
                + "line-height: 1.3em; "
                + " margin-right:0;"
                + "}"; //</editor-fold>

        bulletinBoard.setContentType("text/html");
        bulletinBoard.setText("");

        HTMLEditorKit kit = new HTMLEditorKit();
        bulletinBoard.setEditorKit(kit);

        // add some styles to the html
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(eBullStyles);

        Document setdoc = kit.createDefaultDocument();
        bulletinBoard.setDocument(setdoc);

        String initial = "<div id=\"divMsgs\">"
                + "<div id=\"divLeft\">"
                + "Hello Bob"
                + "</div>"
                + "<div id=\"divRight\">"
                + "How are you today?"
                + "</div>"
                + "</div>";

        try {
            Document doc = bulletinBoard.getDocument();
            bulletinBoard.setText(initial);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void createAndShowGui() {
        jBulletins mainPanel = new jBulletins();
        JFrame frame = new JFrame("Bulletin Board");
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(mainPanel);
        frame.pack();
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGui();
            }
        });
    }
}