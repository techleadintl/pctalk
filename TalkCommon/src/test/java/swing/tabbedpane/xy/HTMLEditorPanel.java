package swing.tabbedpane.xy;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.HTMLEditorKit.HTMLTextAction;

public class HTMLEditorPanel extends JPanel {
	private static final long serialVersionUID = 6083828119565418011L;
	private static Color color_values[] = { new Color(0, 0, 0), new Color(255, 0, 0), new Color(255, 153, 0), new Color(255, 255, 0), new Color(0, 255, 0), new Color(0, 255, 255),
			new Color(0, 0, 255), new Color(153, 0, 255), new Color(255, 0, 255) };
	private static String size_values[] = { "10", "12", "14", "16", "24" };
	private JEditorPane edit_pan = new JEditorPane();
	private JToolBar tool_bar = new JToolBar();

	//元件基本屬性的說明設定中會用到的編輯區[START]
	public HTMLEditorPanel() {
		init();
		init_toolbar_components();
	}
	//元件基本屬性的說明設定中會用到的編輯區[END]

	private void init() {
		//設定JToolBar[START]
		tool_bar.setFloatable(false);
		tool_bar.setLayout(new FlowLayout(FlowLayout.LEADING, 1, 1));
		tool_bar.setBackground(Color.WHITE);
		//設定JToolBar[END]

		//設定JEditorPane[START]
		HTMLEditorKit html_kit = new HTMLEditorKit();
		HTMLDocument html_doc = (HTMLDocument) html_kit.createDefaultDocument();
		html_doc.putProperty("IgnoreCharsetDirective", Boolean.TRUE);
		html_doc.setPreservesUnknownTags(false);

		edit_pan.requestFocus();
		edit_pan.setContentType("text/html");
		edit_pan.setEditorKit(html_kit);
		edit_pan.setDocument(html_doc);
		JScrollPane edit_scr = new JScrollPane(edit_pan);
		edit_scr.setBorder(BorderFactory.createLoweredBevelBorder());
		//設定JEditorPane[END]

		this.setLayout(new BorderLayout());
		this.add(tool_bar, BorderLayout.NORTH);
		this.add(edit_scr, BorderLayout.CENTER);
	}

	private void init_toolbar_components() {
		//建立所有的Action物件[START]
		Icon open_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_open.png"));
		Icon save_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_save.png"));
		Icon cut_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_cut.png"));
		Icon copy_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_copy.png"));
		Icon paste_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_paste.png"));
		Icon bold_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_bold.png"));
		Icon italic_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_italic.png"));
		Icon underline_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_underline.png"));
		Icon align_right_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_align-right.png"));
		Icon align_conter_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_align-center.png"));
		Icon align_left_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_align-left.png"));
		Icon ordered_list_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_ordered_list.png"));
		Icon unordered_list_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_unordered_list.png"));

		Action open_action = new AbstractAction("Open", open_icon) {
			private static final long serialVersionUID = -5784845935880954593L;

			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				if (chooser.showOpenDialog(HTMLEditorPanel.this) != JFileChooser.APPROVE_OPTION) {
					return;
				}
				File file = chooser.getSelectedFile();
				if (file == null) {
					return;
				}

				FileReader reader = null;
				try {
					reader = new FileReader(file);
					edit_pan.read(reader, null);
				} catch (IOException ex) {
					JOptionPane.showMessageDialog(HTMLEditorPanel.this, "File Not Found", "ERROR", JOptionPane.ERROR_MESSAGE);
				} finally {
					if (reader != null) {
						try {
							reader.close();
						} catch (IOException x) {

						}
					}
				}
			}
		};

		Action save_action = new AbstractAction("Save", save_icon) {
			private static final long serialVersionUID = -4339517584506941557L;

			public void actionPerformed(ActionEvent ev) {
				JFileChooser chooser = new JFileChooser();
				if (chooser.showSaveDialog(HTMLEditorPanel.this) != JFileChooser.APPROVE_OPTION) {
					return;
				}
				File file = chooser.getSelectedFile();
				if (file == null) {
					return;
				}

				FileWriter writer = null;
				try {
					writer = new FileWriter(file);
					edit_pan.write(writer);
				} catch (IOException ex) {
					JOptionPane.showMessageDialog(HTMLEditorPanel.this, "File Not Saved", "ERROR", JOptionPane.ERROR_MESSAGE);
				} finally {
					if (writer != null) {
						try {
							writer.close();
						} catch (IOException x) {

						}
					}
				}
			}
		};
		Action cur_action = edit_pan.getActionMap().get(DefaultEditorKit.cutAction);
		cur_action.putValue(Action.SMALL_ICON, cut_icon);
		cur_action.putValue(Action.SHORT_DESCRIPTION, "Cut");

		Action copy_action = edit_pan.getActionMap().get(DefaultEditorKit.copyAction);
		copy_action.putValue(Action.SMALL_ICON, copy_icon);
		copy_action.putValue(Action.SHORT_DESCRIPTION, "Copy");

		Action paste_action = edit_pan.getActionMap().get(DefaultEditorKit.pasteAction);
		paste_action.putValue(Action.SMALL_ICON, paste_icon);
		paste_action.putValue(Action.SHORT_DESCRIPTION, "Paste");

		Action bold_action = edit_pan.getActionMap().get("font-bold");
		bold_action.putValue(Action.SMALL_ICON, bold_icon);
		bold_action.putValue(Action.SHORT_DESCRIPTION, "Bold");

		Action italic_action = edit_pan.getActionMap().get("font-italic");
		italic_action.putValue(Action.SMALL_ICON, italic_icon);
		italic_action.putValue(Action.SHORT_DESCRIPTION, "Italic");

		Action underline_action = edit_pan.getActionMap().get("font-underline");
		underline_action.putValue(Action.SMALL_ICON, underline_icon);
		underline_action.putValue(Action.SHORT_DESCRIPTION, "Underline");

		Action left_action = new StyledEditorKit.AlignmentAction("Left Align", StyleConstants.ALIGN_LEFT);
		left_action.putValue(Action.SMALL_ICON, align_left_icon);
		left_action.putValue(Action.SHORT_DESCRIPTION, "Align-Left");

		Action center_action = new StyledEditorKit.AlignmentAction("Center Align", StyleConstants.ALIGN_CENTER);
		center_action.putValue(Action.SMALL_ICON, align_conter_icon);
		center_action.putValue(Action.SHORT_DESCRIPTION, "Align-Center");

		Action right_action = new StyledEditorKit.AlignmentAction("Right Align", StyleConstants.ALIGN_RIGHT);
		right_action.putValue(Action.SMALL_ICON, align_right_icon);
		right_action.putValue(Action.SHORT_DESCRIPTION, "Align-Right");

		Action order_list_action = new InsertHTMLTextAction("InsertOrderedList", "<ol><li></li></ol>", HTML.Tag.BODY, HTML.Tag.OL);
		order_list_action.putValue(Action.SMALL_ICON, ordered_list_icon);
		order_list_action.putValue(Action.SHORT_DESCRIPTION, "Ordered List");

		Action unorder_list_action = new InsertHTMLTextAction("Unordered List", "<ul><li></li></ul>", HTML.Tag.BODY, HTML.Tag.UL);
		unorder_list_action.putValue(Action.SMALL_ICON, unordered_list_icon);
		unorder_list_action.putValue(Action.SHORT_DESCRIPTION, "Unordered List");
		//建立所有的Action物件[END]

		final SmallButton open_btn = new SmallButton(open_action, "Open_File");
		final SmallButton save_btn = new SmallButton(save_action, "Save");
		final SmallButton cut_btn = new SmallButton(cur_action, "Cut");
		final SmallButton copy_btn = new SmallButton(copy_action, "Copy");
		final SmallButton paste_btn = new SmallButton(paste_action, "Paste");
		final SmallButton bold_btn = new SmallButton(bold_action, "Bold");
		final SmallButton italic_btn = new SmallButton(italic_action, "Italic");
		final SmallButton underline_btn = new SmallButton(underline_action, "Underline");
		final SmallButton left_btn = new SmallButton(left_action, "Left");
		final SmallButton center_btn = new SmallButton(center_action, "Center");
		final SmallButton right_btn = new SmallButton(right_action, "Right");
		final SmallButton olist_btn = new SmallButton(order_list_action, "OrderedList");
		final SmallButton ulist_btn = new SmallButton(unorder_list_action, "UnorderedList");
		final SmallComboBox size_cbox = new SmallComboBox(new DefaultComboBoxModel(size_values));
		final SmallColorComboBox color_cbox = new SmallColorComboBox(color_values);

		size_cbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				edit_pan.getActionMap().get("font-size-" + size_cbox.getSelectedItem()).actionPerformed(evt);
			}
		});
		color_cbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (color_cbox.getSelectedIndex() > -1) {
					new StyledEditorKit.ForegroundAction(color_values[color_cbox.getSelectedIndex()].toString(), color_values[color_cbox.getSelectedIndex()]).actionPerformed(evt);
				}
			}
		});

		open_btn.resetBorder();
		save_btn.resetBorder();
		cut_btn.resetBorder();
		copy_btn.resetBorder();
		paste_btn.resetBorder();
		bold_btn.resetBorder();
		italic_btn.resetBorder();
		underline_btn.resetBorder();
		center_btn.resetBorder();
		olist_btn.resetBorder();
		ulist_btn.resetBorder();

		tool_bar.add(open_btn);
		tool_bar.add(save_btn);
		tool_bar.addSeparator(new Dimension(5, 20));
		tool_bar.add(cut_btn);
		tool_bar.add(copy_btn);
		tool_bar.add(paste_btn);
		tool_bar.addSeparator(new Dimension(5, 20));
		tool_bar.add(bold_btn);
		tool_bar.add(italic_btn);
		tool_bar.add(underline_btn);
		tool_bar.addSeparator(new Dimension(5, 20));
		tool_bar.add(size_cbox);
		tool_bar.add(color_cbox);
		tool_bar.addSeparator(new Dimension(5, 20));
		tool_bar.add(left_btn);
		tool_bar.add(center_btn);
		tool_bar.add(right_btn);
		tool_bar.addSeparator(new Dimension(5, 20));
		tool_bar.add(olist_btn);
		tool_bar.add(ulist_btn);
	}

	public class InsertHTMLTextAction extends HTMLTextAction {
		private static final long serialVersionUID = 230309794117371712L;
		protected HTML.Tag addTag;
		protected HTML.Tag alternateAddTag;
		protected HTML.Tag alternateParentTag;
		protected String html;
		protected HTML.Tag parentTag;

		public InsertHTMLTextAction(final String name, final String html, final HTML.Tag parentTag, final HTML.Tag addTag, final HTML.Tag alternateParentTag, final HTML.Tag alternateAddTag) {
			super(name);
			this.html = html;
			this.parentTag = parentTag;
			this.addTag = addTag;
			this.alternateParentTag = alternateParentTag;
			this.alternateAddTag = alternateAddTag;
		}

		public InsertHTMLTextAction(final String name, final String html, final HTML.Tag parentTag, final HTML.Tag addTag) {
			super(name);
			this.html = html;
			this.parentTag = parentTag;
			this.addTag = addTag;
		}

		public void actionPerformed(final ActionEvent event) {
			if (event == null) {
				return;
			}

			JEditorPane editor = edit_pan;
			HTMLDocument doc = getHTMLDocument(editor);
			int offset = editor.getCaretPosition();

			HTML.Tag usedParentTag = parentTag;
			HTML.Tag usedAddTag = addTag;
			int popDepth = elementCountToTag(doc, offset, parentTag);
			if (popDepth == -1 && alternateParentTag != null) {
				usedParentTag = alternateParentTag;
				usedAddTag = alternateAddTag;
				popDepth = elementCountToTag(doc, offset, alternateParentTag);
			}
			if (popDepth == -1) {
				return;
			}

			Element insertElement = findElementMatchingTag(doc, offset, usedParentTag);
			if (insertElement.getStartOffset() == offset) {
				insertAtBoundary(editor, doc, offset, insertElement, html, usedParentTag, usedAddTag);
			} else {
				int pushDepth = 0;
				insertHTML(editor, doc, offset, html, popDepth, pushDepth, usedAddTag);
			}
		}

		protected void insertHTML(final JEditorPane editor, final HTMLDocument doc, final int offset, final String html, final int popDepth, final int pushDepth, final HTML.Tag addTag) {
			HTMLEditorKit editorKit = getHTMLEditorKit(editor);
			try {
				editorKit.insertHTML(doc, offset, html, popDepth, pushDepth, addTag);
			} catch (BadLocationException e) {
				throw new RuntimeException(e);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		protected void insertAtBoundary(final JEditorPane editor, final HTMLDocument doc, final int offset, final Element insertElement, final String html, final HTML.Tag parentTag,
				final HTML.Tag addTag) {
			insertAtBoundaryImpl(editor, doc, offset, insertElement, html, parentTag, addTag);
		}

		/**
		 * @deprecated
		 */
		protected void insertAtBoundry(final JEditorPane editor, final HTMLDocument doc, final int offset, final Element insertElement, final String html, final HTML.Tag parentTag,
				final HTML.Tag addTag) {
			insertAtBoundaryImpl(editor, doc, offset, insertElement, html, parentTag, addTag);
		}

		private void insertAtBoundaryImpl(final JEditorPane editor, final HTMLDocument doc, final int offset, final Element insertElement, final String html, final HTML.Tag parentTag,
				final HTML.Tag addTag) {
			int popDepth = elementCountToTag(doc, offset, parentTag) + 1;
			int pushDepth = 1;
			insertHTML(editor, doc, offset, html, popDepth, pushDepth, addTag);
		}
	}

	public void setToolBarVisible(boolean enable) {
		tool_bar.setVisible(enable);
	}

	public JToolBar getToolBar() {
		return tool_bar;
	}

	protected void insertStringToEditor(String inputString) {
		try {
			edit_pan.getDocument().insertString(edit_pan.getCaretPosition(), inputString, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getText() {
		return edit_pan.getText();
	}

	public JEditorPane getEditorPane() {
		return (JEditorPane) edit_pan;
	}

	public void setText(String text) {
		edit_pan.setText(text);
	}

	public Document getDocument() {
		return edit_pan.getDocument();
	}

	public void setDocument(Document doc) {
		edit_pan.setDocument(doc);
	}

	public void setEnabled(boolean isEnabled) {
		super.setEnabled(isEnabled);
	}

	private class SmallButton extends JButton implements MouseListener {
		private static final long serialVersionUID = -4666450000937898885L;
		protected Border raised;
		protected Border lowered;
		protected Border inactive;

		public SmallButton(Action action, String tip) {
			super((Icon) action.getValue(Action.SMALL_ICON));
			addActionListener(action);
			raised = new BevelBorder(BevelBorder.RAISED);
			lowered = new BevelBorder(BevelBorder.LOWERED);
			inactive = new EmptyBorder(2, 2, 2, 2);
			this.setBorder(inactive);
			this.setOpaque(false);
			this.setToolTipText(tip);
			this.addMouseListener(this);
		}

		public void resetBorder() {
			setBorder(inactive);
		}

		public float getAlignmentY() {
			return 0.5f;
		}

		public void mousePressed(MouseEvent e) {
			setBorder(lowered);
		}

		public void mouseReleased(MouseEvent e) {
			setBorder(inactive);
		}

		public void mouseEntered(MouseEvent e) {
			setBorder(raised);
		}

		public void mouseExited(MouseEvent e) {
			setBorder(inactive);
		}

		public void mouseClicked(MouseEvent e) {

		}
	}

	private class SmallComboBox extends JComboBox {
		private static final long serialVersionUID = -4666450000937898885L;

		public SmallComboBox(DefaultComboBoxModel cur_model) {
			super(cur_model);
			this.setFocusable(false);
			this.setOpaque(false);
			this.setPreferredSize(new Dimension(60, 20));
		}
	}

	private class SmallColorComboBox extends JComboBox {
		private static final long serialVersionUID = -1109518718952422768L;

		public SmallColorComboBox(Color color_list[]) {
			super(color_list);
			this.setFocusable(false);
			this.setOpaque(false);
			this.setPreferredSize(new Dimension(40, 20));
			this.setRenderer(new SimpleCellRenderer());
		}
	}

	private class SimpleCellRenderer extends JPanel implements ListCellRenderer {
		private static final long serialVersionUID = 1930187218725767343L;
		private Icon font_color_icon = new ImageIcon(ClassLoader.getSystemResource("icon/desc_font_color.png"));
		private final JLabel icon_lab = new JLabel(font_color_icon);
		private final BorderLayout main_lay = new BorderLayout(0, 0);
		private final FlowLayout cen_lay = new FlowLayout(FlowLayout.CENTER, 0, 0);
		private final JPanel cen_pan = new JPanel(cen_lay);

		public SimpleCellRenderer() {
			super();
			this.setPreferredSize(new Dimension(25, 20));
			this.init();
		}

		public void init() {
			icon_lab.setVerticalAlignment(SwingConstants.CENTER);
			icon_lab.setHorizontalAlignment(SwingConstants.CENTER);

			icon_lab.setPreferredSize(new Dimension(20, 16));

			this.setBorder(null);
			icon_lab.setBorder(null);
			cen_pan.setBorder(null);

			cen_pan.setBackground(Color.WHITE);

			cen_pan.add(icon_lab);
			this.setLayout(main_lay);
			this.add(cen_pan, BorderLayout.CENTER);
		}

		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			if (index == -1) {
				icon_lab.setBorder(BorderFactory.createMatteBorder(0, 0, 4, 0, (Color) value));
				cen_pan.setVisible(true);
			} else {
				this.setBackground((Color) value);
				cen_pan.setVisible(false);
			}
			return this;
		}
	}

	public static void main(String args[]) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println("Error setting native LAF: " + e);
		}

		JFrame main_fra = new JFrame();
		main_fra.addWindowListener(new WindowListener() {
			public void windowActivated(WindowEvent e) {
			}

			public void windowClosed(WindowEvent e) {
			}

			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}

			public void windowDeactivated(WindowEvent e) {
			}

			public void windowDeiconified(WindowEvent e) {
			}

			public void windowIconified(WindowEvent e) {
			}

			public void windowOpened(WindowEvent e) {
			}
		});
		HTMLEditorPanel main_pan = new HTMLEditorPanel();

		main_fra.setLayout(new GridLayout(1, 1, 0, 0));
		main_fra.add(main_pan);

		main_pan.setMinimumSize(new Dimension(200, 150));
		main_pan.setPreferredSize(new Dimension(400, 300));
		main_fra.pack();

		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension scr_dim = toolkit.getScreenSize();
		main_fra.setLocation(new Point(scr_dim.width / 2 - main_fra.getWidth() / 2, scr_dim.height / 2 - main_fra.getHeight() / 2));
		main_fra.setVisible(true);
	}
}