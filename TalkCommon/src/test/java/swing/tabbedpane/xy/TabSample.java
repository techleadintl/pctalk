package swing.tabbedpane.xy;
import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.TabSet;
import javax.swing.text.TabStop;
public class TabSample {
  public static void main(String args[]) throws Exception {
    JFrame frame = new JFrame("Tab Attributes");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    StyledDocument document = new DefaultStyledDocument();
    //TabStop.ALIGN_CENTER, TabStop.ALIGN_DECIMAL,TabStop.ALIGN_LEFT, TabStop.ALIGN_RIGHT
    //"\tBAR\n", "\tCENTER\n", "\t3.14159265\n", "\tLEFT\n", "\tRIGHT\n" };
    SimpleAttributeSet attributes = new SimpleAttributeSet();
    TabStop tabstop = new TabStop(150,  TabStop.ALIGN_RIGHT, TabStop.LEAD_UNDERLINE);
    int position = document.getLength();
    document.insertString(position, "TabStop.ALIGN_BAR", null);
 
    TabSet tabset = new TabSet(new TabStop[] { tabstop });
    StyleConstants.setTabSet(attributes, tabset);
    document.setParagraphAttributes(position, 1, attributes, true);
    JTextPane textPane = new JTextPane(document);
    textPane.setEditable(false);
    JScrollPane scrollPane = new JScrollPane(textPane);
    frame.add(scrollPane, BorderLayout.CENTER);
    frame.setSize(300, 150);
    frame.setVisible(true);
  }
}