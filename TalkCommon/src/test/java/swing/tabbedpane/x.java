/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月30日 下午4:46:46
 * 
 ***************************************************************/
package swing.tabbedpane;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;

public class x {
	private static String webContent = "<html>" + "<head>" + "<style type=\"text/css\">mytag {color:red}</style>" + "</head>" + "<body>"
			+ "<p style=\"text-align: right\">Hello World JEditorPane jep = new JEditorPane()f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)</p>" + "</body>" + "</html>";

	static int caretPosition1;

	public static void main(String[] args) throws BadLocationException {
		JFrame f = new JFrame("HTML Example");
		final JTextPane jep = new JTextPane() {
			@Override
			public void paint(Graphics g) {
				Rectangle modelToView1;
				try {
					modelToView1 = modelToView(caretPosition1);
					System.out.println("--" + modelToView1);
				} catch (BadLocationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				super.paint(g);
			}
		};

		jep.setEditorKit(new HTMLEditorKit());
		final Document doc = jep.getDocument();
		jep.setCaretPosition(0);
		int caretPosition = jep.getCaretPosition();
		Rectangle modelToView = jep.modelToView(caretPosition);
		jep.setMinimumSize(new Dimension(0, 0));

		jep.setText(webContent);
		jep.setCaretPosition(doc.getLength());
		caretPosition1 = jep.getCaretPosition();

		addCustomComponent(jep, new JLabel("heihei"));

		JScrollPane comp = new JScrollPane(jep);
		comp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		f.add(comp, BorderLayout.CENTER);

		f.pack();
		f.setSize(300, 200);
		f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		f.setVisible(true);
	}

	public static boolean addCustomComponent(JTextPane jep, Component component) {
		final Document doc = jep.getDocument();
		jep.select(doc.getLength(), doc.getLength());
		boolean result = false;
		try {
			jep.insertComponent(component);
			result = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		jep.setCaretPosition(doc.getLength());
		return result;
	}
}
