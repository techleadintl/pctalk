package swing.image;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.xinwei.talk.common.LAF;

class ClipboardTest2 {
	JFrame mainFrame;
	JPanel mainPanel;
	JButton button;
	Clipboard cb;

	public ClipboardTest2() {
		mainFrame = new JFrame();
		mainPanel = new JPanel();
		button = new JButton("Button");
		button.setIcon(LAF.getTestImageIcon());

		cb = Toolkit.getDefaultToolkit().getSystemClipboard();
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ButtonTextAndImageTransferable btait = new ButtonTextAndImageTransferable(button);
				cb.setContents(btait, btait);
			}
		});

		mainPanel.add(button);
		mainFrame.getContentPane().add(mainPanel);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.pack();
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new ClipboardTest2();
			}
		});
	}
}

class ButtonTextAndImageTransferable extends ImageIcon implements Transferable, ClipboardOwner {
	DataFlavor[] flavors;
	JButton button;

	public void lostOwnership(Clipboard clipboard, Transferable contents) {
		System.out.println("lostownership");
	}

	public ButtonTextAndImageTransferable(JButton button) {
		flavors = new DataFlavor[3];
		flavors[0] = DataFlavor.stringFlavor;
		flavors[1] = DataFlavor.imageFlavor;
		this.button = button;
	}

	public DataFlavor[] getTransferDataFlavors() {
		return flavors;
	}

	public Object getTransferData(DataFlavor flavor) {
		System.out.println(flavor);
		//		if (flavor.equals(flavors[0])) {
		//			return button.getText();
		//		} else {
		//			if (flavor.equals(flavors[1])) {
		//				ImageIcon icon = (ImageIcon) button.getIcon();
		//				return icon.getImage();
		//			}
		//		}
		return new Object[] { button.getText(), ((ImageIcon) button.getIcon()).getImage() };
		//	return null;
	}

	public boolean isDataFlavorSupported(DataFlavor flavor) {
		if (flavor.equals(flavors[0]) || flavor.equals(flavors[1]))
			return true;
		return false;
	}
}