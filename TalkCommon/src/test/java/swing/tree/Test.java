/*************************************************************
 Copyright (c) 2016-2050  信威通信

 All Rights Reserved
 
 This is the proprietary source code of XinWei company product

 @author liyong

 2016年11月1日 上午8:57:33
 
**************************************************************/
package swing.tree;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.TitledBorder;

public class Test {
	public static void main(String[] args) {
		JCollapsiblePane cp = new JCollapsiblePane();

		// JCollapsiblePane can be used like any other container
		cp.setLayout(new BorderLayout());

		// the Controls panel with a textfield to filter the tree
		JPanel controls = new JPanel(new FlowLayout(FlowLayout.LEFT, 4, 0));
		controls.add(new JLabel("Search:"));
		controls.add(new JTextField(10));
		controls.add(new JButton("Refresh"));
		controls.setBorder(new TitledBorder("Filters"));
		cp.add("Center", controls);

		JFrame frame = new JFrame();
		frame.setLayout(new BorderLayout());

		// Put the "Controls" first
		frame.add("North", cp);

		// Then the tree - we assume the Controls would somehow filter the tree
		JScrollPane scroll = new JScrollPane(new JTree());
		frame.add("Center", scroll);

		// Show/hide the "Controls"
		JButton toggle = new JButton(cp.getActionMap().get(JCollapsiblePane.TOGGLE_ACTION));
		toggle.setText("Show/Hide Search Panel");
		frame.add("South", toggle);

		frame.pack();
		frame.setVisible(true);
	}
}
