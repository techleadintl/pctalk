package swing;

import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;

import com.xinwei.common.lookandfeel.component.McWillList;
import com.xinwei.common.lookandfeel.component.McWillListCellRenderer;
import com.xinwei.talk.ui.main.list.ListItem;

import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.BorderLayout;

public class JDialogDemo1 {
	public static void main(String[] agrs) {
		final JDialog dialog = new JDialog();//8

		dialog.setUndecorated(true);//去除窗体
		dialog.setAlwaysOnTop(true); //设置界面悬浮
		dialog.setSize(350, 120);

		JList<JLabel> list;

		DefaultListModel<JLabel> listModel;

		listModel = new DefaultListModel<JLabel>();
		list = new McWillList<JLabel>(listModel);
		list.setCellRenderer(new McWillListCellRenderer());
		list.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		
		listModel.addElement(new JLabel("test"));
		listModel.addElement(new JLabel("test2"));
		
		Container contentPane = dialog.getContentPane();

		contentPane.add(list, BorderLayout.CENTER);

		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowDeactivated(WindowEvent e) {
				e.getWindow().dispose();
			}
		});

		dialog.setVisible(true);
	}
}
