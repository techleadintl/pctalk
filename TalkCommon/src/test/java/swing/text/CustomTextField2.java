package swing.text;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class CustomTextField2 extends JTextField {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JLabel label = new JLabel("Email:");

				CustomTextField2 customTextField = new CustomTextField2(20);
				customTextField.setPlaceholder("example@address.com");
				/*
				 * customTextField.setFont(new Font("Arial", Font.PLAIN, 24)); customTextField.setForeground(Color.RED); customTextField.setPlaceholderForeground(Color.RED);
				 */

				JButton button = new JButton("Test focus");

				JPanel panel = new JPanel();
				panel.add(label);
				panel.add(customTextField);
				panel.add(button);

				JFrame frame = new JFrame("CustomTextField test");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.add(panel);
				frame.pack();
				frame.setVisible(true);
			}
		});
	}

	private Font originalFont;
	private Color originalForeground;
	/**
	 * Grey by default*
	 */
	private Color placeholderForeground = new Color(160, 160, 160);
	private boolean textWrittenIn;

	/**
	 * You can insert all constructors. I inserted only this one.*
	 */
	
	
	public CustomTextField2(int columns) {
		super(columns);
	}

	@Override
	public void setFont(Font f) {
		super.setFont(f);
		if (!isTextWrittenIn()) {
			originalFont = f;
		}
	}

	@Override
	public void setForeground(Color fg) {
		super.setForeground(fg);
		if (!isTextWrittenIn()) {
			originalForeground = fg;
		}
	}

	public Color getPlaceholderForeground() {
		return placeholderForeground;
	}

	public void setPlaceholderForeground(Color placeholderForeground) {
		this.placeholderForeground = placeholderForeground;
	}

	public boolean isTextWrittenIn() {
		return textWrittenIn;
	}

	public void setTextWrittenIn(boolean textWrittenIn) {
		this.textWrittenIn = textWrittenIn;
	}

	public void setPlaceholder(final String text) {

		this.customizeText(text);

		this.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				warn();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				warn();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				warn();
			}

			public void warn() {
				if (getText().trim().length() != 0) {
					setFont(originalFont);
					setForeground(originalForeground);
					setTextWrittenIn(true);
				}

			}
		});

		this.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				if (!isTextWrittenIn()) {
					setText("");
				}

			}

			@Override
			public void focusLost(FocusEvent e) {
				if (getText().trim().length() == 0) {
					customizeText(text);
				}
			}

		});

	}

	private void customizeText(String text) {
		setText(text);
		/**
		 * If you change font, family and size will follow changes, while style will always be italic
		 **/
		setFont(new Font(getFont().getFamily(), Font.ITALIC, getFont().getSize()));
		setForeground(getPlaceholderForeground());
		setTextWrittenIn(false);
	}

}