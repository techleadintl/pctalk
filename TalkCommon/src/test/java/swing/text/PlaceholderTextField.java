package swing.text;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class PlaceholderTextField extends JTextField {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JLabel label = new JLabel("Email:");

				PlaceholderTextField customTextField = new PlaceholderTextField(20);
				customTextField.setPlaceholder("example@address.com");
				/*
				 * customTextField.setFont(new Font("Arial", Font.PLAIN, 24)); customTextField.setForeground(Color.RED); customTextField.setPlaceholderForeground(Color.RED);
				 */

				JButton button = new JButton("Test focus");
				button.requestFocus();
				JPanel panel = new JPanel();
				panel.add(label);
				panel.add(customTextField);
				panel.add(button);

				JFrame frame = new JFrame("CustomTextField test");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.add(panel);
				frame.pack();
				frame.setVisible(true);
			}
		});
	}

	private Font originalFont;
	private Color originalForeground;
	private Color originalBackground;
	private Border originalBorder;
	private String placeholder;
	private boolean textWrittenIn;

	public PlaceholderTextField() {
		addListeners();
	}

	public PlaceholderTextField(final Document pDoc, final String pText, final int pColumns) {
		super(pDoc, pText, pColumns);
		addListeners();
	}

	public PlaceholderTextField(final int pColumns) {
		super(pColumns);
		addListeners();
	}

	public PlaceholderTextField(final String pText) {
		super(pText);
		addListeners();
	}

	public PlaceholderTextField(final String pText, final int pColumns) {
		super(pText, pColumns);
		addListeners();
	}

	public void addListeners() {
		final Color background = getBackground();
		final Border border = getBorder();
		this.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				warn();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				warn();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				warn();
			}

			public void warn() {
				if (getText().trim().length() != 0) {
					setFont(originalFont);
					setForeground(originalForeground);
					setTextWrittenIn(true);
				}
			}
		});

		this.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				if (!isTextWrittenIn()) {
					setText("");
				}
				if (originalBackground != null) {
					setBackground(originalBackground);
				} else {
					setBackground(background);
				}
				if (originalBorder != null) {
					setBorder(originalBorder);
				} else {
					setBorder(border);
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (getText().trim().length() == 0) {
					customizeText(placeholder);
				}
				setBorder(null);
			}
		});
	}

	@Override
	public void setFont(Font f) {
		super.setFont(f);
		if (!isTextWrittenIn()) {
			originalFont = f;
		}
	}

	@Override
	public void setForeground(Color fg) {
		super.setForeground(fg);
		if (!isTextWrittenIn()) {
			originalForeground = fg;
		}
	}

	public void setBackground(Color bg) {
		super.setBackground(bg);
		originalBackground = bg;
	}

	@Override
	public void setBorder(Border border) {
		super.setBorder(border);
		originalBorder = border;
	}

	public boolean isTextWrittenIn() {
		return textWrittenIn;
	}

	public void setTextWrittenIn(boolean textWrittenIn) {
		this.textWrittenIn = textWrittenIn;
	}

	public void setPlaceholder(final String s) {
		placeholder = s;
		this.customizeText(s);
	}

	private void customizeText(String text) {
		setText(text);
		setForeground(getDisabledTextColor());
		setTextWrittenIn(false);
	}

	@Override
	protected void paintComponent(final Graphics pG) {
		super.paintComponent(pG);
		Color placeholderBackground = getPlaceholderBackground();
		if (!isFocusOwner() && placeholderBackground != null) {
			final Graphics2D g = (Graphics2D) pG;
			g.setColor(placeholderBackground);
			g.fillRect(0, 0, getBounds().width, getBounds().height);
		}
	}

	public Color getPlaceholderBackground() {
		return null;
	}
}