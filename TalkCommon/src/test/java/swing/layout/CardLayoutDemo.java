package swing.layout;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.component.McWillCardPanel;

/**
 */
public class CardLayoutDemo {
	int j = 0;

	public void addComponentToPane(Container pane) {
		final McWillCardPanel cardPanel = new McWillCardPanel();
		JPanel controlPanel = new JPanel();

		pane.setLayout(new BorderLayout());
		pane.add(controlPanel, BorderLayout.SOUTH);
		pane.add(cardPanel, BorderLayout.CENTER);

		String cardName[] = { "fdafafaf", "ssss", "ss", "afsssssssssss" };
		for (int i = 0; i < 4; i++) {
			//面板中添加的每个按钮对应设置一个卡片名
			cardPanel.add(cardName[i], new JButton("按钮" + i));
		}

		JButton b0 = new JButton("Next");
		b0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardPanel.show("fdafafaf");
			}
		});
		JButton b1 = new JButton("Next1");

		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardPanel.show("ssss");
			}
		});
		controlPanel.add(b0);
		controlPanel.add(b1);
	}

	private static void createAndShowGUI() {
		JFrame frame = new JFrame("CardLayoutManage");
		frame.setBounds(400, 200, 600, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		CardLayoutDemo demo = new CardLayoutDemo();
		demo.addComponentToPane(frame.getContentPane());
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
