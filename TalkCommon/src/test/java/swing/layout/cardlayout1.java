package swing.layout;

import java.awt.*;

import javax.swing.*;

import java.awt.event.*;//引入事件包

//定义类时 实现监听接口

public class cardlayout1 extends JFrame {

	Panel buttonPanel = new Panel(new FlowLayout(FlowLayout.LEFT));

	//定义卡片布局对象

	//定义字符数组，为卡片命名

	String cardName[] = { "fdafafaf", "ssss", "ss", "afsssssssssss" };

	//定义构造函数

	public cardlayout1() {

		super("卡片布局管理器");

		setSize(400, 200);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setLocationRelativeTo(null);

		setVisible(true);

		final CardLayout card = new CardLayout();
		//设置cardPanel面板对象为卡片布局
		final Panel cardPanel = new Panel();
		
		cardPanel.setLayout(card);

		for (int i = 0; i < 4; i++) {
			cardPanel.add(cardName[i], new JButton("按钮" + i));
		}

		//实例化按钮对象

		JButton b0 = new JButton("0ssss");

		JButton b1 = new JButton("1sssssss");

		//为按钮对象注册监听器

		b0.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				card.show(cardPanel, cardName[0]);
			}
		});

		b1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				card.show(cardPanel, cardName[1]);
			}
		});

		buttonPanel.add(b0);

		buttonPanel.add(b1);

		//定义容器对象为当前窗体容器对象

		Container container = getContentPane();

		//将 cardPanel面板放置在窗口边界布局的中间，窗口默认为边界布局

		container.add(cardPanel, BorderLayout.CENTER);

		// 将controlpaPanel面板放置在窗口边界布局的南边，

		container.add(buttonPanel, BorderLayout.NORTH);

	}

	public static void main(String[] args) {

		cardlayout1 kapian = new cardlayout1();

	}

}
