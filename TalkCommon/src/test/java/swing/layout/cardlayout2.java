package swing.layout;

import java.awt.BorderLayout;
import java.awt.Container;
//引入事件包
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import com.xinwei.common.lookandfeel.component.McWillCardPanel;

//定义类时 实现监听接口

public class cardlayout2 extends JFrame {

	//定义构造函数

	public cardlayout2() {

		super("卡片布局管理器");

		setSize(400, 200);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setLocationRelativeTo(null);

		setVisible(true);
		
		McWillCardPanel cp = new McWillCardPanel();
		for (int i = 0; i < 4; i++) {

			//面板中添加的每个按钮对应设置一个卡片名

			cp.addCard(i + "", new JButton("按钮" + i));

		}

		//定义容器对象为当前窗体容器对象

		Container container = getContentPane();

		//将 cardPanel面板放置在窗口边界布局的中间，窗口默认为边界布局

		container.add(cp, BorderLayout.CENTER);

	}

	public static void main(String[] args) {

		cardlayout2 kapian = new cardlayout2();

	}

}
