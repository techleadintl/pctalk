package swing.window;

import java.awt.AlphaComposite;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.xinwei.common.lookandfeel.util.ImageUtil;

public class DrawTrail2 {
	BufferedImage image = new BufferedImage(500, 500, BufferedImage.TYPE_INT_RGB);
	Graphics g = image.getGraphics();
	private DrawCanvas canvas = new DrawCanvas();
	private int preX = -1;
	private int preY = -1;
	private Image offScreenImage; //图形缓存

	//

	final Toolkit defaultToolkit = java.awt.Toolkit.getDefaultToolkit();

	private int BORDER = 10;

	private double limitMin = 0.1;
	private double limitMax = 5;

	private double step = 0.12;
	private double current = 1;

	Image srcImage;

	Image subImage;
	int srcImageW = 0;
	int srcImageH = 0;

	public Image xx(Image image, int w, int h) {
		int srcImageW = image.getWidth(canvas);
		int srcImageH = image.getHeight(canvas);
		int componentW = w - BORDER;
		int componentH = h - BORDER;

		int H = srcImageH;
		int W = srcImageW;
		if (srcImageH > componentH || srcImageW > componentW) {
			if (srcImageW / srcImageH > componentW / componentH) {
				W = componentW;
				H = srcImageH * componentW / srcImageW;
			} else {
				H = componentH;
				W = srcImageW * componentH / srcImageH;
			}
		}
		current = (double) H / srcImageH;
		return ImageUtil.resizeImage(image, W, H);
	}

	public void init() {

		this.srcImage = new ImageIcon(CustomPanel.class.getResource("QQ图片20170214133847.jpg")).getImage();

		srcImageW = srcImage.getWidth(canvas);
		srcImageH = srcImage.getHeight(canvas);
		//			initImage(image, srcImageW, srcImageH);

		this.subImage = xx(this.srcImage, 500, 400);

		g.fillRect(0, 0, 1600, 1000);
		JFrame frame = new JFrame("测试画出鼠标的轨迹");
		frame.setSize(600, 600);
		//					frame.setAlwaysOnTop(true);
		frame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					SwingUtilities.getWindowAncestor(e.getComponent()).dispose();
				}
			}

		});
		frame.setUndecorated(true);
		//		frame.setBackground(new Color(0, 0, 0, 0));

		frame.add(canvas);
		frame.setLayout(null);
		canvas.setBounds(0, 0, 500, 500);
		frame.setVisible(true);

//		canvas.addMouseMotionListener(new MouseMotionAdapter() {
//			@Override
//			public void mouseDragged(MouseEvent e) {
//				if (preX > 0 && preY > 0) {
//					g.setColor(Color.black);
//					g.drawLine(preX, preY, e.getX(), e.getY());
//				}
//				preX = e.getX();
//				preY = e.getY();
//				canvas.repaint();
//			}
//		});

		canvas.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				System.out.println("ssssssssssssssss");
				int wheelRotation = e.getWheelRotation();
				if (wheelRotation == 1) { //滑轮向后
					current -= step;
					if (current < limitMin) {
						current = limitMin;
					}
				} else if (e.getWheelRotation() == -1) {//滑轮向前
					current += step;
					if (current > limitMax) {
						current = limitMax;
					}
				} else {
				}

				subImage = ImageUtil.resizeImage(srcImage, Double.valueOf(srcImageW * current).intValue(), Double.valueOf(srcImageH * current).intValue());
				//				initImage(scaledImage, imageW, imageH);
				subImage=srcImage;
				BufferedImage newImage = new BufferedImage(canvas.getWidth(), canvas.getHeight(), BufferedImage.TYPE_INT_RGB);
				Graphics graphics = newImage.getGraphics();

				drawImage((Graphics2D) graphics);
				
				image=newImage;

				canvas.repaint();

			}
		});

		frame.addWindowListener(new WindowAdapter()//添加窗口关闭处理函数
		{
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	public void drawImage(Graphics2D g2d) {
		int srcImageW = subImage.getWidth(canvas);
		int srcImageH = subImage.getHeight(canvas);
		int componentW = canvas.getWidth() - BORDER;
		int componentH = canvas.getHeight() - BORDER;

		int y = 0;
		int x = 0;
		int w = srcImageW;
		int h = srcImageH;
		Image image = subImage;
		if (srcImageH > componentH || srcImageW > componentW) {
			if (srcImageW > componentW) {
				x = (srcImageW - componentW) / 2;
				w = componentW;
			}
			if (srcImageH > componentH) {
				y = (srcImageH - componentH) / 2;
				h = componentH;
			}
			image = ImageUtil.getSubimage(subImage, x, y, w, h);
		}
		g2d.setColor(Color.RED);
		g2d.drawString("fdafafa", 50, 50);
		g2d.drawImage(image, 0, 0, null);
//		drawImage(image, g2d);
	}

	protected void drawImage(Image image, Graphics2D g2d) {
		int srcImageW = image.getWidth(canvas);
		int srcImageH = image.getHeight(canvas);
		int componentW = canvas.getWidth() - BORDER;
		int componentH = canvas.getHeight() - BORDER;

		int H = srcImageH;
		int W = srcImageW;
		if (srcImageH > componentH || srcImageW > componentW) {
			if (srcImageW / srcImageH > componentW / componentH) {
				W = componentW;
				H = srcImageH * componentW / srcImageW;
			} else {
				H = componentH;
				W = srcImageW * componentH / srcImageH;
			}
		}

		g2d.setComposite(AlphaComposite.SrcOver.derive(1f));
		g2d.drawImage(image, (componentW - W) / 2, (componentH - H) / 2 + BORDER, W, H, canvas);
	}

	public static void main(String[] args) {
		DrawTrail2 dc = new DrawTrail2();
		dc.init();

	}

	class DrawCanvas extends Canvas {
		private static final long serialVersionUID = 1L;
		private int BORDER = 10;

		@Override
		public void paint(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;

			int width = getWidth();
			int height = getHeight();

			g2d.setComposite(AlphaComposite.SrcOver.derive(0.0f));
			g2d.setColor(getBackground());
			g2d.fillRect(0, 0, width, height);

			g2d.setComposite(AlphaComposite.SrcOver.derive(0.8f));
			g2d.setColor(Color.BLACK);
			g2d.fillRect(0, BORDER, width - BORDER, height - BORDER);

			g2d.drawImage(image, 0, 0, null);
		}

		@Override
		public void update(Graphics g) {
			System.out.println("yyy");
			if (offScreenImage == null)
				offScreenImage = this.createImage(500, 500); //新建一个图像缓存空间,这里图像大小为800*600
			Graphics gImage = offScreenImage.getGraphics(); //把它的画笔拿过来,给gImage保存着

			paint(gImage); //将要画的东西画到图像缓存空间去

			g.drawImage(offScreenImage, 0, 0, null); //然后一次性显示出来

		}

	}
}
