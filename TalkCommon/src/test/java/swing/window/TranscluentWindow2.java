package swing.window;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.xinwei.common.lookandfeel.util.ImageUtil;

public class TranscluentWindow2 {

	public static void main(String[] args) {
		new TranscluentWindow2();
	}

	public TranscluentWindow2() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					//                    try {
					//                        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					//                    } catch (Exception ex) {
					//                    }

					final JFrame frame = new JFrame();
					//					frame.setAlwaysOnTop(true);
					frame.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							if (e.getClickCount() == 2) {
								SwingUtilities.getWindowAncestor(e.getComponent()).dispose();
							}
						}

					});
					frame.setUndecorated(true);
					frame.setBackground(new Color(0, 0, 0, 0));
					TranslucentPane contentPane = new TranslucentPane();
					frame.setContentPane(contentPane);
					//					final CustomPanel comp = new CustomPanel();
					//					comp.setImage(new ImageIcon(CustomPanel.class.getResource("QQ图片20170214133847.jpg")).getImage());
					//
					//					comp.setBorder(BorderFactory.createLineBorder(Color.RED));
					//					comp.setPreferredSize(new Dimension(500, 200));
					//					contentPane.add(comp, BorderLayout.CENTER);
					frame.setSize(500, 400);
					//					frame.pack();
					frame.setLocationRelativeTo(null);

					//					frame.addMouseWheelListener(new MouseAdapter() {
					//						@Override
					//						public void mouseWheelMoved(MouseWheelEvent e) {
					//							super.mouseWheelMoved(e);
					//							System.out.println("ssssssss");
					//							comp.setIcon(LAF.getImageIcon(LAF.getTestImageIcon(), (int) (comp.getWidth() * 1.1), (int) (comp.getHeight() * 1.1)));
					//
					//							frame.setSize((int) (frame.getWidth() * 1.1), (int) (frame.getHeight() * 1.1));
					//							comp.setSize((int) (comp.getWidth() * 1.1), (int) (comp.getHeight() * 1.1));
					//							frame.repaint();
					//							SwingUtil.centerWindowOnScreen(frame);
					//						}
					//
					//					});

					frame.setVisible(true);
					frame.toFront();

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
	}

	Image deleteImage = new ImageIcon(CustomPanel.class.getResource("delete.png")).getImage();

	public class TranslucentPane extends JPanel {
		final Toolkit defaultToolkit = java.awt.Toolkit.getDefaultToolkit();
		int BORDER = 10;

		private double limitMin = 0.1;
		private double limitMax = 5;

		private double step = 0.12;
		private double current = 1;

		Image srcImage;

		Image subImage;
		int srcImageW = 0;
		int srcImageH = 0;

		JLabel label = new JLabel();

		public TranslucentPane() {
			setOpaque(false);

			setLayout(null);

			add(label);

			addMouseWheelListener(new MyMouseListener());

			this.srcImage = new ImageIcon(CustomPanel.class.getResource("QQ图片20170214133847.jpg")).getImage();

			srcImageW = srcImage.getWidth(this);
			srcImageH = srcImage.getHeight(this);
			//			initImage(image, srcImageW, srcImageH);

			this.subImage = xx(this.srcImage, 500, 400);

		}

		public Image xx(Image image, int w, int h) {
			int srcImageW = image.getWidth(this);
			int srcImageH = image.getHeight(this);
			int componentW = w - BORDER;
			int componentH = h - BORDER;

			int H = srcImageH;
			int W = srcImageW;
			if (srcImageH > componentH || srcImageW > componentW) {
				if (srcImageW / srcImageH > componentW / componentH) {
					W = componentW;
					H = srcImageH * componentW / srcImageW;
				} else {
					H = componentH;
					W = srcImageW * componentH / srcImageH;
				}
			}
			current = (double) H / srcImageH;
			return ImageUtil.resizeImage(image, W, H);
		}

		@Override
		protected void paintComponent(Graphics g) {
			if (subImage == null)
				return;

			Graphics2D g2d = (Graphics2D) g.create();
			int width = getWidth();
			int height = getHeight();

			g2d.setComposite(AlphaComposite.SrcOver.derive(0.0f));
			g2d.setColor(getBackground());
			g2d.fillRect(0, 0, width, height);

			g2d.setComposite(AlphaComposite.SrcOver.derive(0.8f));
			g2d.setColor(Color.BLACK);
			g2d.fillRect(0, BORDER, width - BORDER, height - BORDER);

			paintDeleteImage(g2d);
		}

		//		@Override
		//		public void paint(Graphics g) {
		//			super.paint(g);
		//
		//			if (subImage == null)
		//				return;
		//
		//			Graphics2D g2d = (Graphics2D) g.create();
		//			int width = getWidth();
		//			int height = getHeight();
		//
		//			g2d.setComposite(AlphaComposite.SrcOver.derive(0.0f));
		//			g2d.setColor(getBackground());
		//			g2d.fillRect(0, 0, width, height);
		//
		//			g2d.setComposite(AlphaComposite.SrcOver.derive(0.8f));
		//			g2d.setColor(Color.BLACK);
		//			g2d.fillRect(0, BORDER, width - BORDER, height - BORDER);
		//
		//			drawImage(g2d);
		//
		//			paintDeleteImage(g2d);
		//
		//			//			 g.drawImage(image, 0, 0, null); 
		//		}

		protected void paintDeleteImage(Graphics2D g2d) {
			g2d.setComposite(AlphaComposite.SrcOver.derive(1f));
			g2d.setColor(getBackground());
			g2d.fillOval(getWidth() - BORDER * 2, 0, BORDER * 2, BORDER * 2);
			g2d.drawImage(deleteImage, getWidth() - BORDER * 2, 0, BORDER * 2, BORDER * 2, this);
		}

		protected void drawImage(Image image, int srcImageW, int srcImageH) {
			int componentW = getWidth() - BORDER;
			int componentH = getHeight() - BORDER;

			int y = 0;
			int x = 0;
			int w = srcImageW;
			int h = srcImageH;
			if (srcImageH > componentH || srcImageW > componentW) {
				if (srcImageW > componentW) {
					x = (srcImageW - componentW) / 2;
					w = componentW;
				}
				if (srcImageH > componentH) {
					y = (srcImageH - componentH) / 2;
					h = componentH;
				}
				image = ImageUtil.getSubimage(subImage, x, y, w, h);
			}

			drawImage2(image, w, h);
		}

		protected void drawImage2(Image image, int srcImageW, int srcImageH) {
			int componentW = getWidth() - BORDER;
			int componentH = getHeight() - BORDER;
			int H = srcImageH;
			int W = srcImageW;
			if (H == -1) {
				System.out.println();
			}
			if (srcImageH > componentH || srcImageW > componentW) {
				if (srcImageW / srcImageH > componentW / componentH) {
					W = componentW;
					H = srcImageH * componentW / srcImageW;
				} else {
					H = componentH;
					W = srcImageW * componentH / srcImageH;
				}
			}

			//			g2d.setComposite(AlphaComposite.SrcOver.derive(1f));
			label.setIcon(new ImageIcon(image));
			label.setBounds((componentW - W) / 2, (componentH - H) / 2 + BORDER, W, H);
			//			updateUI();
			System.out.println(label.getBounds());

			//			g2d.drawImage(image, (componentW - W) / 2, (componentH - H) / 2 + BORDER, W, H, this);
		}

		class MyMouseListener extends MouseAdapter {
			public void mouseWheelMoved(MouseWheelEvent e) {
				int wheelRotation = e.getWheelRotation();
				if (wheelRotation == 1) { //滑轮向后
					current -= step;
					if (current < limitMin) {
						current = limitMin;
					}
				} else if (e.getWheelRotation() == -1) {//滑轮向前
					current += step;
					if (current > limitMax) {
						current = limitMax;
					}
				} else {
				}

				int w = Double.valueOf(srcImageW * current).intValue();
				int h = Double.valueOf(srcImageH * current).intValue();

				subImage = ImageUtil.resizeImage(srcImage, w, h);
				{
					//					Image xx = ImageUtil.getScaledImage(srcImage, Double.valueOf(srcImageW * current).intValue(), Double.valueOf(srcImageH * current).intValue());
					//					//				initImage(scaledImage, imageW, imageH);
					//					BufferedImage newImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
					//					Graphics graphics = newImage.getGraphics();
					//					graphics.drawImage(xx, 0, 0, null);
					//					subImage = newImage;
				}
				drawImage(subImage, w, h);

				//				repaint();
			}
		}
	}

	public boolean isInOval(int x, int y, int rx, int ry, int r) {
		return Math.sqrt(rx - x) + Math.sqrt(ry - y) < Math.sqrt(r);
	}
	//	/** 获取屏幕的边界 */
	//			Insets screenInsets = defaultToolkit.getScreenInsets(getGraphicsConfiguration());
	//			/** 获取屏幕大小 */
	//			Dimension screenSize = defaultToolkit.getScreenSize();
	//			if (screenInsets.bottom > 0) {
	//				screenSize.height -= screenInsets.bottom;
	//			} else if (screenInsets.right > 0) {
	//				screenSize.width -= screenInsets.right;
	//			} else if (screenInsets.left > 0) {
	//				screenSize.width -= screenInsets.left;
	//			} else if (screenInsets.top > 0) {
	//				screenSize.height -= screenInsets.top;
	//			}
}