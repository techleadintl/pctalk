package swing.window;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class JFrameBackground {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(500, 500, 300, 300);
		frame.setUndecorated(true);
		frame.	setOpacity(0.05f); 
		JPanel pane = new JPanel() {
			@Override
			public void paint(Graphics g) {
				super.paint(g);

				g.setColor(Color.red);
				g.fill3DRect(10, 10, 100, 100, true);
			}
		};

		frame.setContentPane(pane);
		frame.setVisible(true);
	}
}