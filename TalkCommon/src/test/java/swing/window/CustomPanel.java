package swing.window;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.util.ColorUtil;
import com.xinwei.common.lookandfeel.util.ImageUtil;

public class CustomPanel extends JPanel {
	// 这两组x和y为鼠标点下时在屏幕的位置和拖动时所在的位置
	int newX, newY, oldX, oldY;
	// 这两个坐标为组件当前的坐标
	int startX, startY;
	boolean drag;

	int x = 0;
	int y = 0;

	int limitW = 100;
	int limitH = 100;
	private Image srcImage = null;
	private Image image = null;
	private boolean canDrag = true;
	private double wheel = 1.0;
	private double STEP = 0.05;
	int srcImageW = 0;
	int srcImageH = 0;

	public void setImage(Image image) {
		this.srcImage = image;
		wheel = 1.0;
		srcImageW = srcImage.getWidth(this);
		srcImageH = srcImage.getHeight(this);
		initImage(image, srcImageW, srcImageH);
	}

	private boolean initImage(Image image, int imageW, int imageH) {
		if (image == null || srcImage == null)
			return false;

		if (imageW > srcImageW || imageH > srcImageH) {
			return false;
		}
		this.image = image;

		if (imageW <= limitW && imageH <= limitH) {
			canDrag = false;
		} else {
			canDrag = true;
		}
		newX = newY = oldX = oldY = 0;
		startX = startY = 0;
		x = (getWidth() - imageW) / 2;
		y = (getHeight() - imageH) / 2;

		repaint();
		return true;
	}

	public CustomPanel() {

		MyListener m = new MyListener();

		addMouseListener(m);

		addMouseMotionListener(m);
		addMouseWheelListener(m);
	}

	@Override
	public void update(Graphics g) {
		super.update(g);
	}

	public void paint(Graphics g) {
		super.paint(g);
		
		if (image == null)
			return;
		
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(image, x, y, null);
		
		int rx1 = (getWidth() - limitW) / 2;
		int ry1 = (getHeight() - limitH) / 2;
		
		Color color2 = ColorUtil.getAlphaColor(Color.BLACK, 150);
		Paint paint = g2d.getPaint();
		GradientPaint gp = new GradientPaint(0, 0, color2, 0, getHeight(), color2);
		g2d.setPaint(gp);

		g2d.fillRect(0, 0, getWidth(), ry1);
		g2d.fillRect(0, ry1 + limitH, getWidth(), getHeight());
		g2d.fillRect(0, ry1, rx1, limitH);
		g2d.fillRect(rx1 + limitW, ry1, getWidth() - rx1 - limitW, limitH);
		g2d.setColor(McWillTheme.getThemeColor().getSrcColor());
		g2d.drawRect(rx1 - 1, ry1 - 1, limitW + 1, limitH + 1);

		g2d.setPaint(paint);
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (image == null)
			return;
		
		int rx1 = (getWidth() - limitW) / 2;
		int ry1 = (getHeight() - limitH) / 2;
		
		if (canDrag) {
			BufferedImage subimage = ImageUtil.getSubimage(image, rx1 - x, ry1 - y, limitW, limitH);
			setSubimage(subimage);
		} else {
			setSubimage(image);
		}

		super.paintComponent(g);
	}

	protected void setSubimage(Image subimage) {
		// TODO Auto-generated method stub

	}

	class MyListener extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			if (image == null || !canDrag) {
				return;
			}
			// 当鼠标点下的时候记录鼠标当前在屏幕的位置
			startX = x;
			startY = y;
			oldX = e.getXOnScreen();
			oldY = e.getYOnScreen();
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			if (image == null || !canDrag) {
				return;
			}

			newX = e.getXOnScreen();
			newY = e.getYOnScreen();

			x = startX + (newX - oldX);
			y = startY + (newY - oldY);

			// 拖动的时候记录新坐标

			int rx1 = (getWidth() - limitW) / 2;
			int ry1 = (getHeight() - limitH) / 2;

			boolean repaint = true;
			if (x > rx1) {
				x = rx1;
				repaint = false;
			}
			if (y > ry1) {
				y = ry1;
				repaint = false;
			}

			if (x + image.getWidth(e.getComponent()) < rx1 + limitW) {
				x = rx1 + limitW - image.getWidth(e.getComponent());
				repaint = false;
			}
			if (y + image.getHeight(e.getComponent()) < ry1 + limitH) {
				y = ry1 + limitH - image.getHeight(e.getComponent());
				repaint = false;
			}
			if (repaint) {
				repaint();
			}
		}

		public void mouseWheelMoved(MouseWheelEvent e) {
			int wheelRotation = e.getWheelRotation();
			int imageW = 0;
			int imageH = 0;
			if (wheelRotation == 1) { //滑轮向后
				if (wheel - STEP <= 0) {
					return;
				}
				if (image.getWidth(e.getComponent()) == limitW || image.getHeight(e.getComponent()) == limitH) {
					return;
				}

				imageW = Double.valueOf((wheel - STEP) * srcImageW).intValue();
				imageH = Double.valueOf((wheel - STEP) * srcImageH).intValue();
				if (imageW <= 0 || imageH <= 0) {
				}
				if (imageW < limitW || imageH < limitH) {
					if (imageW < limitW) {
						imageH = Double.valueOf(imageH * ((double) limitW / imageW)).intValue();
						imageW = limitW;
					} else {
						imageW = Double.valueOf(imageW * ((double) limitH / imageH)).intValue();
						imageH = limitH;
					}
				} else {
					wheel -= STEP;
				}
			} else if (e.getWheelRotation() == -1) {//滑轮向前
				if (wheel > 1) {
					return;
				}
				imageW = Double.valueOf((wheel + STEP) * srcImageW).intValue();
				imageH = Double.valueOf((wheel + STEP) * srcImageH).intValue();
				if (imageW <= 0 || imageH <= 0) {
				}
				wheel += STEP;
			} else {
			}
			if (imageW <= 0 || imageH <= 0) {
				return;
			}
			Image scaledImage = srcImage.getScaledInstance(imageW, imageH, Image.SCALE_SMOOTH);
			initImage(scaledImage, imageW, imageH);

		}
	}
}