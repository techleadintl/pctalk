package swing.window;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.talk.common.LAF;

public class TranscluentWindow3 {
	ImageIcon _1_1Image = new ImageIcon(CustomPanel.class.getResource("_1_1.png"));

	ImageIcon zoominImage = new ImageIcon(CustomPanel.class.getResource("zoomin.png"));
	ImageIcon zoomoutImage = new ImageIcon(CustomPanel.class.getResource("zoomout.png"));

	ImageIcon screenfullImage = new ImageIcon(CustomPanel.class.getResource("screenfull.png"));
	ImageIcon screenrestoreImage = new ImageIcon(CustomPanel.class.getResource("screenrestore.png"));

	ImageIcon rotateImage = new ImageIcon(CustomPanel.class.getResource("rotate.png"));
	ImageIcon saveImage = new ImageIcon(CustomPanel.class.getResource("save.png"));

	ImageIcon deleteImage1 = new ImageIcon(CustomPanel.class.getResource("delete (55).png"));
	ImageIcon deleteImage2 = new ImageIcon(CustomPanel.class.getResource("delete (54).png"));

	ImageIcon deleteBigImage1 = new ImageIcon(CustomPanel.class.getResource("delete (57).png"));
	ImageIcon deleteBigImage2 = new ImageIcon(CustomPanel.class.getResource("delete (56).png"));

	ImageIcon[] deleteImages = new ImageIcon[] { deleteImage1, deleteImage2 };
	ImageIcon[] deleteBigImages = new ImageIcon[] { deleteBigImage1, deleteBigImage2 };

	JFrame frame;

	boolean max;

	Rectangle screenBound;

	BufferedImage srcImage;

	final Toolkit defaultToolkit = java.awt.Toolkit.getDefaultToolkit();
	int BORDER = 50;

	private int minWindowW = 610;
	private int minWindowH = 410;

	private double limitMin = 0.1;
	private double limitMax = 5;

	private double step = 0.1;
	private double current = 1;

	int srcImageW = 0;
	int srcImageH = 0;

	int imageW = 0;
	int imageH = 0;

	McWillIconButton deleteButton;

	JPanel buttonPanel;

	private Rectangle areaRec;

	public static void main(String[] args) {
		new TranscluentWindow3();
	}

	public TranscluentWindow3() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					srcImage = ImageUtil.getBufferedImage(CustomPanel.class.getResource("QQ图片20170214133847.jpg"));

					frame = new JFrame();

					Rectangle screenRectangle2 = SwingUtil.getScreenRectangle(frame);
					Rectangle screenRectangle = screenRectangle2;
					screenRectangle.width -= 40;
					screenRectangle.height -= 40;

					srcImageW = srcImage.getWidth();
					srcImageH = srcImage.getHeight();

					double x = 1d;

					int windowW = 0;
					int windowH = 0;

					if (srcImageW <= minWindowW && srcImageH < minWindowH) {
						windowW = minWindowW;
						windowH = minWindowH;
					} else if (srcImageW <= screenRectangle.width && srcImageH <= screenRectangle.height) {
						windowW = srcImageW;
						windowH = srcImageH;
					} else {
						while (!(srcImageW * x < screenRectangle.width && srcImageH * x < screenRectangle.height)) {
							x -= 0.1;
						}
						windowW = Double.valueOf(srcImageW * x).intValue();
						windowH = Double.valueOf(srcImageH * x).intValue();

					}
					setImagePercent(windowW, windowH);

					Rectangle rec = SwingUtil.getScreenRectangle(frame);
					areaRec = new Rectangle((rec.x - windowW) / 2, (rec.y - windowH) / 2, windowW, windowH);

					frame.setSize(rec.width, rec.height);

					frame.setUndecorated(true);
					frame.setBackground(new Color(0, 0, 0, 0));
					TranslucentPane contentPane = new TranslucentPane(frame);
					frame.setContentPane(contentPane);
					frame.setLocationRelativeTo(null);

					frame.setVisible(true);
					frame.toFront();

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
	}

	public void setImagePercent(int componentW, int componentH) {
		imageW = srcImageW = srcImage.getWidth(null);
		imageH = srcImageH = srcImage.getHeight(null);

		if (srcImageH > componentH || srcImageW > componentW) {
			if (srcImageW / srcImageH > componentW / componentH) {
				imageW = componentW;
				imageH = srcImageH * componentW / srcImageW;
			} else {
				imageH = componentH;
				imageW = srcImageW * componentH / srcImageH;
			}
		}
		current = (double) imageH / srcImageH;
	}

	public class TranslucentPane extends JPanel {

		public TranslucentPane(final JFrame frame) {
			setOpaque(false);//不能删除。跟paintComponent有关
			setLayout(null);

			MyMouseListener l = new MyMouseListener(frame);
			addMouseListener(l);
			addMouseWheelListener(l);
			addMouseMotionListener(l);

			//			xx();

			deleteButton = new McWillIconButton(deleteImages);
			deleteButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					SwingUtilities.getWindowAncestor(deleteButton).dispose();
				}
			});
			buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 4, 0));
			buttonPanel.setOpaque(false);
			//			buttonPanel.setBorder(BorderFactory.createEtchedBorder());

			Dimension dimension = new Dimension(35, 35);
			McWillIconButton srcButton = new McWillIconButton(_1_1Image);
			srcButton.setPreferredSize(dimension);
			srcButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					current = 1;
					yy();
					repaint();
				}
			});

			final McWillIconButton screenButton = new McWillIconButton(screenfullImage);
			screenButton.setPreferredSize(dimension);
			screenButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (screenBound != null) {
						areaRec = new Rectangle(screenBound);
						screenBound = null;
						deleteButton.updateIcons(deleteImages);
						screenButton.updateIcons(new Icon[] { screenfullImage });
					} else {
						screenBound = new Rectangle(areaRec);

						screenButton.updateIcons(new Icon[] { screenrestoreImage });
						deleteButton.updateIcons(deleteBigImages);

						final Rectangle screen = SwingUtil.getScreenRectangle(frame);
						screen.width += BORDER;
						screen.height += BORDER + 5;
						screen.y -= BORDER;
						
						areaRec = new Rectangle(screen);
					}
					

				}
			});

			McWillIconButton zoomInButton = new McWillIconButton(zoominImage);
			zoomInButton.setPreferredSize(dimension);
			zoomInButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					current += step;
					if (current > limitMax) {
						current = limitMax;
					}
					yy();

					repaint();
				}
			});

			McWillIconButton zoomOutButton = new McWillIconButton(zoomoutImage);
			zoomOutButton.setPreferredSize(dimension);
			zoomOutButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					current -= step;
					if (current < limitMin) {
						current = limitMin;
					}
					yy();
					repaint();
				}
			});

			McWillIconButton rotateButton = new McWillIconButton(rotateImage);
			rotateButton.setPreferredSize(dimension);

			rotateButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					AffineTransform tx = new AffineTransform();
					tx.translate(srcImage.getHeight() / 2, srcImage.getWidth() / 2);
					tx.rotate(Math.PI / 2);
					tx.translate(-srcImage.getWidth() / 2, -srcImage.getHeight() / 2);

					AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
					srcImage = op.filter(srcImage, null);
					int componentW = areaRec.width;
					int componentH = areaRec.height;
					setImagePercent(componentW, componentH);
					repaint();
				}
			});

			McWillIconButton saveButton = new McWillIconButton(saveImage);
			saveButton.setPreferredSize(dimension);
			saveButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					SwingUtil.saveImageFile(frame, srcImage);
				}
			});

			buttonPanel.add(srcButton);
			buttonPanel.add(screenButton);
			buttonPanel.add(zoomInButton);
			buttonPanel.add(zoomOutButton);
			buttonPanel.add(rotateButton);
			buttonPanel.add(saveButton);

			add(deleteButton);
			add(buttonPanel);

		}

		public void yy() {
			imageW = Double.valueOf(srcImageW * current).intValue();
			imageH = Double.valueOf(srcImageH * current).intValue();
		}

		@Override
		protected void paintComponent(Graphics g) {
			Graphics2D g2d = (Graphics2D) g.create();
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

			int width = getWidth();
			int height = getHeight();

			g2d.setComposite(AlphaComposite.SrcOver.derive(0.0f));
			g2d.setColor(getBackground());
			g2d.fillRect(0, 0, width, height);

			g2d.setComposite(AlphaComposite.SrcOver.derive(0.8f));
			g2d.setColor(Color.BLACK);
			g2d.fillRoundRect(0, BORDER, width - BORDER, height - BORDER, 10, 10);

			paintImage(g2d);

			int componentW = getWidth() - BORDER;
			int componentH = getHeight() - BORDER;

			g2d.setComposite(AlphaComposite.SrcOver.derive(0.5f));
			g2d.setColor(Color.BLACK);
			g2d.fillRoundRect((componentW - 50) / 2, (componentH - 25) / 2, 50, 25, 10, 10);
			g2d.setColor(Color.WHITE);
			String str = Double.valueOf(current * 100).intValue() + "%";
			FontMetrics fontMetrics = g2d.getFontMetrics(LAF.getFont(14, true));
			Rectangle2D stringBounds = fontMetrics.getStringBounds(str, g2d);
			g2d.drawString(str, (componentW - (int) stringBounds.getWidth()) / 2 + 3, (componentH + (int) stringBounds.getHeight()) / 2 - 3);

			paintDeleteImage(g2d);

			paintButtonImage(g2d);

			g2d.dispose();

		}

		protected void paintImage(Graphics2D g2d) {
			if (imageW <= 0 || imageH <= 0)
				return;

			int componentW = getWidth() - BORDER;
			int componentH = getHeight() - BORDER;
			int y = 0;
			int x = 0;
			int w = imageW;
			int h = imageH;

			Image newSrcImage = createImage(imageW, imageH);
			Graphics newG = newSrcImage.getGraphics();
			newG.drawImage(srcImage, 0, 0, imageW, imageH, this);
			newG.dispose();

			Image destImage = newSrcImage;

			if (imageH > componentH || imageW > componentW) {
				if (imageW > componentW) {
					x = (imageW - componentW) / 2;
					w = componentW;
				}
				if (imageH > componentH) {
					y = (imageH - componentH) / 2;
					h = componentH;
				}
				destImage = ImageUtil.getSubimage(newSrcImage, x, y, w, h);
			}
			g2d.drawImage(destImage, (componentW - w) / 2, (componentH - h) / 2 + BORDER, w, h, this);

		}

		protected void paintDeleteImage(Graphics2D g2d) {
			g2d.setComposite(AlphaComposite.SrcOver.derive(1f));
			g2d.setColor(Color.WHITE);
			int xx = 2;
			if (screenBound != null) {
				deleteButton.setBounds(getWidth() - BORDER * 4, 0, BORDER * 4, BORDER * 4 + 2);
				g2d.fillOval(getWidth() - BORDER * 4 + xx, xx, BORDER * 4 - xx * 2, BORDER * 4 - xx * 2);
			} else {
				deleteButton.setBounds(getWidth() - BORDER * 2, 0, BORDER * 2, BORDER * 2 + 2);
				g2d.fillOval(getWidth() - BORDER * 2 + xx, xx, BORDER * 2 - xx * 2, BORDER * 2 - xx * 2);
			}
			System.out.println("xxxxxxxxxxx:" + getWidth());
			//			
		}

		protected void paintButtonImage(Graphics2D g2d) {
			g2d.setColor(Color.BLACK);
			g2d.setComposite(AlphaComposite.SrcOver.derive(0.3f));
			g2d.fillRoundRect((getWidth() - 250) / 2, getHeight() - 35, 250, 45, 15, 15);

			buttonPanel.setBounds((getWidth() - 250) / 2, getHeight() - 35, 250, 35);

		}

		class MyMouseListener extends MouseAdapter {
			private Boolean isDragged;
			private Point loc, tmp;

			private Window window;
			private int i = 0;

			public MyMouseListener(JFrame frame) {
				this.window = frame;
			}

			//拖动窗体
			public void mouseReleased(MouseEvent e) {
				isDragged = false;
			}

			public void mousePressed(MouseEvent e) {
				tmp = new Point(e.getX(), e.getY());//获取窗体位置
				isDragged = true;
			}

			public void mouseDragged(MouseEvent e) {
				if (screenBound != null) {
					return;
				}

				if (isDragged) {
					{// 通过降低重绘频率，解决移动出现闪动
						if (i++ % 8 != 0) {
							return;
						}
						if (i > 100000) {
							i = 0;
						}
					}

					loc = new Point(window.getLocation().x + e.getX() - tmp.x, window.getLocation().y + e.getY() - tmp.y);
					window.setLocation(loc);
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					SwingUtilities.getWindowAncestor(e.getComponent()).dispose();
				}
			}

			public void mouseWheelMoved(MouseWheelEvent e) {
				int wheelRotation = e.getWheelRotation();
				if (wheelRotation == 1) { //滑轮向后
					current -= step;
					if (current < limitMin) {
						current = limitMin;
					}
				} else if (e.getWheelRotation() == -1) {//滑轮向前
					current += step;
					if (current > limitMax) {
						current = limitMax;
					}
				} else {
				}

				yy();

				repaint();
			}
		}

		public boolean isInOval(int x, int y, int rx, int ry, int r) {
			return Math.pow(rx - x, 2) + Math.pow(ry - y, 2) < Math.pow(r, 2);
		}
		//	/** 获取屏幕的边界 */
		//			Insets screenInsets = defaultToolkit.getScreenInsets(getGraphicsConfiguration());
		//			/** 获取屏幕大小 */
		//			Dimension screenSize = defaultToolkit.getScreenSize();
		//			if (screenInsets.bottom > 0) {
		//				screenSize.height -= screenInsets.bottom;
		//			} else if (screenInsets.right > 0) {
		//				screenSize.width -= screenInsets.right;
		//			} else if (screenInsets.left > 0) {
		//				screenSize.width -= screenInsets.left;
		//			} else if (screenInsets.top > 0) {
		//				screenSize.height -= screenInsets.top;
		//			}
	}

	enum States {
		NORTH_WEST(new Cursor(Cursor.NW_RESIZE_CURSOR)), NORTH(new Cursor(Cursor.N_RESIZE_CURSOR)), NORTH_EAST(new Cursor(Cursor.NE_RESIZE_CURSOR)), EAST(
				new Cursor(Cursor.E_RESIZE_CURSOR)), SOUTH_EAST(new Cursor(Cursor.SE_RESIZE_CURSOR)), SOUTH(new Cursor(Cursor.S_RESIZE_CURSOR)), SOUTH_WEST(new Cursor(Cursor.SW_RESIZE_CURSOR)), WEST(
						new Cursor(Cursor.W_RESIZE_CURSOR)), SELECT(new Cursor(Cursor.MOVE_CURSOR)), DEFAULT(new Cursor(Cursor.DEFAULT_CURSOR)), CUSTOM(
								Toolkit.getDefaultToolkit().createCustomCursor(LAF.getCursorImage(), new Point(0, 0), "ColorPicker"));
		private Cursor cursor;

		States(Cursor cursor) {
			this.cursor = cursor;
		}

		public Cursor getCursor() {
			return cursor;
		}
	}
}