package swing.window.x;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.xinwei.common.lang.SystemUtil;

public class GCFrame {
	BufferedImage image = new BufferedImage(500, 500, BufferedImage.TYPE_INT_RGB);
	Graphics g = image.getGraphics();
	private DrawCanvas canvas = new DrawCanvas();
	private int preX = -1;
	private int preY = -1;
	private Image offScreenImage; //图形缓存

	public void init() {
		g.fillRect(0, 0, 1600, 1000);
		final JFrame frame = new JFrame("测试画出鼠标的轨迹");
		frame.setSize(500, 400);

		frame.add(canvas);
		frame.setLayout(null);
		canvas.setBounds(0, 0, 500, 500);

		//				frame.setFocusable(false);
		//		frame.setFocusableWindowState(false);

		frame.setVisible(true);

		canvas.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (preX > 0 && preY > 0) {
					g.setColor(Color.black);
					g.drawLine(preX, preY, e.getX(), e.getY());
				}
				preX = e.getX();
				preY = e.getY();
				canvas.repaint();
			}
		});

		frame.addWindowListener(new WindowAdapter()//添加窗口关闭处理函数
		{
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		//		new TranscluentWindow();
		SystemUtil.execute(new Runnable() {
			int i = 0;

			@Override
			public void run() {
				while (true) {
					frame.setExtendedState(Frame.ICONIFIED);
					//					SwingUtilities.invokeLater(new Runnable() {
					//						@Override
					//						public void run() {
					//							frame.setExtendedState(Frame.NORMAL);
					//						}
					//					});
					if (i++ % 2 == 0) {
					} else {
					}

					frame.setExtendedState(Frame.NORMAL);
					SystemUtil.sleep(5000);
				}

			}
		});
	}

	public static void main(String[] args) {
		GCFrame dc = new GCFrame();
		dc.init();

	}

	class DrawCanvas extends Canvas {
		private static final long serialVersionUID = 1L;

		@Override
		public void paint(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			g2.drawImage(image, 0, 0, null);
		}

		@Override
		public void update(Graphics g) {
			if (offScreenImage == null)
				offScreenImage = this.createImage(500, 500); //新建一个图像缓存空间,这里图像大小为800*600
			Graphics gImage = offScreenImage.getGraphics(); //把它的画笔拿过来,给gImage保存着
			paint(gImage); //将要画的东西画到图像缓存空间去
			g.drawImage(offScreenImage, 0, 0, null); //然后一次性显示出来
		}
	}
}
