package swing.window.x;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lang.SystemUtil;

/**
 *
 */
public class TranscluentWindow {

	public static void main(String[] args) {
		new TranscluentWindow(null);
	}

	public TranscluentWindow(final JFrame f) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {

					final JFrame frame = new JFrame();
					//					frame.setUndecorated(false);
					frame.setType(JFrame.Type.UTILITY);//隐藏任务栏图标  
					//					frame.setBackground(new Color(0, 0, 0, 0));
					//					frame.setFocusable(false);
					//					frame.setFocusableWindowState(false);

					frame.setAlwaysOnTop(true);
					//					frame.setBackground(new Color(0, 0, 0, 0));
					//					frame.setContentPane(new TranslucentPane());
					frame.setSize(500, 400);
					//					frame.add(new JLabel(new ImageIcon(ImageIO.read(CustomPanel.class.getResource("QQ图片20170214133847.jpg")))));
					//					frame.pack();
					frame.setLocationRelativeTo(f);
					frame.setVisible(true);

					SystemUtil.execute(new Runnable() {
						int i = 0;

						@Override
						public void run() {
							while (true) {
								frame.setVisible(false);
								frame.setExtendedState(Frame.ICONIFIED);
//								frame.setVisible(true);
								SwingUtilities.invokeLater(new Runnable() {
									@Override
									public void run() {
										frame.setExtendedState(Frame.NORMAL);
										frame.setVisible(true);
									}
								});
								if (i++ % 2 == 0) {
								} else {
								}
								SystemUtil.sleep(3000);
							}

						}
					});
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
	}

	public class TranslucentPane extends JPanel {

		public TranslucentPane() {
			setOpaque(false);
		}

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);

			Graphics2D g2d = (Graphics2D) g.create();
			g2d.setComposite(AlphaComposite.SrcOver.derive(0.0f));
			g2d.setColor(getBackground());
			g2d.fillRect(0, 0, getWidth(), getHeight());

		}

	}

}