
import javax.swing.JFrame;

import com.xinwei.common.lang.SystemUtil;

public class JFrameTest {
	private JFrame frame = null;

	public JFrameTest() {
		frame = new JFrame("Test");
		frame.setType(JFrame.Type.UTILITY);//隐藏任务栏图标  
		frame.setSize(100, 100);
		frame.setLocation(-500, -500);
		frame.setVisible(true);

		new Thread() {
			public void run() {
				while (true) {
					SystemUtil.sleep(1000);
					frame.setVisible(false);
//					frame.setState(JFrame.ICONIFIED);
										frame.setVisible(true);
					SystemUtil.sleep(1000);
				}
			};
		}.start();
	}

	public static void main(String[] args) {
		new JFrameTest();

	}
}