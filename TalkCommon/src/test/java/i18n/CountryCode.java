/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月16日 下午3:11:59
 * 
 ***************************************************************/
package i18n;

import java.io.File;

import com.xinwei.common.resource.I18nConfigurator;

public class CountryCode extends I18nConfigurator {
	private static void addI18n() throws Exception {
		addI18n("countrycode.244", "Angola", "安哥拉");
		addI18n("countrycode.93", "Afghanistan", "阿富汗");
		addI18n("countrycode.355", "Albania", "阿尔巴尼亚");
		addI18n("countrycode.213", "Algeria", "阿尔及利亚");
		addI18n("countrycode.376", "Andorra", "安道尔共和国");
		addI18n("countrycode.1264", "Anguilla", "安圭拉岛");
		addI18n("countrycode.1268", "Antigua and Barbuda", "安提瓜和巴布达");
		addI18n("countrycode.54", "Argentina", "阿根廷");
		addI18n("countrycode.374", "Armenia", "亚美尼亚");
		addI18n("countrycode.247", "Ascension", "阿森松");
		addI18n("countrycode.61", "Australia", "澳大利亚");
		addI18n("countrycode.43", "Austria", "奥地利");
		addI18n("countrycode.994", "Azerbaijan", "阿塞拜疆");
		addI18n("countrycode.1242", "Bahamas", "巴哈马");
		addI18n("countrycode.973", "Bahrain", "巴林");
		addI18n("countrycode.880", "Bangladesh", "孟加拉国");
		addI18n("countrycode.1246", "Barbados", "巴巴多斯");
		addI18n("countrycode.375", "Belarus", "白俄罗斯");
		addI18n("countrycode.32", "Belgium", "比利时");
		addI18n("countrycode.501", "Belize", "伯利兹");
		addI18n("countrycode.229", "Benin", "贝宁");
		addI18n("countrycode.1441", "BermudaIs.", "百慕大群岛");
		addI18n("countrycode.591", "Bolivia", "玻利维亚");
		addI18n("countrycode.267", "Botswana", "博茨瓦纳");
		addI18n("countrycode.55", "Brazil", "巴西");
		addI18n("countrycode.673", "Brunei", "文莱");
		addI18n("countrycode.359", "Bulgaria", "保加利亚");
		addI18n("countrycode.226", "Burkina-faso", "布基纳法索");
		addI18n("countrycode.95", "Burma", "缅甸");
		addI18n("countrycode.257", "Burundi", "布隆迪");
		addI18n("countrycode.237", "Cameroon", "喀麦隆");
		addI18n("countrycode.1", "Canada", "加拿大");
		addI18n("countrycode.1345", "Cayman Is.", "开曼群岛");
		addI18n("countrycode.236", "Central African Republic", "中非共和国");
		addI18n("countrycode.235", "Chad", "乍得");
		addI18n("countrycode.56", "Chile", "智利");
		addI18n("countrycode.86", "China", "中国");
		addI18n("countrycode.57", "Colombia", "哥伦比亚");
		addI18n("countrycode.242", "Congo", "刚果");
		addI18n("countrycode.682", "Cook Is.", "库克群岛");
		addI18n("countrycode.506", "Costa Rica", "哥斯达黎加");
		addI18n("countrycode.53", "Cuba", "古巴");
		addI18n("countrycode.357", "Cyprus", "塞浦路斯");
		addI18n("countrycode.420", "Czech Republic", "捷克");
		addI18n("countrycode.45", "Denmark", "丹麦");
		addI18n("countrycode.253", "Djibouti", "吉布提");
		addI18n("countrycode.1890", "Dominica Rep.", "多米尼加共和国");
		addI18n("countrycode.593", "Ecuador", "厄瓜多尔");
		addI18n("countrycode.20", "Egypt", "埃及");
		addI18n("countrycode.503", "EISalvador", "萨尔瓦多");
		addI18n("countrycode.372", "Estonia", "爱沙尼亚");
		addI18n("countrycode.251", "Ethiopia", "埃塞俄比亚");
		addI18n("countrycode.679", "Fiji", "斐济");
		addI18n("countrycode.358", "Finland", "芬兰");
		addI18n("countrycode.33", "France", "法国");
		addI18n("countrycode.594", "French Guiana", "法属圭亚那");
		addI18n("countrycode.241", "Gabon", "加蓬");
		addI18n("countrycode.220", "Gambia", "冈比亚");
		addI18n("countrycode.995", "Georgia", "格鲁吉亚");
		addI18n("countrycode.49", "Germany", "德国");
		addI18n("countrycode.233", "Ghana", "加纳");
		addI18n("countrycode.350", "Gibraltar", "直布罗陀");
		addI18n("countrycode.30", "Greece", "希腊");
		addI18n("countrycode.1809", "Grenada", "格林纳达");
		addI18n("countrycode.1671", "Guam", "关岛");
		addI18n("countrycode.502", "Guatemala", "危地马拉");
		addI18n("countrycode.224", "Guinea", "几内亚");
		addI18n("countrycode.592", "Guyana", "圭亚那");
		addI18n("countrycode.509", "Haiti", "海地");
		addI18n("countrycode.504", "Honduras", "洪都拉斯");
		addI18n("countrycode.852", "Hongkong", "香港");
		addI18n("countrycode.36", "Hungary", "匈牙利");
		addI18n("countrycode.354", "Iceland", "冰岛");
		addI18n("countrycode.91", "India", "印度");
		addI18n("countrycode.62", "Indonesia", "印度尼西亚");
		addI18n("countrycode.98", "Iran", "伊朗");
		addI18n("countrycode.964", "Iraq", "伊拉克");
		addI18n("countrycode.353", "Ireland", "爱尔兰");
		addI18n("countrycode.972", "Israel", "以色列");
		addI18n("countrycode.39", "Italy", "意大利");
		addI18n("countrycode.225", "IvoryCoast", "科特迪瓦");
		addI18n("countrycode.1876", "Jamaica", "牙买加");
		addI18n("countrycode.81", "Japan", "日本");
		addI18n("countrycode.962", "Jordan", "约旦");
		addI18n("countrycode.855", "Cambodia", "柬埔寨");
		addI18n("countrycode.327", "Kazakstan", "哈萨克斯坦");
		addI18n("countrycode.254", "Kenya", "肯尼亚");
		addI18n("countrycode.82", "Korea", "韩国");
		addI18n("countrycode.965", "Kuwait", "科威特");
		addI18n("countrycode.331", "Kyrgyzstan", "吉尔吉斯坦");
		addI18n("countrycode.856", "Laos", "老挝");
		addI18n("countrycode.371", "Latvia", "拉脱维亚");
		addI18n("countrycode.961", "Lebanon", "黎巴嫩");
		addI18n("countrycode.266", "Lesotho", "莱索托");
		addI18n("countrycode.231", "Liberia", "利比里亚");
		addI18n("countrycode.218", "Libya", "利比亚");
		addI18n("countrycode.423", "Liechtenstein", "列支敦士登");
		addI18n("countrycode.370", "Lithuania", "立陶宛");
		addI18n("countrycode.352", "Luxembourg", "卢森堡");
		addI18n("countrycode.853", "Macao", "澳门");
		addI18n("countrycode.261", "Madagascar", "马达加斯加");
		addI18n("countrycode.265", "Malawi", "马拉维");
		addI18n("countrycode.60", "Malaysia", "马来西亚");
		addI18n("countrycode.960", "Maldives", "马尔代夫");
		addI18n("countrycode.223", "Mali", "马里");
		addI18n("countrycode.356", "Malta", "马耳他");
		addI18n("countrycode.1670", "Mariana Is", "马里亚那群岛");
		addI18n("countrycode.596", "Martinique", "马提尼克");
		addI18n("countrycode.230", "Mauritius", "毛里求斯");
		addI18n("countrycode.52", "Mexico", "墨西哥");
		addI18n("countrycode.373", "Moldova, Republic of", "摩尔多瓦");
		addI18n("countrycode.377", "Monaco", "摩纳哥");
		addI18n("countrycode.976", "Mongolia", "蒙古");
		addI18n("countrycode.1664", "Montserrat Is", "蒙特塞拉特岛");
		addI18n("countrycode.212", "Morocco", "摩洛哥");
		addI18n("countrycode.258", "Mozambique", "莫桑比克");
		addI18n("countrycode.264", "Namibia", "纳米比亚");
		addI18n("countrycode.674", "Nauru", "瑙鲁");
		addI18n("countrycode.977", "Nepal", "尼泊尔");
		addI18n("countrycode.599", "Netheriands Antilles", "荷属安的列斯");
		addI18n("countrycode.31", "Netherlands", "荷兰");
		addI18n("countrycode.64", "NewZealand", "新西兰");
		addI18n("countrycode.505", "Nicaragua", "尼加拉瓜");
		addI18n("countrycode.227", "Niger", "尼日尔");
		addI18n("countrycode.234", "Nigeria", "尼日利亚");
		addI18n("countrycode.850", "North Korea", "朝鲜");
		addI18n("countrycode.47", "Norway", "挪威");
		addI18n("countrycode.968", "Oman", "阿曼");
		addI18n("countrycode.92", "Pakistan", "巴基斯坦");
		addI18n("countrycode.507", "Panama", "巴拿马");
		addI18n("countrycode.675", "Papua New Cuinea", "巴布亚新几内亚");
		addI18n("countrycode.595", "Paraguay", "巴拉圭");
		addI18n("countrycode.51", "Peru", "秘鲁");
		addI18n("countrycode.63", "Philippines", "菲律宾");
		addI18n("countrycode.48", "Poland", "波兰");
		addI18n("countrycode.689", "French Polynesia", "法属玻利尼西亚");
		addI18n("countrycode.351", "Portugal", "葡萄牙");
		addI18n("countrycode.1787", "PuertoRico", "波多黎各");
		addI18n("countrycode.974", "Qatar", "卡塔尔");
		addI18n("countrycode.262", "Reunion", "留尼旺");
		addI18n("countrycode.40", "Romania", "罗马尼亚");
		addI18n("countrycode.7", "Russia", "俄罗斯");
		addI18n("countrycode.1758", "Saint Lueia", "圣卢西亚");
		addI18n("countrycode.1784", "Saint Vincent", "圣文森特岛");
		addI18n("countrycode.684", "Samoa Eastern", "东萨摩亚(美)");
		addI18n("countrycode.685", "Samoa Western", "西萨摩亚");
		addI18n("countrycode.378", "San Marino", "圣马力诺");
		addI18n("countrycode.239", "Sao Tome and Principe", "圣多美和普林西比");
		addI18n("countrycode.966", "Saudi Arabia", "沙特阿拉伯");
		addI18n("countrycode.221", "Senegal", "塞内加尔");
		addI18n("countrycode.248", "Seychelles", "塞舌尔");
		addI18n("countrycode.232", "Sierra Leone", "塞拉利昂");
		addI18n("countrycode.65", "Singapore", "新加坡");
		addI18n("countrycode.421", "Slovakia", "斯洛伐克");
		addI18n("countrycode.386", "Slovenia", "斯洛文尼亚");
		addI18n("countrycode.677", "Solomon Is", "所罗门群岛");
		addI18n("countrycode.252", "Somali", "索马里");
		addI18n("countrycode.27", "South Africa", "南非");
		addI18n("countrycode.34", "Spain", "西班牙");
		addI18n("countrycode.94", "Sri Lanka", "斯里兰卡");
		addI18n("countrycode.1758", "St.Lucia", "圣卢西亚");
		addI18n("countrycode.1784", "St.Vincent", "圣文森特");
		addI18n("countrycode.249", "Sudan", "苏丹");
		addI18n("countrycode.597", "Suriname", "苏里南");
		addI18n("countrycode.268", "Swaziland", "斯威士兰");
		addI18n("countrycode.46", "Sweden", "瑞典");
		addI18n("countrycode.41", "Switzerland", "瑞士");
		addI18n("countrycode.963", "Syriaw", "叙利亚");
		addI18n("countrycode.886", "Taiwan", "台湾省");
		addI18n("countrycode.992", "Tajikstan", "塔吉克斯坦");
		addI18n("countrycode.255", "Tanzania", "坦桑尼亚");
		addI18n("countrycode.66", "Thailand", "泰国");
		addI18n("countrycode.228", "Togo", "多哥");
		addI18n("countrycode.676", "Tonga", "汤加");
		addI18n("countrycode.1809", "Trinidad and Tobago", "特立尼达和多巴哥");
		addI18n("countrycode.216", "Tunisia", "突尼斯");
		addI18n("countrycode.90", "Turkey", "土耳其");
		addI18n("countrycode.993", "Turkmenistan", "土库曼斯坦");
		addI18n("countrycode.256", "Uganda", "乌干达");
		addI18n("countrycode.380", "Ukraine", "乌克兰");
		addI18n("countrycode.971", "United Arab Emirates", "阿拉伯联合酋长国");
		addI18n("countrycode.44", "United Kiongdom", "英国");
		addI18n("countrycode.1", "United States", "美国");
		addI18n("countrycode.598", "Uruguay", "乌拉圭");
		addI18n("countrycode.998", "Uzbekistan", "乌兹别克斯坦");
		addI18n("countrycode.58", "Venezuela", "委内瑞拉");
		addI18n("countrycode.84", "Vietnam", "越南");
		addI18n("countrycode.967", "Yemen", "也门");
		addI18n("countrycode.381", "Yugoslavia", "南斯拉夫");
		addI18n("countrycode.27", "South Africa", "南非");
		addI18n("countrycode.263", "Zimbabwe", "津巴布韦");
		addI18n("countrycode.243", "Zaire", "扎伊尔");
		addI18n("countrycode.260", "Zambia", "赞比亚");
		addI18n("countrycode.0", "Invalid Country Code", "国家代码无效");

	}

	public static void main(String[] args) {
		File f = new File("D:\\software\\camtalk\\TalkPC\\src\\main\\resources\\talk\\images\\country");
		File[] listFiles = f.listFiles();
		for (File fx : listFiles) {
			String name = fx.getName();
			String s = name.replace(".png", "");
			String[] ss = s.split("_");
			System.out.println("countrycode." + ss[ss.length - 1] + "=" + name);

		}
		System.out.println(listFiles);

		try {
			init("src/main/resources/talk/i18n/", "countrycode");
			clearI18n();
			addI18n();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
