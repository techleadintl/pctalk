package i18n;

import com.xinwei.common.resource.I18nConfigurator;

public class I18nTalk extends I18nConfigurator {
	private static void addI18n() throws Exception {
		addI18n("application.name", "Talk", "Talk");
		/****************************************** æ–‡ä»¶ä¼ é€� ,ç‰ˆæœ¬å�‡çº§ ***********************************************/
		addI18n("button.file.accept", "Accept", "æŽ¥å�—");
		addI18n("button.file.reject", "Reject", "æ‹’ç»�");

		addI18n("message.file.exists.question", "The file already exists, overwrite?", "æ–‡ä»¶å·²ç»�å­˜åœ¨ã€‚ é‡�å†™ï¼Ÿ");
		addI18n("message.file.size", "File Size: {0}", "æ–‡ä»¶å¤§å°�ï¼š {0}");
		addI18n("message.file.transfer.canceled", "You have canceled the file transfer", "ä½ å�–æ¶ˆäº†æ–‡ä»¶ä¼ è¾“ã€‚");
		addI18n("message.file.transfer.rejected", "The file transfer was not accepted by {0}", "æ–‡ä»¶ä¼ è¾“æœªè¢«{0}æŽ¥å�—ã€‚");
		addI18n("message.file.transfer.dirnull", "Download directory for file transfer is not valid", "æ–‡ä»¶ä¼ è¾“ç”¨çš„ä¸‹è½½æ–‡ä»¶å¤¹ä¸�å�¯ç”¨");
		addI18n("message.file.transfer.nodir", "Download directory for file transfer does not exist", "æ–‡ä»¶ä¼ è¾“ç”¨çš„ä¸‹è½½æ–‡ä»¶å¤¹ä¸�å­˜åœ¨");
		addI18n("message.file.transfer.cantwritedir", "Can't write to the directory for file transfer", "æ–‡ä»¶ä¼ è¾“æ— æ³•å†™å…¥æ–‡ä»¶å¤¹");
		addI18n("message.file.transfer.notification", "File transfer notification", "æ–‡ä»¶ä¼ è¾“æ��ç¤º");
		addI18n("message.file.transfer.short.message", "is sending you a file called:", "æœ‰æ–‡ä»¶ä¼ è¾“å‘¼å…¥");
		addI18n("message.file.transfer.chat.window", "File transfer request:", "æ–‡ä»¶ä¼ è¾“è¯·æ±‚");
		addI18n("message.file.transfer.file.too.big.error", "The selected file is too big.\nThe maximum filezise is {0} the selected file has {1}", "é€‰ä¸­çš„æ–‡ä»¶å¤ªå¤§äº†ã€‚\næœ€å¤§ä¸�è¶…è¿‡{0}ï¼Œä½†æ˜¯é€‰ä¸­çš„æ–‡ä»¶å¤§å°�ä¸º{1");
		addI18n("message.file.transfer.file.too.big.warning", "The selected file is too big.\nProceed anyway?", "é€‰ä¸­çš„æ–‡ä»¶å¤ªå¤§äº†ã€‚\nç»§ç»­ï¼Ÿ");
		addI18n("message.file.transfer.direrror.setdir", "Click here to change the directory", "ç‚¹å‡»æ­¤å¤„æ”¹å�˜æ–‡ä»¶å¤¹");

		addI18n("label.file.transfer.download.directory", "&Download directory:", "(&D) ä¸‹è½½ç›®å½•ï¼š");
		addI18n("label.file.transfer.time", "Time: {0}", "æ—¶é—´ï¼š {0}");

		addI18n("message.file.transfer.progressbar.text.received", "{0} received   @ {1}    {2}", "{0} æŽ¥æ”¶   @ {1}    {2}");
		addI18n("message.file.transfer.receiving", "You are receiving a file from {0}", "ä½ æ­£åœ¨ä»Ž{0}æŽ¥æ”¶ä¸€ä¸ªæ–‡ä»¶");
		addI18n("message.file.transfer.negotiate.stream", "Negotiating connection stream, please wait...", "è°ˆåˆ¤å¯¹è¯�å±•å¼€ã€‚ è¯·ç­‰å¾…");

		addI18n("message.file.click.to.open", "Click to open", "ç‚¹å‡»ä»¥æ‰“å¼€");
		addI18n("message.user.is.sending.you.a.file", "{0} is sending you a file", "{0}æ­£åœ¨å�‘ä½ å�‘é€�ä¸€ä¸ªæ–‡ä»¶ã€‚");

		/****************************************** æ‰˜ç›˜å�Šæ¶ˆæ�¯æ��ç¤º ***********************************************/
		addI18n("menuitem.tray.open", "Open", "æ‰“å¼€");
		addI18n("menuitem.tray.hide", "Hide", "éš�è—�");
		addI18n("menuitem.tray.exit", "Exit", "é€€å‡º");
		addI18n("menuitem.tray.status", "Status", "çŠ¶æ€�");
		addI18n("menuitem.tray.relogin", "LogOut", "ç™»å½•");

		addI18n("tray.message.file", "Received file,please open chat room", "å�‘æ‚¨å�‘é€�äº†æ–‡ä»¶ï¼Œè¯·æ‰“å¼€ä¼šè¯�çª—å�£ã€‚");
		addI18n("tray.message.image", "[Image]", "[å›¾ç‰‡]");
		addI18n("tray.message.location", "[Location]", "[ä½�ç½®]");
		addI18n("tray.message.voicemail", "[Voice Mail]", "[è¯­éŸ³ä¿¡ç®±]");
		addI18n("tray.message.namecard", "[Name Card]", "[å��ç‰‡]");
		addI18n("tray.message.emoticon", "[Emoticon]", "[è¡¨æƒ…]");
		addI18n("tray.button.openflash", "Open avatar flash", "å¼€å�¯å¤´åƒ�é—ªåŠ¨");
		addI18n("tray.button.stopflash", "Stop avatar flash", "å…³é—­å¤´åƒ�é—ªåŠ¨");
		addI18n("tray.button.ignoreall", "Ignore all ", "å…¨éƒ¨å¿½ç•¥");
		addI18n("tray.button.openall", "Open all ", "å…¨éƒ¨æŸ¥çœ‹");
		/****************************************** ç™»å½•ç•Œé�¢ ***********************************************/
		addI18n("button.login", "Login", "ç™»    å½•");

		addI18n("tip.login.user.name", "Telephone", "ç”µè¯�å�·ç �");
		addI18n("tip.login.user.password", "Password", "ç™»å½•å¯†ç �");

		addI18n("tip.login.user.softkey", "Open Soft KeyBoard", "æ‰“å¼€è½¯é”®ç›˜");
		addI18n("label.login.save.password", "Save Password", "è®°ä½�å¯†ç �");
		addI18n("label.login.auto.login", "Auto Login", "è‡ªåŠ¨ç™»å½•");
		addI18n("label.login.register", "Register >>", "æ³¨å†Œè´¦å�· >>");
		addI18n("label.login.forget.password", "Forget Passwod", "å¿˜è®°å¯†ç �");

		addI18n("menuitem.hide", "Hide", "éš�èº«");
		addI18n("menuitem.online", "Online", "åœ¨çº¿");
		addI18n("menuitem.offline", "Offline", "ç¦»çº¿");
		addI18n("menuitem.pending", "Pending", "æŒ‚æœº");
		addI18n("menuitem.away", "Away", "ç¦»å¼€");
		addI18n("menuitem.busy", "Busy", "å¿™ç¢Œ");
		addI18n("menuitem.notdisturb", " Don't disturb", "å‹¿æ‰°");

		addI18n("tabTitle.login.error", "Login error", "ç™»å½•å‡ºé”™");

		addI18n("message.login.invalid.username.password", "Invalid username or password", "è´¦å�·æˆ–å¯†ç �é”™è¯¯ã€‚");
		addI18n("message.login.server.unavailable", "Can't connect to server: invalid name or server not reachable", "æ— æ³•è¿žæŽ¥åˆ°æœ�åŠ¡å™¨ã€‚");
		addI18n("message.login.conflict.error", "Unable to login due to account already signed in", "è´¦å�·å·²ç™»å½•ï¼Œæ— æ³•é‡�å¤�ç™»å½•ã€‚");
		addI18n("message.login.unrecoverable.error", "Invalid username or password", "é”™è¯¯çš„è´¦å�·æˆ–å¯†ç �ã€‚");
		addI18n("message.login.relogin.error", "You are already signed in {0} and cannot be signed in.", "æ‚¨å·²ç™»å½•{0}ï¼Œä¸�èƒ½é‡�å¤�ç™»å½•ã€‚");

		addI18n("button.relogin", "Relogin", "é‡�æ–°ç™»å½•");
		addI18n("dialog.title.offline", "Off line notification", "ä¸‹çº¿é€šçŸ¥");
		addI18n("label.relogin.info",
				"Your account is logged in at another location. <br/><br/> if this is not your own operation, then your password is likely to have leaked. <br/> recommends that you change your password or emergency freeze Talk.",
				"æ‚¨çš„è´¦å�·åœ¨å�¦ä¸€åœ°ç‚¹ç™»å½•ï¼Œæ‚¨è¢«è¿«ä¸‹çº¿ã€‚<br/><br/>å¦‚æžœè¿™ä¸�æ˜¯æ‚¨æœ¬äººçš„æ“�ä½œï¼Œé‚£ä¹ˆæ‚¨çš„å¯†ç �å¾ˆå�¯èƒ½å·²ç»�æ³„éœ²ã€‚<br/>å»ºè®®æ‚¨ä¿®æ”¹å¯†ç �æˆ–ç´§æ€¥å†»ç»“Talkã€‚");

		////////////////////////////////////////
		addI18n("tabTitle.main.status.message", "Status message", "çŠ¶æ€�æ¶ˆæ�¯");
		addI18n("message.main.current.status", "Let others know your current isInputing or activity", "å�Œæ„�å…¶ä»–äººçŸ¥é�“ä½ çš„çŠ¶æ€�æˆ–æ´»åŠ¨");

		addI18n("avatar.change", "Change avatar", "æ›´æ”¹å¤´åƒ�");
		addI18n("avatar.recommend", "Recommended avatar(total:{0})", "æŽ¨è��å¤´åƒ�(å…±{0}ä¸ª)");
		addI18n("avatar.game", "Game avatar(total:{0})", "æ¸¸æˆ�å¤´åƒ�(å…±{0}ä¸ª)");

		addI18n("avatar.classics", "Classics", "ç»�å…¸å¤´åƒ�");
		addI18n("avatar.personality", "Personality", "ä¸ªæ€§å¤´åƒ�");
		addI18n("avatar.preview", "Preview", "é¢„è§ˆ");
		addI18n("avatar.current", "Useing", "å½“å‰�");
		addI18n("avatar.button.custom.avatar", "Personality", "é€‰æ‹©ä¸ªæ€§å¤´åƒ�");

		addI18n("input.text.search", "Search:contact,group...", "æ�œç´¢ï¼šè�”ç³»äººï¼Œç¾¤ç»„...");
		addI18n("input.text.filter.telandname", "filter by username or tel", "é€šè¿‡ç”¨æˆ·å��æˆ–ç”µè¯�å�·ç �è¿‡æ»¤");

		addI18n("main.close.title", "Close", "å…³é—­æ��ç¤º");
		addI18n("main.close.message", "You clicked the close button, you want to:", "æ‚¨ç‚¹å‡»äº†å…³é—­æŒ‰é’®,æ‚¨æ˜¯æƒ³:");
		addI18n("main.close.option1", "Minimize to system tray, do not exit talk", "æœ€å°�åŒ–åˆ°ç³»ç»Ÿæ‰˜ç›˜ï¼Œä¸�é€€å‡ºç¨‹åº�");
		addI18n("main.close.option2", "Exit talk", "é€€å‡ºç¨‹åº�");
		addI18n("main.close.session", "You have a session window not closed,exit?", "æ‚¨æœ‰ä¼šè¯�çª—å�£æœªå…³é—­ï¼Œç¡®è®¤è¦�é€€å‡ºï¼Ÿ");

		addI18n("custom.talk.nick.group.message", "Group Message", "ç¾¤æ¶ˆæ�¯");
		addI18n("custom.talk.nick.system.message", "System Message", "ç³»ç»Ÿæ¶ˆæ�¯");
		addI18n("custom.talk.nick.phone", "My Phone", "æˆ‘çš„æ‰‹æœº");

		addI18n("contact", "Contacts", "å¥½å�‹");
		addI18n("group", "Groups", "ç¾¤ç»„");

		addI18n("menu.contact", "Contact", "å¥½å�‹");

		//---------liyong
		addI18n("message.contact.search.result", "No result in local", "æ— æœ¬åœ°æ�œç´¢ç»“æžœ...");
		addI18n("tab.contact", "Contact", "å¥½å�‹");
		addI18n("menu.contact.add", "Contact Add", "æ·»åŠ å¥½å�‹");
		/****************************************** å��ç‰‡ ***********************************************/
		addI18n("dialog.title.namecard.talk", "Talk Info", "ä¸ªäººä¿¡æ�¯");
		addI18n("namecard.title", "Name Card", "å��ç‰‡");
		addI18n("namecard.tel", "Tel", "ç”µè¯�");
		addI18n("namecard.display", "Display", "å��ç§°");
		addI18n("namecard.nick", "Nick", "æ˜µç§°");
		addI18n("namecard.emaill", "Emaill", "ç”µå­�é‚®ç®±");
		addI18n("namecard.addr", "Address", "åœ°å�€");
		addI18n("namecard.birthday", "Birthday", "ç”Ÿæ—¥");
		addI18n("namecard.company", "Company", "å…¬å�¸");

		addI18n("namecard.middlename", "Middle Name", "ä¸­é—´å��");
		addI18n("namecard.firstname", "First Name", "å��");
		addI18n("namecard.lastname", "Last Name", "å§“");
		addI18n("namecard.note", "Note", "å¤‡æ³¨");
		addI18n("namecard.signature", "Signature", "ç­¾å��");
		addI18n("namecard.sex", "Sex", "æ€§åˆ«");
		addI18n("namecard.sex.male", "Male", "ç”·");
		addI18n("namecard.sex.female", "female", "ç”·");

		/****************************************** ç¾¤ç»„ ***********************************************/
		addI18n("tab.group", "group", "ç¾¤ç»„");

		addI18n("tabTitle.group.add", "Group Add", "åˆ›å»ºç¾¤ç»„");

		addI18n("title.member.add", "Member Add", "é€‰æ‹©ç¾¤æˆ�å‘˜");

		addI18n("field.group.name", "Group Name", "ç¾¤å��ç§°");
		addI18n("field.group.size", "Group Size", "ç¾¤è§„æ¨¡");
		addI18n("field.group.size.unit", "{0} Person", "{0}äºº");

		addI18n("label.group.select.member", "Please select group member", "è¯·å‹¾é€‰è�”ç³»äºº");
		addI18n("label.group.select.member2", "{0} group members are selected", "å·²ç»�é€‰æ‹©äº†{0}è�”ç³»äºº");

		addI18n("tip.group.name", "Group Name", "ä¸ºä½ ä»¬çš„ç¾¤å�–ä¸ªç»™åŠ›çš„å��å­—å�§!");

		addI18n("warn.group.name", "Please input group name", "è¯·å¡«å†™ç¾¤ç»„å��ç§°");
		///
		addI18n("dialog.title.member.update", "Update Nick Name", "ä¿®æ”¹ç¾¤å†…æ˜µç§°");
		addI18n("label.text.member.nick", "Nick Name", "ç¾¤å†…æ˜µç§°");
		addI18n("warn.empty.text.member.nick", "Nick name is empty", "ç¾¤å†…æ˜µç§°ä¸�èƒ½ä¸ºç©º!");

		addI18n("dialog.title.group.update", "Update Group Name", "ä¿®æ”¹ç¾¤å��ç§°");
		addI18n("label.text.group.name", "Group Name", "ç¾¤å��ç§°");
		addI18n("warn.empty.text.group.name", "Group name is empty", "ç¾¤å��ç§°ä¸�èƒ½ä¸ºç©º!");

		///

		addI18n("button.group.add", "Add >", "å¢žåŠ  >");
		addI18n("button.group.delete", "< Delete", "< åˆ é™¤");
		addI18n("label.group.selected.member", "Selected Members", "å·²é€‰è�”ç³»äºº");
		addI18n("button.group.add.ok", "Create", "åˆ›å»º");

		addI18n("menu.group.add", "Group Add", "åˆ›å»ºç¾¤ç»„");
		addI18n("menu.group.start.chat", "Start Chat", "å�‘èµ·ç¾¤è�Š");
		addI18n("menu.talk.start.chat", "Start Chat", "å�‘èµ·è�Šå¤©");
		addI18n("menu.talk.open", "Open", "æ‰“å¼€");
		addI18n("menu.group.dissolve", "Dissolve Group", "è§£æ•£ç¾¤ç»„");
		addI18n("menu.group.quit", "Quit Group", "é€€å‡ºç¾¤ç»„");
		addI18n("menu.group.kicked", "Kick Member", "å‰”é™¤æˆ�å‘˜");
		addI18n("menu.group.inviteall", "Invite All", "é‚€è¯·å…¨éƒ¨");
		addI18n("menu.group.invite", "Invite", "é‚€è¯·");
		addI18n("menu.group.update.member.nick", "Update Member Nick", "ä¿®æ”¹ç¾¤å†…æ˜µç§°");
		addI18n("menu.group.update.group.nick", "Update Group Nick", "ä¿®æ”¹ç¾¤å��ç§°");

		//	å°�çŽ‹æŠŠå°�æ˜Žï¼Œå°�çº¢åŠ å…¥ç¾¤è�Š
		addI18n("message.invited1.to.group", "{0} has been joined to group [{2}] by {1}", "{0}è¢«{1}åŠ å…¥äº†ç¾¤[{2}]");
		//æˆ‘è¢«å°�æ˜Žé‚€è¯·åŠ ç�­ç¾¤è�Š
		addI18n("message.invited2.to.group", "I have been joined to group [{1}] by {0} ", "æˆ‘è¢«{0}åŠ å…¥äº†ç¾¤[{1}]");
		//toè¢«fromå‰”å‡ºäº†ç¾¤
		addI18n("message.kicked1.from.group", "{0} has been kicked out of group [{2}] by {1} ", "{0}è¢«ç¾¤ä¸»{1}å‰”å‡ºäº†ç¾¤[{2}]");
		//æˆ‘è¢«fromå‰”å‡ºäº†ç¾¤
		addI18n("message.kicked2.from.group", "I have been kicked out of group [{1}] by {0} ", "æˆ‘è¢«ç¾¤ä¸»{0}å‰”å‡ºäº†ç¾¤[{1}]");
		//fromé€€å‡ºäº†ç¾¤
		addI18n("message.leave1.from.group", "{0} has been left from group [{1}]", "{0}é€€å‡ºäº†ç¾¤[{1}]");
		//ç¾¤ä¸»é€€å‡ºäº†é€€ç¾¤
		addI18n("message.leave2.from.group", "Group Administrator {0} has been left from group [{1}]", "ç¾¤ä¸»{0}é€€å‡ºäº†ç¾¤[{1}]");
		//ç¾¤ä¸»è§£æ•£ç¾¤
		addI18n("message.destory.group", "Group Administrator {0} destory group [{1}]", "ç¾¤ä¸»{0}è§£æ•£äº†ç¾¤[{1}]");
		//ä¿®æ”¹ç¾¤ç»„æ˜µç§°
		addI18n("message.update.groupname.group", "group name is changed from [{0}] to [{1}]", "ç¾¤å��ç§°ä»Ž[{0}]ä¿®æ”¹æˆ�äº†[{1}]");
		//ä¿®æ”¹ç¾¤å†…æˆ�å‘˜æ˜µç§°
		addI18n("message.update.membernick.group", "[{0}] change nick to [{1}]", "[{0}]å°†ç¾¤å†…æ˜µç§°ä¿®æ”¹æˆ�äº†[{1}]");
		/****************************************** å¥½å�‹ ***********************************************/
		addI18n("tabTitle.contact.add", "Contact Add", "æ·»åŠ å¥½å�‹");
		addI18n("tip.contact.tel", "Contact Telephone", "ç”µè¯�å�·ç �");
		addI18n("tip.contact.tel.isempty", "Account number is empty", "è¯·è¾“å…¥è�”ç³»äººç”µè¯�å�·ç �ï¼�");
		addI18n("tip.contact.search", "Searching...", "æ­£åœ¨æŸ¥æ‰¾è�”ç³»äºº[{0}]...");
		addI18n("tip.contact.search.noresult", "Search [{0}] no result", "æœªèƒ½æ�œç´¢åˆ°è�”ç³»äºº[{0}]ï¼�");
		addI18n("tip.contact.search.exist", "[{0}] already exist", "[{0}]å·²ç»�æ˜¯å¥½å�‹äº†ï¼�");

		addI18n("button.contact.add.ok", "Add", "æ·»åŠ ");

		/****************************************** æ›´æ�¢çš®è‚¤ ***********************************************/
		addI18n("skin.changeskin", "Change Skin", "æ›´æ”¹å¤–è§‚");
		addI18n("skin.style", "Skin", "çš®è‚¤");
		addI18n("skin.color", "Color", "é¢œè‰²");
		addI18n("skin.screenshot", "Screenshot", "æŠ“å�–");
		addI18n("skin.toset", "Set", "è®¾ç½®");
		addI18n("skin.balloon", "Balloon", "å¤šå½©æ°”æ³¡");

		/****************************************** è�Šå¤©çª—å�£ ***********************************************/
		//å…³é—­çª—å�£
		addI18n("chatroom.close.title", "Close session", "å…³é—­ä¼šè¯�");
		addI18n("chatroom.close.label.all", "Close all sessions or close the current session?", "å…³é—­æ­¤çª—å�£æ‰€æœ‰ä¼šè¯�è¿˜æ˜¯ä»…å…³é—­å½“å‰�ä¼šè¯�ï¼Ÿ");
		addI18n("chatroom.close.button.all", "Close all", "å…³é—­æ‰€æœ‰");
		addI18n("chatroom.close.button.current", "Close current", "å…³é—­å½“å‰�");
		addI18n("chatroom.close.check.all", "Always close all sessions", "æ€»æ˜¯å…³é—­æ‰€æœ‰ä¼šè¯�");

		//
		/****************************************** æˆªå±� ***********************************************/
		addI18n("button.chatroom.screenshot", "Screen Shot", "å±�å¹•æˆªå›¾");

		addI18n("chatroom.screenshot.rgb", "RGB:({0},{1},{2})", "RGB:({0},{1},{2})");
		addI18n("chatroom.screenshot.size", "{0}Ã—{1}", "{0}Ã—{1}");

		/****************************************** æŒ¯å±� ***********************************************/
		addI18n("button.chatroom.screenbuzz", "Screen Buzz", "å�‘é€�æŒ¯å±�æ¶ˆæ�¯");

		/****************************************** æ–‡å­—å­—ä½“å’Œé¢œè‰² ***********************************************/
		addI18n("button.chatroom.textfontcolor", "Text Font And Color", "å­—ä½“å’Œé¢œè‰²");

		/****************************************** è¡¨æƒ… ***********************************************/
		addI18n("button.chatroom.emoticon", "Emoticon", "è¡¨æƒ…");

		/****************************************** çŸ­è¯­éŸ³ ***********************************************/
		addI18n("button.chatroom.chat.shortvoice", "Short Voice", "çŸ­éŸ³");
		addI18n("chatroom.working.shortvoice.message", "Now is recording, Please close record first!", "æ­£åœ¨å½•éŸ³ï¼Œè¯·å…ˆå…³é—­å½•éŸ³ï¼�");

		/****************************************** å›¾ç‰‡ ***********************************************/
		addI18n("button.chatroom.chat.picture", "Image", "å›¾ç‰‡");
		addI18n("button.chatroom.chat.picture.filter", "Image", "å›¾åƒ�æ–‡ä»¶");

		/****************************************** ç¾¤æˆ�å‘˜åˆ—è¡¨ ***********************************************/
		addI18n("button.chatroom.chat.member", "Member List", "ç¾¤æˆ�å‘˜");
		addI18n("button.chatroom.chat.member.invite", "Invite Contact", "é‚€è¯·å¥½å�‹");

		/****************************************** è�Šå¤©è®°å½• ***********************************************/
		addI18n("button.chatroom.chat.history", "Chat History", "æ¶ˆæ�¯è®°å½•");
		addI18n("button.chatroom.chat.history.all", "All", "å…¨éƒ¨");
		addI18n("button.chatroom.chat.history.image", "Images", "å›¾ç‰‡");
		addI18n("button.chatroom.chat.history.file", "Files", "æ–‡ä»¶");
		addI18n("button.chatroom.chat.history.transfer", "File Transfer", "æ–‡ä»¶ä¼ é€�");
		addI18n("button.chatroom.chat.history.query.scope", "Time:", "èŒƒå›´:");
		addI18n("button.chatroom.chat.history.query.content", "Content:", "å†…å®¹:");
		addI18n("button.chatroom.chat.history.query.month", "The last month", "æœ€è¿‘ä¸€ä¸ªæœˆ");
		addI18n("button.chatroom.chat.history.query.month3", "The last 3 months", "æœ€è¿‘ä¸‰ä¸ªæœˆ");
		addI18n("button.chatroom.chat.history.query.year", "The last year", "æœ€è¿‘ä¸€å¹´");
		addI18n("button.chatroom.chat.history.query.all", "All", "å…¨éƒ¨");
		addI18n("button.chatroom.chat.history.query.count", "Result:{0} items", "æ�œç´¢åˆ°{0}æ�¡è®°å½•");

		addI18n("delete.permanently", "Permanently delete?", "æ°¸ä¹…æ€§åˆ é™¤?");
		addI18n("delete.log.permanently", "Permanently delete log", "æ°¸ä¹…æ€§åˆ é™¤è®°å½•");

		/****************************************** æ–‡ä»¶ ***********************************************/
		addI18n("button.chatroom.file.history", "File", "æ–‡ä»¶");
		addI18n("button.chatroom.fileupload", "Select file to send", "é€‰æ‹©è¦�å�‘é€�çš„æ–‡ä»¶");
		addI18n("tabTitle.chatroom.fileupload", "Select file to send", "é€‰æ‹©è¦�å�‘é€�çš„æ–‡ä»¶");
		addI18n("warn.chatroom.fileupload.toobig", " The selected file is too big.\nProceed anyway?", "é€‰ä¸­çš„æ–‡ä»¶å¤ªå¤§äº†ã€‚\nç»§ç»­ï¼Ÿ");
		addI18n("error.chatroom.fileupload.toobig", "The selected file is too big.\nThe maximum filezise is {0} the selected file has {1}", "é€‰ä¸­çš„æ–‡ä»¶å¤ªå¤§äº†ã€‚\næœ€å¤§ä¸�è¶…è¿‡{0}ï¼Œä½†æ˜¯é€‰ä¸­çš„æ–‡ä»¶å¤§å°�ä¸º{1}");
		addI18n("error.chatroom.fileupload.image", "Failed to send image", "å�‘é€�å¤±è´¥");
		addI18n("error.chatroom.filedownload.image", "Failed to download image", "æŽ¥æ”¶å›¾ç‰‡å¤±è´¥");
		addI18n("label.chatroom.fileupload.progress", "{0},left time:{1}", "{0},å‰©ä½™æ—¶é—´:{1}");
		addI18n("label.chatroom.fileupload.complete", "Successed to send file", "æˆ�åŠŸå�‘é€�æ–‡ä»¶");
		addI18n("label.chatroom.fileupload.cancel", "You cancel file \"{0}\" to send,file transfer failed", "æ‚¨å�–æ¶ˆäº†â€œ{0}â€�çš„å�‘é€�ï¼Œæ–‡ä»¶ä¼ è¾“å¤±è´¥ã€‚");
		addI18n("label.chatroom.filedownload.complete", "File save as {0}", "æ–‡ä»¶å­˜è‡³{0}");
		addI18n("label.chatroom.click.to.open", "Click to open", "ç‚¹å‡»ä»¥æ‰“å¼€");
		addI18n("label.chatroom.filedownload.cancel", "Cancel to accept file", "å�–æ¶ˆæŽ¥æ”¶æ–‡ä»¶");
		addI18n("message.file.exists.question", "File already exist,overide?", "æ–‡ä»¶å·²ç»�å­˜åœ¨ã€‚ è¦†ç›–ï¼Ÿ");
		addI18n("message.file.exists.not", "{0} does not exist", "{0} å·²ç»�ä¸�å­˜åœ¨");
		addI18n("message.file.too.large", "File is too large, more than {0}", "æ–‡ä»¶å¤ªå¤§äº†ï¼Œè¶…è¿‡äº†{0}");
		addI18n("message.file.unsupported.image.format", "Unsupported image format", "ä¸�æ”¯æŒ�çš„å›¾ç‰‡æ ¼å¼�");
		addI18n("chatroom.file.working", "If you close the window, you will end the file transfer. Do you want to close the window?", "å¦‚æžœå…³é—­çª—å�£ï¼Œå°†ä¼šç»ˆæ­¢æ–‡ä»¶ä¼ é€�ï¼Œæ˜¯å�¦å…³é—­çª—å�£ï¼Ÿ");
		/****************************************** ç‰ˆæœ¬å�‡çº§ ***********************************************/
		addI18n("title.downloading.im.client", "Download CooTalk client", "ä¸‹è½½ CooTalk å®¢æˆ·ç«¯");
		addI18n("title.upgrading.client", "Upgrading client", "å�‡çº§å®¢æˆ·ç«¯");
		addI18n("title.download.complete", "Download complete", "ä¸‹è½½å®Œæˆ�");
		addI18n("title.cancelled", "Cancelled", "è¢«å�–æ¶ˆ");
		addI18n("title.new.client.available", "New client available", "ç‰ˆæœ¬å�‡çº§");
		addI18n("message.version", "Version: {0}", "ç‰ˆæœ¬ï¼š {0}");
		addI18n("message.file.size", "File Size: {0}", "æ–‡ä»¶å¤§å°�ï¼š {0}");
		addI18n("message.file.size", "File Size: {0}", "æ–‡ä»¶å¤§å°�ï¼š {0}");
		addI18n("message.restart.talk", "You will need to shut down the client to \n install the new version, would you like to do that now?", "ä½ éœ€è¦�å…³é—­å®¢æˆ·ç«¯ä»¥å®‰è£…æœ€æ–°çš„ç‰ˆæœ¬ã€‚ æ˜¯å�¦çŽ°åœ¨å®‰è£…ï¼Ÿ");
		addI18n("message.updating.cancelled", "Updating has been canceled", "æ›´æ–°è¢«å�–æ¶ˆäº†ã€‚");
		addI18n("message.transfer.rate", "Transfer rate", "ä¼ é€�çŽ‡");
		addI18n("message.total.downloaded", "Total downloaded", "å…±è®¡ä¸‹è½½äº†");
		addI18n("message.restart.talk.to.install", "You need to shut down the client to install latest version, would you like to do that now?", "ä½ éœ€è¦�å…³é—­å®¢æˆ·ç«¯ä»¥å®‰è£…æœ€æ–°çš„ç‰ˆæœ¬, æ˜¯å�¦çŽ°åœ¨å®‰è£…ï¼Ÿ");
		addI18n("message.no.updates", "There are no updates", "æ‚¨å·²ç»�æ˜¯æœ€æ–°ç‰ˆæœ¬äº†ï¼�");
		addI18n("message.new.talk.available1", "{0} is now available, would you like to install?", "ç‰ˆæœ¬è¿‡ä½Žï¼Œéƒ¨åˆ†åŠŸèƒ½å�¯èƒ½æ— æ³•æ­£å¸¸ä½¿ç”¨ï¼Œè¯·å�‡çº§è‡³æœ€æ–°ç‰ˆæœ¬{0}ï¼Œ æ˜¯å�¦å®‰è£…ï¼Ÿ");
		addI18n("message.new.talk.available2", "{0} is now available, would you like to install?", "æ‚¨æœ‰æœ€æ–°ç‰ˆæœ¬å�¯ä»¥æ›´æ–°äº†:{0}ï¼Œ æ˜¯å�¦å®‰è£…ï¼Ÿ");

		/****************************************** æ¶ˆæ�¯ ***********************************************/
		addI18n("dialog.title.fire", "Self immolation message", "é˜…å�Žè‡ªç„šæ¶ˆæ�¯");
		addI18n("dialog.title.relay", "Relay message", "è½¬å�‘æ¶ˆæ�¯");
		addI18n("input.text.relay.note", "Leave a message to a friend", "ç»™æœ‹å�‹ç•™è¨€");
		addI18n("button.relay", "Relay", "è½¬å�‘");
		addI18n("label.tip.relay", "Forward to the following contact:{0}/{1}", "è½¬å�‘ç»™ä»¥ä¸‹è�”ç³»äºº:{0}/{1}");
		addI18n("warn.message.relay", "Has reached {0} contact on the line", "å·²ç»�è¾¾åˆ°{0}äººä¸Šçº¿");

		/****************************************** ç³»ç»Ÿè®¾ç½® ***********************************************/
		addI18n("setting.title", "System settings", "ç³»ç»Ÿè®¾ç½®");

		//é€šç”¨è®¾ç½®
		addI18n("setting.general.title", "General settings", "é€šç”¨è®¾ç½®");
		addI18n("setting.general.title1", "General settings", "å¸¸è§„è®¾ç½®");
		addI18n("setting.general.autologin", "Auto login", "è‡ªåŠ¨ç™»å½•");
		addI18n("setting.general.promptsound", "Open message prompt sound", "å¼€å�¯æ–°æ¶ˆæ�¯æ��ç¤ºå£°éŸ³");
		addI18n("setting.general.autoupgrade", "Auto upgrade Talk when updated", "æœ‰æ›´æ–°æ—¶è‡ªåŠ¨å�‡çº§Talk");
		addI18n("setting.general.autostart", "Boot auto start Talk", "å¼€æœºæ—¶è‡ªåŠ¨å�¯åŠ¨Talk");

		//æŽ¥åˆ°æ–°æ¶ˆæ�¯æ—¶
		addI18n("setting.general.receivenewmessage", "When receive a new message", "æŽ¥åˆ°æ–°æ¶ˆæ�¯æ—¶");
		addI18n("setting.general.receivenewmessage.autopop", "Auto pops up", "è‡ªåŠ¨å¼¹å‡º");
		addI18n("setting.general.receivenewmessage.flash", "Flashing tips", "é—ªçƒ�æ��ç¤º");

		//å…³é—­ä¸»é�¢æ�¿æ—¶
		addI18n("setting.general.closemainwindow", "When close main window", "å…³é—­ä¸»é�¢æ�¿æ—¶");
		addI18n("setting.general.closemainwindow.exit", "Exit the program", "é€€å‡ºç¨‹åº�");
		addI18n("setting.general.closemainwindow.tray", "Minimize to the tray", "æœ€å°�åŒ–åˆ°æ‰˜ç›˜");

		//å¿«æ�·æŒ‰é”®
		addI18n("setting.shortcutkey.title", "Shortcut keys", "å¿«æ�·æŒ‰é”®");
		addI18n("setting.shortcutkey.screenshot", "Screenshot", "æˆªå�–å±�å¹•");
		addI18n("setting.shortcutkey.mainwindow", "Show/hide main window", "æ˜¾ç¤º/éš�è—�ä¸»çª—å�£");
		addI18n("setting.shortcutkey.conflict", "Conflict", "å†²çª�");

		//è¯­è¨€è®¾ç½®
		addI18n("setting.language.title", "Language Setting", "è¯­è¨€è®¾ç½®");
		addI18n("setting.language.cn", "ç®€ä½“ä¸­æ–‡", "ç®€ä½“ä¸­æ–‡");
		addI18n("setting.language.en", "English", "English");
		addI18n("setting.language.en", "English", "English");
		addI18n("setting.language.dialog.title", "Exit Login", "é€€å‡ºç™»å½•");
		addI18n("setting.language.dialog.msg", "Change the language to be logged in again.", "æ›´æ”¹è¯­è¨€éœ€é‡�æ–°ç™»å½•æ‰�èƒ½é‡�æ–°ç”Ÿæ•ˆ");

		//å…³äºŽè®¾ç½®
		addI18n("setting.about.title", "About", "å…³äºŽ");
		addI18n("setting.about.talk.version", "Software version", "è½¯ä»¶ç‰ˆæœ¬");

		////////
		/****************************************** æ—¥æœŸ ***********************************************/
		addI18n("month.0", "Jan", "ä¸€æœˆ");
		addI18n("month.1", "Feb", "äºŒæœˆ");
		addI18n("month.2", "Mar", "ä¸‰æœˆ");
		addI18n("month.3", "Apr", "å››æœˆ");
		addI18n("month.4", "May", "äº”æœˆ");
		addI18n("month.5", "Jun", "å…­æœˆ");
		addI18n("month.6", "Jul", "ä¸ƒæœˆ");
		addI18n("month.7", "Aug", "å…«æœˆ");
		addI18n("month.8", "Sep", "ä¹�æœˆ");
		addI18n("month.9", "Oct", "å��æœˆ");
		addI18n("month.10", "Nov", "å��ä¸€æœˆ");
		addI18n("month.11", "Dec", "å��äºŒæœˆ");
		addI18n("week.0", "Su", "æ—¥");
		addI18n("week.1", "Mo", "ä¸€");
		addI18n("week.2", "Tu", "äºŒ");
		addI18n("week.3", "We", "ä¸‰");
		addI18n("week.4", "Th", "å››");
		addI18n("week.5", "Fr", "äº”");
		addI18n("week.6", "Sa", "å…­");
		addI18n("pre.mon", "Previous month", "ä¸Šä¸ªæœˆ");
		addI18n("next.mon", "Next month", "ä¸‹ä¸ªæœˆ");
		addI18n("pre.year", "Previous year", "ä¸Šä¸€å¹´");
		addI18n("next.year", "Next year", "ä¸‹ä¸€å¹´");
		addI18n("today", "Today", "ä»Šå¤©");
		addI18n("yesterday", "Yesterday", "æ˜¨å¤©");

		//////////////////

		/****************************************** å…¬å…±--æŒ‰é’® ***********************************************/
		addI18n("button.yes", "Yes", "æ˜¯");
		addI18n("button.no", "No", "å�¦");
		addI18n("button.ok", "Ok", "ç¡®å®š");
		addI18n("button.close", "Close", "å…³é—­");
		addI18n("button.cancel", "Cancel", "å�–æ¶ˆ");
		addI18n("button.back", "Back", "è¿”å›ž");
		addI18n("button.delete", "Delete", "åˆ é™¤");
		addI18n("button.send", "Send", "å�‘é€�");
		addI18n("button.resend", "Resend", "é‡�å�‘");
		addI18n("button.open", "Open", "æ‰“å¼€");
		addI18n("button.opendir", "Open Dir", "æ‰“å¼€æ–‡ä»¶å¤¹");
		addI18n("button.forward", "Forward", "è½¬å�‘");
		addI18n("button.accept", "Accept", "æŽ¥æ”¶");
		addI18n("button.reject", "Reject", "æ‹’ç»�");
		addI18n("button.save.as", "Save As", "å�¦å­˜ä¸º");

		addI18n("tabTitle.error", "Error", "é”™è¯¯");
		addI18n("tabTitle.warn", "Warn", "è­¦å‘Š");
		addI18n("title.alert", "Alert", "è­¦å‘Š");
		addI18n("title.info", "Information", "æ��ç¤º");

		addI18n("menuitem.save", "Save", "ä¿�å­˜");
		addI18n("menuitem.save.as", "Save as...", "å�¦å­˜ä¸º ...");
		addI18n("menuitem.chatroom.key.enter", "Press Enter to send message", "æŒ‰Enteré”®å�‘é€�æ¶ˆæ�¯");
		addI18n("menuitem.chatroom.key.ctrl.enter", "Press CTRL+Enter to send message", "æŒ‰CTRL+Enteré”®å�‘é€�æ¶ˆæ�¯");

		addI18n("action.print", "Print", "æ‰“å�°");
		addI18n("action.delete", "Delete", "åˆ é™¤");
		addI18n("action.clear", "Clear", "æ¸…é™¤");
		addI18n("action.clear.screen", "Clear Screen", "æ¸…å±�");
		addI18n("action.copy", "Copy", "å¤�åˆ¶");
		addI18n("action.cut", "Cut", "å‰ªåˆ‡");
		addI18n("action.paste", "Paste", "ç²˜è´´");
		addI18n("action.save", "Save", "ä¿�å­˜");
		addI18n("action.select.all", "Select all", "å…¨é€‰");
		addI18n("action.ralay", "Ralay", "è½¬å�‘");

		addI18n("message.default.error", "An error has been detected. Please report to support@xinwei.com.cn", "æ£€æµ‹åˆ°é”™è¯¯ã€‚è¯·å�‘support@xinwei.com.cnæŠ¥å‘Š");

	}

	public static void main(String[] args) {
		try {
			init("src/main/resources/talk/i18n/", "talk");
			clearI18n();
			addI18n();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
