/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月12日 下午8:15:14
 * 
 ***************************************************************/
package com.xinwei.talk.dao;

import java.sql.SQLException;

import com.xinwei.common.lang.Sqlite;

public class Dao {

	protected int createTable(Sqlite sqlite, String sql) {
		try {
			return sqlite.createTable(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			e.printStackTrace();
			return -1;
		}
	}
}
