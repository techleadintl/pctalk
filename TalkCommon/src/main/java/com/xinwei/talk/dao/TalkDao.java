package com.xinwei.talk.dao;

import com.xinwei.talk.model.*;
import com.xinwei.talk.model.message.TalkMessage;

import java.util.*;

public interface TalkDao {

	public void init();

	/*************** 文件 *****************************/
	public int addFile(long msgId, String url, int type, byte[] data, String path, boolean download, boolean finish);

	public int updateFile(long msgId, String url, boolean finish);

	public int deleteFile(long msgId);

	public int deleteFileList(List<Long> msgIdList);

	public int deleteFile(long msgId, String url, int type);

	public TalkFile getFile(long msgId, String url, int type);

	public List<TalkFile> getFileList(int type);

	public List<TalkFile> getFileList(int type, String... urls);

	public List<TalkFile> getFileList(int type, List<String> urls);

	public int updateMsgReadStatus(long msgId, int readStatus);

	/*************** 通讯录 *****************************/
	public int replaceContactList(List<Contact> cs);

	public List<Contact> getContactList();

	/*************** 用户 *****************************/
	public int addTalkList(List<? extends Talk> us);

	public int updateTalkList(List<? extends Talk> us);

	public int updateTalkAvatar(String userTel, byte[] avatar);

	public int updateTalkBackground(String userTel, byte[] background);

	public int deleteTalkList(List<? extends Talk> camTalks);

	public int clearTalkList();

	public List<? extends Talk> getTalkList();

	public List<? extends Talk> getTalkByJid(String jId);
	public List<? extends Talk> getTalkByTel(String tel);

	/*************** 组 *****************************/
	public int addGroupList(List<Group> gs, List<Member> ms);

	public int deleteGroupList(List<Group> gs);

	public List<Group> getGroupList();

	/**
	 * get group from DB by given Group ID
	 * 
	 * @author nisal
	 * @param ID request ID of the group
	 **/
	public List<? extends Group> getGroupByID(String ID);

	public int updateGroupList(List<Group> gs);

	public int updateGroupBackground(String gid, byte[] background);

	/*************** 组成员 *****************************/

	public List<Member> getMemberList();

	public List<Member> getMemberList(String gid);

	public List<Member> getMemberList(List<String> gids);

	public Member getMember(String gid, String uid);

	public Member getMemberByJid(String gid, String jId);

	public int addMember(Member g);

	public int addMemberList(Collection<Member> ms);

	public int updateMember(Member g);

	public int updateMemberList(List<Member> ms);

	public int deleteMemberList(String gid);

	public int deleteMemberList(String gid, List<Member> memberList);

	public int updateMemberAvatar(String gid, String uid, byte[] avatar);

	/*************** 消息 *****************************/
	/*************** 用户消息 *****************************/
	public int removeMessageList(String jid);

	public int deleteMessage(String jid, long msgId);

	public int addMessageList(List<TalkMessage> gs);

	public Map<String, Object> getMessageTimeInfo(String jid, String searchStr, Date searchStartTime,
			Date searchEndTime);

	public List<TalkMessage> getMessageList(String jid, String searchStr, long id, int num, boolean next);

	public List<TalkMessage> getMessageList(String jid, Date date, int num);

	public TalkMessage getLastMessage(String jid);

	/** 查询某种类型的消息 **/
	public List<TalkMessage> getMessageList(String jid, int type);

	/** 查询最大标识数 **/
	public long getMessageMaxId();

	// 查询最近联系人及消息
	public List<TalkMessage> getMessageRecentList(int limit);

	// 查询未阅读消息最新一条
	public List<TalkMessage> getLastMessageList(int read);

	// 查询未阅读消息最新一条
	public List<TalkMessage> getMessageList(int readed);

	public int updateMessageStatus(String jid, int readed);

	public List<TalkMessage> getUnReadMessageList(String jid, int readed);

	/*************** 配置信息 *****************************/
	public int saveUserConfig(String key, String json);

	public int insertUserConfig(String key, String json);

	public int updateUserConfig(String key, String json);

	public String getUserConfig(String key);

	public List<HashMap> getUserConfigList();
}
