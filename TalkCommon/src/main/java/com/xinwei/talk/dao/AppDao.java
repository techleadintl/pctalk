package com.xinwei.talk.dao;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import com.xinwei.common.lang.Sqlite;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.model.VCard;

public class AppDao extends Dao {
	//登录用户
	static Sqlite appInfoSqlite;

	//登錄賬號
	private static final String LOGIN_SQL = "CREATE TABLE IF NOT EXISTS LOGIN_USER(UID VARCHAR(32) PRIMARY KEY, TEL VARCHAR(32), PASSWORD  VARCHAR(32), SAVEPASSWORD BOOLEAN, AUTOLOGIN BOOLEAN, NAME VARCHAR(32),AUTOREPLYMSG VARCHAR(32), NICK VARCHAR(32), COUNTRYCODE INTEGER, NETWORKID INTEGER, AVATAR BLOB, TIME BIGINT)";

	private static final String APP_CONFIG_SQL = "CREATE TABLE IF NOT EXISTS CONFIG_APP(KEY VARCHAR(32) PRIMARY KEY, VALUE TEXT)";

	public void init() {
		
		File appFile = TalkUtil.getAllUsersFile("Info.db");
		appInfoSqlite = new Sqlite(appFile.getAbsolutePath());
		//登录账号信息
		createTable(appInfoSqlite, LOGIN_SQL);
		//配置信息
		createTable(appInfoSqlite, APP_CONFIG_SQL);
	}

	/*************** 应用配置信息 *****************************/
	public int saveAppConfig(String key, String json) {
		try {
			int r = updateAppConfig(key, json);
			if (r < 1) {
				r = insertAppConfig(key, json);
			}
			return r;
		} catch (Exception e) {
			return -1;
		}
	}

	public int insertAppConfig(String key, String json) {
		try {
			String sql = "INSERT INTO CONFIG_APP (KEY,VALUE) VALUES (?,?)";
			return appInfoSqlite.insert(sql, key, json);
		} catch (Exception e) {
			return -1;
		}
	}

	public int updateAppConfig(String key, String json) {
		try {
			String sql = "UPDATE CONFIG_APP SET VALUE=? WHERE KEY=?";
			return appInfoSqlite.insert(sql, json, key);
		} catch (Exception e) {
			return -1;
		}
	}

	public String getAppConfig(String key) {
		try {
			String sql = "SELECT VALUE FROM CONFIG_APP WHERE KEY = ?";
			return appInfoSqlite.query(sql, String.class, key);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<HashMap> getAppConfigList() {

		try {
			String sql = "SELECT * FROM CONFIG_APP";
			return appInfoSqlite.queryList(sql, HashMap.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*************** 登录用户 *****************************/
	public int saveLogin(VCard u) {
		int r = updateLogin(u);
		if (r < 1) {
			r = insertLogin(u);
		}
		return r;
	}

	public int insertLogin(VCard u) {
		try {
			String sql = "INSERT INTO LOGIN_USER (UID,TEL,PASSWORD,SAVEPASSWORD,AUTOLOGIN,NAME,AUTOREPLYMSG,NICK,COUNTRYCODE,NETWORKID,AVATAR,TIME) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
			return appInfoSqlite.insert(sql, u.getUid(), u.getTel(), u.getPassword(), u.isSavePassword(), u.isAutoLogin(), u.getName(), u.getAutoMsg(), u.getNick(), u.getCountryCode(), u.getNetworkId(),
					u.getAvatar(), u.getTimestamp());
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int deleteLogin(String uid) {
		try {
			String sql = "DELETE FROM LOGIN_USER WHERE UID = ?";
			return appInfoSqlite.update(sql, uid);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public VCard getLogin(String uid) {
		try {
			String sql = "SELECT * FROM LOGIN_USER WHERE UID = ?";
			return appInfoSqlite.query(sql, VCard.class, uid);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public VCard getLoginByTel(String tel) {
		try {
			String sql = "SELECT * FROM LOGIN_USER WHERE TEL = ?";
			return appInfoSqlite.query(sql, VCard.class, tel);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<VCard> getLoginList() {
		try {
			String sql = "SELECT * FROM LOGIN_USER";
			return appInfoSqlite.queryList(sql, VCard.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int updateLogin(VCard u) {
		try {
			String sql = "UPDATE LOGIN_USER SET NAME = ?,AUTOREPLYMSG = ?,NICK = ?,TEL = ?,PASSWORD = ?,SAVEPASSWORD = ?,AUTOLOGIN = ?,COUNTRYCODE = ?,NETWORKID= ?,TIME = ?,AVATAR=? WHERE UID = ?";
			return appInfoSqlite.update(sql, u.getName(),u.getAutoMsg(), u.getNick(), u.getTel(), u.getPassword(), u.isSavePassword(), u.isAutoLogin(), u.getCountryCode(), u.getNetworkId(), u.getTimestamp(),
					u.getAvatar(), u.getUid());
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int updateLoginAvatar(String uid, byte[] avatar) {
		try {
			String sql = "UPDATE LOGIN_USER SET AVATAR=? WHERE UID = ?";
			return appInfoSqlite.update(sql, avatar, uid);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

}
