package com.xinwei.talk.dto;

/**
 * @author Charith
 */
public class FindFriendsDTO {
	private String telNo;
	private String nickName;
	private String age;
	private String gender;
	private String country;
	private String email;
	
	public FindFriendsDTO(String telNo, String nickName, String age, String gender, String country,
			String email) {
		super();
		this.telNo = telNo;
		this.nickName = nickName;
		this.age = age;
		this.gender = gender;
		this.country = country;
		this.email = email;
	}
	
	public String getTelNo() {
		return telNo;
	}

	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
