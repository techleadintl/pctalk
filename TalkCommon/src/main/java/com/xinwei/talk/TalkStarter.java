/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月22日 下午5:14:31
 * 
 ***************************************************************/
package com.xinwei.talk;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.ThemeImage;
import com.xinwei.spark.TaskEngine;
import com.xinwei.spark.ui.MessageDialog;
import com.xinwei.talk.common.Constants;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.PluginManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.manager.TransferManager;
import com.xinwei.talk.manager.VCardManager;
import com.xinwei.talk.model.MyTalk;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.ui.main.MainWindow;
import com.xinwei.talk.ui.main.Workspace;
import com.xinwei.xmpp.service.XmppService;

public class TalkStarter {
	public void start() {
		// 初始化数据库
		TalkManager.getTalkDao().init();

		// setTheme();

		final MainWindow mainWindow = TalkManager.getMainWindow();

		Workspace workspace = Workspace.getInstance();

		mainWindow.getContentPane().add(workspace, BorderLayout.CENTER);

		initPlugins();

		initWorkspace();

		// Register PacketListeners Add the packetListener to this instance
		TalkManager.addPacketListener();

		initData();

		initPresence();

		TransferManager.getInstance();
		// 启动自动升级
		TalkManager.getCheckUpdates().checkForUpdate();

		// 显示窗口
		mainWindow.setVisible(true);
		// 释放内存
		mainWindow.setState(Frame.ICONIFIED);
		mainWindow.setState(Frame.NORMAL);

	}

	private void initPresence() {
		final XmppService xmppService = TalkManager.getXmppService();

		xmppService.sendOnlinePresence();
	}

	private void initData() {
		final VCardManager vCardManager = TalkManager.getVCardManager();
		final LocalManager localManager = TalkManager.getLocalManager();
		// 群消息用户，Appear in recent contacts
		Talk groupMessageTalk = new Talk();
		// groupMessageTalk.setAvatar(LAF.getDefaultAvatarIcon());
		groupMessageTalk.setJid(TalkUtil.getUserFullJid(Constants.CUSTOM_TALK_TEL_GROUP_MESSAGE));
		groupMessageTalk.setTel(Constants.CUSTOM_TALK_TEL_GROUP_MESSAGE);
		groupMessageTalk.setNick(Res.getMessage("custom.talk.nick.group.message"));
		groupMessageTalk.setSignature("");
		groupMessageTalk.setUid(String.valueOf(Constants.CUSTOM_TALK_TEL_GROUP_MESSAGE.hashCode()));
		// 系统消息用户，Appear in recent contacts
		Talk systemMessageTalk = new Talk();
		// groupMessageTalk.setAvatar(LAF.getDefaultAvatarIcon());
		systemMessageTalk.setJid(TalkUtil.getUserFullJid(Constants.CUSTOM_TALK_TEL_SYSTEM_MESSAGE));
		systemMessageTalk.setTel(Constants.CUSTOM_TALK_TEL_SYSTEM_MESSAGE);
		systemMessageTalk.setNick(Res.getMessage("custom.talk.nick.system.message"));
		systemMessageTalk.setSignature("");
		systemMessageTalk.setUid(String.valueOf(Constants.CUSTOM_TALK_TEL_SYSTEM_MESSAGE.hashCode()));

		// 增加手机，Appears in recent contacts and friends
		VCard vCard = vCardManager.getVCard();
		MyTalk myTalk = new MyTalk();
		myTalk.setAvatar(IOUtil.readBytes(LAF.getURL("talk/images/avatar/myphone.png")));
		myTalk.setJid(vCard.getJid());
		myTalk.setTel(vCard.getTel());
		myTalk.setNick(Res.getMessage("custom.talk.nick.phone"));
		myTalk.setSignature(vCard.getSignature());
		myTalk.setUid(vCard.getUid());
		myTalk.setRes(Constants.MY_TALK_RES_MOBILE);

		localManager.putOtherTalk(groupMessageTalk);
		localManager.putOtherTalk(systemMessageTalk);
		localManager.putMyTalk(myTalk);

		// 加载本地数据库
		vCardManager.initTalk();
		vCardManager.initGroup();

		// 加载头像
		vCardManager.initAvatar();

		// request contact list from server and update local DB
		TaskEngine.getInstance().submit(new Runnable() {
			public void run() {
				try {
					vCardManager.sysTalk();
				} catch (Exception e) {
					Log.error(SystemUtil.getStackTrace(e));
					MessageDialog.showErrorDialog(e);
				}
			}
		});

		// request group and group member details from server and update DB
		TaskEngine.getInstance().submit(new Runnable() {
			public void run() {
				try {
					vCardManager.sysGroup();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	int i = 0;

	private void initWorkspace() {

	}

	public void initPlugins() {
		final PluginManager pluginManager = PluginManager.getInstance();
		TalkManager.getMainWindow().addMainWindowListener(pluginManager);
		pluginManager.initializes();
	}

	private static void setTheme() {
		Object theme = TalkManager.getUserConfig().getTheme();
		// 加载皮肤
		if (theme != null) {
			if (theme instanceof ThemeColor) {
				McWillTheme.setThemeColor((ThemeColor) theme);
			} else if (theme instanceof ThemeImage) {
				McWillTheme.setThemeImage((ThemeImage) theme);
			}
		}

		// 注册皮肤更新监听器
		McWillTheme.setListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				Object newValue = evt.getNewValue();
				if (newValue == null)
					return;
				TalkManager.getUserConfig().setTheme(newValue);

				TalkManager.saveUserConfig();
			}
		});
	}
}
