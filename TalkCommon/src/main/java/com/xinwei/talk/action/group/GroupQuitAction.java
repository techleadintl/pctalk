package com.xinwei.talk.action.group;

import java.awt.event.ActionEvent;
import java.util.List;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.spark.ui.MessageDialog;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Member;

/**
 * 退出群组
 *
 */
public class GroupQuitAction extends GroupAction {
	private static final long serialVersionUID = -6969381628320014395L;

	public GroupQuitAction() {
		setLabel(Res.getMessage("menu.group.quit"));
		setIcon(LAF.getGroupQuitIcon()[0]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!isVisible()) {
			return;
		}

		String subsId = TalkManager.getVCard().getUid();
		try {
			int r = TalkManager.getTalkService().quitMember(subsId, obj.getId());
//			if (r == 0) {
//				TalkManager.getVCardManager().deleteGroup(group);
//			}
		} catch (Exception e1) {
			MessageDialog.showErrorDialog(e1);
		}

	}

	@Override
	public boolean isVisible() {
		String subsId = TalkManager.getVCard().getUid();
		if (StringUtil.isEmpty(subsId)) {
			return false;
		}

		String id = obj.getId();
		List<Member> memberList = TalkManager.getLocalManager().getMemberList(id);
		if (memberList == null || memberList.isEmpty())
			return false;

//		if (memberList.size() == 1) {
//			Member xwMember = memberList.get(0);
//			if (TalkUtil.isSelfJid(xwMember.getMemJid())) {
//				return false;
//			}
//		}

		return true;
	}
}
