package com.xinwei.talk.action.member;

import com.xinwei.talk.action.XAction;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;

public abstract class MemberAction extends XAction<Member> {
	private static final long serialVersionUID = 1L;
	protected Group group;

	public MemberAction() {
	}

	@Override
	public void setObjs(Object... objs) {
		group = (Group) objs[0];
		obj = (Member) objs[1];
	}
}
