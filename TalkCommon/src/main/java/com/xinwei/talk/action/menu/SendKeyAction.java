/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月20日 下午3:59:38
 * 
 ***************************************************************/
package com.xinwei.talk.action.menu;

import java.awt.Container;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.action.XAction;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.manager.TalkManager;

public class SendKeyAction extends XAction {
	private String key;

	private ImageIcon checkImageIcon;

	public SendKeyAction(String label, String key) {
		this.key = key;
		setLabel(label);
		checkImageIcon = LAF.getSendKeyCheckIcon();

		if (StringUtil.equals(key, TalkManager.getUserConfig().getString("Default-Send-Key"))) {
			setIcon(checkImageIcon);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		setIcon(checkImageIcon);
		JMenuItem menuItem = (JMenuItem) e.getSource();
		Container parent = menuItem.getParent();
		for (int i = 0; i < parent.getComponentCount(); i++) {
			if (parent.getComponent(i) != e.getSource() && parent.getComponent(i) instanceof JMenuItem) {
				((JMenuItem) parent.getComponent(i)).setIcon(null);
			}
		}
		TalkManager.getUserConfig().put("Default-Send-Key", key);
	}

}
