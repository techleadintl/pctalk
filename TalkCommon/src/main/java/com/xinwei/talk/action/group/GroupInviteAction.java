package com.xinwei.talk.action.group;

import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;

/**
 * 
 * 邀请好友
 */
public class GroupInviteAction extends GroupAction {
	private static final long serialVersionUID = -6969381628320014395L;
	LocalManager localManager = TalkManager.getLocalManager();

	public GroupInviteAction() {
		setLabel(Res.getMessage("menu.group.inviteall"));
		setIcon(LAF.getGroupInviteIcon()[0]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		List<Member> memberList = localManager.getMemberList(obj.getId());
		final Map<String, Member> phones = new HashMap<>();
		String loginUid = TalkManager.getVCard().getUid();
		for (Member member : memberList) {
			String memJid = member.getMemJid();
			String memUid = member.getUid();
			if (StringUtil.equals(memUid, loginUid)) {//自己不邀请自己
				continue;
			}

			Talk talk = localManager.getTalkByJid(memJid);
			if (talk == null) {
				phones.put(TalkUtil.getUserTel(memJid), member);
			}
		}

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				try {
					List<Talk> talkList = TalkManager.getTalkService().getTalkList(phones.keySet());
					if (talkList == null)
						return;
					Map<Talk, String> map = new HashMap<Talk, String>();
					for (Talk talk : talkList) {
						Member member = phones.get(talk.getTel());
						String display = null;
						if (member != null) {
							display = member.getMemNick();
						}
						map.put(talk, display);
					}
					try {
						TalkManager.getTalkService().addTalk(map);
						TalkManager.getVCardManager().addTalk(talkList);
					} catch (Exception e) {//如果上传失败，则删除
						Log.error(e);
					}

				} catch (Exception e1) {
				}
			}
		});

	}

	@Override
	public boolean isVisible() {
		List<Member> memberList = localManager.getMemberList(obj.getId());
		if (memberList.size() < 2) {
			return false;
		}
		return true;
	}
}
