package com.xinwei.talk.action.group;

import com.xinwei.talk.action.XAction;
import com.xinwei.talk.model.Group;

public abstract class GroupAction extends XAction<Group> {
	private static final long serialVersionUID = 1L;

	public GroupAction() {
	}

	public boolean isVisible() {
		return true;
	}

	public GroupAction(Group group) {
		super(group);
	}
}
