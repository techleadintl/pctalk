package com.xinwei.talk.action.member;

import java.awt.event.ActionEvent;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.main.dialog.member.MemberUpdateDialog;

/**
 * 
 * 剔除组成员
 */
public class MemberUpdateAction extends MemberAction {
	private static final long serialVersionUID = -6969381628320014395L;

	public MemberUpdateAction() {
		setLabel(Res.getMessage("menu.group.update.member.nick"));
		setIcon(LAF.getTalkUpdateIcon()[0]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (group == null)
			return;
		MemberUpdateDialog memberDialog = new MemberUpdateDialog(group, obj);
		memberDialog.setVisible(true);
	}

	@Override
	public boolean isVisible() {
		if (group == null)
			return false;
		String memUid = obj.getUid();
		String creatorUid = group.getCreatorUid();
		String loginUid = TalkManager.getVCard().getUid();
		//		if (StringUtil.equals(loginUid, memUid) || StringUtil.equals(loginUid, creatorUid)) {//只能修改自己的名片,或者管理员可以修改其他成
		//			return true;
		//		}
		if (StringUtil.equals(loginUid, memUid)) {//只能修改自己的名片
			return true;
		}
		return false;
	}
}
