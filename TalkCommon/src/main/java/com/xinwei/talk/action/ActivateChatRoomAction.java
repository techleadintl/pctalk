package com.xinwei.talk.action;

import java.awt.event.ActionEvent;

import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.ChatManager;
import com.xinwei.talk.model.XWObject;

public class ActivateChatRoomAction extends XAction<XWObject> {
	private static final long serialVersionUID = -5632687033734835465L;

	public ActivateChatRoomAction() {
		setLabel(Res.getMessage("menu.talk.open"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ChatManager.getInstance().activateChatRoom(obj.getJid());
	}

}
