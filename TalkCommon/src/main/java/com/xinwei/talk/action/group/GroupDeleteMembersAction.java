//package com.xinwei.talk.action.group;
//
//import java.awt.event.ActionEvent;
//
//import com.xinwei.common.lang.StringUtil;
//import com.xinwei.talk.common.LAF;
//import com.xinwei.talk.common.Res;
//import com.xinwei.talk.manager.TalkManager;
//import com.xinwei.talk.model.Group;
//import com.xinwei.talk.ui.chat.ChatFrame;
//import com.xinwei.talk.ui.main.dialog.member.MemberDeleteDialog;
//
///**
// * 
// * 剔除组成员
// */
//public class GroupDeleteMembersAction extends GroupAction {
//	private static final long serialVersionUID = -6969381628320014395L;
//
//	public GroupDeleteMembersAction(Group group) {
//		super(group);
//		setLabel(Res.getMessage("menu.group.kicked"));
//		setIcon(LAF.getKickedIcon());
//	}
//
//	@Override
//	public void actionPerformed(ActionEvent e) {
//		ChatFrame chatFrame = TalkManager.getChatManager().getChatFrame();
//		new MemberDeleteDialog(chatFrame, group).setVisible(true);
//	}
//
//	@Override
//	public boolean isVisible() {
//		String subsId = TalkManager.getVCard().getUid();
//		if (StringUtil.isEmpty(subsId)) {
//			return false;
//		}
//		String creatorUid = group.getCreatorUid();
//		if (!subsId.equals(creatorUid)) {
//			return false;
//		}
//		return true;
//	}
//}
