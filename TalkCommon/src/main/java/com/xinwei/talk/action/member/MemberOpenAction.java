/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月6日 下午2:15:57
 * 
 ***************************************************************/
package com.xinwei.talk.action.member;

import java.awt.event.ActionEvent;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;

public class MemberOpenAction extends MemberAction {
	private static final long serialVersionUID = -6969381628320014395L;

	public MemberOpenAction() {
		setLabel(Res.getMessage("menu.talk.start.chat"));
		setIcon(LAF.getTalkChatIcon()[0]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		TalkManager.getChatManager().activateChatRoom(obj.getMemJid(), obj.getMemNick(), obj.getUserKey(), false, true);
	}

	@Override
	public boolean isVisible() {
		String loginUid = TalkManager.getVCard().getUid();
		String memUid = obj.getUid();
		if (StringUtil.equals(memUid, loginUid)) {//自己不跟自己聊天
			return false;
		}

		return true;
	}
}
