/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月16日 下午3:52:57
 * 
 ***************************************************************/
package com.xinwei.talk.action.talk;

import java.awt.event.ActionEvent;

import javax.swing.JFrame;

import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil2;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.ui.main.dialog.user.TalkInfoDialog;

public class TalkInfoAction extends TalkAction {
	public TalkInfoAction() {
		setLabel(Res.getMessage("dialog.title.namecard.talk"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String key = "TalkInfo_" + obj.getTel();
		JFrame dialog = TalkManager.getLocalManager().get(key);
		if (dialog == null) {
			dialog = TalkUtil2.getComponent("talkInfo", new Class[] { Talk.class }, new Object[] { obj });
			TalkManager.getLocalManager().put(key, dialog);
		}
		dialog.setVisible(true);
	}
}
