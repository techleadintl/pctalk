package com.xinwei.talk.action.group;

import java.awt.event.ActionEvent;

import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.ui.chat.ChatFrame;
import com.xinwei.talk.ui.main.dialog.member.MemberAddDialog;

/**
 * 
 * 添加组成员
 */
public class GroupAddMembersAction extends GroupAction {
	private static final long serialVersionUID = -6969381628320014395L;

	public GroupAddMembersAction() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ChatFrame chatFrame = TalkManager.getChatManager().getChatContainer().getChatFrame();
		new MemberAddDialog(chatFrame, obj).setVisible(true);
	}

	@Override
	public boolean isVisible() {
		return true;
	}
}
