package com.xinwei.talk.action.group;

import java.awt.event.ActionEvent;

import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.ChatManager;
import com.xinwei.talk.manager.TalkManager;

public class GroupOpenRoomAction extends GroupAction {

	private static final long serialVersionUID = 6745133608328840712L;

	public GroupOpenRoomAction() {
		setLabel(Res.getMessage("menu.group.start.chat"));
		setIcon(LAF.getGroupChatIcon()[0]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (obj == null)
			return;

		ChatManager chatManager = TalkManager.getChatManager();

		chatManager.activateChatRoom(obj);
	}
}
