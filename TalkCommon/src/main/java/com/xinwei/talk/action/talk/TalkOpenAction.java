/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月16日 下午3:52:57
 * 
 ***************************************************************/
package com.xinwei.talk.action.talk;

import java.awt.event.ActionEvent;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.ChatManager;
import com.xinwei.talk.manager.TalkManager;

public class TalkOpenAction extends TalkAction {
	public TalkOpenAction() {
		setLabel(Res.getMessage("menu.talk.start.chat"));
		setIcon(LAF.getTalkChatIcon()[0]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ChatManager chatManager = TalkManager.getChatManager();

		chatManager.activateChatRoom(obj);
	}

	public boolean isVisible() {
		String loginUid = TalkManager.getVCard().getUid();
		String memUid = obj.getUid();
		if (StringUtil.equals(memUid, loginUid)) {//自己不跟自己聊天
			return false;
		}

		return true;
	}
}
