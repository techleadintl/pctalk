package com.xinwei.talk.action.member;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Talk;

/**
 * 
 * 添加组成员
 */
public class MemberInviteAction extends MemberAction {
	private static final long serialVersionUID = -6969381628320014395L;
	LocalManager localManager = TalkManager.getLocalManager();

	public MemberInviteAction() {
		setLabel(Res.getMessage("menu.group.invite"));
		setIcon(LAF.getGroupInviteIcon()[0]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		final String memJid = obj.getMemJid();
		Talk talk = localManager.getTalkByJid(memJid);
		if (talk != null) {
			return;
		}

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				try {
					String phone = TalkUtil.getUserTel(memJid);
					Talk talk = TalkManager.getTalkService().getTalk(phone);
					if (talk == null) {
						return;
					}
					try {
						TalkManager.getTalkService().addTalk(talk, obj.getMemNick());
						final List<Talk> list = new ArrayList<Talk>();
						list.add(talk);
						TalkManager.getVCardManager().addTalk(list);
					} catch (Exception e) {//如果上传失败，则删除
						Log.error(e);
					}

				} catch (Exception e1) {
				}
			}
		});

	}

	@Override
	public boolean isVisible() {
		if (group == null)
			return false;
		String loginUid = TalkManager.getVCard().getUid();
		String memUid = obj.getUid();
		if (StringUtil.equals(memUid, loginUid)) {//自己不邀请自己
			return false;
		}
		String memJid = obj.getMemJid();
		Talk talk = localManager.getTalkByJid(memJid);//已经是好友了，不需要邀请了。
		if (talk != null) {
			return false;
		}
		return true;
	}
}
