package com.xinwei.talk.action.member;

import java.awt.event.ActionEvent;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;

/**
 * 
 * 剔除组成员
 */
public class MemberDeleteAction extends MemberAction {
	private static final long serialVersionUID = -6969381628320014395L;

	public MemberDeleteAction() {
		setLabel(Res.getMessage("menu.group.kicked"));
		setIcon(LAF.getGroupKickIcon()[0]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (group == null)
			return;
		try {
			TalkManager.getTalkService().deleteMember(group.getCreatorUid(), group.getId(), obj.getUid());
		} catch (Exception e1) {
			Log.error(e1);
		}
	}

	@Override
	public boolean isVisible() {
		if (group == null)
			return false;
		String creatorUid = group.getCreatorUid();
		String memUid = obj.getUid();
		if (StringUtil.equals(memUid, creatorUid)) {//創建者不能刪除
			return false;
		}

		String loginUid = TalkManager.getVCard().getUid();
		if (!StringUtil.equals(loginUid, creatorUid)) {//必须是管理员才有权能删除
			return false;
		}
		return true;
	}
}
