package com.xinwei.talk.action.group;

import java.awt.event.ActionEvent;

import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.ui.main.dialog.group.GroupUpdateDialog;

/**
 * 
 * 修改群名称
 */
public class GroupUpdateAction extends GroupAction {
	private static final long serialVersionUID = -6969381628320014395L;

	public GroupUpdateAction() {
		setLabel(Res.getMessage("menu.group.update.group.nick"));
		setIcon(LAF.getGroupUpdateIcon()[0]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		GroupUpdateDialog memberDialog = new GroupUpdateDialog(obj);
		memberDialog.setVisible(true);
	}

	@Override
	public boolean isVisible() {
		return true;
	}
}
