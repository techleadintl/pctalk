package com.xinwei.talk.action.group;

import java.awt.event.ActionEvent;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.spark.ui.MessageDialog;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;

/**
 * 解散群组
 *
 */
public class GroupDissolveAction extends GroupAction {
	private static final long serialVersionUID = -6969381628320014395L;

	public GroupDissolveAction() {
		setLabel(Res.getMessage("menu.group.dissolve"));
		setIcon(LAF.getGroupDissolveIcon()[0]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!isVisible()) {
			return;
		}

		String subsId = TalkManager.getVCard().getUid();
		try {
			int r = TalkManager.getTalkService().deleteGroup(subsId, obj.getId());
			//			if (r == 0) {
			//				TalkManager.getVCardManager().deleteGroup(group);
			//			}
		} catch (Exception e1) {
			MessageDialog.showErrorDialog(e1);
		}
	}

	@Override
	public boolean isVisible() {
		String subsId = TalkManager.getVCard().getUid();
		if (StringUtil.isEmpty(subsId)) {
			return false;
		}
		String creatorUid = obj.getCreatorUid();
		if (!subsId.equals(creatorUid)) {
			return false;
		}
		return true;
	}
}
