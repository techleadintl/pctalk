package com.xinwei.talk.action.talk;

import com.xinwei.talk.action.XAction;
import com.xinwei.talk.model.Talk;

public abstract class TalkAction extends XAction<Talk> {
	private static final long serialVersionUID = 1L;

	public boolean isVisible() {
		return true;
	}

}
