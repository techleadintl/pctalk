package com.xinwei.talk.action;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;

public abstract class XAction<T> extends AbstractAction {
	private static final long serialVersionUID = 1L;

	protected T obj;

	public XAction() {
	}

	public XAction(T t) {
		this.obj = t;
	}

	public T getObj() {
		return obj;
	}

	public void setObj(T obj) {
		this.obj = obj;
	}

	public void setObjs(Object... objs) {
		this.obj = (T) objs[0];
	}

	public boolean isVisible() {
		return true;
	}

	public void setLabel(String label) {
		putValue(Action.NAME, label);
	}

	public void setIcon(ImageIcon icon) {
		putValue(Action.SMALL_ICON, icon);
	}

}
