package com.xinwei.talk.listener;

import com.xinwei.talk.model.VCard;

public interface VCardListener {

	void vcardChanged(VCard vcard);
}