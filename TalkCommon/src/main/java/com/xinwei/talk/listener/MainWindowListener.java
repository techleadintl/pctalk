/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 上午9:32:45
 * 
 ***************************************************************/
package com.xinwei.talk.listener;

/**
 * The <code>MainWindowListener</code> interface is one of the interfaces extension writers use to add functionality to Talk.
 * <p/>
 * In general, you implement this interface in order to listen for Window events that could otherwise not be listened to by attaching to the MainWindow due to security restrictions.
 */
public interface MainWindowListener {

	/**
	 * Invoked by the <code>MainWindow</code> when it is about the shutdown. When invoked, the <code>MainWindowListener</code> should do anything necessary to persist their current state.
	 * <code>MainWindowListeners</code> authors should take care to ensure that any extraneous processing is not performed on this method, as it would cause a delay in the shutdown process.
	 *
	 * @see com.xinwei.talk.ui.main.MainWindow
	 */
	void shutdown();

	/**
	 * Invoked by the <code>MainWindow</code> when it has been activated, such as when it is coming out of a minimized state. When invoked, the <code>MainWindowListener</code> should do anything
	 * necessary to smoothly transition back to the application.
	 *
	 * @see com.xinwei.talk.ui.main.MainWindow
	 */
	void mainWindowActivated();

	/**
	 * Invoked by the <code>MainWindow</code> when it has been minimized in the toolbar.
	 *
	 * @see com.xinwei.talk.ui.main.MainWindow
	 */
	void mainWindowDeactivated();

}
