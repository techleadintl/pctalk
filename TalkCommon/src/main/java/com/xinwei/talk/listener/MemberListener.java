package com.xinwei.talk.listener;

import java.util.List;

import com.xinwei.talk.model.Member;

public interface MemberListener {

	/**
	 * Called when entries are added.
	 *
	 */
	public void add(String gid,List<Member> list);

	/**
	 * Called when entries are updated.
	 *
	 */
	public void update(String gid,List<Member> list);

	/**
	 * Called when entries are removed.
	 *
	 */
	public void delete(String gid,List<Member> list);

}