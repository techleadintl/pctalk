package com.xinwei.talk.listener;

import com.xinwei.talk.ui.chat.room.ChatRoom;

public class ChatRoomAdapter implements ChatRoomListener {

	public void chatRoomOpened(ChatRoom room) {

	}

	public void chatRoomFileTabClosed(ChatRoom room) {
	}

	public void chatRoomClosed(ChatRoom room) {

	}

	@Override
	public boolean canChatRoomClosed(ChatRoom room) {
		return true;
	}

}
