/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月19日 下午2:26:38
 * 
 ***************************************************************/
package com.xinwei.talk.listener;

import java.util.List;

import com.xinwei.talk.model.avatar.Avatar;

public interface AvatarListener {
	public void update(List<Avatar> avatars);
}
