package com.xinwei.talk.listener;

import java.util.Collection;

import com.xinwei.talk.model.Contact;

public interface ContactListener {

	/**
	 * Called when entries are added.
	 *
	 */
	public void add(Collection<Contact> contacts);

	/**
	 * Called when entries are updated.
	 *
	 */
	public void update(Collection<Contact> contacts);

	/**
	 * Called when entries are removed.
	 *
	 */
	public void delete(Collection<Contact> contacts);

}