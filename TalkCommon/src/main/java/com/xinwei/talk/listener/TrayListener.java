/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月16日 下午1:03:17
 * 
 ***************************************************************/
package com.xinwei.talk.listener;

public interface TrayListener {
	public void login();
}
