package com.xinwei.talk.listener;

import org.jivesoftware.smack.packet.Message;

public interface ChatMessageHandler {
	void messageReceived(Message message);	
}
