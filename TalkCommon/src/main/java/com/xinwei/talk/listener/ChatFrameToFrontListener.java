package com.xinwei.talk.listener;

import com.xinwei.talk.ui.chat.ChatFrame;

public interface ChatFrameToFrontListener {
    
    /**
     * Update. From now on the chat frame will stay on top, or not
     * @param active
     */
    void updateStatus(boolean active);
    
    /**
     * the observer should register on this chatframe component
     * @param chatframe
     */
    void registeredToFrame(ChatFrame chatframe);
}
