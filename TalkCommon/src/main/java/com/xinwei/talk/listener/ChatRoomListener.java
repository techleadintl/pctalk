package com.xinwei.talk.listener;

import com.xinwei.talk.ui.chat.room.ChatRoom;

/**
 * The <code>ChatRoomListener</code> interface is one of the interfaces extension writers use to add functionality to Spark.
 * <p/>
 * In general, you implement this interface in order to listen for ChatRoom activity, such as a ChatRoom opening, closing, or being activated.
 */
public interface ChatRoomListener {

	/**
	 * Invoked by <code>ChatRooms</code> when a new ChatRoom has been opened.
	 *
	 * @param room
	 *            - the <code>ChatRoom</code> that has been opened.
	 */
	void chatRoomOpened(ChatRoom room);

//	void chatRoomFileTabClosed(TalkRoom room);

	/**
	 * Invoke by <code>ChatRooms</code> when a ChatRoom has been closed.
	 *
	 * @param room
	 *            - the <code>ChatRoom</code> that has been closed.
	 */
	void chatRoomClosed(ChatRoom room);

	boolean canChatRoomClosed(ChatRoom room);

}