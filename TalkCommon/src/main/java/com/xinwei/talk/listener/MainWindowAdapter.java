/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 上午9:32:45
 * 
 ***************************************************************/
package com.xinwei.talk.listener;

public abstract class MainWindowAdapter implements MainWindowListener {
	public void shutdown() {
	}

	public void mainWindowActivated() {
	}

	public void mainWindowDeactivated() {
	}
}
