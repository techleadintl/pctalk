package com.xinwei.talk.listener;

import com.xinwei.talk.model.message.TalkMessage;

public interface MessageListener {

	void messageReceived(String fromJid, TalkMessage message);

	void messageSent(String toJid, TalkMessage message);
}