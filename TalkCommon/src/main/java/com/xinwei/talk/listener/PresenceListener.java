package com.xinwei.talk.listener;

import org.jivesoftware.smack.packet.Presence;

public interface PresenceListener {

    void presenceReceived(String fromJid, Presence presence);
}