/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月27日 下午1:16:19
 * 
 ***************************************************************/
package com.xinwei.talk.listener;

import java.awt.event.KeyEvent;

import com.xinwei.talk.ui.chat.room.CommonChatRoom;

public interface ChatRoomKeyListener {

	public void keyTyped(CommonChatRoom room, KeyEvent e);

	public void keyPressed(CommonChatRoom room, KeyEvent e);

	public void keyReleased(CommonChatRoom room, KeyEvent e);

}
