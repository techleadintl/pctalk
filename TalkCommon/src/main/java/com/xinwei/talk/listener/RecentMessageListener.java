/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月17日 下午8:49:11
 * 
 ***************************************************************/
package com.xinwei.talk.listener;

import java.util.Date;
import java.util.List;

import com.xinwei.talk.model.message.TalkMessage;

public interface RecentMessageListener {
	void message(Date serverTime,List<TalkMessage> tmList);
}
