package com.xinwei.talk.listener;

import java.util.Collection;

import com.xinwei.talk.model.XWObject;

public interface ObjectListener {

	/**
	 * Called when entries are added.
	 *
	 */
	public void add(Collection<? extends XWObject> objects);

	/**
	 * Called when entries are updated.
	 *
	 */
	public void update(Collection<? extends XWObject> objects);

	/**
	 * Called when entries are removed.
	 *
	 */
	public void delete(Collection<? extends XWObject> objects);

}