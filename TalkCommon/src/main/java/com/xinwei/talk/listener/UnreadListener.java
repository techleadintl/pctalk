/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月25日 上午10:06:14
 * 
 ***************************************************************/
package com.xinwei.talk.listener;

import com.xinwei.common.vo.Entry;
import com.xinwei.talk.model.message.TalkMessage;

public interface UnreadListener {
	public void doUnreadMessage(String jid, Entry<Integer, TalkMessage> unreadMessage);
}
