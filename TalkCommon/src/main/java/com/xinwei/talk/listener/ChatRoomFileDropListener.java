package com.xinwei.talk.listener;
import java.awt.Component;
import java.io.File;
import java.util.Collection;

/**
 * The <code>FileDropListener</code> interface is one of the interfaces extension
 * writers use to add functionality to Spark.
 * <p/>
 * In general, you implement this interface in order to listen
 * for file drops onto components.
 */
public interface ChatRoomFileDropListener {

    /**
     * Called when a file(s) has been Drag and Dropped onto a component.
     *
     * @param files     the Collection of Files.
     * @param component the Component the files were dropped on.
     */
    void filesDropped(Collection<File> files, Component component);

}
