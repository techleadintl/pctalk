package com.xinwei.talk.ui.main.dialog.member;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.ScrollPaneConstants;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillScrollPane;
import com.xinwei.common.lookandfeel.component.text.McWillSearchTextField;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
import com.xinwei.spark.ui.MessageDialog;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.ui.main.dialog.member.model.CaptionSort;
import com.xinwei.talk.ui.main.dialog.member.model.MemberInfo;
import com.xinwei.talk.ui.main.dialog.member.model.MemberInfoSort;
import com.xinwei.talk.ui.main.dialog.member.ui.LeftMemItemUI;
import com.xinwei.talk.ui.main.dialog.member.ui.RightMemItemUI;
import com.xinwei.talk.ui.main.dialog.member.util.MemUtil;
import com.xinwei.talk.ui.util.component.XWDialogTitle;
import com.xinwei.talk.ui.util.component.dialog.XWDialog;

@SuppressWarnings("serial")
public class MemberAddDialog extends XWDialog {

	private JPanel rightMemsPanel;

	private JPanel leftMemsPanel;

	private JLabel rightTitleLabel;

	private McWillSearchTextField findField;

	private Group group;

	List<MemberInfo> memberInfoList;

	private String HOLD_TEXT = Res.getMessage("input.text.filter.telandname");

	public MemberAddDialog(JFrame owner, Group group) throws HeadlessException {
		super(owner, "", false);

		JRootPane rootPane = getRootPane();
		((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

		setSize(new Dimension(575, 450));

		this.group = group;

		createDialog();

		setLocationRelativeTo(owner);

	}

	@Override
	protected JPanel createDialogContainer() {
		return new McWillContentPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				LAF.paintMainAddGroupDialog(this, (Graphics2D) g);
			}
		};
	}

	@Override
	protected JPanel createDialogContent() {
		JPanel dialogPanel = new JPanel(new BorderLayout());
		XWDialogTitle titlePanel = new XWDialogTitle(this, Res.getMessage("title.member.add"));
		titlePanel.setBorder(BorderFactory.createEmptyBorder(6, 10, 6, 0));
		dialogPanel.add(titlePanel, BorderLayout.NORTH);

		dialogPanel.add(createInputPanel(), BorderLayout.CENTER);

		return dialogPanel;
	}

	protected JPanel createInputPanel() {
		JPanel inputPanel = new JPanel();

		inputPanel.setPreferredSize(new Dimension(650, 450));

		inputPanel.setLayout(new GridLayout(1, 2));

		final JPanel leftPanel = createLeftUI();

		JPanel rightPane = createRightUI();

		inputPanel.add(leftPanel);

		inputPanel.add(rightPane);

		initLeftUI();

		updateRightUI();

		return inputPanel;
	}

	private JPanel createLeftUI() {
		final JPanel leftPanel = new JPanel(new BorderLayout(5, 15));

		leftPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 0, 5));

		//左侧
		findField = new McWillSearchTextField(HOLD_TEXT, LAF.getTextSearchIcon(), LAF.getDeleteIcon()[0]) {
			@Override
			public void doSearch(String text) {
				doFilter(text);
			}

		};
		findField.setPreferredSize(new Dimension(0, 27));

		leftMemsPanel = new JPanel();
		leftMemsPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP, 0, 0, true, false));

		McWillScrollPane scrollIndicator = new McWillScrollPane(leftMemsPanel);

		leftPanel.add(findField, BorderLayout.NORTH);
		leftPanel.add(scrollIndicator, BorderLayout.CENTER);

		return leftPanel;
	}

	private JPanel createRightUI() {
		JPanel rightPanel = new JPanel(new BorderLayout());
		rightPanel.setBorder(BorderFactory.createEmptyBorder(15, 5, 0, 15));
		
		rightTitleLabel = new JLabel();
		rightTitleLabel.setPreferredSize(new Dimension(0, 27));

		rightMemsPanel = new JPanel();
		rightMemsPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP, 0, 1, true, false));

		McWillScrollPane rightScrollPane = new McWillScrollPane(rightMemsPanel);
		rightScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		rightScrollPane.setBorder(null);

		rightPanel.add(rightTitleLabel, BorderLayout.NORTH);

		rightPanel.add(rightScrollPane, BorderLayout.CENTER);

		return rightPanel;
	}

	private void doFilter(String text) {
		if (CollectionUtil.isEmpty(memberInfoList))
			return;

		List<MemberInfo> filter = null;

		if (StringUtil.isEmpty(text)) {
			filter = memberInfoList;
		} else {
			filter = new ArrayList<>();
			for (MemberInfo memberInfo : memberInfoList) {
				boolean flag = memberInfo.search(text);
				if (flag) {
					filter.add(memberInfo);
				}
			}
		}

		updateLeftUI(filter);
	}

	@Override
	protected boolean onOkPressed() {
		Component[] components = rightMemsPanel.getComponents();
		if (components == null || components.length == 0)
			return false;

		List<Member> memberList = new ArrayList<Member>();
		for (Component component : components) {
			MemberInfo memberInfo = (MemberInfo) ((JComponent) component).getClientProperty("data");
			if (memberInfo == null) {
				continue;
			}

			Member m2 = new Member();
			m2.setUid(memberInfo.uid);
			m2.setMemNick(memberInfo.displayName);
			m2.setMemJid(memberInfo.jid);
			m2.setMemRole(Member.ROLE_USER);

			memberList.add(m2);
		}
		if (memberList.isEmpty()) {
			//TODO:添加提示信息
			return false;
		}

		try {
			TalkManager.getTalkService().addGroupMembers(group, memberList);

			MemberAddDialog.this.dispose();

			//			XEventDispatcher.dispatchAsynEvent("", evt);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			//TODO:添加提示信息
			MessageDialog.showErrorDialog(e);
			return false;
		}

	}

	private void onLeftClick(MemberInfo memberInfo, final LeftMemItemUI leftItem, boolean selected) {
		if (selected) {
			RightMemItemUI rightItem = new RightMemItemUI(memberInfo) {
				@Override
				public void onRemoveClick() {
					removeSelectedItem(leftItem);
				}
			};

			rightItem.putClientProperty("data", memberInfo);

			leftItem.putClientProperty("right", rightItem);

			rightMemsPanel.add(rightItem);

			updateRightUI();

		} else {
			removeSelectedItem(leftItem);
		}

	}

	private void changeLeftBackgroud(final JPanel memberPanel, Component source) {
		Component[] components = memberPanel.getComponents();
		for (Component component : components) {
			if (!(component instanceof LeftMemItemUI))
				continue;

			if (component == source) {
				component.setBackground(McWillTheme.getThemeColor().getSubDarkColor());
			} else {
				component.setBackground(McWillTheme.getThemeColor().getLightColor());
			}
		}
	}

	private void removeSelectedItem(final LeftMemItemUI leftMemItemUI) {
		JComponent rightItem = (JComponent) leftMemItemUI.getClientProperty("right");
		if (rightItem != null) {
			leftMemItemUI.putClientProperty("right", null);
			rightMemsPanel.remove(rightItem);
			updateRightUI();
		}
		leftMemItemUI.setCheckLabelImage(false);
	}

	private void updateRightUI() {
		int componentCount = rightMemsPanel.getComponentCount();
		if (componentCount == 0) {
			rightTitleLabel.setText(Res.getMessage("label.group.select.member"));
			if (okButton != null) {
				okButton.setEnabled(false);
			}
		} else {
			rightTitleLabel.setText(Res.getMessage("label.group.select.member2", componentCount));
			if (okButton != null) {
				okButton.setEnabled(true);
			}
		}

		rightMemsPanel.updateUI();
	}

	private void initLeftUI() {
		memberInfoList = new ArrayList<>();
		Collection<Talk> camtalks = TalkManager.getLocalManager().getTalkList();

		for (Talk camtalk : camtalks) {
			MemberInfo memberInfo = MemUtil.getMemberInfo(camtalk.getJid(), camtalk.getUid(), TalkUtil.getDisplayName(camtalk.getJid()));
			memberInfoList.add(memberInfo);
		}
		updateLeftUI(memberInfoList);
	}

	private void updateLeftUI(List<MemberInfo> memberInfoList) {
		Map<String, List<MemberInfo>> map = new HashMap<String, List<MemberInfo>>();
		//按照首字符进行分组成map
		for (MemberInfo memberInfo : memberInfoList) {
			List<MemberInfo> list = map.get(memberInfo.caption);
			if (list == null) {
				list = new ArrayList<>();
				map.put(memberInfo.caption, list);
			}
			list.add(memberInfo);
		}
		List<String> captionList = new ArrayList<String>(map.keySet());

		Collections.sort(captionList, new CaptionSort());

		leftMemsPanel.removeAll();
		leftMemsPanel.updateUI();

		MemberInfoSort c = new MemberInfoSort();
		for (String caption : captionList) {
			JLabel captionLabel = new JLabel(caption);
			captionLabel.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
			captionLabel.setOpaque(true);
			captionLabel.setBackground(McWillTheme.getThemeColor().getSubLightColor());

			leftMemsPanel.add(captionLabel);

			//对于同一个首字母进行排序
			List<MemberInfo> list = map.get(caption);
			Collections.sort(list, c);

			for (final MemberInfo memberInfo : list) {
				Member groupMember = TalkManager.getLocalManager().getMemberByUid(group.getId(), memberInfo.uid);
				boolean check = false;
				if (groupMember != null) {
					check = true;
				}
				LeftMemItemUI leftMemItemUI = new LeftMemItemUI(memberInfo, check) {
					@Override
					public void onMemItemClick() {
						changeLeftBackgroud(leftMemsPanel, this);
					}

					@Override
					public void onMemItemChecked(boolean oldCheck, boolean newCheck) {
						onLeftClick(memberInfo, this, newCheck);
					}
				};

				leftMemsPanel.add(leftMemItemUI);
			}
		}
	}

}