package com.xinwei.talk.ui.skin.skin;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.component.McWillImageDialog;
import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
import com.xinwei.common.lookandfeel.layout.WrapFlowLayout;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.talk.common.LAF;

@SuppressWarnings("serial")
public class SkinStylePanel extends JPanel {

	private static final Dimension PREFERRED_SIZE = new Dimension(85, 85);

	public SkinStylePanel(final Window parentDialog, List<String> icons) {
		setOpaque(false);
		setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		WrapFlowLayout mgr = new WrapFlowLayout(5, 5);
		mgr.setHgap(WrapFlowLayout.LEFT);
		setLayout(mgr);

		addIconButtons(icons);

		addChooseButton(parentDialog);
	}

	private void addIconButtons(List<String> icons) {
		for (final String res : icons) {
			final URL url = LAF.getURL(res);
			final byte[] bytes = IOUtil.readBytes(url);

			byte[] resizeBytes = ImageUtil.resize(bytes, PREFERRED_SIZE.width, PREFERRED_SIZE.height);
			JLabel item = new McWillRolloverLabel(new ImageIcon(resizeBytes), false);
			item.setPreferredSize(PREFERRED_SIZE);
			add(item);
			item.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					McWillTheme.setThemeImage(bytes);
				}
			});
		}
	}

	private void addChooseButton(final Window parentDialog) {
		final ImageIcon icon = LAF.getAddSkinImageIcon();
		final Image image = icon.getImage();
		JButton imageChooseButton = new McWillIconButton() {
			@Override
			protected void paintComponent(Graphics g) {
				g.drawImage(image, 0, 0, getWidth() - 1, getHeight() - 1, null);
			}
		};
		imageChooseButton.setPreferredSize(PREFERRED_SIZE);
		add(imageChooseButton);

		imageChooseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser choose = new McWillImageDialog(parentDialog);

				setTheme(parentDialog, choose);
			}

			private void setTheme(final Window window, JFileChooser choose) {
				if (choose.showOpenDialog(window) == JFileChooser.APPROVE_OPTION) {
					File file = choose.getSelectedFile();
					if (file != null && file.exists()) {
						String imageType = ImageUtil.getImageType(file);
						if (imageType != null) {
							McWillTheme.setThemeImage(IOUtil.readBytes(file));
							return;
						} else {
							JOptionPane.showMessageDialog(window, "Please choose a valid image file.", "", JOptionPane.WARNING_MESSAGE);
							setTheme(window, choose);
						}
					}
				}
			}

		});
	}
}
