/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月5日 下午4:01:52
 * 
 ***************************************************************/
package com.xinwei.talk.ui.skin.balloon;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

public class BalloonPanel extends JPanel {
	public BalloonPanel(final Window skinDialog) {
		setLayout(new BorderLayout());

		List<Color> colors = new ArrayList<Color>();
		colors.add(new Color(230, 230, 230));
		colors.add(new Color(202, 202, 202));

		colors.add(new Color(152, 225, 101));
		colors.add(new Color(142, 212, 6));

		colors.add(new Color(255, 204, 255));
		colors.add(new Color(255, 153, 255));

		colors.add(new Color(255, 204, 204));
		colors.add(new Color(255, 153, 153));

		colors.add(new Color(255, 255, 204));
		colors.add(new Color(255, 255, 153));

		colors.add(new Color(204, 255, 255));
		colors.add(new Color(153, 255, 255));

		add(new BallonStylePanel(skinDialog), BorderLayout.CENTER);
		add(new BalloonColorPanel(skinDialog, colors), BorderLayout.SOUTH);

	}

}
