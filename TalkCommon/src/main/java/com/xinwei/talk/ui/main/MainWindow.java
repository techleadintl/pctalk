/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 上午9:05:19
 * 
 ***************************************************************/
package com.xinwei.talk.ui.main;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.image.BufferedImage;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Presence;

import com.xinwei.Startup;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillRadioGroup;
import com.xinwei.common.lookandfeel.component.McWillTitlePane;
import com.xinwei.common.lookandfeel.component.McWillWindowButtonUI;
import com.xinwei.common.lookandfeel.component.McWillWindowTitleActions;
import com.xinwei.common.lookandfeel.contentpanel.McWillWindowContentPanel;
import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
import com.xinwei.spark.ui.InputTextAreaDialog;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.listener.MainWindowListener;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.conf.UserConfig;
import com.xinwei.talk.ui.chat.ChatContainer;
import com.xinwei.talk.ui.chat.ChatFrame;
import com.xinwei.talk.ui.plugin.global.screen.XScreenShot;
import com.xinwei.talk.ui.util.component.XWConfirmDialog;

/**
 * The <code>MainWindow</code> class acts as both the DockableHolder and the
 * proxy to the Workspace in Talk.
 */
public final class MainWindow extends JFrame implements McWillWindowButtonUI, McWillWindowTitleActions {
	private static final long serialVersionUID = -6062104959613603510L;

	private final Set<MainWindowListener> listeners = new HashSet<MainWindowListener>();

	private boolean focused;

	private JSplitPane splitPane;

	private static MainWindow mainWindow;
	private static final Object LOCK = new Object();

	/**
	 * Returns the singleton instance of <CODE>MainWindow</CODE>, creating it if
	 * necessary.
	 * <p/>
	 *
	 * @return the singleton instance of <Code>MainWindow</CODE>
	 */
	public static MainWindow getInstance() {
		synchronized (LOCK) {
			if (null == mainWindow) {
				mainWindow = new MainWindow(Res.getMessage("application.name"));
				mainWindow.setType(JFrame.Type.UTILITY);//隐藏任务栏图标  

			}
		}
		return mainWindow;
	}

	/**
	 * Constructs the UI for the MainWindow. The MainWindow UI is the container
	 * for the entire Spark application.
	 *
	 * @param tabTitle
	 *            the tabTitle of the frame.
	 * @param icon
	 *            the icon used in the frame.
	 */
	@SuppressWarnings("serial")
	private MainWindow(String title) {
		setTitle(title);

		setIconImage(LAF.getApplicationImage().getImage());
		McWillWindowContentPanel contentPane = new McWillWindowContentPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				LAF.paintMainFrame(this, (Graphics2D) g);
			}
		};
		/*setContentPane(contentPane);

		contentPane.setTitleIconVisible(true);

		// Add Workspace Container
		setMinimumSize(new Dimension(300, 500));
		setSize(310, 750);

		final Toolkit toolkit = java.awt.Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();

		setLocation(screenSize.width - getWidth() - 10, (screenSize.height - getHeight()) / 2);
*/
		// Setup WindowListener to be the proxy to the actual window listener
		// which cannot normally be used outside of the Window component because
		// of protected access.

		addWindowListener(new WindowAdapter() {
			public void windowActivated(WindowEvent e) {
				for (MainWindowListener listener : listeners) {
					listener.mainWindowActivated();
				}
			}

			public void windowClosing(WindowEvent e) {
				setVisible(false);
			}
		});

		addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent e) {
				focused = true;
			}

			public void windowLostFocus(WindowEvent e) {
				focused = false;
			}
		});
		
		
		// added May 10 from here
		
		Toolkit _toolkit = Toolkit.getDefaultToolkit();
		_toolkit.addAWTEventListener(new java.awt.event.AWTEventListener() {
			public void eventDispatched(AWTEvent event) {
				if (event.getClass() == KeyEvent.class) {
					KeyEvent kE = ((KeyEvent) event);

					UserConfig userConfig = TalkManager.getUserConfig();
					String scr_hotkey = userConfig.getShortKeyValue("Screenshot");
					String main_win_hotkey = userConfig.getShortKeyValue("MainWindow");
					
					String[] main_win_keys= main_win_hotkey.split("\\s");
					String[] scr_keys= scr_hotkey.split("\\s");
					
					int  scr_keyCode = 0;
					int  main_win_keyCode = 0;
					
					if(scr_keys.length == 3){
						scr_keyCode = java.awt.event.KeyEvent.getExtendedKeyCodeForChar(scr_keys[2].charAt(0));
					}

					if(main_win_keys.length == 3){
						main_win_keyCode = java.awt.event.KeyEvent.getExtendedKeyCodeForChar(main_win_keys[2].charAt(0));
					}

					if (kE.getKeyCode() == main_win_keyCode && kE.isControlDown() && kE.getID() == KeyEvent.KEY_PRESSED) {
						final MainWindow mainWindow = TalkManager.getMainWindow();
						System.out.println("Ctrl +" + kE.getKeyCode());

						if (mainWindow.isVisible() == true) {	
							mainWindow.setVisible(false);
						} else {
							mainWindow.setVisible(true);
						}
					}
					else if (kE.getKeyCode() == scr_keyCode && kE.isControlDown() && kE.getID() == KeyEvent.KEY_PRESSED) {
						
						final ChatFrame chatFrame = TalkManager.getChatManager().getChatFrame();
						new XScreenShot() {
							@Override
							public void doFinish(BufferedImage captureImage) {
								if (captureImage != null) {
									captureImage = null;
								}
							}
						}.doCapture(chatFrame);
						
					}
					


				}
			}

		}, java.awt.AWTEvent.KEY_EVENT_MASK);
		
		// to here
		
		toFront();		
		
	}

	/**
	 * Adds a MainWindow listener to {@link MainWindow}. The listener will be
	 * called when either the MainWindow has been minimized, maximized, or is
	 * shutting down.
	 *
	 * @param listener
	 *            the <code>MainWindowListener</code> to register
	 */
	public void addMainWindowListener(MainWindowListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes the specified {@link MainWindowListener}.
	 *
	 * @param listener
	 *            the <code>MainWindowListener</code> to remove.
	 */
	public void removeMainWindowListener(MainWindowListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Notifies all {@link MainWindowListener}s that the <code>MainWindow</code>
	 * is shutting down.
	 */
	private void fireWindowShutdown() {
		for (MainWindowListener listener : listeners) {
			listener.shutdown();
		}
	}

	/**
	 * Prepares Talk for shutting down by first calling all
	 * {@link MainWindowListener}s and setting the Agent to be offline.
	 */
	
	
	
	public void shutdown() {
		final XMPPConnection con = TalkManager.getConnection();

		if (con.isConnected()) {
			// Send disconnect.
			con.disconnect();
		}

		// Notify all MainWindowListeners
		try {
			fireWindowShutdown();
		} catch (Exception ex) {
			Log.error(ex);
		}
		// Close application.
		if (!Res.getBoolean("disable.exit"))
			System.exit(1);

	}

	/**
	 * Prepares Camtalk for shutting down by first calling all
	 * {@link MainWindowListener}s and setting the Agent to be offline.
	 *
	 * @param sendStatus
	 *            true if Camtalk should send a presence with a isInputing
	 *            message.
	 */
	public void logout(boolean sendStatus) {
		final XMPPConnection con = TalkManager.getConnection();
		String status = ""; //null;

		if (con.isConnected() && sendStatus) {
			final InputTextAreaDialog inputTextDialog = new InputTextAreaDialog();
			//status = inputTextDialog.getInput(Res.getMessage("tabTitle.main.status.message"), Res.getMessage("message.main.current.status"), LAF.getImageIcon("talk/images/common/user1_message-24x24.png"), this);
		}

		if (status != null || !sendStatus) {
			// Notify all MainWindowListeners
			try {
				// Set auto-login to false;
				TalkManager.getVCard().setAutoLogin(false);
				TalkManager.saveVCard();

				fireWindowShutdown();
				setVisible(false);
			} finally {
				closeConnectionAndInvoke(status);
			}
		}
		
	}

	/**
	 * Closes the current connection and restarts Spark.
	 *
	 * @param reason
	 *            the reason for logging out. This can be if user gave no
	 *            reason.
	 */
	public void closeConnectionAndInvoke(String reason) {
		final XMPPConnection con = TalkManager.getConnection();
		
		
		if (con.isConnected()) {
			if (reason != null) {
				Presence byePresence = new Presence(Presence.Type.unavailable, reason, -1, null);
				con.disconnect(byePresence);
			} else {
				con.disconnect();
			}
		}
		
			if (!restartApplicationWithScript()) {
				restartApplicationWithJava();
			}
	}

	private File getLibDirectory() throws IOException {
		File jarFile;
		
		try {
			
			jarFile = new File(Startup.class.getProtectionDomain().getCodeSource().getLocation().toURI()); //E:\Project\talk_pc\TalkCommon\bin
			
		} catch (Exception e) {
			Log.error("Cannot get jar file containing the startup class", e);
			return null;
		}
		if (jarFile.getName().endsWith(".exe")) { // for inno setup
			
			return new File(jarFile.getParent(), "lib");
		}
		if (!jarFile.getName().endsWith(".jar")) {
			
			Log.error("The startup class is not packaged in a jar file");
			return null;
		}
		File libDir = jarFile.getParentFile();
		return libDir;
		
		
	}

	private String getClasspath() throws IOException {
		File libDir = getLibDirectory();
		String libPath = libDir.getCanonicalPath();
		String[] files = libDir.list();
		StringBuilder classpath = new StringBuilder();
		for (String file : files) {
			if (file.endsWith(".jar")) {
				classpath.append(libPath + File.separatorChar + file + File.pathSeparatorChar);
			}
		}
		return classpath.toString();
	}

	private String getCommandPath() throws IOException {
		return getLibDirectory().getParentFile().getCanonicalPath();
	}

	public boolean restartApplicationWithScript() {
		
		String command = null;
		try {
			if (SwingUtil.isWindows()) {
				String sparkExe = getCommandPath() + File.separator + Res.getString("short.name") + ".exe";
				if (!new File(sparkExe).exists()) {
					Log.warn("Client EXE file does not exist");
					return false;
				}
				String starterExe = getCommandPath() + File.separator + "starter.exe";
				if (new File(starterExe).exists()) {
					command = starterExe + " \"" + sparkExe + "\"";
				} else {
					command = sparkExe;
				}
			} else if (SwingUtil.isLinux()) {
				command = getCommandPath() + File.separator + Res.getString("short.name");
				if (!new File(command).exists()) {
					Log.warn("Client startup script does not exist");
					return false;
				}
			} else if (SwingUtil.isMac()) {
				command = "open -a " + Res.getString("short.name");
			}


			Runtime.getRuntime().exec(command);
			
			shutdown();
			return true;
			
		} catch (IOException e) {
			Log.error("Error trying to restart application with script", e);
			return false;
		}
		 
	}

	public boolean restartApplicationWithJava() {
		String javaBin = System.getProperty("java.home") + File.separatorChar + "bin" + File.separatorChar + "java";
		try {
			String toExec[] = new String[] { javaBin, "-cp", getClasspath(), Startup.class.getName() };
			Runtime.getRuntime().exec(toExec);
		} catch (Exception e) {
			Log.error("Error trying to restart application with java", e);
			return false;
		}
		shutdown();
		return true;
	}

	/**
	 * Returns true if the Spark window is in focus.
	 *
	 * @return true if the Spark window is in focus.
	 */
	public boolean isInFocus() {
		return focused;
	}

	/**
	 * Return true if the MainWindow is docked.
	 *
	 * @return true if the window is docked.
	 */
	public boolean isDocked() {
		return TalkManager.getUserConfig().getBoolean("DockingEnabled");
	}

	/**
	 * Returns the inner split pane.
	 *
	 * @return the split pane.
	 */
	public JSplitPane getSplitPane() {
		// create the split pane only if required.
		if (splitPane == null) {
			splitPane = new JSplitPane();
		}
		return this.splitPane;
	}

	public void bringFrameIntoFocus() {
		if (!isVisible()) {
			setVisible(true);
		}

		if (getState() == Frame.ICONIFIED) {
			setExtendedState(Frame.NORMAL);
		}

		toFront();
		requestFocus();
	}

	public Image getIconImage() {
		List<Image> iconImages = getIconImages();
		if (iconImages == null || iconImages.isEmpty())
			return null;
		return iconImages.get(0);

	}

	@Override
	public boolean isMax() {
		return false;
	}

	@Override
	public boolean isIconify() {
		return true;
	}

	@Override
	public void iconify(final McWillTitlePane titlePane) {
		//		setState(Frame.ICONIFIED);//释放内存
		//		titlePane.xClose();
		//		//释放之前资源
		//		UIReleaseUtil.freeSwingObject(confirmDialog);
		titlePane.xIconify();
	}

	MainWindowCloseConfirmDialog confirmDialog;

	@Override
	public synchronized void close(McWillTitlePane titlePane) {
				boolean closeMainWindow = TalkManager.getUserConfig().getCloseMainWindow();
				if (closeMainWindow) {
					ChatContainer chatContainer = TalkManager.getChatManager().getChatContainer();
					if (chatContainer.getChatRooms().length != 0) {
						XWConfirmDialog dialog = new XWConfirmDialog(TalkManager.getMainWindow(), Res.getMessage("title.info"), Res.getMessage("main.close.session"));
						dialog.setVisible(true);
						if (dialog.getResult() != XWConfirmDialog.OK) {
							return;
						}
					}
					shutdown();
				} else {
					iconify(titlePane);
				}

		setState(Frame.ICONIFIED);//释放内存
		titlePane.xClose();
	}

	@Override
	public void maximize(McWillTitlePane titlePane) {

	}

	@Override
	public void restore(McWillTitlePane titlePane) {
	}

	public class MainWindowCloseConfirmDialog extends XWConfirmDialog {
		private static final long serialVersionUID = 8447361099667316655L;
		McWillRadioGroup<Integer> radioGroup;

		public MainWindowCloseConfirmDialog() {
			super(TalkManager.getMainWindow(), Res.getMessage("main.close.title"), Res.getMessage("main.close.message"));
		}

		@Override
		protected Component createInputCenter() {
			JPanel radioPanel = new JPanel();
			radioPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.LEFT));
			radioGroup = new McWillRadioGroup<Integer>();
			radioGroup.addRadio(radioPanel, Res.getMessage("main.close.option1"), true, 0);
			radioGroup.addRadio(radioPanel, Res.getMessage("main.close.option2"), false, 1);
			return radioPanel;
		}

		public int getSelected() {
			return radioGroup.getSelected();
		}
	}

}
