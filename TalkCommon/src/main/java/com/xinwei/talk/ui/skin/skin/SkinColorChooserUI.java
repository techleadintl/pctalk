package com.xinwei.talk.ui.skin.skin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.LookAndFeel;
import javax.swing.TransferHandler;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.colorchooser.ColorSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ColorChooserUI;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;

import com.xinwei.talk.ui.skin.skin.colorchooser.ColorWheelPanel;
import com.xinwei.talk.ui.skin.skin.colorchooser.MainPanel;

public class SkinColorChooserUI extends ColorChooserUI {
	protected MainPanel mainPanel;
	protected JColorChooser chooser;
	protected ChangeListener previewListener;
	protected PropertyChangeListener propertyChangeListener;
	protected AbstractColorChooserPanel[] defaultChoosers;
	protected JComponent previewPanel;
	private static TransferHandler defaultTransferHandler = new ColorTransferHandler();
	private MouseListener previewMouseListener;

	public static ComponentUI createUI(JComponent c) {
		return new SkinColorChooserUI();
	}

	public void installUI(JComponent c) {
		chooser = (JColorChooser) c;

		installDefaults();
		installListeners();

		chooser.setLayout(new BorderLayout());
		mainPanel = new MainPanel();
		chooser.add(mainPanel);
		defaultChoosers = createDefaultChoosers();
		chooser.setChooserPanels(defaultChoosers);

		installPreviewPanel();

		chooser.applyComponentOrientation(c.getComponentOrientation());
		
		chooser.setOpaque(false);
	}

	protected AbstractColorChooserPanel[] createDefaultChoosers() {
		return new AbstractColorChooserPanel[] { new ColorWheelPanel(chooser) };
	}

	public void uninstallUI(JComponent c) {
		chooser.remove(mainPanel);

		uninstallListeners();
		uninstallDefaultChoosers();
		uninstallDefaults();

		mainPanel.setPreviewPanel(null);
		if (previewPanel instanceof UIResource) {
			chooser.setPreviewPanel(null);
		}

		mainPanel = null;
		previewPanel = null;
		chooser = null;
	}

	protected void installDefaults() {
		LookAndFeel.installColorsAndFont(chooser, "ColorChooser.background", "ColorChooser.foreground", "ColorChooser.font");
		TransferHandler th = chooser.getTransferHandler();
		if (th == null || th instanceof UIResource) {
			chooser.setTransferHandler(defaultTransferHandler);
		}
	}

	protected void uninstallDefaults() {
		if (chooser.getTransferHandler() instanceof UIResource) {
			chooser.setTransferHandler(null);
		}
	}

	protected void installListeners() {
		propertyChangeListener = createPropertyChangeListener();
		chooser.addPropertyChangeListener(propertyChangeListener);

		previewListener = new PreviewListener();
		chooser.getSelectionModel().addChangeListener(previewListener);

		previewMouseListener = new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (chooser.getDragEnabled()) {
					TransferHandler th = chooser.getTransferHandler();
					th.exportAsDrag(chooser, e, TransferHandler.COPY);
				}
			}
		};

	}

	protected void uninstallListeners() {
		chooser.removePropertyChangeListener(propertyChangeListener);
		chooser.getSelectionModel().removeChangeListener(previewListener);
		previewPanel.removeMouseListener(previewMouseListener);
	}

	protected PropertyChangeListener createPropertyChangeListener() {
		return new PropertyHandler();
	}

	protected void installPreviewPanel() {
		if (previewPanel != null) {
			mainPanel.setPreviewPanel(null);
			previewPanel.removeMouseListener(previewMouseListener);
		}
		previewPanel = chooser.getPreviewPanel();
		if ((previewPanel != null) && (mainPanel != null) && (chooser != null) && (previewPanel.getSize().getHeight() + previewPanel.getSize().getWidth() == 0)) {
			mainPanel.setPreviewPanel(null);
			return;
		}
		if (previewPanel == null || previewPanel instanceof UIResource) {
			// previewPanel = ColorChooserComponentFactory.getPreviewPanel(); //
			// get from table?
			previewPanel = new CooColorPreviewPanel();
			chooser.setPreviewPanel(previewPanel);
		}
		previewPanel.setForeground(chooser.getColor());
		mainPanel.setPreviewPanel(previewPanel);
		previewPanel.addMouseListener(previewMouseListener);
	}

	class PreviewListener implements ChangeListener {
		public void stateChanged(ChangeEvent e) {
			ColorSelectionModel model = (ColorSelectionModel) e.getSource();
			if (previewPanel != null) {
				previewPanel.setForeground(model.getSelectedColor());
				previewPanel.repaint();
			}
		}
	}

	protected void uninstallDefaultChoosers() {
		for (int i = 0; i < defaultChoosers.length; i++) {
			chooser.removeChooserPanel(defaultChoosers[i]);
		}
	}

	public class PropertyHandler implements PropertyChangeListener {

		public void propertyChange(PropertyChangeEvent e) {

			if (e.getPropertyName().equals(JColorChooser.CHOOSER_PANELS_PROPERTY)) {
				AbstractColorChooserPanel[] oldPanels = (AbstractColorChooserPanel[]) e.getOldValue();
				AbstractColorChooserPanel[] newPanels = (AbstractColorChooserPanel[]) e.getNewValue();

				for (int i = 0; i < oldPanels.length; i++) { // remove old
																// panels
					if (oldPanels[i] != null) {
						Container wrapper = oldPanels[i].getParent();
						if (wrapper != null) {
							Container parent = wrapper.getParent();
							if (parent != null)
								parent.remove(wrapper); // remove from hierarchy
							oldPanels[i].uninstallChooserPanel(chooser); // uninstall
						}
					}
				}

				mainPanel.removeAllColorChooserPanels();
				for (int i = 0; i < newPanels.length; i++) {
					if (newPanels[i] != null) {
						mainPanel.addColorChooserPanel(newPanels[i]);
					}
				}

				chooser.applyComponentOrientation(chooser.getComponentOrientation());
				for (int i = 0; i < newPanels.length; i++) {
					if (newPanels[i] != null) {
						newPanels[i].installChooserPanel(chooser);
					}
				}
			}
			if (e.getPropertyName().equals(JColorChooser.PREVIEW_PANEL_PROPERTY)) {
				if (e.getNewValue() != previewPanel) {
					installPreviewPanel();
				}
			}
			if (e.getPropertyName().equals("componentOrientation")) {
				ComponentOrientation o = (ComponentOrientation) e.getNewValue();
				JColorChooser cc = (JColorChooser) e.getSource();
				if (o != (ComponentOrientation) e.getOldValue()) {
					cc.applyComponentOrientation(o);
					cc.updateUI();
				}
			}
		}
	}

	@SuppressWarnings("serial")
	static class ColorTransferHandler extends TransferHandler implements UIResource {
		ColorTransferHandler() {
			super("color");
		}
	}

	@SuppressWarnings("serial")
	class CooColorPreviewPanel extends JPanel implements UIResource {

		/** Creates new form. */
		public CooColorPreviewPanel() {
			initComponents();
			setToolTipText("on");
		}

		public void paintComponent(Graphics g) {
			Insets insets = getInsets();
			int x = insets.left;
			int y = insets.top;
			int w = getWidth() - insets.left - insets.right;
			int h = getHeight() - insets.top - insets.bottom;
			g.setColor(previewBackgroundColor);
			g.fillRect(x + 1, y + 1, w - 2, h - 2);
			g.setColor(previewBorderColor);
			g.drawRect(x, y, w - 1, h - 1);
			g.setColor(getForeground());
			g.fillRect(x + 2, y + 2, w - 4, h - 4);
		}

		public String getToolTipText(MouseEvent evt) {
			Color color = getForeground();
			return (color == null) ? null : color.getRed() + ", " + color.getGreen() + ", " + color.getBlue();
		}

		private void initComponents() {

			setLayout(new java.awt.BorderLayout());

			setPreferredSize(new java.awt.Dimension(26, 26));
		}
	}

	private final static Color previewBorderColor = new Color(0x949494);
	private final static Color previewBackgroundColor = new Color(0xffffff);
}
