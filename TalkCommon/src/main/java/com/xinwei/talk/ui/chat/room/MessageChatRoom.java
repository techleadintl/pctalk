/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月21日 上午9:45:59
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

import org.jivesoftware.smack.packet.Message.Type;

import com.xinwei.spark.SwingWorker;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.chat.TalkTextPane;
import com.xinwei.talk.ui.chat.balloon.BalloonMessage;

public class MessageChatRoom extends ChatRoom {
	private static final long serialVersionUID = 1L;
	TalkTextPane talkTextPane;
	JScrollPane transcriptWindowScrollPanel;
	JPanel infoPanel;

	public MessageChatRoom(String roomId, String roomTitle, String roomDesc) {
		this.roomId = roomId;
		this.roomTitle = roomTitle;
		this.roomDesc = roomDesc;
		createUI();
		// Add VCard Panel
		addVcardPanel();
	}

	protected void createUI() {
		setLayout(new BorderLayout(0, 0));

		talkTextPane = new TalkTextPane();
		talkTextPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

		transcriptWindowScrollPanel = new JScrollPane(talkTextPane);
		transcriptWindowScrollPanel.setAutoscrolls(true);
		transcriptWindowScrollPanel.getVerticalScrollBar().setBlockIncrement(200);
		transcriptWindowScrollPanel.getVerticalScrollBar().setUnitIncrement(20);
		transcriptWindowScrollPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		infoPanel = new JPanel();
		infoPanel.setOpaque(false);
		infoPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 2));
		add(infoPanel, BorderLayout.NORTH);
		add(transcriptWindowScrollPanel, BorderLayout.CENTER);

		transcriptWindowScrollPanel.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
			private boolean scrollAtStart = false;

			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				if (!scrollAtStart) {
					scrollAtStart = true;
					SwingWorker thread = new SwingWorker() {

						@Override
						public Object construct() {
							int start = transcriptWindowScrollPanel.getVerticalScrollBar().getMaximum();
							int second = 0;
							int i = 0;
							do {
								try {
									Thread.sleep(150);
									second = transcriptWindowScrollPanel.getVerticalScrollBar().getMaximum();
									if (start == second) {
										++i;
									} else {
										scrollToBottom();
										talkTextPane.repaint();
									}
									start = second;
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							} while (i < 5);
							return null;
						}
					};
					thread.start();
				}
			}
		});

	}

	public void scrollToBottom() {
		int lengthOfChat = talkTextPane.getDocument().getLength();
		talkTextPane.setCaretPosition(lengthOfChat);

		try {
			final JScrollBar scrollBar = transcriptWindowScrollPanel.getVerticalScrollBar();
			EventQueue.invokeLater(new Runnable() {

				@Override
				public void run() {
					scrollBar.setValue(scrollBar.getMaximum());
				}
			});

		} catch (Exception e) {
			Log.error(e);
		}
	}

	protected void addVcardPanel() {
		final JLabel label = new JLabel();
		label.setFont(new Font("Dialog", Font.BOLD, 14));
		label.setText(getRoomTitle());
		if (label != null) {
			infoPanel.add(label);
		}
	}

	@Override
	public Type getChatType() {
		return Type.normal;
	}

	@Override
	public void updateStatus(boolean active) {
		// TODO Auto-generated method stub

	}

	public String getChatMessageFrom() {
		return TalkManager.getVCard().getTel();
	}

	public String getChatMessageTo() {
		return TalkUtil.getUserTel(getRoomId());
	}

	@Override
	public String canClose() {
		return null;
	}

	@Override
	public boolean close(boolean force) {
		ListenerManager.fireChatRoomClosed(this);

		transcriptWindowScrollPanel.getViewport().remove(talkTextPane);

		this.removeAll();
		setClosed(true);
		
		return true;
	}

	@Override
	public void initTalkMessages() {
		List<TalkMessage> transcriptMessageList = TalkManager.getMessageManager().getCacheMessageList(getRoomId());
		insertHistroyMessageList(transcriptMessageList);
	}

	@Override
	public void receiveTalkMessage(TalkMessage message) {
		insertBalloonMessage(message, message.getTalkBody(), false);

	}

	private synchronized void insertBalloonMessage(final TalkMessage talkMessage, TalkBody bodyMessage, final boolean isHistory) {
		//获取服务器时间
		Date sentDate = talkMessage.getTalkBody().getTime();
		try {
			BalloonMessage balloonMsg = new BalloonMessage(talkMessage.getId());
			balloonMsg.setStatus(BalloonMessage.STATUS_YES);
			balloonMsg.setTalkMessage(talkMessage);
			balloonMsg.setDate(sentDate);
			balloonMsg.setAlignment(BalloonMessage.ALIGNMENT_LEFT);
			balloonMsg.setBalloon(false);
			balloonMsg.setAvatar(false);
			balloonMsg.setTimepattern("yyyy-MM-dd HH:mm:ss");

			talkTextPane.addBalloonMessage(balloonMsg);

		} catch (Exception e) {
			Log.error("Error message.", e);
		}
	}

	public void insertHistroyMessageList(List<TalkMessage> messages) {
		if (messages == null)
			return;
		for (final TalkMessage message : messages) {
			insertBalloonMessage(message, message.getTalkBody(), false);
		}
		scrollToBottom();
	}
}
