package com.xinwei.talk.ui.skin.skin.filechooser;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ImagePreviewPanel extends JPanel implements PropertyChangeListener {

	private static final int ACCSIZE = 155;

	private JLabel imageLabel;

	public ImagePreviewPanel() {
		setPreferredSize(new Dimension(ACCSIZE, -1));
		setLayout(new BorderLayout());
		imageLabel = new JLabel(" ");
		add(imageLabel, BorderLayout.CENTER);
	}

	public void propertyChange(PropertyChangeEvent e) {
		String propertyName = e.getPropertyName();

		if (propertyName.equals(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY)) {
			File selection = (File) e.getNewValue();
			String name;

			if (selection == null)
				return;
			else
				name = selection.getAbsolutePath();

			if ((name != null) && name.toLowerCase().endsWith(".jpg") || name.toLowerCase().endsWith(".jpeg")
					|| name.toLowerCase().endsWith(".gif") || name.toLowerCase().endsWith(".png")) {
				ImageIcon icon = new ImageIcon(name);
				Image scaleImage = scaleImage(icon.getImage());
				imageLabel.setIcon(new ImageIcon(scaleImage));
			}
		}
	}

	private Image scaleImage(Image image) {
		int width = image.getWidth(this);
		int height = image.getHeight(this);
		double ratio = 1.0;

		if (width >= height) {
			ratio = (double) (ACCSIZE - 5) / width;
			width = ACCSIZE - 5;
			height = (int) (height * ratio);
		} else {
			if (getHeight() > 150) {
				ratio = (double) (ACCSIZE - 5) / height;
				height = ACCSIZE - 5;
				width = (int) (width * ratio);
			} else {
				ratio = (double) getHeight() / height;
				height = getHeight();
				width = (int) (width * ratio);
			}
		}
		return image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
	}
}
