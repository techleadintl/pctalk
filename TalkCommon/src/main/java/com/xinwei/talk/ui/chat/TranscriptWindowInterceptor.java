package com.xinwei.talk.ui.chat;

import org.jivesoftware.smack.packet.Message;

public interface TranscriptWindowInterceptor {

	/**
	 * Is called before a message by this user is inserted into the TranscriptWindow.
	 *
	 * @param window
	 *            the TranscriptWindow.
	 * @param userid
	 *            the userid.
	 * @param message
	 *            the message to be inserted.
	 * @return true if it should be handled by a custom interceptor.
	 */
	boolean isMessageIntercepted(TranscriptWindow window, Message message);

}