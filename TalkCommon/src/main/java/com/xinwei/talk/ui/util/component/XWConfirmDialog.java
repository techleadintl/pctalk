package com.xinwei.talk.ui.util.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillDialog;
import com.xinwei.common.lookandfeel.component.McWillTextPane;
import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
import com.xinwei.common.lookandfeel.util.Constants;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;

public class XWConfirmDialog extends McWillDialog implements ActionListener {

	private String message;
	protected JButton button1;
	protected JButton button2;

	public XWConfirmDialog() {
	}

	public XWConfirmDialog(String message) {
		this(null, null, message);
	}

	public XWConfirmDialog(String title, String message) {
		this(null, title, message);
	}

	public XWConfirmDialog(Window window, String title, String message) {
		super(window);
		this.message = message;
		createUI();
		if (title != null) {
			setTitle(title);
		}
		setIconImage(LAF.getApplicationImage().getImage());
		if (window != null) {
			setLocationRelativeTo(window);
			SwingUtil.centerWindowOnComponent(this, window);
		}
//		setDefaultCloseOperation(HIDE_ON_CLOSE);
	}

	public JPanel createContentPanel() {
		JPanel panel = new JPanel();
		panel.putClientProperty(Constants.COLOR_TYPE, Constants.COLOR_LIGHT);
		panel.setLayout(new BorderLayout(15, 0));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 0));

		//提示图标
		Component west = createWest();
		//文本信息
		JPanel inputPanel = new JPanel(new BorderLayout());

		Component inputNorth = createInputNorth();
		if (inputNorth != null) {
			inputPanel.add(inputNorth, BorderLayout.NORTH);
		}

		Component inputInputCenter = createInputCenter();
		if (inputInputCenter != null) {
			inputPanel.add(inputInputCenter, BorderLayout.CENTER);
		}

		panel.add(west, BorderLayout.WEST);
		panel.add(inputPanel, BorderLayout.CENTER);

		return panel;

	}

	protected Component createWest() {
		JPanel iconPanel = new JPanel(new BorderLayout());
		iconPanel.add(new JLabel(LAF.getInformationDialogImageIcon()), BorderLayout.NORTH);
		return iconPanel;
	}

	protected Component createInputNorth() {
		if (message != null) {
			McWillTextPane textPane=new McWillTextPane();
			textPane.setText(message);
			return textPane;
		} else {
			return null;
		}
	}

	protected Component createInputCenter() {
		return null;
	}

	public JPanel createButtonPanel() {
		JPanel panel = new JPanel();
		panel.putClientProperty(Constants.COLOR_TYPE, Constants.COLOR_SUBLIGHT);
		panel.setLayout(new FlowLayout(VerticalFlowLayout.RIGHT));

		button1 = createButton1();
		button2 = createButton2();
		if (button1 != null) {
			panel.add(button1);
		}
		if (button2 != null) {
			panel.add(button2);
		}
		return panel;
	}

	protected JButton createButton2() {
		JButton button = new JButton(Res.getMessage("button.cancel"));
		button.setPreferredSize(new Dimension(60, 24));
		button.addActionListener(this);
		return button;
	}

	protected JButton createButton1() {
		JButton button = new JButton(Res.getMessage("button.ok"));
		button.setPreferredSize(new Dimension(60, 24));
		button.addActionListener(this);
		return button;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button1) {
			result = OK;
			setVisible(false);
			dispose();
		} else if (e.getSource() == button2) {
			result = CANCEL;
			setVisible(false);
			dispose();
		}

	}
}
