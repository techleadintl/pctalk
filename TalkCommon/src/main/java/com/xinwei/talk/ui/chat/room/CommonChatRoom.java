/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月20日 上午11:16:21
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.packet.Message;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lang.SyncLock;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.common.lookandfeel.McWillBorders.SeparatorBorder;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillSplitPane;
import com.xinwei.common.lookandfeel.util.ColorUtil;
import com.xinwei.common.lookandfeel.util.Constants;
import com.xinwei.spark.SwingWorker;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.conf.UserConfig;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.model.message.specific.TalkImageSpecific;
import com.xinwei.talk.model.message.specific.TalkParam;
import com.xinwei.talk.model.message.specific.TalkTextSpecific;
import com.xinwei.talk.model.message.specific.TalkVoiceSpecific;
import com.xinwei.talk.ui.chat.ChatInputEditor;
import com.xinwei.talk.ui.chat.ChatRoomTransferHandler;
import com.xinwei.talk.ui.chat.TranscriptWindow;
import com.xinwei.talk.ui.chat.balloon.BalloonMessage;
import com.xinwei.talk.ui.chat.component.ImageComponent;
import com.xinwei.talk.ui.chat.component.VoiceComponent;
import com.xinwei.talk.ui.chat.room.history.ChatRoomTabbedPane;
import com.xinwei.talk.ui.chat.voice.VoiceManager;
import com.xinwei.talk.ui.plugin.emoticon.EmoticonManager;
import com.xinwei.talk.ui.util.border.ChatLineBorder;
import com.xinwei.talk.ui.util.component.button.ChatRoomButton;

/**
 * The base implementation of all ChatRoom conversations. You would implement
 * this class to have most types of Chat.
 */
public abstract class CommonChatRoom extends ChatRoom {
	private static final long serialVersionUID = 7981019929515888299L;

	private static final String KEY_STR = "Default-Send-Key";

	private static final ImageIcon checkImageIcon = LAF.getSendKeyCheckIcon();

	//关闭聊天窗口时，有正在工作的内容，需要提示是否关闭
	private List<ChatRoomWorking> workingList = new ArrayList<ChatRoomWorking>();
	private Object workingListLock = new Object();

	protected String threadID;

	protected long lastActivity;

	private McWillSplitPane horizontalLeftVerticalSplit;

	// 房间borderlayout的North
	private ChatToolBar chatToolBar;

	// 房间borderlayout的Center
	private JPanel horizontalPanel;
	private JPanel horizontalLeftPanel;
	private ChatRightPanel horizontalRightPanel;
	private ChatRoomTabbedPane tabbedPane;

	private JScrollPane transcriptWindowScrollPanel;
	private TranscriptWindow transcriptWindow;

	//
	private JPanel chatInputPanel;

	private ChatInputToolBar chatInputToolBar;

	private JScrollPane chatInputScrollPanel;
	private ChatInputEditor chatInputEditor;

	// 房间borderlayout的South
	private JPanel buttonPanel;
	private JButton sendButton;
	private JButton arrowButton;
	private JButton closeButton;

	private boolean mousePressed;

	//	private McWillList2<ChatRoomClosingListener> closingListeners = new CopyOnWriteArrayList<ChatRoomClosingListener>();

	private ChatRoomTransferHandler transferHandler;

	private MouseListener transcriptWindowMouseListener;

	private ConnectionListener connectionListener;

	private JPopupMenu sendKeyPopupMenu;

	public static final String DEFAULT_SEND_KEY_ENTER = "enter";
	public static final String DEFAULT_SEND_KEY_CTRL_ENTER = "ctrl + enter";

	protected void createRoom(boolean _chatPanel) {
		
		createUI(_chatPanel);

		createMenu();

		addListeners();

		initUI();

		scrollToBottom();
	}

	protected void createUI(boolean panel) {
		setLayout(new BorderLayout(0, 0));

		chatToolBar = new ChatToolBar();

		horizontalPanel = new JPanel();
		horizontalPanel.setLayout(new BorderLayout());
		horizontalPanel.setBorder(new SeparatorBorder() {
			@Override
			public Color getLineColor() {
				return ColorUtil.brighter(McWillTheme.getThemeColor().getMiddleColor1(), 0.2);
			}
		});

		horizontalLeftPanel = new JPanel();
		horizontalLeftPanel.setLayout(new BorderLayout());

		horizontalRightPanel = new ChatRightPanel();
		horizontalRightPanel.setLayout(new BorderLayout());
		//		horizontalRightPanel.setPreferredSize(new Dimension(400, -1));
		horizontalRightPanel.setVisible(false);

		tabbedPane = new ChatRoomTabbedPane(this);
		tabbedPane.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		tabbedPane.setBorder(new SeparatorBorder(null, SeparatorBorder.LEFT) {
			@Override
			public Color getLineColor() {
				return McWillTheme.getThemeColor().getMiddleColor();
			}
		});

		// Add Vertical Split Pane
		horizontalLeftVerticalSplit = new McWillSplitPane(JSplitPane.VERTICAL_SPLIT) {
			@Override
			public Color getSplitLineColor() {
				return McWillTheme.getThemeColor().getMiddleColor();
			}
		};
		horizontalLeftVerticalSplit.setBorder(null);
		horizontalLeftVerticalSplit.setOneTouchExpandable(true);
		horizontalLeftVerticalSplit.setDividerLocation(330);
		horizontalLeftVerticalSplit.setResizeWeight(0.87);
		//		horizontalLeftVerticalSplit.setDividerSize(1);

		transcriptWindow = new TranscriptWindow();
		transcriptWindowScrollPanel = new JScrollPane(transcriptWindow);
		transcriptWindowScrollPanel.setAutoscrolls(true);
		transcriptWindowScrollPanel.getVerticalScrollBar().setBlockIncrement(200);
		transcriptWindowScrollPanel.getVerticalScrollBar().setUnitIncrement(20);
		transcriptWindowScrollPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		transcriptWindowScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		chatInputEditor = new ChatInputEditor();
		chatInputScrollPanel = new JScrollPane(chatInputEditor);
		chatInputEditor.setBorder(null);
		chatInputScrollPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

		chatInputPanel = new JPanel();
		chatInputPanel.setLayout(new BorderLayout());

		chatInputToolBar = new ChatInputToolBar();

		createButtonPanel();

		chatInputPanel.add(chatInputToolBar, BorderLayout.NORTH);
		chatInputPanel.add(chatInputScrollPanel, BorderLayout.CENTER);
		if (buttonPanel != null) {
			chatInputPanel.add(buttonPanel, BorderLayout.SOUTH);
		}

		// Add Chat Panel to Split Pane
		horizontalLeftVerticalSplit.setTopComponent(transcriptWindowScrollPanel);
		if(panel == true){
		horizontalLeftVerticalSplit.setBottomComponent(chatInputPanel);						// changed May 18
		}
		else{
			
		}

		horizontalLeftPanel.add(horizontalLeftVerticalSplit, BorderLayout.CENTER);
		//		horizontalLeftPanel.add(buttonPanel, BorderLayout.SOUTH);
		horizontalRightPanel.add(tabbedPane, BorderLayout.CENTER);

		horizontalPanel.add(horizontalLeftPanel, BorderLayout.CENTER);
		horizontalPanel.add(horizontalRightPanel, BorderLayout.EAST);

		add(chatToolBar, BorderLayout.NORTH);
		add(horizontalPanel, BorderLayout.CENTER);

		//		horizontalPanel.setOpaque(true);
		//		horizontalLeftPanel.setOpaque(true);
		//		horizontalRightPanel.setOpaque(true);
		//		horizontalLeftVerticalSplit.setOpaque(true);
		//		buttonPanel.setOpaque(true);
		//		chatInputPanel.setOpaque(true);
		chatInputEditor.putClientProperty(Constants.OPAQUE, true);
		//		
		//		Color chatRoomColor = LAF.getChatRoomColor();
		//		horizontalRightPanel.putClientProperty(Constants.COLOR_RGB, chatRoomColor);
		////		chatInputEditor.putClientProperty(Constants.COLOR_RGB, Color.WHITE);
		//		transcriptWindow.putClientProperty(Constants.COLOR_RGB, chatRoomColor);
		//		chatToolBar.putClientProperty(Constants.COLOR_RGB, chatRoomColor);
		//		buttonPanel.putClientProperty(Constants.COLOR_RGB, chatRoomColor);
		//		chatInputToolBar.putClientProperty(Constants.COLOR_RGB, chatRoomColor);
	}

	protected void createButtonPanel() {
		buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		JPanel sendPanel = new JPanel(null);
		sendPanel.setPreferredSize(new Dimension(80, 26));

		JButton lineButton = new JButton();
		sendButton = new JButton();
		sendButton.setBounds(new Rectangle(0, 1, 60, 25));
		sendButton.setPreferredSize(new Dimension(60, 24));

		arrowButton = new JButton("↓");
		arrowButton.setBounds(new Rectangle(55, 1, 25, 25));

		closeButton = new JButton();
		closeButton.setPreferredSize(new Dimension(60, 24));

		lineButton.setBorder(new ChatLineBorder());
		lineButton.setBounds(new Rectangle(56, 1, 5, 25));

		sendButton.setFocusPainted(false);
		arrowButton.setFocusPainted(false);
		lineButton.setFocusable(false);
		lineButton.setFocusPainted(false);

		chatInputEditor.setSelectedTextColor(LAF.getChatInputSelectedTextColor());
		chatInputEditor.setSelectionColor(LAF.getChatInputSelectionColor());

		sendPanel.add(lineButton);
		sendPanel.add(sendButton);
		sendPanel.add(arrowButton);

		buttonPanel.add(closeButton);
		buttonPanel.add(sendPanel);
	}

	protected void createMenu() {
		final UserConfig userConfig = TalkManager.getUserConfig();
		ActionListener l = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JMenuItem menuItem = (JMenuItem) e.getSource();
				menuItem.setIcon(checkImageIcon);
				Container parent = menuItem.getParent();
				for (int i = 0; i < parent.getComponentCount(); i++) {
					if (parent.getComponent(i) != e.getSource() && parent.getComponent(i) instanceof JMenuItem) {
						((JMenuItem) parent.getComponent(i)).setIcon(null);
					}
				}
				userConfig.put(KEY_STR, menuItem.getClientProperty(KEY_STR));
				TalkManager.saveUserConfig();
			}
		};

		sendKeyPopupMenu = new JPopupMenu();
		JMenuItem enterItem = new JMenuItem(Res.getMessage("menuitem.chatroom.key.enter"));
		JMenuItem ctrlEnterItem = new JMenuItem(Res.getMessage("menuitem.chatroom.key.ctrl.enter"));

		enterItem.putClientProperty(KEY_STR, DEFAULT_SEND_KEY_ENTER);
		ctrlEnterItem.putClientProperty(KEY_STR, DEFAULT_SEND_KEY_CTRL_ENTER);
		sendKeyPopupMenu.putClientProperty(DEFAULT_SEND_KEY_ENTER, enterItem);
		sendKeyPopupMenu.putClientProperty(DEFAULT_SEND_KEY_CTRL_ENTER, ctrlEnterItem);

		enterItem.addActionListener(l);
		ctrlEnterItem.addActionListener(l);

		sendKeyPopupMenu.add(enterItem);
		sendKeyPopupMenu.add(ctrlEnterItem);

		String string = userConfig.getString(KEY_STR);
		if (string == null) {
			userConfig.put(KEY_STR, ctrlEnterItem.getClientProperty(KEY_STR));
			TalkManager.saveUserConfig();
			string = userConfig.getString(KEY_STR);
		}

		JMenuItem menuItem = (JMenuItem) sendKeyPopupMenu.getClientProperty(string);
		if (menuItem != null) {
			menuItem.setIcon(checkImageIcon);
		}
	}

	protected void addListeners() {
		ActionListener buttonActionListener = getButtonActionListener();
		sendButton.addActionListener(buttonActionListener);
		arrowButton.addActionListener(buttonActionListener);
		closeButton.addActionListener(buttonActionListener);

		// Add Connection Listener
		connectionListener = new ChatRoomConnectionListener(this);
		try {
			TalkManager.getConnection().addConnectionListener(connectionListener);
		} catch (Exception ex) {

		}

		//		// Add Focus Listener
		//		addFocusListener(this);
		//
		//		// Add Key Listener to Send Field
		//		getChatInputEditor().getDocument().addDocumentListener(this);

		// Add Key Listener to Send Field
		getChatInputEditor().addKeyListener(new ChatInputKeyListener(this));

		transcriptWindowMouseListener = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {

				if (e.getClickCount() != 2) {
					getChatInputEditor().requestFocus();
				}
			}

			public void mouseReleased(MouseEvent e) {
				mousePressed = false;
				if (transcriptWindow.getSelectedText() == null) {
					getChatInputEditor().requestFocus();
				}
			}

			public void mousePressed(MouseEvent e) {
				mousePressed = true;
			}
		};

		transcriptWindow.addMouseListener(transcriptWindowMouseListener);

		// For the first 5*150ms we wait for transcript to load and move
		// scrollpane to max postion if size of scrollpane changed
		transcriptWindowScrollPanel.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
			private boolean scrollAtStart = false;

			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				if (!scrollAtStart) {
					scrollAtStart = true;
					SwingWorker thread = new SwingWorker() {

						@Override
						public Object construct() {
							int start = transcriptWindowScrollPanel.getVerticalScrollBar().getMaximum();
							int second = 0;
							int i = 0;
							do {
								try {
									Thread.sleep(150);
									second = transcriptWindowScrollPanel.getVerticalScrollBar().getMaximum();
									if (start == second) {
										++i;
									} else {
										scrollToBottom();
										getTranscriptWindow().repaint();
									}
									start = second;
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							} while (i < 5);
							return null;
						}
					};
					thread.start();
				}
			}
		});

	}

	// Setup base layout.
	protected void initUI() {
		String stext = Res.getMessage("button.send");
		if (!stext.contains("S"))
			stext += "(S)";
		sendButton.setText(stext);
		sendButton.setMnemonic(KeyEvent.VK_S);

		String ctext = Res.getMessage("button.close");
		if (!ctext.contains("C"))
			ctext += "(C)";
		closeButton.setText(ctext);
		closeButton.setMnemonic(KeyEvent.VK_C);

		transferHandler = new ChatRoomTransferHandler(this);
		getTranscriptWindow().setTransferHandler(transferHandler);

		chatInputEditor.setTransferHandler(transferHandler);

		chatInputEditor.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ctrl F4"), "closeTheRoom");
		chatInputEditor.getActionMap().put("closeTheRoom", new AbstractAction("closeTheRoom") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				// Leave this chat.
				close(false);
			}
		});
	}

	public void initTalkMessages() {
		List<TalkMessage> talkMessageList = TalkManager.getMessageManager().getCacheMessageList(getRoomId());
		insertHistroyMessageList(talkMessageList);
	}
	//	public void receivedMessage(Msg message) {
	//		// Fire Msg Filters
	//
	//		//		CamtalkManager.getChatManager().filterIncomingMessage(this, message);
	//		//
	//		//		CamtalkManager.getChatManager().fireGlobalMessageReceievedListeners(this, message);
	//
	//		ListenerManager.fireMessageReceived(this, message);
	//
	//		//		SparkManager.getWorkspace().getTranscriptPlugin().persistChatRoom(this);
	//	}

	//	/**
	//	 * Adds a new message to the transcript history.
	//	 * 
	//	 * @param to
	//	 *            who the message is to.
	//	 * @param from
	//	 *            who the message was from.
	//	 * @param body
	//	 *            the body of the message.
	//	 * @param date
	//	 *            when the message was received.
	//	 */
	//	public void addToTranscript(String to, String from, String body, Date date) {
	//		final Msg newMessage = new Msg();
	//		newMessage.setTo(to);
	//		newMessage.setFrom(from);
	//		newMessage.setBody(body);
	//		newMessage.setProperty("date", date);
	//	}

	/**
	 * Scrolls the chat window to the bottom.
	 */
	public void scrollToBottom() {
		if (mousePressed) {
			return;
		}

		int lengthOfChat = transcriptWindow.getDocument().getLength();
		transcriptWindow.setCaretPosition(lengthOfChat);

		try {
			final JScrollBar scrollBar = transcriptWindowScrollPanel.getVerticalScrollBar();
			EventQueue.invokeLater(new Runnable() {

				@Override
				public void run() {
					scrollBar.setValue(scrollBar.getMaximum());
				}
			});

		} catch (Exception e) {
			Log.error(e);
		}
	}

	/**
	 * Requests valid focus to the SendField.
	 */
	public void positionCursor() {
		getChatInputEditor().setCaretPosition(getChatInputEditor().getCaretPosition());
		chatInputEditor.requestFocusInWindow();
	}

	/**
	 * Disable the chat room. This is called when a chat has been either
	 * transfered over or the customer has left the chat room.
	 */
	public abstract void leaveChatRoom();

	/**
	 * Notifies all <code>MessageListener</code> that a message has been sent.
	 * 
	 * @param message
	 *            the message sent.
	 */
	public void fireMessageSent(TalkMessage message) {
		ListenerManager.fireMessageSent(getRoomId(), message);
	}

	protected void fireMessageReceived(TalkMessage message) {
		ListenerManager.fireMessageReceived(getRoomId(), message);
	}

	/**
	 * Close the ChatRoom.
	 */
	public boolean close(boolean force) {
		if (force) {
			return closeRoom();
		} else {
			TalkManager.getChatManager().closeChatRoom(this);
		}
		return true;
	}

	public String canClose() {
		synchronized (workingListLock) {
			for (ChatRoomWorking working : workingList) {
				return working.getMessage();
			}
		}
		return null;
		//		return tabbedPane.canClose();
	}

	public void addChatRoomWorking(ChatRoomWorking working) {
		if (working == null)
			return;
		synchronized (workingListLock) {
			workingList.add(working);
		}
	}

	public void removeChatRoomWorking(ChatRoomWorking working) {
		if (working == null)
			return;
		synchronized (workingListLock) {
			workingList.remove(working);
		}
	}

	public void closeTabbedPane() {
		tabbedPane.close();
	}

	private boolean closeRoom() {
		synchronized (workingListLock) {
			for (ChatRoomWorking working : workingList) {
				working.close();
			}
		}
		workingList.clear();

		ListenerManager.fireChatRoomClosed(this);

		getTranscriptWindow().removeMouseListener(transcriptWindowMouseListener);

		transcriptWindowScrollPanel.getViewport().remove(transcriptWindow);

		// Remove Connection Listener
		TalkManager.getConnection().removeConnectionListener(connectionListener);

		this.removeAll();

		getTranscriptWindow().setTransferHandler(null);
		getChatInputEditor().setTransferHandler(null);

		transferHandler = null;

		getChatInputEditor().close();

		getChatInputEditor().getActionMap().remove("closeTheRoom");
		chatInputPanel.remove(chatInputEditor);
		_chatFrame.removeWindowToFrontListener(this);

		setClosed(true);

		return true;
	}

	/**
	 * Notify all users that a collection of files has been dropped onto the
	 * ChatRoom.
	 * 
	 * @param files
	 *            the files dropped.
	 */
	public void fireFileDropListeners(Collection<File> files) {
		ListenerManager.fireFileDropListeners(this, files);
	}

	/**
	 * Implementation of this method should return the last time this chat room
	 * sent or recieved a message.
	 * 
	 * @return the last time (in system milliseconds) that the room last
	 *         recieved a message.
	 */
	public long getLastActivity() {
		return lastActivity;
	}

	/**
	 * Returns the SendField component.
	 * 
	 * @return the SendField ChatSendField.
	 */
	public ChatInputEditor getChatInputEditor() {
		return chatInputEditor;
	}

	/**
	 * Returns the chatWindow components.
	 * 
	 * @return the ChatWindow component.
	 */
	public TranscriptWindow getTranscriptWindow() {
		return transcriptWindow;
	}

	public ChatRoomTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	/**
	 * Returns the toolbar used on top of the chat room.
	 * 
	 * @return the toolbar used on top of this chat room.
	 */
	public ChatToolBar getToolBar() {
		return chatToolBar;
	}

	public ChatRightPanel getHorizontalRightPanel() {
		return horizontalRightPanel;
	}

	public ChatInputToolBar getChatInputToolBar() {
		return chatInputToolBar;
	}

	public JPanel getChatInputToolBarRight() {
		return chatInputToolBar.getRight();
	}

	public JPanel getChatInputToolBarLeft() {
		return chatInputToolBar.getLeft();
	}

	//	public void addFileComponent(FileUI component) {
	//		tabbedPane.addFileComponent(component);
	//	}
	//
	//	public void removeFileComponent(FileUI component) {
	//		tabbedPane.removeFileComponent(component);
	//	}
	//
	//	public void showMessageComponent() {
	//		tabbedPane.showMessageComponent(this);
	//	}

	public void addChatRoomButton(JComponent button) {
		addChatRoomButton(button, false);
	}

	public void addChatRoomButton(JComponent button, boolean forceRepaint) {
		chatToolBar.addChatRoomButton(button);
		if (forceRepaint) {
			chatToolBar.invalidate();
			chatToolBar.repaint();
		}
	}

	public void addChatInputToolBarLeftComponent(JComponent button) {
		button.setPreferredSize(new Dimension(35, 33));
		chatInputToolBar.getLeft().add(button);
	}

	public void removeChatInputToolBarLeftComponent(JComponent component) {

		chatInputToolBar.getLeft().remove(component);
	}

	public void addChatInputToolBarRightComponent(JComponent button) {
		button.setPreferredSize(new Dimension(35, 33));
		chatInputToolBar.getRight().add(button, 0);
	}

	/**
	 * Creates and sends a message object from the text in the Send Field, using
	 * the default nickname specified in your Chat Preferences.
	 */
	//	public void sendMessage() {
	//		ChatMessage chatMessage = getChatMessage();
	//
	////		chatMessage.addBody(new TextMsg(text));
	//		
	//		StringBuilder sb = new StringBuilder();
	//		try {
	//			StyledDocument styledDocument = chatInputEditor.getStyledDocument();
	//			int length = chatInputEditor.getText().length();
	//			for (int i = 0; i < length; i++) {
	//				if (styledDocument.getCharacterElement(i).getName().equals("icon")) {
	//					Icon icon = StyleConstants.getIcon(styledDocument.getCharacterElement(i).getAttributes());
	//					//第 i位置插入了圖片
	//					//					Icon icon = picVector.get(k);
	//					String description = icon.toString();
	//					EmoticonManager emoticonManager = EmoticonManager.getInstance();
	//					if (emoticonManager.getEmoticon(description) != null) {//是内置表情
	//						sb.append(description);
	//					} else {
	//						Emoticon customEmoticon = emoticonManager.getCustomEmoticon(description);
	//						if (customEmoticon != null) {//是自定义表情
	//
	//						} else {//是其他图片：截图或者选择的图片文件
	//
	//						}
	//					}
	//					//					k++;
	//				} else {
	//					sb.append(styledDocument.getText(i, 1));
	//				}
	//			}
	//		} catch (Exception ex) {
	//			Log.error(ex);
	//		}
	//
	//		String text = sb.toString();
	//		if (StringUtil.isEmpty(text)) {
	//			return;
	//		}
	//		sendMessage(text);
	//	}
	public void sendMessage() {
		TalkBody bodyMessage = getBodyMessage();

		StringBuilder sb = new StringBuilder();
		EmoticonManager emoticonManager = EmoticonManager.getInstance();
		try {
			StyledDocument styledDocument = chatInputEditor.getStyledDocument();

			int length = styledDocument.getLength();
			for (int i = 0; i < length; i++) {
				if (styledDocument.getCharacterElement(i).getName().equals(StyleConstants.ComponentElementName)) {
					//第 i位置插入了圖片
					Component component = StyleConstants.getComponent(styledDocument.getCharacterElement(i).getAttributes());

					TalkSpecific msg = null;
					if (component instanceof ImageComponent) {
						ImageComponent label = (ImageComponent) component;
						Icon icon = label.getIcon();
						String description = icon.toString();
						if (emoticonManager.getEmoticon(description) != null) {//是内置表情
							sb.append(description);
						} else {//TODO:emoticonManager.getCustomEmoticon(description)//自定义表情
							msg = new TalkImageSpecific(label.getData(), label.getImageName());
						}
					} else if (component instanceof VoiceComponent) {
						VoiceComponent button = (VoiceComponent) component;
						byte[] wav = button.getVoice();
						//					byte[] amr = VoiceManager.wavToAmr(wav);
						msg = new TalkVoiceSpecific(wav, button.getVoiceName());

					}
					if (msg != null) {
						String txt = sb.toString();
						if (txt.length() > 0) {
							bodyMessage.addSpecific(new TalkTextSpecific(sb.toString()));
							sb = new StringBuilder();
						}
						bodyMessage.addSpecific(msg);
					}
				} else {
					sb.append(styledDocument.getText(i, 1));
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
		}
		String txt = sb.toString();
		if (txt.length() > 0) {
			bodyMessage.addSpecific(new TalkTextSpecific(sb.toString()));
		}
		if (bodyMessage.isEmpty()) {
			return;
		}

		setSendParam(bodyMessage);

		sendMessage(bodyMessage);
	}

	protected TalkParam setSendParam(TalkBody bodyMessage) {
		SimpleAttributeSet styles = chatInputEditor.getStyles();
		TalkParam param = new TalkParam();
		param.setB(StyleConstants.isBold(styles) ? 1 : 0);
		param.setI(StyleConstants.isItalic(styles) ? 1 : 0);
		param.setU(StyleConstants.isUnderline(styles) ? 1 : 0);
		param.setS(StyleConstants.getFontSize(styles));
		param.setF(StyleConstants.getFontFamily(styles));
		param.setFg(StyleConstants.getForeground(styles).getRGB());
		param.setBg(StyleConstants.getBackground(styles).getRGB());
		param.setBalloon(TalkManager.getUserConfig().getBalloon());
		bodyMessage.setParam(param);
		return param;
	}

	/**
	 * Creates a Msg object from the given text and delegates to the room for
	 * sending.
	 * 
	 * @param text
	 *            the text to send.
	 */

	//	public void sendMessage(String text) {
	//		// IF there is no body, just return and do nothing
	//		if (StringUtil.isEmpty(text)) {
	//			return;
	//		}
	//
	//		// Set the body of the message using typedMessage and remove control
	//		// characters
	//		text = text.replaceAll("[\\u0001-\\u0008\\u000B-\\u001F]", "");
	//
	//		ChatMessage chatMessage = getChatMessage();
	//
	//		chatMessage.addBody(new TextMsg(text));
	//
	//		sendMessage(chatMessage);
	//
	//	}

	//	public void sendVoice(byte[] voice, int second) throws Exception {
	//		//		String fileName = UUID.randomUUID().toString() + ".amr";
	//		//
	//		//		//上传文件
	//		//		HttpFileResponseInfo response = uploadChatFile(fileName, voice);
	//		//
	//		//		//发送文件消息
	//		//		String fileAbsolutePath = response.getFileAbsolutePath();
	//		//
	//		//		ChatMessage chatMessage = getChatMessage();
	//		//
	//		//		chatMessage.addBody(new VoiceMsg(fileAbsolutePath, fileName, second, "null"));
	//		//
	//		//		sendMessage(chatMessage);
	//	}

	//	public void sendImage(BufferedImage image) throws Exception {
	//		String fileName = UUID.randomUUID().toString() + ".png";
	//
	//		byte[] png = ImageUtil.getPng(image);
	//
	//		//		ImageManager imageManager = TalkManager.getImageManager();
	//		//		//上传文件
	//		//		HttpFileResponseInfo response = imageManager.uploadChatFile(fileName, png);
	//		//
	//		//		String fileAbsolutePath = response.getFileAbsolutePath();
	//		//
	//		//		//保存文件
	//		//		imageManager.addFile(fileAbsolutePath, png);
	//
	//		//发送文件消息
	//		ChatMessage chatMessage = getChatMessage();
	//
	//		ImageMsg imageMsg = new ImageMsg(null, fileName, "null", "null");
	//		imageMsg.setImage(png);
	//
	//		chatMessage.addBody(imageMsg);
	//
	//		sendMessage(chatMessage);
	//	}

	//	public void sendImage(byte[] image, String type) throws Exception {
	//		String fileName = UUID.randomUUID().toString() + "." + type;
	//
	//		//发送文件消息
	//		ChatMessage chatMessage = getChatMessage();
	//
	//		ImageMsg imageMsg = new ImageMsg(null, fileName, "null", "null");
	//		imageMsg.setData(image);
	//
	//		chatMessage.addBody(imageMsg);
	//
	//		sendMessage(chatMessage);
	//	}

	//	public void insertVoice(byte[] wav) {
	//		//发送文件消息
	//		ChatMessage chatMessage = getChatMessage();
	//
	//		VoiceMsg msg = new VoiceMsg(wav, UUID.randomUUID().toString() + ".amr", "");
	//
	//		chatMessage.addBody(msg);
	//
	//		sendMessage(chatMessage);
	//	}

	//发送文本消息，图片等
	public void sendMessage(final TalkBody bodyMessage) {
		lastActivity = System.currentTimeMillis();
		if (bodyMessage.isEmpty()) {
			return;
		}
		//清空输入框
		getChatInputEditor().clear();
		getChatInputEditor().setCaretPosition(0);
		getChatInputEditor().requestFocusInWindow();

		final TalkMessage transcriptMessage = getTalkMessage(null, bodyMessage, TalkMessage.TYPE_SEND, null);
		sendTalkMessage(transcriptMessage);
	}

	public void sendTalkMessage(final TalkMessage talkMessage) {
		Object multi = Res.get("MULTI-MESSAGE");
		TalkBody talkBody = talkMessage.getTalkBody();
		List<TalkSpecific> msgList = talkBody.getSpecificList();
		final List<TalkMessage> talkMessageList = new ArrayList<>();
		if (msgList != null && msgList.size() > 1 && multi != null && !Boolean.parseBoolean(multi.toString())) {
			List<TalkSpecific> msgList2 = new ArrayList<>(msgList);
			talkBody.clear();
			for (TalkSpecific msg : msgList2) {
				TalkMessage cloneTalkMessage = talkMessage.clone();
				TalkBody cloneTalkBody = talkBody.clone();
				List<TalkSpecific> msgList3 = new ArrayList<>();
				msgList3.add(msg);
				cloneTalkBody.setSpecificList(msgList3);

				cloneTalkMessage.setTalkBody(cloneTalkBody);
				cloneTalkMessage.setId(TalkManager.getMessageManager().nextMessageId());

				talkMessageList.add(cloneTalkMessage);
			}
		} else {
			talkMessageList.add(talkMessage);
		}
		doSendTalkMessage(talkMessageList);
	}

	private void doSendTalkMessage(final List<TalkMessage> talkMessageList) {
		final List<BalloonMessage> balloonMessageList = new ArrayList<BalloonMessage>();
		for (TalkMessage talkMessage : talkMessageList) {
			String packetId = talkMessage.getTalkBody().getPacketId();
			if (packetId == null) {
				packetId = TalkManager.getMessageManager().nextPackageId();
				talkMessage.getTalkBody().setPacketId(packetId);
			}

			final BalloonMessage balloonMessage = insertRealTimeMessage(talkMessage);
			if (balloonMessage != null) {
				balloonMessageList.add(balloonMessage);
			}
		}
		final AtomicInteger uploadAtomicInteger = new AtomicInteger();
		final AtomicInteger sendAtomicInteger = new AtomicInteger();
		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				//第一步:取出需要上传的
				List<BalloonMessage> uploadList = new ArrayList<BalloonMessage>(balloonMessageList);
				for (final BalloonMessage balloonMessage : uploadList) {
					TaskEngine.getInstance().submit(new Runnable() {
						@Override
						public void run() {
							balloonMessage.setStatus(BalloonMessage.STATUS_INIT);
							boolean result = MessageUtil.handResourceMsg(getTranscriptWindow(), balloonMessage, com.xinwei.talk.common.Constants.UPDOWN_TYPE_UPLOAD);
							if(result){
								balloonMessage.setStatus(BalloonMessage.STATUS_YES);
							}else{
								balloonMessage.setStatus(BalloonMessage.STATUS_NO);
								balloonMessageList.remove(balloonMessage);
							}
							uploadAtomicInteger.incrementAndGet();
						}
					});
				}
				SystemUtil.waitForSize(uploadAtomicInteger, uploadList.size(), 360 * 1000);

				//第二步，发送消息
				List<BalloonMessage> sendList = new ArrayList<BalloonMessage>(balloonMessageList);
				for (final BalloonMessage balloonMessage : sendList) {
					boolean result = MessageUtil.sendBalloonMessage(CommonChatRoom.this, getTranscriptWindow(), balloonMessage);
					if(result){
						balloonMessage.setStatus(BalloonMessage.STATUS_YES);
					}else{
						balloonMessage.setStatus(BalloonMessage.STATUS_NO);
						balloonMessageList.remove(balloonMessage);
					}
					sendAtomicInteger.incrementAndGet();
				}
				getTranscriptWindow().repaint();
				//		SystemUtil.waitForSize(sendAtomicInteger, sendList.size(), 360 * 1000);
			}
		});

	}

	//	private void doSendTalkMessage(final TalkMessage talkMessage) {
	//		final Message message = new Message();
	//		String packetId = talkMessage.getTalkBody().getPacketId();
	//		if (packetId == null) {
	//			packetId = TalkManager.getMessageManager().nextPackageId();
	//			talkMessage.getTalkBody().setPacketId(packetId);
	//			message.setPacketID(packetId);
	//		} else {
	//			message.setPacketID(packetId);
	//		}
	//
	//		final BalloonMessage balloonMessage = insertRealTimeMessage(talkMessage);
	//		if (balloonMessage == null) {
	//			return;
	//		}
	//		TaskEngine.getInstance().submit(new Runnable() {
	//			@Override
	//			public void run() {
	//				//第一步:取出需要上传的
	//				boolean result = MessageUtil.handResourceMsg(getTranscriptWindow(), balloonMessage, com.xinwei.talk.common.Constants.UPDOWN_TYPE_UPLOAD);
	//				if (!result) {
	//					return;
	//				}
	//
	//				//第二步，发送消息
	//				boolean sendMsgFailed = false;
	//				final SyncLock lock = new SyncLock(message.getPacketID());
	//				try {
	//					// Notify users that message has been sent,如：保存到历史记录中
	//					fireMessageSent(talkMessage);
	//
	//					//封装消息的类型、发送方、接收方
	//					message.setType(getChatType());
	//
	//					message.setBody(MessageUtil.getXmppBodyString(talkMessage.getTalkBody(), getChatType()));
	//
	//					message.setTo(getRoomId());
	//					message.setFrom(TalkManager.getSessionManager().getJID());
	//
	//					//发送消息
	//					TalkManager.getConnection().sendPacket(message);
	//
	//				} catch (Exception e) {
	//					sendMsgFailed = true;
	//					Log.error(e);
	//					return;
	//				}
	//				//设置发送消息失败
	//				if (sendMsgFailed) {
	//					balloonMessage.setStatus(BalloonMessage.STATUS_NO);
	//					return;
	//				}
	//
	//				//第三步，等待服务器响应
	//				//300秒钟没有消息回复，认为发送消息失败
	//				lock.lock(300, TimeUnit.SECONDS);
	//				Date date = (Date) lock.getContent();
	//				if (date != null) {
	//					//					//清除缓存,
	//					//					getTranscriptWindow().removeMsg(talkMessage.getId());
	//
	//					getTranscriptWindow().updateMessageDate(balloonMessage, date);
	//
	//					balloonMessage.setStatus(BalloonMessage.STATUS_YES);
	//				} else {
	//					balloonMessage.setStatus(BalloonMessage.STATUS_NO);
	//				}
	//			}
	//		});
	//	}

	/**
	 * The main isIn point when receiving any messages. This will either handle
	 * a message from a customer or delegate itself as an agent handler.
	 * 
	 * @param talkMessage
	 *            - the message receieved.
	 */
	public void receiveTalkMessage(final TalkMessage talkMessage) {
		final BalloonMessage balloonMessage = insertRealTimeMessage(talkMessage);
		if (balloonMessage == null) {
			return;
		}

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				balloonMessage.setStatus(BalloonMessage.STATUS_INIT);
				boolean result = MessageUtil.handResourceMsg(getTranscriptWindow(), balloonMessage, com.xinwei.talk.common.Constants.UPDOWN_TYPE_DOWNLOAD);
				if (result) {
					balloonMessage.setStatus(BalloonMessage.STATUS_YES);
				} else {
					balloonMessage.setStatus(BalloonMessage.STATUS_NO);
				}
				getTranscriptWindow().repaint();
				//				if (result) {
				//					getTranscriptWindow().removeMsg(talkMessage.getId());
				//				}
			}
		});
	}

	public void sendFile(String url, File file, boolean complete) {
		//发送文件消息
		TalkBody bodyMessage = getBodyMessage();
		setSendParam(bodyMessage);

		TalkFileSpecific fileMsg = new TalkFileSpecific(url, file.getName(), file.length());
		fileMsg.setFileAbsolutePath(file.getAbsolutePath());

		if (complete) {
			fileMsg.setFileType(TalkFileSpecific.SEND_COMPLETE);
		} else {
			fileMsg.setFileType(TalkFileSpecific.SEND_CANCEL);
		}

		bodyMessage.addSpecific(fileMsg);

		final TalkMessage talkMessage = getTalkMessage(null, bodyMessage, TalkMessage.TYPE_SEND, null);
		if (complete) {
			sendTalkMessage(talkMessage);
		} else {
			insertRealTimeMessage(talkMessage);
		}
	}

	public void receiveFile(TalkMessage oldTalkMessage, TalkFileSpecific fileMsg, File downloadFile, Date date, boolean complete) {
		if (downloadFile != null) {
			fileMsg.setFileAbsolutePath(downloadFile.getAbsolutePath());
		}

		int fileType = fileMsg.getFileType();

		//重新接收的,删除原来的记录，增加新记录
		if (fileType == TalkFileSpecific.RECEIVE_CANCEL) {
			getTranscriptWindow().deleteTalkMessage(getRoomId(), oldTalkMessage.getId());
			date = TalkManager.getXmppService().getServerDate();
		}

		if (complete) {
			fileMsg.setFileType(TalkFileSpecific.RECEIVE_COMPLETE);
		} else {
			fileMsg.setFileType(TalkFileSpecific.RECEIVE_CANCEL);
		}

		TalkBody bodyMessage = oldTalkMessage.getTalkBody();

		TalkMessage newTalkMessage = getTalkMessage(null, bodyMessage, TalkMessage.TYPE_RECEIVE, date);

		insertRealTimeMessage(newTalkMessage);

		fireMessageReceived(newTalkMessage);
	}

	public void insertHistroyMessageList(List<TalkMessage> messages) {
		if (messages == null)
			return;
		for (final TalkMessage talkMessage : messages) {
			getTranscriptWindow().insertHistroyMessage(this, talkMessage);
		}
		scrollToBottom();
	}

	private BalloonMessage insertRealTimeMessage(TalkMessage talkMessage) {
		BalloonMessage component = getTranscriptWindow().insertRealTimeMessage(this, talkMessage);
		scrollToBottom();
		return component;
	}

	public abstract TalkMessage getTalkMessage(String packetId, TalkBody bodyMessage, int transcriptMessageType, Date time);

	/**
	 * Sends the current message.
	 * 
	 * @param message
	 *            - the message to send.
	 */
	//	public void sendMessage(Msg message) {
	//		//封装消息的类型、发送方、接收方
	//		message.setType(getChatType());
	//		message.setTo(getRoomId());
	//		message.setFrom(TalkManager.getSessionManager().getJID());
	//		addPacketID(message.getPacketID());
	//		//发送消息
	//		try {
	//			TalkManager.getConnection().sendPacket(message);
	//		} catch (Exception ex) {
	//			Log.error("Error sending message", ex);
	//			return;
	//		}
	//
	//		//显示框
	//		UserConfig userConfig = TalkManager.getUserConfig();
	//		getTranscriptWindow().insertMessage(message, userConfig);
	//		//		getTranscriptWindow().validate();
	//		//		getTranscriptWindow().repaint();
	//		//滚动到最后一条消息
	//		scrollToBottom();
	//
	//		//输入框
	//		getChatInputEditor().clear();
	//		getChatInputEditor().setCaretPosition(0);
	//		getChatInputEditor().requestFocusInWindow();
	//
	//		lastActivity = System.currentTimeMillis();
	//
	//		// Notify users that message has been sent,如：保存到历史记录中
	//		fireMessageSent(message);
	//	}

	///////////////////////////////////////////

	private TalkBody getBodyMessage() {
		return new TalkBody(getMessageFrom(), getMessageTo());
	}

	public abstract String getMessageTo();

	public abstract String getMessageFrom();

	protected ButtonActionListener getButtonActionListener() {
		return new ButtonActionListener();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof CommonChatRoom)) {
			return false;
		}
		return getRoomId().equals(((CommonChatRoom) obj).getRoomId());
	}

	@Override
	public String toString() {
		return TalkUtil.getDisplayName(getRoomId());
	}

	public class ButtonActionListener implements ActionListener {

		// I would normally use the command pattern, but
		// have no real use when dealing with just a couple options.
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(closeButton)) {
				close(false);
			} else if (e.getSource().equals(arrowButton)) {
				sendKeyPopupMenu.show(arrowButton, arrowButton.getX() - 160, arrowButton.getY() + 20);
			} else if (e.getSource().equals(sendButton)) {
				sendMessage();
				// Clear send field and disable send button
				getChatInputEditor().clear();
				//			this.sendButton.setEnabled(false);
			}

		}
	};

	/**
	 * Used for the top toolbar.
	 */
	public class ChatToolBar extends JPanel {
		private static final long serialVersionUID = 5926527530611601841L;
		/** 功能 **/
		private JPanel functionPanel;

		/** 信息 **/
		private JPanel infoPanel;

		/**
		 * Default Constructor.
		 */
		public ChatToolBar() {

			// Set Layout
			setLayout(new BorderLayout());

			infoPanel = new JPanel() {
				@Override
				public void paintComponent(Graphics g) {
					super.paintComponent(g);
					//					LAF.paintChatTitlePane(this, g);
				}
			};
			infoPanel.setOpaque(false);
			infoPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 2));

			functionPanel = new JPanel();
			functionPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 20));
			functionPanel.setOpaque(false);
			functionPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 15, 2));

			add(infoPanel, BorderLayout.NORTH);

			add(functionPanel, BorderLayout.CENTER);

			this.setOpaque(false);
		}

		public void addInfoComponent(Component head) {
			infoPanel.add(head);
		}

		/**
		 * Adds a new ChatRoomButton the CommandBar.
		 * 
		 * @param button
		 *            the button.
		 */
		public void addChatRoomButton(Component button) {
			functionPanel.add(button);

			// Make all JButtons the same size
			Component[] comps = functionPanel.getComponents();
			final int no = comps != null ? comps.length : 0;

			final List<Component> buttons = new ArrayList<Component>();
			for (int i = 0; i < no; i++) {
				try {
					Component component = comps[i];
					buttons.add(component);
				} catch (NullPointerException e) {
					Log.error(e);
				}
			}

			SwingUtil.makeSameSize(buttons.toArray(new JComponent[buttons.size()]));
		}

		/**
		 * Removes the ChatRoomButton from the CommandBar.
		 * 
		 * @param button
		 *            the button.
		 */
		public void removeChatRoomButton(ChatRoomButton button) {
			functionPanel.remove(button);
		}
	}

	public class ChatInputToolBar extends JPanel {
		private static final long serialVersionUID = 5926527530611601841L;
		private JPanel chatInputToolBarRight;
		private JPanel chatInputToolBarLeft;
		private JPanel chatInputToolBarTop;

		/**
		 * Default Constructor.
		 */
		public ChatInputToolBar() {
			setLayout(new BorderLayout());

			final int H = 40;
			chatInputToolBarLeft = new JPanel(new FlowLayout(FlowLayout.LEFT, 13, 3));
			chatInputToolBarRight = new JPanel(new FlowLayout(FlowLayout.RIGHT, 13, 3));
			chatInputToolBarTop = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0)) {
				@Override
				public void setVisible(boolean aFlag) {
					int dividerLocation = horizontalLeftVerticalSplit.getDividerLocation();
					int newSize = dividerLocation;
					if (aFlag) {
						newSize = dividerLocation - H;
					} else {
						newSize = dividerLocation + H;
					}
					horizontalLeftVerticalSplit.setDividerLocation(newSize);
					super.setVisible(aFlag);
				}
			};
			chatInputToolBarTop.setPreferredSize(new Dimension(100, H));
			chatInputToolBarTop.setVisible(false);
			add(chatInputToolBarTop, BorderLayout.NORTH);
			add(chatInputToolBarLeft, BorderLayout.WEST);
			add(chatInputToolBarRight, BorderLayout.EAST);
			setBorder(new SeparatorBorder() {
				@Override
				public Color getLineColor() {
					return ColorUtil.brighter(McWillTheme.getThemeColor().getMiddleColor1(), 0.2);
				}
			});
		}

		public JPanel getRight() {
			return chatInputToolBarRight;
		}

		public JPanel getLeft() {
			return chatInputToolBarLeft;
		}

		public JPanel getTop() {
			return chatInputToolBarTop;
		}
	}

	public class ChatRightPanel extends JPanel {
	}

	class ChatInputKeyListener implements KeyListener {
		CommonChatRoom room;

		public ChatInputKeyListener(CommonChatRoom room) {
			this.room = room;
		}

		@Override
		public void keyTyped(KeyEvent e) {
			ListenerManager.fireChatInputKeyTyped(room, e);

		}

		@Override
		public void keyReleased(KeyEvent e) {
			ListenerManager.fireChatInputKeyReleased(room, e);

		}

		@Override
		public void keyPressed(KeyEvent e) {
			ListenerManager.fireChatInputKeyPressed(room, e);
		}
	}

	class ChatRoomConnectionListener implements ConnectionListener {
		CommonChatRoom room;

		public ChatRoomConnectionListener(CommonChatRoom room) {
			this.room = room;
		}

		@Override
		public void connectionClosed() {
			// TODO Auto-generated method stub

		}

		@Override
		public void connectionClosedOnError(Exception e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void reconnectingIn(int seconds) {
			// TODO Auto-generated method stub

		}

		@Override
		public void reconnectionSuccessful() {
			// TODO Auto-generated method stub

		}

		@Override
		public void reconnectionFailed(Exception e) {
			// TODO Auto-generated method stub

		}

	}

}
