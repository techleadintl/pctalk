/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 上午11:36:23
 * 
 ***************************************************************/
package com.xinwei.talk.ui.main.status;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;

import com.sun.image.codec.jpeg.ImageFormatException;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageDecoder;
import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.spark.ModelUtil;
import com.xinwei.spark.SwingTimerTask;
import com.xinwei.spark.TaskEngine;
import com.xinwei.spark.ui.PresenceListener;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.listener.VCardListener;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.ui.util.component.XWAvatarLabel;

public class StatusPanel extends JPanel implements VCardListener {
	private static final long serialVersionUID = -4322806442034868526L;

	private JLabel imageLabel;
	private JLabel descriptiveLabel;
	private JLabel nameLabel;

	private CommandPanel commandPanel;

	private Presence currentPresence;

	public StatusPanel() {
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createEmptyBorder(10, 0, 5, 0));

		createUI();

		initUI();

		addListeners();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		LAF.paintMainStatusPanel(this, (Graphics2D) g);
	}

	private void createUI() {
		imageLabel = new XWAvatarLabel();

		nameLabel = new JLabel();
		nameLabel.setFont(new Font(Font.DIALOG, Font.BOLD, 12));

		descriptiveLabel = new JLabel();

		//		statusPanel = new StatusPanel();

		commandPanel = new CommandPanel();

		add(imageLabel, new GridBagConstraints(0, 0, 1, 3, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(2, 8, 2, 2), 0, 0));
		add(nameLabel, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(2, 12, 0, 0), 0, 0));
		//		add(statusPanel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 7, 0, 0), 0, 0));
		add(descriptiveLabel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 7, 0, 0), 0, 0));

		add(commandPanel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));

		currentPresence = new Presence(Presence.Type.available, Res.getString("isInputing.online"), 1, Presence.Mode.available);

		//setBorder(BorderFactory.createLineBorder(new Color(197, 213, 230), 1));
	}

	public void initUI() {

		nameLabel.setToolTipText(TalkManager.getConnection().getUser());
		loadVCard();
	}

	public void addListeners() {
		TalkManager.getSessionManager().addPresenceListener(new PresenceListener() {
			@Override
			public void presenceChanged(Presence presence) {
				presence.setStatus(ModelUtil.modifyWildcards(presence.getStatus()));
				changeAvailability(presence);
			}
		});
		//		addButton.addMouseListener(new MouseAdapter() {
		//			public void mouseReleased(MouseEvent e) {
		//				showPopup(e);
		//			}
		//
		//			private void showPopup(MouseEvent e) {
		//				final JPopupMenu popup = new JPopupMenu();
		//
		//				popup.setUI(new BasePopupMenuUI());
		//
		//				final JMenuItem groupItem = new JMenuItem(Res.getMessage("menu.group.add"), LAF.getImageIcon("camtalk/images/menu/add_group.png"));
		//				final JMenuItem userMenu = new JMenuItem(Res.getMessage("menu.contact.add"), LAF.getImageIcon("camtalk/images/menu/add_contact.png"));
		//
		//				popup.add(groupItem);
		//				popup.addSeparator();
		//				popup.add(userMenu);
		//
		//				groupItem.addActionListener(new ActionListener() {
		//					public void actionPerformed(ActionEvent e) {
		//						new UserDialog().showDialog();
		//					}
		//				});
		//				popup.show(addButton, 0, addButton.getHeight());
		//
		//			}
		//		});

		// Show profile on double click of image label
		imageLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent mouseEvent) {
				if (mouseEvent.getClickCount() == 1) {
					String key = "MyTalkInfo_" + TalkManager.getVCard().getTel();
					Object object = TalkManager.getLocalManager().get(key);
					if (!(object instanceof MyInfoDialog)) {
						MyInfoDialog dialog = new MyInfoDialog();
						TalkManager.getLocalManager().put(key, dialog);
						object = dialog;
					}
					((MyInfoDialog) object).setVisible(true);
				}
			}

			public void mouseEntered(MouseEvent e) {
				imageLabel.setCursor(SwingUtil.HAND_CURSOR);
			}

			public void mouseExited(MouseEvent e) {
				imageLabel.setCursor(SwingUtil.DEFAULT_CURSOR);
			}
		});

		final TimerTask task = new SwingTimerTask() {
			@Override
			public void doRun() {
				ListenerManager.addVCardListener(StatusPanel.this);
			}
		};
		TaskEngine.getInstance().schedule(task, 3000);
	}

	public void loadVCard() {
		final Runnable loadVCard = new Runnable() {
			public void run() {
				VCard vCard = TalkManager.getVCardManager().getVCard();
				updateVCardInformation(vCard);
			}
		};

		TaskEngine.getInstance().submit(loadVCard);
	}

	public void setAvatar(Icon icon) {
		Icon avatar = LAF.getStatusBarAvatar(icon);

		imageLabel.setIcon(avatar);

		imageLabel.invalidate();
		imageLabel.validate();
		imageLabel.repaint();

		//		revalidate();
	}

	public CommandPanel getCommandPanel() {
		return commandPanel;
	}

	public void setNickname(String nickname) {
		nameLabel.setText(nickname);
	}

	public void changeAvailability(final Presence presence) {
		if (!presence.isAvailable()) {
			return;
		}

		if ((presence.getMode() == currentPresence.getMode()) && (presence.getType() == currentPresence.getType()) && (presence.getStatus().equals(currentPresence.getStatus()))) {
			PacketExtension pe = presence.getExtension("x", "vcard-temp:x:update");
			if (pe != null) {
				// Update VCard
				loadVCard();
			}
			return;
		}

		currentPresence = presence;
	}

	public Presence getPresence() {
		return currentPresence;
	}

	protected void updateVCardInformation(final VCard vCard) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				setNickname(TalkUtil.getDisplayName(vCard.getJid()));

				String signature = vCard.getSignature();
				if (StringUtil.isNotEmpty(signature)) {
					setDescriptiveText(signature);
				}

				byte[] avatarBytes = vCard.getAvatar();
				
				setAvatar(ImageManager.getTalkAvatar(avatarBytes));
			}
		});

	}

	public static Presence copyPresence(Presence presence) {
		return new Presence(presence.getType(), presence.getStatus(), presence.getPriority(), presence.getMode());
	}

	/**
	 * Return the nickname Component used to display the users profile name.
	 *
	 * @return the label.
	 */
	public JLabel getNicknameLabel() {
		return nameLabel;
	}

	public void setDescriptiveText(String text) {
		descriptiveLabel.setText(text);
	}

	public Dimension getPreferredSize() {
		Dimension dim = super.getPreferredSize();
		dim.width = 0;
		return dim;
	}

	public void vcardChanged(VCard vcard) {
		updateVCardInformation(vcard);
	}

	protected Presence getCurrentPresence() {
		return currentPresence;
	}

}
