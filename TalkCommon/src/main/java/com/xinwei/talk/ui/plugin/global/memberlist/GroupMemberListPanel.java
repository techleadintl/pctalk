/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月14日 下午12:57:22
 * 
 ***************************************************************/
package com.xinwei.talk.ui.plugin.global.memberlist;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lookandfeel.component.McWillList;
import com.xinwei.common.lookandfeel.component.McWillListCellRenderer;
import com.xinwei.talk.action.XAction;
import com.xinwei.talk.action.member.MemberDeleteAction;
import com.xinwei.talk.action.member.MemberInviteAction;
import com.xinwei.talk.action.member.MemberOpenAction;
import com.xinwei.talk.action.member.MemberUpdateAction;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.common.TalkUtil2;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.util.component.XWScrollPane;
import com.xinwei.talk.ui.util.component.avatar.AvatarPanel2;

public class GroupMemberListPanel extends JPanel {
	private LocalManager localManager = TalkManager.getLocalManager();
	private JTextPane textPane;

	private McWillList<MemberListCellPanel> list;

	private DefaultListModel<MemberListCellPanel> listModel;
	private CommonChatRoom chatRoom;
	private String groupCreator = null;

	final JPopupMenu popup = new JPopupMenu();

	Comparator comparator = new Comparator<MemberListCellPanel>() {
		public int compare(MemberListCellPanel o1, MemberListCellPanel o2) {
			Member m1 = o1.member;
			Member m2 = o2.member;
			int role = m1.getMemRole() - m2.getMemRole();
			if (role == 0) {
				String name1 = TalkUtil.getDisplayNameMember(m1.getGid(), m1.getMemJid());
				String name2 = TalkUtil.getDisplayNameMember(m1.getGid(), m2.getMemJid());
				if (name1 == null && name2 == null)
					return 0;
				if (name1 == null)
					return 1;
				if (name2 == null)
					return -1;
				return 0 - name1.compareTo(name2);

			} else {
				return role > 0 ? 1 : -1;
			}
		};
	};

	public GroupMemberListPanel(CommonChatRoom chatRoom) {
		this.chatRoom = chatRoom;
		setPreferredSize(new Dimension(200, 100));
		createUI();
		updateData();
	}

	private void createUI() {
		setOpaque(false);
		textPane = new JTextPane();
		textPane.setEditable(false);

		JLabel line = new JLabel(LAF.getLine());
		//		line.setBorder(BorderFactory.createLineBorder(Color.RED));

		listModel = new DefaultListModel<MemberListCellPanel>();
		list = new McWillList<MemberListCellPanel>(listModel);
		list.setCellRenderer(new McWillListCellRenderer());

		JPanel notePanel = new JPanel(new BorderLayout());
		notePanel.add(line, BorderLayout.SOUTH);
		notePanel.add(new XWScrollPane(textPane), BorderLayout.CENTER);

		setLayout(new BorderLayout());
		//		add(notePanel, BorderLayout.NORTH);
		add(new XWScrollPane(list), BorderLayout.CENTER);

		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
					MemberListCellPanel item = (MemberListCellPanel) list.getSelectedValue();
					if (item != null) {
						Member member = item.getMember();
						TalkManager.getChatManager().activateChatRoom(member.getMemJid(), member.getMemNick(), member.getUserKey(), false, true);
					}
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				checkPopup(e);
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				checkPopup(e);
			}

		});

	}

	public void updateData() {
		String roomId = chatRoom.getRoomId();

		List<Member> groupMembers = localManager.getMemberListByJid(roomId);

		add(groupMembers);

	}

	public void add(List<Member> members) {
		Group group = localManager.getGroupByJid(chatRoom.getRoomId());
		if (group != null) {
			groupCreator = group.getCreatorUid();
		}

		if (CollectionUtil.isNotEmpty(members)) {
			String groupId = TalkUtil.getGroupId(chatRoom.getRoomId());

			for (final Member member : members) {
				if (!StringUtil.equals(groupId, member.getGid()))
					continue;
				MemberListCellPanel listItem = getListItem(TalkUtil.getBareJid(member.getMemJid()));
				if (listItem == null) {
					listModel.addElement(new MemberListCellPanel(member));
				}
			}
		}
		sort();
	}

	public void update(List<Member> members) {
		Group group = localManager.getGroupByJid(chatRoom.getRoomId());
		if (group != null) {
			groupCreator = group.getCreatorUid();
		}
		if (CollectionUtil.isNotEmpty(members)) {
			String groupId = TalkUtil.getGroupId(chatRoom.getRoomId());

			for (final Member member : members) {
				if (!StringUtil.equals(groupId, member.getGid()))
					continue;
				MemberListCellPanel listItem = getListItem(TalkUtil.getBareJid(member.getMemJid()));
				if (listItem != null) {
					listItem.updateData(member);
				}
			}
		}
		sort();
	}

	public void delete(List<Member> members) {
		Group group = localManager.getGroupByJid(chatRoom.getRoomId());
		if (group != null) {
			groupCreator = group.getCreatorUid();
		}
		if (CollectionUtil.isNotEmpty(members)) {
			String groupId = TalkUtil.getGroupId(chatRoom.getRoomId());

			for (final Member member : members) {
				if (!StringUtil.equals(groupId, member.getGid()))
					continue;

				removeListItem(TalkUtil.getBareJid(member.getMemJid()));
			}
		}
		sort();
	}

	public void removeListItem(String jid) {
		List<MemberListCellPanel> removes = new ArrayList<MemberListCellPanel>();

		Enumeration<MemberListCellPanel> elements = listModel.elements();
		while (elements.hasMoreElements()) {
			MemberListCellPanel listItem = elements.nextElement();
			Member member = listItem.getMember();
			if (member == null)
				continue;
			String memJid = member.getMemJid();

			String bareJid = TalkUtil.getBareJid(memJid);

			if (StringUtil.equals(bareJid, jid)) {
				removes.add(listItem);
			}
		}

		for (MemberListCellPanel listItem : removes) {
			listModel.removeElement(listItem);
		}
	}

	public void sort() {
		MemberListCellPanel[] array = new MemberListCellPanel[listModel.size()];

		listModel.copyInto(array);

		List<MemberListCellPanel> itemList = Arrays.asList(array);

		Collections.sort(itemList, comparator);

		listModel.removeAllElements();
		list.updateUI();
		for (MemberListCellPanel item : itemList) {
			listModel.addElement(item);
		}
	}

	public MemberListCellPanel getListItem(String jid) {
		for (int i = 0; i < listModel.getSize(); i++) {
			MemberListCellPanel listItem = listModel.get(i);
			Member member = listItem.getMember();
			if (member == null)
				continue;
			String memJid = member.getMemJid();

			String bareJid = TalkUtil.getBareJid(memJid);

			if (StringUtil.equals(bareJid, jid)) {
				return listItem;
			}
		}
		return null;
	}

	private void checkPopup(MouseEvent e) {
		if (!e.isPopupTrigger())
			return;
		Group group = localManager.getGroupByJid(chatRoom.getRoomId());
		if (group == null) {
			return;
		}
		// Otherwise, handle single selection
		int index = list.locationToIndex(e.getPoint());
		if (index != -1) {
			int[] indexes = list.getSelectedIndices();
			boolean selected = false;
			for (int o : indexes) {
				if (index == o) {
					selected = true;
				}
			}

			if (!selected) {
				list.setSelectedIndex(index);
			}
		}

		List<Object> items = new ArrayList<Object>();

		for (Object item : list.getSelectedValues()) {
			items.add(item);
		}

		firePopupEvent(e, group, items);
	}

	private void firePopupEvent(MouseEvent e, Group group, final Collection<Object> items) {
		if (CollectionUtil.isEmpty(items)) {
			return;
		}

		Member member = null;
		for (Object item : items) {
			MemberListCellPanel contactItem = (MemberListCellPanel) item;
			member = contactItem.getMember();
			if (member != null) {
				break;
			}
		}

		if (member == null) {
			return;
		}
		popup.removeAll();

		addPopuMenuItemList(popup, group, member);

		//菜单不为空
		if (popup.getComponentCount() != 0) {
			popup.show(e.getComponent(), e.getX(), e.getY());
		}

	}

	protected void addPopuMenuItemList(JPopupMenu popup, final Group group, final Member member) {
		TalkUtil2.addPopunActions(popup, "member", group, member);
	}

	public class MemberListCellPanel extends JPanel {
		private AvatarPanel2 avatarPanel;
		private JLabel nameLabel;
		private JLabel specialLabel;
		private Member member;

		public MemberListCellPanel(Member member) {
			setOpaque(true);

			setLayout(new BorderLayout());

			setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

			createUI();

			updateData(member);

		}

		private void createUI() {
			avatarPanel = new AvatarPanel2();

			nameLabel = new JLabel();
			nameLabel.setFont(LAF.getFont(13));

			specialLabel = new JLabel();
			specialLabel.setFont(LAF.getFont(10));
			specialLabel.setForeground(Color.WHITE);
			specialLabel.setHorizontalAlignment(SwingConstants.CENTER);

			nameLabel.setPreferredSize(new Dimension(20, 20));
			specialLabel.setPreferredSize(new Dimension(20, 20));
			avatarPanel.setPreferredSize(new Dimension(50, 50));

			add(nameLabel, BorderLayout.CENTER);
			add(avatarPanel, BorderLayout.WEST);
			add(specialLabel, BorderLayout.EAST);
		}

		public void updateData(Member member) {
			this.member = member;

			int memRole = member.getMemRole();

			if (memRole == Member.ROLE_ADMIN || StringUtil.equals(member.getUid(), groupCreator)) {
				specialLabel.setIcon(LAF.getBlueStar());
				//specialLabel.setToolTipText("群主");
			}

			String memJid = member.getMemJid();
			nameLabel.setText(TalkUtil.getDisplayNameMember(member.getGid(), memJid));

			avatarPanel.setJid(memJid);

		}

		public Member getMember() {
			return member;
		}

	}

}
