/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月26日 下午12:52:17
 * 
 ***************************************************************/
package com.xinwei.talk.ui.plugin.buzz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.SwingUtilities;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.provider.ProviderManager;

import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.spark.SwingTimerTask;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.listener.ChatRoomAdapter;
import com.xinwei.talk.manager.ChatManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.chat.ChatFrame;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.plugin.Plugin;
import com.xinwei.talk.ui.chat.room.ChatRoom;

public class BuzzPlugin implements Plugin {

	private static final String ELEMENTNAME = "attention";
	private static final String NAMESPACE = "urn:xmpp:attention:0";

	private static final String ELEMENTNAME_OLD = "buzz";
	private static final String NAMESPACE_OLD = "http://www.jivesoftware.com/spark";

	public void initialize() {
		ProviderManager.getInstance().addExtensionProvider(ELEMENTNAME, NAMESPACE, BuzzPacket.class);

		ProviderManager.getInstance().addExtensionProvider(ELEMENTNAME_OLD, NAMESPACE_OLD, BuzzPacket.class);

		TalkManager.getConnection().addPacketListener(new PacketListener() {
			public void processPacket(Packet packet) {
				if (packet instanceof Message) {
					final Message message = (Message) packet;

					boolean buzz = message.getExtension(ELEMENTNAME_OLD, NAMESPACE_OLD) != null || message.getExtension(ELEMENTNAME, NAMESPACE) != null;
					if (buzz) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								shakeWindow(message);
							}
						});
					}
				}
			}
		}, new PacketTypeFilter(Message.class));

		ListenerManager.addChatRoomListener(new ChatRoomAdapter() {
			public void chatRoomOpened(final CommonChatRoom room) {
				if (room.getChatType() == Type.chat) {
					addBuzzFeatureToChatRoom(room);
				}
			}
		});
	}

	public void shutdown() {
	}

	public boolean canShutDown() {
		return true;
	}

	public void uninstall() {
	}

	private void addBuzzFeatureToChatRoom(final CommonChatRoom room) {
		JButton button = new McWillIconButton(LAF.getRoomBuzzImageIcon(), true);
		button.setToolTipText(Res.getMessage("button.chatroom.screenbuzz"));
		button.addActionListener(new BuzzAction(room));

		room.addChatInputToolBarLeftComponent(button);
	}

	private void shakeWindow(Message message) {
		String bareJID = TalkUtil.getBareJid(message.getFrom());

		ChatManager chatManager = TalkManager.getChatManager();
		ChatRoom room = chatManager.getChatContainer().getChatRoom(bareJID);
		if (room == null)
			return;

		buzz(room);
		//		// Insert offline message
		//		room.getTranscriptWindow().insertNotificationMessage(Res.getString("message.buzz.message", nickname), ChatManager.NOTIFICATION_COLOR);
		//		room.scrollToBottom();
	}

	/**
	 * 振动窗口
	 */
	public void buzz(ChatRoom room) {
		ChatManager chatManager = TalkManager.getChatManager();
		ChatFrame chatFrame = chatManager.getChatFrame();
		if (chatFrame != null) {
			//			if (SettingsManager.getLocalPreferences().isBuzzEnabled()) {
			ShakeWindow d = new ShakeWindow(chatFrame);
			d.startShake();
			chatManager.getChatContainer().activateChatRoom(room);
		}
	}

	class BuzzAction implements ActionListener {
		CommonChatRoom chatRoom;

		public BuzzAction(CommonChatRoom chatRoom) {
			this.chatRoom = chatRoom;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			final JButton buzzButton = (JButton) e.getSource();
			final String jid = chatRoom.getRoomId();
			Message message = new Message();
			message.setFrom(TalkManager.getSessionManager().getJID());
			message.setTo(jid);
			message.addExtension(new com.xinwei.talk.ui.plugin.buzz.BuzzPacket());
			TalkManager.getConnection().sendPacket(message);

			//			chatRoom.getTranscriptWindow().insertNotificationMessage(Res.getString("message.buzz.sent"), ChatManager.NOTIFICATION_COLOR);
			buzzButton.setEnabled(false);

			// Enable the button after 30 seconds to prevent abuse.
			final TimerTask enableTask = new SwingTimerTask() {
				@Override
				public void doRun() {
					buzzButton.setEnabled(true);
				}
			};

			TaskEngine.getInstance().schedule(enableTask, 10000);

			buzz(chatRoom);
		}
	}
}
