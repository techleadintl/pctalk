package com.xinwei.talk.ui.main.list.group.bak;
//package com.xinwei.plugin.group;
//
//import java.awt.Color;
//import java.awt.Dimension;
//import java.util.List;
//
//import javax.swing.ImageIcon;
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//
//import org.jivesoftware.spark.util.GraphicUtils;
//
//import com.xinwei.talk.lookandfeel.McWillBorderFactory;
//import com.xinwei.talk.lookandfeel.layout.TileLayout;
//
//public class GroupAvatarPanel2 extends JPanel {
//	private static final int GAP = 2;
//
//	private Dimension _SIZE9 = null;
//	private Dimension _SIZE4 = null;
//
//	public GroupAvatarPanel2(int WIDTH) {
//		setOpaque(false);
//
//		setMinimumSize(new Dimension(WIDTH, WIDTH));
//		setMaximumSize(new Dimension(WIDTH, WIDTH));
//		setPreferredSize(new Dimension(WIDTH, WIDTH));
//		WIDTH -= 2;
//
//		_SIZE9 = new Dimension((WIDTH - 2 * GAP) / 3, (WIDTH - 2 * GAP) / 3);
//
//		_SIZE4 = new Dimension((WIDTH - 1 * GAP) / 2, (WIDTH - 1 * GAP) / 2);
//	}
//
//	public void updateAvatar(McWillList2<byte[]> icons) {
//		removeAll();
//		
//		int num = icons.size();
//		Dimension SIZE = _SIZE9;
//		if (num <= 4) {
//			num = 4;
//			SIZE = _SIZE4;
//		} else {
//			num = 9;
//		}
//		setLayout(new TileLayout(GAP, GAP));
//		for (int i = 0; i < num; i++) {
//			JLabel label = new JLabel();
//			label.setPreferredSize(SIZE);
//			label.setOpaque(true);
//			add(label);
//
//			//			Image image = icons.get(i).getImage();
//			//			Image scaledInstance = image.getScaledInstance(SIZE.width, SIZE.height, Image.SCALE_DEFAULT);
//			//			scaledInstance.flush();
//			//			ImageIcon icon2 = new ImageIcon(scaledInstance);
//
//			if (i < icons.size()) {
//				ImageIcon icon2 = GraphicUtils.scale(new ImageIcon(icons.get(i)), SIZE.width, SIZE.height);
//				label.setIcon(icon2);
//				//				label.setBorder(McWillBorderFactory.getInstance().getLabelBorder(Color.GRAY, 0));
//			}
//		}
//	}
//
//}
