/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月3日 下午4:16:17
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room;

import javax.swing.JPanel;

import org.jivesoftware.smack.packet.Message;

import com.xinwei.common.lookandfeel.util.UIReleaseUtil;
import com.xinwei.talk.listener.ChatFrameToFrontListener;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.chat.ChatFrame;
import com.xinwei.talk.ui.chat.TranscriptWindow;

public abstract class ChatRoom extends JPanel implements ChatFrameToFrontListener {
	protected String roomId;

	protected String roomTitle;

	protected String roomDesc;

	protected ChatFrame _chatFrame;

	private boolean closed;

	public abstract String canClose();

	/**
	 * Close the TalkRoom.
	 */
	public abstract boolean close(boolean force);

	/**
	 * Returns the <code>Msg.Type</code> specific to this chat room. GroupChat
	 * is Msg.Type.groupchat Normal Chat is Msg.TYPE.NORMAL
	 * 
	 * @return the ChatRooms Msg.TYPE
	 */
	public abstract Message.Type getChatType();

	public abstract void initTalkMessages();

	public abstract void receiveTalkMessage(final TalkMessage talkMessage);

	public void fireMessageSent(TalkMessage message) {
	}

	public void sendTalkMessage(final TalkMessage talkMessage) {
	}

	public TranscriptWindow getTranscriptWindow() {
		return null;
	}

	/**
	 * Get the roomname to use for this ChatRoom.
	 * 
	 * @return - the Roomname of this ChatRoom.
	 */
	public String getRoomId() {
		return roomId;
	}

	/**
	 * Returns the tabTitle of this room to use. The tabTitle will be used in
	 * the tabTitle bar of the ChatRoom.
	 * 
	 * @return - the tabTitle of this ChatRoom.
	 */
	public String getRoomTitle() {
		return roomTitle;
	}

	public String getRoomDesc() {
		return roomDesc;
	}

	@Override
	public void updateStatus(boolean active) {
		// TODO Auto-generated method stub

	}

	@Override
	public void registeredToFrame(ChatFrame chatframe) {
		this._chatFrame = chatframe;
		_chatFrame.addWindowToFronListener(this);
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
		UIReleaseUtil.freeSwingObject(this);
	}
}
