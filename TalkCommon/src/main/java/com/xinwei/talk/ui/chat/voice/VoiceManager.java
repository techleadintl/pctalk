/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月22日 下午6:53:15
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.voice;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.TalkUtil;

import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncodingAttributes;
import it.sauronsoftware.jave.MultimediaInfo;

public class VoiceManager {
	public static void main(String[] args) {
		byte[] data = IOUtil.readBytes("D:\\software\\camtalk\\TalkPC\\Users\\008613426351153\\sounds\\x.wav");
		//		byte[] xx = wavToAmr(data);

		data = IOUtil.readBytes("F:\\语音文件\\output.amr");
		byte[] amrToWav = amrToWav(data);

	}

	public static File getParentFile() {
		File parent = null;
		try {
			parent = TalkUtil.getUserFile("sounds");
		} catch (Exception e) {
		}
		if (parent == null)
			parent = new File("D:\\software\\camtalk\\TalkPC\\Users\\008613426351153\\sounds");
		return parent;
	}

	//ffmpeg -acodec libamr_nb -i shenhuxi.amr amr2wav.wav
	public static byte[] amrToWav(byte[] data) {
		File parent = getParentFile();
		if (parent == null)
			return null;

		String uuid = UUID.randomUUID().toString();
		String srcName = uuid + ".amr";
		String destName = uuid + ".wav";

		File srcFile = new File(parent.getPath(), srcName);
		File destFile = new File(parent.getPath(), destName);
		byte[] bufferInput;
		try {
			writeToFile(srcFile, data);

			AudioAttributes audio = new AudioAttributes();
			audio.setCodec("pcm_s16le");

			EncodingAttributes attrs = new EncodingAttributes();
			attrs.setFormat("wav");
			attrs.setAudioAttributes(audio);
			Encoder encoder = new Encoder();
			encoder.encode(srcFile, destFile, attrs);

		} catch (Exception e) {
			Log.error(e.getMessage());
		} finally {
			bufferInput = IOUtil.readBytes(destFile);

			if (destFile != null) {
				destFile.delete();
			}
			if (srcFile != null) {
				srcFile.delete();
			}
		}
		return bufferInput;
	}

	//ffmpeg -acodec libamr_nb -i shenhuxi.amr amr2wav.wav
	//ffmpeg -i test.wav -acodec libamr_nb -ab 12.2k -ar 8000 -ac 1 wav2amr.amr
	public static byte[] wavToAmr(byte[] data) {
		File parent = getParentFile();
		if (parent == null)
			return null;

		String uuid = UUID.randomUUID().toString();
		String srcName = uuid + ".wav";
		String destName = uuid + ".amr";

		File srcFile = new File(parent.getPath(), srcName);
		File destFile = new File(parent.getPath(), destName);
		byte[] bufferInput;
		try {
			writeToWavFile(srcFile, data);

			destFile = new File(parent.getPath(), destName);

			AudioAttributes audio = new AudioAttributes();
			audio.setCodec("libamr_nb");
			audio.setBitRate(new Integer(12200));
			audio.setChannels(new Integer(1));
			audio.setSamplingRate(new Integer(8000));

			EncodingAttributes attrs = new EncodingAttributes();
			attrs.setFormat("amr");
			attrs.setAudioAttributes(audio);
			Encoder encoder = new Encoder();
			encoder.encode(srcFile, destFile, attrs);
		} catch (Exception e) {
			Log.error(e.getMessage());
			return null;
		} finally {
			bufferInput = IOUtil.readBytes(destFile);
			if (destFile != null) {
				destFile.delete();
			}
			if (srcFile != null) {
				srcFile.delete();
			}
		}
		return bufferInput;
	}

	//	// （4）保存文件
	public static void writeToWavFile(File file, byte audioData[]) {
		if (!file.getParentFile().exists()) {
			return;
		}
		// 取得录音输入流
		AudioFormat audioFormat = getAudioFormat();

		InputStream byteArrayInputStream = new ByteArrayInputStream(audioData);

		AudioInputStream audioInputStream = new AudioInputStream(byteArrayInputStream, audioFormat, audioData.length / audioFormat.getFrameSize());
		// 写入文件
		try {
			AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// （4）保存文件
	public static void writeToFile(File file, byte audioData[]) {
		if (!file.getParentFile().exists()) {
			return;
		}
		IOUtil.writeBytes(file, audioData);
	}

	public static long getDuration(byte[] wav) {
		Encoder encoder = new Encoder();
		final File file = new File(getParentFile(), UUID.randomUUID().toString() + ".d");
		try {
			writeToWavFile(file, wav);
			MultimediaInfo m = encoder.getInfo(file);
			long x = m.getDuration() / 1000;
			if (x == 0)
				x = 1;
			return x;
		} catch (Exception e) {
		} finally {
			TaskEngine.getInstance().submit(new Runnable() {

				@Override
				public void run() {
					boolean delete = file.delete();
					int i = 0;
					while (!delete) {
						if (i++ > 100) {
							break;
						}
						SystemUtil.sleep(100);
						delete = file.delete();
					}
				}
			});
		}
		return 0;
	}

	public static long getDuration(List<byte[]> wavs) {
		long duration = 0;
		for (byte[] wav : wavs) {
			duration += getDuration(wav);
		}
		return duration;
	}

	// 取得AudioFormat
	public static AudioFormat getAudioFormat() {
		// 44100,16,2
		// 8000,11025,16000,22050,44100
		float sampleRate = 8000.0F;

		// 8,16
		int sampleSizeInBits = 16;

		// 1,2
		int channels = 1;

		// true,false
		boolean signed = true;

		// true,false
		boolean bigEndian = false;

		return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
	}
}
