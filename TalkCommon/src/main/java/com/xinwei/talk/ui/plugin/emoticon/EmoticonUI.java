/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月14日 下午8:14:26
 * 
 ***************************************************************/
package com.xinwei.talk.ui.plugin.emoticon;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Collection;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lookandfeel.McWillImageIcon;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.util.ImageUtil;

public class EmoticonUI extends JPanel {
	private static final long serialVersionUID = 2360054381356167669L;
	private EmoticonPickListener listener;

	public EmoticonUI() {
		setBackground(Color.white);

		final EmoticonManager manager = EmoticonManager.getInstance();

		Collection<McWillImageIcon> emoticons = manager.getEmoticons();

		if (emoticons != null) {

			int no = emoticons.size();

			int rows = no / 7;

			setLayout(new GridLayout(rows, 7));

			// Add Emoticons
			for (final McWillImageIcon emoticon : emoticons) {
				McWillIconButton emotButton = new McWillIconButton();
				URL url = emoticon.getUrl();
				emotButton.setIcon(new ImageIcon(ImageUtil.resize(IOUtil.readBytes(url), 24, 24)));
				emotButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						listener.emoticonPicked(emoticon);
					}
				});
				add(emotButton);
			}
		}
	}

	public void setEmoticonPickListener(EmoticonPickListener listener) {
		this.listener = listener;
	}

	public interface EmoticonPickListener {
		void emoticonPicked(McWillImageIcon emoticon);
	}
}
