/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月1日 上午9:36:49
 * 
 ***************************************************************/
package com.xinwei.talk.ui.main.search;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;

import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.component.McWillCollapsiblePane;
import com.xinwei.common.lookandfeel.component.McWillColorPanel;
import com.xinwei.common.lookandfeel.component.McWillList;
import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.ui.main.list.ListItem;
import com.xinwei.talk.ui.util.component.XWScrollPane;

@SuppressWarnings("serial")
public class SearchListPanel extends McWillColorPanel {
	private McWillList<JComponent> friendList;
	private DefaultListModel<JComponent> friendListModel;
	private McWillList<JComponent> groupList;
	private DefaultListModel<JComponent> groupListModel;
	private McWillCollapsiblePane friendCollapsiblePane;
	private McWillCollapsiblePane groupCollapsiblePane;

	private JPanel panel;

	public SearchListPanel() {
		setLayout(new BorderLayout(0, 0));

		panel = new JPanel(new VerticalFlowLayout(VerticalFlowLayout.TOP, 0, 0, true, false));

		//friend
		friendListModel = new DefaultListModel<JComponent>();
		friendList = new McWillList<JComponent>(friendListModel);
		friendList.setCellRenderer(new MyListCellRenderer());
		MyMouseListener mouseListener = new MyMouseListener();
		friendList.addMouseListener(mouseListener);
		MyKeyListener keyListener = new MyKeyListener();
		friendList.addKeyListener(keyListener);
		friendList.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		friendCollapsiblePane = new McWillCollapsiblePane(Res.getMessage("contact"));
		friendCollapsiblePane.setContentPane(friendList);
		//group
		groupListModel = new DefaultListModel<JComponent>();
		groupList = new McWillList<JComponent>(groupListModel);
		groupList.setCellRenderer(new MyListCellRenderer());
		groupList.addMouseListener(mouseListener);
		groupList.addKeyListener(keyListener);
		groupList.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		groupCollapsiblePane = new McWillCollapsiblePane(Res.getMessage("group"));
		groupCollapsiblePane.setContentPane(groupList);

		panel.add(friendCollapsiblePane);
		panel.add(groupCollapsiblePane);

		XWScrollPane scrollIndicator = new XWScrollPane(panel);
		add(scrollIndicator, BorderLayout.CENTER);

	}

	public void updateUI(String text) {
		List<SearchItemInfo> camtalkItemList = new ArrayList<SearchItemInfo>();
		List<SearchItemInfo> groupItemList = new ArrayList<SearchItemInfo>();

		Collection<Talk> camtalks = TalkManager.getLocalManager().getTalkList();

		for (Talk camtalk : camtalks) {
			SearchItemInfo searchItemInfo = new SearchItemInfo(camtalk);
			boolean flag = true;
			if (text != null) {
				flag = searchItemInfo.search(text);
			}
			if (flag) {
				camtalkItemList.add(searchItemInfo);
			}
		}
		Collection<Group> groupList = TalkManager.getLocalManager().getGroupList();

		for (Group group : groupList) {
			SearchItemInfo searchItemInfo = new SearchItemInfo(group);
			boolean flag = true;
			if (text != null) {
				flag = searchItemInfo.search(text);
			}
			if (flag) {
				groupItemList.add(searchItemInfo);
			}
		}

		updateUI(camtalkItemList, groupItemList);
	}

	private void updateUI(List<SearchItemInfo> camtalkItemList, List<SearchItemInfo> groupItemList) {
		//清除所有数据和界面
		friendListModel.clear();
		groupListModel.clear();
		panel.removeAll();
		panel.updateUI();

		if (camtalkItemList.isEmpty() && groupItemList.isEmpty()) {
			JLabel captionLabel = new JLabel(Res.getMessage("message.contact.search.result"));
			captionLabel.setHorizontalTextPosition(JLabel.CENTER);
			captionLabel.setFont(LAF.getFont(14));
			captionLabel.setForeground(LAF.getColor(100, 100, 100));
			captionLabel.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 0));
			captionLabel.setOpaque(true);
			captionLabel.setBackground(McWillTheme.getThemeColor().getMiddleColor());

			panel.add(captionLabel);
			return;
		}

		ItemSort sort = new ItemSort();

		if (!camtalkItemList.isEmpty()) {
			Collections.sort(camtalkItemList, sort);

			for (final SearchItemInfo searchItemInfo : camtalkItemList) {
				SearchListItem leftMemItemPanel = new SearchListItem(searchItemInfo);
				friendListModel.addElement(leftMemItemPanel);

			}
			panel.add(friendCollapsiblePane);
			friendCollapsiblePane.setCollapsed(false);
		}
		if (!groupItemList.isEmpty()) {
			Collections.sort(groupItemList, sort);

			for (final SearchItemInfo searchItemInfo : groupItemList) {
				SearchListItem leftMemItemPanel = new SearchListItem(searchItemInfo);
				groupListModel.addElement(leftMemItemPanel);

			}
			panel.add(groupCollapsiblePane);
			groupCollapsiblePane.setCollapsed(false);
		}

	}

	public void doubleClicked() {
		JComponent selectedValue = friendList.getSelectedValue() != null ? friendList.getSelectedValue() : groupList.getSelectedValue();
		if (selectedValue == null && (friendListModel.getSize() + groupListModel.getSize() == 1)) {
			if (friendListModel.isEmpty()) {
				selectedValue = groupListModel.getElementAt(0);
			} else {
				selectedValue = friendListModel.getElementAt(0);
			}
		}

		if (selectedValue instanceof SearchListItem) {
			SearchListItem item = (SearchListItem) selectedValue;
			doubleClickedItem(item.getSearchItemInfo().obj);

		}

	}

	private void checkPopup(MouseEvent e) {
		if (!e.isPopupTrigger())
			return;
		// Otherwise, handle single selection
		Point point = e.getPoint();
		List<Object> items = new ArrayList<Object>();
		if (e.getSource() == groupList) {
			int index = groupList.locationToIndex(point);
			if (index != -1) {
				int[] indexes = groupList.getSelectedIndices();
				boolean selected = false;
				for (int o : indexes) {
					if (index == o) {
						selected = true;
					}
				}

				if (!selected) {
					groupList.setSelectedIndex(index);
				}
			}

			for (Object item : groupList.getSelectedValues()) {
				items.add(item);
			}
		} else if (e.getSource() == friendList) {
			int index = friendList.locationToIndex(point);
			if (index != -1) {
				int[] indexes = friendList.getSelectedIndices();
				boolean selected = false;
				for (int o : indexes) {
					if (index == o) {
						selected = true;
					}
				}
				if (!selected) {
					friendList.setSelectedIndex(index);
				}
			}
			for (Object item : friendList.getSelectedValues()) {
				items.add(item);
			}
		}
		firePopupEvent(e, items);
	}

	private void firePopupEvent(MouseEvent e, final Collection<Object> items) {
		if (CollectionUtil.isEmpty(items)) {
			return;
		}

		Object object = null;
		for (Object item : items) {
			if (item instanceof SearchListItem) {
				SearchListItem searchListItem = (SearchListItem) item;
				object = searchListItem.getSearchItemInfo().obj;
				if (object != null) {
					break;
				}
			}
		}

		if (object == null) {
			return;
		}
		popup.removeAll();
		addPopuMenuItemList(popup, object);

		popup.show(e.getComponent(), e.getX(), e.getY());
	}

	final JPopupMenu popup = new JPopupMenu();

	protected void addPopuMenuItemList(JPopupMenu popup, Object object) {
	}

	protected void doubleClickedItem(Object object) {
	}

	//排序  
	public class ItemSort implements Comparator<SearchItemInfo> {
		@Override
		public int compare(SearchItemInfo o1, SearchItemInfo o2) {
			return o1.withoutTone.compareTo(o2.withoutTone);
		}
	}

	// ---------
	class MyMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getSource() == groupList) {
				friendList.clearSelection();
			} else if (e.getSource() == friendList) {
				groupList.clearSelection();
			}

			if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
				doubleClicked();
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			checkPopup(e);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			checkPopup(e);
		}
	}

	class MyKeyListener extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyChar() == KeyEvent.VK_ENTER) {
				doubleClicked();
			}
		}
	}

	public class MyListCellRenderer implements ListCellRenderer<JComponent> {
		@Override
		public Component getListCellRendererComponent(JList<? extends JComponent> list, JComponent value, int index, boolean isSelected, boolean cellHasFocus) {
			value.setOpaque(true);
			ThemeColor themeColor = McWillTheme.getThemeColor();
			if (isSelected || cellHasFocus) {
				value.setBackground(themeColor.getMiddleColor1());
			} else {
				Color backgroundColor = ((McWillList) list).getRolloverIndex() == index ? themeColor.getLightColor() : list.getBackground();
				value.setBackground(backgroundColor);
			}
			return value;
		}
	}
}
