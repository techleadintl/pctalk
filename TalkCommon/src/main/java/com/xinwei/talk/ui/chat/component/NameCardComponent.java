/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月29日 下午4:51:10
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import com.xinwei.common.lookandfeel.component.McWillLine;
import com.xinwei.common.lookandfeel.component.McWillWindowButtonUI;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkNameCardSpecific;

public class NameCardComponent extends JPanel {
	List<TalkSpecific> namecardMsgList;

	public NameCardComponent(List<TalkSpecific> namecardMsgList) {
		this.namecardMsgList = namecardMsgList;

		setLayout(new VerticalFlowLayout(VerticalFlowLayout.LEFT));

		int size = namecardMsgList.size();
		int row = size / 2 + (size % 2 == 0 ? 0 : 1);
		int column = size > 1 ? 2 : size;

		JPanel cardPanel = new JPanel(new GridLayout(row, column, 5, 5));
		for (TalkSpecific msg : namecardMsgList) {
			final TalkNameCardSpecific cardMsg = (TalkNameCardSpecific) msg;
			JButton button = new JButton();
			button.setContentAreaFilled(false);
			button.setBorderPainted(false);
			button.setCursor(new Cursor(Cursor.HAND_CURSOR));
			button.setFocusPainted(false);
			button.setHorizontalAlignment(SwingConstants.LEFT);
			button.setBorder(BorderFactory.createEmptyBorder());
			button.setIcon(LAF.getMsgNameCard());
			button.setText(cardMsg.getNick());
			button.setPreferredSize(new Dimension(115, 42));
			final NameCardDialog nameCardDialog = new NameCardDialog(cardMsg);
			button.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					nameCardDialog.setVisible(true);
				}
			});

			cardPanel.add(button);
		}
		int w = (115 + 5) * column;
		int h = (42 + 5) * row + 32;

		setPreferredSize(new Dimension(0, h));
		setMaximumSize(new Dimension(w, h));
		//		setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
		JLabel l = new JLabel(Res.getMessage("namecard.title"));
		l.setFont(LAF.getFont(14));
		l.setForeground(Color.BLACK);
		//		l.setHorizontalAlignment(SwingConstants.CENTER);
		add(l);
		add(new McWillLine() {
			@Override
			public Color getBackground() {
				return Color.GRAY;
			}
		});
		add(cardPanel);
	}

	public class NameCardDialog extends JDialog implements McWillWindowButtonUI {
		@Override
		public boolean isMax() {
			return false;
		}

		@Override
		public boolean isIconify() {
			return true;
		}

		public NameCardDialog(TalkNameCardSpecific card) {
			setLayout(new BorderLayout());
			setUndecorated(true);//去除窗体
			setAlwaysOnTop(true); //设置界面悬浮

			//			JRootPane rootPane = getRootPane();
			//			((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

			setLocationRelativeTo(TalkManager.getChatFrame());

			setResizable(false);

			setTitle(LAF.getMessage("namecard.title"));
			//			JPanel contentPane = createDialogContainer();
			//			setContentPane(contentPane);

			JTextPane panel = new JTextPane();

			panel.setOpaque(true);
			panel.setEditable(false);

			panel.setContentType("text/html");
			String table = "<html>" + "<table><tr><td width='10%'><img src={13} height=40></td><td>{0}</td></tr></table><hr/>" //
					+ "<table>" //
					+ "<tr><td><font color=#000000>{1}</td><td>{2}</td></tr>"//
					+ "<tr><td><font color=#000000>{3}</td><td>{4}</td></tr>" //
					+ "<tr><td><font color=#000000>{5}</td><td>{6}</td></tr>"//
			//		    + "<tr><td><font color=#000000>{7}</td><td>{8}</td></tr>"//
					+ "<tr><td><font color=#000000>{9}</td><td>{10}</td></tr>" //
					+ "<tr><td><font color=#000000>{11}</td><td>{12}</td></tr>" //
					+ "</table></html>";

			String html = MessageFormat.format(table, //
					card.getNick(), //
					Res.getMessage("namecard.tel"), card.getMobile(), //
					Res.getMessage("namecard.firstname"), card.getFirstname(), //
					Res.getMessage("namecard.lastname"), card.getLastname(), //
					Res.getMessage("namecard.middlename"), card.getMiddlename(), //
					Res.getMessage("namecard.note"), card.getNote(), //
					Res.getMessage("namecard.signature"), card.getSignature(), //
					LAF.getURL("talk/images/room/component/namecard.png").toString());//

			panel.setText(html);
			panel.select(0, 0);
			panel.setCaretPosition(0);
			add(panel, BorderLayout.CENTER);

			setSize(240, 250);
		}

		protected JPanel createDialogContainer() {
			return new McWillContentPanel() {
				public void paintComponent(Graphics g) {
					super.paintComponent(g);
					//					LAF.paintTrayDialog(this, g);
				}
			};
		}

		public class NameCardPanel extends JTextPane {
			//			private JLabel nickLabel;
			//			private JLabel firstNameLabel;
			//			private JLabel middleNameLabel;
			//			private JLabel lastNameLabel;
			//			private JLabel nodeLabel;
			//			private JLabel signatureLabel;

			//			addI18n("namecard.title", "Name Card", "名片");
			//			addI18n("namecard.tel", "Tel", "电话");
			//			addI18n("namecard.middlename", "Middle Name", "中间名");
			//			addI18n("namecard.firstname", "First Name", "名");
			//			addI18n("namecard.lastname", "Last Name", "姓");
			//			addI18n("namecard.note", "Note", "备注");
			//			addI18n("namecard.signature", "Signature", "签名");

			private String jid;

			public NameCardPanel() {

				setOpaque(true);

				setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

				setContentType("text/html");

			}

			protected Color DEFAULT_COLOR = new Color(240, 240, 240);

			protected Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 11);

		}
	}

}
