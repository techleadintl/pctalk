package com.xinwei.talk.ui.main.dialog.member.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jivesoftware.smack.util.StringUtils;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.ui.main.dialog.member.model.MemberInfo;

public class LeftMemItemUI extends JPanel {
	private static final long serialVersionUID = 1514044406550293152L;
	private static Icon uncheckImage = LAF.getUncheckIcon();
	private static Icon checkImage = LAF.getCheckIcon();

	private boolean check;

	private JButton checkLabel;

	//组
	private MemberInfo memberInfo;

	//显示名称 文字标签
	protected JLabel nameTxtLabel;

	//显示称 文字标签
	protected JLabel telLabel;

	//状态 图标标签
	protected JLabel avatarIconLabel;

	protected int iconSize;

	MouseAdapter selectedListener = new MouseAdapter() {
		public void mousePressed(MouseEvent e) {
			boolean newCheck = !LeftMemItemUI.this.check;
			
			setCheckLabelImage(newCheck);
			
			onMemItemChecked(LeftMemItemUI.this.check, newCheck);

			LeftMemItemUI.this.check = newCheck;
			
			
		};
	};

	MouseAdapter backgroudListener = new MouseAdapter() {
		public void mousePressed(MouseEvent e) {
			onMemItemClick();
		};
	};

	public LeftMemItemUI(MemberInfo memberInfo, boolean check) {
		this.memberInfo = memberInfo;
		this.check = check;
		this.setOpaque(true);
		setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
		setBackground(McWillTheme.getThemeColor().getLightColor());

		this.setLayout(new BorderLayout());

		JPanel leftPanel = new JPanel(new GridBagLayout());
		leftPanel.setPreferredSize(new Dimension(265, 55));
		avatarIconLabel = new McWillRolloverLabel();

		//		avatarIconLabel.setBorder(McWillBorderFactory.getInstance().getLabelLightBorder());

		nameTxtLabel = new JLabel();
		nameTxtLabel.setHorizontalTextPosition(JLabel.LEFT);
		nameTxtLabel.setHorizontalAlignment(JLabel.LEFT);

		telLabel = new JLabel();
		telLabel.setHorizontalTextPosition(JLabel.LEFT);
		telLabel.setHorizontalAlignment(JLabel.LEFT);

		leftPanel.add(avatarIconLabel, new GridBagConstraints(0, 0, 1, 2, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 10, 5, 0), 0, 0));
		leftPanel.add(nameTxtLabel, new GridBagConstraints(1, 0, 1, 1, 1.0D, 0.5D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));
		leftPanel.add(telLabel, new GridBagConstraints(1, 1, 1, 1, 1.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));

		ImageIcon avatar = ImageManager.getTalkAvatar(memberInfo.jid, 45, 45);
		avatarIconLabel.setIcon(avatar);
		setDisplayText();

		add(leftPanel, BorderLayout.CENTER);

		checkLabel = new McWillIconButton();
		setCheckLabelImage(check);
		

		add(this.checkLabel, BorderLayout.EAST);

		addCheckedListener(this, backgroudListener);

		if (!check) {
			addCheckedListener(this, selectedListener);
		} else {
			checkLabel.setEnabled(false);
		}
	}

	public boolean getCheck() {
		return this.check;
	}

	public void onMemItemChecked(boolean oldCheck, boolean newCheck) {

	}

	public void setCheckLabelImage(boolean check) {
		if (check) {
			this.checkLabel.setIcon(checkImage);
		} else {
			this.checkLabel.setIcon(uncheckImage);
		}
	}

	public void onMemItemClick() {

	}

	public String getJID() {
		return memberInfo.jid;
	}

	protected void setDisplayText() {
		String displayName = getDisplayName();

		nameTxtLabel.setText(displayName);

		nameTxtLabel.setToolTipText(displayName);

		String displayTel = getDisplayTel();
		telLabel.setForeground(Color.GRAY);
		telLabel.setText(displayTel);
		telLabel.setToolTipText(displayTel);
	}

	public String getDisplayName() {
		final String displayName = memberInfo.displayName;
		return StringUtils.unescapeNode(displayName);
	}

	public String getDisplayTel() {
		final String fullJid = memberInfo.jid;
		return TalkUtil.getUserTel(fullJid);
	}

	protected void addCheckedListener(Component p, MouseListener mouseListener) {
		Component[] components = null;
		if (p instanceof JComponent) {
			JComponent childComponent = (JComponent) p;
			childComponent.addMouseListener(mouseListener);
			components = childComponent.getComponents();
		} else if (p instanceof Container) {
			components = ((Container) p).getComponents();
		}
		if (components == null)
			return;

		for (Component c : components) {
			addCheckedListener(c, mouseListener);
		}
	}
}
