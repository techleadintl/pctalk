package com.xinwei.talk.ui.login;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillButton;
import com.xinwei.common.lookandfeel.component.McWillLinkButton;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;

public class LoginBottom extends JPanel {
	JPanel errorPanel;
	JLabel label;

	LoginDialog dialog;

	int H = 30;

	public LoginBottom(final LoginDialog dialog) {
		this.setLayout(new BorderLayout(0, 0));
		this.setOpaque(false);
		this.dialog = dialog;
		McWillLinkButton registerButton = new McWillLinkButton();
		registerButton.setText(Res.getMessage("label.login.register"));
		registerButton.setLinkVisited(false);
		registerButton.setLinkColor(Color.WHITE);
		registerButton.setActiveLinkColor(Color.WHITE);

		registerButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.getFrame().setExtendedState(JFrame.ICONIFIED);
				SwingUtil.openBrowseURL(Res.getProperty("talk.register"));
			}
		});

		errorPanel = new JPanel(new BorderLayout(0, 0));
		errorPanel.setBackground(new Color(73, 165, 255));
		errorPanel.setPreferredSize(new Dimension(0, H));
		label = new JLabel();
		label.setForeground(new Color(55, 55, 55));

		McWillButton comp = new McWillButton(LAF.getLoginUpIcon());
		comp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showErrorPanel(false);
			}
		});
		errorPanel.setOpaque(true);
		errorPanel.setVisible(false);
		errorPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

		errorPanel.add(comp, BorderLayout.EAST);
		errorPanel.add(label, BorderLayout.WEST);
		add(registerButton, BorderLayout.CENTER);
		add(errorPanel, BorderLayout.SOUTH);
	}

	public void showMessage(String tel) {
		if (tel != null) {
			String message = Res.getMessage("message.login.relogin.error");
			label.setText(MessageFormat.format(message, tel));
		}
		showErrorPanel(tel != null);
	}

	public void showErrorPanel(boolean flag) {
		if (errorPanel.isVisible() && flag) {
			return;
		}
		if (!errorPanel.isVisible() && !flag) {
			return;
		}

		if (flag) {
			TaskEngine.getInstance().schedule(new TimerTask() {
				@Override
				public void run() {
					showErrorPanel(false);
				}
			}, 1000 * 15);
		}

		Rectangle b = getBounds();
		JFrame frame = dialog.getFrame();
		errorPanel.setVisible(flag);
		int h = 0;
		int y = 3;
		if (flag) {
			h = H;
			y = 0 - 3;
		} else {
			h = 0 - H;
			y = 3;
		}
		setBounds(b.x, b.y + y, b.width, b.height + h);
		frame.setSize(frame.getWidth(), frame.getHeight() + h);
	}
}
