/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月5日 下午5:36:14
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.balloon;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class TextPanePainter {
	//	public static Color centerColor = new Color(188, 237, 245);
	//	public static Color centerBorderColor = new Color(156, 205, 213);
	public static Color centerColor = new Color(240, 240, 240);
	public static Color centerBorderColor = new Color(180, 180, 180);
	public static Color leftColor = new Color(230, 230, 230);
	public static Color leftBorderColor = new Color(198, 198, 198);
	public static Color rightColor = new Color(152, 225, 101);
	public static Color rightBorderColor = new Color(131, 217, 39);

	public static Color getColor(BalloonMessage message) {
		int alignment = message.getAlignment();
		if (alignment == BalloonMessage.ALIGNMENT_LEFT) {
			return leftColor;
		} else if (alignment == BalloonMessage.ALIGNMENT_RIGHT) {
			return rightColor;
		} else {
			return centerColor;
		}
	}

	/**
	 * 
	 * @param g2D
	 * @param message
	 * @param rec
	 * @param border
	 *            气泡边框
	 * @param bubbleTriangleWidth
	 *            消息气泡小箭头直角宽度
	 * @param bubbleTriangleHeight
	 *            消息气泡小箭头直角高度
	 * @param bubbleTriangleY
	 *            消息气泡小箭头上边距
	 * @param arcWidth
	 * @param arcHeight
	 * @param textRect
	 *            文字区域
	 * @param color
	 * @param borderColor
	 */
	public static void drawColorBalloon(Graphics2D g2D, BalloonMessage message, Rectangle rec, int border, int bubbleTriangleWidth, int bubbleTriangleHeight, int bubbleTriangleY, int arcWidth, int arcHeight, Rectangle textRect, Color color, Color borderColor) {
		g2D.setColor(color);
		g2D.fillRoundRect(textRect.x, textRect.y, textRect.width, textRect.height, arcWidth, arcHeight);
		// 绘制圆角消息边框  
		g2D.setColor(borderColor);
		g2D.drawRoundRect(textRect.x, textRect.y, textRect.width, textRect.height, arcWidth, arcHeight);

		// 绘制圆角消息气泡  
		if (message.isBalloon()) {
			// 绘制额消息气泡小箭头  
			int xPoints[] = new int[3];
			int yPoints[] = new int[3];
			if (message.getAlignment() == BalloonMessage.ALIGNMENT_RIGHT) { //BalloonMessage.ALIGNMENT_LEFT
				// 头像靠左显示  
				xPoints[0] = rec.x - border;
				yPoints[0] = rec.y + bubbleTriangleY;
				xPoints[1] = xPoints[0] - bubbleTriangleWidth;
				yPoints[1] = yPoints[0] + 2;
				xPoints[2] = xPoints[0];
				yPoints[2] = yPoints[0] + bubbleTriangleHeight;
			} else {
				// 头像靠右显示  
				xPoints[0] = (rec.x - border) + (rec.width + 2 * border);
				yPoints[0] = rec.y + bubbleTriangleY;
				xPoints[1] = xPoints[0] + bubbleTriangleWidth;
				yPoints[1] = yPoints[0] + 2;
				xPoints[2] = xPoints[0];
				yPoints[2] = yPoints[0] + bubbleTriangleHeight;
			}

			g2D.setColor(color);
			g2D.fillPolygon(xPoints, yPoints, 3);
			g2D.setColor(borderColor);
			g2D.drawPolyline(xPoints, yPoints, 3);
			g2D.setColor(color);
			g2D.drawLine(xPoints[0], yPoints[0] + 1, xPoints[2], yPoints[2] - 1);
		}
	}

}
