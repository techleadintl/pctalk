package com.xinwei.talk.ui.skin.skin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.Window;

import javax.swing.JColorChooser;
import javax.swing.JDialog;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillWindowButtonUI;
import com.xinwei.talk.ui.skin.McWillSkinContentPanel;

@SuppressWarnings("serial")
public class SkinColorChooserDialog extends JDialog implements McWillWindowButtonUI {
	public SkinColorChooserDialog(Window owner, String title) throws HeadlessException {
		super(owner);

		setModal(true);
		setResizable(false);
		McWillSkinContentPanel skinContentPanel = new McWillSkinContentPanel();

		setTitle(title);

		Color color = Color.white;
		if (McWillTheme.isThemeColor())
			color = McWillTheme.getThemeColor().getSrcColor();
		final SkinColorChooserUI skinColorChooserUI = new SkinColorChooserUI();
		JColorChooser pane = new JColorChooser(color) {
			@Override
			public void updateUI() {
				setUI(skinColorChooserUI);
			}
		};
		skinContentPanel.add(pane, BorderLayout.CENTER);

		setContentPane(skinContentPanel);

		pack();

		setLocationRelativeTo(owner);

		McWillTheme.setOpaque(pane);
	}

	@Override
	public boolean isMax() {
		return false;
	}

	@Override
	public boolean isIconify() {
		return false;
	}
}