package com.xinwei.talk.ui.util.component;
//package com.xinwei.talk.ui.util.component;
//
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.swing.ImageIcon;
//import javax.swing.JLabel;
//
//import com.xinwei.talk.common.LAF;
//
//public class XWRadio extends JLabel {
//	private boolean selection;
//	private ImageIcon uncheckImage;
//	private ImageIcon checkImage;
//	private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
//	private XWRadioGroup group;
//
//	public XWRadio() {
//		this(LAF.getUncheckIcon(), LAF.getCheckIcon(), false);
//	}
//
//	public XWRadio(boolean check) {
//		this(LAF.getUncheckIcon(), LAF.getCheckIcon(), check);
//	}
//
//	public XWRadio(ImageIcon uncheckImage, ImageIcon checkImage) {
//		this(uncheckImage, checkImage, false);
//	}
//
//	public XWRadio(ImageIcon uncheckImage, ImageIcon checkImage, boolean check) {
//		this.uncheckImage = uncheckImage;
//		this.checkImage = checkImage;
//		setSelected(check);
//		addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseClicked(MouseEvent e) {
//				if (!enable) {
//					return;
//				}
//				XWCheck.this.setSelected(!XWCheck.this.selected);
//				for (ActionListener actionListener : actionListeners) {
//					actionListener.actionPerformed(new ActionEvent(XWCheck.this, 0, getActionCommand()));
//				}
//			}
//		});
//	}
//
//	public void setActionCommand(String action) {
//		putClientProperty("$ActionCommand", action);
//	}
//
//	public XWRadio(Composite parent, Image uncheckImage, Image checkImage) {
//		this.uncheckImage = uncheckImage;
//		this.checkImage = checkImage;
//		newInstance(parent, 0);
//	}
//
//	public void newInstance(Composite parent, String text) {
//		newInstance(parent, 0);
//		this.label.setText(text);
//	}
//
//	public CLabel newInstance(Composite parent, int style) {
//		this.label = new CLabel(parent, style);
//		this.label.addMouseListener(new MouseAdapter() {
//			public void mouseDown(MouseEvent e) {
//				XWRadio.this.setGroupSelected();
//			}
//		});
//		setSelection(false);
//		return this.label;
//	}
//
//	public CLabel getLabel() {
//		return this.label;
//	}
//
//	public void setForeground(Color color) {
//		this.label.setForeground(color);
//	}
//
//	public void setBackground(Color color) {
//		this.label.setBackground(color);
//	}
//
//	public void setFont(Font font) {
//		this.label.setFont(font);
//	}
//
//	private void setGroupSelected() {
//		if ((this.group == null) || (this.group.isEmpty())) {
//			setSelection(!this.selection);
//			return;
//		}
//
//		if (!this.selection) {
//			for (XWRadio radio : this.group) {
//				if (radio == this)
//					radio.setSelection(true);
//				else
//					radio.setSelection(false);
//			}
//		} else {
//			setSelection(true);
//		}
//
//		afterSelected();
//	}
//
//	protected void afterSelected() {
//	}
//
//	public void setLayout(Layout layout) {
//		this.label.setLayout(layout);
//	}
//
//	public void setLayoutData(Object layoutData) {
//		this.label.setLayoutData(layoutData);
//	}
//
//	public String getText() {
//		return this.label.getText();
//	}
//
//	public void setText(String text) {
//		this.label.setText(text);
//	}
//
//	public String getToolTipText() {
//		return this.label.getToolTipText();
//	}
//
//	public void setToolTipText(String toolTipText) {
//		this.label.setToolTipText(toolTipText);
//	}
//
//	public boolean getSelection() {
//		return this.selection;
//	}
//
//	public void setSelection(boolean selection) {
//		this.selection = selection;
//		setImage(selection);
//		Event event = new Event();
//		event.data = this;
//		if (selection)
//			this.label.notifyListeners(13, event);
//	}
//
//	private void setImage(boolean check) {
//		if (check)
//			this.label.setImage(this.checkImage);
//		else
//			this.label.setImage(this.uncheckImage);
//	}
//
//	public Image getUncheckImage() {
//		return this.uncheckImage;
//	}
//
//	public void setUncheckImage(Image uncheckImage) {
//		this.uncheckImage = uncheckImage;
//	}
//
//	public Image getCheckImage() {
//		return this.checkImage;
//	}
//
//	public void setCheckImage(Image checkImage) {
//		this.checkImage = checkImage;
//	}
//
//	public void setGroup(XWRadioGroup group) {
//		this.group = group;
//	}
//
//	public int getIndex() {
//		return this.group.getIndex(this);
//	}
//
//	public void addSelectionListener(SelectionListener listener) {
//		if (listener == null)
//			return;
//		TypedListener typedListener = new TypedListener(listener);
//		this.label.addListener(13, typedListener);
//	}
//
//	public Object getData() {
//		return this.label.getData();
//	}
//
//	public void setData(Object data) {
//		this.label.setData(data);
//	}
//
//	public Object getData(String MOUSE_KEY) {
//		return this.label.getData(MOUSE_KEY);
//	}
//
//	public void setData(String MOUSE_KEY, Object value) {
//		this.label.setData(MOUSE_KEY, value);
//	}
//
//	public Listener[] getListeners(int eventType) {
//		return this.label.getListeners(eventType);
//	}
//}