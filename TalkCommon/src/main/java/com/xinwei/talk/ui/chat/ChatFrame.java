/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月16日 下午1:57:34
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillTitlePane;
import com.xinwei.common.lookandfeel.component.McWillWindowButtonUI;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.listener.ChatFrameToFrontListener;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.chat.relay.RelayDialog;
import com.xinwei.talk.ui.chat.room.ChatRoom;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;

/**
 * The Window used to display the ChatRoom container.
 */
public class ChatFrame extends JFrame implements McWillWindowButtonUI {

	private static final long serialVersionUID = -7789413067818105293L;
	private Collection<ChatFrameToFrontListener> _windowToFrontListeners = new ArrayList<>();
	/** 转发消息窗口 ***/
	private RelayDialog relayDialog;

	/**
	 * Creates default ChatFrame.
	 * 
	 * @param bottomPanel
	 */
	public ChatFrame(final ChatContainer chatContainer, final JPanel rightPanel) {
		setIconImages(LAF.getApplicationImages());

		getContentPane().setLayout(new BorderLayout());

		JRootPane root = getRootPane();

		McWillTitlePane titlePanel = new McWillTitlePane(root, ((BaseRootPaneUI) root.getUI()));
		((BaseRootPaneUI) root.getUI()).setTitlePane(root, titlePanel);

		ChatFrameContentPanel frameContentPanel = new ChatFrameContentPanel() {
			@Override
			public void addTitlePanle(Component comp) {
				rightPanel.add(comp, BorderLayout.NORTH);
			}
		};
		frameContentPanel.setTitleTextIconVisible(false);
		frameContentPanel.setLayout(new BorderLayout(0, -3));
		frameContentPanel.add(chatContainer, BorderLayout.CENTER);

		setContentPane(frameContentPanel);

		setSize(715, 560);
		setMinimumSize(new Dimension(715, 500));

		SwingUtil.centerWindowOnScreen(this);

		List<WindowAdapter> chatWindowListeners = ListenerManager.getChatWindowListeners();
		if (chatWindowListeners != null) {
			for (WindowAdapter windowAdapter : chatWindowListeners) {
				addWindowFocusListener(windowAdapter);
				addWindowListener(windowAdapter);
			}
		}
	}

	/**
	 * Brings the ChatFrame into focus on the desktop.
	 */
	public void bringFrameIntoFocus() {
		if (getState() == Frame.ICONIFIED) {
			setState(Frame.NORMAL);
		}
//		setVisible(true);

		toFront();
		requestFocus();
	}

	/**
	 * set if the chatFrame should always stay on top
	 * 
	 * @param active
	 */
	public void setWindowAlwaysOnTop(boolean active) {
		setAlwaysOnTop(active);
		this.fireWindowOnTopListeners(active);
	}

	private void fireWindowOnTopListeners(boolean active) {
		for (ChatFrameToFrontListener fl : _windowToFrontListeners) {
			fl.updateStatus(active);
		}

	}

	/**
	 * removes the Window to Front Listener for specified {@link CommonChatRoom}
	 * 
	 * @param chatRoom
	 */
	public void removeWindowToFrontListener(ChatRoom chatRoom) {
		_windowToFrontListeners.remove(chatRoom);
	}

	/**
	 * 
	 * Remove listeners from the "window-alway-on-top" information
	 * 
	 * @param chatRoom
	 */
	public void addWindowToFronListener(ChatRoom chatRoom) {
		_windowToFrontListeners.add(chatRoom);
		fireWindowOnTopListeners(isAlwaysOnTop());
	}

	public RelayDialog getRelayDialog() {
		return relayDialog;
	}

	public void setRelayDialog(RelayDialog relayDialog) {
		this.relayDialog = relayDialog;
	}

	public void setVisible(boolean b) {
		if (!b) {
			setExtendedState(Frame.ICONIFIED);//为了释放内存
			if (TalkManager.getMainWindow().isVisible()) {
				TalkManager.getMainWindow().toFront();
			}
		}
		super.setVisible(b);
	}

	public boolean isMax() {
		return true;
	}

	@Override
	public boolean isIconify() {
		return true;
	}
}
