package com.xinwei.talk.ui.skin;

import java.awt.BorderLayout;
import java.awt.Frame;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import com.xinwei.common.lookandfeel.ui.tabbedpane.PPTTabbedPaneUI;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.skin.balloon.BalloonPanel;
import com.xinwei.talk.ui.skin.skin.SkinPanel;

public class WindowSkinDialog extends JFrame {

	private static final long serialVersionUID = 1L;

	JTabbedPane tabbedPane;

	private static WindowSkinDialog windowSkinDialog;

	public static synchronized WindowSkinDialog getInstance() {
		if (windowSkinDialog == null) {
			windowSkinDialog = new WindowSkinDialog();
			windowSkinDialog.toFront();
		}
		return windowSkinDialog;
	}

	public void showSkinTab() {
		tabbedPane.setSelectedIndex(0);
		setVisible(true);
		setExtendedState(Frame.NORMAL);
	}

	public void showBalloonTab() {
		tabbedPane.setSelectedIndex(1);
		setVisible(true);
		setExtendedState(Frame.NORMAL);
	}

	private WindowSkinDialog() {
		//"更改外观"
		super(Res.getMessage("skin.changeskin"));
		setIconImage(LAF.getApplicationImage().getImage());

		final McWillSkinContentPanel skinContentPanel = new McWillSkinContentPanel();
		setSize(622, 420);

		setResizable(false);

		final PPTTabbedPaneUI pptTabbedPaneUI = new PPTTabbedPaneUI();
		tabbedPane = new JTabbedPane() {
			public void updateUI() {
				setUI(pptTabbedPaneUI);
			}
		};

		tabbedPane.setOpaque(false);

		tabbedPane.putClientProperty("TabWidth", 65);

		tabbedPane.addTab(Res.getMessage("skin.style"), LAF.getMainTabSkin(15, 13)[1], new SkinPanel(this));

		tabbedPane.addTab(Res.getMessage("skin.balloon"), LAF.getRoomFontBalloonIcon(15, 13)[0], new BalloonPanel(this));

		skinContentPanel.add(tabbedPane, BorderLayout.CENTER);

		setContentPane(skinContentPanel);

		setLocationRelativeTo(TalkManager.getMainWindow());

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		//		addWindowListener(new WindowAdapter() {
		//			@Override
		//			public void windowClosing(WindowEvent e) {
		//				McWillTitlePane titlePanel = (McWillTitlePane) skinContentPanel.getTitlePanel();
		//				titlePanel.doIconify();
		//				super.windowClosing(e);
		//				windowSkinDialog = null;
		//			}
		//		});
	}
}
