/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月4日 下午8:28:53
 * 
 ***************************************************************/
package com.xinwei.talk.ui.main.content;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
import com.xinwei.talk.common.LAF;

@SuppressWarnings("serial")
public class ButtonPanel extends JPanel {
	public ButtonPanel() {
		setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP, 5, 30, true, false));
		setBorder(BorderFactory.createEmptyBorder(30, 0, 0, 0));
		setPreferredSize(new Dimension(40, 500));
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		LAF.paintMainButtonPanel(this, (Graphics2D) g);
	}

	public void setxx(JButton button) {

	}
}
