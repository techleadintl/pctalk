package com.xinwei.talk.ui.setting;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.JComponent;
import javax.swing.JSplitPane;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.lookandfeel.util.PainterUtil;

@SuppressWarnings("serial")
public class SettingFrameContentPanel extends McWillContentPanel {
	SettingContainer settingContainer;

	public SettingFrameContentPanel(SettingContainer settingContainer) {
		this.settingContainer = settingContainer;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		JSplitPane splitPane = (JSplitPane) settingContainer.getComponent(0);
		JComponent leftComponent = (JComponent) splitPane.getLeftComponent();
		JComponent rightComponent = (JComponent) splitPane.getRightComponent();

		McWillTheme.setOpaque(rightComponent);


		ThemeColor themeColor = McWillTheme.getThemeColor();

		Color color1 = themeColor.getSrcColor();
		Color color2 = themeColor.getSubDarkColor();
		Color color4 = themeColor.getSubLightColor();
		Color color5 = themeColor.getLightColor();
		//左侧
		Rectangle leftBounds = leftComponent.getBounds();
		PainterUtil.paintBackgroudColor(g2d, color1, leftBounds.x, leftBounds.y, leftBounds.width, leftBounds.height);

		//右侧
		Rectangle rightBounds = rightComponent.getBounds();
		PainterUtil.paintBackgroudColor(g2d, color4, rightBounds.x, rightBounds.y, rightBounds.width, rightBounds.height);
	}

}
