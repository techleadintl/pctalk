/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月22日 下午7:16:15
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.component;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.chat.TalkTextPane;
import com.xinwei.talk.ui.chat.TranscriptWindow;
import com.xinwei.talk.ui.chat.balloon.BalloonMessage;
import com.xinwei.talk.ui.chat.balloon.BalloonTextPane;
import com.xinwei.talk.ui.chat.room.ChatRoom;
import com.xinwei.talk.ui.util.component.XWDialogTitle;
import com.xinwei.talk.ui.util.component.dialog.XWDialog;

public class FireComponent extends McWillRolloverLabel {
	MessageDialog dialog;

	public FireComponent(final BalloonTextPane textPane, final BalloonMessage balloonMessage) {
		setOpaque(true);

		setPreferredSize(new Dimension(250, 50));

		setIcon(LAF.getTestImageIcon());

		MouseAdapter mouseListener = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (balloonMessage.getStatus() != BalloonMessage.STATUS_YES) {
					return;
				}
				String jid = balloonMessage.getTalkMessage().getJid();
				ChatRoom chatRoom = TalkManager.getChatManager().getChatContainer().getChatRoom(jid);
				if (chatRoom == null) {
					return;
				}
				TranscriptWindow transcriptWindow = chatRoom.getTranscriptWindow();
				if (transcriptWindow == null) {
					return;
				}
				transcriptWindow.deleteTalkMessage(jid, balloonMessage.getId());
				if (dialog != null && dialog.isVisible())
					return;
				dialog = new MessageDialog(balloonMessage);
				dialog.setVisible(true);
			}

		};
		addMouseListener(mouseListener);
	}

	public class MessageDialog extends XWDialog {
		private BalloonMessage balloonMsg;

		public MessageDialog(BalloonMessage balloonMsg) {
			super(TalkManager.getChatFrame(), "", false);
			this.balloonMsg = balloonMsg.clone();
			this.balloonMsg.clear();
			createDialog();
			JRootPane rootPane = getRootPane();
			((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

			setSize(new Dimension(450, 300));

			SwingUtil.centerWindowOnComponent(this, TalkManager.getChatFrame());
		}

		@Override
		protected JPanel createDialogContainer() {
			return new McWillContentPanel() {
				public void paintComponent(Graphics g) {
					super.paintComponent(g);
					LAF.paintMainAddGroupDialog(this, (Graphics2D) g);
				}
			};
		}

		@Override
		protected JPanel createDialogContent() {
			JPanel dialogPanel = new JPanel(new BorderLayout());
			XWDialogTitle titlePanel = new XWDialogTitle(this, Res.getMessage("dialog.title.fire"));
			titlePanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 0));
			dialogPanel.add(titlePanel, BorderLayout.NORTH);

			dialogPanel.add(createInputPanel(), BorderLayout.CENTER);

			return dialogPanel;
		}

		private JPanel createInputPanel() {
			JPanel inputPanel = new JPanel();
			MessagePanel msgPanel = new MessagePanel(balloonMsg);
			inputPanel.setLayout(new GridBagLayout());
			inputPanel.add(msgPanel, new GridBagConstraints(0, 4, 2, 1, 1.0D, 1.0D, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
			return inputPanel;
		}

		@Override
		protected LayoutManager getButtonBarLayout() {
			return new FlowLayout(FlowLayout.RIGHT, 30, 5);
		}

		protected JPanel createButtonBar() {
			JPanel bottomButtonPanel = new JPanel(getButtonBarLayout());
			bottomButtonPanel.setOpaque(false);

			closeButton = SwingUtil.newButton(Res.getMessage("button.close"));

			closeButton.addActionListener(this);

			bottomButtonPanel.add(closeButton);

			return bottomButtonPanel;
		}

		class MessagePanel extends JPanel {
			public MessagePanel(BalloonMessage balloonMsg) {
				this.setLayout(new BorderLayout());
				TalkTextPane textPane = new TalkTextPane();
				textPane.setEditable(false);
				textPane.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
				JScrollPane transcriptWindowScrollPanel = new JScrollPane(textPane);
				transcriptWindowScrollPanel.setAutoscrolls(true);
				transcriptWindowScrollPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
				transcriptWindowScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
				add(transcriptWindowScrollPanel, BorderLayout.CENTER);
				try {
					balloonMsg.getTalkMessage().getTalkBody().setFire(false);
					balloonMsg.setAvatar(false);
					balloonMsg.setBackground(false);
					balloonMsg.setAlignment(BalloonMessage.ALIGNMENT_LEFT);
					textPane.addBalloonMessage(balloonMsg);
					textPane.setCaretPosition(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
