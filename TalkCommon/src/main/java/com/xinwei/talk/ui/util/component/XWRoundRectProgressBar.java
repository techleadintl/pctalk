package com.xinwei.talk.ui.util.component;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

/**
 * 圆形进度条。
 * 
 * @version 1.0。
 *
 */
public class XWRoundRectProgressBar extends JPanel {

	private static final long serialVersionUID = 1L;

	private boolean stringPainted;

	/**
	 * 最小进度值。
	 */
	private int minimumProgress;

	/**
	 * 最大进度值。
	 */
	private int maximumProgress;

	/**
	 * 当前进度值。
	 */
	private int progress;

	/**
	 * 背景颜色。
	 */
	private Color backgroundColor;

	/**
	 * 前景颜色。
	 */
	private Color foregroundColor;

	/**
	 * 数字颜色。
	 */
	private Color digitalColor;

	/**
	 * 创建一个圆形进度条对象。
	 */
	public XWRoundRectProgressBar() {
		setMinimum(0);
		setMaximum(100);
		setValue(0);
		setBackgroundColor(new Color(209, 206, 200));
		setForegroundColor(new Color(172, 168, 163));
		setDigitalColor(Color.BLACK);
	}

	/**
	 * 绘制圆形进度条。
	 * 
	 * @param g
	 *            画笔。
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D graphics2d = (Graphics2D) g;
		// 开启抗锯齿
		graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		int x = 0;
		int y = 0;
		Dimension preferredSize = getPreferredSize();
		int width = preferredSize.width;
		int height = preferredSize.height;
		int fontSize = 12;
		graphics2d.setStroke(new BasicStroke(20.0f));
		graphics2d.setColor(backgroundColor);
		graphics2d.fillRoundRect(x, y, width, height, 5, 5);
		graphics2d.setColor(foregroundColor);

		graphics2d.fillRoundRect(x, y, (int) (width * ((progress * 1.0) / (getMaximum() - getMinimum()))), height, 5, 5);

		if (stringPainted) {
			graphics2d.setFont(new Font("黑体", Font.BOLD, fontSize));
			FontMetrics fontMetrics = graphics2d.getFontMetrics();
			int digitalWidth = fontMetrics.stringWidth(progress + "%");
			int digitalAscent = fontMetrics.getAscent();
			graphics2d.setColor(digitalColor);
			graphics2d.drawString(progress + "%", width / 2 - digitalWidth / 2, height / 2 + digitalAscent / 2-2);
		}
	}

	/**
	 * 返回最小进度值。
	 * 
	 * @return 最小进度值。
	 */
	public int getMinimum() {
		return minimumProgress;
	}

	/**
	 * 设置最小进度值。
	 * 
	 * @param minimumProgress
	 *            最小进度值。
	 */
	public void setMinimum(int minimumProgress) {
		if (minimumProgress <= getMaximum()) {
			this.minimumProgress = minimumProgress;
		}
	}

	/**
	 * 返回最大进度值。
	 * 
	 * @return 最大进度值。
	 */
	public int getMaximum() {
		return maximumProgress;
	}

	/**
	 * 设置最大进度值。
	 * 
	 * @param maximumProgress
	 *            最大进度值。
	 */
	public void setMaximum(int maximumProgress) {
		if (maximumProgress >= getMinimum()) {
			this.maximumProgress = maximumProgress;
		}
	}

	/**
	 * 返回当前进度值。
	 * 
	 * @return 当前进度值。
	 */
	public int getValue() {
		return progress;
	}

	/**
	 * 设置当前进度值。
	 * 
	 * @param progress
	 *            当前进度值。
	 */
	public void setValue(int progress) {
		if (progress >= getMinimum() && progress <= getMaximum()) {
			this.progress = progress;
			this.repaint();
		}
	}

	/**
	 * 返回背景颜色。
	 * 
	 * @return 背景颜色。
	 */
	public Color getBackgroundColor() {
		return backgroundColor;
	}

	/**
	 * 设置背景颜色。
	 * 
	 * @param backgroundColor
	 *            背景颜色。
	 */
	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
		this.repaint();
	}

	/**
	 * 返回前景颜色。
	 * 
	 * @return 前景颜色。
	 */
	public Color getForegroundColor() {
		return foregroundColor;
	}

	/**
	 * 设置前景颜色。
	 * 
	 * @param foregroundColor
	 *            前景颜色。
	 */
	public void setForegroundColor(Color foregroundColor) {
		this.foregroundColor = foregroundColor;
		this.repaint();
	}

	/**
	 * 返回数字颜色。
	 * 
	 * @return 数字颜色。
	 */
	public Color getDigitalColor() {
		return digitalColor;
	}

	/**
	 * 设置数字颜色。
	 * 
	 * @param digitalColor
	 *            数字颜色。
	 */
	public void setDigitalColor(Color digitalColor) {
		this.digitalColor = digitalColor;
		this.repaint();
	}

	public boolean isStringPainted() {
		return stringPainted;
	}

	public void setStringPainted(boolean stringPainted) {
		this.stringPainted = stringPainted;
	}

}
