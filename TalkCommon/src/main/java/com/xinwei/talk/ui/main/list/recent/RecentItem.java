/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月16日 下午2:36:30
 * 
 ***************************************************************/
package com.xinwei.talk.ui.main.list.recent;

import java.util.Date;

import javax.swing.JPanel;

import com.xinwei.common.lang.DateUtil;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.model.XWObject;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.main.list.ListItem;

public class RecentItem extends ListItem {
	private static final long serialVersionUID = 1514044406550293152L;

	private TalkMessage talkMessage;

	public RecentItem(XWObject obj) {
		super();
		setObject(obj);
	}

	protected void setInfo(XWObject obj) {
		super.setInfo(obj);
	}

	public TalkMessage getTalkMessage() {
		return talkMessage;
	}

	protected javax.swing.JPanel createUpPanel() {
		JPanel upPanel = new JPanel(null);
		upPanel.add(nameLabel);
		upPanel.add(specialLabel);
		nameLabel.setBounds(0, 3, 130, 23);
		specialLabel.setBounds(133, 3, 80, 23);
		return upPanel;
	};

	public void setTalkMessage(Date today, Date thisYear, TalkMessage message) {
		this.talkMessage = message;
		//清空聊天界面
		descriptionLabel.setText("");
		specialLabel.setText("");

		Date time = message.getTalkBody().getTime();

		if (time.after(today)) {
			specialLabel.setText(DateUtil.getString(time, "HH:mm"));
		} else if (time.after(new Date(today.getTime() - 24 * 3600 * 1000))) {
			specialLabel.setText(Res.getMessage("yesterday"));
		} else if (time.after(thisYear)) {
			specialLabel.setText(DateUtil.getString(time, "MM-dd"));
		} else {
			specialLabel.setText(DateUtil.getString(time, "yyyy"));
		}

		descriptionLabel.setText(MessageUtil.getMessagePrompt(message));
	}

}
