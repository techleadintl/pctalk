/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 下午12:59:28
 * 
 ***************************************************************/
package com.xinwei.talk.ui.main;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import org.jivesoftware.smackx.debugger.EnhancedDebuggerWindow;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillCardPanel;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.component.McWillLine;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.TalkUtil2;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.main.content.ButtonPanel;
import com.xinwei.talk.ui.main.content.WorkspacePane;
import com.xinwei.talk.ui.main.dialog.group.GroupDialog;
import com.xinwei.talk.ui.main.dialog.user.TalkDialog;
import com.xinwei.talk.ui.main.list.group.GroupListPanel;
import com.xinwei.talk.ui.main.list.recent.RecentListPanel;
import com.xinwei.talk.ui.main.list.talk.TalkListPanel;
import com.xinwei.talk.ui.main.search.SearchInputPanel;
import com.xinwei.talk.ui.main.status.StatusPanel;
import com.xinwei.talk.ui.setting.SettingFrame;
import com.xinwei.talk.ui.skin.WindowSkinDialog;

/**
 * The inner Container for Spark. The Workspace is the container for all plugins
 * into the Spark install. Plugins would use this for the following:
 * <p/>
 * <ul>
 * <li>Add own tab to the main tabbed pane. ex.
 * <p/>
 * <p/>
 * Workspace workspace = SparkManager.getWorkspace(); JButton button = new
 * JButton("HELLO SPARK USERS"); workspace.getWorkspacePane().addTab("MyPlugin",
 * button);
 * </p>
 * <p/>
 * <li>Retrieve the ContactList.
 */
public class Workspace extends JPanel implements ActionListener {

	private static final long serialVersionUID = 7076407890063933765L;
	private StatusPanel statusPanel;
	private WorkspacePane workspacePane;
	private ButtonPanel buttonPanel;
	private SearchInputPanel searchInputPanel;

	private McWillIconButton recentButton = new McWillIconButton(LAF.getMainTabRecent());
	private McWillIconButton contactButton = new McWillIconButton(LAF.getMainTabContact());
	private McWillIconButton groupButton = new McWillIconButton(LAF.getMainTabGroup());
	private McWillIconButton settingButton = new McWillIconButton(LAF.getMainTabSetting());
	private McWillIconButton skinButton = new McWillIconButton(LAF.getMainTabSkin());
	private McWillIconButton addUserButton = new McWillIconButton(LAF.getMainTabAddContact());
	private McWillIconButton addGroupButton = new McWillIconButton(LAF.getMainTabAddGroup());
	private McWillIconButton logoutbtn = new McWillIconButton(LAF.getMainTablogout());

	private static Workspace singleton;
	private static final Object LOCK = new Object();

	/**
	 * Returns the singleton instance of <CODE>Workspace</CODE>, creating it if
	 * necessary.
	 * <p/>
	 *
	 * @return the singleton instance of <Code>Workspace</CODE>
	 */
	public static Workspace getInstance() {
		// Synchronize on LOCK to ensure that we don't end up creating
		// two singletons.
		synchronized (LOCK) {
			if (null == singleton) {
				singleton = new Workspace();
			}
		}
		return singleton;
	}

	/**
	 * Creates the instance of the SupportChatWorkspace.
	 */
	private Workspace() {
		// Add MainWindow listener
		workspacePane = new WorkspacePane();

		workspacePane.addCard("contact", new TalkListPanel(), contactButton);
		workspacePane.addCard("group", new GroupListPanel(), groupButton);
		workspacePane.addCard("recent", new RecentListPanel(), recentButton);
		workspacePane.show("contact");

		statusPanel = new StatusPanel();

		searchInputPanel = new SearchInputPanel(workspacePane);

		buttonPanel = new ButtonPanel();

		buttonPanel.add(contactButton);
		buttonPanel.add(groupButton);
		buttonPanel.add(recentButton);
		buttonPanel.add(settingButton);
		buttonPanel.add(skinButton);
		buttonPanel.add(addUserButton);
		buttonPanel.add(addGroupButton);
		buttonPanel.add(logoutbtn);

		recentButton.addActionListener(this);
		contactButton.addActionListener(this);
		groupButton.addActionListener(this);
		settingButton.addActionListener(this);
		skinButton.addActionListener(this);
		addUserButton.addActionListener(this);
		addGroupButton.addActionListener(this);
		logoutbtn.addActionListener(this);

		// Build default workspace
		this.setLayout(new GridBagLayout());

		add(statusPanel, new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		add(searchInputPanel, new GridBagConstraints(0, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 10));
		add(new McWillLine(), new GridBagConstraints(0, 3, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		add(buttonPanel, new GridBagConstraints(0, 9, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
		add(workspacePane, new GridBagConstraints(1, 9, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

		this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F12"), "showDebugger");

		this.getActionMap().put("showDebugger", new AbstractAction("showDebugger") {
			private static final long serialVersionUID = 4066886679016416923L;

			public void actionPerformed(ActionEvent evt) {
				EnhancedDebuggerWindow window = EnhancedDebuggerWindow.getInstance();
				window.setVisible(true);
			}
		});
	}

	/**
	 * Returns the isInputing box for the User.
	 *
	 * @return the isInputing box for the user.
	 */
	public StatusPanel getStatusBar() {
		return statusPanel;
	}

	/**
	 * Returns the Workspace TabbedPane. If you wish to add your component,
	 * simply use addTab( name, icon, component ) call.
	 *
	 * @return the workspace JideTabbedPane
	 */
	public McWillCardPanel getWorkspacePane() {
		return workspacePane;
	}

	@Override
	public synchronized void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == recentButton) {
			workspacePane.show("recent");
		} else if (source == contactButton) {
			workspacePane.show("contact");
		} else if (source == groupButton) {
			workspacePane.show("group");
		} else if (source == settingButton) {
			SettingFrame sf = SettingFrame.getInstance();
			sf.bringFrameIntoFocus();

		} else if (source == skinButton) {
			WindowSkinDialog windowSkinDialog = WindowSkinDialog.getInstance();
			if (windowSkinDialog.isVisible()) {
				windowSkinDialog.showSkinTab();
				return;
			} else {
				MainWindow window = TalkManager.getMainWindow();
				Rectangle bounds = window.getBounds();

				if (bounds.x >= windowSkinDialog.getWidth()) {
					windowSkinDialog.setLocation(bounds.x - windowSkinDialog.getWidth(), (int) (window.getLocation().getY() + skinButton.getLocation().getY() + 10));
				} else {
					int screenWidth = java.awt.Toolkit.getDefaultToolkit().getScreenSize().width;
					if (screenWidth - bounds.x - bounds.width >= windowSkinDialog.getWidth()) {
						windowSkinDialog.setLocation(bounds.x + bounds.width, (int) (window.getLocation().getY() + skinButton.getLocation().getY() + 10));
					} else {
						SwingUtil.centerWindowOnScreen(windowSkinDialog);
					}
				}
			}
			windowSkinDialog.showSkinTab();

		} else if (source == addUserButton) {
			if (talkDialog != null && talkDialog.isVisible())
				return;
			talkDialog = TalkUtil2.getComponent("addtalk");
			if (talkDialog == null)
				return;
			talkDialog.setLocationRelativeTo(null);
			talkDialog.setVisible(true);
			talkDialog.toFront();
			talkDialog.requestFocus();
		} else if (source == addGroupButton) {
			if (groupDialog != null && groupDialog.isVisible())
				return;
			groupDialog = TalkUtil2.getComponent("addgroup");
			if (groupDialog == null)
				return;
			groupDialog.setLocationRelativeTo(null);
			groupDialog.setVisible(true);
			groupDialog.toFront();
			groupDialog.requestFocus();
		}else if (source == logoutbtn) {
			final MainWindow mainWindow = TalkManager.getMainWindow();
			mainWindow.logout(true);
		}
	}

	JDialog talkDialog;
	JDialog groupDialog;

}
