package com.xinwei.talk.ui.main.dialog.user;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.util.PainterUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.model.Talk;

public class SearchResultPanel extends JPanel {
	private JLabel imageLabel;
	private JLabel displayNameLabel;
	private JLabel descriptionLabel;

	public SearchResultPanel() {

		setOpaque(true);

		setLayout(new GridBagLayout());

		setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

		createUI();
	}

	private void createUI() {
		imageLabel = new JLabel();

		displayNameLabel = new JLabel();
		displayNameLabel.setFont(LAF.getFont(14));
		descriptionLabel = new JLabel();
		descriptionLabel.setFont(LAF.getFont(12));

		displayNameLabel.setPreferredSize(new Dimension(200, 30));
		descriptionLabel.setPreferredSize(new Dimension(200, 30));
		imageLabel.setPreferredSize(new Dimension(65, 65));
		descriptionLabel.setForeground(LAF.getColor(150, 150, 150));

		add(imageLabel, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		add(displayNameLabel, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
		add(descriptionLabel, new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));

		//		displayNameLabel.setBorder(BorderFactory.createEtchedBorder());
		//		descriptionLabel.setBorder(BorderFactory.createEtchedBorder());

	}

	public void sertResult(final Talk user) {
		imageLabel.setVisible(true);
		if (StringUtil.isNotBlank(user.getNick())) {
			displayNameLabel.setText(user.getNick());
		} else {
			displayNameLabel.setText(TalkUtil.getUserTel(user));
		}
		descriptionLabel.setText(user.getSignature());

		try {
			ImageIcon imageIcon = null;
			if (user.getAvatar() == null || user.getAvatar().length == 0) {
				imageIcon = LAF.getDefaultAvatarIcon();
			} else {
				imageIcon = new ImageIcon(user.getAvatar());
			}
			Image image = imageIcon.getImage().getScaledInstance(65, 65, Image.SCALE_SMOOTH);
			imageIcon = new ImageIcon(image);
			imageLabel.setIcon(imageIcon);
		} catch (Exception e) {
			e.printStackTrace();
			Log.error(e);
		}
		imageLabel.repaint();
	}

	public void sertResult(final String user) {
		imageLabel.setVisible(false);
		displayNameLabel.setText(user);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		JComponent component = this;
		PainterUtil.paintBackgroudColor(g, McWillTheme.getThemeColor().getSubLightColor(), 0, 0, component.getWidth(), component.getHeight());

	}

}