/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年2月28日 下午1:25:25
 * 
 ***************************************************************/
package com.xinwei.talk.ui.setting.detail;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.component.McWillRadioGroup;
import com.xinwei.common.lookandfeel.layout.WrapFlowLayout;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.setting.SettingDetailPanel;
import com.xinwei.talk.ui.setting.SettingFrame;
import com.xinwei.talk.ui.setting.SettingSubPanel;
import com.xinwei.talk.ui.util.component.XWConfirmDialog;

public class LanguagePanel extends SettingDetailPanel {
	McWillRadioGroup<String> languageRadioGroup;

	protected void addSubPanels() {
		add(new SettingSubPanel(Res.getMessage("setting.language.title")) {
			@Override
			protected void createContentPanel(JPanel contentPanel) {
				languageRadioGroup = new McWillRadioGroup<String>();
				languageRadioGroup.addRadio(contentPanel, Res.getMessage("setting.language.cn"), false, Locale.SIMPLIFIED_CHINESE.getLanguage() + "_" + Locale.SIMPLIFIED_CHINESE.getCountry());
				languageRadioGroup.addRadio(contentPanel, Res.getMessage("setting.language.en"), true, Locale.US.getLanguage() + "_" + Locale.US.getCountry());

				contentPanel.setPreferredSize(new Dimension(0, 1 * (18 + 10) + 25));
				contentPanel.setLayout(new WrapFlowLayout(50, 10));
				
				contentPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 0, 0));
			}
		});
	}

	protected void initValues() {
		languageRadioGroup.setSelected(Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry());
	}

	protected void addListeners() {
		ActionListener al = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//语言设置
				if (e.getSource() == languageRadioGroup) {
					String selected = languageRadioGroup.getSelected();
					if (TalkManager.getConfigManager().getLanguage() == selected) {
						return;
					}
					TalkManager.getConfigManager().setLanguage(selected, null);

					CloseConfirmDialog dialog = new CloseConfirmDialog();
					dialog.setVisible(true);
				}
			}

		};
		//语言设置
		languageRadioGroup.addActionListener(al);
	}

	public class CloseConfirmDialog extends XWConfirmDialog {
		private static final long serialVersionUID = 8447361099667316655L;

		public CloseConfirmDialog() {
			super(SettingFrame.getInstance(), Res.getMessage("setting.language.dialog.title"), Res.getMessage("setting.language.dialog.msg"));
			setModal(false);
			addWindowFocusListener(new WindowFocusListener() {

				@Override
				public void windowLostFocus(WindowEvent e) {
					CloseConfirmDialog.this.setVisible(false);
				}

				@Override
				public void windowGainedFocus(WindowEvent e) {
				}
			});
		}

		protected JButton createButton1() {
			JButton button = new JButton(Res.getMessage("button.ok"));
			button.setPreferredSize(new Dimension(100, 24));
			button.addActionListener(this);
			return button;
		}

		protected JButton createButton2() {
			return null;
		}
	}
}
