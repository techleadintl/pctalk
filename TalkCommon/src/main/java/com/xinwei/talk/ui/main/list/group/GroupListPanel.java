package com.xinwei.talk.ui.main.list.group;

import java.util.List;

import javax.swing.JPopupMenu;

import com.xinwei.talk.common.TalkUtil2;
import com.xinwei.talk.listener.MemberListener;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.ui.main.list.ListPanel;

public class GroupListPanel extends ListPanel<Group> {
	private static final long serialVersionUID = -8486385306563904232L;

	@Override
	protected void addPopuMenuItemList(JPopupMenu popup, Group group) {
		TalkUtil2.addPopunActions(popup, "group", group);

	}

	public void addListeners() {
		super.addListeners();

		ListenerManager.addGroupListener(new MyObjectListener());

		ListenerManager.addMemberListener(new MyMemberListener());

	}

	class MyMemberListener implements MemberListener {
		@Override
		public void add(final String gid, final List<Member> members) {
		}

		@Override
		public void update(final String gid, final List<Member> members) {

		}

		@Override
		public void delete(final String gid, final List<Member> members) {
		}
	}

}
