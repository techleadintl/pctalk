package com.xinwei.talk.ui.main.dialog.group;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;

import com.xinwei.common.lang.PinyinUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillFilterList;
import com.xinwei.common.lookandfeel.component.McWillFilterList.FilterModel;
import com.xinwei.common.lookandfeel.component.McWillFilterList.MatchComparator;
import com.xinwei.common.lookandfeel.component.McWillScrollPane;
import com.xinwei.common.lookandfeel.component.text.McWillSearchTextField;
import com.xinwei.common.lookandfeel.component.text.McWillTextInputPanel;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.ui.util.component.list.TalkListItemRenderer;

@SuppressWarnings("serial")
public class ListPanel extends JPanel {
	private static final long serialVersionUID = 3575273326105549609L;

	//好友List
	private McWillFilterList<Talk> leftList;
	private FilterModel leftListModel;
	private FilterModel rightListModel;
	//已选List
	private McWillFilterList<Talk> rightList;

	//增加按钮
	private JButton addButton;
	//删除按钮
	private JButton deleteButton;

	private McWillTextInputPanel leftTextField;

	private String HOLD_TEXT = Res.getMessage("input.text.filter.telandname");

	public ListPanel() {
		this.setLayout(new GridBagLayout());

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridBagLayout());
		buttonPanel.setBackground((Color) UIManager.get("ContactItem.background"));
		buttonPanel.setAlignmentY(Component.TOP_ALIGNMENT);

		addButton = SwingUtil.newButton();
		addButton.setText(Res.getMessage("button.group.add"));

		deleteButton = SwingUtil.newButton();
		deleteButton.setText(Res.getMessage("button.group.delete"));

		GridBagConstraints c = new GridBagConstraints();
		//		c.ipadx = 20;
		//		c.ipady = 10;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(15, 0, 15, 0);
		buttonPanel.add(addButton, c);
		c.gridx = 1;
		c.gridy = 4;
		buttonPanel.add(deleteButton, c);

		//左侧
		leftTextField = new McWillSearchTextField(HOLD_TEXT, LAF.getTextSearchIcon(), LAF.getDeleteIcon()[0]) {
			@Override
			public void doSearch(String text) {
				doFilter(leftList, text);
			}
		};
		leftTextField.setPreferredSize(new Dimension(0, 27));

		leftList = new McWillFilterList<Talk>();
		leftList.setMatchComparator(new MyMatchComparator());
		leftListModel = leftList.getFilterModel();
		leftList.setCellRenderer(new TalkListItemRenderer());
		SwingUtil.setOpaqueFalse(leftList);

		McWillScrollPane leftScrollPane = new McWillScrollPane(leftList);
		leftScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		JPanel leftPanel = new JPanel(new BorderLayout(5, 5));

		leftPanel.add(leftTextField, BorderLayout.NORTH);
		leftPanel.add(leftScrollPane, BorderLayout.CENTER);

		//右侧
		JLabel rightLabel = new JLabel(Res.getMessage("label.group.selected.member"));
		rightLabel.setPreferredSize(new Dimension(0, 27));

		rightList = new McWillFilterList<Talk>();
		rightList.setMatchComparator(new MyMatchComparator() {
			@Override
			public int compare(Talk o1, Talk o2) {
				return 0;
			}
		});
		rightListModel = rightList.getFilterModel();
		rightList.setCellRenderer(new TalkListItemRenderer());
		SwingUtil.setOpaqueFalse(rightList);
		
		McWillScrollPane rightScrollPane = new McWillScrollPane(rightList);
		rightScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		JPanel rightPanel = new JPanel(new BorderLayout(5, 5));

		rightPanel.add(rightLabel, BorderLayout.NORTH);
		rightPanel.add(rightScrollPane, BorderLayout.CENTER);

		//
		add(leftPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 10, 0, 10), 0, 0));

		add(buttonPanel, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 10, 0, 10), 0, 0));

		add(rightPanel, new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 10, 0, 10), 0, 0));

		buildList();

		addListeners();

	}

	private void addListeners() {
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				doAdd();
			}
		});

		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				doDelete();
			}
		});

		leftList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) { //When double click JList  
					doAdd(); //Event  
				}
			}
		});

		rightList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) { //When double click JList  
					doDelete(); //Event  
				}
			}
		});
	}

	private void doFilter(McWillFilterList<Talk> list, String text) {
		if (text.trim().equals(HOLD_TEXT)) {
			return;
		}
		list.doFilter(text.trim());
	}

	private void doDelete() {
		if (rightList.getSelectedIndex() != -1) {
			leftListModel.addElement(rightList.getSelectedValue());

			int i = rightList.getSelectedIndex();
			rightListModel.remove(i);
			rightList.setSelectedIndex(i > 0 ? i - 1 : 0);
		}
	}

	private void doAdd() {
		if (leftList.getSelectedIndex() != -1) {
			Talk selectedValue = leftList.getSelectedValue();

			int i = leftList.getSelectedIndex();
			leftListModel.remove(i);
			leftList.setSelectedIndex(i > 0 ? i - 1 : 0);

			rightListModel.addElement(selectedValue);
			rightList.setSelectedValue(selectedValue, true);
		}
	}

	private void buildList() {
		leftListModel.clear();

		List<Talk> camTalks = new ArrayList<Talk>(TalkManager.getLocalManager().getTalkList());

		for (int i = 0; i < camTalks.size(); i++) {

			Talk camTalk = camTalks.get(i);

			try {
				leftListModel.add(i, camTalk);
			} catch (Exception e) {
				Log.error(e);
			}
		}
	}

	public Enumeration<Talk> getSelectedContacts() {
		return rightListModel.elements();
	}

	class MyMatchComparator implements MatchComparator<Talk> {
		@Override
		public int compare(Talk o1, Talk o2) {
			return TalkUtil.getDisplayName(o1.getJid()).compareTo(TalkUtil.getDisplayName(o2.getJid()));
		}

		@Override
		public boolean isMatch(Talk camtalk, String text) {
			if (text == null)
				return true;
			String tel = TalkUtil.getUserTel(camtalk.getJid());
			String displayName = TalkUtil.getDisplayName(camtalk.getJid());
			return PinyinUtil.isMatch(text, displayName, tel);
		}
	};
}
