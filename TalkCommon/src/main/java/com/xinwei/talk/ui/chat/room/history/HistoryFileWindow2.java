package com.xinwei.talk.ui.chat.room.history;
///*************************************************************
// * * Copyright (c) 2016-2050  信威通信
// *
// * All Rights Reserved
// * 
// * This is the proprietary source code of XinWei company product
// *
// * @author liyong
// *
// * 2016年10月23日 下午3:17:53
// * 
// ***************************************************************/
//package com.xinwei.talk.ui.chat.room.history;
//
//import java.awt.BorderLayout;
//import java.awt.CardLayout;
//import java.awt.Dimension;
//import java.awt.FlowLayout;
//import java.awt.GridBagConstraints;
//import java.awt.GridBagLayout;
//import java.awt.GridLayout;
//import java.awt.Insets;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.io.File;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//import javax.swing.JScrollPane;
//import javax.swing.JTable;
//
//import com.xinwei.talk.common.LAF;
//import com.xinwei.talk.common.Res;
//import com.xinwei.talk.manager.TalkManager;
//import com.xinwei.talk.model.message.TranscriptMessage;
//import com.xinwei.talk.model.message.chat.ChatMessage;
//import com.xinwei.talk.model.message.chat.FileMsg;
//import com.xinwei.talk.ui.chat.room.ChatRoom;
//import com.xinwei.common.lang.DateUtil;
//import com.xinwei.common.lang.SwingUtil;
//import com.xinwei.common.lookandfeel.component.McWillLinkButton;
//import com.xinwei.common.lookandfeel.component.McWillEditList;
//import com.xinwei.common.lookandfeel.component.McWillListCell;
//import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
//import com.xinwei.common.lookandfeel.util.Constants;
//import com.xinwei.common.vo.Entry;
//import com.xinwei.spark.ui.MessageDialog;
//
//@SuppressWarnings("serial")
//public class HistoryFileWindow2 extends JPanel {
//
//	private ChatRoom room;
//
//	public HistoryFileWindow2(ChatRoom room) {
//		this.room = room;
//
//		setOpaque(true);
//
//		setLayout(new BorderLayout(0, 0));
//
//		Map<String, List<FileMsg>> fileMsgMap = null;
//		try {
//			fileMsgMap = getFileMsgMap();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		JScrollPane panel = createPanel(fileMsgMap);
//
//		add(panel, BorderLayout.CENTER);
//
//	}
//
//	public Map<String, List<FileMsg>> getFileMsgMap() throws Exception {
//		List<TranscriptMessage> transcriptMessageList = TalkManager.getHistoryManager().getTranscriptMessageList(room.getRoomId());
//		if (transcriptMessageList == null) {
//			return null;
//		}
//
//		Map<String, List<FileMsg>> fileMap = new HashMap<>();
//
//		for (TranscriptMessage message : transcriptMessageList) {
//			Date time = message.getTime();
//			String timeStr = DateUtil.format(time, DateUtil.PATTERN_DEFAULT);
//
//			ChatMessage chatMessage = message.getChatMessage();
//
//			List<FileMsg> fileList = chatMessage.getFileList();
//			if (fileList == null) {
//				continue;
//			}
//			for (FileMsg msg : fileList) {
//				int fileType = msg.getFileType();
//				if (fileType == FileMsg.SEND_COMPLETE || fileType == FileMsg.RECEIVE_COMPLETE) {
//					List<FileMsg> list = fileMap.get(timeStr);
//					if (list == null) {
//						list = new ArrayList<>();
//						fileMap.put(timeStr, list);
//					}
//					list.add(msg);
//				}
//			}
//		}
//		return fileMap;
//	}
//
//	public JScrollPane createPanel(Map<String, List<FileMsg>> fileMap) {
//		McWillEditList<Entry<String, FileMsg>> fileListUI = new McWillEditList<Entry<String, FileMsg>>(FileMsgListCell.class);
//		fileListUI.setRowHeight(70);
//		if (fileMap != null) {
//			ArrayList<String> timeList = new ArrayList<String>(fileMap.keySet());
//
//			Collections.sort(timeList);
//
//			for (final String timeStr : timeList) {
//				List<FileMsg> fileList = fileMap.get(timeStr);
//				if (fileList == null) {
//					continue;
//				}
//
//				for (FileMsg fileMsg : fileList) {
//					fileListUI.addElement(new Entry<String, FileMsg>(timeStr, fileMsg));
//				}
//			}
//		}
//		return fileListUI;
//	}
//
//	public static class FileMsgListCell extends McWillListCell<Entry<String, FileMsg>> {
//		McWillRolloverLabel avatarIconLabel;
//
//		JLabel fileNameLabel;
//		JLabel timeLabel;
//
//		McWillLinkButton openLabel;
//		McWillLinkButton openDirLabel;
//		McWillLinkButton deleteLabel;
//
//		JPanel buttonPanel;
//		CardLayout cardLayout;
//
//		File file;
//
//		@Override
//		protected void createUI() {
//			setOpaque(true);
//			setLayout(new GridBagLayout());
//
//			avatarIconLabel = new McWillRolloverLabel();
//			avatarIconLabel.setPreferredSize(new Dimension(50, 50));
//
//			fileNameLabel = new JLabel();
//			fileNameLabel.setHorizontalTextPosition(JLabel.LEFT);
//			fileNameLabel.setHorizontalAlignment(JLabel.LEFT);
//
//			timeLabel = new JLabel();
//			timeLabel.setHorizontalTextPosition(JLabel.LEFT);
//			timeLabel.setHorizontalAlignment(JLabel.LEFT);
//
//			openLabel = new McWillLinkButton(Res.getMessage("button.open"));
//			openDirLabel = new McWillLinkButton(Res.getMessage("button.opendir"));
//			deleteLabel = new McWillLinkButton(Res.getMessage("button.delete"));
//
//			JLabel infoLabel = new JLabel("发送成功");
//			infoLabel.setHorizontalTextPosition(JLabel.RIGHT);
//			infoLabel.setHorizontalAlignment(JLabel.RIGHT);
//
//			JPanel jPanel = new JPanel(new GridLayout(2, 1));
//			jPanel.add(fileNameLabel);
//			jPanel.add(timeLabel);
//
//			JPanel jp = new JPanel(new FlowLayout(FlowLayout.TRAILING));
//			jp.add(openLabel);
//			jp.add(openDirLabel);
//			jp.add(deleteLabel);
//
//			buttonPanel = new JPanel();
//			buttonPanel.setOpaque(false);
//			cardLayout = new CardLayout(); // 创建卡片布局的对象
//			buttonPanel.setLayout(cardLayout);// 重新设置面板的布局 为卡片布局
//			buttonPanel.add(jp, "two"); // 必须指定 标识符 ，如果没有标识符		
//			buttonPanel.add(infoLabel, "one"); // 必须指定 标识符 ，如果没有标识符
//
//			add(avatarIconLabel, new GridBagConstraints(0, 0, 1, 1, 0.0D, 0.0D, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
//			add(jPanel, new GridBagConstraints(1, 0, 1, 1, 0.0D, 1.0D, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 0), 0, 0));
//			add(buttonPanel, new GridBagConstraints(2, 0, 1, 1, 1.0D, 1.0D, GridBagConstraints.EAST, GridBagConstraints.VERTICAL, new Insets(5, 0, 5, 0), 0, 0));
//
//			addListeners();
//
//		}
//
//		@Override
//		protected void updateData(JTable table, Entry<String, FileMsg> isIn, boolean isSelected, int row, int column) {
//			if (this.object == isIn) {
//				return;
//			}
//			FileMsg value = isIn.getValue();
//			file = new File(isIn.getValue().getFileAbsolutePath());
//
//			super.updateData(table, isIn, isSelected, row, column);
//
//			fileNameLabel.setText(value.getFilename());
//
//			timeLabel.setText(isIn.getKey());
//
//			avatarIconLabel.setIcon(LAF.getImageIcon(SwingUtil.getFileImageIcon(value.getFilename()), 50, 50));
//
//		}
//
//		private void addListeners() {
//			SwingUtil.addHandCurosrListener(avatarIconLabel, fileNameLabel, openLabel, openDirLabel, deleteLabel);
//
//			openLabel.putClientProperty(Constants.CLICK, true);
//
//			avatarIconLabel.addMouseListener(new MouseAdapter() {
//				public void mouseClicked(MouseEvent e) {
//					if (e.getClickCount() == 2) {
//						SwingUtil.openFile(file);
//					}
//				}
//			});
//			fileNameLabel.addMouseListener(new MouseAdapter() {
//				public void mouseClicked(MouseEvent e) {
//					if (e.getClickCount() == 2) {
//						SwingUtil.openFile(file);
//					}
//				}
//			});
//			openLabel.addActionListener(new ActionListener() {
//				@Override
//				public void actionPerformed(ActionEvent e) {
//					boolean exists = file.exists();
//					if (!exists) {
//						PromptMessageDialog.showAlert(TalkManager.getChatFrame(), Res.getMessage("message.file.exists.not", file.getAbsolutePath()));
//						return;
//					}
//					String errorMsg = SwingUtil.openFile(file);
//					if (errorMsg != null) {
//						PromptMessageDialog.showAlert(TalkManager.getChatFrame(), errorMsg, 400, 300);
//					}
//				}
//			});
//
//			openDirLabel.addActionListener(new ActionListener() {
//				public void actionPerformed(ActionEvent e) {
//					boolean exists = file.getParentFile().exists();
//					if (!exists) {
//						PromptMessageDialog.showAlert(TalkManager.getChatFrame(), Res.getMessage("message.file.exists.not", file.getParentFile()));
//						return;
//					}
//					SwingUtil.openFile(file.getParentFile());
//				}
//			});
//
//			openDirLabel.addActionListener(new ActionListener() {
//				public void actionPerformed(ActionEvent e) {
//					// TODO Auto-generated method stub
//
//				}
//			});
//		}
//
//		@Override
//		protected void onSelected(JTable table, Object value, boolean isSelected, int row, int column) {
//			//			cardLayout.show(buttonPanel, "two"); // 显示指定的内容
//			//			System.out.println("onSelected--row:" + row + ",col:" + column);
//		}
//
//		@Override
//		protected void onHovered(JTable table, Object value, boolean isSelected, int row, int column) {
//			cardLayout.show(buttonPanel, "two"); // 显示指定的内容
//		}
//
//		@Override
//		protected void onDefault(JTable table, Object value, boolean isSelected, int row, int column) {
//			cardLayout.show(buttonPanel, "one"); // 显示指定的内容
//		}
//	}
//}
