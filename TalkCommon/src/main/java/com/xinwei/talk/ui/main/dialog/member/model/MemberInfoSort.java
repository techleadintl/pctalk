package com.xinwei.talk.ui.main.dialog.member.model;

import java.util.Comparator;

//排序  
public class MemberInfoSort implements Comparator<MemberInfo> {
	@Override
	public int compare(MemberInfo o1, MemberInfo o2) {
		return o1.userNameWithTone.compareTo(o2.userNameWithTone);
	}
}