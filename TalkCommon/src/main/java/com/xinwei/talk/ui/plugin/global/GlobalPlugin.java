/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月26日 下午12:52:17
 * 
 ***************************************************************/
package com.xinwei.talk.ui.plugin.global;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smack.packet.XMPPError;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.dispatcher.SwingDispatchService;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputListener;
import org.jnativehook.mouse.NativeMouseWheelEvent;
import org.jnativehook.mouse.NativeMouseWheelListener;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.common.lookandfeel.McWillImageIcon;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.component.McWillImageDialog;
import com.xinwei.common.lookandfeel.listener.McWillWindowAdapter;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.common.ui.flash.FlashWindow;
import com.xinwei.common.vo.Entry;
import com.xinwei.spark.TaskEngine;
import com.xinwei.spark.ui.MessageDialog;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.listener.AvatarListener;
import com.xinwei.talk.listener.ChatRoomAdapter;
import com.xinwei.talk.listener.ChatRoomFileDropListener;
import com.xinwei.talk.listener.MainWindowListener;
import com.xinwei.talk.listener.MemberListener;
import com.xinwei.talk.listener.MessageListener;
import com.xinwei.talk.listener.ObjectListener;
import com.xinwei.talk.listener.UnreadListener;
import com.xinwei.talk.listener.VCardListener;
import com.xinwei.talk.manager.ChatManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.manager.TransferManager;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.XWObject;
import com.xinwei.talk.model.avatar.Avatar;
import com.xinwei.talk.model.avatar.TalkAvatar;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.chat.ChatContainer;
import com.xinwei.talk.ui.chat.ChatFrame;
import com.xinwei.talk.ui.chat.ChatInputEditor;
import com.xinwei.talk.ui.chat.room.ChatRoom;
import com.xinwei.talk.ui.chat.room.ChatRoomWorking;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.chat.room.file.ChatRoomFileTab;
import com.xinwei.talk.ui.chat.room.history.ChatMessageTabbedPane;
import com.xinwei.talk.ui.chat.room.history.ChatRoomTabbedPane;
import com.xinwei.talk.ui.chat.voice.CaptureVoice;
import com.xinwei.talk.ui.login.OfflineDialog;
import com.xinwei.talk.ui.main.dialog.member.MemberAddDialog;
import com.xinwei.talk.ui.main.dialog.user.TalkInfoDialog;
import com.xinwei.talk.ui.main.status.MyInfoDialog;
import com.xinwei.talk.ui.plugin.Plugin;
import com.xinwei.talk.ui.plugin.emoticon.EmoticonUI;
import com.xinwei.talk.ui.plugin.emoticon.EmoticonUI.EmoticonPickListener;
import com.xinwei.talk.ui.plugin.global.font.FontAndColorUI;
import com.xinwei.talk.ui.plugin.global.memberlist.GroupMemberListPanel;
import com.xinwei.talk.ui.plugin.global.prompt.PromptMessageDialog;
import com.xinwei.talk.ui.plugin.global.screen.XScreenShot;
import com.xinwei.talk.ui.util.component.tab.Tab;
import com.xinwei.talk.ui.util.component.tab.TabbedPaneAdapter;
import com.xinwei.xmpp.smack.XWXMPPConnection;

public class GlobalPlugin implements Plugin {
	final FlashWindow flashWindow = new FlashWindow();

	public void initialize() {
		TalkManager.getMainWindow().addWindowListener(new WindowAdapter() {
			public void windowClosed(WindowEvent e) {
				// Clean up the native hook.
				try {
					GlobalScreen.unregisterNativeHook();
				} catch (NativeHookException ex) {
					ex.printStackTrace();
				}
				System.runFinalization();
				System.exit(0);
			}
			@Override
			public void windowOpened(WindowEvent e) {
				super.windowOpened(e);
			}
		});
		TalkManager.getMainWindow().addMainWindowListener(new MainWindowListener() {
			@Override
			public void shutdown() {
				ChatContainer chatContainer = TalkManager.getChatManager().getChatContainer();
				if (chatContainer.getChatRooms().length != 0) {
					chatContainer.closeAllChatRoom(true);
				}
				TalkManager.getMainWindow().dispose();
			}

			@Override
			public void mainWindowDeactivated() {

			}

			@Override
			public void mainWindowActivated() {

			}
		});
		// 注册应用程序全局事件, 所有的事件都会被此事件监听器处理.  
		Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
		logger.setLevel(Level.OFF);
		logger.setUseParentHandlers(false);
		//		logger.setLevel(Level.ALL);
		GlobalScreen.setEventDispatcher(new SwingDispatchService());
		try {
			GlobalScreen.registerNativeHook();
		} catch (NativeHookException ex) {
			System.err.println("There was a problem registering the native hook.");
			System.err.println(ex.getMessage());
		}
		MyNativeListener nativeListener = new MyNativeListener();
		GlobalScreen.addNativeKeyListener(nativeListener);// 全局键盘事件 
		//		GlobalScreen.addNativeMouseListener(nativeListener);// 全局鼠标事件 
		//		GlobalScreen.addNativeMouseMotionListener(nativeListener);// 全局鼠标移动事件 
		//		GlobalScreen.addNativeMouseWheelListener(nativeListener);// 全局鼠标移动事件

		TalkManager.getConnection().addConnectionListener(new MyConnectionListener());
		{
			//隐藏窗口
			//		ScreenWindowHide screenWindowHide = new ScreenWindowHide(TalkManager.getMainWindow());
			//		TalkManager.setScreenWindowHide(screenWindowHide);
			//		screenWindowHide.start();
			//		TalkManager.getMainWindow().addWindowListener(new WindowAdapter() {
			//			Point loc = TalkManager.getMainWindow().getLocation();
			//
			//			public void windowActivated(WindowEvent e) {
			//				loc = e.getWindow().getLocation();
			//			}
			//
			//			@Override
			//			public void windowIconified(WindowEvent e) {
			//				loc = e.getWindow().getLocation();
			//			}
			//
			//			@Override
			//			public void windowDeiconified(WindowEvent e) {
			//				e.getWindow().setLocation(loc);
			//			}
			//
			//			@Override
			//			public void windowDeactivated(WindowEvent e) {
			//				loc = e.getWindow().getLocation();
			//			}
			//		});
		}
		ListenerManager.addAvatarListener(new MyAvatarListener());
		ListenerManager.addVCardListener(new MyVCardListener());

		/** 接收消息监听器 ，接收消息开启 闪动任务栏 **/
		ListenerManager.addMessageListener(new MyMessageListener());
		/** 接收消息监听器 ，接收消息播放声音 **/
		ListenerManager.addMessageListener(new MyMessageSoundListener());

		/** 聊天窗口监听器,1：聊天窗口打开关闭闪动任务栏 ；2：聊天窗口关闭，关闭所有聊天室 **/
		ListenerManager.addChatWindowListener(new MyChatFrameListener());

		/** 聊天窗口未讀 监听器 接收消息时，打开提示窗口 **/
		ListenerManager.addUnreadListener(new MyUnreadListener());

		/** 聊天窗口文件拖拽 监听器 **/
		ListenerManager.addFileDropListener(new MyChatRoomFileDropListener());

		/** 组删除时关闭聊天窗口 **/
		/** 如果有新组添加时，向服务器发送加入组消息 **/
		ListenerManager.addGroupListener(new MyGroupListener());

		ListenerManager.addChatRoomListener(new ChatRoomAdapter() {
			public void chatRoomOpened(final ChatRoom talkRoom) {
				JButton button = null;
				if (!(talkRoom instanceof CommonChatRoom))
					return;
				final CommonChatRoom room = (CommonChatRoom) talkRoom;

				/** tab关闭打开，进行调整ChatFrame大小 **/
				final ChatRoomTabbedPane tabbedPane = room.getTabbedPane();
				tabbedPane.addTabbedPaneListener(new ChatRoomTabbedPaneListner(room));

				/** 聊天窗口输入框按键监听器 **/
				room.getChatInputEditor().addKeyListener(new ChatInputKeyListener(room));

				/** 聊天窗口文字字体颜色 **/
				button = new McWillIconButton(LAF.getRoomFontImageIcon(), true);
				button.setToolTipText(Res.getMessage("button.chatroom.textfontcolor"));
				button.addActionListener(new TextFontColorActionListener(room));
				room.addChatInputToolBarLeftComponent(button);

				/** 聊天窗口表情 **/
				button = new McWillIconButton(LAF.getRoomEmoticonImageIcon(), true);
				button.setToolTipText(Res.getMessage("button.chatroom.emoticon"));
				button.addActionListener(new EmoticonActionListener(room));
				room.addChatInputToolBarLeftComponent(button);

				/** 聊天窗口文件上传 **/
				button = new McWillIconButton(LAF.getRoomFileImageIcon(), true);
				button.setToolTipText(Res.getMessage("button.chatroom.fileupload"));
				button.addActionListener(new FileUploadActionListener(room));
				room.addChatInputToolBarLeftComponent(button);

				/** 聊天窗口发送图片 **/
				button = new McWillIconButton(LAF.getRoomPictureImageIcon(), true);
				button.setToolTipText(Res.getMessage("button.chatroom.chat.picture"));
				button.addActionListener(new PictureActionListener(room));
				room.addChatInputToolBarLeftComponent(button);

				/** 聊天窗口截屏 **/
				button = new McWillIconButton(LAF.getRoomScreenshotImageIcon(), true);
				button.setToolTipText(Res.getMessage("button.chatroom.screenshot"));
				button.addActionListener(new ScreenShotActionListener(room));
				room.addChatInputToolBarLeftComponent(button);

				/** 聊天窗口短语音 **/
				button = new McWillIconButton(LAF.getRoomShortVoiceImageIcon(), true);
				button.setToolTipText(Res.getMessage("button.chatroom.chat.shortvoice"));
				button.addActionListener(new ShortVoiceActionListener(room, LAF.getRoomShortVoiceImageIcon()[0], LAF.getRoomShortVoiceAnimateImageIcon()));
				room.addChatInputToolBarLeftComponent(button);

				//				/** 聊天窗口传输文件测试 **/
				//				button = new McWillIconButton(LAF.getRoomShortVoiceImageIcon(), true);
				//				button.setToolTipText("传输文件测试");
				//				button.addActionListener(new ActionListener() {
				//
				//					@Override
				//					public void actionPerformed(ActionEvent e) {
				//						try {
				//							sendFile(room.getRoomId()+"/"+TalkUtil.getJidResource(), new File("d://注册信息.txt"), TalkManager.getConnection());
				//						} catch (Exception e1) {
				//							e1.printStackTrace();
				//						}
				//					}
				//
				//					public void sendFile(String user, File file, XMPPConnection connection) throws Exception {
				//						FileTransferManager manager = new FileTransferManager(connection);
				//						OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer(user);
				//						long timeOut = 1000000;
				//						long sleepMin = 3000;
				//						long spTime = 0;
				//						int rs = 0;
				//
				//						transfer.sendFile(file, "pls re file!");
				//						rs = transfer.getStatus().compareTo(FileTransfer.Status.complete);
				//						while (rs != 0) {
				//							rs = transfer.getStatus().compareTo(FileTransfer.Status.complete);
				//							spTime = spTime + sleepMin;
				//							if (spTime > timeOut) {
				//								return;
				//							}
				//							Thread.sleep(sleepMin);
				//						}
				//					}
				//				});
				room.addChatInputToolBarLeftComponent(button);
				/** 聊天窗口消息记录 **/
				button = new McWillIconButton(LAF.getRoomHistoryImageIcon(), true);
				button.setToolTipText(Res.getMessage("button.chatroom.chat.history"));
				button.addActionListener(new ChatMessageActionListner(room));
				room.addChatInputToolBarRightComponent(button);

				/** 组窗口 加入成员列表显示 **/
				if (room.getChatType() == Type.groupchat) {
					//邀请好友
					button = new McWillIconButton(LAF.getMemberInviteImageIcon(), true);
					button.setToolTipText(Res.getMessage("button.chatroom.chat.member.invite"));
					button.addActionListener(new MemberInviteActionListner(room));
					room.addChatRoomButton(button);

					//成员列表
					button = new McWillIconButton(LAF.getMemberListImageIcon(), true);
					button.setToolTipText(Res.getMessage("button.chatroom.chat.member"));
					MemberListActionListner l = new MemberListActionListner(room);
					button.addActionListener(l);
					room.addChatRoomButton(button);
					l.actionPerformed(null);
				} else {
					String roomId = room.getRoomId();
					Talk talk = TalkManager.getLocalManager().getTalkByJid(roomId);
					if (talk == null) {
						//邀请好友
						button = new McWillIconButton(LAF.getMemberInviteImageIcon(), true);
						button.setToolTipText(Res.getMessage("button.chatroom.chat.member.invite"));
						button.addActionListener(new TalkInviteActionListner(room));
						room.addChatRoomButton(button);
					}
				}
			}
		});

		/** 聊天窗口关闭时或者file tab关闭 **/
		ListenerManager.addChatRoomListener(new ChatRoomAdapter() {
			@Override
			public boolean canChatRoomClosed(ChatRoom room) {
				//				return room.canClose();
				return true;
			}

			@Override
			public void chatRoomClosed(ChatRoom room) {
				close(room);
			}

			@Override
			public void chatRoomFileTabClosed(ChatRoom room) {
				close(room);
			}

			private void close(ChatRoom chatRoom) {
				//关闭 tab
				if (chatRoom instanceof CommonChatRoom) {
					((CommonChatRoom) chatRoom).closeTabbedPane();
				}

				//关闭聊天窗口
				TalkManager.getChatManager().getChatContainer().chatRoomClosed(chatRoom);
			}
		});

	}

	public void shutdown() {
	}

	public boolean canShutDown() {
		return true;
	}

	public void uninstall() {
	}

	class ChatInputKeyListener extends KeyAdapter {
		CommonChatRoom chatRoom;

		public ChatInputKeyListener(CommonChatRoom chatRoom) {
			this.chatRoom = chatRoom;
		}

		@Override
		public void keyPressed(KeyEvent e) {
			String defaultSendKey = TalkManager.getUserConfig().getString("Default-Send-Key");

			ChatInputEditor chatInputEditor = chatRoom.getChatInputEditor();
			final KeyStroke keyStroke = KeyStroke.getKeyStroke(e.getKeyCode(), e.getModifiers());
			if (keyStroke.equals(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.ALT_DOWN_MASK))) {//Enter+ALT
				final Document document = chatInputEditor.getDocument();
				try {
					document.insertString(chatInputEditor.getCaretPosition(), "\n", null);
					chatInputEditor.requestFocusInWindow();
					//					room.getSendButton().setEnabled(true);
				} catch (BadLocationException badLoc) {
					Log.error("Error when checking for enter:", badLoc);
				}
			} else if (keyStroke.equals(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.CTRL_DOWN_MASK))) {//Enter+CTL
				if (CommonChatRoom.DEFAULT_SEND_KEY_CTRL_ENTER.equalsIgnoreCase(defaultSendKey)) {
					e.consume();
					chatRoom.sendMessage();
					chatInputEditor.setText("");
					chatInputEditor.setCaretPosition(0);

					//				SparkManager.getWorkspace().getTranscriptPlugin().persistChatRoom(this);
				}
			} else if (e.getKeyChar() == KeyEvent.VK_ENTER && !keyStroke.equals(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.SHIFT_DOWN_MASK))) {
				if (CommonChatRoom.DEFAULT_SEND_KEY_ENTER.equalsIgnoreCase(defaultSendKey)) {
					e.consume();
					chatRoom.sendMessage();
					chatInputEditor.setText("");
					chatInputEditor.setCaretPosition(0);

					//				SparkManager.getWorkspace().getTranscriptPlugin().persistChatRoom(this);
				}
			} else if (keyStroke.equals(KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_MASK))) {//CTL+V
				Clipboard clb = Toolkit.getDefaultToolkit().getSystemClipboard();
				Transferable contents = clb.getContents(e.getSource());
				if (contents == null)
					return;
				try {
					if (contents.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {//文件
						List<File> files = (List<File>) contents.getTransferData(DataFlavor.javaFileListFlavor);
						for (File file : files) {
							if (file.isFile()) {
								TalkManager.getTransferManager().sendFile(file, chatRoom.getRoomId());
							}
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	abstract class ChatRoomActionListener implements ActionListener {

		protected CommonChatRoom chatRoom;

		public ChatRoomActionListener(CommonChatRoom chatRoom) {
			this.chatRoom = chatRoom;
		}
	}

	class TextFontColorActionListener extends ChatRoomActionListener {
		// Show popup
		JPanel top;

		public TextFontColorActionListener(final CommonChatRoom chatRoom) {
			super(chatRoom);

			FontAndColorUI ui = new FontAndColorUI(chatRoom.getChatInputEditor());
			top = chatRoom.getChatInputToolBar().getTop();
			top.add(ui);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			top.setVisible(!top.isVisible());
		}
	}

	class EmoticonActionListener extends ChatRoomActionListener {
		// Show popup
		final JPopupMenu popup = new JPopupMenu();

		public EmoticonActionListener(final CommonChatRoom chatRoom) {
			super(chatRoom);
			EmoticonUI emoticonUI = new EmoticonUI();
			emoticonUI.setEmoticonPickListener(new EmoticonPickListener() {
				public void emoticonPicked(McWillImageIcon emoticon) {
					try {
						popup.setVisible(false);
						chatRoom.getChatInputEditor().insertEmoticon(emoticon);
						chatRoom.getChatInputEditor().requestFocus();
					} catch (Exception e1) {
						Log.error(e1);
					}

				}
			});

			popup.add(emoticonUI);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (popup.getHeight() == 0) {
				popup.setVisible(true);
			}
			popup.show(chatRoom.getChatInputToolBar(), 0, -popup.getHeight());
		}
	}

	class FileUploadActionListener extends ChatRoomActionListener {

		public FileUploadActionListener(CommonChatRoom chatRoom) {
			super(chatRoom);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			ChatManager chatManager = TalkManager.getChatManager();
			TransferManager transferManager = TalkManager.getTransferManager();
			FileDialog fileChooser = new FileDialog(chatManager.getChatFrame(), Res.getMessage("button.chatroom.fileupload"), FileDialog.LOAD);
			fileChooser.setVisible(true);

			if (fileChooser.getDirectory() == null || fileChooser.getFile() == null) {
				return;
			}

			File file = new File(fileChooser.getDirectory(), fileChooser.getFile());

			if (file.exists()) {
				transferManager.sendFile(file, chatRoom.getRoomId());
			}
		}
	}

	public class MemberListActionListner extends ChatRoomAdapter implements ActionListener, MemberListener {

		protected CommonChatRoom chatRoom;
		protected GroupMemberListPanel listPanel;

		public MemberListActionListner(CommonChatRoom chatRoom) {
			this.chatRoom = chatRoom;

			//窗口打开是
			listPanel = new GroupMemberListPanel(chatRoom);

			//添加成员监听器
			ListenerManager.addMemberListener(this);
			ListenerManager.addChatRoomListener(this);
		}

		public void chatRoomClosed(CommonChatRoom room) {
			if (room != chatRoom) {//如果不是该房间
				return;
			}
			//删除成员监听器
			ListenerManager.removeMemberListener(this);
			ListenerManager.removeChatRoomListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (chatRoom == null)
				return;

			ChatRoomTabbedPane tabbedPane = chatRoom.getTabbedPane();

			String tabTitle = Res.getMessage("button.chatroom.chat.member");
			Tab tab = tabbedPane.getTab(tabTitle);

			if (tab == null) {
				tabbedPane.addTab(tabTitle, null, listPanel);

			} else {
				tabbedPane.close(tab);
			}
		}

		@Override
		public void add(String gid, List<Member> members) {
			String groupId = TalkUtil.getGroupId(chatRoom.getRoomId());
			if (!StringUtil.equals(gid, groupId))
				return;

			listPanel.add(members);
		}

		@Override
		public void update(String gid, List<Member> members) {
			String groupId = TalkUtil.getGroupId(chatRoom.getRoomId());
			if (!StringUtil.equals(gid, groupId))
				return;

			listPanel.update(members);
		}

		@Override
		public void delete(String gid, List<Member> members) {
			String groupId = TalkUtil.getGroupId(chatRoom.getRoomId());
			if (!StringUtil.equals(gid, groupId))
				return;

			listPanel.delete(members);
		}
	}

	public class MemberInviteActionListner extends ChatRoomActionListener {

		public MemberInviteActionListner(CommonChatRoom chatRoom) {
			super(chatRoom);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			ChatRoomTabbedPane tabbedPane = chatRoom.getTabbedPane();

			ChatFrame chatFrame = TalkManager.getChatManager().getChatContainer().getChatFrame();
			new MemberAddDialog(chatFrame, TalkManager.getLocalManager().getGroupByJid(chatRoom.getRoomId())).setVisible(true);
		}

	}

	public class TalkInviteActionListner extends ChatRoomActionListener {

		public TalkInviteActionListner(CommonChatRoom chatRoom) {
			super(chatRoom);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			final String jid = chatRoom.getRoomId();
			Talk talk = TalkManager.getLocalManager().getTalkByJid(jid);
			if (talk != null) {//已经存在
				return;
			}
			String loginJid = TalkManager.getVCard().getJid();
			if (StringUtil.equals(jid, loginJid)) {//自己不邀请自己
				return;
			}

			TaskEngine.getInstance().submit(new Runnable() {
				@Override
				public void run() {
					try {
						String phone = TalkUtil.getUserTel(jid);
						Talk talk = TalkManager.getTalkService().getTalk(phone);
						if (talk == null) {
							return;
						}
						try {
							TalkManager.getTalkService().addTalk(talk, null);
							final List<Talk> list = new ArrayList<Talk>();
							list.add(talk);
							TalkManager.getVCardManager().addTalk(list);
						} catch (Exception e) {//如果上传失败，则删除
							Log.error(e);
						}

					} catch (Exception e1) {
					}
				}
			});
		}

	}

	class ChatMessageActionListner extends ChatRoomActionListener {
		public ChatMessageActionListner(CommonChatRoom chatRoom) {
			super(chatRoom);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			ChatRoomTabbedPane tabbedPane = chatRoom.getTabbedPane();

			String tabTitle = Res.getMessage("button.chatroom.chat.history");
			Tab tab = tabbedPane.getTab(tabTitle);

			if (tab == null) {
				ChatMessageTabbedPane chatMessageTabbedPane = new ChatMessageTabbedPane(chatRoom);
				tabbedPane.addTab(tabTitle, null, chatMessageTabbedPane);
			} else {
				tabbedPane.close(tab);
			}
		}
	}

	class ScreenShotActionListener extends ChatRoomActionListener {

		public ScreenShotActionListener(CommonChatRoom chatRoom) {
			super(chatRoom);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JButton button = (JButton) e.getSource();

			button.setEnabled(false);

			final ChatFrame chatFrame = TalkManager.getChatManager().getChatFrame();
			new XScreenShot() {
				@Override
				public void doFinish(BufferedImage captureImage) {
					if (captureImage != null) {
						//						TalkManager.getTransferManager().sendImage(captureImage, chatRoom);
						chatRoom.getChatInputEditor().insertBufferedImage(captureImage);
						captureImage = null;
					}
				}
			}.doCapture(chatFrame);

			button.setEnabled(true);
		}
	}

	class ShortVoiceActionListener extends ChatRoomActionListener {
		private boolean stoping = false;
		Icon icon;
		Icon[] icons;

		ChatRoomWorking working = null;
		CaptureVoice captureVoice = null;

		public ShortVoiceActionListener(final CommonChatRoom chatRoom, Icon icon, Icon[] icons) {
			super(chatRoom);
			this.icon = icon;
			this.icons = icons;

		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (stoping) {
				return;
			}
			final JButton button = (JButton) e.getSource();
			if (captureVoice == null) {

				captureVoice = createCaptureVoice();

				working = new ChatRoomWorking(chatRoom) {
					@Override
					public boolean canClose() {
						return captureVoice == null;
					}
				};
				working.setMessage(Res.getMessage("chatroom.working.shortvoice.message"));
				chatRoom.addChatRoomWorking(working);

				try {
					captureVoice.doCapture();
				} catch (Exception ex) {
					Log.error(ex);
				}
				SystemUtil.execute(new Runnable() {
					public void run() {
						int i = 0;
						while (captureVoice != null) {
							try {
								button.setIcon(icons[i++ % 14]);
								SystemUtil.sleep(100);
								if (i > 100000) {
									i = 0;
								}
							} catch (Exception ex) {
								ex.printStackTrace();
							}
						}
						button.setIcon(icon);
					}
				});
			} else {
				stoping = true;
				if (captureVoice != null) {
					captureVoice.doStop();
				}
				chatRoom.removeChatRoomWorking(working);
			}

		}

		protected CaptureVoice createCaptureVoice() {
			return new CaptureVoice() {
				public void doCapture() {
					try {
						super.doCapture();
					} catch (Exception e) {
						Log.error(e);
						doFinish(null);
					}
				}

				public void doFinish(byte[] wavBytes) {
					if (wavBytes != null) {
						//					byte[] amr = VoiceManager.wavToAmr(wav);
						//						chatRoom.insertVoice(wavBytes);
						chatRoom.getChatInputEditor().insertVoice(wavBytes);
					}
					stoping = false;
					captureVoice = null;
				}
			};
		}
	}

	class PictureActionListener extends ChatRoomActionListener {

		public PictureActionListener(CommonChatRoom chatRoom) {
			super(chatRoom);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			ChatManager chatManager = TalkManager.getChatManager();
			JFileChooser fileChooser = new McWillImageDialog(chatManager.getChatFrame()) {
				@Override
				protected String getDescriptionPrefix() {
					return Res.getMessage("button.chatroom.chat.picture.filter");
				}
			};//创建文件选择器
			fileChooser.setDialogTitle(Res.getMessage("button.chatroom.chat.picture"));
			int i = fileChooser.showOpenDialog(chatManager.getChatFrame());
			String error = null;
			if (i != JFileChooser.APPROVE_OPTION)
				return;
			//判断是否为打开的按钮
			File selectedFile = fileChooser.getSelectedFile(); //取得选中的文件
			if (!selectedFile.exists()) {
				error = Res.getMessage("message.file.exists.not", selectedFile.getAbsolutePath());
				MessageDialog.showAlert(TalkManager.getChatFrame(), error);
				return;
			}
			//图片太大了
			if (selectedFile.length() > 20 * 1024 * 1024) {
				MessageDialog.showAlert(TalkManager.getChatFrame(), Res.getMessage("message.file.too.large", SwingUtil.format(20 * 1024 * 1024)));
				return;
			}
			String imageFormat = ImageUtil.getImageFormat(selectedFile);
			System.out.println("> Cootel-Engine : Image format - "+imageFormat);
			//不支持的图片格式
			if (imageFormat == null) {
				MessageDialog.showAlert(TalkManager.getChatFrame(), Res.getMessage("message.file.unsupported.image.format"));
				return;
			}
			chatRoom.getChatInputEditor().insertImageFile(selectedFile);

			//			TaskEngine.getInstance().submit(new Runnable() {
			//				public void run() {
			//					chatRoom.setCursor(new Cursor(Cursor.WAIT_CURSOR));
			//					try {
			//						chatRoom.sendImage(readBytes, imageFormat);
			//					} catch (Exception e) {
			//						MessageDialog.showAlert(TalkManager.getChatFrame(), SystemUtil.getStackTrace(e));
			//					}
			//					chatRoom.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			//				}
			//			});

		}
	}

	List<Component> comList = new ArrayList<Component>();

	class ChatRoomTabbedPaneListner extends TabbedPaneAdapter {
		CommonChatRoom chatRoom;

		public ChatRoomTabbedPaneListner(CommonChatRoom chatRoom) {
			this.chatRoom = chatRoom;
		}

		@Override
		public void tabAdded(Tab tab, Component component, int index) {
			int change = getChangeW(component);
			if (change > 0) {
				ChatFrame chatFrame = TalkManager.getChatFrame();
				Dimension size = chatFrame.getSize();
				chatFrame.setSize(new Dimension(size.width + change, size.height));
			}
			comList.add(component);
			chatRoom.getHorizontalRightPanel().setVisible(true);
		}

		@Override
		public void tabRemoved(Tab tab, Component component, int index) {
			comList.remove(component);
			final int change = getChangeW(component);
			final ChatFrame chatFrame = TalkManager.getChatFrame();
			final Dimension size = chatFrame.getSize();
			if (change > 0) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						chatFrame.setSize(new Dimension(size.width - change, size.height));
					}
				});
			}
			if (chatRoom.getTabbedPane().getTabCount() == 0) {
				chatRoom.getHorizontalRightPanel().setVisible(false);
			}
		}

		public int getChangeW(final Component tabComponent) {
			int max = 0;
			for (Component com : comList) {
				int w = getComponentW(com);
				max = Math.max(max, w);
			}
			int tabComponentW = getComponentW(tabComponent);
			return tabComponentW - max;
		}

		public int getComponentW(final Component component) {
			int w = 0;
			if (component instanceof GroupMemberListPanel) {
				w = 200;
			} else if (component instanceof ChatMessageTabbedPane) {
				w = 400;
			} else if (component instanceof ChatRoomFileTab) {
				w = 250;
			}
			return w;
		}
	}

	class MyChatRoomFileDropListener implements ChatRoomFileDropListener {
		@Override
		public void filesDropped(Collection<File> files, Component component) {
			if (component instanceof CommonChatRoom) {
				CommonChatRoom roomImpl = (CommonChatRoom) component;

				for (File file : files) {
					TalkManager.getTransferManager().sendFile(file, roomImpl.getRoomId());
				}

				TalkManager.getChatManager().getChatContainer().activateChatRoom(roomImpl);
			}
		}
	}

	class MyUnreadListener implements UnreadListener {
		PromptMessageDialog promptMessageDialog = new PromptMessageDialog();

		@Override
		public void doUnreadMessage(String jid, Entry<Integer, TalkMessage> unreadMessage) {
			//提示抛出窗口
			if (unreadMessage != null) {
				promptMessageDialog.updateData(jid, unreadMessage.getValue());
				promptMessageDialog.setV(true);
			}

			//聊天窗口
			TalkManager.getChatManager().getChatContainer().updateList();
		}
	}

	class MyGroupListener implements ObjectListener {
		@Override
		public void delete(Collection<? extends XWObject> objects) {
			for (XWObject obj : objects) {

				ChatContainer chatContainer = TalkManager.getChatManager().getChatContainer();
				ChatRoom talkRoom = chatContainer.getChatRoom(obj.getJid());
				if (talkRoom != null) {
					talkRoom.close(true);
				}
			}
		}

		@Override
		public void add(Collection<? extends XWObject> objects) {
			for (final XWObject group : objects) {
				TaskEngine.getInstance().submit(new Runnable() {
					public void run() {
						try {
							TalkManager.getXmppService().joinGroup(group.getJid());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		}

		@Override
		public void update(Collection<? extends XWObject> objects) {

		}
	}

	class MyMessageListener implements MessageListener {
		@Override
		public void messageSent(String to, TalkMessage message) {
		}

		@Override
		public void messageReceived(String from, TalkMessage message) {
			if (SwingUtil.isWindows()) {
				//不带资源的用户JID
				String userJid = TalkUtil.getBareJid(from);
				ChatContainer chatContainer = TalkManager.getChatManager().getChatContainer();
				ChatRoom chatRoom = chatContainer.getChatRoom(userJid);
				if (chatRoom == null)
					return;
				boolean isActive = chatContainer.isActiveChatRoom(chatRoom);
				if (isActive) {//活动房间
					ChatFrame chatFrame = TalkManager.getChatFrame();
					if (!chatFrame.isActive()) {
						flashWindow.startFlashing(chatFrame);
					}
				}
			}
		}
	}

	class MyMessageSoundListener implements MessageListener {
		@Override
		public void messageSent(String to, TalkMessage message) {
		}

		@Override
		public void messageReceived(String from, TalkMessage message) {
			if (!TalkManager.getUserConfig().getMsgSound()) {//没有开启声音提示
				return;
			}
			TalkManager.getSoundManager().playClip(LAF.getURL("talk/sounds/incoming.wav"));
		}
	}

	class MyChatFrameListener extends McWillWindowAdapter {
		@Override
		public void windowActivated(WindowEvent e) {
			if (SwingUtil.isWindows()) {
				flashWindow.stopFlashing(e.getWindow());
			}
		}

		@Override
		public boolean preWindowClosing() {
			return TalkManager.getChatManager().getChatContainer().closeAllChatRoom(false);
		}

	}

	class MyAvatarListener implements AvatarListener {
		@Override
		public void update(List<Avatar> avatars) {
			if (avatars == null)
				return;
			for (Avatar avatar : avatars) {
				if (avatar instanceof TalkAvatar) {
					String key = "TalkInfo_" + ((TalkAvatar) avatar).getUserTel();
					Object object = TalkManager.getLocalManager().get(key);
					if (object instanceof TalkInfoDialog) {
						TalkInfoDialog dialog = ((TalkInfoDialog) object);
						dialog.updateAvatar();
					}
				}
			}
		}
	}

	class MyVCardListener implements VCardListener {
		@Override
		public void vcardChanged(VCard vcard) {
			String key = "MyTalkInfo_" + vcard.getTel();
			Object object = TalkManager.getLocalManager().get(key);
			if (object instanceof MyInfoDialog) {
				MyInfoDialog dialog = ((MyInfoDialog) object);
				dialog.updateAvatar();
			}
		}
	}

	class MyConnectionListener implements ConnectionListener {
		OfflineDialog offlineFrame = new OfflineDialog();

		@Override
		public void connectionClosed() {
		}

		@Override
		public void connectionClosedOnError(Exception exception) {
			String message = null;
			XMPPError xmppError = null;
			if (exception instanceof XMPPException) {
				XMPPException xmppException = (XMPPException) exception;
				xmppError = xmppException.getXMPPError();
			}
			if (xmppError != null) {
				message = xmppError.getMessage();
				org.jivesoftware.smack.packet.XMPPError.Type type = xmppError.getType();
			}
			if ("Replaced by new connection".equals(message)) {
				XWXMPPConnection connection = TalkManager.getConnection();
				ConnectionConfiguration configuration = connection.getConfiguration();
				configuration.setReconnectionAllowed(false);
				offlineFrame.setVisible(true);

			}
			//			TalkManager.getConnection().connect();

		}

		@Override
		public void reconnectingIn(int e) {
		}

		@Override
		public void reconnectionSuccessful() {
			offlineFrame.setVisible(false);
		}

		@Override
		public void reconnectionFailed(Exception e) {
		}
	}

	class MyNativeListener implements NativeKeyListener, NativeMouseInputListener, NativeMouseWheelListener {
		Shortcut shortcut = new Shortcut();

		public void nativeKeyPressed(NativeKeyEvent e) {
			//			if (e.getKeyCode() == NativeKeyEvent.VC_ESCAPE) {
			//				try {
			//					GlobalScreen.unregisterNativeHook();
			//				} catch (NativeHookException e1) {
			//					e1.printStackTrace();
			//				}
			//			}
			//			e.getModifiers()
			String v = getKeyValue(e);

			//			NativeKeyEvent.getModifiersText(modifiers)

			if (v == null || v.length() == 0)
				return;
			String shortKeyMethod = TalkManager.getUserConfig().getShortKeyMethod(v);
			if (shortKeyMethod != null) {
				try {
					Method method = Shortcut.class.getMethod(shortKeyMethod);
					method.invoke(shortcut);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}

		List<Integer> chars = Arrays.asList(new Integer[] { NativeKeyEvent.VC_A, NativeKeyEvent.VC_B, NativeKeyEvent.VC_C, NativeKeyEvent.VC_D, NativeKeyEvent.VC_E, NativeKeyEvent.VC_F, NativeKeyEvent.VC_G, NativeKeyEvent.VC_H, NativeKeyEvent.VC_I,
				NativeKeyEvent.VC_J, NativeKeyEvent.VC_K, NativeKeyEvent.VC_L, NativeKeyEvent.VC_M, NativeKeyEvent.VC_N, NativeKeyEvent.VC_O, NativeKeyEvent.VC_P, NativeKeyEvent.VC_Q, NativeKeyEvent.VC_R, NativeKeyEvent.VC_S, NativeKeyEvent.VC_T,
				NativeKeyEvent.VC_U, NativeKeyEvent.VC_V, NativeKeyEvent.VC_W, NativeKeyEvent.VC_X, NativeKeyEvent.VC_Y, NativeKeyEvent.VC_Z });
		List<Integer> fs = Arrays.asList(new Integer[] { NativeKeyEvent.VC_F1, NativeKeyEvent.VC_F2, NativeKeyEvent.VC_F3, NativeKeyEvent.VC_F4, NativeKeyEvent.VC_F5, NativeKeyEvent.VC_F6, NativeKeyEvent.VC_F7, NativeKeyEvent.VC_F8,
				NativeKeyEvent.VC_F9, NativeKeyEvent.VC_F10, NativeKeyEvent.VC_F11, NativeKeyEvent.VC_F10, NativeKeyEvent.VC_F12, NativeKeyEvent.VC_F13, NativeKeyEvent.VC_F14, NativeKeyEvent.VC_F15, NativeKeyEvent.VC_F16, NativeKeyEvent.VC_F17,
				NativeKeyEvent.VC_F18, NativeKeyEvent.VC_F19 });
		List<Integer> nums = Arrays.asList(
				new Integer[] { NativeKeyEvent.VC_1, NativeKeyEvent.VC_2, NativeKeyEvent.VC_3, NativeKeyEvent.VC_4, NativeKeyEvent.VC_5, NativeKeyEvent.VC_6, NativeKeyEvent.VC_7, NativeKeyEvent.VC_8, NativeKeyEvent.VC_9, NativeKeyEvent.VC_0 });

		protected String getKeyValue(NativeKeyEvent e) {
			int modifiers = e.getModifiers();

			String v = "";
			if ((modifiers & NativeKeyEvent.VC_CONTROL) != 0) {
				v += "Ctrl + ";
			}

			if ((modifiers & NativeKeyEvent.VC_ALT) != 0) {
				v += "Alt + ";
			}

			if ((modifiers & NativeKeyEvent.VC_SHIFT) != 0) {
				v += "Shift + ";
			}
			if (v.length() == 0) {
				return "";
			}
			int keyCode = e.getKeyCode();
			if (chars.contains(keyCode) || fs.contains(keyCode) || nums.contains(keyCode)) {
				v += NativeKeyEvent.getKeyText(keyCode).toUpperCase();
			} else {
				v = "";
			}
			return v;
		}

		public void nativeKeyReleased(NativeKeyEvent e) {

		}

		public void nativeKeyTyped(NativeKeyEvent e) {

		}

		public void nativeMouseClicked(NativeMouseEvent e) {
			System.out.println("Mouse Clicked: " + e.getClickCount());
		}

		public void nativeMousePressed(NativeMouseEvent e) {
			System.out.println("Mouse Pressed: " + e.getButton());
		}

		public void nativeMouseReleased(NativeMouseEvent e) {
			System.out.println("Mouse Released: " + e.getButton());
		}

		public void nativeMouseMoved(NativeMouseEvent e) {
			System.out.println("Mouse Moved: " + e.getX() + ", " + e.getY());
		}

		public void nativeMouseDragged(NativeMouseEvent e) {
			System.out.println("Mouse Dragged: " + e.getX() + ", " + e.getY());
		}

		@Override
		public void nativeMouseWheelMoved(NativeMouseWheelEvent e) {
			System.out.println("Mosue Wheel Moved: " + e.getWheelRotation());
		}
	}

	public class Shortcut {
		public void Screenshot() {
			final ChatFrame chatFrame = TalkManager.getChatFrame();
			new XScreenShot() {
				@Override
				public void doFinish(BufferedImage captureImage) {
					if (captureImage != null && chatFrame != null) {
						ChatRoom chatRoom = TalkManager.getChatManager().getChatContainer().getActiveChatRoom();
						if (chatRoom instanceof CommonChatRoom) {
							((CommonChatRoom) chatRoom).getChatInputEditor().insertBufferedImage(captureImage);
						}
						captureImage = null;
					}
				}
			}.doCapture(chatFrame != null && chatFrame.isVisible() ? chatFrame : null);
		}

		public void MainWindow() {
			TalkManager.getScreenWindowHide().setTray();
			com.xinwei.talk.ui.main.MainWindow mainWindow = TalkManager.getMainWindow();
			if (!mainWindow.isVisible()) {
				mainWindow.setVisible(true);
				mainWindow.bringFrameIntoFocus();
			} else {
				mainWindow.setVisible(false);
			}
		}
	}
}
