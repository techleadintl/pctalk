/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月22日 下午7:16:15
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.component;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.util.ClipboardUtil;

public class ImageComponent extends McWillRolloverLabel {
	private byte[] data;
	private String imageName;

	protected ImageIcon holdIcon;

	private MouseListener mouseListener;

	public ImageComponent() {
	}

	public ImageComponent(ImageIcon icon, String fileName) {
		this(icon, null, fileName);
	}

	public ImageComponent(ImageIcon holdIcon, byte[] data, String fileName) {
		setLabelIcon(holdIcon, data);
		setOpaque(true);
		this.imageName = fileName;
	}

	public void setLabelIcon(ImageIcon holdIcon, byte[] data) {
		this.holdIcon = holdIcon;
		this.data = data;

		if (data != null) {
			addPopupMenu(this);
		}
		if (holdIcon == null)
			return;
		if (holdIcon.getIconHeight() > 150 || holdIcon.getIconWidth() > 150) {
			boolean h = holdIcon.getIconHeight() > holdIcon.getIconWidth();
			int H = 0;
			int W = 0;
			if (h) {
				W = holdIcon.getIconWidth() * 150 / holdIcon.getIconHeight();
				H = 150;
			} else {
				W = 150;
				H = holdIcon.getIconHeight() * 150 / holdIcon.getIconWidth();
			}
			setIcon(new ImageIcon(ImageUtil.resize(data, W, H)));
		} else {
			setIcon(holdIcon);
		}

	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public void setHoldIcon(ImageIcon icon) {
		this.holdIcon = icon;
	}

	public ImageIcon getHoldIcon() {
		return this.holdIcon;
	}

	private void addPopupMenu(final JLabel label) {
		if (mouseListener != null) {
			return;
		}

		final JPopupMenu popupMenu = new JPopupMenu();
		addMenus(popupMenu);
		mouseListener = new MouseAdapter() {
			ImagePreview imagePreview;

			@Override
			public void mouseClicked(MouseEvent e) {
				if (holdIcon == null) {
					return;
				}
				if (imagePreview == null || !imagePreview.isVisible()) {
					imagePreview = new ImagePreview(TalkManager.getChatFrame(), ImageUtil.getBufferedImage(holdIcon));
					imagePreview.setVisible();
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger() && holdIcon != null) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		};
		label.addMouseListener(mouseListener);
	}

	protected void addMenus(final JPopupMenu popupMenu) {
		popupMenu.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.gray), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
		JMenuItem copyMenu = new JMenuItem(Res.getMessage("action.copy"), LAF.getCopyImageIcon());
		copyMenu.setMnemonic('C');
		copyMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ClipboardUtil.writeToClipboard(holdIcon.getImage());
			}
		});
		JMenuItem saveMenu = new JMenuItem(Res.getMessage("action.save"), LAF.getSaveAsImageIcon());
		saveMenu.setMnemonic('S');
		saveMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ClipboardUtil.writeToFile(holdIcon.getImage());
			}
		});

		popupMenu.add(copyMenu);
		popupMenu.add(saveMenu);
	}
}
