/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月7日 上午9:49:33
 * 
 ***************************************************************/
package com.xinwei.talk.ui.plugin.global.prompt;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.TalkMessage;

public class PromptMessageDialog extends JDialog {
	int DIALOG_WIDTH = 290;

	PromptMsgPanel panel;

	private VisibleWatcher visibleWatcher;

	public PromptMessageDialog() {
		setLayout(new BorderLayout());
		setUndecorated(true);//去除窗体
		setAlwaysOnTop(true); //设置界面悬浮

		JRootPane rootPane = getRootPane();
		((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

		setLocationRelativeTo(new JFrame());

		setResizable(false);

		JPanel contentPane = createDialogContainer();
		setContentPane(contentPane);

		panel = new PromptMsgPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));

		panel.setOpaque(false);

		contentPane.add(panel, BorderLayout.CENTER);

		setSize(DIALOG_WIDTH, 75);

		setVisible(true);
		setV(false);
	}

	/**
	 * 解决对话框弹出时，抢夺界面光标问题
	 */
	public void setV(boolean v) {
		Toolkit defaultToolkit = java.awt.Toolkit.getDefaultToolkit();
		Dimension screenSize = defaultToolkit.getScreenSize();
		int screenWidth = screenSize.width;
		int screenHeight = screenSize.height;
		if (v) {
			/** 获取屏幕的边界 */
			Insets screenInsets = defaultToolkit.getScreenInsets(this.getGraphicsConfiguration());

			setLocation(screenWidth - DIALOG_WIDTH - 5, screenHeight - screenInsets.bottom - 80 - 75);
		} else {
			setLocation(-screenWidth, -screenHeight);
		}
	}

	public void setH() {
	}

	protected JPanel createDialogContainer() {
		return new McWillContentPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				LAF.paintTrayDialog(this, g);
			}
		};
	}

	public void updateData(String jid, TalkMessage message) {
		if (visibleWatcher != null) {
			visibleWatcher.setStop(true);
		}
		visibleWatcher = new VisibleWatcher();

		TaskEngine.getInstance().submit(visibleWatcher);

		panel.updateUIData(jid, message);
	}

	public class PromptMsgPanel extends JPanel {

		private JLabel imageLabel;
		private JLabel displayNameLabel;
		private JLabel descriptionLabel1;
		private JLabel descriptionLabel2;
		private McWillIconButton removeButton;

		private String jid;

		public PromptMsgPanel() {

			setOpaque(true);

			setLayout(new GridBagLayout());

			setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

			createUI();

			addListeners();
		}

		private void addListeners() {
			removeButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					PromptMessageDialog.this.setV(false);
				}
			});
			MouseListener ml = new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					PromptMessageDialog.this.setV(false);
					TalkManager.getChatManager().activateChatRoom(jid);
				}
			};
			addMouseListener(ml);
			imageLabel.addMouseListener(ml);
			displayNameLabel.addMouseListener(ml);
			descriptionLabel1.addMouseListener(ml);
			descriptionLabel2.addMouseListener(ml);
		}

		private void createUI() {
			imageLabel = new McWillRolloverLabel("", true);

			displayNameLabel = new JLabel();
			displayNameLabel.setFont(LAF.getFont(14));
			descriptionLabel1 = new JLabel();
			descriptionLabel1.setFont(LAF.getFont(12));
			descriptionLabel1.setForeground(LAF.getColor(150, 150, 150));

			descriptionLabel2 = new JLabel();
			descriptionLabel2.setFont(LAF.getFont(12));
			descriptionLabel2.setForeground(LAF.getColor(150, 150, 150));

			removeButton = new McWillIconButton(LAF.getDeleteIcon());

			add(imageLabel, new GridBagConstraints(0, 0, 1, 3, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
			add(displayNameLabel, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
			add(removeButton, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
			add(descriptionLabel1, new GridBagConstraints(1, 1, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 0, 0), 0, 0));
			add(descriptionLabel2, new GridBagConstraints(1, 2, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 5, 0, 0), 0, 0));

			//						descriptionLabel1.setBorder(BorderFactory.createEtchedBorder());
			//						displayNameLabel.setBorder(BorderFactory.createEtchedBorder());
			//						removeButton.setBorder(BorderFactory.createEtchedBorder());
			//						descriptionLabel2.setBorder(BorderFactory.createEtchedBorder());
		}

		public void updateUIData(String jid, TalkMessage message) {
			this.jid = jid;
			if (message.getIsGroup()) {
				displayNameLabel.setText(TalkUtil.getDisplayName(jid));
				imageLabel.setIcon(new ImageIcon(ImageManager.getGroupAvatarImage(jid, 65, 65, true)));
			} else {
				displayNameLabel.setText(TalkUtil.getDisplayName(jid));
				imageLabel.setIcon(ImageManager.getTalkAvatar(jid, 60, 60));
			}

			//清空聊天界面
			descriptionLabel1.setText("");
			descriptionLabel2.setText("");

			String longString = MessageUtil.getMessagePrompt(message);
			if (longString != null && longString.length() > 0) {
				char[] chars = longString.toCharArray();
				FontMetrics fontMetrics = descriptionLabel1.getFontMetrics(descriptionLabel1.getFont());
				if (fontMetrics.charsWidth(chars, 0, chars.length) < 200) {
					descriptionLabel2.setText(longString);
				} else {
					int i = 1;
					while (true) {
						if (fontMetrics.charsWidth(chars, 0, i) > 200 || i >= chars.length) {
							break;
						}
						i++;
					}
					descriptionLabel1.setText(longString.substring(0, i));
					descriptionLabel2.setText(longString.substring(i));
				}
			} 
		}

		protected Color DEFAULT_COLOR = new Color(240, 240, 240);

		protected Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 11);

	}

	class VisibleWatcher implements Runnable {
		private boolean stop;

		private long time;

		public void setStop(boolean stop) {
			this.stop = stop;
		}

		@Override
		public void run() {
			while (!stop && time < 4000) {
				SystemUtil.sleep(100);
				time += 100;
			}
			if (!stop) {
				setV(false);
			}
		}

	}
}
