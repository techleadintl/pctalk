package com.xinwei.talk.ui.util.component;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
/**
 */
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.talk.common.LAF;

@SuppressWarnings("serial")
public class XWFrameTitle extends JPanel implements MouseListener {
	private JButton min, close;
	private JLabel titleLabel;
	private JFrame frame;

	public XWFrameTitle(JFrame frame) {
		this.frame = frame;

		Dimension loginBackgroundSize = LAF.getLoginBackgroundSize();

		titleLabel = new JLabel();

		this.setLayout(new BorderLayout());
		this.setOpaque(false);

		Dimension iconifyIconSize = LAF.getDialogIconifyIconSize();

		//关闭按钮
		close = new McWillIconButton(LAF.getCloseIcons());
		close.setBounds(loginBackgroundSize.width - iconifyIconSize.width - 15, 5, iconifyIconSize.width, iconifyIconSize.height);
		close.addMouseListener(this);

		//缩小按钮
		min = new McWillIconButton(LAF.getIconifyIcons());
		min.setBounds(loginBackgroundSize.width - 2 * iconifyIconSize.width - 30, 5, iconifyIconSize.width, iconifyIconSize.height);
		min.addMouseListener(this);

		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 15, 5));
		buttonPanel.add(min);
		buttonPanel.add(close);

		this.add(titleLabel, BorderLayout.WEST);
		this.add(buttonPanel, BorderLayout.EAST);

	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getSource() == min) {
			frame.setExtendedState(JFrame.ICONIFIED);
		}
		if (e.getSource() == close) {
			frame.setVisible(false);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
