/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月19日 下午1:40:06
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room.file;

import javax.swing.JPanel;

public abstract class FileUI extends JPanel {
	public abstract void cancelTransfer();
}
