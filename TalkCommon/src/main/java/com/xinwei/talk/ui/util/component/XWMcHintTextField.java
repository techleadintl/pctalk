/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月10日 下午3:42:32
 * 
 ***************************************************************/
package com.xinwei.talk.ui.util.component;

import java.awt.Graphics;

import com.xinwei.common.lookandfeel.component.McWillHintTextField;
import com.xinwei.talk.common.LAF;

public class XWMcHintTextField extends McWillHintTextField {
	public XWMcHintTextField() {
		setHint("");
	}

	public XWMcHintTextField(final String hint) {
		setHint(hint);
	}

	@Override
	protected void paintComponent(Graphics g) {
		LAF.paintTextFieldBackgroud(this, g);
		super.paintComponent(g);
	}
}
