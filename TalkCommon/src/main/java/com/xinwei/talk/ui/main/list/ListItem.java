package com.xinwei.talk.ui.main.list;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.component.McWillScrollPaneLabel;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.XWObject;
import com.xinwei.talk.ui.util.component.avatar.AvatarPanel2;

public class ListItem extends JPanel {
	private static final long serialVersionUID = 1514044406550293152L;
	protected LocalManager localManager = TalkManager.getLocalManager();
	//头像 图标标签
	protected AvatarPanel2 avatarPanel;

	//显示名称 文字标签
	protected JLabel nameLabel;

	protected JLabel descriptionLabel;

	protected JLabel specialLabel;

	protected XWObject obj;

	public ListItem() {
		super();
		this.setOpaque(true);
		setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
		setLayout(new BorderLayout());

		avatarPanel = new AvatarPanel2(true);
		avatarPanel.setAvatarSize(50, 50);

		nameLabel = new McWillScrollPaneLabel() {
			@Override
			public String getText() {
				return getDisplayName();
			}
		};
		nameLabel.setFont(LAF.getFont(13));

		descriptionLabel = new McWillScrollPaneLabel();
		descriptionLabel.setFont(LAF.getFont(12));
		descriptionLabel.setForeground(LAF.getColor(150, 150, 150));

		specialLabel = new JLabel();
		specialLabel.setFont(LAF.getFont(12));
		specialLabel.setForeground(LAF.getColor(150, 150, 150));
		specialLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));

		JPanel upPanel = createUpPanel();

		JPanel jPanel = new JPanel(new GridLayout(2, 1));
		jPanel.add(upPanel);
		jPanel.add(descriptionLabel);

		add(avatarPanel, BorderLayout.WEST);
		add(jPanel, BorderLayout.CENTER);
	}

	protected JPanel createUpPanel() {
		JPanel upPanel = new JPanel(new BorderLayout());
		upPanel.add(nameLabel, BorderLayout.CENTER);
		upPanel.add(specialLabel, BorderLayout.EAST);
		return upPanel;
	}

	public void setObject(XWObject object) {
		this.obj = object;
		if (object != null) {
			String jid = object.getJid();
			setToolTipText(TalkUtil.getDisplayName(jid) + "(" + TalkUtil.getTalkName(jid));
		} else {
			setToolTipText(null);
		}
		setInfo(obj);
	}

	public XWObject getObject() {
		return obj;
	}

	protected void setInfo(XWObject obj) {
		this.obj = obj;
		String jid = obj.getJid();
		avatarPanel.setJid(jid);
		if (obj instanceof Talk) {
			descriptionLabel.setText(((Talk) obj).getSignature());
		} else if (obj instanceof Group) {
			descriptionLabel.setText(((Group) obj).getDescribe());
		}
	}

	public String getDisplayName() {
		if (obj != null) {
			return TalkUtil.getDisplayName(obj.getJid());
		} else {
			return "";
		}
	}

	@Override
	public boolean equals(Object obj) {
		ListItem that = (ListItem) obj;
		return that.getObject().getJid().equals(this.obj.getJid());
	}
}
