/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年2月28日 下午1:25:25
 * 
 ***************************************************************/
package com.xinwei.talk.ui.setting;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;

public class SettingDetailPanel extends JPanel {

	public SettingDetailPanel() {
		setLayout(new VerticalFlowLayout(VerticalFlowLayout.LEFT, 0, 0, true, true));
		addSubPanels();
		initValues();
		addListeners();
	}

	protected void addSubPanels() {
	}

	protected void initValues() {
	}

	protected void addListeners() {
	}
}
