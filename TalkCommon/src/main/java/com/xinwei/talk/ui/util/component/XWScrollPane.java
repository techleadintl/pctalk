/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月12日 下午1:40:27
 * 
 ***************************************************************/
package com.xinwei.talk.ui.util.component;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.ScrollBarUI;

@SuppressWarnings("serial")
public class XWScrollPane extends JPanel {
	JScrollPane jScrollPane;

	public XWScrollPane(Component list) {
		setLayout(new BorderLayout());

		jScrollPane = new JScrollPane(list) {
			@Override
			public JScrollBar createVerticalScrollBar() {
				JScrollBar createVerticalScrollBar = new JScrollBar(JScrollBar.VERTICAL) {
					@Override
					public void updateUI() {
						putClientProperty("isButtonVisible", isButtonVisible());
						putClientProperty("scrollBarWidth", getScrollBarWidth());
						putClientProperty("isPaintTrack", isPaintTrack());
						ComponentUI ui2 = UIManager.getUI(this);
						setUI((ScrollBarUI) ui2);
					}
				};
				return createVerticalScrollBar;
			}
		};
		jScrollPane.setBorder(BorderFactory.createEmptyBorder());
		jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		add(jScrollPane, BorderLayout.CENTER);
	}

	public JScrollPane getScrollPane() {
		return jScrollPane;
	}

	public int getScrollBarWidth() {
		return 9;
	}

	public boolean isButtonVisible() {
		return true;
	}

	public boolean isPaintTrack() {
		return true;
	}
}
