/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年2月28日 下午1:25:25
 * 
 ***************************************************************/
package com.xinwei.talk.ui.setting.detail;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import com.xinwei.common.lookandfeel.component.McWillCheckLabel;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.conf.UserConfig;
import com.xinwei.talk.ui.setting.SettingDetailPanel;
import com.xinwei.talk.ui.setting.SettingSubPanel;

public class ShortcutKeyPanel extends SettingDetailPanel {
	McWillCheckLabel screenShotCheckLabel;
	JTextField screenShotTextField;
	JLabel screenShotConflictLabel;
	McWillCheckLabel openMainWindowCheckLabel;
	JTextField openMainWindowTextField;
	JLabel openMainWindowConflictLabel;

	@Override
	protected void addSubPanels() {
		final Dimension preferredSize = new Dimension(180, 25);

		
		add(new SettingSubPanel(Res.getMessage("setting.shortcutkey.title")) {
			@Override
			protected void createContentPanel(JPanel contentPanel) {
				int strut = 10;
				Box leftBox = Box.createVerticalBox();
				Box centerBox = Box.createVerticalBox();
				Box rightBox = Box.createVerticalBox();
				Box formBox = Box.createHorizontalBox();
				// 截图
				screenShotCheckLabel = new McWillCheckLabel(LAF.getLoginCheck()[0], LAF.getLoginCheck()[1]);
				screenShotCheckLabel.setText(Res.getMessage("setting.shortcutkey.screenshot"));
				screenShotTextField = new JTextField();
				screenShotTextField.setEditable(false);
				screenShotTextField.setPreferredSize(preferredSize);
				screenShotConflictLabel = new JLabel(Res.getMessage("setting.shortcutkey.conflict"));
				screenShotConflictLabel.setVisible(false);
				screenShotConflictLabel.setForeground(Color.RED);
				// 打开隐藏主窗口
				openMainWindowCheckLabel = new McWillCheckLabel(LAF.getLoginCheck()[0], LAF.getLoginCheck()[1]);
				openMainWindowCheckLabel.setText(Res.getMessage("setting.shortcutkey.mainwindow"));
				openMainWindowTextField = new JTextField();
				openMainWindowTextField.setEditable(false);
				openMainWindowTextField.setPreferredSize(preferredSize);
				openMainWindowConflictLabel = new JLabel(Res.getMessage("setting.shortcutkey.conflict"));
				openMainWindowConflictLabel.setVisible(false);
				openMainWindowConflictLabel.setForeground(Color.RED);

				openMainWindowCheckLabel.setLabelFor(openMainWindowTextField);

				leftBox.add(screenShotCheckLabel);
				leftBox.add(Box.createVerticalStrut(strut));
				leftBox.add(openMainWindowCheckLabel);

				centerBox.add(screenShotTextField);
				centerBox.add(Box.createVerticalStrut(strut));
				centerBox.add(openMainWindowTextField);

				rightBox.add(screenShotConflictLabel);
				rightBox.add(Box.createVerticalStrut(strut));
				rightBox.add(openMainWindowConflictLabel);

				formBox.add(leftBox);
				formBox.add(Box.createHorizontalStrut(25));
				formBox.add(centerBox);
				formBox.add(Box.createHorizontalStrut(15));
				formBox.add(rightBox);

				contentPanel.add(formBox);

				contentPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

			}
		});

		// added @ may 10 from here
		/*
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		toolkit.addAWTEventListener(new java.awt.event.AWTEventListener() {
			public void eventDispatched(AWTEvent event) {
				if (event.getClass() == KeyEvent.class) {
					KeyEvent kE = ((KeyEvent) event);
					if (kE.getKeyCode() == KeyEvent.VK_Q && kE.isControlDown() && kE.getID() == KeyEvent.KEY_PRESSED) {

						final MainWindow mainWindow = TalkManager.getMainWindow();
						System.out.println("Ctrl +Q");

						if (mainWindow.isVisible() == true) {	
							mainWindow.setVisible(false);
						} else {
							mainWindow.setVisible(true);
						}
					}

				}
			}

		}, java.awt.AWTEvent.KEY_EVENT_MASK);
		*/
		// to here

	}

	
	@Override
	protected void initValues() {
		UserConfig userConfig = TalkManager.getUserConfig();
		// 截图
		String S_KEY = "Screenshot";
		screenShotTextField.putClientProperty("$KEY", S_KEY);
		screenShotTextField.putClientProperty("$Conflict", screenShotConflictLabel);
		boolean screenshotEnable = userConfig.getShortKeyEnable(S_KEY);
		screenShotCheckLabel.setSelected(screenshotEnable);
		screenShotTextField.setEnabled(screenshotEnable);
		screenShotTextField.setEditable(screenshotEnable);
		screenShotTextField.setText(userConfig.getShortKeyValue(S_KEY));
		screenShotTextField.setToolTipText(screenShotTextField.getText());
		// 打开隐藏主窗口
		String O_KEY = "MainWindow";
		openMainWindowTextField.putClientProperty("$KEY", O_KEY);
		openMainWindowTextField.putClientProperty("$Conflict", openMainWindowConflictLabel);
		boolean mainwindowEnable = userConfig.getShortKeyEnable(O_KEY);
		openMainWindowCheckLabel.setSelected(mainwindowEnable);
		openMainWindowTextField.setEnabled(mainwindowEnable);
		openMainWindowTextField.setEditable(mainwindowEnable);
		openMainWindowTextField.setText(userConfig.getShortKeyValue(O_KEY));
		openMainWindowTextField.setToolTipText(openMainWindowTextField.getText());
	}

	@Override
	protected void addListeners() {
		ActionListener al = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 截图
				UserConfig userConfig = TalkManager.getUserConfig();
				if (e.getSource() == screenShotCheckLabel) {
					String key = (String) screenShotTextField.getClientProperty("$KEY");
					if (screenShotCheckLabel.isSelected()
							&& userConfig.hasShortKeyValue(key, screenShotTextField.getText())) {
						screenShotConflictLabel.setVisible(true);
					} else {
						screenShotConflictLabel.setVisible(false);

						userConfig.setShortKeyEnable(key, screenShotCheckLabel.isSelected());
						TalkManager.saveUserConfig();
					}
					screenShotTextField.setEnabled(screenShotCheckLabel.isSelected());
					screenShotTextField.setEditable(screenShotCheckLabel.isSelected()); // added
																						// may
																						// 09

				}
				// 打开隐藏主窗口
				else if (e.getSource() == openMainWindowCheckLabel) {
					String key = (String) openMainWindowTextField.getClientProperty("$KEY");
					if (openMainWindowCheckLabel.isSelected()
							&& userConfig.hasShortKeyValue(key, openMainWindowTextField.getText())) {
						openMainWindowConflictLabel.setVisible(true);
					} else {
						openMainWindowConflictLabel.setVisible(false);
						userConfig.setShortKeyEnable(key, openMainWindowCheckLabel.isSelected());

						TalkManager.saveUserConfig();
					}
					openMainWindowTextField.setEnabled(openMainWindowCheckLabel.isSelected());
					openMainWindowTextField.setEditable(openMainWindowCheckLabel.isSelected()); // added
																								// may
																								// 09
				}
			}
		};

		screenShotCheckLabel.addActionListener(al);
		openMainWindowCheckLabel.addActionListener(al);

		KeyAdapter kl = new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				JTextField textField = (JTextField) e.getSource();
				String text = textField.getText();
				String key = (String) textField.getClientProperty("$KEY");

				String shortKeyValue = TalkManager.getUserConfig().getShortKeyValue(key);
				if (text.equals(shortKeyValue)) {
					return;
				}

				JLabel label = (JLabel) textField.getClientProperty("$Conflict");
				if (TalkManager.getUserConfig().hasShortKeyValue(key, text)) {
					label.setVisible(true);
					textField.setText(shortKeyValue);							// added may 11
				} else {
					label.setVisible(false);
					TalkManager.getUserConfig().setShortKeyValue(key, text);

					TalkManager.saveUserConfig();
					
					textField.setToolTipText(text);
	
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println("keyReleased");

			}

			@Override
			public void keyPressed(KeyEvent e) {
				JTextField textField = (JTextField) e.getSource();
		
				String keyPressValue = getKeyPressValue(e);

				if(!(keyPressValue=="")){
				textField.setText(keyPressValue);
				}
				textField.setSelectionEnd(keyPressValue.length());

				e.consume();
			}
		};

		screenShotTextField.addKeyListener(kl);
		openMainWindowTextField.addKeyListener(kl);

	}

	public static String getKeyPressValue(KeyEvent e) {
		String v = "";
		if (e.isControlDown()) {
			v += "Ctrl + ";
		}
		if (e.isAltDown()) {
			v += "Alt + ";
		}
		if (e.isShiftDown()) {
			v += "Shift + ";
		}
		if (v.length() == 0) {
			return "";
		}
		int keyCode = e.getKeyCode();
		if (keyCode >= KeyEvent.VK_F1 && keyCode <= KeyEvent.VK_F19) {
			v += "F" + (keyCode - KeyEvent.VK_F1 + 1);
		} else if (keyCode >= KeyEvent.VK_A && keyCode <= KeyEvent.VK_Z) {
			v += String.valueOf((char) keyCode).toUpperCase();
		} else if (keyCode >= KeyEvent.VK_0 && keyCode <= KeyEvent.VK_9) {
			v += (char) keyCode;
		} else {
			v = "";
		}
		return v;

	}

}

/*
 * 
 * // Toolkit toolkit = Toolkit.getDefaultToolkit(); // // 注册应用程序全局键盘事件,
 * 所有的键盘事件都会被此事件监听器处理. // //Registers application global keyboard events. All
 * keyboard events are handled by this event listener. //
 * toolkit.addAWTEventListener(new java.awt.event.AWTEventListener() {
 * 
 * // public void eventDispatched(AWTEvent event) { // if (event.getClass() ==
 * KeyEvent.class) { // KeyEvent kE = ((KeyEvent) event); // 处理按键事件 Ctrl+Enter
 * 
 * // if ((kE.getKeyCode() == KeyEvent.VK_ENTER) && (((InputEvent)
 * event).isControlDown())) { // // window.refreshAction(); // } /* page down
 */// else if (kE.getKeyCode() == KeyEvent.VK_PAGE_DOWN && kE.getID() ==
	// KeyEvent.KEY_PRESSED) {
// window.nextPageAction();
// // System.out.println("page down:"
// // + kE.getID());
//
// } /* page up */else if (kE.getKeyCode() == KeyEvent.VK_PAGE_UP && kE.getID()
// == KeyEvent.KEY_PRESSED) {
// window.previousPageAction();
// // System.out.println("page down:"
// // + kE.getID());

// } /* Ctrl +K */else if (kE.getKeyCode() == KeyEvent.VK_K &&
// kE.isControlDown() && kE.getID() == KeyEvent.KEY_PRESSED) {
// System.out.println("Ctrl +K");
// window.getSearchTF().requestFocus();
// }
// /* home */else if (kE.getKeyCode() == KeyEvent.VK_HOME && kE.getID() ==
// KeyEvent.KEY_PRESSED) {
// System.out.println("home");
// window.firstPageAction();
// }
// /* end */else if (kE.getKeyCode() == KeyEvent.VK_END && kE.getID() ==
// KeyEvent.KEY_PRESSED) {
// System.out.println("end");
// window.lastPageAction();
// }
// }
// }
// }, java.awt.AWTEvent.KEY_EVENT_MASK);
/*
 * }
 */
