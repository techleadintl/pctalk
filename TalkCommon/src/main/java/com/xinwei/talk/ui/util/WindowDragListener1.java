/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月12日 下午2:33:50
 * 
 ***************************************************************/
package com.xinwei.talk.ui.util;

import java.awt.Point;
import java.awt.Window;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class WindowDragListener1 implements MouseListener, MouseMotionListener {
	private Boolean isDragged;
	private Point loc, tmp;

	private Window window;
	private int i = 0;

	public WindowDragListener1() {
	}

	public WindowDragListener1(Window window) {
		this.window = window;
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	//拖动窗体
	public void mouseReleased(MouseEvent e) {
		isDragged = false;
//		dialog.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}

	public void mousePressed(MouseEvent e) {
		tmp = new Point(e.getX(), e.getY());//获取窗体位置
		isDragged = true;
//		dialog.setCursor(new Cursor(Cursor.MOVE_CURSOR));
	}

	public void mouseDragged(MouseEvent e) {
		if (isDragged) {
			{// 通过降低重绘频率，解决移动出现闪动
				if (i++ % 8 != 0) {
					return;
				}
				if (i > 100000) {
					i = 0;
				}
			}

			loc = new Point(window.getLocation().x + e.getX() - tmp.x, window.getLocation().y + e.getY() - tmp.y);
			window.setLocation(loc);
		}
	}

	public Window getDialog() {
		return window;
	}

	public void setWindow(Window dialog) {
		this.window = dialog;
	}
}
