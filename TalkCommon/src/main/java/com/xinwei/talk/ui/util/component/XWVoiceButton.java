package com.xinwei.talk.ui.util.component;
//package com.xinwei.talk.ui.util.component;
//
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.io.IOException;
//import java.io.InputStream;
//
//import javax.swing.ImageIcon;
//
//import org.jivesoftware.spark.component.RolloverButton;
//
//import sun.audio.AudioDataStream;
//import sun.audio.AudioPlayer;
//import sun.audio.AudioStream;
//
//
//@SuppressWarnings("serial")
//public class XWVoiceButton extends RolloverButton implements ActionListener {
//
//	private InputStream voiceStream;
//	private InputStream newPlay = null;
//
//	public XWVoiceButton(ImageIcon icon, InputStream voiceStream){
//		super(icon);
//		this.voiceStream = voiceStream;
//		this.addActionListener(this);
//	}
//
//	@Override
//	public void actionPerformed(ActionEvent e) {
//		stop();
//		play();
//	}
//
//	protected void play() {
//		
//		AudioDataStream ads = null;
//		try {
//			voiceStream.reset();
//			ads = new AudioDataStream((new AudioStream(voiceStream)).getData());
//		} catch (IOException e) {
//			return;
//		}
//		newPlay = ads;		
//		AudioPlayer.player.start(ads);
//	}
//	
//	protected void stop() {
//		if (newPlay != null) {
//			AudioPlayer.player.stop(newPlay);
//			newPlay = null;
//		}
//	}
//
//}
