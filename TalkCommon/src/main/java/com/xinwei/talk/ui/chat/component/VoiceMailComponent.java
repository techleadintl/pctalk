package com.xinwei.talk.ui.chat.component;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayInputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.xinwei.common.lang.SystemUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.model.message.specific.TalkVoiceMailSpecific;
import com.xinwei.talk.ui.chat.voice.VoiceManager;

@SuppressWarnings("serial")
public class VoiceMailComponent extends JLabel {
	private static ImageIcon leftIcon = LAF.getMsgVoiceLeft();
	private static ImageIcon[] leftIcons = LAF.getMsgVoiceLefts();
	private static ImageIcon rightIcon = LAF.getMsgVoiceRight();
	private static ImageIcon[] rightIcons = LAF.getMsgVoiceRights();
	//wav bytes
	private byte[] wav;
	private Play newPlay = null;

	private boolean left;

	public VoiceMailComponent(TalkVoiceMailSpecific msg, boolean left, ImageIcon icon) {
		this(msg, left, icon, null);
	}

	public VoiceMailComponent(TalkVoiceMailSpecific msg, boolean left, ImageIcon icon, byte[] voice) {
		if (icon != null) {
			setIcon(icon);
		} else {
			setIcon(left ? leftIcon : rightIcon);
		}
		setVoice(voice);
		setHorizontalAlignment(SwingConstants.RIGHT);

		setText(msg.getDate());
		this.left = left;
		setOpaque(false);

		setPreferredSize(new Dimension(0, 32));
		setMaximumSize(new Dimension(100, 32));

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//				if (e.getClickCount() == 2) {
				stop();
				play();
				//				}
			}
		});
	}

	public void setDefaultIcon(boolean flag) {
		if (left) {
			setIcon(leftIcon);
		} else {
			setIcon(rightIcon);
		}
	}

	//播放录音
	public void play() {
		//将baos中的数据转换为字节数据
		//转换为输入流
		ByteArrayInputStream bais = new ByteArrayInputStream(wav);
		AudioFormat af = VoiceManager.getAudioFormat();
		AudioInputStream ais = new AudioInputStream(bais, af, wav.length / af.getFrameSize());

		try {
			DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, af);
			SourceDataLine sd = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
			sd.open(af);
			sd.start();
			//创建播放进程
			Play py;
			if (left) {
				py = new Play(ais, sd, leftIcon, leftIcons);
			} else {
				py = new Play(ais, sd, rightIcon, rightIcons);
			}

			py.start();
			newPlay = py;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				//关闭流
				if (ais != null) {
					ais.close();
				}
				if (bais != null) {
					bais.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	protected void stop() {
		if (newPlay != null) {
			newPlay.stop(true);
			newPlay = null;
		}
	}

	public byte[] getVoice() {
		return wav;
	}

	public void setVoice(byte[] voice) {
		this.wav = voice;
	}


	//播放类
	class Play {
		AudioInputStream ais;
		SourceDataLine sd;
		ImageIcon icon;
		ImageIcon[] icons;
		private int i = 0;
		private boolean stop = false;

		public Play(AudioInputStream ais, SourceDataLine sd, ImageIcon icon, ImageIcon[] icons) {
			this.ais = ais;
			this.sd = sd;
			this.icon = icon;
			this.icons = icons;
		}

		public void stop(boolean stop) {
			this.stop = stop;
		}

		//播放baos中的数据即可
		public void start() {
			SystemUtil.execute(new Runnable() {
				public void run() {
					byte bts[] = new byte[10000];
					try {
						int cnt;
						//读取数据到缓存数据
						while ((cnt = ais.read(bts, 0, bts.length)) != -1 && !stop) {
							if (cnt > 0) {
								//写入缓存数据
								sd.write(bts, 0, cnt);//将音频数据写入到混频器
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						stop = true;
						try {
							sd.drain();
							sd.close();
						} catch (Exception e) {
						}
					}
				}
			});
			SystemUtil.execute(new Runnable() {
				public void run() {
					while (!stop) {
						setIcon(icons[i++ % 3]);
						SystemUtil.sleep(400);
						if (i > 100000) {
							i = 0;
						}
					}
					stop = true;
					setIcon(icon);
				}
			});
		}

	}
}
