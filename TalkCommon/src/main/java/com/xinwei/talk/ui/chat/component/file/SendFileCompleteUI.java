package com.xinwei.talk.ui.chat.component.file;

import java.io.File;

import com.xinwei.talk.common.Res;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.ui.chat.balloon.BalloonMessage;

public class SendFileCompleteUI extends FileCompleteUI {
	public SendFileCompleteUI(BalloonMessage balloonMsg,TalkFileSpecific fileMsg, final File file) {
		super(balloonMsg,fileMsg, file);
	}

	protected void initData(final File file) {
		filePathTextPane.setText(Res.getMessage("label.chatroom.fileupload.complete") + ":" + file.getAbsolutePath());
	}
}
