package com.xinwei.talk.ui.skin.skin.colorchooser;
import java.awt.AWTException;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Robot;
import java.awt.Transparency;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.security.AccessControlException;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

public class ColorPickListener implements ActionListener {
	/**
	 * This frame is constantly moved to the current location of the mouse. This
	 * ensures that we can trap mouse clicks while the picker cursor is showing.
	 * Also, by tying the picker cursor to this frame, we ensure that the picker
	 * cursor (the magnifying glass) is shown.
	 */
	private Dialog pickerFrame;

	private Timer pickerTimer;

	/**
	 * Holds the image of the picker cursor.
	 */
	private BufferedImage cursorImage;
	/**
	 * Graphics object used for drawing on the cursorImage.
	 */
	private Graphics2D cursorGraphics;

	/**
	 * The hot spot of the cursor.
	 */
	private Point hotSpot;

	/**
	 * Offset from the hot spot of the pickerCursor to the pixel that we want to
	 * pick. We can't pick the color at the hotSpot of the cursor, because this
	 * point is obscured by the pickerFrame.
	 */
	private Point pickOffset;

	/**
	 * The magnifying glass image.
	 */
	private BufferedImage magnifierImage;

	/**
	 * The robot is used for creating screen captures.
	 */
	private Robot robot;
	private final static Color transparentColor = new Color(0, true);

	private Color previousColor = Color.white;
	private Point previousLoc = new Point();
	private Point pickLoc = new Point();
	private Point captureOffset = new Point();
	private Rectangle captureRect;
	private Rectangle zoomRect;
	private Rectangle glassRect;

	private Component pickerButton;

	public ColorPickListener() {
		try {
			robot = new Robot();
			robot.createScreenCapture(new Rectangle(0, 0, 1, 1));
		} catch (AWTException e) {
			throw new AccessControlException("Unable to capture screen");
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		pickerButton = (Component) e.getSource();

		pickBegin(e);
	}

	private Dialog getPickerFrame() {
		if (pickerFrame == null) {
			Window owner = SwingUtilities.getWindowAncestor(pickerButton);
			if (owner instanceof Dialog) {
				pickerFrame = new Dialog((Dialog) owner);
			} else if (owner instanceof Frame) {
				pickerFrame = new Dialog((Frame) owner);
			} else {
				pickerFrame = new Dialog(new JFrame());
			}

			pickerFrame.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent evt) {
					pickFinish();
				}

				@Override
				public void mouseExited(MouseEvent evt) {
					updatePicker();
				}
			});

			pickerFrame.addMouseMotionListener(new MouseMotionAdapter() {
				@Override
				public void mouseMoved(MouseEvent evt) {
					updatePicker();
				}
			});
			pickerFrame.setSize(3, 3);
			pickerFrame.setUndecorated(true);
			pickerFrame.setAlwaysOnTop(true);

			pickerFrame.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					switch (e.getKeyCode()) {
					case KeyEvent.VK_ESCAPE:
						pickCancel();
						break;
					case KeyEvent.VK_ENTER:
						pickFinish();
						break;
					}
				}
			});

			magnifierImage = getPickerMagnifier();

			glassRect = new Rectangle(3, 3, 26, 26);

			zoomRect = new Rectangle(4, 4, 25, 25);

			hotSpot = new Point(29, 29);

			captureRect = new Rectangle(-15, -15, 5, 5);

			pickOffset = new Point(-13, -13);

			captureOffset = new Point(captureRect.x, captureRect.y);

			cursorImage = pickerButton.getGraphicsConfiguration().createCompatibleImage(magnifierImage.getWidth(), magnifierImage.getHeight(), Transparency.TRANSLUCENT);
			cursorGraphics = cursorImage.createGraphics();
			cursorGraphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);

			pickerTimer = new Timer(5, new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					updatePicker();
				}
			});
		}
		return pickerFrame;
	}

	/**
	 * Updates the color picker.
	 */
	protected void updatePicker() {
		if (pickerFrame != null && pickerFrame.isShowing()) {
			PointerInfo info = MouseInfo.getPointerInfo();
			Point mouseLoc = info.getLocation();
			pickerFrame.setLocation(mouseLoc.x - pickerFrame.getWidth() / 2, mouseLoc.y - pickerFrame.getHeight() / 2);

			pickLoc.x = mouseLoc.x + pickOffset.x;
			pickLoc.y = mouseLoc.y + pickOffset.y;

			if (pickLoc.x >= 0 && pickLoc.y >= 0) {
				Color c = robot.getPixelColor(pickLoc.x, pickLoc.y);
				if (!c.equals(previousColor) || !mouseLoc.equals(previousLoc)) {
					previousColor = c;
					previousLoc = mouseLoc;

					captureRect.setLocation(mouseLoc.x + captureOffset.x, mouseLoc.y + captureOffset.y);
					if (captureRect.x >= 0 && captureRect.y >= 0) {
						BufferedImage capture = robot.createScreenCapture(captureRect);

						cursorGraphics.setComposite(AlphaComposite.Src);
						cursorGraphics.setColor(transparentColor);
						cursorGraphics.fillRect(0, 0, cursorImage.getWidth(), cursorImage.getHeight());

						cursorGraphics.setColor(Color.red);
						cursorGraphics.fillOval(glassRect.x, glassRect.y, glassRect.width, glassRect.height);

						cursorGraphics.setComposite(AlphaComposite.SrcIn);
						cursorGraphics.drawImage(capture, zoomRect.x, zoomRect.y, zoomRect.width, zoomRect.height, pickerButton);

						cursorGraphics.setComposite(AlphaComposite.SrcOver);
						cursorGraphics.drawImage(magnifierImage, 0, 0, pickerButton);

						pickerFrame.setCursor(pickerButton.getToolkit().createCustomCursor(cursorImage, hotSpot, "ColorPicker"));
					}
				}
			}
		}
	}

	private void pickBegin(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_pickBegin
		getPickerFrame();
		pickerTimer.start();
		getPickerFrame().setVisible(true);
	}

	protected void pickFinish() {
		pickerTimer.stop();
		pickerFrame.setVisible(false);
		PointerInfo info = MouseInfo.getPointerInfo();
		Point loc = info.getLocation();
		pickColor = robot.getPixelColor(loc.x + pickOffset.x, loc.y + pickOffset.y);
		setPickColor(pickColor);
	}

	protected void pickCancel() {
		pickerTimer.stop();
		pickerFrame.setVisible(false);
	}

	private static BufferedImage getPickerMagnifier() {
		BufferedImage result = getBlankImage(48, 48);
		Graphics2D g = result.createGraphics();

		g.setColor(Color.RED);
		g.translate(-4, -6);
		int xc = 20;
		int yc = 22;
		int r = 15;

		g.setStroke(new BasicStroke(2.5f));
		g.drawOval(xc - r, yc - r, 2 * r, 2 * r);
		g.setStroke(new BasicStroke(4.0f));
		GeneralPath handle = new GeneralPath();
		handle.moveTo((float) (xc + r / Math.sqrt(2.0)), (float) (yc + r / Math.sqrt(2.0)));
		handle.lineTo(45, 47);
		g.draw(handle);
		g.translate(4, 6);

		g.setStroke(new BasicStroke(1.0f));
		g.drawLine(16, 4, 16, 13);
		g.drawLine(4, 16, 13, 16);
		g.drawLine(16, 19, 16, 28);
		g.drawLine(19, 16, 28, 16);
		return result;
	}

	private static BufferedImage getBlankImage(int width, int height) {
		GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice d = e.getDefaultScreenDevice();
		GraphicsConfiguration c = d.getDefaultConfiguration();
		BufferedImage compatibleImage = c.createCompatibleImage(width, height, Transparency.TRANSLUCENT);
		return compatibleImage;
	}

	public Color getPickColor() {
		return pickColor;
	}

	public void setPickColor(Color selectedColor) {
		this.pickColor = selectedColor;
	}

	private Color pickColor;
}
