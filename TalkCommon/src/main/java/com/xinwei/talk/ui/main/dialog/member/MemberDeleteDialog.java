//package com.xinwei.talk.ui.main.dialog.member;
//
//import java.awt.BorderLayout;
//import java.awt.Color;
//import java.awt.Component;
//import java.awt.Dimension;
//import java.awt.GridLayout;
//import java.awt.HeadlessException;
//import java.awt.Window;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.swing.BorderFactory;
//import javax.swing.JButton;
//import javax.swing.JComponent;
//import javax.swing.JDialog;
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//import javax.swing.ScrollPaneConstants;
//
//import com.xinwei.common.lang.CollectionUtil;
//import com.xinwei.common.lang.StringUtil;
//import com.xinwei.common.lookandfeel.McWillTheme;
//import com.xinwei.common.lookandfeel.component.McWillIconSearchTextField;
//import com.xinwei.common.lookandfeel.component.McWillIconSearchTextField.SearchListener;
//import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
//import com.xinwei.common.lookandfeel.component.McWillScrollPane;
//import com.xinwei.common.lookandfeel.util.Constants;
//import com.xinwei.spark.ui.MessageDialog;
//import com.xinwei.talk.common.LAF;
//import com.xinwei.talk.common.Res;
//import com.xinwei.talk.manager.TalkManager;
//import com.xinwei.talk.model.Group;
//import com.xinwei.talk.model.Member;
//import com.xinwei.talk.ui.main.dialog.member.model.CaptionSort;
//import com.xinwei.talk.ui.main.dialog.member.model.MemberInfo;
//import com.xinwei.talk.ui.main.dialog.member.model.MemberInfoSort;
//import com.xinwei.talk.ui.main.dialog.member.ui.GroupMemberDialogContentPanel;
//import com.xinwei.talk.ui.main.dialog.member.ui.LeftMemItemUI;
//import com.xinwei.talk.ui.main.dialog.member.ui.RightMemItemUI;
//import com.xinwei.talk.ui.main.dialog.member.util.MemUtil;
//
//@SuppressWarnings("serial")
//public class MemberDeleteDialog extends JDialog {
//	final String SEPARATOR = " ";
//
//	private JPanel rightMemsPanel;
//
//	private JPanel leftMemsPanel;
//
//	private JLabel rightTitleLabel;
//
//	private JButton saveButton;
//
//	private McWillIconSearchTextField findField;
//
//	private Group group;
//
//	List<MemberInfo> memberInfoList;
//
//	public MemberDeleteDialog(Window owner, Group group) throws HeadlessException {
//		super(owner);
//		this.group = group;
//ddd
//		setModal(true);
//
//		setResizable(false);
//
//		GroupMemberDialogContentPanel contentPanel = new GroupMemberDialogContentPanel();
//
//		//		setTitle(tabTitle);
//
//		JPanel pane = createDialogContent();
//
//		contentPanel.add(pane, BorderLayout.CENTER);
//
//		setContentPane(contentPanel);
//
//		pack();
//
//		setLocationRelativeTo(owner);
//
//		McWillTheme.setOpaque(pane);
//	}
//
//	protected JPanel createDialogContent() {
//		JPanel inputPanel = new JPanel();
//
//		inputPanel.setPreferredSize(new Dimension(650, 450));
//
//		inputPanel.setLayout(new GridLayout(1, 2, 2, 0));
//
//		final JPanel leftPanel = createLeftUI();
//
//		JPanel rightPane = createRightUI();
//
//		inputPanel.add(leftPanel);
//
//		inputPanel.add(rightPane);
//
//		initLeftUI();
//
//		updateRightUI();
//
//		return inputPanel;
//	}
//
//	private JPanel createLeftUI() {
//		final JPanel leftPanel = new JPanel(new BorderLayout(0, 15));
//
//		findField = new McWillIconSearchTextField(LAF.getSearchIcon(), LAF.getDeleteIcon()[0]);
//		findField.setPreferredSize(new Dimension(-1, 25));
//
//		leftMemsPanel = new JPanel();
//		leftMemsPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP, 0, 0, true, false));
//
//		McWillScrollPane leftScrollPane = new McWillScrollPane(leftMemsPanel);
//		leftScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
//		leftScrollPane.setBorder(null);
//
//		leftPanel.putClientProperty(Constants.OPAQUE, false);
//
//		leftPanel.add(findField, BorderLayout.NORTH);
//		leftPanel.add(leftScrollPane, BorderLayout.CENTER);
//
//		findField.setSearchListener(new SearchListener() {
//			@Override
//			public void doSearch(String text) {
//				if (CollectionUtil.isEmpty(memberInfoList))
//					return;
//
//				List<MemberInfo> filter = null;
//
//				if (StringUtil.isEmpty(text)) {
//					filter = memberInfoList;
//				} else {
//					filter = new ArrayList<>();
//					for (MemberInfo memberInfo : memberInfoList) {
//						boolean flag = memberInfo.search(text);
//						if (flag) {
//							filter.add(memberInfo);
//						}
//					}
//				}
//
//				updateLeftUI(filter);
//			}
//		});
//
//		return leftPanel;
//	}
//
//	private JPanel createRightUI() {
//		JPanel rightPanel = new JPanel(new BorderLayout());
//
//		rightTitleLabel = new JLabel();
//
//		JPanel buttonPanel = new JPanel();
//
//		saveButton = new JButton(Res.getMessage("button.ok"));
//		JButton closeButton = new JButton(Res.getMessage("button.close"));
//
//		Dimension preferredSize = new Dimension(30, -1);
//		saveButton.setPreferredSize(preferredSize);
//		closeButton.setPreferredSize(preferredSize);
//
//		buttonPanel.add(saveButton);
//		buttonPanel.add(closeButton);
//
//		rightMemsPanel = new JPanel();
//		rightMemsPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP, 0, 1, true, false));
//
//		McWillScrollPane rightScrollPane = new McWillScrollPane(rightMemsPanel);
//		rightScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
//		rightScrollPane.setBorder(null);
//
//		rightPanel.add(rightTitleLabel, BorderLayout.NORTH);
//
//		rightPanel.add(rightScrollPane, BorderLayout.CENTER);
//
//		rightPanel.add(buttonPanel, BorderLayout.SOUTH);
//
//		rightMemsPanel.setBackground(Color.RED);
//		rightMemsPanel.putClientProperty(Constants.OPAQUE, false);
//
//		saveButton.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				onSaveClick();
//			}
//		});
//
//		closeButton.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				MemberDeleteDialog.this.dispose();
//			}
//		});
//
//		return rightPanel;
//	}
//
//	private void onSaveClick() {
//		Component[] components = rightMemsPanel.getComponents();
//		if (components == null || components.length == 0)
//			return;
//
//		List<String> memberList = new ArrayList<String>();
//		for (Component component : components) {
//			MemberInfo memberInfo = (MemberInfo) ((JComponent) component).getClientProperty("data");
//			if (memberInfo == null) {
//				continue;
//			}
//			memberList.add(memberInfo.uid);
//		}
//		if (memberList.isEmpty()) {
//			//TODO:添加提示信息
//			return;
//		}
//
//		try {
//
//			TalkManager.getHttpService().deleteMembers(group.getCreatorUid(), group.getId(), memberList);
//
//			MemberDeleteDialog.this.dispose();
//
//			//			XEventDispatcher.dispatchAsynEvent("", evt);
//		} catch (Exception e) {
//			e.printStackTrace();
//			//TODO:添加提示信息
//			MessageDialog.showErrorDialog(e);
//			return;
//		}
//
//	}
//
//	private void onLeftClick(MemberInfo memberInfo, final LeftMemItemUI leftItem, boolean selected) {
//		if (selected) {
//			RightMemItemUI rightItem = new RightMemItemUI(memberInfo) {
//				@Override
//				public void onRemoveClick() {
//					removeSelectedItem(leftItem);
//				}
//			};
//
//			rightItem.putClientProperty("data", memberInfo);
//
//			leftItem.putClientProperty("right", rightItem);
//
//			rightMemsPanel.add(rightItem);
//
//			updateRightUI();
//
//		} else {
//			removeSelectedItem(leftItem);
//		}
//
//	}
//
//	private void changeLeftBackgroud(final JPanel memberPanel, Component source) {
//		Component[] components = memberPanel.getComponents();
//		for (Component component : components) {
//			if (!(component instanceof LeftMemItemUI))
//				continue;
//
//			if (component == source) {
//				component.setBackground(McWillTheme.getThemeColor().getSubDarkColor());
//			} else {
//				component.setBackground(McWillTheme.getThemeColor().getLightColor());
//			}
//		}
//	}
//
//	private void removeSelectedItem(final LeftMemItemUI leftUserItemPanel) {
//		JComponent rightItem = (JComponent) leftUserItemPanel.getClientProperty("right");
//		if (rightItem != null) {
//			leftUserItemPanel.putClientProperty("right", null);
//			rightMemsPanel.remove(rightItem);
//			updateRightUI();
//		}
//	}
//
//	private void initLeftUI() {
//		memberInfoList = new ArrayList<>();
//		List<Member> groupMembers = TalkManager.getLocalManager().getMemberList(group.getId());
//
//		for (Member member : groupMembers) {
//			MemberInfo memberInfo = MemUtil.getMemberInfo(member.getMemJid(), member.getUid(), member.getMemNick());
//			memberInfoList.add(memberInfo);
//		}
//		updateLeftUI(memberInfoList);
//	}
//
//	private void updateRightUI() {
//		int componentCount = rightMemsPanel.getComponentCount();
//		if (componentCount == 0) {
//			rightTitleLabel.setText(Res.getMessage("label.group.select.member"));
//			saveButton.setEnabled(false);
//		} else {
//			rightTitleLabel.setText(Res.getMessage("label.group.select.member2", componentCount));
//			saveButton.setEnabled(true);
//		}
//
//		rightMemsPanel.updateUI();
//	}
//
//	private void updateLeftUI(List<MemberInfo> memberInfoList) {
//		Map<String, List<MemberInfo>> map = new HashMap<>();
//
//		for (MemberInfo memberInfo : memberInfoList) {
//			List<MemberInfo> list = map.get(memberInfo.caption);
//			if (list == null) {
//				list = new ArrayList<>();
//				map.put(memberInfo.caption, list);
//			}
//			list.add(memberInfo);
//		}
//		List<String> captionList = new ArrayList<>(map.keySet());
//
//		Collections.sort(captionList, new CaptionSort());
//
//		leftMemsPanel.removeAll();
//		leftMemsPanel.updateUI();
//
//		MemberInfoSort c = new MemberInfoSort();
//		for (String caption : captionList) {
//			JLabel captionLabel = (JLabel) leftMemsPanel.getClientProperty(caption);
//			if (captionLabel == null) {
//				captionLabel = new JLabel(caption);
//				captionLabel.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
//				captionLabel.setOpaque(true);
//				captionLabel.setBackground(McWillTheme.getThemeColor().getSrcColor());
//				leftMemsPanel.putClientProperty(caption, captionLabel);
//			}
//
//			leftMemsPanel.add(captionLabel);
//
//			List<MemberInfo> list = map.get(caption);
//			Collections.sort(list, c);
//
//			for (final MemberInfo memberInfo : list) {
//				LeftMemItemUI leftMemItemPanel = (LeftMemItemUI) leftMemsPanel.getClientProperty(memberInfo);
//
//				if (leftMemItemPanel == null) {
//
//					leftMemItemPanel = new LeftMemItemUI(memberInfo, false) {
//						@Override
//						public void onMemItemClick() {
//							changeLeftBackgroud(leftMemsPanel, this);
//						}
//
//						@Override
//						public void onMemItemChecked(boolean oldCheck, boolean newCheck) {
//							onLeftClick(memberInfo, this, newCheck);
//						}
//					};
//					leftMemsPanel.putClientProperty(memberInfo, leftMemItemPanel);
//				}
//
//				leftMemsPanel.add(leftMemItemPanel);
//			}
//		}
//	}
//}