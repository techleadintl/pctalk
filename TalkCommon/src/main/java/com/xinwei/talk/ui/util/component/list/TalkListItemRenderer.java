package com.xinwei.talk.ui.util.component.list;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.model.Talk;

public class TalkListItemRenderer extends JLabel implements ListCellRenderer<Talk> {
	private static final long serialVersionUID = -694803906607554443L;

	/**
	 * Construct Default JLabelIconRenderer.
	 */
	public TalkListItemRenderer() {
		setOpaque(true);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends Talk> list, Talk camTalk, int index, boolean isSelected, boolean cellHasFocus) {
		if (isSelected) {
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
		} else {
			setBackground(list.getBackground());
			setForeground(list.getForeground());
		}

		this.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));

		setIcon(ImageManager.getTalkAvatar(camTalk.getJid(), 25, 25));

		setText(TalkUtil.getDisplayName(camTalk.getJid()));
		return this;
	}
}
