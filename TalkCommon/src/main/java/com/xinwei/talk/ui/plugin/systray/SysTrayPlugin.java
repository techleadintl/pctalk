/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 下午1:49:54
 * 
 ***************************************************************/
package com.xinwei.talk.ui.plugin.systray;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPException;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.common.lookandfeel.component.McWillXPopupMenu;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.common.lookandfeel.util.ScreenWindowHide;
import com.xinwei.common.vo.Entry;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.listener.TrayListener;
import com.xinwei.talk.listener.UnreadListener;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.main.MainWindow;
import com.xinwei.talk.ui.plugin.Plugin;

public class SysTrayPlugin implements Plugin, ActionListener {
	private McWillXPopupMenu popupMenu = new McWillXPopupMenu();

	private JMenuItem openMenu;
	private JMenuItem minimizeMenu;
	private JMenuItem exitMenu;
	private JMenuItem reloginMenu;
	//private JMenuItem logoutMenu;

	private ImageIcon availableIcon;
	private ImageIcon loginingIcon;
	private ImageIcon offlineIcon;
	private TrayIcon trayIcon;

	private LinkedList<TalkMessage> messageList = new LinkedList<TalkMessage>();
	private Object lock = new Object();

	TrayMessageDialog trayMessageDialog;
	boolean stop;
	//	private boolean isAvatarFlash = true;
	boolean flag = false;
	private Image avatarImage;

	boolean logining = false;

	@Override
	public void initialize() {
		if (!SystemTray.isSupported()) {
			Log.error("Tray don't supports on this platform.");
			return;
		}
		openMenu = new JMenuItem(Res.getMessage("menuitem.tray.open"));
		minimizeMenu = new JMenuItem(Res.getMessage("menuitem.tray.hide"));
		exitMenu = new JMenuItem(Res.getMessage("menuitem.tray.exit"));
		reloginMenu = new JMenuItem(Res.getMessage("menuitem.tray.relogin"));
		//logoutMenu = new JMenuItem(Res.getMessage("menuitem.tray.logout"));

		SystemTray tray = SystemTray.getSystemTray();

		if (SwingUtil.isLinux()) {
			availableIcon = LAF.getImageIcon("talk/images/tray/tray-24x24.png");
			loginingIcon = LAF.getImageIcon("talk/images/tray/tray-24x24.gif");
		} else {
			availableIcon = LAF.getImageIcon("talk/images/tray/tray-16x16.png");
			loginingIcon = LAF.getImageIcon("talk/images/tray/tray-16x16.gif");
		}
		offlineIcon = ImageUtil.getGrayImageIcon(availableIcon);

		try {
			String trayTip = Res.getMessage("application.name");
			if (trayTip != null) {
				trayTip += ": " + TalkUtil.getDisplayName(TalkManager.getVCard().getJid());
			}
			trayIcon = new TrayIcon(availableIcon.getImage(), trayTip, null);
			trayIcon.setImageAutoSize(true);
			tray.add(trayIcon);
		} catch (Exception e) {
			// Not Supported
		}

		trayMessageDialog = new TrayMessageDialog(this);

		addListeners();

		startFlash();

	}

	private void startFlash() {
		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				while (!stop) {
					synchronized (lock) {
						updateTrayIcon();
					}
					SystemUtil.sleep(500);
				}
			}
		});

		TalkManager.getMessageManager();
	}

	public void updateTrayIcon() {
		synchronized (lock) {
			if (TalkManager.getUserConfig().getAvatarFlash() && avatarImage != null) {
				if (flag) {
					trayIcon.setImage(avatarImage);
				} else {
					trayIcon.setImage(LAF.getTransparent().getImage());
				}
				flag = !flag;
			} else {
				if (logining) {
					trayIcon.setImage(loginingIcon.getImage());
				} else {
					if (TalkManager.getConnection().isConnected()) {
						trayIcon.setImage(availableIcon.getImage());
					} else {
						trayIcon.setImage(offlineIcon.getImage());
					}
				}
			}
		}
	}

	//	public void setAvatarFlash() {
	//		synchronized (lock) {
	//			isAvatarFlash = !isAvatarFlash;
	//			//保存到数据库
	//			UserConfig userConfig = TalkManager.getUserConfig();
	//			userConfig.setAvatarFlash(isAvatarFlash);
	//			TalkManager.saveUserConfig();
	//
	//			//更新最新消息头像
	//			if (isAvatarFlash && !messageList.isEmpty()) {
	//				updateAvatarImage();
	//			}
	//			updateTrayIcon();
	//		}
	//	}

	public void updateAvatarImage() {
		Dimension size = SystemTray.getSystemTray().getTrayIconSize();
		TalkMessage first = messageList.getFirst();
		if (TalkUtil.isGroup(first.getJid())) {
			avatarImage = LAF.getGroupDefaultAvatarImage();
		} else {
			avatarImage = ImageManager.getAvatarImage(first.getJid(), size.width, size.height, true);
		}

		//		
	}

	//	public boolean isAvatarFlash() {
	//		return isAvatarFlash;
	//	}

	private void addListeners() {
		TalkManager.getConnection().addConnectionListener(new MyConnectionListener());

		ListenerManager.addUnreadListener(new MyUnreadListener());

		openMenu.addActionListener(this);
		minimizeMenu.addActionListener(this);
		exitMenu.addActionListener(this);
		reloginMenu.addActionListener(this);
		//logoutMenu.addActionListener(this);

		trayIcon.addMouseListener(new TrayMouseListener());
		trayIcon.addMouseMotionListener(new TrayMouseMotionListener());

		ListenerManager.addTrayListener(new TrayListener() {
			@Override
			public void login() {
				reconnect();
			}
		});
	}

	private void openMainWindow(final MainWindow mainWindow) {
		mainWindow.setVisible(true);
		mainWindow.bringFrameIntoFocus();
	}

	@Override
	public void shutdown() {
		stop = true;
		if (SystemTray.isSupported()) {
			SystemTray tray = SystemTray.getSystemTray();
			tray.remove(trayIcon);
		}
	}

	@Override
	public boolean canShutDown() {
		return true;
	}

	@Override
	public void uninstall() {
	}

	protected void reconnect() {
		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				logining = true;
				updateTrayIcon();
				try {
					TalkManager.getConnection().connect();
				} catch (XMPPException e1) {
					e1.printStackTrace();
				}
				logining = false;
				updateTrayIcon();
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		final MainWindow mainWindow = TalkManager.getMainWindow();
		if (source == openMenu) {
			openMainWindow(mainWindow);
		} else if (source == minimizeMenu) {
			mainWindow.setVisible(false);
		} else if (source == reloginMenu) {
			ListenerManager.fireTrayLogin();
			mainWindow.logout(true);
		}else if (source == exitMenu) {
			mainWindow.shutdown();
		}
	}

	class MyUnreadListener implements UnreadListener {
		public void doUnreadMessage(String jid, Entry<Integer, TalkMessage> unreadMessage) {
			if (jid == null)
				return;
			removeTranscriptMessage(jid);
			if (unreadMessage == null) {
				if (messageList.isEmpty()) {
					avatarImage = null;
					//使用默认头像
					updateTrayIcon();
				} else {
					//更新最新消息头像
					updateAvatarImage();
				}
			} else {
				messageList.add(unreadMessage.getValue());
				Collections.sort(messageList, new Comparator<TalkMessage>() {
					@Override
					public int compare(TalkMessage o1, TalkMessage o2) {
						return o1.getTalkBody().getTime().compareTo(o2.getTalkBody().getTime());
					}
				});
				//更新最新消息头像
				updateAvatarImage();
			}
		}

		private TalkMessage removeTranscriptMessage(String jid) {
			//将最新的接到消息的jid放在第一个位置
			TalkMessage message = null;
			for (TalkMessage tm : messageList) {
				if (jid.equals(tm.getJid())) {
					message = tm;
					break;
				}
			}
			if (message != null) {
				messageList.remove(message);
			}
			return message;
		}

	}

	class MyConnectionListener implements ConnectionListener {

		@Override
		public void connectionClosed() {
			updateTrayIcon();
		}

		@Override
		public void connectionClosedOnError(Exception arg0) {
			updateTrayIcon();
		}

		@Override
		public void reconnectingIn(int arg0) {
			updateTrayIcon();
		}

		@Override
		public void reconnectionSuccessful() {
			updateTrayIcon();
		}

		@Override
		public void reconnectionFailed(Exception arg0) {
			updateTrayIcon();
		}
	}

	class TrayMouseListener extends MouseAdapter {
		final MainWindow mainWindow = TalkManager.getMainWindow();

		@Override
		public void mouseClicked(MouseEvent event) {
			// if we using single click on tray icon
			if (event.getButton() == MouseEvent.BUTTON1) {
				if (event.getClickCount() == 1 && !messageList.isEmpty()) {
					if (TalkManager.getUserConfig().getAvatarFlash()) {
						trayMessageDialog.activateFirst();
					}
				} else {
					ScreenWindowHide screenWindowHide = TalkManager.getScreenWindowHide();
					if (screenWindowHide != null) {
						screenWindowHide.setTray();
					}
					openMainWindow(mainWindow);
					//					if (!mainWindow.isVisible() || !mainWindow.isShowing()) {
					//						openMainWindow(mainWindow);
					//					} else {
					//						mainWindow.setVisible(false);
					//					}
				}
			} else if (event.getButton() == MouseEvent.BUTTON3) {
				popupMenu.clear();
				if (!TalkManager.getConnection().isConnected()) {
					popupMenu.add(reloginMenu);
				}
				popupMenu.add(openMenu);
				popupMenu.add(minimizeMenu);
				popupMenu.add(reloginMenu);
				// Exit Menu
				if (!Res.getBoolean("disable.exit")) {
					popupMenu.add(exitMenu);
				}
				popupMenu.showMouseXY();
			}
		}

		@Override
		public void mousePressed(MouseEvent event) {
			// on Mac i would want the window to show when i left-click the Icon
			if (SwingUtil.isMac() && event.getButton() != MouseEvent.BUTTON3) {
				mainWindow.setVisible(false);
				mainWindow.setVisible(true);
				mainWindow.requestFocusInWindow();
				//							mainWindow.bringFrameIntoFocus();
				mainWindow.toFront();
				mainWindow.requestFocus();
			}
		}
	}

	class TrayMouseMotionListener extends MouseMotionAdapter {
		int x1, x2;
		int y1, y2;
		boolean isIn = false;

		public TrayMouseMotionListener() {
			final Toolkit defaultToolkit = java.awt.Toolkit.getDefaultToolkit();
			Dimension trayIconSize = SystemTray.getSystemTray().getTrayIconSize();
			final int w = trayIconSize.width;
			final int h = trayIconSize.height;
			final Rectangle rec = new Rectangle();

			TaskEngine.getInstance().submit(new Runnable() {
				@Override
				public void run() {
					while (!stop) {
						if (isIn) {
							doIn(defaultToolkit, w, h, rec);
						}
						SystemUtil.sleep(250);
					}
				}
			});
		}

		private void doIn(final Toolkit defaultToolkit, final int w, final int h, final Rectangle rec) {
			/** 获取屏幕的边界 */
			Insets screenInsets = defaultToolkit.getScreenInsets(trayMessageDialog.getGraphicsConfiguration());

			/** 获取底部任务栏高度 */
			if (screenInsets.bottom > 0) {
				rec.x = (x2 + x1) / 2 - w;
				rec.y = (y2 + y1) / 2 - h - screenInsets.bottom;
				rec.width = 2 * w;
				rec.height = 2 * h + screenInsets.bottom;
			} else if (screenInsets.right > 0) {
				rec.x = (x2 + x1) / 2 - w - screenInsets.right;
				rec.y = (y2 + y1) / 2 - h;
				rec.width = 2 * w + screenInsets.right;
				rec.height = 2 * h;
			} else if (screenInsets.left > 0) {
				rec.x = (x2 + x1) / 2 - w;
				rec.y = (y2 + y1) / 2 - h;
				rec.width = 2 * w + screenInsets.left;
				rec.height = 2 * h;
			} else if (screenInsets.top > 0) {
				rec.x = (x2 + x1) / 2 - w;
				rec.y = (y2 + y1) / 2 - h;
				rec.width = 2 * w;
				rec.height = 2 * h + screenInsets.top;
			}
			Point location = MouseInfo.getPointerInfo().getLocation();
			if (!rec.contains(location)) {
				Rectangle bounds = trayMessageDialog.getBounds();
				if (!bounds.contains(location)) {
					isIn = false;
					mouseExited(null);
				}
			}
		}

		public void mouseMoved(MouseEvent e) {
			int x = e.getXOnScreen();
			int y = e.getYOnScreen();
			if (isIn) {
				if (x < x1) {
					x1 = x;
				}
				if (x > x2) {
					x2 = x;
				}
				if (y < y1) {
					y1 = y;
				}
				if (y > y2) {
					y2 = y;
				}
			} else {
				isIn = true;
				x1 = x2 = x;
				y1 = y2 = y;
				mouseEntered((x1 + x2) / 2, (y1 + y2) / 2);
			}
		}

		public void mouseEntered(int X, int Y) {
			trayMessageDialog.setVisible(true);

			Toolkit defaultToolkit = java.awt.Toolkit.getDefaultToolkit();
			Dimension screenSize = defaultToolkit.getScreenSize();
			int screenWidth = screenSize.width;
			int screenHeight = screenSize.height;

			int dialogWidth = trayMessageDialog.getWidth();
			int dialogHeight = trayMessageDialog.getHeight();

			/** 获取屏幕的边界 */
			Insets screenInsets = defaultToolkit.getScreenInsets(trayMessageDialog.getGraphicsConfiguration());
			int x = 0;
			int y = 0;

			if (screenInsets.bottom > 0) {
				x = X - dialogWidth / 2;
				y = screenHeight - screenInsets.bottom - dialogHeight;
				int x1 = screenWidth - screenInsets.right - 2 - dialogWidth;
				if (x > x1) {
					x = x1;
				}
			} else if (screenInsets.right > 0) {
				x = screenWidth - screenInsets.right - dialogWidth;
				y = Y - dialogHeight / 2;
				int y1 = screenHeight - screenInsets.bottom - 2 - dialogHeight;
				if (y < y1) {
					y = y1;
				}
			} else if (screenInsets.left > 0) {
				x = screenInsets.left;
				y = Y - dialogHeight / 2;
				int y1 = screenHeight - screenInsets.bottom - 2 - dialogHeight;
				if (y < y1) {
					y = y1;
				}
			} else if (screenInsets.top > 0) {
				x = X - dialogWidth / 2;
				y = screenInsets.top;
				int x1 = screenWidth - screenInsets.right - 2 - dialogWidth;
				if (x > x1) {
					x = x1;
				}
			}

			trayMessageDialog.setLocation(x, y);

		}

		public void mouseExited(MouseEvent e) {
			trayMessageDialog.setVisible(false);
		}
	}

}
