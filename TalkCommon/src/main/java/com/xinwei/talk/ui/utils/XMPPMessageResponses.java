package com.xinwei.talk.ui.utils;

/**
 * This class is contains, all possible message response types, that received
 * from XMPP connection (chat messages, add/remove/update groups and users)
 * 
 * @author nisal
 **/
public enum XMPPMessageResponses {

	XMPP_MESSAGE_RES_CODE_GROUP_ADDED(1004);

	private int value;

	XMPPMessageResponses(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}

}
