package com.xinwei.talk.ui.chat.room.file.bak;
//package com.xinwei.talk.ui.plugin.global.file.bak;
//
//import java.awt.Color;
//import java.awt.Graphics;
//import java.awt.Graphics2D;
//import java.awt.GridBagConstraints;
//import java.awt.GridBagLayout;
//import java.awt.Insets;
//import java.awt.Rectangle;
//import java.awt.RenderingHints;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.awt.event.MouseListener;
//import java.io.File;
//
//import javax.swing.Icon;
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//
//import com.xinwei.common.lang.SwingUtil;
//import com.xinwei.common.lookandfeel.McWillBorders;
//import com.xinwei.common.lookandfeel.component.McWillTextPane;
//import com.xinwei.talk.common.LAF;
//import com.xinwei.talk.common.Res;
//import com.xinwei.talk.manager.TalkManager;
//import com.xinwei.talk.manager.TransferManager;
//import com.xinwei.talk.service.file.OutFileTransfer;
//import com.xinwei.talk.ui.chat.room.CommonChatRoom;
//import com.xinwei.talk.ui.util.component.label.LinkLabel;
//
//public class SendFileCancelUI extends JPanel {
//	private static final long serialVersionUID = -4403839897649365671L;
//
//	int FILE_NAME_MAX_LENGTH = 200;
//
//	McWillTextPane infoLabel;
//
//	LinkLabel resendButton;
//
//	private File fileToSend;
//
//	private CommonChatRoom room;
//
//	private MouseListener resendButtonListener;
//
//	private String url;
//
//	private boolean isHistory = false;
//
//	public SendFileCancelUI(String url, final File file, final CommonChatRoom room, boolean isHistory) {
//		this.fileToSend = file;
//		this.url = url;
//		this.room = room;
//		this.isHistory = isHistory;
//		setLayout(new GridBagLayout());
//
//		final JPanel panel = new JPanel();
//		panel.setBorder(McWillBorders.getLabelBorder());
//
//		panel.setLayout(new GridBagLayout());
//
//		infoLabel = new McWillTextPane();
//		infoLabel.setEditable(false);
//
//		String fileName = file.getName();
//
//		String text = SwingUtil.format(file.length());
//		fileName += " (" + text + ")";
//		infoLabel.setText(Res.getMessage("label.chatroom.fileupload.cancel", fileName));
//
//		if (!isHistory) {
//			resendButton = new LinkLabel(Res.getMessage("button.resend"), Color.blue, Color.red);
//			resendButton.setAlignmentY(0.75f);
//			resendButtonListener = new MouseAdapter() {
//				public void mouseClicked(MouseEvent e) {
//					resendFile(fileToSend);
//				}
//			};
//			resendButton.addMouseListener(resendButtonListener);
//
//			infoLabel.insertComponent(resendButton);
//		}
//
//		infoLabel.requestFocus();
//
//		infoLabel.updateUI();
//
//		panel.add(new JLabel(LAF.getDeleteIcon()[1]), new GridBagConstraints(0, 0, 1, 1, 0.0D, 0.0D, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
//		panel.add(infoLabel, new GridBagConstraints(1, 0, 2, 1, 1.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
//
//		add(panel, new GridBagConstraints(0, 0, 1, 1, 1.0D, 0.0D, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 45, 10, 45), 0, 0));
//	}
//
//	private void resendFile(File file) {
//		resendButton.setEnabled(false);
//		resendButton.removeMouseListener(resendButtonListener);
//		TransferManager transferManager = TalkManager.getTransferManager();
//		transferManager.sendFile(file, room.getRoomId());
//	}
//
//}
