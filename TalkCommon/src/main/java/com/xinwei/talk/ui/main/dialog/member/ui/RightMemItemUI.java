package com.xinwei.talk.ui.main.dialog.member.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jivesoftware.smack.util.StringUtils;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillButton;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.ui.main.dialog.member.model.MemberInfo;

public class RightMemItemUI extends JPanel {
	private static final long serialVersionUID = 1514044406550293152L;

	private JButton removeButton;

	//组
	private MemberInfo memberInfo;

	//显示名称 文字标签
	protected JLabel nameTxtLabel;

	//显示称 文字标签
	protected JLabel telLabel;

	//头像 图标标签
	protected JLabel avatarIconLabel;

	public RightMemItemUI(MemberInfo memberInfo) {
		this.memberInfo = memberInfo;

		setOpaque(true);

		setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 15));

		setLayout(new BorderLayout());

		JPanel leftPanel = new JPanel(new GridBagLayout());
		leftPanel.setPreferredSize(new Dimension(265, 55));
		avatarIconLabel = new McWillRolloverLabel();

		nameTxtLabel = new JLabel();
		nameTxtLabel.setHorizontalTextPosition(JLabel.LEFT);
		nameTxtLabel.setHorizontalAlignment(JLabel.LEFT);

		telLabel = new JLabel();
		telLabel.setHorizontalTextPosition(JLabel.LEFT);
		telLabel.setHorizontalAlignment(JLabel.LEFT);

		leftPanel.add(avatarIconLabel, new GridBagConstraints(0, 0, 1, 2, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 10, 5, 0), 0, 0));
		leftPanel.add(nameTxtLabel, new GridBagConstraints(1, 0, 1, 1, 1.0D, 0.5D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));
		leftPanel.add(telLabel, new GridBagConstraints(1, 1, 1, 1, 1.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));

		JPanel buttonPanel=new JPanel(new VerticalFlowLayout(VerticalFlowLayout.CENTER));
		removeButton = new McWillIconButton(LAF.getDeleteIcon(),true);
		
		buttonPanel.add(removeButton);

		add(leftPanel, BorderLayout.CENTER);

		add(buttonPanel, BorderLayout.EAST);

		ImageIcon avatar = ImageManager.getTalkAvatar(memberInfo.jid, 45, 45);
		avatarIconLabel.setIcon(avatar);

		setDisplayName();

		removeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onRemoveClick();
			}
		});

		setBackground(McWillTheme.getThemeColor().getLightColor());
	}

	public void onRemoveClick() {

	}

	public void addCheckListener(PropertyChangeListener listener) {
		this.addPropertyChangeListener(listener);
	}

	public String getJID() {
		return memberInfo.jid;
	}

	protected void setDisplayName() {
		String displayName = getDisplayName();

		nameTxtLabel.setText(displayName);

		nameTxtLabel.setToolTipText(displayName);
		String displayTel = getDisplayTel();
		telLabel.setForeground(Color.GRAY);
		telLabel.setText(displayTel);
		telLabel.setToolTipText(displayTel);
	}

	public String getDisplayName() {
		final String displayName = memberInfo.displayName;
		return StringUtils.unescapeNode(displayName);
	}

	public String getDisplayTel() {
		final String fullJid = memberInfo.jid;
		return TalkUtil.getUserTel(fullJid);
	}
}
