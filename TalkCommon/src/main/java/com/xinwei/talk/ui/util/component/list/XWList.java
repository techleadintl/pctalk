/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月20日 上午10:54:54
 * 
 ***************************************************************/
package com.xinwei.talk.ui.util.component.list;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.McWillBorders;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;

public class XWList<T extends Component> extends JPanel {
	Component hoverComponent;

	Component selectedComponent;

	public XWList() {
		setLayout(new VerticalFlowLayout(VerticalFlowLayout.TOP, 0, 0, true, false));
		setOpaque(false);
	}

	@Override
	public Component add(Component comp) {
		addComponentListener(comp);
		return super.add(comp);
	}

	@Override
	public void add(Component comp, Object constraints) {
		addComponentListener(comp);
		super.add(comp, constraints);
	}

	@Override
	public Component add(Component comp, int index) {
		addComponentListener(comp);
		return super.add(comp, index);
	}

	@Override
	public void add(Component comp, Object constraints, int index) {
		super.add(comp, constraints, index);
		addComponentListener(comp);
	}

	private void addComponentListener(final Component comp) {
		MouseAdapter listener = new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				setSelected(comp);
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				if (hoverComponent == comp)
					return;
				setHover(comp);
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				if (hoverComponent == comp)
					return;
				setHover(comp);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				Dimension size = comp.getPreferredSize();
				if (e.getY() <= 0 || e.getY() >= size.height - 2 || e.getX() <= 0 || e.getX() >= size.width - 2) {
					setHover(null);
				}
			}
		};

		comp.addMouseListener(listener);
		comp.addMouseMotionListener(listener);

	}

	public void setSelected(Component source) {
		selectedComponent = source;

		Component[] components = getComponents();
		for (Component component : components) {
			if (component == source) {
				component.setBackground(McWillTheme.getThemeColor().getSubDarkColor());
//				((WorkspacePane) component).setBorder(McWillBorders.getLabelBorder());
			} else {
				component.setBackground(McWillTheme.getThemeColor().getLightColor());
//				((WorkspacePane) component).setBorder(null);
			}
		}
	}

	public void setHover(Component source) {
		hoverComponent = source;
		Component[] components = getComponents();
		for (Component component : components) {
			boolean selected = isSelected(component);
			if (selected) {
				continue;
			}
			if (component == hoverComponent) {
				component.setBackground(McWillTheme.getThemeColor().getMiddleColor());
			} else {
				component.setBackground(McWillTheme.getThemeColor().getLightColor());
			}
		}
	}

	public Component getSelected() {
		return selectedComponent;
	}

	public void clearSelected() {
		setSelected(null);
	}

	public boolean isSelected(Component component) {
		return selectedComponent == component;
	}

	public boolean isHover(Component component) {
		return hoverComponent == component;
	}

	public List<T> getElementList() {
		List<T> asList = new ArrayList<T>();
		Component[] components = getComponents();
		for (Component component : components) {
			asList.add((T) component);
		}
		return asList;
	}

}
