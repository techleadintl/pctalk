package com.xinwei.talk.ui.main.dialog.member.model;

import java.util.Comparator;

//排序  
public class CaptionSort implements Comparator<String> {
	@Override
	public int compare(String o1, String o2) {
		if (o1.equals("#"))
			return 1;
		if (o2.equals("#")) {
			return -1;
		}
		return o1.compareTo(o2);
	}
}