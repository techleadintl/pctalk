package com.xinwei.talk.ui.main.status;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.MessageFormat;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Contact;
import com.xinwei.talk.model.VCard;

public class MyInfoDialog extends JFrame {
	final VCard vcard;
	JPanel avatarPanel;

	public MyInfoDialog() {
		vcard = TalkManager.getVCard();
		setIconImages(LAF.getApplicationImages());

		getContentPane().setLayout(new BorderLayout());

		createDialog();

		//		JRootPane rootPane = getRootPane();
		//		((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

		setSize(new Dimension(350, 385));

		SwingUtil.centerWindowOnScreen(this);
	}

	protected void createDialog() {

		setLayout(new BorderLayout());

		add(createDialogContent(), BorderLayout.CENTER);

		pack();
	}

	protected JPanel createDialogContainer() {
		return new McWillContentPanel() {
			public void paintComponent(Graphics g) {
				LAF.paintMainAddGroupDialog(this, (Graphics2D) g);
				super.paintComponent(g);
			}
		};
	}

	protected JPanel createDialogContent() {
		String title = Res.getMessage("dialog.title.namecard.talk");
		JPanel dialogPanel = new JPanel(new BorderLayout());
		//		XWDialogTitle titlePanel = new XWDialogTitle(this, title);
		//		titlePanel.setBorder(BorderFactory.createEmptyBorder(6, 10, 6, 0));
		//		dialogPanel.add(titlePanel, BorderLayout.NORTH);
		avatarPanel = new JPanel(new BorderLayout()) {
			protected void paintComponent(Graphics g) {
				ImageIcon talkAvatar = ImageManager.getTalkAvatar(vcard.getJid(), 50, 50, true);
				g.drawImage(talkAvatar.getImage(), (getWidth() - 60) / 2, (getHeight() - 60) / 2, 50, 50, this);
				super.paintComponent(g);
			}
		};
		avatarPanel.setPreferredSize(new Dimension(0, 80));
		//		avatarPanel.add(label,BorderLayout.CENTER);
		dialogPanel.add(avatarPanel, BorderLayout.NORTH);

		dialogPanel.add(createInputPanel(), BorderLayout.CENTER);

		setTitle(title);

		avatarPanel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent mouseEvent) {
				if (mouseEvent.getClickCount() == 1) {
					byte[] talkAvatar = ImageManager.getTalkAvatar(vcard.getJid(), true);
					AvatarDialog dialog = new AvatarDialog(TalkManager.getMainWindow(), talkAvatar);
					dialog.setVisible(true);
					SwingUtil.centerWindowOnScreen(dialog);
				}
			}
		});

		return dialogPanel;
	}

	public void updateAvatar() {
		avatarPanel.repaint();
	}

	private Component createInputPanel() {
		JTextPane panel = new JTextPane();
		panel.setEditable(false);
		panel.setContentType("text/html");

		String table = "<html>" + "<table>" //
				+ "<tr><td>{0}</td><td>{1}</td></tr>" //
				+ "<tr><td>{2}</td><td>{3}</td></tr>" //
				+ "<tr><td>{4}</td><td>{5}</td></tr>" //
				+ "<tr><td>{6}</td><td>{7}</td></tr>"//
				+ "<tr><td>{8}</td><td>{9}</td></tr>" //
				+ "<tr><td>{10}</td><td>{11}</td></tr>" //
				+ "<tr><td>{12}</td><td>{13}</td></tr>" //
				+ "<tr><td>{14}</td><td>{15}</td></tr>"//
				+ "<tr><td>{16}</td><td>{17}</td></tr>" //
				+ "<tr><td>{18}</td><td>{19}</td></tr>" //
				+ "</table>" + "</html>";

		String signature = vcard.getSignature();
		String firstName = null;
		String lastName = null;
		String emaill = null;
		String addr = null;
		String birthday = null;
		String remark = null;
		Contact contact = TalkManager.getLocalManager().getContactByTel(vcard.getTel());
		if (contact != null) {
			firstName = contact.getFirstName();
			lastName = contact.getLastName();
			emaill = contact.getEmaill();
			addr = contact.getAddr();
			birthday = contact.getBirthday();
			remark = contact.getRemark();
		}
		String html = MessageFormat.format(table, //
				Res.getMessage("namecard.display"), TalkUtil.getDisplayName(vcard.getJid()), //
				Res.getMessage("namecard.nick"), getString(vcard.getNick()), //
				Res.getMessage("namecard.tel"), getTel(vcard.getTel()), //
				Res.getMessage("namecard.signature"), getString(signature), //
				Res.getMessage("namecard.firstname"), getString(firstName), //
				Res.getMessage("namecard.lastname"), getString(lastName), //
				Res.getMessage("namecard.emaill"), getString(emaill), //
				Res.getMessage("namecard.addr"), getString(addr), //
				Res.getMessage("namecard.birthday"), getString(birthday), //
				Res.getMessage("namecard.note"), getString(remark));//

		panel.setText(html);
		return panel;
	}

	public String getString(String str) {
		if (str == null)
			return "";
		return str;
	}

	public String getTel(String tel) {
		String[] countryCodeAndTel = TalkUtil.getCountryCodeAndTel(tel);
		if (countryCodeAndTel == null)
			return getString(tel);
		return countryCodeAndTel[0] + " " + countryCodeAndTel[1];
	}

	protected LayoutManager getButtonBarLayout() {
		return new FlowLayout(FlowLayout.RIGHT);
	}

	/**
	 * Brings the ChatFrame into focus on the desktop.
	 */
	public void bringFrameIntoFocus() {
		if (!isVisible()) {
			setVisible(true);
		}

		if (getState() == Frame.ICONIFIED) {
			setState(Frame.NORMAL);
		}

		toFront();
		requestFocus();
	}

	/**
	 * set if the chatFrame should always stay on top
	 * 
	 * @param active
	 */
	public void setWindowAlwaysOnTop(boolean active) {
		setAlwaysOnTop(active);
	}
}
