package com.xinwei.talk.ui.chat.room.file.bak;
//package com.xinwei.talk.ui.plugin.global.file.bak;
//
//import java.awt.Color;
//import java.awt.Cursor;
//import java.awt.Dimension;
//import java.awt.GridBagConstraints;
//import java.awt.GridBagLayout;
//import java.awt.Insets;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.io.File;
//
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//
//import com.xinwei.common.lang.SwingUtil;
//import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
//import com.xinwei.common.lookandfeel.component.McWillScrollPaneLabel;
//import com.xinwei.common.lookandfeel.component.McWillTextPane;
//import com.xinwei.spark.ui.MessageDialog;
//import com.xinwei.talk.common.Res;
//import com.xinwei.talk.manager.TalkManager;
//import com.xinwei.talk.ui.util.component.label.LinkLabel;
//
//public class SendFileCompleteUI2 extends JPanel {
//	private static final long serialVersionUID = -4403839897649365671L;
//
//	int FILE_NAME_MAX_LENGTH = 200;
//	McWillRolloverLabel avatarIconLabel;
//
//	McWillScrollPaneLabel fileNameLabel;
//	McWillTextPane infoLabel;
//
//	LinkLabel openLabel;
//	LinkLabel openDirLabel;
//	LinkLabel forwardLabel;
//
//	private File file;
//
//	public SendFileCompleteUI2(final File file) {
//		this.file = file;
//		setLayout(new GridBagLayout());
//
//		avatarIconLabel = new McWillRolloverLabel();
//
//		fileNameLabel = new McWillScrollPaneLabel();
//		fileNameLabel.setHorizontalTextPosition(JLabel.LEFT);
//		fileNameLabel.setHorizontalAlignment(JLabel.LEFT);
//
//		infoLabel = new McWillTextPane();
////		infoLabel.setHorizontalTextPosition(JLabel.LEFT);
////		infoLabel.setHorizontalAlignment(JLabel.LEFT);
//
//		openLabel = new LinkLabel(Res.getMessage("button.open"), Color.blue, Color.red);
//		openDirLabel = new LinkLabel(Res.getMessage("button.opendir"), Color.blue, Color.red);
//		forwardLabel = new LinkLabel(Res.getMessage("button.forward"), Color.blue, Color.red);
//
//		add(avatarIconLabel, new GridBagConstraints(0, 0, 1, 2, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));
//		add(fileNameLabel, new GridBagConstraints(1, 0, 2, 1, 1.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
//		add(infoLabel, new GridBagConstraints(1, 1, 2, 1, 1.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));
//
//		//		add(new JSeparator(), new GridBagConstraints(0, 2, 3, 1, 1.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
//
//		add(openLabel, new GridBagConstraints(0, 3, 1, 1, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
//		add(openDirLabel, new GridBagConstraints(1, 3, 1, 1, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
//		add(forwardLabel, new GridBagConstraints(2, 3, 1, 1, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
//
//		avatarIconLabel.setIcon(SwingUtil.getFileImageIcon(file));
//
//		String fileName = file.getName();
//		fileNameLabel.setToolTipText(fileName);
//
//		String text = SwingUtil.format(file.length());
//		fileName = SwingUtil.getString(fileName, fileNameLabel.getFont(), FILE_NAME_MAX_LENGTH, " (" + text + ")");
//		fileNameLabel.setText(fileName);
//
//		infoLabel.setText(Res.getMessage("label.chatroom.fileupload.complete"));
//
//		addListeners();
//		
//		setPreferredSize(new Dimension(250, 100));
//
//	}
//
//	private void addListeners() {
//
//		avatarIconLabel.addMouseListener(new MouseAdapter() {
//			public void mouseClicked(MouseEvent e) {
//				if (e.getClickCount() == 2) {
//					SwingUtil.openFile(file);
//				}
//			}
//
//			public void mouseEntered(MouseEvent e) {
//				avatarIconLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
//
//			}
//
//			public void mouseExited(MouseEvent e) {
//				avatarIconLabel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
//			}
//		});
//		fileNameLabel.addMouseListener(new MouseAdapter() {
//			public void mouseClicked(MouseEvent e) {
//				if (e.getClickCount() == 2) {
//					SwingUtil.openFile(file);
//				}
//			}
//
//			public void mouseEntered(MouseEvent e) {
//				fileNameLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
//
//			}
//
//			public void mouseExited(MouseEvent e) {
//				fileNameLabel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
//			}
//		});
//
//		openLabel.addMouseListener(new MouseAdapter() {
//			public void mouseClicked(MouseEvent e) {
//				boolean exists = file.exists();
//				if (!exists) {
//					MessageDialog.showAlert(TalkManager.getChatFrame(), Res.getMessage("message.file.exists.not", file.getAbsolutePath()));
//					return;
//				}
//				SwingUtil.openFile(file);
//			}
//		});
//		openDirLabel.addMouseListener(new MouseAdapter() {
//			public void mouseClicked(MouseEvent e) {
//				boolean exists = file.getParentFile().exists();
//				if (!exists) {
//					MessageDialog.showAlert(TalkManager.getChatFrame(), Res.getMessage("message.file.exists.not", file.getParentFile()));
//					return;
//				}
//				SwingUtil.openFile(file.getParentFile());
//			}
//		});
//	}
//}
