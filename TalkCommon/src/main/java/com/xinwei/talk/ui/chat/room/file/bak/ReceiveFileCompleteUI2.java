package com.xinwei.talk.ui.chat.room.file.bak;
//package com.xinwei.talk.ui.plugin.global.file.bak;
//
//import java.awt.Color;
//import java.awt.Component;
//import java.awt.Cursor;
//import java.awt.Dimension;
//import java.awt.GridBagConstraints;
//import java.awt.GridBagLayout;
//import java.awt.Insets;
//import java.awt.event.ActionEvent;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.io.File;
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.Date;
//
//import javax.swing.AbstractAction;
//import javax.swing.Action;
//import javax.swing.Icon;
//import javax.swing.ImageIcon;
//import javax.swing.JFileChooser;
//import javax.swing.JLabel;
//import javax.swing.JOptionPane;
//import javax.swing.JPanel;
//import javax.swing.JPopupMenu;
//
//import com.xinwei.common.lang.SwingUtil;
//import com.xinwei.common.lang.URLFileSystem;
//import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
//import com.xinwei.common.lookandfeel.component.McWillTextPane;
//import com.xinwei.spark.GraphicUtils;
//import com.xinwei.talk.common.LAF;
//import com.xinwei.talk.common.Log;
//import com.xinwei.talk.common.Res;
//import com.xinwei.talk.ui.chat.Downloads;
//import com.xinwei.talk.ui.util.component.label.LinkLabel;
//
//public class ReceiveFileCompleteUI2 extends JPanel {
//	private static final long serialVersionUID = -4403839897649365671L;
//
//	int FILE_NAME_MAX_LENGTH = 200;
//	McWillRolloverLabel avatarIconLabel;
//
//	JLabel fileNameLabel;
//	McWillTextPane infoLabel;
//
//	LinkLabel openLabel;
//	LinkLabel openDirLabel;
//	LinkLabel forwardLabel;
//
//	private File downloadedFile;
//
//	public ReceiveFileCompleteUI2(final File downloadedFile, Date time) {
//		this.downloadedFile = downloadedFile;
//		setLayout(new GridBagLayout());
//
//		Icon fileImageIcon = null;
//		if (SwingUtil.isImage(downloadedFile.getName())) {
//			try {
//				URL imageURL = downloadedFile.toURI().toURL();
//				ImageIcon image = new ImageIcon(imageURL);
//				fileImageIcon = GraphicUtils.scaleImageIcon(image, 32, 32);
//			} catch (MalformedURLException e) {
//				Log.error("Could not locate image.", e);
//			}
//		} else {
//			fileImageIcon = SwingUtil.getFileImageIcon(downloadedFile);
//		}
//		if (fileImageIcon == null) {
//			fileImageIcon = LAF.getDocumentInfoImageIcon();
//		}
//
//		avatarIconLabel = new McWillRolloverLabel();
//
//		fileNameLabel = new JLabel();
//		fileNameLabel.setHorizontalTextPosition(JLabel.LEFT);
//		fileNameLabel.setHorizontalAlignment(JLabel.LEFT);
//
//		infoLabel = new McWillTextPane();
//		infoLabel.setEditable(false);
//		//		timeLabel.setAutoscrolls(true);
//
//		openLabel = new LinkLabel(Res.getMessage("button.open"), Color.blue, Color.red);
//		openDirLabel = new LinkLabel(Res.getMessage("button.opendir"), Color.blue, Color.red);
//		forwardLabel = new LinkLabel(Res.getMessage("button.forward"), Color.blue, Color.red);
//
//		add(avatarIconLabel, new GridBagConstraints(0, 0, 1, 2, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));
//		add(fileNameLabel, new GridBagConstraints(1, 0, 2, 1, 1.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
//		add(infoLabel, new GridBagConstraints(1, 1, 2, 1, 1.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));
//
//		add(openLabel, new GridBagConstraints(0, 3, 1, 1, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
//		add(openDirLabel, new GridBagConstraints(1, 3, 1, 1, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
//		add(forwardLabel, new GridBagConstraints(2, 3, 1, 1, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
//
//		avatarIconLabel.setIcon(fileImageIcon);
//
//		String fileName = downloadedFile.getName();
//		fileNameLabel.setToolTipText(fileName);
//
//		String text = SwingUtil.format(downloadedFile.length());
//		fileName = SwingUtil.getString(fileName, fileNameLabel.getFont(), FILE_NAME_MAX_LENGTH, " (" + text + ")");
//		fileNameLabel.setText(fileName);
//
//		infoLabel.setText(Res.getMessage("label.chatroom.filedownload.complete", downloadedFile.getAbsolutePath()));
//		infoLabel.setToolTipText(downloadedFile.getAbsolutePath());
//		addListeners();
//		
//		setPreferredSize(new Dimension(250, 100));
//	}
//
//	private void addListeners() {
//		avatarIconLabel.addMouseListener(new MouseAdapter() {
//			public void mouseClicked(MouseEvent e) {
//				if (e.getClickCount() == 2) {
//					SwingUtil.openFile(downloadedFile);
//				}
//			}
//
//			public void mouseEntered(MouseEvent e) {
//				avatarIconLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
//
//			}
//
//			public void mouseExited(MouseEvent e) {
//				avatarIconLabel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
//			}
//
//			public void mousePressed(MouseEvent e) {
//				showPopup(e, downloadedFile);
//			}
//
//			public void mouseReleased(MouseEvent e) {
//				showPopup(e, downloadedFile);
//			}
//		});
//		fileNameLabel.addMouseListener(new MouseAdapter() {
//			public void mouseClicked(MouseEvent e) {
//				if (e.getClickCount() == 2) {
//					SwingUtil.openFile(downloadedFile);
//				}
//			}
//
//			public void mouseEntered(MouseEvent e) {
//				fileNameLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
//
//			}
//
//			public void mouseExited(MouseEvent e) {
//				fileNameLabel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
//			}
//		});
//
//		openLabel.addMouseListener(new MouseAdapter() {
//			public void mouseClicked(MouseEvent e) {
//				SwingUtil.openFile(downloadedFile);
//			}
//		});
//		openDirLabel.addMouseListener(new MouseAdapter() {
//			public void mouseClicked(MouseEvent e) {
//				SwingUtil.openFile(downloadedFile.getParentFile());
//			}
//		});
//	}
//
//	private void showPopup(MouseEvent e, final File downloadedFile) {
//		if (e.isPopupTrigger()) {
//			final JPopupMenu popup = new JPopupMenu();
//
//			final Component ui = this;
//			Action saveAsAction = new AbstractAction() {
//				private static final long serialVersionUID = -3010501340128285438L;
//
//				public void actionPerformed(ActionEvent e) {
//					final JFileChooser chooser = Downloads.getFileChooser();
//					File selectedFile = chooser.getSelectedFile();
//					if (selectedFile != null) {
//						selectedFile = new File(selectedFile.getParent(), downloadedFile.getName());
//					} else {
//						selectedFile = downloadedFile;
//					}
//					chooser.setSelectedFile(selectedFile);
//
//					int ok = chooser.showSaveDialog(ui);
//					if (ok == JFileChooser.APPROVE_OPTION) {
//						File file = chooser.getSelectedFile();
//						try {
//							if (file.exists()) {
//								int confirm = JOptionPane.showConfirmDialog(ui, Res.getMessage("message.file.exists.question"), Res.getString("tabTitle.file.exists"), JOptionPane.YES_NO_OPTION,
//										JOptionPane.QUESTION_MESSAGE);
//								if (confirm == JOptionPane.NO_OPTION) {
//									return;
//								}
//							}
//							URLFileSystem.copy(downloadedFile.toURI().toURL(), file);
//						} catch (IOException e1) {
//							Log.error(e1);
//						}
//					}
//				}
//			};
//
//			saveAsAction.putValue(Action.NAME, Res.getMessage("menuitem.save.as"));
//			popup.add(saveAsAction);
//			popup.show(this, e.getX(), e.getY());
//		}
//	}
//
//}
