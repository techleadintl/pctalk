package com.xinwei.talk.ui.chat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.util.ColorUtil;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.common.vo.Entry;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.MessageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.chat.room.ChatRoom;

public class ChatContactPanel extends JPanel {
	private ChatRoom chatRoom;

	private boolean isSelected;
	private boolean isRollovered;

	private static final int GAP = 2;

	protected List<byte[]> avatars = new ArrayList<byte[]>();

	private String jid;

	private String name;

	MessageManager messageManager;

	private Point mousePoint;

	private Rectangle deleteRec;

	public ChatContactPanel(final ChatContainer chatContainer, final ChatRoom chatRoom) {
		this.chatRoom = chatRoom;

		messageManager = TalkManager.getMessageManager();

		setOpaque(true);

		setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

		createUI();
		updateUIData();

		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				mousePoint = e.getPoint();
			}
		});
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtil.contains(mousePoint, deleteRec)) {
					chatRoom.close(false);
				}
			}
		});
	}

	private void createUI() {

		setLayout(new BorderLayout());

		setPreferredSize(new Dimension(0, 40));
	}

	public void updateUIData() {
		jid = chatRoom.getRoomId();
		name = chatRoom.getRoomTitle();
		TalkUtil.getDisplayName(jid);
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2D = (Graphics2D) g;
		Object savedRederingHint = g2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		ThemeColor themeColor = McWillTheme.getThemeColor();
		g2D.setColor(themeColor.getSrcColor());
		g2D.fillRect(0, 0, getWidth(), getHeight());

		Container parent = getParent().getParent().getParent();
		if (isSelected) {
			g2D.setColor(themeColor.getMiddleColor1());
		} else if (isRollovered) {
			g2D.setColor(ColorUtil.getAlphaColor(themeColor.getMiddleColor1(), 80));
		} else {
			g2D.setColor(themeColor.getSrcColor());
		}
		g2D.fillRoundRect(0, 0, parent.getWidth() - 0, getHeight() - 0, 10, 10);

		//		if (isRollovered) {
		//			g2D.setColor(g2D.getColor().darker());
		//			g2D.drawRoundRect(0, 0, parent.getWidth()-1, getHeight()-1, 10, 10);
		//		}

		drawImages(g, 5, 5, 30, 30);
		drawUnreadMessageCount(g2D, 5, 5, 30, 30);

		g2D.setColor(Color.DARK_GRAY);

		if (StringUtil.isNotBlank(name)) {
			String name1 = SwingUtil.getString(name, g2D.getFont(), parent.getWidth() - 50, "");
			g2D.drawString(name1, 40, 23);
		}

		deleteRec = null;
		int imageH = 22;
		Rectangle rec = new Rectangle(parent.getWidth() - imageH, (getHeight() - imageH) / 2, imageH, imageH);
		if (isRollovered) {
			Image image;
			if (mousePoint != null && SwingUtil.contains(mousePoint, rec)) {
				image = LAF.getDelete2Icon()[1].getImage();
				deleteRec = rec;
			} else {
				image = LAF.getDelete2Icon()[0].getImage();
			}
			g2D.drawImage(image, rec.x, rec.y, rec.width - 4, rec.height - 4, this);
		}
		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, savedRederingHint);
	}

	private void drawImages(Graphics g, int X, int Y, int W, int H) {
		List<byte[]> images = getAvatarImages();

		g.setColor(getImageBackgroud());

		int y = X;
		int x = Y;
		int darwW = W;
		int darwH = H;
		ImageUtil.drawMergeImage(g, x, y, darwW, darwH, GAP, images);
	}

	private void drawUnreadMessageCount(Graphics g, int X, int Y, int W, int H) {
		int unreadMessageCount = getUnreadMessageCount();

		if (unreadMessageCount < 1) {
			return;
		}
		String unreadMessageCountStr = String.valueOf(unreadMessageCount);
		if (unreadMessageCountStr.length() > 2) {
			unreadMessageCountStr = "...";
		}

		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		int w = 16;
		int h = 16;
		int x = W - 4;
		int y = 1;

		Paint paint = g2d.getPaint();
		GradientPaint gp = new GradientPaint(x, y, Color.RED, w, h, Color.RED);
		g2d.setPaint(gp);
		g2d.fillOval(x, y, w, h);
		g2d.setPaint(paint);

		g2d.setColor(Color.WHITE);

		g2d.setFont(getUnreadMessageFont());
		Rectangle2D stringBounds = SwingUtil.getStringBounds(unreadMessageCountStr, g2d.getFont());

		g2d.drawString(unreadMessageCountStr, x + (int) Math.ceil((w - stringBounds.getWidth()) / 2), 13);
	}

	protected List<byte[]> getAvatarImages() {
		avatars.clear();
		if (TalkUtil.isGroup(jid)) {
			avatars.addAll(ImageManager.getMemberAvatarList(jid, true));
		} else {
			byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
			avatars.add(talkAvatar);
		}

		return avatars;
	}

	protected int getUnreadMessageCount() {
		Entry<Integer, TalkMessage> unreadMessage = messageManager.getUnreadMessage(jid);
		if (unreadMessage == null)
			return 0;
		return unreadMessage.getKey();
	}

	protected Color DEFAULT_COLOR = new Color(240, 240, 240);

	protected Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 11);

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public boolean isRollovered() {
		return isRollovered;
	}

	public void setRollovered(boolean isRollovered) {
		this.isRollovered = isRollovered;
	}

	protected Font getUnreadMessageFont() {
		return DEFAULT_FONT;
	}

	protected Color getImageBackgroud() {
		return DEFAULT_COLOR;
	}

	public Rectangle getDeleteRec() {
		return deleteRec;
	}
}