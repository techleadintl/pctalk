/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月18日 下午6:46:57
 * 
 ***************************************************************/
package com.xinwei.talk.ui.main.content;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JComponent;

import com.xinwei.common.lookandfeel.component.McWillCardPanel;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.talk.common.LAF;

public class WorkspacePane extends McWillCardPanel {

	private Map<String, McWillIconButton> buttonMap = new HashMap<String, McWillIconButton>();

	public WorkspacePane() {
		setBorder(BorderFactory.createEmptyBorder(10, 10, 20, 10));
		addCardChangeListener(new CardChangeListener());
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		LAF.paintMainWorkspacePane(this, (Graphics2D) g);
	}

	public void addCard(String name, JComponent comp, McWillIconButton button) {
		super.addCard(name, comp);
		buttonMap.put(name, button);
	}

	@Override
	public void show(String name, boolean c) {
		super.show(name, c);

		Set<Entry<String, McWillIconButton>> entrySet = buttonMap.entrySet();
		for (Entry<String, McWillIconButton> entry : entrySet) {
			McWillIconButton button = entry.getValue();
			if (name.equals(entry.getKey())) {
				button.setIcon(button.getButtonRolloverIcon());
			} else {
				button.setIcon(button.getButtonIcon());
			}
		}
	}
}
