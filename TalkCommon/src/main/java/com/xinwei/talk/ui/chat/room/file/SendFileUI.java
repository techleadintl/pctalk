package com.xinwei.talk.ui.chat.room.file;

import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillLinkButton2;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.http.service.HttpFileService;
import com.xinwei.http.service.file.OutFileTransfer;
import com.xinwei.http.service.file.TransferStatus;
import com.xinwei.spark.SwingWorker;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.manager.TransferManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.ui.chat.room.ChatRoomWorking;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.chat.room.GroupChatRoom;
import com.xinwei.talk.ui.chat.room.TalkChatRoom;
import com.xinwei.talk.ui.util.component.XWRoundRectProgressBar;
import com.xinwei.talk.ui.util.component.label.LinkLabel;

public class SendFileUI extends FileUI {
	private static final long serialVersionUID = -4403839897649365671L;

	int FILE_NAME_MAX_LENGTH = 200;

	protected FileDragLabel imageLabel = new FileDragLabel();
	protected LinkLabel fileLabel = new LinkLabel();

	protected McWillLinkButton2 cancelButton = new McWillLinkButton2();
	protected XWRoundRectProgressBar progressBar = new XWRoundRectProgressBar();
	protected File fileToSend;
	protected OutFileTransfer transfer;

	protected String nickname;
	protected JLabel progressLabel = new JLabel();

	protected CommonChatRoom room;

	protected Icon fileImageIcon;

	protected ChatRoomWorking working;

	public void init(CommonChatRoom room, final OutFileTransfer transfer) {
		this.transfer = transfer;
		this.room = room;
		setLayout(new GridBagLayout());

		cancelButton.setText(Res.getMessage("button.cancel"));

		progressBar.setPreferredSize(new Dimension(250, 8));
		progressBar.setMaximum(100);
		progressBar.setStringPainted(true);

		add(imageLabel, new GridBagConstraints(0, 0, 1, 4, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		add(fileLabel, new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		add(progressBar, new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(3, 5, 0, 5), 0, 0));
		add(progressLabel, new GridBagConstraints(1, 4, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		add(cancelButton, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 15), 0, 0));

		addListeners();

		String fileName = transfer.getFilename();
		String text = SwingUtil.format(transfer.getContentLength());

		fileToSend = transfer.getFile();

		imageLabel.setFile(fileToSend);

		fileName = SwingUtil.getString(fileName, imageLabel.getFont(), FILE_NAME_MAX_LENGTH, " (" + text + ")");

		fileLabel.setText(fileName);

		fileImageIcon = SwingUtil.getFileImageIcon(transfer.getFile());
		if (fileImageIcon != null) {
			imageLabel.setIcon(fileImageIcon);
		}
	}

	protected OutFileTransfer addListeners() {
		cancelButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent mouseEvent) {
				cancelTransfer();
			}

		});

		imageLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				openFile(fileToSend);
			}

			public void mouseEntered(MouseEvent e) {
				imageLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));

			}

			public void mouseExited(MouseEvent e) {
				imageLabel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
		});
		fileLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				openFile(fileToSend);
			}

			public void mouseEntered(MouseEvent e) {
				fileLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));

			}

			public void mouseExited(MouseEvent e) {
				fileLabel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
		});
		return transfer;
	}

	public void sendFile() {
		//发送文件
		working = new ChatRoomWorking(room, Res.getMessage("chatroom.file.working")) {
			@Override
			public void close() {
				cancelTransfer();
			}
		};
		room.addChatRoomWorking(working);

		sendFile(transfer);

		SwingWorker worker = new SwingWorker() {

			public Object construct() {
				while (true) {
					try {
						long starttime = System.currentTimeMillis();
						long startbyte = transfer.getBytesSent();
						Thread.sleep(500);
						TransferStatus status = transfer.getStatus();
						if (status == TransferStatus.error || status == TransferStatus.complete || status == TransferStatus.cancelled) {
							break;
						}
						long endtime = System.currentTimeMillis();
						long endbyte = transfer.getBytesSent();

						long timediff = endtime - starttime;
						long bytediff = endbyte - startbyte;

						long leftbyte = transfer.getContentLength() - transfer.getBytesSent();
						updateBar(transfer, nickname, leftbyte, bytediff, timediff);
					} catch (InterruptedException e) {
						Log.error("Unable to sleep thread.", e);
					}

				}
				return "";
			}

			public void finished() {
				updateBar(transfer, nickname, 0, 0, 0);
			}
		};

		worker.start();
	}

	private void sendFile(final OutFileTransfer transfer) {
		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				try {
					Object obj = TalkUtil.getObject( room.getRoomId());

					//文件上传的地方
					HttpFileService fileService = TalkManager.getFileService();
					HttpResponseInfo httpResponseInfo = fileService.uploadChatFile(obj, transfer);
					
					String fileAbsolutePath = fileService.getFileUrl(httpResponseInfo);

					doFinish(transfer, fileAbsolutePath);

				} catch (Exception e) {
					transfer.setStatus(TransferStatus.error);
					Log.error(e);
					room.removeChatRoomWorking(working);
					return;
				}
			}
		});
	}

	private void openFile(File downloadedFile) {
		try {
			Desktop.getDesktop().open(downloadedFile);
		} catch (IOException e) {
			Log.error(e);
		}
	}

	private void updateBar(final OutFileTransfer transfer, String nickname, long leftbyte, long bytediff, long timediff) {
		TransferStatus status = transfer.getStatus();
		if (status == TransferStatus.initial) {
			//			titleLabel.setText(Res.getString("message.negotiation.file.transfer", nickname));
		} else if (status == TransferStatus.in_progress) {
			if (!progressBar.isVisible()) {
				progressBar.setVisible(true);
				progressLabel.setVisible(true);
			}
			try {
				SwingUtilities.invokeAndWait(new Runnable() {
					public void run() {
						long p = (transfer.getBytesSent() * 100 / transfer.getContentLength());
						progressBar.setValue(Math.round(p));
					}
				});
			} catch (Exception e) {
				Log.error(e);
			}

			String est = SwingUtil.calculateLeftTime(leftbyte, bytediff, timediff);
			progressLabel.setText(Res.getMessage("label.chatroom.fileupload.progress", SwingUtil.calculateSpeed(bytediff, timediff), est));
		} else if (status == TransferStatus.complete) {
			TransferManager.getInstance().removeFileComponent(room, this);
		} else if (status == TransferStatus.cancelled) {
			TransferManager.getInstance().removeFileComponent(room, this);
			doFinish(transfer, null);
		} else if (status == TransferStatus.error) {
			if (transfer.getException() != null) {
				Log.error("Error occured during file transfer.", transfer.getException());
			}
			TransferManager.getInstance().removeFileComponent(room, this);
			doFinish(transfer, null);
		}

	}

	protected void doFinish(final OutFileTransfer transfer, final String url) {
		SwingWorker writeImageThread = new SwingWorker() {
			public Object construct() {
				return true;
			}

			public void finished() {
				if (transfer.getStatus() == TransferStatus.complete) {
					doSendComplete(transfer, url);
				} else if (transfer.getStatus() == TransferStatus.cancelled) {
					doSendCancel(transfer, url);
				} else if (transfer.getStatus() == TransferStatus.error) {
					doSendCancel(transfer, url);
				}

			}

			private void doSendCancel(final OutFileTransfer transfer, final String url) {
				if (TalkManager.getChatManager().getChatContainer().containChatRoom(room)) {
					room.sendFile(url, transfer.getFile(), false);
					room.removeChatRoomWorking(working);
					TalkManager.getChatManager().getChatContainer().activateChatRoom(room);
				}
			}

			private void doSendComplete(final OutFileTransfer transfer, final String url) {
				room.sendFile(url, transfer.getFile(), true);
				room.removeChatRoomWorking(working);
				TalkManager.getChatManager().getChatContainer().activateChatRoom(room);
			}
		};
		writeImageThread.start();

	}

	public void cancelTransfer() {
		if (transfer != null && !transfer.isDone()) {
			transfer.cancel();
		}
	}

}
