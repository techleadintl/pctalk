//package com.xinwei.talk.ui.util.component.roombak;
//
//import java.awt.Component;
//import java.awt.ComponentOrientation;
//import java.awt.Container;
//import java.awt.Dimension;
//import java.awt.Graphics;
//import java.awt.Graphics2D;
//import java.net.URL;
//import java.util.List;
//
//import javax.swing.BorderFactory;
//import javax.swing.ImageIcon;
//import javax.swing.JComponent;
//import javax.swing.JPanel;
//import javax.swing.UIManager;
//
//import com.xinwei.common.lookandfeel.ninepatch.NinePatchIcon;
//import com.xinwei.talk.common.Res;
//
//public class BalloonPanel extends JPanel {
//	private static ImageIcon SEND_BKG_IMG = new ImageIcon(BalloonPanel.class.getResource("images/left_green_1.png"));
//	private static ImageIcon RECEIVE_BKG_IMG = new ImageIcon(BalloonPanel.class.getResource("images/right_grey_1.png"));
//
//	static {
//		List<String> leftIcons = (List<String>) UIManager.get("McWill.BalloonLeftIcon");
//		for (final String res : leftIcons) {
//			final URL imageURL = Res.getURL(res);
//			ImageIcon imageIcon = new ImageIcon(imageURL);
//			SEND_BKG_IMG = imageIcon;
//		}
//		List<String> rightIcons = (List<String>) UIManager.get("McWill.BalloonRightIcon");
//		for (final String res : rightIcons) {
//			final URL imageURL = Res.getURL(res);
//			ImageIcon imageIcon = new ImageIcon(imageURL);
//			RECEIVE_BKG_IMG = imageIcon;
//		}
//	}
//
//	NinePatchIcon ninePatchIcon;
//	private ChatMsgTextPane textPane;
//	private Container msgPanelParent;
//
//	private int LEFT_WIDTH = 65;
//
//	boolean isLeft;
//
//	public BalloonPanel(boolean isLeft) {
//		this.isLeft = isLeft;
//		if (isLeft) {
//			ninePatchIcon = new NinePatchIcon(SEND_BKG_IMG);
//			setBorder(BorderFactory.createEmptyBorder(8, 18, 8, 8));
//		} else {
//			ninePatchIcon = new NinePatchIcon(RECEIVE_BKG_IMG);
//			setBorder(BorderFactory.createEmptyBorder(8, 15, 8, 18));
//		}
//		setOpaque(false);
//	}
//
//	@Override
//	protected void paintComponent(Graphics g) {
//		super.paintComponent(g);
//		if (textPane == null || msgPanelParent == null) {
//			init(this);
//		}
//
//		Dimension size = this.getPreferredSize();
//		Dimension msgPanelParentSize = msgPanelParent.getSize();
//		if (isLeft) {
//			if (size.width < msgPanelParentSize.width - LEFT_WIDTH) {
//				ninePatchIcon.paintIcon((Graphics2D) g, 0, 0, size.width, size.height);
//			} else {
//				ninePatchIcon.paintIcon((Graphics2D) g, 0, 0, msgPanelParentSize.width - LEFT_WIDTH, size.height);
//			}
//		} else {
//			if (size.width < msgPanelParentSize.width - LEFT_WIDTH) {
//				textPane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
//				ninePatchIcon.paintIcon((Graphics2D) g, msgPanelParentSize.width - LEFT_WIDTH - size.width+10, 0, size.width-10, size.height);
//			} else {
//				textPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
//				ninePatchIcon.paintIcon((Graphics2D) g, 0, 0, msgPanelParentSize.width - LEFT_WIDTH, size.height);
//			}
//		}
//	}
//
//	protected void init(JComponent parent) {
//		if (parent instanceof ChatMsgTextPane) {
//			textPane = (ChatMsgTextPane) parent;
//			Container parent2 = textPane.getParent();
//			while (parent2 != null) {
//				if (parent2 instanceof ChatMsgPanel) {
//					msgPanelParent = parent2.getParent();
//					break;
//				}
//				parent2 = parent2.getParent();
//			}
//			return;
//		}
//		Component[] components = parent.getComponents();
//		for (Component child : components) {
//			if (child instanceof JComponent) {
//				init((JComponent) child);
//			}
//		}
//	}
//}