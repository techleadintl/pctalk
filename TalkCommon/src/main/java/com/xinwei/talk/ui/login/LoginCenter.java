package com.xinwei.talk.ui.login;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.component.McWillCardPanel;
import com.xinwei.common.lookandfeel.component.McWillCheckLabel;
import com.xinwei.common.lookandfeel.component.McWillFilterComboBox;
import com.xinwei.common.lookandfeel.component.McWillHintTextField;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.component.McWillList;
import com.xinwei.common.lookandfeel.ui.McWillMenuItemUI;
import com.xinwei.spark.ui.MessageDialog;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.ui.login.keyboard.SoftKeyBoardPopup;

public class LoginCenter extends McWillCardPanel implements ActionListener, KeyListener, ItemListener, FocusListener {
	class LoginTextFieldPanel extends JPanel {
		public LoginTextFieldPanel() {
			super(new BorderLayout());
		}

		@Override
		protected void paintComponent(Graphics g) {
			LAF.paintLoginTextField(this, g);
			super.paintComponent(g);
		}
	}

	public class MyListCellRenderer implements ListCellRenderer<JComponent> {
		@Override
		public Component getListCellRendererComponent(JList<? extends JComponent> list, JComponent value, int index, boolean isSelected, boolean cellHasFocus) {
			if (value == null)
				return null;
			value.setOpaque(true);
			ThemeColor themeColor = McWillTheme.getThemeColor();
			if (isSelected || cellHasFocus) {
				value.setBackground(themeColor.getMiddleColor1());
			} else {
				Color backgroundColor = ((McWillList) list).getRolloverIndex() == index ? themeColor.getMiddleColor() : themeColor.getLightColor();
				value.setBackground(backgroundColor);
			}
			return value;
		}
	}

	public class CountryItem extends JPanel {
		private static final long serialVersionUID = 1514044406550293152L;

		public String name;
		public String code;

		private String iconPath;

		public ImageIcon icon;

		public boolean invalid = false;

		public CountryItem(String name, String code, ImageIcon icon, String iconPath) {
			if ("0".equals(code) || code == null) {
				code = "";
				invalid = true;
			}

			this.code = code;
			this.name = name;
			this.icon = icon;
			this.iconPath = iconPath;

			setLayout(new BorderLayout());
			setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

			JLabel nameLabel = new JLabel(name);
			nameLabel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));

			add(new JLabel(icon), BorderLayout.WEST);
			add(nameLabel, BorderLayout.CENTER);
			add(new JLabel(code, JLabel.RIGHT), BorderLayout.EAST);
		}

		@Override
		public String toString() {
			return name;
		}
	}

	class IntegerDocument extends PlainDocument {
		private static final long serialVersionUID = 1L;

		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
			// 若字符串为空，直接返回。
			if (str == null) {
				return;
			}
			int len = getLength();
			String s = getText(0, len);// 文本框已有的字符
			try {
				s = s.substring(0, offs) + str + s.substring(offs, len);// 在已有的字符后添加字符
				int i = Integer.parseInt(s); // 只能为正整数
				if (i < 1 || i > 3000) { // 限制范围
					throw new Exception();
				}
			} catch (Exception e) {
				Toolkit.getDefaultToolkit().beep();// 发出提示声音
				return;
			}
			super.insertString(offs, str, a);// 把字符添加到文本框
		}
	}

	private static final String FONT_NAME = "SimHei";

	private static final long serialVersionUID = 2445523786538863459L;

	private JTextField usernameField = new JTextField();
	private JPasswordField passwordField = new JPasswordField();
	private JTextField countryCodeField = new JTextField();
	private JTextField countryNameField = new JTextField();
	private McWillFilterComboBox countryComboBox;

	private SoftKeyBoardPopup keyPopup;

	private McWillCheckLabel savePasswordCheck;
	private McWillCheckLabel autoLoginCheck;

	private McWillIconButton loginButton;

	private McWillIconButton selectUserButton;

	private McWillIconButton keyBoardButton;

	private JButton selectCountryButton;

	private JLabel avatarLabel;

	private JLabel busyLabel;

	JLabel countryLabel;

	Color color = new Color(110, 159, 189);

	Color fg = new Color(22, 112, 235);

	private LoginDialog loginDialog;

	private String countryName;

	public void show(String name) {
		if (busyLabel != null) {
			busyLabel.setVisible(false);
		}
		show(name, true);
		if (busyLabel != null) {

			Map<String, VCard> vCardMap = TalkManager.getVCardMap();
			
			VCard vCard = vCardMap.get(getUsername());
			
			setAvatar(vCard);

			busyLabel.setVisible(true);
		}
	}

	public LoginCenter(LoginDialog loginDialog) {
		this.setOpaque(false);
		this.loginDialog = loginDialog;

		JPanel input = new JPanel(null);
		JPanel loading = new JPanel(null);

		addCard("input-panel", input);
		addCard("loading-panel", loading);

		createInputUI(input);

		createLoadingUI(loading);

		addListeners();

		initData(loginDialog);

	}

	private void addListeners() {
		addCardChangeListener(new CardChangeListener());

		countryComboBox.addItemListener(this);
		countryComboBox.addKeyListener(this);

		countryCodeField.addKeyListener(this);
		countryCodeField.addFocusListener(this);

		usernameField.addKeyListener(this);
		passwordField.addKeyListener(this);

		selectUserButton.addActionListener(this);
		//田伟修改（1）
		selectCountryButton.addActionListener(this);
		keyBoardButton.addActionListener(this);
		loginButton.addActionListener(this);
		autoLoginCheck.addActionListener(this);
		savePasswordCheck.addActionListener(this);

	}

	private void createLoadingUI(final JPanel loading) {
		loading.setLayout(null);
		avatarLabel = new JLabel() {
			@Override
			protected void paintComponent(Graphics g) {
				LAF.paintAvatar((ImageIcon) getIcon(), this, g);
			}
		};
		avatarLabel.setBounds((LAF.getLoginBackgroundSize().width - 70) / 2, 63, 70, 70);

		busyLabel = new JLabel(LAF.getLoginingIcon());
		busyLabel.setBounds((LAF.getLoginBackgroundSize().width - 500) / 2, 185, 500, 35);

		loading.add(avatarLabel);
		loading.add(busyLabel);
	}

	@SuppressWarnings("unchecked")
	private void createInputUI(JPanel conetnt) {
		Dimension size = LAF.getLoginBackgroundSize();
		final JPanel countryNamePanel = new LoginTextFieldPanel();
		countryLabel = new JLabel();// 创建对象
		countryLabel.setOpaque(false);
		countryLabel.setHorizontalAlignment(JLabel.CENTER);
		countryComboBox = new McWillFilterComboBox(getCountryList()) {
			@Override
			protected ImageIcon[] getPopIcons() {
				return LAF.getLoginUserPopIcon();
			}

			@Override
			protected JTextField createXTextField() {
				countryNameField = new JTextField();
				countryNameField.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
				countryNameField.setOpaque(false);
				countryNameField.setForeground(Color.WHITE);
				countryNameField.setCaretColor(Color.WHITE);
				countryNameField.setSelectionColor(new Color(42, 122, 175));
				countryNameField.setSelectedTextColor(new Color(0x80ffff));
				countryNameField.setFont(new Font("ArialMT", Font.BOLD, 15));
				countryNameField.setHorizontalAlignment(JTextField.CENTER);
				return countryNameField;
			}

			@Override
			protected JButton createXButton() {
				selectCountryButton = new McWillIconButton(LAF.getLoginUserPopIcon());// 创建对象
				selectCountryButton.setOpaque(false);
				selectCountryButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				selectCountryButton.setHorizontalAlignment(JButton.RIGHT);
				return selectCountryButton;
			}

			@Override
			public boolean isMatch(String text, Object element) {
				CountryItem country = (CountryItem) element;
				if (country.name.toLowerCase().contains(text.toLowerCase()) || country.code.toLowerCase().contains(text.toLowerCase())) {
					return true;
				} else {
					return false;
				}
			}

			@Override
			protected void doNoMatchs(DefaultComboBoxModel showModel) {
				showModel.addElement(getDefaultCountry());
			}
		};
		countryComboBox.setRenderer(new MyListCellRenderer());
		countryComboBox.setOpaque(false);
		countryComboBox.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));

		countryNamePanel.add(countryLabel, BorderLayout.WEST);
		countryNamePanel.add(countryComboBox, BorderLayout.CENTER);

		// 用户信息栏
		final JPanel userPanel = new JPanel(new BorderLayout(3, 0));
		final JPanel countryCodePanel = new LoginTextFieldPanel();
		final JPanel usernamePanel = new LoginTextFieldPanel();
		userPanel.add(countryCodePanel, BorderLayout.WEST);
		userPanel.add(usernamePanel, BorderLayout.CENTER);

		// 国家码
		JLabel countryCodeLabel = new JLabel("+", JLabel.CENTER);// 创建对象
		countryCodeLabel.setForeground(Color.WHITE);
		countryCodeLabel.setFont(LAF.getFont(18, true));
		countryCodeLabel.setOpaque(false);
		countryCodeField = new JTextField();
		countryCodeField.setDocument(new IntegerDocument());
		countryCodeField.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
		countryCodeField.setOpaque(false);
		countryCodeField.setForeground(Color.WHITE);
		countryCodeField.setCaretColor(Color.WHITE);
		countryCodeField.setSelectionColor(new Color(42, 122, 175));
		countryCodeField.setSelectedTextColor(new Color(0x80ffff));
		countryCodeField.setFont(new Font("ArialMT", Font.BOLD, 15));

		countryCodePanel.add(countryCodeLabel, BorderLayout.WEST);
		countryCodePanel.add(countryCodeField, BorderLayout.CENTER);

		// 用户电话号码
		JLabel userLabel = new JLabel(LAF.getLoginUserIcon());// 创建对象
		userLabel.setOpaque(false);
		usernameField = new McWillHintTextField(getUserNameDefaultText());// 创建对象

		usernameField.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
		usernameField.setOpaque(false);
		usernameField.setForeground(Color.WHITE);
		usernameField.setCaretColor(Color.WHITE);
		usernameField.setSelectionColor(new Color(42, 122, 175));
		usernameField.setSelectedTextColor(new Color(0x80ffff));
		usernameField.setFont(new Font("ArialMT", Font.BOLD, 15));

		selectUserButton = new McWillIconButton(LAF.getLoginUserPopIcon());// 创建对象
		selectUserButton.setOpaque(false);
		selectUserButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		selectUserButton.setHorizontalAlignment(JButton.RIGHT);

		usernamePanel.add(userLabel, BorderLayout.WEST);
		usernamePanel.add(usernameField, BorderLayout.CENTER);
		usernamePanel.add(selectUserButton, BorderLayout.EAST);

		// 密码框
		final JPanel passwordPanel = new LoginTextFieldPanel();

		JLabel passwordLabel = new JLabel(LAF.getLoginLockIcon());// 创建对象
		passwordLabel.setOpaque(false);

		passwordField = new JPasswordField();
		passwordField.setOpaque(false);
		passwordField.setEchoChar('*');
		passwordField.setBorder(null);
		passwordField.setSelectionColor(new Color(42, 122, 175));
		passwordField.setForeground(Color.WHITE);
		passwordField.setCaretColor(Color.WHITE);
		passwordField.setFont(new Font("ArialMT", Font.BOLD, 16));
		passwordField.setSelectedTextColor(new Color(0x80ffff));
		keyPopup = new SoftKeyBoardPopup(passwordField);

		keyBoardButton = new McWillIconButton(LAF.getLoginKeyBoardIcon());// 创建对象
		keyBoardButton.setHorizontalAlignment(JButton.RIGHT);

		passwordPanel.add(passwordLabel, BorderLayout.WEST);
		passwordPanel.add(passwordField, BorderLayout.CENTER);
		passwordPanel.add(keyBoardButton, BorderLayout.EAST);

		keyBoardButton.setToolTipText(Res.getMessage("tip.login.user.softkey"));
		keyBoardButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		// 登录按钮
		loginButton = new McWillIconButton(LAF.getLoginButtonIcon());
		loginButton.setString(Res.getMessage("button.login"));
		loginButton.setForeground(Color.WHITE);
		loginButton.setFont(new Font(FONT_NAME, Font.PLAIN, 16));

		final JPanel checkPanel = new JPanel(new BorderLayout());
		// 记住密码
		savePasswordCheck = new McWillCheckLabel(LAF.getLoginCheck()[0], LAF.getLoginCheck()[1]);
		savePasswordCheck.setText(Res.getMessage("label.login.save.password"));
		savePasswordCheck.setOpaque(false);

		// 自动登录
		autoLoginCheck = new McWillCheckLabel(LAF.getLoginCheck()[0], LAF.getLoginCheck()[1]);
		autoLoginCheck.setText(Res.getMessage("label.login.auto.login"));
		autoLoginCheck.setOpaque(false);

		checkPanel.add(savePasswordCheck, BorderLayout.WEST);
		checkPanel.add(autoLoginCheck, BorderLayout.EAST);

		/////////////////////////
		// 输入框位置
		int inputIconW = 28;
		int inputH = 38;

		Dimension preferredSize = new Dimension(inputIconW, inputH);
		countryLabel.setPreferredSize(new Dimension(inputIconW + 10, inputH));

		selectCountryButton.setPreferredSize(preferredSize);

		// 国家码
		countryCodePanel.setPreferredSize(new Dimension(70, inputH));
		countryCodeLabel.setPreferredSize(preferredSize);
		// 电话号码
		userLabel.setPreferredSize(preferredSize);
		selectUserButton.setPreferredSize(preferredSize);

		passwordLabel.setPreferredSize(preferredSize);
		keyBoardButton.setPreferredSize(preferredSize);
		// 输入面板位置
		int W = 280;
		int H = 40;
		int X = (size.width - W) / 2;
		int GRAP = 10;
		int countryY = 26;
		int usernameY = countryY + H + GRAP;
		int passwordY = usernameY + H + GRAP;
		int loginY = passwordY + H + GRAP;
		int checkY = loginY + H + GRAP;

		countryNamePanel.setBounds(X, countryY, W, H);// 设定位置
		userPanel.setBounds(X, usernameY, W, H);// 设定位置
		passwordPanel.setBounds(X, passwordY, W, H);// 设定位置
		loginButton.setBounds(X, loginY, W, H);
		checkPanel.setBounds(X, checkY, W, 20);

		conetnt.add(countryNamePanel);
		conetnt.add(userPanel);
		conetnt.add(passwordPanel);
		conetnt.add(loginButton);
		conetnt.add(checkPanel);
	}

	private void initData(LoginDialog loginDialog) {

		Map<String, VCard> vCardMap = TalkManager.getVCardMap();

		String lastUsername = TalkManager.getAppConfig().getLastUsername();

		VCard personalVCard = vCardMap.get(lastUsername);

		if (personalVCard == null && !vCardMap.isEmpty()) {
			personalVCard = new ArrayList<>(vCardMap.values()).get(0);
		}

		setVCard(personalVCard);

		if (getUsername().trim().length() > 0) {
			passwordField.requestFocus();
		}

		requestFocus();
	}

	public void setVCard(VCard vcard) {
		if (vcard == null) {
			return;
		}

		int countryCode = vcard.getCountryCode();
		String code = String.valueOf(countryCode);
		String tel = vcard.getTel().substring(("00" + code).length());
		setCountry(code);
		usernameField.setText(tel);

		if (vcard.isSavePassword()) {
			String password = vcard.getPassword();
			if (password != null) {
				passwordField.setText(password);
			}
			savePasswordCheck.setSelected(true);
		}

		if (vcard.isAutoLogin()) {
			autoLoginCheck.setSelected(vcard.isAutoLogin());
		}
	}
	protected void setCountry(String text) {
		boolean flag = true;
		ComboBoxModel model = countryComboBox.getModel();
		for (int i = 0; i < model.getSize(); i++) {
			Object item = model.getElementAt(i);
			if (!(item instanceof CountryItem))
				continue;
			CountryItem country = (CountryItem) item;
			if (country.code.equals(text)) {
				flag = false;
				countryComboBox.selectedItem(country);
				break;
			}
		}
		if (flag) {// 如果无效则选择最后一项
			countryComboBox.selectedItem(model.getElementAt(model.getSize() - 1));
			countryCodeField.setText(text);
		}
	}
	public String setCountryFx(String text, int index,boolean isStartup) {
		boolean flag = true;
		ComboBoxModel model = countryComboBox.getModel();
		Object item = model.getElementAt(index);
		System.out.println(index);
		System.out.println(text);
		CountryItem country = (CountryItem) item;
		System.out.println(country);

		if(isStartup) {
			for (int i = 0; i < model.getSize(); i++) {
				item = model.getElementAt(i);
				if (!(item instanceof CountryItem))
					continue;
				country = (CountryItem) item;
				if (country.code.equals(text)) {
					String countryName = country.name;
					
					flag = false;
					countryComboBox.selectedItem(country);
					return countryName;

				}
			}
		}
		if (flag) {
			countryComboBox.selectedItem(model.getElementAt(model.getSize() - 1));
			countryCodeField.setText(text);
		}
		String a = (country.code);
		return a;
	}

	/**
	 * Returns the username the user defined.
	 *
	 * @return the username.
	 */
	public String getUsername() {
		String countryCode = countryCodeField.getText().trim();
		String tel = usernameField.getText().trim();
		String username = "00" + countryCode + tel;
		return username;
	}

	/**
	 * Returns the password specified by the user.
	 *
	 * @return the password.
	 */
	public String getPassword() {
		return new String(passwordField.getPassword());
	}

	public boolean isSavePassword() {
		return savePasswordCheck.isSelected();
	}

	public boolean isAutoLogin() {
		return autoLoginCheck.isSelected();
	}

	public SoftKeyBoardPopup getSoftKeyBoardPopup() {
		return keyPopup;
	}

	public void setAvatar(VCard vCard) {
		byte[] avatar;
		if (vCard == null) {
			avatar = LAF.getDefaultAvatarImageBytes();
		} else {
			avatar = vCard.getAvatar();
		}
		if (avatar == null || avatar.length == 0) {
			avatar = LAF.getDefaultAvatarImageBytes();
		}
		ImageIcon imageIcon = LAF.getImageIcon(avatar, 70, 70);
		avatarLabel.setIcon(imageIcon);
	}

	public Dimension getPreferredSize() {
		final Dimension dim = super.getPreferredSize();
		dim.height = 230;
		return dim;
	}

	private String getCountryCodeDefaultText() {
		// return Res.getMessage("tip.login.user.name");
		return "";
	}

	private String getCountryNameDefaultText() {
		// return Res.getMessage("tip.login.user.name");
		return "";
	}

	private String getUserNameDefaultText() {
		return Res.getMessage("tip.login.user.name");
	}

	private String getUserPasswordDefaultText() {
		return Res.getMessage("tip.login.user.password");
	}

	JPopupMenu countryPopup = new JPopupMenu();

	@Override
	public void actionPerformed(ActionEvent e) {
		JComponent source = (JComponent) e.getSource();
		if (source == loginButton) {
			login();
		} else if (source == selectUserButton) {
			JPopupMenu popup = getUserPopupMenu(TalkManager.getVCardMap());
			if (popup == null) {
				return;
			}
			Container inputPanel = source.getParent().getParent();
			popup.show(inputPanel, 80, inputPanel.getSize().height - 2);
		} else if (source == keyBoardButton) {
			SoftKeyBoardPopup keyPopup = getSoftKeyBoardPopup();
			if (!keyPopup.isVisible()) {
				Container inputPanel = source.getParent();
				keyPopup.show(inputPanel, 0 - 50, inputPanel.getSize().height);
				keyPopup.getSoftKeyBoardPanel().reset();
				keyPopup.repaint();
			}
		} else if (source == savePasswordCheck) {
			if (!savePasswordCheck.isSelected()) {
				autoLoginCheck.setSelected(false);
			}
		} else if (source == autoLoginCheck) {
			if (autoLoginCheck.isSelected()) {
				savePasswordCheck.setSelected(true);
			}
			//田伟修改（2）
		} else if (source == selectCountryButton) {  
			countryNameField.setText("");   
			countryComboBox.findMatchs();
		}
	}

	public void login() {
		String text = countryCodeField.getText();
		boolean flag = false;
		ComboBoxModel model = countryComboBox.getModel();
		for (int i = 0; i < model.getSize(); i++) {
			Object item = model.getElementAt(i);
			if (!(item instanceof CountryItem))
				continue;
			CountryItem country = (CountryItem) item;
			if (country.code.equals(text)) {
				flag = true;
				break;
			}
		}
		if (!flag) {
			MessageDialog.showAlert(loginDialog.getFrame(), Res.getMessage("countrycode.0"), 300, 100);
			return;
		}

		//loginDialog.login(getUsername(), getPassword(), savePasswordCheck.isSelected(), autoLoginCheck.isSelected());
	}

	public void itemStateChanged(ItemEvent e) {
		Object item = e.getItem();
		if (!(item instanceof CountryItem))
			return;
		CountryItem country = (CountryItem) item;
		countryLabel.setIcon(LAF.getImageIcon(country.iconPath, 24, 58 * 24 / 70));
		if (!country.invalid) {
		}
		countryCodeField.setText(country.code);
	}

	@Override
	public void focusLost(FocusEvent e) {
		JComponent source = (JComponent) e.getSource();
		
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
		JComponent source = (JComponent) e.getSource();
		if (source == usernameField || source == passwordField) {
			if (e.getKeyChar() == KeyEvent.VK_ENTER) {
				login();
			}
		} else if (source == countryCodeField) {
			String text = countryCodeField.getText().trim();
			setCountry(text);
		}

	}

	@Override
	public void focusGained(FocusEvent e) {
	
	}

	public void focusGained() {
		passwordField.requestFocus(true);
	}

	private JPopupMenu getUserPopupMenu(Map<String, VCard> vCardMap) {
		if (vCardMap == null)
			return null;

		Collection<VCard> values = vCardMap.values();

		final JPopupMenu popupMenu = new JPopupMenu();
		for (final VCard vCard : values) {
			try {
				byte[] avatarBytes = null;
				try {
					avatarBytes = vCard.getAvatar();
				} catch (Exception e) {
					Log.error("Cannot retrieve avatar bytes.", e);
				}
				ImageIcon icon = null;
				if (avatarBytes != null && avatarBytes.length > 0) {
					icon = LAF.getImageIcon(avatarBytes);
				}
				if (icon == null) {
					icon = LAF.getDefaultAvatarIcon();
				}
				final ImageIcon avatarIcon = SwingUtil.scale(icon, 40 - 2, 40 - 2);
				final JMenuItem menuItem = new JMenuItem(avatarIcon);
				menuItem.setUI(new McWillMenuItemUI() {
					@Override
					protected Color getMenuSrcColor() {
						return Color.WHITE;
					}

					@Override
					protected Color getMenuDestColor() {
						return Color.WHITE;
					}

					@Override
					protected Color getMenuSelectColor() {
						return new Color(0xeaf0fd);
					}
				});
				menuItem.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						setVCard(vCard);
					}
				});

				menuItem.setLayout(null);
				menuItem.setBorder(null);

				menuItem.setPreferredSize(new Dimension(200, 50));

				JPanel panel = new JPanel();

				panel.setLayout(new BorderLayout());

				JLabel label = new JLabel();
				
				label.setText(vCard.getNick());

				JButton button = new McWillIconButton(LAF.getCloseIcons()[1]);
				button.setMargin(new Insets(0, 2, 0, 2));
				button.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						TalkManager.deleteVCard(vCard);
						popupMenu.remove(menuItem);
						popupMenu.setVisible(false); // hide the popup menu
					}
				});

				panel.add(button, BorderLayout.EAST);
				panel.add(label, BorderLayout.CENTER);

				panel.setBounds(46, 2, 200, 46);
				menuItem.add(panel);

				// menuItem.setBorder(McWillBorders.getLabelBorder(color, 0));

				popupMenu.add(menuItem);

			} catch (Exception e) {
				Log.error(e);
			}

		}
		return popupMenu;
	}

	public Vector<CountryItem> getCountryList() {
		Vector<CountryItem> countryList = new Vector<CountryItem>();

		String startWith = "countrycode.";

		Map<String, String> map = TalkManager.getLocalManager().get("countrycode");

		Set<Entry<String, String>> entrySet = map.entrySet();
		int length = startWith.length();
		for (Entry<String, String> entry : entrySet) {
			String key = entry.getKey();
			String iconPath = "talk/images/country/" + entry.getValue();
			ImageIcon icon = LAF.getImageIcon(iconPath, 40, 58 * 40 / 70);
			String name = Res.getMessage(key);
			String code = key.substring(length);
			countryList.add(new CountryItem(name, code, icon, iconPath));
		}

		return countryList;

	}

	public CountryItem getDefaultCountry() {
		ImageIcon icon = LAF.getImageIcon("talk/images/country/no_flags.png", 40, 58 * 40 / 70);
		String name = Res.getMessage("countrycode.0");
		return new CountryItem(name, "0", icon, "talk/images/country/no_flags.png");
	}
}