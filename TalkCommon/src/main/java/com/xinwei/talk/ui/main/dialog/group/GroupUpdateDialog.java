package com.xinwei.talk.ui.main.dialog.group;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.text.McWillLimitTextField;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.spark.TaskEngine;
import com.xinwei.spark.ui.MessageDialog;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.ui.util.component.XWDialogTitle;
import com.xinwei.talk.ui.util.component.dialog.XWDialog;

@SuppressWarnings("serial")
public class GroupUpdateDialog extends XWDialog {

	private McWillLimitTextField groupnameField;

	private Group group;

	private JLabel infoLabel;

	/**
	 * Create a new instance of RosterDialog.
	 */
	public GroupUpdateDialog(Group group) {
		super(TalkManager.getChatFrame(), "", true);
		this.group = group;
		createDialog();

		JRootPane rootPane = getRootPane();
		((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

		setSize(new Dimension(350, 150));

		SwingUtil.centerWindowOnScreen(this);
	}

	@Override
	protected JPanel createDialogContainer() {
		return new McWillContentPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				LAF.paintMainAddGroupDialog(this, (Graphics2D) g);
			}
		};
	}

	@Override
	protected JPanel createDialogContent() {
		JPanel dialogPanel = new JPanel(new BorderLayout());
		XWDialogTitle titlePanel = new XWDialogTitle(this, Res.getMessage("dialog.title.group.update"));
		titlePanel.setBorder(BorderFactory.createEmptyBorder(6, 10, 6, 0));
		dialogPanel.add(titlePanel, BorderLayout.NORTH);

		dialogPanel.add(createInputPanel(), BorderLayout.CENTER);

		return dialogPanel;
	}

	private JPanel createInputPanel() {
		JPanel inputPanel = new JPanel(new BorderLayout(10,5));

		//JLabel
		JLabel label = new JLabel(Res.getMessage("label.text.group.name"));
		groupnameField = new McWillLimitTextField();
		groupnameField.setMaxLength(30);
		groupnameField.setPreferredSize(new Dimension(200, 27));
		groupnameField.setForeground(Color.GRAY);
		
		infoLabel = new JLabel();
		infoLabel.setPreferredSize(new Dimension(200, 20));

		inputPanel.add(label, BorderLayout.WEST);
		inputPanel.add(groupnameField, BorderLayout.CENTER);
		inputPanel.add(infoLabel, BorderLayout.SOUTH);

		inputPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));

		String name = group.getName();
		if (name != null) {
			groupnameField.setText(name);
		}
		
		
		return inputPanel;
	}

	@Override
	protected boolean onValidate() {
		final String nickname = groupnameField.getText().trim();
		if (StringUtil.isBlank(nickname)) {
			infoLabel.setText(Res.getMessage("warn.empty.text.group.name"));
			return false;
		}

		return true;
	}

	@Override
	protected boolean onOkPressed() {
		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				try {
					TalkManager.getTalkService().updateGroup(group, groupnameField.getText(), group.getDescribe());
				} catch (Exception e1) {
					e1.printStackTrace();
					MessageDialog.showErrorDialog(e1);
				}
			}
		});
		return true;
	}

	@Override
	protected LayoutManager getButtonBarLayout() {
		return new FlowLayout(FlowLayout.CENTER, 30, 5);
	}

	protected JPanel createButtonBar() {
		JPanel bottomButtonPanel = new JPanel(getButtonBarLayout());
		bottomButtonPanel.setOpaque(false);

		closeButton = SwingUtil.newButton(closeTxt);
		okButton = SwingUtil.newButton(Res.getMessage("button.ok"));

		okButton.addActionListener(this);
		closeButton.addActionListener(this);

		bottomButtonPanel.add(closeButton);
		bottomButtonPanel.add(okButton);

		return bottomButtonPanel;
	}

	/**
	 * Display the RosterDialog using the MainWindow as the parent.
	 */
	public void showDialog() {
		setLocationRelativeTo(null);

		setVisible(true);
		toFront();
		requestFocus();
	}

}