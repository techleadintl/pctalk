//package com.xinwei.talk.ui.util.component.avatar;
//
//import java.awt.Color;
//import java.awt.Dimension;
//import java.awt.Font;
//import java.awt.GradientPaint;
//import java.awt.Graphics;
//import java.awt.Graphics2D;
//import java.awt.Image;
//import java.awt.Paint;
//import java.awt.Rectangle;
//import java.awt.RenderingHints;
//import java.awt.geom.Rectangle2D;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.swing.JPanel;
//
//import com.xinwei.common.lang.SwingUtil;
//
//public class AvatarPanel extends JPanel {
//	private static final long serialVersionUID = -7299155244603711933L;
//
//	private static final int GAP = 2;
//
//	protected List<Image> avatars = new ArrayList<Image>();
//
//	public AvatarPanel() {
//		setOpaque(false);
//	}
//
//	public void setAvatarSize(int width, int height) {
//		setMinimumSize(new Dimension(width, height));
//		setMaximumSize(new Dimension(width, height));
//		setPreferredSize(new Dimension(width, height));
//	}
//
//	@Override
//	protected void paintComponent(Graphics g) {
//		super.paintComponent(g);
//		Rectangle bounds = getBounds();
//
//		drawImages(g, bounds);
//
//		drawUnreadMessageCount(g, bounds);
//	}
//
//	private void drawImages(Graphics g, Rectangle bounds) {
//		List<Image> images = getAvatarImages();
//
//		int num = 0;
//		if (images != null) {
//			num = images.size();
//		}
//
//		g.setColor(getImageBackgroud());
//
//		int y = 6;
//		int x = 2;
//		int DARWW = bounds.width - x - 6;
//		int DARWH = bounds.height - y - 3;
//		DARWW = DARWH = Math.min(DARWW, DARWH);
//		if (num > 8) {
//			int h = (DARWH - 2 * GAP) / 3;
//			int w = (DARWW - 2 * GAP) / 3;
//
//			g.drawImage(images.get(0), x, y, w, h, this);
//
//			g.drawImage(images.get(1), x + w + GAP, y, w, h, this);
//
//			g.drawImage(images.get(2), x + 2 * w + 2 * GAP, y, w, h, this);
//
//			g.drawImage(images.get(3), x, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(4), x + w + GAP, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(5), x + 2 * w + 2 * GAP, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(6), x, y + 2 * h + 2 * GAP, w, h, this);
//
//			g.drawImage(images.get(7), x + w + GAP, y + 2 * h + 2 * GAP, w, h, this);
//
//			g.drawImage(images.get(8), x + 2 * w + 2 * GAP, y + 2 * h + 2 * GAP, w, h, this);
//		} else if (num == 8) {
//			int h = (DARWH - 2 * GAP) / 3;
//			int w = (DARWW - 2 * GAP) / 3;
//			g.fillRect(x + w + GAP, y + h + GAP, w, h);
//
//			g.drawImage(images.get(0), x, y, w, h, this);
//
//			g.drawImage(images.get(1), x + w + GAP, y, w, h, this);
//
//			g.drawImage(images.get(2), x + 2 * w + 2 * GAP, y, w, h, this);
//
//			g.drawImage(images.get(3), x, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(4), x + 2 * w + 2 * GAP, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(5), x, y + 2 * h + 2 * GAP, w, h, this);
//
//			g.drawImage(images.get(6), x + w + GAP, y + 2 * h + 2 * GAP, w, h, this);
//
//			g.drawImage(images.get(7), x + 2 * w + 2 * GAP, y + 2 * h + 2 * GAP, w, h, this);
//		} else if (num == 5) {
//			int h = (DARWH - 2 * GAP) / 3;
//			int w = (DARWW - 2 * GAP) / 3;
//			g.fillRect(x + w + GAP, y, w, h);
//			g.fillRect(x, y + h + GAP, w, h);
//			g.fillRect(x + 2 * w + 2 * GAP, y + h + GAP, w, h);
//			g.fillRect(x + w + GAP, y + 2 * h + 2 * GAP, w, h);
//
//			g.drawImage(images.get(0), x, y, w, h, this);
//
//			g.drawImage(images.get(1), x + 2 * w + 2 * GAP, y, w, h, this);
//
//			g.drawImage(images.get(2), x + w + GAP, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(3), x, y + 2 * h + 2 * GAP, w, h, this);
//
//			g.drawImage(images.get(4), x + 2 * w + 2 * GAP, y + 2 * h + 2 * GAP, w, h, this);
//		} else if (num == 6) {
//			int h = (DARWH - 2 * GAP) / 3;
//			int w = (DARWW - 2 * GAP) / 3;
//			g.fillRect(x, y, w, h);
//			g.fillRect(x + 2 * w + 2 * GAP, y, w, h);
//			g.fillRect(x + w + GAP, y + 2 * h + 2 * GAP, w, h);
//
//			g.drawImage(images.get(0), x + w + GAP, y, w, h, this);
//
//			g.drawImage(images.get(1), x, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(2), x + w + GAP, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(3), x + 2 * w + 2 * GAP, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(4), x, y + 2 * h + 2 * GAP, w, h, this);
//
//			g.drawImage(images.get(5), x + 2 * w + 2 * GAP, y + 2 * h + 2 * GAP, w, h, this);
//		} else if (num == 7) {
//			int h = (DARWH - 2 * GAP) / 3;
//			int w = (DARWW - 2 * GAP) / 3;
//
//			g.fillRect(x, y, w, h);
//			g.fillRect(x + w + GAP, y, w, h);
//			g.fillRect(x + 2 * w + 2 * GAP, y, w, h);
//
//			g.drawImage(images.get(0), (DARWW - w) / 2, y, w, h, this);
//
//			g.drawImage(images.get(1), x, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(2), x + w + GAP, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(3), x + 2 * w + 2 * GAP, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(4), x, y + 2 * h + 2 * GAP, w, h, this);
//
//			g.drawImage(images.get(5), x + w + GAP, y + 2 * h + 2 * GAP, w, h, this);
//
//			g.drawImage(images.get(6), x + 2 * w + 2 * GAP, y + 2 * h + 2 * GAP, w, h, this);
//		} else if (num == 4) {
//			int h = (DARWH - GAP) / 2;
//			int w = (DARWW - GAP) / 2;
//
//			g.drawImage(images.get(0), x, y, w, h, this);
//
//			g.drawImage(images.get(1), x + w + GAP, y, w, h, this);
//
//			g.drawImage(images.get(2), x, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(3), x + w + GAP, y + h + GAP, w, h, this);
//		} else if (num == 3) {
//			int h = (DARWH - GAP) / 2;
//			int w = (DARWW - GAP) / 2;
//
//			g.fillRect(x, y, w, h);
//			g.fillRect(x + w + GAP, y, w, h);
//
//			g.drawImage(images.get(0), (DARWW - w) / 2, y, w, h, this);
//
//			g.drawImage(images.get(1), x, y + h + GAP, w, h, this);
//
//			g.drawImage(images.get(2), x + w + GAP, y + h + GAP, w, h, this);
//		} else if (num == 2) {
//			int h = (DARWH - GAP) / 2;
//			int w = (DARWW - GAP) / 2;
//			g.fillRect(x + w + GAP, y, w, h);
//			g.fillRect(x, y + h + GAP, w, h);
//
//			g.drawImage(images.get(0), x, y, w, h, this);
//			g.drawImage(images.get(1), x + w + GAP, y + h + GAP, w, h, this);
//
//		} else if (num == 1) {
//			int h = DARWH;
//			int w = DARWW;
//			g.drawImage(images.get(0), x, y, w, h, this);
//		} else {
//			int h = DARWH;
//			int w = DARWW;
//			g.fillRect(x, y, w, h);
//		}
//	}
//
//	private void drawUnreadMessageCount(Graphics g, Rectangle bounds) {
//		int unreadMessageCount = getUnreadMessageCount();
//
//		if (unreadMessageCount < 1) {
//			return;
//		}
//		String unreadMessageCountStr = String.valueOf(unreadMessageCount);
//		if (unreadMessageCountStr.length() > 2) {
//			unreadMessageCountStr = "...";
//		}
//
//		Graphics2D g2d = (Graphics2D) g;
//		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
//
//		int w = 16;
//		int h = 16;
//		int x = bounds.width - w - 2;
//		int y = 1;
//
//		Paint paint = g2d.getPaint();
//		GradientPaint gp = new GradientPaint(x, y, Color.RED, w, h, Color.RED);
//		g2d.setPaint(gp);
//		g2d.fillOval(x, y, w, h);
//		g2d.setPaint(paint);
//
//		g2d.setColor(Color.WHITE);
//
//		g2d.setFont(getUnreadMessageFont());
//		Rectangle2D stringBounds = SwingUtil.getStringBounds(unreadMessageCountStr, g2d.getFont());
//
//		g2d.drawString(unreadMessageCountStr, x + (int) Math.ceil((w - stringBounds.getWidth()) / 2), 13);
//	}
//
//	protected List<Image> getAvatarImages() {
//		avatars.clear();
//
//		setAvatarImages(avatars);
//
//		return avatars;
//	}
//
//	protected void setAvatarImages(List<Image> avatars) {
//
//	}
//
//	protected int getUnreadMessageCount() {
//		return 0;
//	}
//
//	protected Font getUnreadMessageFont() {
//		return DEFAULT_FONT;
//	}
//
//	protected Color getImageBackgroud() {
//		return DEFAULT_COLOR;
//	}
//
//	protected Color DEFAULT_COLOR = new Color(240, 240, 240);
//
//	protected Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 11);
//}
