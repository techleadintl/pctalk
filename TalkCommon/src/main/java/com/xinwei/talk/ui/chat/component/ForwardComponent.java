/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月17日 上午10:50:15
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.listener.ComponentMouseMotionListener;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;

public class ForwardComponent extends JPanel {
	private boolean isSelected;
	private boolean isRollovered;

	private static final int GAP = 2;

	protected List<byte[]> avatars = new ArrayList<byte[]>();

	private String jid;

	private String name;

	private Point mousePoint;

	private Rectangle deleteRec;

	public ForwardComponent(String jid) {
		setOpaque(true);

		this.jid = jid;
		name = TalkUtil.getDisplayName(jid);

		setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

		createUI();

		addMouseMotionListener(new ComponentMouseMotionListener(this) {
			@Override
			public void mouseMoved(MouseEvent e) {
				mousePoint = e.getPoint();
				super.mouseMoved(e);
				repaint();
			}

			@Override
			public void mouseEntered(int x, int y) {
				isRollovered = true;
				repaint();
			}

			@Override
			public void mouseExited() {
				isRollovered = false;
				mousePoint = null;
				repaint();
			}
		});
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtil.contains(mousePoint, deleteRec)) {
					doDelete();
				}
			}
		});

	}

	public void doDelete() {

	}

	private void createUI() {

		setLayout(new BorderLayout());
		int stringWidth = SwingUtil.getStringWidth(name, getFont());

		if (stringWidth > 50) {
			stringWidth = 50;
		}

		setPreferredSize(new Dimension(stringWidth + 25 + 35, 35));
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2D = (Graphics2D) g;
		Object savedRederingHint = g2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		ThemeColor themeColor = McWillTheme.getThemeColor();
		g2D.setColor(themeColor.getSrcColor());
		//		g2D.fillRect(0, 0, getWidth(), getHeight());

		if (isSelected) {
			g2D.setColor(themeColor.getMiddleColor());
		} else if (isRollovered) {
			g2D.setColor(themeColor.getSrcColor());
		} else {
			g2D.setColor(themeColor.getMiddleColor1());
			//			g2D.setColor(ColorUtil.getAlphaColor(themeColor.getMiddleColor1(), 50));
		}
		g2D.fillRoundRect(0, 0, getWidth() - 0, getHeight() - 0, 10, 10);

		//		if (isRollovered) {
		//			g2D.setColor(g2D.getColor().darker());
		//			g2D.drawRoundRect(0, 0, parent.getWidth()-1, getHeight()-1, 10, 10);
		//		}

		drawImages(g, 5, 5, 25, 25);

		g2D.setColor(Color.DARK_GRAY);
		if (StringUtil.isNotBlank(name)) {
			String name1 = SwingUtil.getString(name, g2D.getFont(), 50, "");
			g2D.drawString(name1, 35, 20);
		}

		deleteRec = null;
		int imageH = 22;
		Rectangle rec = new Rectangle(getWidth() - imageH, (getHeight() - imageH) / 2, imageH, imageH);
		if (isRollovered) {
			Image image;
			if (mousePoint != null && SwingUtil.contains(mousePoint, rec)) {
				image = LAF.getRemove2Icon()[1].getImage();
				deleteRec = rec;
			} else {
				image = LAF.getRemove2Icon()[0].getImage();
			}
			g2D.drawImage(image, rec.x, rec.y, rec.width - 4, rec.height - 4, this);
		}
		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, savedRederingHint);

	}

	private void drawImages(Graphics g, int X, int Y, int W, int H) {
		List<byte[]> images = getAvatarImages();

		g.setColor(getImageBackgroud());

		int y = X;
		int x = Y;
		int DARWW = W;
		int DARWH = H;
		ImageUtil.drawMergeImage(g, x, y, DARWW, DARWH, GAP, images);
	}

	protected List<byte[]> getAvatarImages() {
		avatars.clear();
		if (TalkUtil.isGroup(jid)) {
			avatars.addAll(ImageManager.getMemberAvatarList(jid, true));
		} else {
			avatars.add(ImageManager.getTalkAvatar(jid, true));
		}

		return avatars;
	}

	protected Color DEFAULT_COLOR = new Color(240, 240, 240);

	protected Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 11);

	protected Font getUnreadMessageFont() {
		return DEFAULT_FONT;
	}

	protected Color getImageBackgroud() {
		return DEFAULT_COLOR;
	}
}
