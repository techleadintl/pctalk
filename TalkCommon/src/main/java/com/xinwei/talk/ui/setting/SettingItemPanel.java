package com.xinwei.talk.ui.setting;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.util.ColorUtil;

public class SettingItemPanel extends JPanel {
	private Image image;
	private String name;

	private boolean isSelected;
	private boolean isRollovered;

	public SettingItemPanel(final Image image, String name) {
		this.image = image;

		this.name = name;

		setOpaque(true);

		setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

		createUI();

	}

	private void createUI() {

		setLayout(new BorderLayout());

		setPreferredSize(new Dimension(0, 40));
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2D = (Graphics2D) g.create();
		Object savedRederingHint = g2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		ThemeColor themeColor = McWillTheme.getThemeColor();
		g2D.setColor(themeColor.getSrcColor());
		g2D.fillRect(0, 0, getWidth(), getHeight());

		Container parent = getParent().getParent().getParent();
		if (isSelected) {
			g2D.setColor(themeColor.getMiddleColor1());
		} else if (isRollovered) {
			g2D.setColor(ColorUtil.getAlphaColor(themeColor.getMiddleColor1(), 80));
		} else {
			g2D.setColor(themeColor.getSrcColor());
		}
		g2D.fillRoundRect(0, 0, parent.getWidth() - 0, getHeight() - 0, 10, 10);

		//		if (isRollovered) {
		//			g2D.setColor(g2D.getColor().darker());
		//			g2D.drawRoundRect(0, 0, parent.getWidth()-1, getHeight()-1, 10, 10);
		//		}

		g.setColor(getImageBackgroud());

		g.drawImage(image, 5, (getHeight() - image.getHeight(this)) / 2, this);

		g2D.setColor(Color.DARK_GRAY);

		String name1 = SwingUtil.getString(name, g2D.getFont(), parent.getWidth() - 50, "");
		g2D.drawString(name1, 27, 23);

		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, savedRederingHint);

		g2D.dispose();
	}

	protected Color DEFAULT_COLOR = new Color(240, 240, 240);

	protected Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 11);

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public boolean isRollovered() {
		return isRollovered;
	}

	public void setRollovered(boolean isRollovered) {
		this.isRollovered = isRollovered;
	}

	protected Color getImageBackgroud() {
		return DEFAULT_COLOR;
	}
}