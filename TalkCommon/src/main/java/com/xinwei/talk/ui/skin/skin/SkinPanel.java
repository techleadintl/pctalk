/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月5日 下午3:23:52
 * 
 ***************************************************************/
package com.xinwei.talk.ui.skin.skin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.UIManager;


public class SkinPanel extends JPanel {
	public SkinPanel(final Window skinDialog) {
		setLayout(new BorderLayout());

		List<Color> colors = (List<Color>) UIManager.get("McWill.SkinColor");
		if (colors == null || colors.isEmpty()) {
			colors = new ArrayList<Color>();
			colors.add(new Color(155, 114, 239));
			colors.add(new Color(241, 158, 189));
			colors.add(new Color(238, 87, 67));
			colors.add(new Color(164, 0, 0));
			colors.add(new Color(206, 67, 0));
			colors.add(new Color(254, 185, 0));
			colors.add(new Color(255, 228, 1));
			colors.add(new Color(137, 190, 55));
			colors.add(new Color(99, 140, 11));
			colors.add(new Color(73, 165, 236));
			colors.add(new Color(119, 198, 255));
		}
		List<String> icons = (List<String>) UIManager.get("McWill.SkinIcon");

		JPanel conent = new JPanel(new BorderLayout());
		conent.add(new SkinStylePanel(skinDialog, icons), BorderLayout.CENTER);
		conent.add(new SkinColorPanel(skinDialog, colors), BorderLayout.SOUTH);

		add(conent, BorderLayout.CENTER);
		add(new JPanel(), BorderLayout.SOUTH);//按钮

	}
}
