/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月13日 下午1:06:47
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.xinwei.talk.ui.chat.balloon.BalloonTextPane;

public class TalkTextPane extends BalloonTextPane implements MouseListener, MouseMotionListener {

	public TalkTextPane() {
		super();

		setDragEnabled(true);

		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public void mouseClicked(MouseEvent e) {
//		try {
//			final int pos = viewToModel(e.getPoint());
//			final Element element = getStyledDocument().getCharacterElement(pos);
//
//			if (element != null) {
//				final AttributeSet as = element.getAttributes();
//				final Object o = as.getAttribute("link");
//
//				if (o != null) {
//					try {
//						final String url = (String) o;
//						if (e.getButton() == MouseEvent.BUTTON1)
//							SwingUtil.openURL(url);
//						else if (e.getButton() == MouseEvent.BUTTON3) {
//							JPopupMenu popupmenu = new JPopupMenu();
//							JMenuItem linkcopy = new JMenuItem(Res.getString("action.copy"));
//							linkcopy.addActionListener(new ActionListener() {
//
//								@Override
//								public void actionPerformed(ActionEvent e) {
//									SwingUtil.setClipboard(url);
//
//								}
//							});
//							linkcopy.setEnabled(true);
//							popupmenu.add(linkcopy);
//							popupmenu.show(this, e.getX(), e.getY());
//						}
//					} catch (Exception ioe) {
//						Log.error("Error launching browser:", ioe);
//					}
//				}
//			}
//		} catch (Exception ex) {
//			Log.error("Visible Error", ex);
//		}
	}

	public void mousePressed(MouseEvent e) {
	}

	/**
	 * This launches the <code>BrowserLauncher</code> with the URL located in <code>ChatArea</code>. Note that the url will automatically be clickable when added to <code>ChatArea</code>
	 *
	 * @param e
	 *            - the MouseReleased event
	 */
	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
	}

	protected void releaseResources() {
	}

	public void clear() {
		super.setText("");
		super.clear();
	}

}
