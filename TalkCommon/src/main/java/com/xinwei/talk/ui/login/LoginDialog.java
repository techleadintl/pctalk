/**
 * $RCSfile: ,v $
 * $Revision: $
 * $Date: $
 *
 * Copyright (C) 2004-2011 Jive Software. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xinwei.talk.ui.login;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillBackground;
import com.xinwei.http.model.HttpRequestInfo;
import com.xinwei.talk.TalkStarter;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.SessionManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.ui.util.WindowDragListener;
import com.xinwei.xmpp.smack.XWXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smackx.ChatStateManager;

import javax.security.auth.callback.*;
import javax.swing.*;
import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Dialog to log in a user into the Spark Server. The TrayMessageDialog is used
 * only for login in registered users into the Spark Server.
 */
public class LoginDialog implements CallbackHandler {
	private JFrame frame;

	/**
	 * Empty Constructor
	 */
	public LoginDialog() {
	}

	private LoginTop loginTop;
	private LoginCenter loginCenter;

	private LoginBottom loginBottom;

	private XMPPConnection connection = null;

	public void invoke(final JFrame parentFrame) {
		// Construct Dialog
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				frame = new JFrame(Res.getMessage("application.name"));
				frame.setIconImage(LAF.getApplicationImage().getImage());

				Dimension loginBackgroundSize = LAF.getLoginBackgroundSize();
				frame.setSize(loginBackgroundSize);
				frame.setDefaultCloseOperation(3);
				frame.setLocationRelativeTo(null);// 设置窗体中间位置
				frame.setLayout(null);// 绝对布局
				frame.setUndecorated(true);// 去除窗体
				frame.setAlwaysOnTop(true); // 设置界面悬浮

				JRootPane rootPane = frame.getRootPane();
				((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

				rootPane.setBorder(BorderFactory.createEmptyBorder());

				// 上部
				loginTop = new LoginTop(frame);
				loginTop.setBounds(0, 0, loginBackgroundSize.width, 73);
				frame.add(loginTop);

				// 中部
				loginCenter = new LoginCenter(LoginDialog.this);
				loginCenter.setBounds(0, 73, loginBackgroundSize.width, 283);
				frame.add(loginCenter);

				// 底部
				loginBottom = new LoginBottom(LoginDialog.this);
				loginBottom.setBounds(0, loginBackgroundSize.height - 50, loginBackgroundSize.width, 50);
				frame.add(loginBottom);

				// 设置背景
				McWillBackground background = new McWillBackground();
				background.setIcon(LAF.getLoginBackgroundImage());
				background.setBounds(0, 0, loginBackgroundSize.width, loginBackgroundSize.height);
				frame.add(background);

				// 拖动窗体

				WindowDragListener windowDragListener = new WindowDragListener(frame);

				frame.addMouseListener(windowDragListener);
				frame.addMouseMotionListener(windowDragListener);

				frame.setLocationRelativeTo(parentFrame);

				frame.setResizable(false);

				// Center dialog on screen
				SwingUtil.centerWindowOnScreen(frame);

				// Make dialog top most.
				frame.setVisible(true);

				loginCenter.focusGained();

				// 自动登录
				if (loginCenter.isAutoLogin() && loginCenter.getPassword() != null) {
					loginCenter.login();
				}
			}
		});

	}

	protected ConnectionConfiguration retrieveConnectionConfiguration(String xmppHost, int port, String loginServer) {
		ConnectionConfiguration config = null;

		config = new ConnectionConfiguration(xmppHost, port, loginServer);

		config.setReconnectionAllowed(true);
		config.setRosterLoadedAtLogin(true);
		config.setSendPresence(false);

		config.setCompressionEnabled(false);

		return config;
	}

	/**
	 * Validates the users login information and execute login API.
	 */
	public synchronized int loginFx(final String code, final String name, final String password,
			final boolean savePassword, final boolean autoLogin, FileOutputStream lock) {

		String countryCode = code.trim();
		String tel = name.trim();
		String username = "00" + countryCode + tel;

		int returnCode = doLogin(username, password, savePassword, autoLogin);
		if (returnCode == 0) {
			startTalk();
		} else {
			System.out.println(lock);
			if (lock != null) {
				IOUtil.closeStream(lock);
			}
		}
		return returnCode;
	}

	/*
	 * public synchronized void login(final String name, final String password,
	 * final boolean savePassword, final boolean autoLogin) { final FileOutputStream
	 * LOCK = SystemUtil.lockFile(TalkUtil.getUserHome(name).getAbsolutePath() +
	 * "/lock"); TalkManager.LOCK = LOCK; if (TalkManager.LOCK == null) {
	 * loginBottom.showMessage(name); return; } else {
	 * loginBottom.showMessage(null); }
	 * 
	 * //loginCenter.show("loading-panel");
	 * 
	 * final SwingWorker loginValidationThread = new SwingWorker() { public Object
	 * construct() { int returnCode = doLogin(name, password, savePassword,
	 * autoLogin); if (returnCode!=0) { if (LOCK != null) {//登录失败释放锁
	 * IOUtil.closeStream(LOCK); } loginCenter.show("input-panel"); } return
	 * loginSuccessfull; } };
	 * 
	 * loginValidationThread.start(); }
	 */
	private int doLogin(final String loginUsername, final String loginPassword, final boolean savePassword,
			final boolean autoLogin) {
		final SessionManager sessionManager = TalkManager.getSessionManager();
		int returnCode = 0;
		String errorMessage = null;

		SmackConfiguration.setPacketReplyTimeout(Res.getInteger("packet.reply.timeout") * 1000);

		// Get connection
		VCard vCard = TalkManager.getVCard();
		try {
			// 设置登录服务器地址
			vCard.setLoginServer(Res.getString("talk.server"));

			TalkManager.getLoginService().login(loginUsername, loginPassword);

			String xmppHost = vCard.getImServer();
			// vCard.setJid("weiquan#camtalk_00855383037722@camito.cn");
			// String xmppHost = "172.18.5.80";

			String jid = vCard.getJid();

			String imUsername = TalkUtil.getName(jid);
			String imServerName = TalkUtil.getServer(jid);

			int port = Res.getInteger("xmpp.port", 5222);

			ConnectionConfiguration config = retrieveConnectionConfiguration(xmppHost, port, imServerName);
			connection = new XWXMPPConnection(config, this);

			connection.connect();

			Object token = vCard.getV("token");
			connection.login(imUsername, token + "##" + HttpRequestInfo.DEVICE_ID + "##" + TalkUtil.getXmppResource(),
					TalkUtil.getXmppResource());

			sessionManager.setServerAddress(connection.getServiceName());
			sessionManager.initializeSession(connection, loginUsername, loginPassword);
			sessionManager.setJID(connection.getUser());
		} catch (Exception xee) {
			// Changed by Srinath
			if (xee.getMessage().equals("validateSubscriber is fail.")) {
				Log.warn("Username or passward error");
				errorMessage = Res.getMessage("message.login.invalid.username");
				returnCode = 1;

			} else if (xee.getMessage().equals("用户名或密码错误")) {
				Log.warn("Username or passward error");
				errorMessage = Res.getMessage("message.login.invalid.password");
				returnCode = 2;
			} else {
				Log.warn("connection error");
				errorMessage = Res.getMessage("message.login.unrecoverable.error");
				returnCode = 3;

			}

			Log.warn("Exception in Login:", xee);
			return returnCode;
		}

		/*
		 * if (errorMessage != null) { final String finalerrorMessage = errorMessage;
		 * 
		 * Platform.runLater(new Runnable() {
		 * 
		 * @Override public void run() {
		 * 
		 * Alert alert = new Alert(AlertType.CONFIRMATION);
		 * alert.setTitle("Error Login"); alert.setHeaderText(null);
		 * alert.setContentText(finalerrorMessage); ButtonType buttonTypeOne = new
		 * ButtonType("Ok");
		 * 
		 * alert.getButtonTypes().setAll(buttonTypeOne); Optional<ButtonType> result =
		 * alert.showAndWait();
		 * 
		 * if ((result.isPresent()) && (result.get() == buttonTypeOne)) {
		 * 
		 * }
		 * 
		 * } }); return false; }
		 */
		// Since the connection and workgroup are valid. Add a ConnectionListener
		connection.addConnectionListener(TalkManager.getSessionManager());

		// Initialize chat state notification mechanism in smack
		ChatStateManager.getInstance(TalkManager.getConnection());

		// Persist information
		if (savePassword) {
			vCard.setPassword(loginPassword);
		} else {
			vCard.setPassword("");
		}
		vCard.setSavePassword(savePassword);
		vCard.setAutoLogin(autoLogin);

		TalkManager.saveVCard();

		TalkManager.getAppConfig().setLastUsername(loginUsername);

		TalkManager.saveAppConfig();

		return returnCode;
	}

	/**
	 * Initializes Spark and initializes all plugins.
	 */
	public void startTalk() {
//		frame.setVisible(false);

		try {
			TalkManager.getCheckUpdates().loginCheckForUpdate();
		} catch (Exception e) {
			Log.error(e);
		}
		new TalkStarter().start();// set-up/download/load all necessary data for the current session
	}

	@Override
	public void handle(javax.security.auth.callback.Callback[] callbacks)
			throws IOException, UnsupportedCallbackException {
		for (Callback callback : callbacks) {
			if (callback instanceof NameCallback) {
				NameCallback ncb = (NameCallback) callback;
				ncb.setName(loginCenter.getUsername());
			} else if (callback instanceof PasswordCallback) {
				PasswordCallback pcb = (PasswordCallback) callback;
				pcb.setPassword(loginCenter.getPassword().toCharArray());
			} else {
				Log.error("Unknown callback requested: " + callback.getClass().getSimpleName());
			}
		}
	}

	public JFrame getFrame() {
		return frame;
	}

}
