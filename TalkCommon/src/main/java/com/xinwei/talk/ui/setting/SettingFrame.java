/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年2月27日 上午10:52:07
 * 
 ***************************************************************/
package com.xinwei.talk.ui.setting;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;

import javax.swing.JFrame;
import javax.swing.JRootPane;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillTitlePane;
import com.xinwei.common.lookandfeel.component.McWillWindowButtonUI;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.ui.setting.detail.AboutPanel;
import com.xinwei.talk.ui.setting.detail.LanguagePanel;
import com.xinwei.talk.ui.setting.detail.SettingCommonPanel;
import com.xinwei.talk.ui.setting.detail.ShortcutKeyPanel;

/**
 * The Window used to display the ChatRoom container.
 */
public class SettingFrame extends JFrame implements McWillWindowButtonUI {

	private static final long serialVersionUID = -7789413067818105293L;
	private static SettingFrame sf;

	public static synchronized SettingFrame getInstance() {
		if (sf == null) {
			sf = new SettingFrame();
		}
		return sf;
	}

	SettingContainer settingContainer;

	/**
	 * Creates default ChatFrame.
	 * 
	 * @param bottomPanel
	 */
	private SettingFrame() {
		setIconImages(LAF.getApplicationImages());

		getContentPane().setLayout(new BorderLayout());

		JRootPane root = getRootPane();

		McWillTitlePane titlePanel = new McWillTitlePane(root, ((BaseRootPaneUI) root.getUI()));
		((BaseRootPaneUI) root.getUI()).setTitlePane(root, titlePanel);

		settingContainer = new SettingContainer(this);

		SettingFrameContentPanel frameContentPanel = new SettingFrameContentPanel(settingContainer) {
			@Override
			public void addTitlePanle(Component comp) {
				settingContainer.getRightPane().add(comp, BorderLayout.NORTH);
			}
		};
		frameContentPanel.setTitleTextIconVisible(false);
		frameContentPanel.setLayout(new BorderLayout(0, -3));
		frameContentPanel.add(settingContainer, BorderLayout.CENTER);

		setContentPane(frameContentPanel);

		setSize(650, 435);
		setResizable(false);

		setTitle(Res.getMessage("setting.title"));

		SwingUtil.centerWindowOnScreen(this);

	}

	protected void addNewSettingItems() {
		SettingItem commonSettingItem = new SettingItem();
		commonSettingItem.setImage(LAF.getImage("talk/images/setting/common.png"));
		commonSettingItem.setName(Res.getMessage("setting.general.title"));
		commonSettingItem.setRight(new SettingCommonPanel());
		settingContainer.addSettingItem(commonSettingItem);

		SettingItem shortcutKeySettingItem = new SettingItem();
		shortcutKeySettingItem.setImage(LAF.getImage("talk/images/setting/shortcut.png"));
		shortcutKeySettingItem.setName(Res.getMessage("setting.shortcutkey.title"));
		shortcutKeySettingItem.setRight(new ShortcutKeyPanel());
		settingContainer.addSettingItem(shortcutKeySettingItem);

		SettingItem languageSettingItem = new SettingItem();
		languageSettingItem.setImage(LAF.getImage("talk/images/setting/language.png"));
		languageSettingItem.setName(Res.getMessage("setting.language.title"));
		languageSettingItem.setRight(new LanguagePanel());
		settingContainer.addSettingItem(languageSettingItem);

		SettingItem aboutSettingItem = new SettingItem();
		aboutSettingItem.setImage(LAF.getImage("talk/images/setting/about.png"));
		aboutSettingItem.setName(Res.getMessage("setting.about.title"));
		aboutSettingItem.setRight(new AboutPanel());
		settingContainer.addSettingItem(aboutSettingItem);
	}

	/**
	 * Brings the ChatFrame into focus on the desktop.
	 */
	public void bringFrameIntoFocus() {
		if (!isVisible()) {
			settingContainer.clearSettingItems();
			addNewSettingItems();
			setVisible(true);
		}

		if (getState() == Frame.ICONIFIED) {
			setState(Frame.NORMAL);
		}

		toFront();
		requestFocus();
	}

	public void setVisible(boolean b) {
		if (!b) {
			setExtendedState(Frame.ICONIFIED);//为了释放内存
		}
		super.setVisible(b);
	}

	public boolean isMax() {
		return false;
	}

	@Override
	public boolean isIconify() {
		return true;
	}
}
