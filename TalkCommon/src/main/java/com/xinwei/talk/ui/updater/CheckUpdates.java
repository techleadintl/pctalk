/**
 * $Revision: $
 * $Date: $
 * 
 * Copyright (C) 2004-2011 Jive Software. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xinwei.talk.ui.updater;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;

import com.xinwei.common.lang.ByteFormat;
import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.HttpUtils;
import com.xinwei.common.lang.NumberUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.spark.TaskEngine;
import com.xinwei.spark.ui.ConfirmDialog;
import com.xinwei.spark.ui.TitlePanel;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.TalkManager;

public class CheckUpdates {
	public static int INIT = -1000;
	public static int CONFIRM_DL_S = 1;

	public static int CONFIRM_DL_YES = 12;

	public static int CONFIRM_DL_CANCEL = -12;

	public static int DOWNLOAD_S = 2;

	public static int DOWNLOAD_YES = 21;

	public static int DOWNLOAD_CANCEL = -21;

	public static int DOWNLOAD_ERROR = -22;

	public static int CONFIRM_INSTALL_S = 3;

	public static int CONFIRM_INSTALL_YES = 31;

	public static int CONFIRM_INSTALL_CANCEL = -31;

	public static int INSTALL_S = 4;

	public static int INSTALL_YES = 41;

	public static int INSTALL_CANCEL = -41;

	public static int INSTALL_ERROR = -42;

	public int result = INIT;

	private String sizeText;

	private int read = 0;

	String title = Res.getMessage("title.new.client.available");

	String yesButton = Res.getMessage("button.yes");
	String noButton = Res.getMessage("button.no");

	public CheckUpdates() {
	}

	/**
	 * @param explicit
	 *            true if the user explicitly asks for the latest version.
	 * @throws Exception
	 *             if there is an error during check
	 */
	public void timerCheckForUpdate() throws Exception {
		if (result > 0) {
			return;
		}
		// Server Version
		TalkVersion serverVersion = null;
		try {
			serverVersion = getLatestVersion();
		} catch (Exception e) {
			Log.error(e);
		}
		int v = isGreater(serverVersion);

		if (v == TalkVersion.NO) {//No need to upgrade
			return;
		}
		boolean autoUpdater = TalkManager.getUserConfig().getAutoUpdater();

		if (!autoUpdater) {
			//确认是否下载 Confirm download
			confirmDownload(serverVersion, true);

			while (result == CONFIRM_DL_S) {//准备时，等待  Waiting while preparing
				SystemUtil.sleep(5);
			}

			if (result == CONFIRM_DL_CANCEL) {//取消时，退出 Exit when canceled
				if (v == TalkVersion.YES) {//可以升级 Can be upgraded
					return;
				}
				if (v == TalkVersion.MUST) {//必须升级 Must be upgraded
					TalkManager.getMainWindow().shutdown();
					return;
				}
			}
		}

		//下载 download 
		File downloadedFile = download(serverVersion);

		while (result == DOWNLOAD_S) {//准备时，等待 Waiting while preparing
			SystemUtil.sleep(5);
		}
		if (result == DOWNLOAD_CANCEL || result == DOWNLOAD_ERROR) {//取消时，退出 Exit when canceled
			if (v == TalkVersion.YES) {//可以升级 Can be upgraded
				return;
			}
			if (v == TalkVersion.MUST) {//必须升级  Must be upgraded
				TalkManager.getMainWindow().shutdown();
			}
		}

		if (autoUpdater) {
			//确认是否安装 Confirm whether to install
			confirmInstall(downloadedFile);

			while (result == CONFIRM_INSTALL_S) {//准备时，等待 Waiting while preparing
				SystemUtil.sleep(5);
			}
			if (result == CONFIRM_INSTALL_CANCEL) {//取消时，退出 Exit when canceled
				if (v == TalkVersion.YES) {//可以升级 Can be upgraded
					return;
				}
				if (v == TalkVersion.MUST) {//必须升级 Must be upgraded
					TalkManager.getMainWindow().shutdown();
				}
			}
		}
		//安装  installation
		install(downloadedFile);

		while (result == INSTALL_S) {
			SystemUtil.sleep(5);
		}

		if (result == INSTALL_ERROR) {
			if (v == TalkVersion.YES) {//可以升级
				return;
			}
			if (v == TalkVersion.MUST) {//必须升级
				TalkManager.getMainWindow().shutdown();
			}
		}
	}

	public void checkForUpdate() {
		result = INIT;
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				try {
					timerCheckForUpdate();
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (result != INIT) {
					return;
				}
				//还原初始值
				TaskEngine.getInstance().schedule(this, 15 * 60 * 1000);
			}
		};
		TaskEngine.getInstance().schedule(task, 5 * 60 * 1000);
	}

	public void loginCheckForUpdate() throws Exception {
		if (result > 0) {
			return;
		}

		if (isLocalBuildAvailable()) {
			return;
		}
		// Server Version
		TalkVersion serverVersion = null;
		try {
			serverVersion = getLatestVersion();
		} catch (Exception e) {
			Log.error(e);
		}
		int v = isGreater(serverVersion);

		if (v == TalkVersion.NO || v == TalkVersion.YES) {//不用升级
			return;
		}

		//确认是否下载
		confirmDownload(serverVersion, false);

		while (result == CONFIRM_DL_S) {//准备时，等待
			SystemUtil.sleep(5);
		}
		if (result == CONFIRM_DL_CANCEL) {//取消时，退出
			System.exit(0);
		}
		//下载
		File downloadedFile = download(serverVersion);

		while (result == DOWNLOAD_S) {//准备时，等待
			SystemUtil.sleep(5);
		}
		if (result == DOWNLOAD_CANCEL || result == DOWNLOAD_ERROR) {//取消时，退出
			System.exit(0);
		}
		//确认是否安装
		confirmInstall(downloadedFile);

		while (result == CONFIRM_INSTALL_S) {//准备时，等待
			SystemUtil.sleep(5);
		}
		if (result == CONFIRM_INSTALL_CANCEL) {//取消时，退出
			System.exit(0);
		}
		//安装
		install(downloadedFile);

		while (result == INSTALL_S) {
			SystemUtil.sleep(5);
		}

		if (result == INSTALL_ERROR) {
			System.exit(0);
		}
	}

	private void confirmDownload(final TalkVersion latestVersion, boolean explicit) {
		//进入确认流程
		result = CONFIRM_DL_S;

		// Otherwise updates are available
		final ConfirmDialog confirmDialog = new ConfirmDialog() {
			@Override
			public void doYes() {
				result = CONFIRM_DL_YES;
			}

			@Override
			public void doNo() {
				result = CONFIRM_DL_CANCEL;
			}
		};
		String message = null;
		if (explicit) {
			message = Res.getMessage("message.new.talk.available2", latestVersion.getVersion());
		} else {
			message = Res.getMessage("message.new.talk.available1", latestVersion.getVersion());
		}
		confirmDialog.showConfirmDialog(new JFrame(), title, message, yesButton, noButton, null);
		confirmDialog.setDialogSize(400, 300);

		confirmDialog.centerWindowOnScreen();

	}

	//	private void toDownloadUpdate(final TalkVersion latestVersion, final File fileToDownload) {
	//		//进入确认流程
	//		result = DOWNLOAD_S;
	//
	//		SwingWorker worker = new SwingWorker() {
	//			public Object construct() {
	//				SystemUtil.sleep(50);
	//				return "ok";
	//			}
	//
	//			public void finished() {
	//				if (SwingUtil.isWindows()) {
	//					download(fileToDownload, latestVersion);
	//				} else {
	//					//					try {// Launch browser to download page.
	//					//						SwingUtil.openURL(latestVersion.getDownloadURL());
	//					//						result = DOWNLOAD_E;
	//					//					} catch (Exception e) {
	//					//						Log.error(e);
	//					//						result = DOWNLOAD_ERROR;
	//					//					}
	//				}
	//			}
	//		};
	//		worker.start();
	//	}

	private File download(final TalkVersion version) {
		//进入确认流程
		result = DOWNLOAD_S;
		final File fileToDownload = getFileToDownload(version);
		final java.util.Timer timer = new java.util.Timer();

		// Prepare HTTP post
		final GetMethod post = new GetMethod(version.getDownloadURL());

		// Get HTTP client
		final HttpClient httpclient = new HttpClient();
		int contentLength = 0;
		try {
			int result = httpclient.executeMethod(post);
			if (result != 200) {
				result = DOWNLOAD_ERROR;
				return null;
			}

			long length = post.getResponseContentLength();
			contentLength = (int) length;

		} catch (IOException e) {
			Log.error(e);
		}
		if (contentLength <= 0) {
			result = DOWNLOAD_ERROR;
			return null;
		}

		final JProgressBar bar = new JProgressBar(0, contentLength);
		final JFrame frame = new JFrame(Res.getMessage("title.downloading.im.client"));

		frame.setIconImage(LAF.getApplicationImage().getImage());

		final TitlePanel titlePanel = new TitlePanel(Res.getMessage("title.upgrading.client"), Res.getMessage("message.version", version.getVersion()), LAF.getDocumentInfoImageIcon(), true);

		sizeText = null;
		read = 0;

		final Thread thread = new Thread(new Runnable() {
			public void run() {
				try {
					InputStream stream = post.getResponseBodyAsStream();
					long size = post.getResponseContentLength();
					ByteFormat formater = new ByteFormat();
					sizeText = formater.format(size);
					titlePanel.setDescription(Res.getMessage("message.version", version.getVersion()) + " \n" + Res.getMessage("message.file.size", sizeText));

					fileToDownload.getParentFile().mkdirs();

					FileOutputStream out = new FileOutputStream(fileToDownload);
					copy(bar, stream, out);
					out.close();
					if (result != DOWNLOAD_CANCEL) {
						result = DOWNLOAD_YES;
					} else {
						out.close();
						fileToDownload.delete();
					}
					frame.dispose();
				} catch (Exception ex) {
					result = DOWNLOAD_ERROR;
					// Nothing to do
				} finally {
					timer.cancel();
					// Release current connection to the connection pool once you are done
					post.releaseConnection();
				}
			}
		});

		frame.getContentPane().setLayout(new GridBagLayout());
		frame.getContentPane().add(titlePanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		frame.getContentPane().add(bar, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));

		JEditorPane pane = new JEditorPane();
		boolean displayContentPane = StringUtil.isNotBlank(version.getVersionMessage());

		pane.setEditable(false);
		if (version.getVersionMessage() != null) {
			pane.setText(version.getVersionMessage());
		}

		if (displayContentPane) {
			frame.getContentPane().add(new JScrollPane(pane), new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
		}

		frame.getContentPane().setBackground(Color.WHITE);
		frame.pack();
		if (displayContentPane) {
			frame.setSize(600, 400);
		} else {
			frame.setSize(400, 100);
		}
		frame.setLocationRelativeTo(TalkManager.getMainWindow());
		SwingUtil.centerWindowOnScreen(frame);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				thread.interrupt();
				boolean downloadComplete = result == DOWNLOAD_YES;
				result = DOWNLOAD_CANCEL;
				if (!downloadComplete) {
					JOptionPane.showMessageDialog(TalkManager.getMainWindow(), Res.getMessage("message.updating.cancelled"), Res.getMessage("title.cancelled"), JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		frame.setVisible(true);
		thread.start();

		timer.scheduleAtFixedRate(new TimerTask() {
			int seconds = 1;

			public void run() {
				ByteFormat formatter = new ByteFormat();
				long value = bar.getValue();
				long average = value / seconds;
				String text = formatter.format(average) + "/Sec";

				String total = formatter.format(value);
				titlePanel.setDescription(Res.getMessage("message.version", version.getVersion()) + " \n" + Res.getMessage("message.file.size", sizeText) + "\n"
						+ Res.getMessage("message.transfer.rate") + ": " + text + "\n" + Res.getMessage("message.total.downloaded") + ": " + total);
				seconds++;
			}
		}, 1000, 1000);

		return fileToDownload;
	}

	/**
	 * Common code for copy routines. By convention, the streams are closed in the same method in which they were opened. Thus, this method does not close the streams when the copying is done.
	 *
	 * @param in
	 *            Source stream
	 * @param out
	 *            Destination stream
	 */
	private void copy(final JProgressBar bar, final InputStream in, final OutputStream out) {
		Runnable runUpdateBar = new Runnable() {
			@Override
			public void run() {
				bar.setValue(read);
			}
		};

		try {
			final byte[] buffer = new byte[4096];
			while (result != DOWNLOAD_CANCEL) {
				int bytesRead = in.read(buffer);
				if (bytesRead < 0) {
					break;
				}
				out.write(buffer, 0, bytesRead);
				read += bytesRead;
				SwingUtilities.invokeLater(runUpdateBar);
			}
		} catch (IOException e) {
			result = DOWNLOAD_ERROR;
			Log.error(e);
		}
	}

	public File getFileToDownload(TalkVersion latestVersion) {
		// Otherwise updates are available
		String downloadURL = latestVersion.getDownloadURL();
		String filename = downloadURL.substring(downloadURL.lastIndexOf("/") + 1);

		if (filename.indexOf('=') != -1) {
			filename = filename.substring(filename.indexOf('=') + 1);
		}

		// Set Download Directory 
		final File downloadDir = TalkUtil.getBinFile("updates");
		if (!downloadDir.exists()) {
			downloadDir.mkdirs();
		}

		// Set file to download.
		final File fileToDownload = new File(downloadDir, filename);
		if (fileToDownload.exists()) {
			fileToDownload.delete();
		}
		return fileToDownload;
	}

	//	public void update1(final TalkVersion latestVersion, final boolean modal) {
	//		// Otherwise updates are available
	//		String downloadURL = latestVersion.getDownloadURL();
	//		String filename = downloadURL.substring(downloadURL.lastIndexOf("/") + 1);
	//
	//		if (filename.indexOf('=') != -1) {
	//			filename = filename.substring(filename.indexOf('=') + 1);
	//		}
	//
	//		// Set Download Directory 
	//		final File downloadDir = new File(TalkUtil.getBinDirectory(), "updates");
	//		if (!downloadDir.exists()) {
	//			downloadDir.mkdirs();
	//		}
	//
	//		// Set file to download.
	//		final File fileToDownload = new File(downloadDir, filename);
	//		if (fileToDownload.exists()) {
	//			fileToDownload.delete();
	//		}
	//
	//		ConfirmDialog confirm = new ConfirmDialog();
	//		String title = Res.getMessage("title.new.client.available");
	//
	//		String message = null;
	//		if (modal) {
	//			message = Res.getMessage("message.new.talk.available1", filename);
	//		} else {
	//			message = Res.getMessage("message.new.talk.available2", filename);
	//		}
	//
	//		String yesButton = Res.getMessage("button.yes");
	//		String noButton = Res.getMessage("button.no");
	//
	//		confirm.setConfirmListener(new ConfirmDialog.ConfirmListener() {
	//			public void doYes() {
	//				SwingWorker worker = new SwingWorker() {
	//					public Object construct() {
	//						try {
	//							Thread.sleep(50);
	//						} catch (InterruptedException e) {
	//							Log.error(e);
	//						}
	//						return "ok";
	//					}
	//
	//					public void finished() {
	//						if (SwingUtil.isWindows()) {
	//							downloadUpdate(fileToDownload, latestVersion);
	//						} else {
	//							try {// Launch browser to download page.
	//								SwingUtil.openURL(latestVersion.getDownloadURL());
	//							} catch (Exception e) {
	//								Log.error(e);
	//							}
	//						}
	//					}
	//
	//				};
	//				worker.start();
	//			}
	//
	//			public void doNo() {
	//				if (modal) {
	//					System.exit(0);
	//				}
	//			}
	//		});
	//
	//		JDialog confirmDialog = confirm.showConfirmDialog(TalkManager.getMainWindow(), title, message, yesButton, noButton, null);
	//		confirm.setDialogSize(400, 300);
	//
	//		GraphicUtils.centerWindowOnScreen(confirmDialog);
	//		confirmDialog.setVisible(true);
	//	}

	//-1：不升级，0:可升级，1：必须升级
	private int isGreater(TalkVersion serverVersion) {
		if (serverVersion == null) {
			return TalkVersion.NO;
		}

		String newV = serverVersion.getVersion().trim();
		return isGreater(newV);

	}

	private int isGreater(String newV) {
		if (StringUtil.isBlank(newV)) {
			return TalkVersion.NO;
		}

		String oldV = Res.getProperty("talk.version");
		if (StringUtil.isBlank(oldV)) {
			return TalkVersion.MUST;
		}

		String[] newVs = newV.split("\\.");
		String[] oldVs = oldV.split("\\.");

		int i = Math.min(newVs.length, oldVs.length);
		if (i < 3) {
			return TalkVersion.NO;
		}
		int newInt = NumberUtil.getInt(newVs[0], -1);
		int oldInt = NumberUtil.getInt(oldVs[0], -1);
		if (newInt > oldInt) {
			return TalkVersion.MUST;
		}
		newInt = NumberUtil.getInt(newVs[1], -1);
		oldInt = NumberUtil.getInt(oldVs[1], -1);
		if (newInt > oldInt) {
			return TalkVersion.MUST;
		}

		newInt = NumberUtil.getInt(newVs[2], -1);
		oldInt = NumberUtil.getInt(oldVs[2], -1);
		if (newInt > oldInt) {
			return TalkVersion.YES;
		}
		return TalkVersion.NO;

	}

	/**
	 * Prompts the user to install the latest Spark.
	 *
	 * @param downloadedFile
	 *            the location of the latest downloaded client.
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 */
	private void confirmInstall(final File downloadedFile) {
		result = CONFIRM_INSTALL_S;
		ConfirmDialog confirm = new ConfirmDialog() {
			@Override
			public void doYes() {
				result = CONFIRM_INSTALL_YES;
			}

			@Override
			public void doNo() {
				result = CONFIRM_INSTALL_CANCEL;
			}
		};
		confirm.showConfirmDialog(TalkManager.getMainWindow(), Res.getMessage("title.download.complete"), Res.getMessage("message.restart.talk"), yesButton, noButton, null);
		confirm.centerWindowOnScreen();
	}

	private void install(final File downloadedFile) {
		result = INSTALL_S;
		try {
			if (SwingUtil.isWindows()) {
				Runtime.getRuntime().exec(downloadedFile.getAbsolutePath());
			} else if (SwingUtil.isMac()) {
				Runtime.getRuntime().exec("open " + downloadedFile.getCanonicalPath());
			}
			TalkManager.getMainWindow().shutdown();
		} catch (Exception e) {
			result = INSTALL_ERROR;
		}
	}

	public static TalkVersion getLatestVersion() throws Exception {
		List<String> fileServers = TalkManager.getVCard().getFileServers();
		if (CollectionUtil.isEmpty(fileServers)) {
			return null;
		}
		String fileServer = fileServers.get(0);

		String url = MessageFormat.format(Res.getProperty("talk.updater.pattern"), fileServer);
		String xml = HttpUtils.get(url);
		if (StringUtil.isBlank(xml)) {
			return null;
		}

		XmlPullParser parser = new MXParser();
		parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
		parser.setInput(new StringReader(xml));

		TalkVersion talkVersion = new TalkVersion();
		boolean done = false;
		while (!done) {
			int eventType = parser.next();
			if (eventType == XmlPullParser.START_TAG) {
				if (parser.getName().equals("version")) {
					talkVersion.setVersion(parser.nextText());
				} else if (parser.getName().equals("updateTime")) {
					talkVersion.setUpdateTime(parser.nextText());
				} else if (parser.getName().equals("downloadURL")) {
					talkVersion.setDownloadURL(parser.nextText());
				} else if (parser.getName().equals("versionMessage")) {
					talkVersion.setVersionMessage(parser.nextText());
				}
			} else if (eventType == XmlPullParser.END_TAG) {
				if (parser.getName().equals("talk")) {
					done = true;
				}
			}
		}
		return talkVersion;
	}

	/**
	 * Checks to see if a new version of Spark has already been downloaded by not installed.
	 *
	 * @return true if a newer version exists.
	 */
	private boolean isLocalBuildAvailable() {
		// Check the bin directory for previous downloads. If there is a newer version of Spark, ask if they wish to install.
		if (!SwingUtil.isWindows()) {
			return false;
		}

		File binDirectory = TalkUtil.getBinFile("updates");
		File[] files = binDirectory.listFiles();
		if (files == null || files.length == 0) {
			return false;
		}

		//CooTalk_1.2.2.exe
		for (File file : files) {
			String fileName = file.getName();
			String regex = "CooTalk_(.*).exe";
			Pattern p = Pattern.compile(regex);
			Matcher matcher = p.matcher(fileName);

			if (!matcher.matches()) {
				file.delete();
				continue;
			}

			String versionNumber = matcher.group(1);
			int v = isGreater(versionNumber);
			if (TalkVersion.NO == v) {
				continue;
			}

			//确认是否安装
			confirmInstall(file);

			while (result == CONFIRM_INSTALL_S) {//准备时，等待
				SystemUtil.sleep(5);
			}
			if (result == CONFIRM_INSTALL_CANCEL) {//取消时，退出
				if (v == TalkVersion.YES) {//可以升级
					continue;
				}
				if (v == TalkVersion.MUST) {//必须升级
					System.exit(0);
				}
			}
			//安装
			install(file);

			while (result == INSTALL_S) {
				SystemUtil.sleep(5);
			}

			if (result == INSTALL_ERROR) {
				if (v == TalkVersion.YES) {//可以升级
					continue;
				}
				if (v == TalkVersion.MUST) {//必须升级
					System.exit(0);
				}
			}

			return true;
		}

		return false;
	}

}
