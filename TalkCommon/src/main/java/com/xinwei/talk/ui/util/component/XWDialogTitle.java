package com.xinwei.talk.ui.util.component;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.ui.util.WindowDragListener;

@SuppressWarnings("serial")
public class XWDialogTitle extends JPanel implements ActionListener {
	private JButton close;
	private JLabel titleLabel;
	private Window dialog;

	public XWDialogTitle(Window dialog, String title) {
		this.dialog = dialog;

		titleLabel = new JLabel(title);
		titleLabel.setForeground(LAF.getDialogTitleForegroundColor());

		this.setLayout(new BorderLayout());
		this.setOpaque(false);

		//关闭按钮
		close = new McWillIconButton(LAF.getCloseIcons());
		close.addActionListener(this);

		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 2));
		buttonPanel.add(close);

		this.add(titleLabel, BorderLayout.WEST);
		this.add(buttonPanel, BorderLayout.EAST);
		WindowDragListener windowDragListener = new WindowDragListener(dialog);
		addMouseListener(windowDragListener);
		addMouseMotionListener(windowDragListener);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		LAF.paintDialogTitle(this, g);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == close) {
			dialog.setVisible(false);
		}
	}
}
