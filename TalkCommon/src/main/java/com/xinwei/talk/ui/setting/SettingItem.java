package com.xinwei.talk.ui.setting;

import java.awt.Image;

import javax.swing.JPanel;

public class SettingItem {
	private String name;
	private Image image;
	private JPanel right;
	private JPanel left;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public JPanel getRight() {
		return right;
	}

	public void setRight(JPanel right) {
		this.right = right;
	}

	public JPanel getLeft() {
		return left;
	}

	public void setLeft(JPanel left) {
		this.left = left;
	}

}