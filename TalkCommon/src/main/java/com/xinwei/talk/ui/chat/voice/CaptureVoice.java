/*************************************************************
 * * Copyright (c) 2012-2020  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2012-8-28 上午10:56:11
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.voice;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.TalkUtil;

import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncodingAttributes;

public abstract class CaptureVoice {
	/** 最大录音时间 **/
	private static int MAX_CAPTURE_VOICE = 30000;
	/** 控制录音标志 **/
	boolean stopCapture = false;
	/** 录音格式 **/
	AudioFormat audioFormat;

	/** 从TargetDataLine写入ByteArrayOutputStream录音 **/

	ByteArrayOutputStream byteArrayOutputStream;

	int totaldatasize = 0;

	TargetDataLine targetDataLine;

	// 播放数据：从AudioInputStream写入SourceDataLine播放

	AudioInputStream audioInputStream;

	SourceDataLine sourceDataLine;

	CaptureListenerThread listener;

	public abstract void doFinish(byte[] content);

	/**
	 * 录音:保存在ByteArrayOutputStream
	 * 
	 * @throws LineUnavailableException
	 */
	public void doCapture() throws Exception {
		audioFormat = VoiceManager.getAudioFormat();

		DataLine.Info dataLineInfo = new DataLine.Info(TargetDataLine.class, audioFormat);

		targetDataLine = (TargetDataLine) AudioSystem.getLine(dataLineInfo);

		targetDataLine.open(audioFormat);

		targetDataLine.start();
		// 创建独立线程进行录音
		new CaptureThread().start();

		// 启动监听线程到一定时间停止录音
		listener = new CaptureListenerThread();
		listener.start();
	}

	// 播放ByteArrayOutputStream中的数据
	public void doPlay() {
		try {
			// 取得录音数据
			byte audioData[] = byteArrayOutputStream.toByteArray();
			// 转换成输入流
			InputStream byteArrayInputStream = new ByteArrayInputStream(audioData);

			AudioFormat audioFormat = VoiceManager.getAudioFormat();

			audioInputStream = new AudioInputStream(byteArrayInputStream, audioFormat, audioData.length / audioFormat.getFrameSize());

			DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, audioFormat);

			sourceDataLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);

			sourceDataLine.open(audioFormat);

			sourceDataLine.start();

			// 创建独立线程进行播放
			new PlayThread().start();
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	// （3）停止录音
	public void doStop() {
		stopCapture = true;
	}

	private void toFinish() {
		listener.interrupt();
		doFinish(byteArrayOutputStream.toByteArray());
		targetDataLine.stop();
		targetDataLine.close();
		targetDataLine = null;
	}
	//
	//	// （4）保存文件
	//	public void doSave(File file) {
	//		if (!file.getParentFile().exists()) {
	//			return;
	//		}
	//		// 取得录音输入流
	//		AudioFormat audioFormat = getAudioFormat();
	//
	//		byte audioData[] = byteArrayOutputStream.toByteArray();
	//
	//		InputStream byteArrayInputStream = new ByteArrayInputStream(audioData);
	//
	//		audioInputStream = new AudioInputStream(byteArrayInputStream,
	//
	//				audioFormat, audioData.length / audioFormat.getFrameSize());
	//		// 写入文件
	//		try {
	//			AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, file);
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//	}
	//
	//	//ffmpeg -acodec libamr_nb -i shenhuxi.amr amr2wav.wav
	//	//ffmpeg -i test.wav -acodec libamr_nb -ab 12.2k -ar 8000 -ac 1 wav2amr.amr
	//	public byte[] convertVoice() {
	//		File parent = TalkUtil.getUserFile("sounds");
	//		if (parent == null)
	//			return null;
	//
	//		long currentTime = System.currentTimeMillis();
	//		String srcName = null;
	//		String destName = null;
	//
	//		srcName = currentTime + ".wav";
	//		destName = currentTime + ".amr";
	//
	//		File srcFile = null;
	//		File destFile = null;
	//		try {
	//			srcFile = new File(parent.getPath(), srcName);
	//			doSave(srcFile);
	//
	//			destFile = new File(parent.getPath(), destName);
	//
	//			AudioAttributes audio = new AudioAttributes();
	//			audio.setCodec("libamr_nb");
	//			audio.setBitRate(new Integer(12200));
	//			audio.setChannels(new Integer(1));
	//			audio.setSamplingRate(new Integer(8000));
	//
	//			EncodingAttributes attrs = new EncodingAttributes();
	//			attrs.setFormat("amr");
	//			attrs.setAudioAttributes(audio);
	//			Encoder encoder = new Encoder();
	//			encoder.encode(srcFile, destFile, attrs);
	//
	//			destFile = new File(parent.getPath(), destName);
	//			if (!destFile.exists()) {
	//				return null;
	//			}
	//			FileInputStream wavStream = new FileInputStream(destFile);
	//
	//			byte[] bufferInput = new byte[(int) destFile.length()];
	//			wavStream.read(bufferInput);
	//			wavStream.close();
	//
	//			return bufferInput;
	//
	//		} catch (Exception e) {
	//			Log.error(e.getMessage());
	//			return null;
	//		} finally {
	//			if (destFile != null) {
	//				destFile.delete();
	//			}
	//			if (srcFile != null) {
	//				srcFile.delete();
	//			}
	//		}
	//	}

	class CaptureThread extends Thread {
		byte tempBuffer[] = new byte[10000];

		public void run() {
			byteArrayOutputStream = new ByteArrayOutputStream();
			totaldatasize = 0;
			stopCapture = false;
			try {// 循环执行，直到按下停止录音按钮
				while (!stopCapture) {
					// 读取10000个数据
					int cnt = targetDataLine.read(tempBuffer, 0, tempBuffer.length);
					if (cnt > 0) {
						// 保存该数据
						byteArrayOutputStream.write(tempBuffer, 0, cnt);
						totaldatasize += cnt;
					}
				}
				byteArrayOutputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			toFinish();
		}
	}

	class PlayThread extends Thread {
		byte tempBuffer[] = new byte[10000];

		public void run() {
			try {
				int cnt;
				// 读取数据到缓存数据
				while ((cnt = audioInputStream.read(tempBuffer, 0, tempBuffer.length)) != -1) {
					if (cnt > 0) {
						// 写入缓存数据
						sourceDataLine.write(tempBuffer, 0, cnt);
					}
				}
				// Block等待临时数据被输出为空
				sourceDataLine.drain();
				sourceDataLine.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class CaptureListenerThread extends Thread {
		public void run() {
			try {
				sleep(MAX_CAPTURE_VOICE);
			} catch (Exception e) {
			}
			stopCapture = true;
		}
	}
}
