package com.xinwei.talk.ui.main.dialog.member.util;

import java.util.ArrayList;
import java.util.List;

import com.xinwei.common.lang.PinyinUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.ui.main.dialog.member.model.MemberInfo;

public class MemUtil {

	public static MemberInfo getMemberInfo(String jid,String uid,String displayName) {
		MemberInfo m = new MemberInfo();
		m.jid = jid;
		m.displayName = displayName;
		m.uid = uid;

		m.userNameWithTone = PinyinUtil.convertWithTone(displayName);
		m.userNameWithoutTone = PinyinUtil.convertWithoutTone(displayName);

		m.caption = String.valueOf(getCaption(m.userNameWithoutTone)).toUpperCase();

		m.searchKeys = PinyinUtil.getSearchKeys(displayName);

		return m;
	}

	public static char getCaption(String str) {
		if (str.length() < 1) {
			return '#';
		}
		char c = str.charAt(0);
		if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) {
			return c;
		}
		return '#';
	}

	final static String SEPARATOR = " ";
}
