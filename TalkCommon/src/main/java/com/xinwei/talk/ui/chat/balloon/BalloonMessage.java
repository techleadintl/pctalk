/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月22日 下午7:47:14
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.balloon;

import java.awt.Component;
import java.awt.Rectangle;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;

public class BalloonMessage implements Serializable, Cloneable {
	public static final int ALIGNMENT_CENTER = 1;

	public static final int ALIGNMENT_LEFT = 2;

	public static final int ALIGNMENT_RIGHT = 3;

	public static final int STATUS_INIT = 1;

	public static final int STATUS_NO = 2;

	public static final int STATUS_YES = 3;

	public String timepattern = "HH:mm:ss";

	protected long id;

	protected int length;

	public int timeStartPosition;
	public int timeEndPosition;
	protected int textStartPosition;
	protected int textEndPosition;
	public int nickStartPosition;
	public int nickEndPosition;

	protected Rectangle textRect;
	protected Rectangle buttonRect;

	protected Map<TalkSpecific, Component> componentMap;
	//
	protected int alignment = ALIGNMENT_LEFT;
	protected boolean isBalloon = true;
	protected boolean isBackground = true;
	protected boolean isAvatar = true;
	protected int foreground;
	protected String fontFamily = "serif";
	protected int fontSize = 12;
	protected boolean underline = false;
	protected boolean italic = false;
	protected boolean bold = false;
	protected String backgroundBalloon;

	private ImageIcon avatar;
	private String nick;

	//0：标识为初始状态 loading，1：成功加载，2：加载失败
	protected int status = 0;
	protected String specMsg;

	protected TalkMessage talkMessage;

	protected Date date;

	public BalloonMessage(long id) {
		this.id = id;
	}

	public int getAlignment() {
		return alignment;
	}

	public void setAlignment(int alignment) {
		this.alignment = alignment;
	}

	public boolean isBalloon() {
		return isBalloon;
	}

	public void setBalloon(boolean isBalloon) {
		this.isBalloon = isBalloon;
	}

	public TalkMessage getTalkMessage() {
		return talkMessage;
	}

	public void setTalkMessage(TalkMessage talkMessage) {
		this.talkMessage = talkMessage;
	}

	public int getForeground() {
		return foreground;
	}

	public void setForeground(int foreground) {
		this.foreground = foreground;
	}

	public String getFontFamily() {
		return fontFamily;
	}

	public void setFontFamily(String fontFamily) {
		this.fontFamily = fontFamily;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public boolean isUnderline() {
		return underline;
	}

	public void setUnderline(boolean underline) {
		this.underline = underline;
	}

	public boolean isItalic() {
		return italic;
	}

	public void setItalic(boolean italic) {
		this.italic = italic;
	}

	public boolean isBold() {
		return bold;
	}

	public void setBold(boolean bold) {
		this.bold = bold;
	}

	public int getTimeStartPosition() {
		return timeStartPosition;
	}

	public void setTimeStartPosition(int timeStartPosition) {
		this.timeStartPosition = timeStartPosition;
	}

	public int getTimeEndPosition() {
		return timeEndPosition;
	}

	public void setTimeEndPosition(int timeEndPosition) {
		this.timeEndPosition = timeEndPosition;
	}

	public int getTextStartPosition() {
		return textStartPosition;
	}

	public void setTextStartPosition(int textStartPosition) {
		this.textStartPosition = textStartPosition;
	}

	public int getTextEndPosition() {
		return textEndPosition;
	}

	public void setTextEndPosition(int textEndPosition) {
		this.textEndPosition = textEndPosition;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Component getComponent(TalkSpecific msg) {
		if (componentMap == null) {
			return null;
		} else {
			return componentMap.get(msg);
		}
	}

	public void putComponent(TalkSpecific msg, Component label) {
		if (componentMap == null) {
			componentMap = new HashMap<TalkSpecific, Component>();
		}
		componentMap.put(msg, label);
	}

	public void clear() {
		if (componentMap != null) {
			componentMap.clear();
		}
		componentMap=new HashMap<>();
		length = 0;

		timeStartPosition = 0;
		timeEndPosition = 0;
		textStartPosition = 0;
		textEndPosition = 0;
		nickStartPosition = 0;
		nickEndPosition = 0;

		textRect = null;
		buttonRect = null;
	}

	public boolean isBackground() {
		return isBackground;
	}

	public void setBackground(boolean isBackground) {
		this.isBackground = isBackground;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int paintStatus) {
		this.status = paintStatus;
	}

	public Rectangle getTextRect() {
		return textRect;
	}

	public void setTextRect(Rectangle textRect) {
		this.textRect = textRect;
	}

	public Rectangle getButtonRect() {
		return buttonRect;
	}

	public void setButtonRect(Rectangle buttonRect) {
		this.buttonRect = buttonRect;
	}

	public void redo() {
	}

	public String getSpecMsg() {
		return specMsg;
	}

	public void setSpecMsg(String specMsg) {
		this.specMsg = specMsg;
	}

	public String getBackgroundBalloon() {
		return backgroundBalloon;
	}

	public void setBackgroundBalloon(String backgroundBalloon) {
		this.backgroundBalloon = backgroundBalloon;
	}

	public boolean isAvatar() {
		return isAvatar;
	}

	public void setAvatar(boolean isAvatar) {
		this.isAvatar = isAvatar;
	}

	public ImageIcon getAvatar() {
		return avatar;
	}

	public void setAvatar(ImageIcon avatar) {
		this.avatar = avatar;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public int getNickStartPosition() {
		return nickStartPosition;
	}

	public void setNickStartPosition(int nameStartPosition) {
		this.nickStartPosition = nameStartPosition;
	}

	public int getNickEndPosition() {
		return nickEndPosition;
	}

	public void setNickEndPosition(int nameEndPosition) {
		this.nickEndPosition = nameEndPosition;
	}

	public String getTimepattern() {
		return timepattern;
	}

	public void setTimepattern(String timepattern) {
		this.timepattern = timepattern;
	}

	public BalloonMessage clone() {
		BalloonMessage o = null;
		try {
			o = (BalloonMessage) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return o;
	}
}
