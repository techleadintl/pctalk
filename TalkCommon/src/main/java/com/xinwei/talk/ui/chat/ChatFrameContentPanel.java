package com.xinwei.talk.ui.chat;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.JComponent;
import javax.swing.JSplitPane;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.ThemeImage;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.lookandfeel.util.PainterUtil;
import com.xinwei.talk.manager.TalkManager;

@SuppressWarnings("serial")
public class ChatFrameContentPanel extends McWillContentPanel {
	private ThemeImage themeImage = null;
	private Color color1;
	private Color color2;

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		ChatContainer chatContainer = TalkManager.getChatManager().getChatContainer();
		JSplitPane splitPane = (JSplitPane) chatContainer.getComponent(0);
		JComponent leftComponent = (JComponent) splitPane.getLeftComponent();
		JComponent rightComponent = (JComponent) splitPane.getRightComponent();
		
		McWillTheme.setOpaque(rightComponent);
		
//		JComponent leftComponent = (JComponent) chatContainer.getComponent(0);
//		JComponent rightComponent = (JComponent) chatContainer.getComponent(1);

		ThemeColor themeColor = McWillTheme.getThemeColor();

		Color color1 = themeColor.getSrcColor();
		Color color2 = themeColor.getSubDarkColor();
		Color color4 = themeColor.getSubLightColor();
		Color color5 = themeColor.getLightColor();
		//左侧
		Rectangle leftBounds = leftComponent.getBounds();
		PainterUtil.paintBackgroudColor(g2d, color1, leftBounds.x, leftBounds.y, leftBounds.width, leftBounds.height);

		//右侧
		Rectangle rightBounds = rightComponent.getBounds();
		PainterUtil.paintBackgroudColor(g2d, color4, rightBounds.x, rightBounds.y, rightBounds.width, rightBounds.height);
		
		
		//		if (McWillTheme.isThemeColor()) {
		//
		//		} else if (McWillTheme.isThemeImage()) {
		//			
		//			
		//			rightComponent.setBorder(BorderFactory.createEtchedBorder());
		//			Rectangle rightBounds = rightComponent.getBounds();
		//			
		//			Rectangle leftBounds = leftComponent.getBounds();
		//			PainterUtil.paintBackgroudColor(g2d, color1, leftBounds.x, leftBounds.y, leftBounds.width, leftBounds.height);
		//			
		//			int height = 108;
		//			ThemeImage themeImage = McWillTheme.getThemeImage();
		//			Image image = themeImage.getImage();
		//			if (image != null) {
		////				PainterUtil.tileBackgroudImage(rightComponent, image, g2d, 1f);
		//				PainterUtil.paintBackgroudImage(rightComponent, image, g2d, 1f);
		//				Color color = themeColor.getSrcColor();
		//				
		//				color1 = ColorUtil.getAlphaColor(color, 0);
		//				
		//				color2 = ColorUtil.getAlphaColor(Color.WHITE, 230);
		//				if (this.themeImage != themeImage) {
		//					this.themeImage = themeImage;
		//				}
		//				Paint paint = g2d.getPaint();
		//				GradientPaint gp = new GradientPaint(rightBounds.x, rightBounds.y, color1, rightBounds.x, height, color2);
		//				g2d.setPaint(gp);
		//				g2d.fillRect(rightBounds.x, rightBounds.y, rightBounds.width, rightBounds.height);
		//				g2d.setPaint(paint);
		//			}else{
		//				PainterUtil.paintBackgroudColor(g2d, color4, rightBounds.x, rightBounds.y, rightBounds.width, rightBounds.height);
		//			}
		//		}
	}

}
