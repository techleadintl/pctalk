/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月21日 下午1:35:34
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room.history;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.lang.reflect.Method;

import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.plaf.basic.BasicGraphicsUtils;

import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.BaseTabbedPaneUI;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.util.ColorUtil;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;

@SuppressWarnings("serial")
public class ChatMessageTabbedPane extends JTabbedPane {
	ChatMessageTabbedPaneUI ui2;

	public ChatMessageTabbedPane(CommonChatRoom room) {
		addTab(Res.getMessage("button.chatroom.chat.history.all"), null, new HistoryTranscriptWinow(room));
		addTab(Res.getMessage("button.chatroom.chat.history.image"), null, new HistoryImageWindow(room));
		addTab(Res.getMessage("button.chatroom.chat.history.file"), null, new HistoryFileWindow(room));
		setPreferredSize(new Dimension(400, 100));
	}

	@Override
	public void updateUI() {
		if (ui2 == null) {
			ui2 = new ChatMessageTabbedPaneUI();
		}
		setUI(ui2);
	}

	public class ChatMessageTabbedPaneUI extends BaseTabbedPaneUI {
		protected void installDefaults() {
			super.installDefaults();
			tabInsets = new Insets(0, 10, 5, 10);
		}

		protected void paintRoundedTopTabBorder(int tabIndex, Graphics g, int x1, int y1, int x2, int y2, boolean isSelected) {
		}

		protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h) {

		}

		protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
		}

		protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
			if (isSelected || tabIndex == rolloverIndex) {
				Graphics2D g2D = (Graphics2D) g;
				Object savedRederingHint = g2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
				g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				Color borderColor = null;
				if (isSelected) {
					borderColor = McWillTheme.getThemeColor().getSrcColor();
				} else {
					borderColor = McWillTheme.getThemeColor().getMiddleColor();
				}
				g.setColor(borderColor);
				g.fillRoundRect(x, y, w, h, 8, 8);
				g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, savedRederingHint);

			}
		}

		protected void paintText(Graphics g, int tabPlacement, Font font, FontMetrics metrics, int tabIndex, String title, Rectangle textRect, boolean isSelected) {
			int mnemIndex = tabPane.getDisplayedMnemonicIndexAt(tabIndex);

			if (isSelected) {
				Color titleColor = AbstractLookAndFeel.getWindowTitleForegroundColor();
				if (ColorUtil.getGrayValue(titleColor) > 164)
					g.setColor(Color.black);
				else
					g.setColor(Color.white);
				g.setColor(titleColor);
			} else {
				g.setColor(tabPane.getForegroundAt(tabIndex));
			}
			drawStringUnderlineCharAt(tabPane, g, title, mnemIndex, textRect.x, textRect.y + metrics.getAscent());
		}

		public void drawStringUnderlineCharAt(JComponent c, Graphics g, String text, int underlinedIndex, int x, int y) {
			Graphics2D g2D = (Graphics2D) g;
			try {
				Class swingUtilities2Class = sun.swing.SwingUtilities2.class;
				Class classParams[] = { JComponent.class, Graphics.class, String.class, Integer.TYPE, Integer.TYPE, Integer.TYPE };
				Method m = swingUtilities2Class.getMethod("drawStringUnderlineCharAt", classParams);
				Object methodParams[] = { c, g, text, new Integer(underlinedIndex), new Integer(x), new Integer(y) };
				m.invoke(null, methodParams);
			} catch (Exception ex) {
				BasicGraphicsUtils.drawString(g, text, underlinedIndex, x, y);
			}
		}
	}

}
