package com.xinwei.talk.ui.updater;

public class TalkVersion {
	public static int YES = 0;
	public static int NO = -1;
	public static int MUST = 1;
	private String version;
	private String updateTime;
	private String downloadURL;
	private String versionMessage;

	public TalkVersion() {

	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	//	public Date getUpdateTime() {
	//		return new Date(updateTime);
	//	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getDownloadURL() {
		return downloadURL;
	}

	public void setDownloadURL(String downloadURL) {
		this.downloadURL = downloadURL;
	}

	public String getVersionMessage() {
		return versionMessage;
	}

	public void setVersionMessage(String versionMessage) {
		this.versionMessage = versionMessage;
	}

	public String getXML() {
		StringBuffer buf = new StringBuffer();
		buf.append("<talk>");
		buf.append("<version>").append(version).append("</version>");
		buf.append("<updateTime>").append(updateTime).append("</updateTime>");
		buf.append("<downloadURL>").append(downloadURL).append("</downloadURL>");
		buf.append("<versionMessage>").append(versionMessage).append("</versionMessage>");
		buf.append("</talk>");
		return buf.toString();
	}

}
