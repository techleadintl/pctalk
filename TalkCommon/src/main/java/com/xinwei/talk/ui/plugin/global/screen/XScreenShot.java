/*************************************************************
 * * Copyright (c) 2012-2020  信威
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2012-8-28 李拥10:56:11
 * 
 ***************************************************************/
package com.xinwei.talk.ui.plugin.global.screen;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

import com.xinwei.common.ui.ScreenShot;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.ui.util.ClipboardUtil;

public abstract class XScreenShot extends ScreenShot {
	private static final ImageIcon DOODLE_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/doodle.png", 18, 18);
	private static final ImageIcon ARROW_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/arrow.png", 18, 18);
	private static final ImageIcon OVAL_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/oval.png", 18, 18);
	private static final ImageIcon CANCEL_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/cancel.png", 18, 18);
	private static final ImageIcon OK_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/ok.png", 18, 18);
	private static final ImageIcon RECTANGLE_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/rectangle.png", 18, 18);
	private static final ImageIcon SAVE_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/save.png", 18, 18);
	private static final ImageIcon UNDO_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/undo.png", 18, 18);
	private static final ImageIcon VERTICAL_LINE_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/vertical_line.png");
	private static final ImageIcon STROKE2_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/stroke2.png", 18, 18);
	private static final ImageIcon STROKE4_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/stroke4.png", 18, 18);
	private static final ImageIcon STROKE6_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/stroke6.png", 18, 18);
	private static final ImageIcon COLOR1_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/color1.png", 18, 18);
	private static final ImageIcon COLOR2_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/color2.png", 18, 18);
	private static final ImageIcon COLOR3_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/color3.png", 18, 18);
	private static final ImageIcon COLOR4_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/color4.png", 18, 18);
	private static final ImageIcon COLOR5_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/color5.png", 18, 18);
	private static final ImageIcon COLOR6_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/color6.png", 18, 18);
	private static final ImageIcon COLOR7_IMAGE_ICON = LAF.getImageIcon("talk/images/room/screenshot/color7.png", 18, 18);

	@Override
	protected void doClipboard(BufferedImage captureImage) {
		ClipboardUtil.writeToClipboard(captureImage);
	}

	public abstract void doFinish(BufferedImage captureImage);

	protected String getRgbStr(Color pickColor) {
		return Res.getMessage("chatroom.screenshot.rgb", pickColor.getRed(), pickColor.getGreen(), pickColor.getBlue());
	}

	protected String getSizeStr(int w, int h) {
		return Res.getMessage("chatroom.screenshot.size", String.valueOf(w), String.valueOf(h));
	}

	protected ImageIcon getDoodleImageIcon() {
		return DOODLE_IMAGE_ICON;
	};

	protected ImageIcon getArrowImageIcon() {
		return ARROW_IMAGE_ICON;
	}

	protected ImageIcon getOvalImageIcon() {
		return OVAL_IMAGE_ICON;
	}

	protected ImageIcon getCancelImageIcon() {
		return CANCEL_IMAGE_ICON;
	}

	protected ImageIcon getOkImageIcon() {
		return OK_IMAGE_ICON;
	}

	protected ImageIcon getRectangleImageIcon() {
		return RECTANGLE_IMAGE_ICON;
	}

	protected ImageIcon getSaveImageIcon() {
		return SAVE_IMAGE_ICON;
	}

	protected ImageIcon getUndoImageIcon() {
		return UNDO_IMAGE_ICON;
	}

	protected ImageIcon getVerticalLineImageIcon() {
		return VERTICAL_LINE_IMAGE_ICON;
	}

	protected ImageIcon getStroke2ImageIcon() {
		return STROKE2_IMAGE_ICON;
	}

	protected ImageIcon getStroke4ImageIcon() {
		return STROKE4_IMAGE_ICON;
	}

	protected ImageIcon getStroke6ImageIcon() {
		return STROKE6_IMAGE_ICON;
	}

	protected ImageIcon getColor1ImageIcon() {
		return COLOR1_IMAGE_ICON;
	}

	protected ImageIcon getColor2ImageIcon() {
		return COLOR2_IMAGE_ICON;
	}

	protected ImageIcon getColor3ImageIcon() {
		return COLOR3_IMAGE_ICON;
	}

	protected ImageIcon getColor4ImageIcon() {
		return COLOR4_IMAGE_ICON;
	}

	protected ImageIcon getColor5ImageIcon() {
		return COLOR5_IMAGE_ICON;
	}

	protected ImageIcon getColor6ImageIcon() {
		return COLOR6_IMAGE_ICON;
	}

	protected ImageIcon getColor7ImageIcon() {
		return COLOR7_IMAGE_ICON;
	}

	protected java.util.List<? extends Image> getWindowImageList() {
		return LAF.getApplicationImages();
	}
}
