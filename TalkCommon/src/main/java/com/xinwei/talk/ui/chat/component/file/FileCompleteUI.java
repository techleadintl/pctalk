package com.xinwei.talk.ui.chat.component.file;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillLine;
import com.xinwei.common.lookandfeel.component.McWillTextPane;
import com.xinwei.spark.ui.MessageDialog;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.ui.chat.balloon.BalloonMessage;
import com.xinwei.talk.ui.chat.relay.RelayDialog;
import com.xinwei.talk.ui.util.component.label.LinkLabel;
import com.xinwei.talk.ui.util.component.room.ChatRoomComponent;

public class FileCompleteUI extends ChatRoomComponent {
	private static final long serialVersionUID = -4403839897649365671L;

	int FILE_NAME_MAX_LENGTH = 200;
	protected JLabel fileIconLabel;
	//	protected Color leftColor1 = LAF.getColor(188, 237, 245);
	//	protected Color leftBorderColor = LAF.getColor(155, 155, 155);
	protected Color leftColor1 = Color.WHITE;
	protected Color leftBorderColor = LAF.getColor(155, 155, 155);
	protected McWillTextPane fileNameTextPane;
	protected McWillTextPane filePathTextPane;

	protected LinkLabel openLabel;
	protected LinkLabel openDirLabel;
	protected LinkLabel forwardLabel;

	protected File file;
	protected TalkFileSpecific fileMsg;
	protected BalloonMessage balloonMsg;

	public FileCompleteUI(BalloonMessage balloonMsg, TalkFileSpecific fileMsg, final File file) {
		init(balloonMsg, fileMsg, file);
	}

	public void init(BalloonMessage balloonMsg, TalkFileSpecific fileMsg, final File file) {
		this.fileMsg = fileMsg;
		this.file = file;
		this.balloonMsg = balloonMsg;

		fileIconLabel = new JLabel();

		fileNameTextPane = new McWillTextPane();
		fileNameTextPane.setEditable(false);

		filePathTextPane = new McWillTextPane();
		filePathTextPane.setEditable(false);

		openLabel = new LinkLabel(Res.getMessage("button.open"), Color.blue, Color.red);
		openDirLabel = new LinkLabel(Res.getMessage("button.opendir"), Color.blue, Color.red);
		forwardLabel = new LinkLabel(Res.getMessage("button.forward"), Color.blue, Color.red);

		initMyData(file);

		addMyListeners();

		JPanel buttonPanel = new JPanel(new BorderLayout());
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(15, 0, 0, 0));

		JPanel downButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
		downButtonPanel.add(openLabel);
		downButtonPanel.add(openDirLabel);
		downButtonPanel.add(forwardLabel);
		buttonPanel.add(new McWillLine(), BorderLayout.NORTH);
		buttonPanel.add(downButtonPanel, BorderLayout.CENTER);

		JPanel infoPanel = new JPanel(new BorderLayout());
		infoPanel.add(fileNameTextPane, BorderLayout.NORTH);
		infoPanel.add(filePathTextPane, BorderLayout.CENTER);

		contentPanel.add(fileIconLabel, BorderLayout.WEST);
		contentPanel.add(infoPanel, BorderLayout.CENTER);
		contentPanel.add(buttonPanel, BorderLayout.SOUTH);
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 5));
		//		setPreferredSize(new Dimension(400, 150));
		//		setMaximumSize(new Dimension(400, 400));
	}

	private void initMyData(final File file) {
		fileIconLabel.setIcon(SwingUtil.getFileImageIcon(file));
		String text = SwingUtil.format(file.length());
		fileNameTextPane.setText(file.getName() + " (" + text + ")");
		filePathTextPane.setText(file.getAbsolutePath());
		filePathTextPane.setForeground(Color.gray);
		initData(file);

		//		String fileName = file.getName();
		//
		//		fileName = SwingUtil.getString(fileName, fileNameTextPane.getFont(), FILE_NAME_MAX_LENGTH, " (" + text + ")");
		//		fileNameTextPane.setText(fileName);

	}

	protected void initData(final File file) {
	}

	private void addMyListeners() {
		fileIconLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					SwingUtil.openFile(file);
				}
			}

			public void mouseEntered(MouseEvent e) {
				fileIconLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));

			}

			public void mouseExited(MouseEvent e) {
				fileIconLabel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
		});
		fileNameTextPane.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					SwingUtil.openFile(file);
				}
			}

			public void mouseEntered(MouseEvent e) {
				fileNameTextPane.setCursor(new Cursor(Cursor.HAND_CURSOR));

			}

			public void mouseExited(MouseEvent e) {
				fileNameTextPane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
		});

		openLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				boolean exists = file.exists();
				if (!exists) {
					MessageDialog.showAlert(TalkManager.getChatFrame(), Res.getMessage("message.file.exists.not", file.getAbsolutePath()));
					return;
				}
				SwingUtil.openFile(file);
			}
		});
		openDirLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				boolean exists = file.getParentFile().exists();
				if (!exists) {
					MessageDialog.showAlert(TalkManager.getChatFrame(), Res.getMessage("message.file.exists.not", file.getParentFile()));
					return;
				}
				SwingUtil.openFile(file.getParentFile());
			}
		});

		forwardLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				RelayDialog relayDialog = TalkManager.getChatFrame().getRelayDialog();
				if (relayDialog != null && relayDialog.isVisible()) {
					if (relayDialog.getBalloonMsg().getId() == balloonMsg.getId()) {
						return;
					}
					relayDialog.setVisible(false);
				}
				relayDialog = new RelayDialog(balloonMsg);
				relayDialog.setVisible(true);
				TalkManager.getChatFrame().setRelayDialog(relayDialog);
			}
		});

		addListeners();
	}

	protected void addListeners() {

	}
}
