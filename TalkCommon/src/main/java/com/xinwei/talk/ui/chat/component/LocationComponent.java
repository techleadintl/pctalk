/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月27日 上午11:04:30
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.component;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.model.message.specific.TalkLocationSpecific;

public class LocationComponent extends JButton {
	private static ImageIcon imageIcon = LAF.getMsgLocation();
	//t= m-normal map, k–satellite,h–hybrid,p–terrain
	private String GOOGLE = "http://maps.google.com/maps?&z=15&mrt=yp&t=k&q={0}+{1}";
	//	private String BAIDU = "http://api.map.baidu.com/geocoder?location={0},{1}&coord_type=gcj02&output=html";
	String BAIDU = "http://api.map.baidu.com/marker?location={0},{1}&title={3}的位置&content={2}&output=html";

	private String mapUrl = null;

	public LocationComponent(final TalkLocationSpecific msg, final String nick) {
		setText(msg.getAddr());

		setContentAreaFilled(false);
		setBorderPainted(false);
		setCursor(new Cursor(Cursor.HAND_CURSOR));
		setFocusPainted(false);
		setHorizontalAlignment(SwingConstants.LEFT);
		setBorder(BorderFactory.createEmptyBorder());

		setForeground(Color.darkGray);
		setVerticalAlignment(SwingConstants.BOTTOM);
		setHorizontalAlignment(SwingConstants.CENTER);
		setPreferredSize(new Dimension(0, 145));
		setMaximumSize(new Dimension(300, 145));

		Locale locale = Locale.getDefault();

		if (locale.getLanguage().equals("zh")) {
			mapUrl = BAIDU;
		} else {
			mapUrl = GOOGLE;
		}
		
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtil.openBrowseURL(MessageFormat.format(mapUrl, msg.getLat(), msg.getLng(), msg.getAddr(), nick));
			}
		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(imageIcon.getImage(), 0, 0, getWidth(), getHeight(), this);
		super.paintComponent(g);
	}

}
