package com.xinwei.talk.ui.main.dialog.member.model;

import java.util.ArrayList;
import java.util.List;

public class MemberInfo {
	public String jid;
	public String displayName;
	public String uid;
	public String userNameWithTone;
	public String userNameWithoutTone;
	public List<String> searchKeys = new ArrayList<String>();
	public String caption;

	public boolean search(String text) {
		text = text.toLowerCase();
		for (String searchKey : searchKeys) {
			if (searchKey.contains(text)) {
				return true;
			}
		}
		return false;
	}
}