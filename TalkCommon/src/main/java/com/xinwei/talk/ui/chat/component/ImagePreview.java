package com.xinwei.talk.ui.chat.component;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicRootPaneUI;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.LAF;

public class ImagePreview {
	ImageIcon _1_1Image = LAF.getImageIcon("talk/images/imagepreview/_1_1.png");

	ImageIcon zoominImage = LAF.getImageIcon("talk/images/imagepreview/zoomin.png");
	ImageIcon zoomoutImage = LAF.getImageIcon("talk/images/imagepreview/zoomout.png");

	ImageIcon screenfullImage = LAF.getImageIcon("talk/images/imagepreview/screenfull.png");
	ImageIcon screenrestoreImage = LAF.getImageIcon("talk/images/imagepreview/screenrestore.png");

	ImageIcon rotateImage = LAF.getImageIcon("talk/images/imagepreview/rotate.png");
	ImageIcon saveImage = LAF.getImageIcon("talk/images/imagepreview/save.png");

	ImageIcon deleteImage1 = LAF.getImageIcon("talk/images/imagepreview/delete1.png");
	ImageIcon deleteImage2 = LAF.getImageIcon("talk/images/imagepreview/delete2.png");
	ImageIcon deleteBigImage1 = LAF.getImageIcon("talk/images/imagepreview/delete3.png");
	ImageIcon deleteBigImage2 = LAF.getImageIcon("talk/images/imagepreview/delete4.png");

	ImageIcon[] deleteImages = new ImageIcon[] { deleteImage1, deleteImage2 };
	ImageIcon[] deleteBigImages = new ImageIcon[] { deleteBigImage1, deleteBigImage2 };

	final Toolkit defaultToolkit = java.awt.Toolkit.getDefaultToolkit();

	private int minWindowW = 550;
	private int minWindowH = 350;
	private double limitMin = 0.1;
	private double limitMax = 5;

	private double step = 0.1;
	private double current = 1;

	JDialog window;

	boolean max;

	int BORDER = 12;

	BufferedImage srcImage;

	int srcImageW = 0;
	int srcImageH = 0;

	int imageW = 0;
	int imageH = 0;

	ImagePreviewPane imagePreviewPane;

	McWillIconButton closeButton;

	JPanel buttonPanel;

	McWillIconButton srcButton;

	McWillIconButton screenButton;

	McWillIconButton zoomInButton;

	McWillIconButton zoomOutButton;

	McWillIconButton rotateButton;

	McWillIconButton saveButton;

	private Rectangle areaRec = new Rectangle();
	Rectangle tmpAreaRec;

	private int startX, startY, endX, endY, pressedX, pressedY;
	private Rectangle[] recs = new Rectangle[9];// 
	private Rectangle buttonRec = new Rectangle();// 
	private States currentState = States.DEFAULT;//

	private boolean isIn;

	private long percentTime;

	private boolean isShowPercent;

	public void setVisible() {
		if (isVisible())
			return;
		window.setVisible(true);

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				while (window.isVisible()) {
					boolean flag = false;
					if (isShowPercent && System.currentTimeMillis() - percentTime > 1000) {
						isShowPercent = false;
						flag = true;
					}
					if (flag) {
						imagePreviewPane.repaint();
					}
					SystemUtil.sleep(250);
				}
			}
		});
	}

	public boolean isVisible() {
		return window.isVisible();
	}

	public ImagePreview(final Window parentWindow, final BufferedImage srcImage) {
		this.srcImage = srcImage;

		window = new JDialog(parentWindow) {
			BasicRootPaneUI ui2 = new BasicRootPaneUI();

			protected JRootPane createRootPane() {
				JRootPane rp = new JRootPane() {
					@Override
					public void updateUI() {
						setUI(ui2);
					}
				};
				rp.setOpaque(true);
				return rp;
			}
		};
		window.setIconImages(LAF.getApplicationImages());
		initWindowImage(srcImage);

		window.setUndecorated(true);
		window.setBackground(new Color(0, 0, 0, 0));

		imagePreviewPane = new ImagePreviewPane();
		ImagePreviewMouseListener l = new ImagePreviewMouseListener();
		imagePreviewPane.addMouseListener(l);
		imagePreviewPane.addMouseWheelListener(l);
		imagePreviewPane.addMouseMotionListener(l);

		window.setContentPane(imagePreviewPane);
		//		window.setLocationRelativeTo(parentWindow);
		//		window.setAlwaysOnTop(true);
	}

	protected void initWindowImage(final BufferedImage srcImage) {
		Rectangle screenRectangle = SwingUtil.getScreenRectangle(window);
		screenRectangle.width -= 40;
		screenRectangle.height -= 40;

		srcImageW = srcImage.getWidth();
		srcImageH = srcImage.getHeight();
		int windowW = 0;
		int windowH = 0;
		double x = 1d;

		if (srcImageW <= minWindowW && srcImageH < minWindowH) {
			windowW = minWindowW;
			windowH = minWindowH;
		} else if (srcImageW <= screenRectangle.width && srcImageH <= screenRectangle.height) {
			windowW = srcImageW;
			windowH = srcImageH;
			minWindowH = Math.min(windowH, minWindowH);
			minWindowW = Math.min(windowW, minWindowW);
		} else {
			while (!(srcImageW * x < screenRectangle.width && srcImageH * x < screenRectangle.height)) {
				x -= 0.1;
			}
			windowW = Double.valueOf(srcImageW * x).intValue();
			windowH = Double.valueOf(srcImageH * x).intValue();
		}
		setImagePercent(windowW, windowH);

		Rectangle rec = SwingUtil.getScreenRectangle(window);

		updateAreaRec((rec.width - windowW) / 2, (rec.height - windowH) / 2, windowW, windowH);

		window.setSize(rec.width, rec.height);
	}

	public void setImagePercent(int componentW, int componentH) {
		imageW = srcImageW = srcImage.getWidth(null);
		imageH = srcImageH = srcImage.getHeight(null);

		if (srcImageH > componentH || srcImageW > componentW) {
			if (srcImageW / srcImageH > componentW / componentH) {
				imageW = componentW;
				imageH = srcImageH * componentW / srcImageW;
			} else {
				imageH = componentH;
				imageW = srcImageW * componentH / srcImageH;
			}
		}
		current = (double) imageH / srcImageH;
		setImageSize();
	}

	public void setImageSize() {
		imageW = Double.valueOf(srcImageW * current).intValue();
		imageH = Double.valueOf(srcImageH * current).intValue();
		percentTime = System.currentTimeMillis();
		isShowPercent = true;
	}

	int span = 3;

	public void updateAreaRec(int x, int y, int w, int h) {
		areaRec.x = x;
		areaRec.y = y;
		areaRec.width = w;
		areaRec.height = h;

		int x1 = x;
		int y1 = y + BORDER;
		int x2 = w + x - BORDER;
		int y2 = h + y;
		int width = x2 - x1;
		int height = y2 - y1;

		recs[0] = new Rectangle(x1, y1, span, span);//左上
		recs[1] = new Rectangle(x1 + span, y1, width - 2 * span, span);//上中
		recs[2] = new Rectangle(x2 - span, y1, span, span);//右上
		recs[3] = new Rectangle(x2 - span, y1 + span, span, height - 2 * span);//右中
		recs[4] = new Rectangle(x2 - span, y2 - span, span, span);//右下
		recs[5] = new Rectangle(x1 + span, y2 - span, width - 2 * span, span);//下中
		recs[6] = new Rectangle(x1, y2 - span, span, span);//左下
		recs[7] = new Rectangle(x1, y1 + span, span, height - 2 * span);//左中
		recs[8] = new Rectangle(x1 + span + 1, y1 + span + 1, width - 2 * span - 2, height - 2 * span - 2);//中间区域

		buttonRec = new Rectangle(x1 + span + 1, y2 - 40, width - 2 * span - 2, 40 - span);//按钮区域
	}

	public void updateAreaRec(Rectangle rec) {
		updateAreaRec(rec.x, rec.y, rec.width, rec.height);
	}

	public class ImagePreviewPane extends JPanel {
		public ImagePreviewPane() {
			setOpaque(false);//不能删除。跟paintComponent有关
			setLayout(null);

			closeButton = new McWillIconButton(deleteImages);

			buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 4, 0));
			buttonPanel.setOpaque(false);

			Dimension dimension = new Dimension(35, 35);
			srcButton = new McWillIconButton(_1_1Image);
			srcButton.setPreferredSize(dimension);

			screenButton = new McWillIconButton(screenfullImage);
			screenButton.setPreferredSize(dimension);

			zoomInButton = new McWillIconButton(zoominImage);
			zoomInButton.setPreferredSize(dimension);

			zoomOutButton = new McWillIconButton(zoomoutImage);
			zoomOutButton.setPreferredSize(dimension);

			rotateButton = new McWillIconButton(rotateImage);
			rotateButton.setPreferredSize(dimension);

			saveButton = new McWillIconButton(saveImage);
			saveButton.setPreferredSize(dimension);

			buttonPanel.add(srcButton);
			buttonPanel.add(screenButton);
			buttonPanel.add(zoomInButton);
			buttonPanel.add(zoomOutButton);
			buttonPanel.add(rotateButton);
			buttonPanel.add(saveButton);

			add(closeButton);
			add(buttonPanel);

			ImagePreviewActionListener al = new ImagePreviewActionListener();
			srcButton.addActionListener(al);
			screenButton.addActionListener(al);
			zoomInButton.addActionListener(al);
			zoomOutButton.addActionListener(al);
			rotateButton.addActionListener(al);
			saveButton.addActionListener(al);

			closeButton.addActionListener(al);
		}

		@Override
		protected synchronized void paintComponent(Graphics g) {
			Graphics2D g2d = (Graphics2D) g.create();
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

			int width = getWidth();
			int height = getHeight();

			g2d.setComposite(AlphaComposite.SrcOver.derive(0.0f));
			g2d.setColor(getBackground());
			g2d.fillRect(0, 0, width, height);

			g2d.setComposite(AlphaComposite.SrcOver.derive(0.60f));
			g2d.setColor(Color.BLACK);
			g2d.fillRoundRect(areaRec.x, areaRec.y + BORDER, areaRec.width - BORDER, areaRec.height - BORDER, 10, 10);

			paintImage(g2d);

			if (isShowPercent) {
				paintPercent(g2d);
			}

			paintCloseImage(g2d);
			if (isIn) {
				paintButtonPanel(g2d);
			} else {
				buttonPanel.setVisible(false);
			}
			g2d.dispose();

		}

		protected void paintImage(Graphics2D g2d) {
			if (imageW <= 0 || imageH <= 0)
				return;

			int componentW = areaRec.width - BORDER;
			int componentH = areaRec.height - BORDER;
			int y = 0;
			int x = 0;
			int w = imageW;
			int h = imageH;

//			Image newSrcImage = createImage(imageW, imageH);
//			Graphics newG = newSrcImage.getGraphics();
//			newG.drawImage(srcImage, 0, 0, imageW, imageH, this);
//			newG.dispose();
			
			Image newSrcImage=ImageUtil.resizeHigh(srcImage,imageW, imageH);

			Image destImage = newSrcImage;

			if (imageH > componentH || imageW > componentW) {
				if (imageW > componentW) {
					x = (imageW - componentW) / 2;
					w = componentW;
				}
				if (imageH > componentH) {
					y = (imageH - componentH) / 2;
					h = componentH;
				}
				destImage = ImageUtil.getSubimage(newSrcImage, x, y, w, h);
			}
			g2d.setComposite(AlphaComposite.SrcOver.derive(1f));
			g2d.setColor(getBackground());
			g2d.fillRect(areaRec.x + (componentW - w) / 2, areaRec.y + (componentH - h) / 2 + BORDER, w, h);
			g2d.drawImage(destImage, areaRec.x + (componentW - w) / 2, areaRec.y + (componentH - h) / 2 + BORDER, w, h, this);
		}

		Font font = LAF.getFont(14, true);

		protected void paintPercent(Graphics2D g2d) {
			int componentW = areaRec.width - BORDER;
			int componentH = areaRec.height - BORDER;
			g2d.setComposite(AlphaComposite.SrcOver.derive(0.6f));
			g2d.setColor(Color.BLACK);
			g2d.fillRoundRect(areaRec.x + (componentW - 50) / 2, areaRec.y + (componentH - 25) / 2, 50, 25, 10, 10);

			String str = Double.valueOf(current * 100).intValue() + "%";
			g2d.setFont(font);
			FontMetrics fontMetrics = g2d.getFontMetrics();
			g2d.setComposite(AlphaComposite.SrcOver.derive(1f));
			g2d.setColor(getBackground());
			Rectangle2D stringBounds = fontMetrics.getStringBounds(str, g2d);
			g2d.fillRect((int) stringBounds.getX(), (int) stringBounds.getY(), (int) stringBounds.getWidth(), (int) stringBounds.getHeight());

			g2d.setColor(Color.WHITE);
			g2d.drawString(str, areaRec.x + (componentW - (int) stringBounds.getWidth()) / 2 + 3, areaRec.y + (componentH + (int) stringBounds.getHeight()) / 2 - 3);
		}

		protected void paintCloseImage(Graphics2D g2d) {
			g2d.setComposite(AlphaComposite.SrcOver.derive(1f));
			g2d.setColor(Color.WHITE);
			int xx = 2;
			if (tmpAreaRec != null) {
				closeButton.setBounds(areaRec.x + areaRec.width - BORDER * 4, areaRec.y, BORDER * 4, BORDER * 4 + 2);
				g2d.fillOval(areaRec.x + areaRec.width - BORDER * 4 + xx, areaRec.y + xx, BORDER * 4 - xx * 2, BORDER * 4 - xx * 2);
			} else {
				closeButton.setBounds(areaRec.x + areaRec.width - BORDER * 2, areaRec.y, BORDER * 2, BORDER * 2 + 2);
				g2d.fillOval(areaRec.x + areaRec.width - BORDER * 2 + xx, areaRec.y + xx, BORDER * 2 - xx * 2, BORDER * 2 - xx * 2);
			}
		}

		protected void paintButtonPanel(Graphics2D g2d) {
			g2d.setColor(Color.BLACK);
			g2d.setComposite(AlphaComposite.SrcOver.derive(0.6f));
			g2d.fillRoundRect(areaRec.x + (areaRec.width - 250) / 2, areaRec.y + areaRec.height - 35, 250, 60, 15, 15);

			g2d.clearRect(areaRec.x + (areaRec.width - 250) / 2, areaRec.y + areaRec.height, 250, 40);

			buttonPanel.setBounds(areaRec.x + (areaRec.width - 250) / 2, areaRec.y + areaRec.height - 35, 250, 35);

			buttonPanel.setVisible(true);
		}
	}

	class ImagePreviewActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object source = e.getSource();
			if (source == closeButton) {//关闭
				SwingUtilities.getWindowAncestor(closeButton).dispose();
			} else if (source == saveButton) {//保存
				SwingUtil.saveImageFile(window, srcImage);
			} else {
				if (source == srcButton) {//1:1
					current = 1;
					setImageSize();
				} else if (source == screenButton) {//全屏
					if (tmpAreaRec != null) {//还原全屏，正常
						updateAreaRec(tmpAreaRec);

						tmpAreaRec = null;
						closeButton.updateIcons(deleteImages);
						screenButton.updateIcons(new Icon[] { screenfullImage });
					} else {//全屏
						tmpAreaRec = new Rectangle(areaRec);

						screenButton.updateIcons(new Icon[] { screenrestoreImage });
						closeButton.updateIcons(deleteBigImages);

						final Rectangle screenRec = SwingUtil.getScreenRectangle(window);

						updateAreaRec(screenRec.x, screenRec.y - BORDER, screenRec.width + BORDER, screenRec.height + BORDER + 5);
					}
				} else if (source == zoomInButton) {//放大
					current += step;
					if (current > limitMax) {
						current = limitMax;
					}
					setImageSize();
				} else if (source == zoomOutButton) {//缩小
					current -= step;
					if (current < limitMin) {
						current = limitMin;
					}
					setImageSize();
				} else if (source == rotateButton) {//旋转
					AffineTransform tx = new AffineTransform();
					tx.translate(srcImage.getHeight() / 2, srcImage.getWidth() / 2);
					tx.rotate(Math.PI / 2);
					tx.translate(-srcImage.getWidth() / 2, -srcImage.getHeight() / 2);

					AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
					srcImage = op.filter(srcImage, null);

					setImagePercent(areaRec.width, areaRec.height);
				}
				imagePreviewPane.repaint();
			}
		}

	}

	class ImagePreviewMouseListener extends MouseAdapter {
		@Override
		public void mouseMoved(MouseEvent e) {
			Point point = e.getPoint();
			if (buttonRec.contains(point)) {
				if (!isIn) {
					isIn = true;
					currentState = States.CENTER;
					imagePreviewPane.setCursor(currentState.getCursor());

				}
				if (!buttonPanel.isVisible()) {
					imagePreviewPane.repaint();
				}
			} else {
				if (isIn) {
					isIn = false;
					if (buttonPanel.isVisible()) {
						buttonPanel.setVisible(false);
					}
				}
			}

			if (tmpAreaRec != null) {
				return;
			}

			States[] st = States.values();
			for (int i = 0; i < recs.length; i++) {
				if (recs[i] == null)
					continue;
				if (recs[i].contains(point)) {
					currentState = st[i];
					imagePreviewPane.setCursor(currentState.getCursor());
					return;
				}
			}
			currentState = States.DEFAULT;

			imagePreviewPane.setCursor(currentState.getCursor());
		}

		public void mousePressed(MouseEvent e) {
			pressedX = e.getX();
			pressedY = e.getY();

			startX = areaRec.x;
			startY = areaRec.y;
			endX = areaRec.x + areaRec.width;
			endY = areaRec.y + areaRec.height;
		}

		public void mouseDragged(MouseEvent e) {
			if (tmpAreaRec != null) {
				return;
			}

			int x = e.getX();
			int y = e.getY();

			Rectangle screenRectangle = SwingUtil.getScreenRectangle(window);

			if (currentState == States.CENTER) {
				/** 获取屏幕的边界 */
				if (endY + (y - pressedY) > screenRectangle.getHeight() - 5) {
					return;
				}
				if (endX + (x - pressedX) > screenRectangle.getWidth() - 5) {
					return;
				}
				if (startX + x - pressedX < screenRectangle.x + 5) {
					return;
				}
				if (startY + y - pressedY < screenRectangle.y + 5) {
					return;
				}
				startX += (x - pressedX);
				startY += (y - pressedY);
				endX += (x - pressedX);
				endY += (y - pressedY);
				pressedX = x;
				pressedY = y;

			} else if (currentState == States.EAST) {
				if (x - areaRec.x < minWindowW) {
					return;
				}
				endX = x;
			} else if (currentState == States.WEST) {
				if (areaRec.x + areaRec.width - x < minWindowW) {
					return;
				}
				startX = x;
			} else if (currentState == States.NORTH) {
				if (areaRec.y + areaRec.height - y < minWindowH) {
					return;
				}
				startY = y;
			} else if (currentState == States.SOUTH) {
				if (y - areaRec.y < minWindowH) {
					return;
				}
				endY = y;
			} else if (currentState == States.NORTH_EAST) {
				if (x - areaRec.x < minWindowW) {
					return;
				}
				if (areaRec.y + areaRec.height - y < minWindowH) {
					return;
				}
				endX = x;
				startY = y;
			} else if (currentState == States.NORTH_WEST) {
				if (areaRec.x + areaRec.width - x < minWindowW) {
					return;
				}
				if (areaRec.y + areaRec.height - y < minWindowH) {
					return;
				}
				startX = x;
				startY = y;
			} else if (currentState == States.SOUTH_EAST) {
				if (x - areaRec.x < minWindowW) {
					return;
				}
				if (y - areaRec.y < minWindowH) {
					return;
				}
				endX = x;
				endY = y;
			} else if (currentState == States.SOUTH_WEST) {
				if (areaRec.x + areaRec.width - x < minWindowW) {
					return;
				}
				if (y - areaRec.y < minWindowH) {
					return;
				}
				startX = x;
				endY = y;
			}

			updateAreaRec(startX, startY, endX - startX, endY - startY);

			imagePreviewPane.repaint();

		}

		int i = 0;

		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
				SwingUtilities.getWindowAncestor(e.getComponent()).dispose();
			}
		}

		public void mouseWheelMoved(MouseWheelEvent e) {
			int wheelRotation = e.getWheelRotation();
			if (wheelRotation == 1) { //滑轮向后
				current -= step;
				if (current < limitMin) {
					current = limitMin;
				}
			} else if (e.getWheelRotation() == -1) {//滑轮向前
				current += step;
				if (current > limitMax) {
					current = limitMax;
				}
			} else {
			}

			setImageSize();

			imagePreviewPane.repaint();
		}

		int span = 5;

	}

	enum States {
		NORTH_WEST(new Cursor(Cursor.NW_RESIZE_CURSOR)), //0
		NORTH(new Cursor(Cursor.N_RESIZE_CURSOR)), //1
		NORTH_EAST(new Cursor(Cursor.NE_RESIZE_CURSOR)), //2
		EAST(new Cursor(Cursor.E_RESIZE_CURSOR)), //3
		SOUTH_EAST(new Cursor(Cursor.SE_RESIZE_CURSOR)), //4
		SOUTH(new Cursor(Cursor.S_RESIZE_CURSOR)), //5
		SOUTH_WEST(new Cursor(Cursor.SW_RESIZE_CURSOR)), //6
		WEST(new Cursor(Cursor.W_RESIZE_CURSOR)), //7
		CENTER(new Cursor(Cursor.DEFAULT_CURSOR)), //8
		DEFAULT(new Cursor(Cursor.DEFAULT_CURSOR));//10

		private Cursor cursor;

		States(Cursor cursor) {
			this.cursor = cursor;
		}

		public Cursor getCursor() {
			return cursor;
		}
	}
}