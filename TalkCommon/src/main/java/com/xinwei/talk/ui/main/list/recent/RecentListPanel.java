package com.xinwei.talk.ui.main.list.recent;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.swing.JPopupMenu;

import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.DateUtil;
import com.xinwei.talk.action.XAction;
import com.xinwei.talk.action.group.GroupDissolveAction;
import com.xinwei.talk.action.group.GroupInviteAction;
import com.xinwei.talk.action.group.GroupOpenRoomAction;
import com.xinwei.talk.action.group.GroupQuitAction;
import com.xinwei.talk.action.group.GroupUpdateAction;
import com.xinwei.talk.action.talk.TalkInfoAction;
import com.xinwei.talk.action.talk.TalkOpenAction;
import com.xinwei.talk.common.Constants;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.common.TalkUtil2;
import com.xinwei.talk.listener.RecentMessageListener;
import com.xinwei.talk.manager.ChatManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.XWObject;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.main.list.ListPanel;

public class RecentListPanel extends ListPanel<XWObject> {
	private static final long serialVersionUID = -8486385306563904232L;

	public void addListeners() {
		super.addListeners();
		ListenerManager.addRecentMessageListener(new MyMessageListener());
	}

	@Override
	protected void addPopuMenuItemList(JPopupMenu popup, XWObject object) {
		String userTel = TalkUtil.getUserTel(object.getJid());
		if (!Constants.CUSTOM_TALK_TEL_GROUP_MESSAGE.equals(userTel) && !Constants.CUSTOM_TALK_TEL_SYSTEM_MESSAGE.equals(userTel)) {

		}
		if (Constants.CUSTOM_TALK_TEL_GROUP_MESSAGE.equals(userTel) || Constants.CUSTOM_TALK_TEL_SYSTEM_MESSAGE.equals(userTel)) {
			TalkUtil2.addPopunActions(popup,"activateChatRoom", object);
		} else {
			if (object instanceof Talk) {
				TalkUtil2.addPopunActions(popup,"talk", object);
			} else if (object instanceof Group) {
				TalkUtil2.addPopunActions(popup,"group", object);
			} else {
				TalkUtil2.addPopunActions(popup,"activateChatRoom", object);
			}
		}
	}

	class MyMessageListener implements RecentMessageListener {
		@Override
		public synchronized void message(Date serverTime, List<TalkMessage> tmList) {
			if (CollectionUtil.isEmpty(tmList)) {
				return;
			}
			Date today = DateUtil.getDate(serverTime, "yyyy-MM-dd");
			Date year = DateUtil.getDate(serverTime, "yyyy");

			for (TalkMessage talkMessage : tmList) {
				RecentItem recentItem = (RecentItem) getListItem(talkMessage.getJid());
				if (recentItem == null) {
					boolean isGroup = TalkUtil.isGroup(talkMessage.getJid());
					if (isGroup) {
						Group group = TalkManager.getLocalManager().getGroupByJid(talkMessage.getJid());
						if (group != null) {
							recentItem = new RecentItem(group);
						}
					} else {
						Talk talk = TalkManager.getLocalManager().getTalkByJid(talkMessage.getJid());
						if (talk != null) {
							recentItem = new RecentItem(talk);
						}
					}
					if (recentItem == null) {
						recentItem = new RecentItem(new XWObject(talkMessage.getJid()));
					}
					listModel.addElement(recentItem);
					//////////
					Talk groupMessageTalk = new Talk();
					//		groupMessageTalk.setAvatar(LAF.getDefaultAvatarIcon());
					groupMessageTalk.setJid(TalkUtil.getUserFullJid(Constants.CUSTOM_TALK_TEL_GROUP_MESSAGE));
					groupMessageTalk.setTel(Constants.CUSTOM_TALK_TEL_GROUP_MESSAGE);
					groupMessageTalk.setNick(Res.getMessage("custom.talk.nick.group.message"));
					groupMessageTalk.setSignature("");
					groupMessageTalk.setUid(String.valueOf(Constants.CUSTOM_TALK_TEL_GROUP_MESSAGE.hashCode()));
					//系统消息用户，出现最近联系人中
					Talk systemMessageTalk = new Talk();
					//		groupMessageTalk.setAvatar(LAF.getDefaultAvatarIcon());
					systemMessageTalk.setJid(TalkUtil.getUserFullJid(Constants.CUSTOM_TALK_TEL_SYSTEM_MESSAGE));
					systemMessageTalk.setTel(Constants.CUSTOM_TALK_TEL_SYSTEM_MESSAGE);
					systemMessageTalk.setNick(Res.getMessage("custom.talk.nick.system.message"));
					systemMessageTalk.setSignature("");
					systemMessageTalk.setUid(String.valueOf(Constants.CUSTOM_TALK_TEL_SYSTEM_MESSAGE.hashCode()));

				}
				recentItem.setTalkMessage(today, year, talkMessage);
			}

			RecentItem[] array = new RecentItem[listModel.size()];

			listModel.copyInto(array);

			List<RecentItem> itemList = Arrays.asList(array);

			java.util.Collections.sort(itemList, new Comparator<RecentItem>() {
				public int compare(RecentItem o1, RecentItem o2) {
					Long id1 = o1.getTalkMessage().getId();
					Long id2 = o2.getTalkMessage().getId();
					return id2.compareTo(id1);
				};
			});

			//			for (int i = 0; i < list.size(); i++) {
			//				listModel.remove(i);
			//			}

			listModel.removeAllElements();
			list.updateUI();
			for (RecentItem item : itemList) {
				listModel.addElement(item);
			}
		}

	}
}
