/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月20日 上午11:38:06
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.jivesoftware.smack.packet.Message;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.spark.ModelUtil;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.Constants;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ChatManager;
import com.xinwei.talk.manager.MessageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.specific.TalkParam;
import com.xinwei.talk.model.message.specific.TalkTextSpecific;
import com.xinwei.talk.ui.chat.balloon.BalloonMessage;
import com.xinwei.talk.ui.chat.relay.RelayDialog;
import com.xinwei.talk.ui.chat.room.ChatRoom;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.util.ClipboardUtil;

public class TranscriptWindow extends TalkTextPane implements MouseListener, MouseMotionListener, ActionListener {

	private static final long serialVersionUID = -2168845249388070573L;
	protected final Set<String> times = new HashSet<String>();

	protected final SimpleDateFormat notificationDateFormatter;
	protected final String notificationDateFormat = ((SimpleDateFormat) SimpleDateFormat.getDateInstance(SimpleDateFormat.FULL)).toPattern();

	protected Date lastUpdated;

	protected Date lastPost;

	//作用:消息重发功能
	private Map<Long, BalloonMessage> magMap = new HashMap<>();

	private JPopupMenu popup = new JPopupMenu();

	private JMenuItem copyMenu;
	private JMenuItem selectAllMenu;

	/**
	 * Creates a default instance of <code>TranscriptWindow</code>.
	 */
	public TranscriptWindow() {
		setEditable(false);
		notificationDateFormatter = new SimpleDateFormat(notificationDateFormat);

		copyMenu = new JMenuItem(Res.getMessage("action.copy"));
		copyMenu.addActionListener(this);

		selectAllMenu = new JMenuItem(Res.getMessage("action.select.all"), LAF.getSelectAllIcon()[0]);
		selectAllMenu.addActionListener(this);

		// Make sure ctrl-c works
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("Ctrl c"), "copy");
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("Ctrl a"), "select all");

		getActionMap().put("copy", new AbstractAction("copy") {
			private static final long serialVersionUID = 4949716854440264528L;

			public void actionPerformed(ActionEvent evt) {
				SwingUtil.setClipboard(getSelectedText());
			}
		});
		getActionMap().put("select all", new AbstractAction("select all") {
			private static final long serialVersionUID = 1797491846835591379L;

			public void actionPerformed(ActionEvent evt) {
				selectAll();
			}
		});

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == copyMenu) {
			//			SwingUtil.setClipboard(getSelectedText());
			ClipboardUtil.copy(this);
		} else if (e.getSource() == selectAllMenu) {
			requestFocus();
			selectAll();
		}
	}

	public void mousePressed(MouseEvent e) {
		if (e.isPopupTrigger()) {
			handlePopup(e);
		}
	}

	/**
	 * This launches the <code>BrowserLauncher</code> with the URL located in <code>ChatArea</code>. Note that the url will automatically be clickable when added to <code>ChatArea</code>
	 *
	 * @param e
	 *            - the MouseReleased event
	 */
	public void mouseReleased(MouseEvent e) {
		if (e.isPopupTrigger()) {
			handlePopup(e);
		}
	}

	public void mouseMoved(MouseEvent e) {
		try {
			final int pos = viewToModel(e.getPoint());
			final Element element = getStyledDocument().getCharacterElement(pos);

			if (element != null) {
				final AttributeSet as = element.getAttributes();
				final Object o = as.getAttribute("link");

				if (o != null) {
					setCursor(HAND_CURSOR);
				} else {
					setCursor(DEFAULT_CURSOR);
				}
			}
		} catch (Exception ex) {
			Log.error("Error in CheckLink:", ex);
		}
	}

	private void handlePopup(MouseEvent e) {
		popup.removeAll();

		addPopupMenus(popup, e);

		popup.show(this, e.getX(), e.getY());
	}

	/**
	 * Adds Print and Clear actions.
	 *
	 * @param object
	 *            the TransferWindow
	 * @param popup
	 *            the popup menu to add to.
	 */
	@SuppressWarnings("serial")
	public void addPopupMenus(JPopupMenu popup, MouseEvent e) {
		ChatManager manager = TalkManager.getChatManager();
		ChatRoom room = manager.getChatContainer().getActiveChatRoom();
		if (room == null)
			return;
		final String jid = room.getRoomId();
		if (jid == null)
			return;

		final BalloonMessage msg = getMouseBalloonMessage();

		final MessageManager msgMgr = TalkManager.getMessageManager();

		//复制
		Action copyAction = new AbstractAction() {
			public void actionPerformed(ActionEvent actionEvent) {
				copyTalkMessage(msg.getId());
			}
		};
		copyAction.putValue(Action.NAME, Res.getMessage("action.copy"));
		copyAction.putValue(Action.SMALL_ICON, LAF.getCopyImageIcon());

		//删除
		Action deleteAction = new AbstractAction() {
			public void actionPerformed(ActionEvent actionEvent) {
				deleteTalkMessage(jid, msg.getId());
			}
		};
		deleteAction.putValue(Action.NAME, Res.getMessage("action.delete"));
		deleteAction.putValue(Action.SMALL_ICON, LAF.getDelete3Icon()[0]);

		//转发
		Action relayAction = new AbstractAction() {
			public void actionPerformed(ActionEvent actionEvent) {
				RelayDialog relayDialog = TalkManager.getChatFrame().getRelayDialog();
				if (relayDialog != null && relayDialog.isVisible()) {
					if (relayDialog.getBalloonMsg().getId() == msg.getId()) {
						return;
					}
					relayDialog.setVisible(false);
				}
				relayDialog = new RelayDialog(msg);
				relayDialog.setVisible(true);
				TalkManager.getChatFrame().setRelayDialog(relayDialog);
			}
		};
		relayAction.putValue(Action.NAME, Res.getMessage("action.ralay"));
		relayAction.putValue(Action.SMALL_ICON, LAF.getDelete3Icon()[0]);

		//清除
		Action clearAction = new AbstractAction() {
			public void actionPerformed(ActionEvent actionEvent) {
				clearTalkMessageList(jid);
			}
		};
		clearAction.putValue(Action.NAME, Res.getMessage("action.clear.screen"));
		clearAction.putValue(Action.SMALL_ICON, LAF.getClearIcon()[0]);

		if (msg == null) {
			// Handle enable
			boolean textSelected = ModelUtil.hasLength(getSelectedText());
			if (textSelected) {
				popup.add(copyMenu);
			}
			if (!hasInitMessage() && !msgMgr.hasSendMessage(jid)) {
				popup.add(clearAction);
			}

			popup.add(selectAllMenu);

		} else if (msg.getId() > 0) {
			if (isInitMessage(msg.getId()) || msgMgr.isSendMessage(msg.getId())) {
				return;
			}

			boolean isFile = MessageUtil.isChatMessage(msg.getTalkMessage().getMsgType(), Constants.MSG_TYPE_FILE);
			if (!isFile) {
				popup.add(copyAction);
			}
			popup.add(relayAction);
			popup.add(deleteAction);
		}

	}

	public void deleteTalkMessage(final String jid, final long messageId) {
		boolean f = TalkManager.getMessageManager().deleteMessage(jid, messageId);
		if (f) {
			delete(messageId);
			removeMsg(messageId);
		}
	}

	public void copyTalkMessage(final long messageId) {
		int selectionStart = getSelectionStart();
		int selectionEnd = getSelectionEnd();
		seletct(messageId);
		ClipboardUtil.copy(TranscriptWindow.this);
		select(selectionStart, selectionEnd);
	}

	public void clearTalkMessageList(final String jid) {
		int ok = JOptionPane.showConfirmDialog(TalkManager.getChatFrame(), Res.getMessage("delete.permanently"), Res.getMessage("delete.log.permanently"), JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE);
		if (ok == JOptionPane.YES_OPTION) {
			boolean f = TalkManager.getMessageManager().clearMessage(jid);
			if (f) {
				clear();
			}
		}
	}

	public void insertHistroyMessage(CommonChatRoom room, final TalkMessage talkMessage) {
		TalkBody bodyMessage = talkMessage.getTalkBody();
		if (bodyMessage == null)
			return;
		final BalloonMessage balloonMessage;
		TalkParam msgParam = talkMessage.getMsgParam();
		if (msgParam == null) {
			msgParam = new TalkParam();
		}
		balloonMessage = insertTalkMessage(talkMessage, bodyMessage, msgParam, true);

		if (balloonMessage == null)
			return;

		
		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				//3：历史消息已读,4历史消息未读
				final int type = talkMessage.getReaded() == TalkMessage.STATUS_UNREAD ? com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DOWNLOAD : com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DB;
				
				//处理图片，声音等消息
				balloonMessage.setStatus(BalloonMessage.STATUS_INIT);
				boolean result = MessageUtil.handResourceMsg(TranscriptWindow.this, balloonMessage, type);
				if(result){
					balloonMessage.setStatus(BalloonMessage.STATUS_YES);
				}else{
					balloonMessage.setStatus(BalloonMessage.STATUS_NO);
				}
				repaint();
				//				removeMsg(talkMessage.getId());

			}
		});

	}

	public BalloonMessage insertRealTimeMessage(CommonChatRoom room, TalkMessage talkMessage) {
		TalkBody talkBody = talkMessage.getTalkBody();
		if (talkBody == null)
			return null;
		return insertTalkMessage(talkMessage, talkBody, talkMessage.getMsgParam(), false);
	}

	//	private BalloonMessage insertFileMessage(CommonChatRoom room, TranscriptMessage transcriptMessage, Date time, boolean isHistory) {
	//		List<Msg> fileList = transcriptMessage.getMsgList(Constants.MSG_TYPE_FILE);
	//		if (fileList == null || fileList.isEmpty()) {
	//			return null;
	//		}
	//		FileMsg fileMsg = (FileMsg) fileList.get(0);
	//		int fileType = fileMsg.getFileType();
	//		Component component = null;
	//		boolean isChatMsg = true;
	//		if (fileType == FileMsg.SEND_COMPLETE) {
	//			isChatMsg = true;
	//			File file = new File(fileMsg.getFileAbsolutePath());
	//			if (file.exists()) {
	//				component = new SendFileCompleteUI(fileMsg, file);
	//			}
	//		} else if (fileType == FileMsg.SEND_CANCEL) {
	//			isChatMsg = false;
	//			File file = new File(fileMsg.getFileAbsolutePath());
	//			if (file.exists()) {
	//				component = new SendFileCancelUI(file);
	//			}
	//		} else if (fileType == FileMsg.RECEIVE_COMPLETE) {
	//			isChatMsg = true;
	//			//接收文件文件消息
	//			File file = new File(fileMsg.getFileAbsolutePath());
	//			if (file.exists()) {
	//				component = new ReceiveFileCompleteUI(fileMsg, file);
	//			}
	//
	//		} else if (fileType == FileMsg.RECEIVE_CANCEL) {
	//			isChatMsg = false;
	//			//接收文件文件消息
	//			component = new ReceiveFileCancelUI(transcriptMessage.getChatMessage(), fileMsg);
	//		}
	//
	//		if (component != null) {
	//			Param msgParam = transcriptMessage.getMsgParam();
	//			if (msgParam == null) {
	//				msgParam = new Param();
	//			}
	//			if (isChatMsg) {
	//				return insertBalloonMessage(transcriptMessage, component, msgParam, isHistory);
	//			} else {
	//				//
	//				msgParam.setBalloonType(Param.BALLOON_TYPE_HINT);
	//				insertBalloonMessage(transcriptMessage, component, msgParam, isHistory);
	//			}
	//		}
	//		return null;
	//	}

	private synchronized BalloonMessage insertTalkMessage(final TalkMessage transcriptMessage, TalkBody bodyMessage, TalkParam param, final boolean isHistory) {
		BalloonMessage balloonMessage = getMsg(transcriptMessage.getId());
		if (balloonMessage != null) {
			return balloonMessage;
		}

		if (param == null) {
			return null;
		}

		// 消息拦截器，拦截不需要显示的消息
		//TODO:

		//获取服务器时间
		Date sentDate = transcriptMessage.getTalkBody().getTime();
		try {
			Date dispalyDate = sentDate;
			if (dispalyDate == null) {
				dispalyDate = new Date();
			}
			String timeStr = getDate(dispalyDate);
			if (!times.contains(timeStr)) {
				BalloonMessage balloonMsg = new BalloonMessage(0 - transcriptMessage.getId());
				balloonMsg.setStatus(BalloonMessage.STATUS_YES);
				balloonMsg.setBalloon(false);
				balloonMsg.setAvatar(false);
				balloonMsg.setBackground(false);
				balloonMsg.setAlignment(BalloonMessage.ALIGNMENT_CENTER);
				TalkBody cm = new TalkBody();
				cm.addSpecific(new TalkTextSpecific(timeStr));
				balloonMsg.setTalkMessage(new TalkMessage(cm));
				addBalloonMessage(balloonMsg);

				times.add(timeStr);
			}

			////

			balloonMessage = new BalloonMessage(transcriptMessage.getId()) {
				public void redo() {
					if (isHistory) {
						return;
					}
					String jid = talkMessage.getJid();
					ChatRoom chatRoom = TalkManager.getChatManager().getChatContainer().getChatRoom(jid);
					if (chatRoom == null) {
						return;
					}
					if (talkMessage.getSendOrReceive() == TalkMessage.TYPE_RECEIVE) {
						chatRoom.receiveTalkMessage(talkMessage);
					} else {
						chatRoom.sendTalkMessage(talkMessage);
					}
				}
			};

			balloonMessage.setTalkMessage(transcriptMessage);

			Object[] userInfos = TalkUtil.getTalkMessageUserInfos(transcriptMessage);
			balloonMessage.setAvatar((ImageIcon) userInfos[1]);
			balloonMessage.setNick((String) userInfos[2]);
			if (sentDate != null) {
				balloonMessage.setDate(sentDate);
			}
			//			balloonMessage.setDate(dispalyDate);

			int balloonType = param.getBalloonType();

			//消息 靠左，还是靠右
			if (balloonType == TalkParam.BALLOON_TYPE_HINT) {
				balloonMessage.setAlignment(BalloonMessage.ALIGNMENT_CENTER);
				balloonMessage.setBalloon(false);
				balloonMessage.setAvatar(false);
			} else if (balloonType == TalkParam.BALLOON_TYPE_NORMAL) {
				if ((Boolean) userInfos[0]) {
					balloonMessage.setAlignment(BalloonMessage.ALIGNMENT_LEFT);
				} else {
					balloonMessage.setAlignment(BalloonMessage.ALIGNMENT_RIGHT);
				}
			} else {
				balloonMessage.setAlignment(BalloonMessage.ALIGNMENT_CENTER);
				balloonMessage.setBalloon(false);
				balloonMessage.setAvatar(false);
			}

			//字体设置
			String fontFamily = param.getF();
			if (StringUtil.isNotBlank(fontFamily)) {
				balloonMessage.setFontFamily(fontFamily);
			}
			balloonMessage.setFontSize(param.getS());
			balloonMessage.setBold(param.getB() == 1);
			balloonMessage.setItalic(param.getI() == 1);
			balloonMessage.setUnderline(param.getU() == 1);
			balloonMessage.setForeground(param.getFg());
			balloonMessage.setBackgroundBalloon(param.getBalloon());

			addBalloonMessage(balloonMessage);

			putMsg(transcriptMessage.getId(), balloonMessage);

			return balloonMessage;

		} catch (Exception e) {
			Log.error("Error message.", e);
		}
		return null;
	}

	public void putMsg(long messageId, BalloonMessage msg) {
		magMap.put(messageId, msg);
	}

	public void removeMsg(long messageId) {
		magMap.remove(messageId);
	}

	public BalloonMessage getMsg(long messageId) {
		return magMap.get(messageId);
	}

	/**
	 * Return the last time the <code>TranscriptWindow</code> was updated.
	 *
	 * @return the last time the <code>TranscriptWindow</code> was updated.
	 */
	public Date getLastUpdated() {
		return lastUpdated;
	}

	/**
	 * Disable the entire <code>TranscriptWindow</code> and visually represent it as disabled.
	 */
	public void showWindowDisabled() {
		final Document document = getDocument();
		final SimpleAttributeSet attrs = new SimpleAttributeSet();
		StyleConstants.setForeground(attrs, Color.LIGHT_GRAY);

		final int length = document.getLength();
		StyledDocument styledDocument = getStyledDocument();
		styledDocument.setCharacterAttributes(0, length, attrs, false);
	}

	/**
	 * Persist a current transcript.
	 *
	 * @param fileName
	 *            the name of the file to save the transcript as. Note: This can be modified by the user.
	 * @param transcript
	 *            the collection of transcript.
	 * @param headerData
	 *            the string to prepend to the transcript.
	 * @see CommonChatRoom#getTranscripts()
	 */
	public void saveTranscript(String fileName, List<Message> transcript, String headerData) {
		//		try {
		//			SimpleDateFormat formatter;
		//
		//			File defaultSaveFile = new File(Spark.getSparkUserHome() + "/" + fileName);
		//			final JFileChooser fileChooser = new JFileChooser(defaultSaveFile);
		//			fileChooser.setSelectedFile(defaultSaveFile);
		//
		//			// Show save dialog; this method does not return until the dialog is closed
		//			int result = fileChooser.showSaveDialog(this);
		//			final File selFile = fileChooser.getSelectedFile();
		//
		//			if (selFile != null && result == JFileChooser.APPROVE_OPTION) {
		//				final StringBuffer buf = new StringBuffer();
		//				final Iterator<Msg> transcripts = transcript.iterator();
		//				buf.append("<html><body>");
		//				if (headerData != null) {
		//					buf.append(headerData);
		//				}
		//
		//				buf.append("<table width=600>");
		//				while (transcripts.hasNext()) {
		//					final Msg message = transcripts.next();
		//					String from = message.getFrom();
		//					if (from == null) {
		//						from = pref.getNickname();
		//					}
		//
		//					if (Msg.Type.groupchat == message.getType()) {
		//						if (ModelUtil.hasLength(StringUtils.parseResource(from))) {
		//							from = StringUtils.parseResource(from);
		//						}
		//					}
		//
		//					final String body = message.getBody();
		//					final Date insertionDate = (Date) message.getProperty("insertionDate");
		//					formatter = new SimpleDateFormat("hh:mm:ss");
		//
		//					String object = "";
		//					if (insertionDate != null) {
		//						object = "(" + formatter.format(insertionDate) + ") ";
		//					}
		//					buf.append("<tr><td nowrap><font size=2>").append(object).append("<strong>").append(from).append(":</strong>&nbsp;").append(body).append("</font></td></tr>");
		//
		//				}
		//				buf.append("</table></body></html>");
		//				final BufferedWriter writer = new BufferedWriter(new FileWriter(selFile));
		//				writer.write(buf.toString());
		//				writer.close();
		//				UIManager.put("OptionPane.okButtonText", Res.getString("ok"));
		//				JOptionPane.showMessageDialog(SparkManager.getMainWindow(), "Chat transcript has been saved.", "Chat Transcript Saved", JOptionPane.INFORMATION_MESSAGE);
		//			}
		//		} catch (Exception ex) {
		//			Log.error("Unable to save chat transcript.", ex);
		//			UIManager.put("OptionPane.okButtonText", Res.getString("ok"));
		//			JOptionPane.showMessageDialog(SparkManager.getMainWindow(), "Could not save transcript.", "Error", JOptionPane.ERROR_MESSAGE);
		//		}

	}

	public void cleanup() {
		getActionMap().remove("copy");
		getActionMap().remove("cut");
		getActionMap().remove("paste");

		clear();

		removeMouseListener(this);
		removeMouseMotionListener(this);
	}

	@Override
	public void clear() {
		times.clear();
		magMap.clear();
		super.clear();
	}

	protected Date getLastPost() {
		return lastPost;
	}

	protected void setLastPost(Date lastPost) {
		this.lastPost = lastPost;
	}

	protected void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	//	/**
	//	 * Inserts a history message.
	//	 *
	//	 * @param userid
	//	 *            the userid of the sender.
	//	 * @param message
	//	 *            the message to insert.
	//	 * @param date
	//	 *            the Date object created when the message was delivered.
	//	 */
	//	public void insertHistoryMessage(String userid, String message, Date date) {
	//		try {
	//			String object;
	//
	//			long lastPostTime = lastPost != null ? lastPost.getTime() : 0;
	//			long lastPostStartOfDay = DateUtil.startOfDayInMillis(lastPostTime);
	//			long newPostStartOfDay = DateUtil.startOfDayInMillis(date.getTime());
	//
	//			int diff = DateUtil.getDaysDiff(lastPostStartOfDay, newPostStartOfDay);
	//			if (diff != 0) {
	//				insertCustomText(notificationDateFormatter.format(date), true, true, Color.BLACK);
	//			}
	//
	//			object = getDate(date);
	//			object = object + userid + ": ";
	//
	//			lastPost = date;
	//
	//			// Agent color is always blue
	//			StyleConstants.setBold(styles, false);
	//			StyleConstants.setForeground(styles, Color.BLACK);
	//			final Document doc = getDocument();
	//			styles.removeAttribute("link");
	//
	//			StyleConstants.setFontSize(styles, defaultFont.getSize());
	//			doc.insertString(doc.getLength(), object, styles);
	//
	//			// Reset Styles for message
	//			StyleConstants.setBold(styles, false);
	//			StyleConstants.setForeground(styles, (Color) UIManager.get("History.foreground"));
	//
	//			boolean r = insertJsonMessage(null, message);
	//
	//			if (r) {
	//				StyleConstants.setForeground(styles, Color.BLACK);
	//				insertText("\n");
	//			}
	//		} catch (Exception ex) {
	//			Log.error("Error message.", ex);
	//		}
	//	}
	//
	//	private boolean insertJsonMessage(Msg message, String body) throws Exception, MalformedURLException, IOException {
	//		ChatMessage xwMessage = GsonUtil.fromJson(body, ChatMessage.class);
	//		McWillList2<MessageBody> bodies = xwMessage.getBodies();
	//
	//		if (bodies == null)
	//			return false;
	//
	//		for (MessageBody messageBody : bodies) {
	//			String sendOrReceive = messageBody.getType();
	//			MessageType messageBodyType = MessageType.toType(sendOrReceive);
	//
	//			if (messageBodyType == MessageType.txt) {
	//				insertText(message, messageBody);
	//			} else if (messageBodyType == MessageType.img) {
	//				insertImage(message, messageBody);
	//			} else if (messageBodyType == MessageType.audio) {
	//				insertAudio(message, messageBody);
	//			} else if (messageBodyType == MessageType.offlinefile) {
	//				insertOfflineFile(message, messageBody);
	//			} else if (messageBodyType == MessageType.voicemail) {
	//				setText(messageBody.toString());
	//			} else {
	//				setText(messageBody.toString());
	//			}
	//		}
	//		return true;
	//	}
	//
	//	private void insertAudio(Msg message, MessageBody messageBody) throws MalformedURLException, IOException {
	//		//		URL url = new URL(messageBody.getUrl());
	//		//		InputStream wavVoiceStream = url.openStream();
	//		//		if (wavVoiceStream != null) {
	//		//			addComponent(new XWVoiceButton(SparkRes.getImageIcon(SparkRes.SOUND_PREFERENCES_IMAGE), wavVoiceStream));
	//		//		}
	//	}
	//
	//	private void insertImage(Msg message, MessageBody messageBody) throws Exception {
	//		addComponent(getImageLable(new ImageIcon(TalkManager.getXWService().downloadImage(messageBody.getUrl()), messageBody.getFilename())));
	//	}
	//
	//	private void insertText(Msg message, MessageBody messageBody) {
	//		setText(messageBody.getMsg());
	//	}
	//
	//	protected void insertOfflineFile(Msg message, MessageBody messageBody) {
	//		//		String httpUrl = messageBody.getUrl();
	//		//		String fileName = messageBody.getFilename();
	//		//		long fileSize = messageBody.getLength();
	//		//		String hash = messageBody.getHash();
	//		//		String requestor = message.getFrom();
	//		//
	//		//		insertCustomText(Res.getString("message.file.transfer.chat.window"), true, false, Color.BLACK);
	//		//
	//		//		final ReceiveFileTransfer receivingMessageUI = new ReceiveFileTransfer();
	//		//
	//		//		FileTransferRequest request = new FileTransferRequest(httpUrl, fileName, fileSize, hash, requestor);
	//		//		receivingMessageUI.acceptFileTransfer(request);
	//		//
	//		//		ChatRoom chatRoom = XW.getChatRoom(requestor);
	//		//
	//		//		chatRoom.addClosingListener(new ChatRoomClosingListener() {
	//		//			public void closing() {
	//		//				receivingMessageUI.cancelTransfer();
	//		//			}
	//		//		});
	//		//
	//		//		addComponent(receivingMessageUI);
	//		//
	//		//		String fileTransMsg = chatRoom.getRoomname() + " " + Res.getString("message.file.transfer.short.message") + " " + fileName;
	//		//		SparkManager.getChatManager().getChatContainer().fireNotifyOnMessage(chatRoom, true, fileTransMsg, Res.getString("message.file.transfer.notification"));
	//
	//	}
	//
	//	private JLabel getImageLable(final ImageIcon icon) {
	//		final JLabel imageLable = new JLabel();
	//
	//		final JPopupMenu popupMenu = new JPopupMenu();
	//		popupMenu.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.gray), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
	//		JMenuItem copyMenu = new JMenuItem(Res.getMessage("action.copy"), LAF.getImageIcon("camtalk/images/action/copy.png"));
	//		copyMenu.setMnemonic('C');
	//		copyMenu.addActionListener(new ActionListener() {
	//			@Override
	//			public void actionPerformed(ActionEvent e) {
	//				new ClipboardUtil().writeToClipboard(icon.getImage());
	//			}
	//		});
	//		JMenuItem saveMenu = new JMenuItem(Res.getMessage("action.save"), LAF.getImageIcon("camtalk/images/action/save_as.png"));
	//		saveMenu.setMnemonic('S');
	//		saveMenu.addActionListener(new ActionListener() {
	//			@Override
	//			public void actionPerformed(ActionEvent e) {
	//				new ClipboardUtil().writeToFile(icon.getImage());
	//			}
	//		});
	//
	//		popupMenu.add(copyMenu);
	//		popupMenu.add(saveMenu);
	//
	//		imageLable.setBorder(BorderFactory.createBevelBorder(0, Color.white, Color.lightGray));
	//		if (icon.getIconHeight() > 128 || icon.getIconWidth() > 128) {
	//			imageLable.setIcon(new ImageIcon(icon.getImage().getScaledInstance(-1, 128, Image.SCALE_SMOOTH)));
	//			final JDialog dlg = new JDialog();
	//			imageLable.addMouseListener(new MouseAdapter() {
	//				@Override
	//				public void mouseClicked(MouseEvent e) {
	//					if (!dlg.isVisible() && e.getClickCount() == 2) {
	//						dlg.setModal(true);
	//						dlg.setSize(icon.getIconWidth() + 3, icon.getIconHeight() + 25);
	//						JLabel lab = new JLabel();
	//						lab.setIcon(icon);
	//						dlg.getContentPane().add(lab);
	//						dlg.setResizable(false);
	//						dlg.setLocationRelativeTo(imageLable);
	//						dlg.setVisible(true);
	//						dlg.toFront();
	//					}
	//				}
	//
	//				public void mouseReleased(MouseEvent e) {
	//					if (e.isPopupTrigger()) {
	//						popupMenu.show(e.getComponent(), e.getX(), e.getY());
	//					}
	//				}
	//			});
	//
	//		} else {
	//			imageLable.setIcon(icon);
	//		}
	//		return imageLable;
	//	}

	public String getDate(Date insertDate) {
		if (insertDate == null) {
			insertDate = new Date();
		}

		//		StyleConstants.setFontFamily(styles, defaultFont.getFontName());
		//		StyleConstants.setFontSize(styles, defaultFont.getSize());

		final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		final String date = formatter.format(insertDate);

		return date;
	}
	/////////////////////////////////////////////////////////////////////
}
