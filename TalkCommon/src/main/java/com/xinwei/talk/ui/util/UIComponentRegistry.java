/**
 * $RCSfile: ,v $
 * $Revision: $
 * $Date: $
 *  
 * Copyright (C) 2011 eZuce Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xinwei.talk.ui.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;

import com.xinwei.http.service.file.OutFileTransfer;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.chat.room.file.SendFileUI;

/**
 * This is a registry for components that may be replaced by plugins. Also doubles as a factory to instantiate those components.
 *
 */
public class UIComponentRegistry {
	// use Spark defaults, so without any plugins we still have Spark's
	private static Class<? extends SendFileUI> sendFileUIClass = SendFileUI.class;

	public UIComponentRegistry() {
		// disable instantiation
	}

	public static void registerSendFileUI(Class<? extends SendFileUI> clazz) {
		if (sendFileUIClass != clazz) {
			sendFileUIClass = clazz;
		} else {
			Log.warn("Class " + clazz.getName() + " already registered.");
		}
	}

	/**
	 * Retrieves the button factory instance (this is implemented as a singleton).
	 *
	 * @return The instance of the class currently registered as button factory.
	 */
	public static SendFileUI getSendFileUI() {
		return instantiate(sendFileUIClass);
	}

	/**
	 * Instantiate a given class.
	 *
	 * @param currentClass
	 *            Class to instantiate.
	 * @param args
	 *            Arguments for the class constructor.
	 * @return New instance, what else?
	 */
	private static <T> T instantiate(Class<? extends T> currentClass, Object... args) {
		T instance = null;

		Log.debug("Args: " + Arrays.toString(args));
		try {
			if (args != null) {
				Class<? extends Object>[] classes = new Class<?>[args.length];
				for (int i = 0; i < args.length; i++) {
					classes[i] = args[i].getClass();
				}
				final Constructor<? extends T> ctor = currentClass.getDeclaredConstructor(classes);
				instance = ctor.newInstance(args);
			} else {
				final Constructor<? extends T> ctor = currentClass.getDeclaredConstructor();
				instance = ctor.newInstance();
			}
		} catch (final Exception e) {
			// not pretty but we're catching several exceptions we can do little
			// about
			Log.error("Error calling constructor for " + currentClass.getName(), e);
		}
		return instance;
	}

}