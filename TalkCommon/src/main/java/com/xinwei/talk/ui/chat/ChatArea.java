/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月20日 上午11:18:27
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import com.xinwei.common.lang.Callback;
import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.McWillImageIcon;
import com.xinwei.common.lookandfeel.component.McWillTextPane;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.common.lookandfeel.util.TalkHighlighter;
import com.xinwei.spark.ModelUtil;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.listener.LinkInterceptor;
import com.xinwei.talk.ui.chat.component.ImageComponent;
import com.xinwei.talk.ui.chat.component.VoiceComponent;
import com.xinwei.talk.ui.util.ClipboardUtil;

import sun.awt.image.ToolkitImage;

/**
 * The ChatArea class handles proper chat text formatting such as url handling. Use ChatArea for proper formatting of bold, italics, underlined and urls.
 */
public class ChatArea extends McWillTextPane implements MouseListener, MouseMotionListener, ActionListener {
	private static final long serialVersionUID = -2155445968040220072L;

	/**
	 * The SimpleAttributeSet used within this instance of JTextPane.
	 */
	public final SimpleAttributeSet styles = new SimpleAttributeSet();

	/**
	 * The default Hand cursor.
	 */
	public static final Cursor HAND_CURSOR = new Cursor(Cursor.HAND_CURSOR);

	/**
	 * The default Text Cursor.
	 */
	public static final Cursor DEFAULT_CURSOR = new Cursor(Cursor.DEFAULT_CURSOR);

	/**
	 * The currently selected Font Family to use.
	 */
	private String fontFamily;

	/**
	 * The currently selected Font Size to use.
	 */
	private int fontSize;

	private JPopupMenu popup;

	private JMenuItem cutMenu;
	private JMenuItem copyMenu;
	private JMenuItem pasteMenu;
	private JMenuItem selectAll;

	private List<LinkInterceptor> interceptors = new ArrayList<LinkInterceptor>();

	/**
	 * ChatArea Constructor.
	 */
	public ChatArea() {
		// Set Default Font
		fontSize = 12;

		cutMenu = new JMenuItem(Res.getMessage("action.cut"));
		cutMenu.addActionListener(this);

		copyMenu = new JMenuItem(Res.getMessage("action.copy"));
		copyMenu.addActionListener(this);

		pasteMenu = new JMenuItem(Res.getMessage("action.paste"));
		pasteMenu.addActionListener(this);

		selectAll = new JMenuItem(Res.getMessage("action.select.all"));
		selectAll.addActionListener(this);

		// Set Default Font
		setFont(new Font("Dialog", Font.PLAIN, 12));

		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("Ctrl x"), "cut");

		getActionMap().put("cut", new AbstractAction("cut") {
			private static final long serialVersionUID = 9117190151545566922L;

			public void actionPerformed(ActionEvent evt) {
				cutAction();
			}
		});

		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("Ctrl c"), "copy");

		getActionMap().put("copy", new AbstractAction("copy") {
			private static final long serialVersionUID = 4949716854440264528L;

			public void actionPerformed(ActionEvent evt) {
				copyAction();
			}
		});

		getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("Ctrl v"), "paste");

		getActionMap().put("paste", new AbstractAction("paste") {
			private static final long serialVersionUID = -8767763580660683678L;

			public void actionPerformed(ActionEvent evt) {
				pasteAction();
			}
		});

		setHighlighter(new TalkHighlighter(this));
	}

	/**
	 * Set the current text of the ChatArea.
	 *
	 * @param message
	 *            inserts the text directly into the ChatArea
	 */
	public void setText(String message) {
		if (ModelUtil.hasLength(message)) {
			try {
				insert(message);
			} catch (BadLocationException e) {
				Log.error(e);
			}
		}
	}

	/**
	 * setText is a core JTextPane method that can beused to inject a different Document sendOrReceive for instance HTMLDocument (setText("<HTML></HTML>") We should keep the functionality - it is
	 * useful when we want to inject a different Document sendOrReceive instead of StyleDocument
	 * 
	 * @param content
	 */
	public void setInitialContent(String content) {
		super.setText(content);
	}

	/**
	 * Removes the last appearance of word from the TextArea
	 * 
	 * @param word
	 */
	public void removeLastWord(String word) {
		select(getText().lastIndexOf(word), getText().length());
		replaceSelection("");
	}

	/**
	 * Removes everything in between <b>begin</b> and <b>end</b>
	 * 
	 * @param begin
	 * @param end
	 */
	public void removeWordInBetween(int begin, int end) {
		select(begin, end);
		replaceSelection("");
	}

	/**
	 * Clear the current document. This will remove all text and element attributes such as bold, italics, and underlining. Note that the font family and font size will be persisted.
	 */
	public void clear() {
		super.setText("");
		if (fontFamily != null) {
			setFontFamily(fontFamily);
		}

		if (fontSize != 0) {
			setFontSize(fontSize);
		}

//		StyleConstants.setUnderline(styles, false);
//		StyleConstants.setBold(styles, false);
//		StyleConstants.setItalic(styles, false);
//		setCharacterAttributes(styles, false);
	}

	/**
	 * Does the actual insertion of text, adhering to the styles specified during message creation in either the thin or thick client.
	 *
	 * @param text
	 *            - the text to insert.
	 * @throws BadLocationException
	 *             if location is not available to insert into.
	 */
	public void insert(String text) throws BadLocationException {
		boolean bold = false;
		boolean italic = false;
		boolean underlined = false;

		final StringTokenizer tokenizer = new StringTokenizer(text, " \n \t", true);
		while (tokenizer.hasMoreTokens()) {
			String textFound = tokenizer.nextToken();
			if ((textFound.startsWith("http://") || textFound.startsWith("ftp://") || textFound.startsWith("https://") || textFound.startsWith("www.")) && textFound.indexOf(".") > 1) {
				insertLink(textFound);
			} else if (textFound.startsWith("\\\\") || (textFound.indexOf("://") > 0 && textFound.indexOf(".") < 1)) {
				insertAddress(textFound);
			}
			//			else if (!insertEmoticon(textFound)) {
			//				insertText(textFound);
			//			}
		}

		// By default, always have decorations off.
//		StyleConstants.setBold(styles, bold);
//		StyleConstants.setItalic(styles, italic);
//		StyleConstants.setUnderline(styles, underlined);
	}

	/**
	 * Inserts text into the current document.
	 *
	 * @param text
	 *            the text to insert
	 * @throws BadLocationException
	 *             if the location is not available for insertion.
	 */
	public void insertText(String text) throws BadLocationException {
		final Document doc = getDocument();
		styles.removeAttribute("link");
		doc.insertString(doc.getLength(), text, styles);
		
		setCaretPosition(doc);
	}

	/**
	 * Inserts text into the current document.
	 *
	 * @param text
	 *            the text to insert
	 * @param color
	 *            the color of the text
	 * @throws BadLocationException
	 *             if the location is not available for insertion.
	 */
	public void insertText(String text, Color color) throws BadLocationException {
		final Document doc = getDocument();
		StyleConstants.setForeground(styles, color);
		doc.insertString(doc.getLength(), text, styles);
		
		setCaretPosition(doc);
	}

	/**
	 * Inserts a link into the current document.
	 *
	 * @param link
	 *            - the link to insert( ex. http://www.javasoft.com )
	 * @throws BadLocationException
	 *             if the location is not available for insertion.
	 */
	public void insertLink(String link) throws BadLocationException {
		final Document doc = getDocument();
		styles.addAttribute("link", link);

		StyleConstants.setForeground(styles, (Color) UIManager.get("Link.foreground"));
		StyleConstants.setUnderline(styles, true);
		doc.insertString(doc.getLength(), link, styles);
		StyleConstants.setUnderline(styles, false);
		StyleConstants.setForeground(styles, (Color) UIManager.get("TextPane.foreground"));
		styles.removeAttribute("link");
		
		setCaretPosition(doc);
	}

	/**
	 * Inserts a network address into the current document.
	 *
	 * @param address
	 *            - the address to insert( ex. \superpc\etc\file\ OR http://localhost/ )
	 * @throws BadLocationException
	 *             if the location is not available for insertion.
	 */
	public void insertAddress(String address) throws BadLocationException {
		final Document doc = getDocument();
		styles.addAttribute("link", address);

		StyleConstants.setForeground(styles, (Color) UIManager.get("Address.foreground"));
		StyleConstants.setUnderline(styles, true);
		doc.insertString(doc.getLength(), address, styles);
		StyleConstants.setUnderline(styles, false);
		StyleConstants.setForeground(styles, (Color) UIManager.get("TextPane.foreground"));
		styles.removeAttribute("link");
		setCaretPosition(doc);
	}

	/**
	 * Inserts an emotion icon into the current document.
	 *
	 * @param imageKey
	 *            - the smiley representation of the image.( ex. :) )
	 * @return true if the image was found, otherwise false.
	 */
	public boolean insertEmoticon(McWillImageIcon emoticon) {
		if (emoticon == null) {
			return false;
		}
		return insertImage(emoticon, null, null);
	}

	public boolean insertBufferedImage(BufferedImage bufferedImage) {
		if (bufferedImage == null) {
			return false;
		}
		ImageIcon icon = new ImageIcon(bufferedImage);

		byte[] data = ImageUtil.getPng(bufferedImage);

		return insertImage(icon, data, UUID.randomUUID().toString() + ".png");

	}

	public boolean insertImageFile(File file) {
		if (file == null) {
			return false;
		}
		ImageIcon icon = new ImageIcon(file.getAbsolutePath());

		byte[] data = IOUtil.readBytes(file);

		insertImage(icon, data, file.getName());

		return true;
	}

	public boolean insertImage(ImageIcon icon, byte data[], String name) {
		if (icon == null && data == null) {
			return false;
		}
		ImageComponent label = new ImageComponent(icon, data, name);

		final Document doc = getDocument();
		select(doc.getLength(), doc.getLength());

		insertComponent(label);

		setCaretPosition(doc);

		return true;
	}

	public boolean insertVoice(byte[] wavBytes) {
		if (wavBytes == null) {
			return false;
		}
		final Document doc = getDocument();
		select(doc.getLength(), doc.getLength());

		VoiceComponent c = new VoiceComponent(UUID.randomUUID().toString() + ".amr", true, null, wavBytes);
		//		c.setMaximumSize(new Dimension(32, 32));
		insertComponent(c);

		setCaretPosition(doc);
		
		return true;
	}

	protected void setCaretPosition(final Document doc) {
		setCaretPosition(doc.getLength());

		try {
			setCharacterAttributes(styles, false);
		} catch (Exception ex) {
			Log.error("Error settings bold:", ex);
		}
	}

	/**
	 * Sets the current element to be either bold or not depending on the current state. If the element is currently set as bold, it will be set to false, and vice-versa.
	 */
	public void setFontBold(boolean bold) {
		StyleConstants.setBold(styles, bold);
		try {
			setCharacterAttributes(styles, false);
		} catch (Exception ex) {
			Log.error("Error settings bold:", ex);
		}
	}

	/**
	 * Sets the current element to be either italicized or not depending on the current state. If the element is currently set as italic, it will be set to false, and vice-versa.
	 */
	public void setFontItalic(boolean isItalic) {
		StyleConstants.setItalic(styles, isItalic);
		try {
			setCharacterAttributes(styles, false);
		} catch (Exception fontException) {
			Log.error("Error settings italics:", fontException);
		}
	}

	/**
	 * Sets the current document to be either underlined or not depending on the current state. If the element is currently set as underlined, it will be set to false, and vice-versa.
	 */
	public void setFontUnderline(boolean isUnderlined) {
		StyleConstants.setUnderline(styles, isUnderlined);
		try {
			setCharacterAttributes(styles, false);
		} catch (Exception underlineException) {
			Log.error("Error settings underline:", underlineException);
		}
	}

	/**
	 * Set the font on the current element.
	 *
	 * @param font
	 *            the font to use with the current element
	 */
	public void setFontFamily(String font) {
		if (font == null)
			return;
		StyleConstants.setFontFamily(styles, font);
		try {
			setCharacterAttributes(styles, false);
		} catch (Exception fontException) {
			Log.error("Error settings font:", fontException);
		}

		fontFamily = font;
	}

	/**
	 * Set the current font size.
	 *
	 * @param size
	 *            the current font size.
	 */
	public void setFontSize(int size) {
		StyleConstants.setFontSize(styles, size);
		try {
			setCharacterAttributes(styles, false);
		} catch (Exception fontException) {
			Log.error("Error settings font:", fontException);
		}

		fontSize = size;
	}

	public void setFontForeground(Color color) {
		StyleConstants.setForeground(styles, color);
		try {
			setCharacterAttributes(styles, false);
		} catch (Exception fontException) {
			Log.error("Error settings font:", fontException);
		}
	}

	public void setCharacterAttributes(AttributeSet attr, boolean replace) {
		int p0 = 0;
		int p1 = getDocument().getLength();

		StyledDocument doc = getStyledDocument();
		doc.setCharacterAttributes(p0, p1 - p0, attr, replace);

		MutableAttributeSet inputAttributes = getInputAttributes();
		if (replace) {
			inputAttributes.removeAttributes(inputAttributes);
		}
		inputAttributes.addAttributes(attr);
	}

	public void mouseClicked(MouseEvent e) {
		try {
			final int pos = viewToModel(e.getPoint());
			final Element element = getStyledDocument().getCharacterElement(pos);

			if (element != null) {
				final AttributeSet as = element.getAttributes();
				final Object o = as.getAttribute("link");

				if (o != null) {
					try {
						final String url = (String) o;
						boolean handled = fireLinkInterceptors(e, url);
						if (!handled) {
							if (e.getButton() == MouseEvent.BUTTON1)
								SwingUtil.openURL(url);
							else if (e.getButton() == MouseEvent.BUTTON3) {
								JPopupMenu popupmenu = new JPopupMenu();
								JMenuItem linkcopy = new JMenuItem(Res.getString("action.copy"));
								linkcopy.addActionListener(new ActionListener() {

									@Override
									public void actionPerformed(ActionEvent e) {
										SwingUtil.setClipboard(url);

									}
								});
								linkcopy.setEnabled(true);
								popupmenu.add(linkcopy);
								popupmenu.show(this, e.getX(), e.getY());
							}
						}
					} catch (Exception ioe) {
						Log.error("Error launching browser:", ioe);
					}
				}
			}
		} catch (Exception ex) {
			Log.error("Visible Error", ex);
		}
	}

	public void mousePressed(MouseEvent e) {
		if (e.isPopupTrigger()) {
			handlePopup(e);
		}
	}

	/**
	 * This launches the <code>BrowserLauncher</code> with the URL located in <code>ChatArea</code>. Note that the url will automatically be clickable when added to <code>ChatArea</code>
	 *
	 * @param e
	 *            - the MouseReleased event
	 */
	public void mouseReleased(MouseEvent e) {
		if (e.isPopupTrigger()) {
			handlePopup(e);
		}

	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
	}

	/**
	 * Checks to see if the mouse is located over a browseable link.
	 *
	 * @param e
	 *            - the current MouseEvent.
	 */
	public void mouseMoved(MouseEvent e) {
		checkForLink(e);
	}

	/**
	 * Checks to see if the mouse is located over a browseable link.
	 *
	 * @param e
	 *            - the current MouseEvent.
	 */
	private void checkForLink(MouseEvent e) {
		try {
			final int pos = viewToModel(e.getPoint());
			final Element element = getStyledDocument().getCharacterElement(pos);

			if (element != null) {
				final AttributeSet as = element.getAttributes();
				final Object o = as.getAttribute("link");

				if (o != null) {
					setCursor(HAND_CURSOR);
				} else {
					setCursor(DEFAULT_CURSOR);
				}
			}
		} catch (Exception ex) {
			Log.error("Error in CheckLink:", ex);
		}
	}

	/**
	 * Examines the chatInput text pane, and returns a string containing the text with any markup (jive markup in our case). This will strip any terminating new line from the input.
	 *
	 * @return a string of marked up text.
	 */
	public String getMarkup() {
		final StringBuffer buf = new StringBuffer();
		final String text = getText();
		final StyledDocument doc = getStyledDocument();
		final Element rootElem = doc.getDefaultRootElement();

		// MAY RETURN THIS BLOCK
		if (text.trim().length() <= 0) {
			return null;
		}

		boolean endsInNewline = text.charAt(text.length() - 1) == '\n';
		for (int j = 0; j < rootElem.getElementCount(); j++) {
			final Element pElem = rootElem.getElement(j);

			for (int i = 0; i < pElem.getElementCount(); i++) {
				final Element e = pElem.getElement(i);
				final AttributeSet as = e.getAttributes();
				final boolean bold = StyleConstants.isBold(as);
				final boolean italic = StyleConstants.isItalic(as);
				final boolean underline = StyleConstants.isUnderline(as);
				int end = e.getEndOffset();

				if (end > text.length()) {
					end = text.length();
				}

				if (endsInNewline && end >= text.length() - 1) {
					end--;
				}

				// swing text.. :-/
				if (j == rootElem.getElementCount() - 1 && i == pElem.getElementCount() - 1) {
					end = text.length();
				}

				final String current = text.substring(e.getStartOffset(), end);
				if (bold) {
					buf.append("[b]");
				}
				if (italic) {
					buf.append("[i]");
				}
				if (underline) {
					buf.append("[u]");
				}
				//buf.append( "[font face=/\"" + fontFamily + "/\" size=/\"" + fontSize + "/\"/]" );

				// Iterator over current string to find url tokens
				final StringTokenizer tkn = new StringTokenizer(current, " ", true);
				while (tkn.hasMoreTokens()) {
					final String token = tkn.nextToken();
					if (token.startsWith("http://") || token.startsWith("ftp://") || token.startsWith("https://")) {
						buf.append("[url]").append(token).append("[/url]");
					} else if (token.startsWith("www")) {
						buf.append("[url ");
						buf.append("http://").append(token);
						buf.append("]");
						buf.append(token);
						buf.append("[/url]");
					} else {
						buf.append(token);
					}
				}

				// Always add end tags for markup
				if (underline) {
					buf.append("[/u]");
				}
				if (italic) {
					buf.append("[/i]");
				}
				if (bold) {
					buf.append("[/b]");
				}
				//  buf.append( "[/font]" );
			}
		}

		return buf.toString();
	}

	private void handlePopup(MouseEvent e) {
		popup = new JPopupMenu();
		popup.add(cutMenu);
		popup.add(copyMenu);
		popup.add(pasteMenu);
		popup.addSeparator();
		popup.add(selectAll);

		// Handle enable
		int selectionStart = getSelectionStart();
		int selectionEnd = getSelectionEnd();
		boolean textSelected = selectionEnd > selectionStart;
		boolean hasContent = ClipboardUtil.hasContent();
		cutMenu.setEnabled(textSelected && isEditable());
		copyMenu.setEnabled(textSelected);
		pasteMenu.setEnabled(hasContent && isEditable());

		popup.show(this, e.getX(), e.getY());
	}

	public void addLinkInterceptor(LinkInterceptor interceptor) {
		interceptors.add(interceptor);
	}

	public void removeLinkInterceptor(LinkInterceptor interceptor) {
		interceptors.remove(interceptor);
	}

	public boolean fireLinkInterceptors(MouseEvent event, String link) {
		for (LinkInterceptor linkInterceptor : new ArrayList<LinkInterceptor>(interceptors)) {
			boolean handled = linkInterceptor.handleLink(event, link);
			if (handled) {
				return true;
			}
		}

		return false;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == cutMenu) {
			cutAction();
		} else if (e.getSource() == copyMenu) {
			copyAction();
		} else if (e.getSource() == pasteMenu) {
			pasteAction();
		} else if (e.getSource() == selectAll) {
			requestFocus();
			selectAll();
		}
	}

	private void copyAction() {
		ClipboardUtil.copy(this);
	}

	private void cutAction() {
		ClipboardUtil.cut(this);
	}

	private void pasteAction() {
		ClipboardUtil.paste(this, new Callback() {
			@Override
			public void call(Object... objects) {
				Object object = objects[0];
				if (object instanceof String) {
					replaceSelection(String.valueOf(object));
				} else if (object instanceof BufferedImage) {
					BufferedImage image = (BufferedImage) object;
					insertBufferedImage(image);
				} else if (object instanceof ToolkitImage) {
					insertBufferedImage(((ToolkitImage) object).getBufferedImage());
				}

			}
		});

		//		Clipboard clb = Toolkit.getDefaultToolkit().getSystemClipboard();
		//		Transferable contents = clb.getContents(null);
		//
		//		if (contents == null)
		//			return;
		//
		//		try {
		//			if (contents.isDataFlavorSupported(DataFlavor.imageFlavor)) {//图片
		//				insertBufferedImage((BufferedImage) contents.getTransferData(DataFlavor.imageFlavor));
		//			} else if (contents.isDataFlavorSupported(DataFlavor.stringFlavor)) {
		//				String text = (String) contents.getTransferData(DataFlavor.stringFlavor);
		//				if (text != null) {
		//					replaceSelection(text);
		//				}
		//			}
		//		} catch (Exception e) {
		//			e.printStackTrace();
		//		}

	}

	protected void releaseResources() {
		getActionMap().remove("copy");
		getActionMap().remove("cut");
		getActionMap().remove("paste");
	}

	public SimpleAttributeSet getStyles() {
		return styles;
	}

}