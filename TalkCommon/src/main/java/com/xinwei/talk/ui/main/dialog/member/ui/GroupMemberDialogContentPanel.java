package com.xinwei.talk.ui.main.dialog.member.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.lookandfeel.util.WindowUtil;

@SuppressWarnings("serial")
public class GroupMemberDialogContentPanel extends McWillContentPanel {

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		ThemeColor themeColor = McWillTheme.getThemeColor();

		Color color2 = themeColor.getMiddleColor();
		g2d.setColor(color2);
		g.fillRect(0, 0, getWidth(), getHeight());

		// 绘制窗体边框
		g2d.setStroke(new BasicStroke(1));
		g2d.setPaint(McWillTheme.getThemeColor().getSubLightColor());
		g2d.drawLine(0, WindowUtil.ARC / 2, 0, getHeight() - WindowUtil.ARC / 2);
		g2d.drawLine(getWidth() - 1, WindowUtil.ARC / 2, getWidth() - 1, getHeight() - WindowUtil.ARC / 2);
		g2d.drawLine(WindowUtil.ARC / 2, 0, getWidth() - WindowUtil.ARC / 2, 0);
		g2d.drawLine(WindowUtil.ARC / 2, getHeight() - 1, getWidth() - WindowUtil.ARC / 2, getHeight() - 1);
		g2d.setStroke(new BasicStroke(1.5f));
		g2d.drawArc(0, 0, WindowUtil.ARC, WindowUtil.ARC, 90, 90);
		//		g2d.drawArc(0, getHeight() - WindowUtil.ARC - 1, WindowUtil.ARC, WindowUtil.ARC, 180, 90);
		g2d.drawArc(getWidth() - WindowUtil.ARC - 1, 0, WindowUtil.ARC, WindowUtil.ARC, 0, 90);
		//		g2d.drawArc(getWidth() - WindowUtil.ARC - 1, getHeight() - WindowUtil.ARC - 1, WindowUtil.ARC, WindowUtil.ARC, 270, 90);

	}
}