/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月13日 下午1:57:34
 * 
 ***************************************************************/
package com.xinwei.talk.ui.login;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.text.MessageFormat;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.lookandfeel.util.PainterUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.ui.util.component.XWDialogTitle;

public class OfflineDialog extends JFrame implements ActionListener {
	protected JButton okButton;
	protected JButton reloginButton;

	public OfflineDialog() {
		setIconImages(LAF.getApplicationImages());

		getContentPane().setLayout(new BorderLayout());

		createDialog();

		JRootPane rootPane = getRootPane();
		((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

		setSize(new Dimension(350, 200));

		SwingUtil.centerWindowOnScreen(this);
	}

	protected void createDialog() {
		Container contentPanel = createDialogContainer();

		contentPanel.setLayout(new BorderLayout());

		contentPanel.add(createDialogContent(), BorderLayout.CENTER);
		contentPanel.add(createButtonBar(), BorderLayout.SOUTH);

		setContentPane(contentPanel);

		pack();
	}

	protected JPanel createDialogContainer() {
		return new McWillContentPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				LAF.paintMainAddGroupDialog(this, (Graphics2D) g);
			}
		};
	}

	protected JPanel createDialogContent() {
		String title = Res.getMessage("dialog.title.offline");
		JPanel dialogPanel = new JPanel(new BorderLayout());
		XWDialogTitle titlePanel = new XWDialogTitle(this, title);
		titlePanel.setBorder(BorderFactory.createEmptyBorder(6, 10, 6, 0));
		dialogPanel.add(titlePanel, BorderLayout.NORTH);

		dialogPanel.add(createInputPanel(), BorderLayout.CENTER);

		setTitle(title);

		return dialogPanel;
	}

	private Component createInputPanel() {
		HyperlinkListener hlLsnr = new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType() != HyperlinkEvent.EventType.ACTIVATED)
					return;
				URL linkUrl = e.getURL();
				if (linkUrl != null) {
					try {
						Desktop.getDesktop().browse(linkUrl.toURI());
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}

		};
		JTextPane panel = new JTextPane() {
			public void paintComponent(Graphics g) {
				setOpaque(true);
				Graphics2D g2d = (Graphics2D) g;
				g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
				PainterUtil.paintBackgroudColor(g, Color.RED, 0, 0, getWidth(), getHeight());
				super.paintComponent(g);

			}
		};
		panel.setEditable(false);
		panel.setContentType("text/html");
		panel.addHyperlinkListener(hlLsnr);
		String table = "<html>" + "<tr style='font-size:10px;color:#333333';><td valign='top'><p><img src={0}></p></td>" + "<td>" + "<p>{1}</p><br/>" + "</td></tr>" + "</html>";

		String html = MessageFormat.format(table, LAF.getURL("talk/images/common/info.png").toString(), Res.getMessage("label.relogin.info"));
		panel.setText(html);
		return panel;
	}

	protected JPanel createButtonBar() {
		JPanel bottomButtonPanel = new JPanel(getButtonBarLayout());
		bottomButtonPanel.setOpaque(false);

		okButton = SwingUtil.newButton(Res.getMessage("button.ok"));
		reloginButton = SwingUtil.newButton(Res.getMessage("button.relogin"));

		okButton.addActionListener(this);
		reloginButton.addActionListener(this);

		bottomButtonPanel.add(reloginButton);
		bottomButtonPanel.add(okButton);

		return bottomButtonPanel;
	}

	protected LayoutManager getButtonBarLayout() {
		return new FlowLayout(FlowLayout.RIGHT);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton) {
			this.dispose();
			return;
		}
		if (e.getSource() == reloginButton) {
			ListenerManager.fireTrayLogin();
			this.dispose();
			return;
		}
	}

	/**
	 * Brings the ChatFrame into focus on the desktop.
	 */
	public void bringFrameIntoFocus() {
		if (!isVisible()) {
			setVisible(true);
		}

		if (getState() == Frame.ICONIFIED) {
			setState(Frame.NORMAL);
		}

		toFront();
		requestFocus();
	}

	/**
	 * set if the chatFrame should always stay on top
	 * 
	 * @param active
	 */
	public void setWindowAlwaysOnTop(boolean active) {
		setAlwaysOnTop(active);
	}
}
