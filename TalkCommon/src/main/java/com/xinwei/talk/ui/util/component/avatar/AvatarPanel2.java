package com.xinwei.talk.ui.util.component.avatar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.common.vo.Entry;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.MessageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.TalkMessage;

public class AvatarPanel2 extends JPanel {
	private static final long serialVersionUID = -7299155244603711933L;

	private static final int GAP = 2;

	protected List<byte[]> avatars = new ArrayList<byte[]>();

	private boolean unread;

	private String jid;

	private MessageManager messageManager;

	public AvatarPanel2() {
		this(null);
	}

	public AvatarPanel2(boolean unread) {
		this(null, unread);
	}

	public AvatarPanel2(String jid) {
		this(jid, false);
	}

	public AvatarPanel2(String jid, boolean unread) {
		setOpaque(false);
		this.unread = unread;
		this.jid = jid;
		messageManager = TalkManager.getMessageManager();
	}

	public void setAvatarSize(int width, int height) {
		setMinimumSize(new Dimension(width, height));
		setMaximumSize(new Dimension(width, height));
		setPreferredSize(new Dimension(width, height));
	}

	@Override
	protected void paintComponent(Graphics g) {
		//		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
		Rectangle bounds = getBounds();

		drawImages(g, bounds);
		if (unread) {
			drawUnreadMessageCount(g, bounds);
		}
	}

	private void drawImages(Graphics g, Rectangle bounds) {
		List<byte[]> images = getAvatarImages();
		if(images.get(0)==null){
			System.out.println();
		}

		g.setColor(getImageBackgroud());

		int y = 6;
		int x = 2;
		int DARWW = bounds.width - x - 6;
		int DARWH = bounds.height - y - 3;
		
		ImageUtil.drawMergeImage(g, x, y, DARWW, DARWH, GAP, images);
	}

	private void drawUnreadMessageCount(Graphics g, Rectangle bounds) {
		int unreadMessageCount = getUnreadMessageCount();

		if (unreadMessageCount < 1) {
			return;
		}
		String unreadMessageCountStr = String.valueOf(unreadMessageCount);
		if (unreadMessageCountStr.length() > 2) {
			unreadMessageCountStr = "...";
		}

		Graphics2D g2d = (Graphics2D) g;

		int w = 16;
		int h = 16;
		int x = bounds.width - w - 2;
		int y = 1;

		Paint paint = g2d.getPaint();
		GradientPaint gp = new GradientPaint(x, y, Color.RED, w, h, Color.RED);
		g2d.setPaint(gp);
		g2d.fillOval(x, y, w, h);
		g2d.setPaint(paint);

		g2d.setColor(Color.WHITE);

		g2d.setFont(getUnreadMessageFont());
		Rectangle2D stringBounds = SwingUtil.getStringBounds(unreadMessageCountStr, g2d.getFont());

		g2d.drawString(unreadMessageCountStr, x + (int) Math.ceil((w - stringBounds.getWidth()) / 2), 13);
	}

	protected List<byte[]> getAvatarImages() {
		avatars.clear();
		if (TalkUtil.isGroup(jid)) {
			avatars.addAll(ImageManager.getMemberAvatarList(jid, true));
		} else {
			byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
			avatars.add(talkAvatar);
		}

		return avatars;
	}

	protected int getUnreadMessageCount() {
		Entry<Integer, TalkMessage> unreadMessage = messageManager.getUnreadMessage(jid);
		if (unreadMessage == null)
			return 0;
		return unreadMessage.getKey();
	}

	public void setJid(String jid) {
		this.jid = jid;
	}

	protected Font getUnreadMessageFont() {
		return DEFAULT_FONT;
	}

	protected Color getImageBackgroud() {
		return DEFAULT_COLOR;
	}

	protected Color DEFAULT_COLOR = new Color(240, 240, 240);

	protected Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 11);

}
