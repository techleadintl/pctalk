package com.xinwei.talk.ui.main.list.talk;

import javax.swing.JPopupMenu;

import com.xinwei.talk.common.TalkUtil2;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.ui.main.list.ListPanel;

public class TalkListPanel extends ListPanel<Talk> {
	private static final long serialVersionUID = -8486385306563904232L;

	@Override
	protected void addPopuMenuItemList(JPopupMenu popup, Talk talk) {
		TalkUtil2.addPopunActions(popup, "talk", talk);
	}

	public void addListeners() {
		super.addListeners();
		ListenerManager.addTalkListener(new MyObjectListener());
	}
}
