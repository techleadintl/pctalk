/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年2月28日 下午1:27:37
 * 
 ***************************************************************/
package com.xinwei.talk.ui.setting;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.xinwei.common.lookandfeel.component.McWillLineImage;
import com.xinwei.talk.common.LAF;

public class SettingSubPanel extends JPanel {
	public SettingSubPanel(String title) {
		setLayout(new BorderLayout());

		JPanel titlePane = createTitlePanel(title);
		JPanel contentPanel = createContentPanel();

		add(titlePane, BorderLayout.NORTH);
		add(contentPanel, BorderLayout.CENTER);
	}

	protected JPanel createTitlePanel(String title) {
		JPanel titlePane = new JPanel(new BorderLayout());

		JLabel titleLabel = new JLabel(title);
		titleLabel.setFont(LAF.getFont(14, true));
		JLabel lineLabel = new McWillLineImage(LAF.getLineSetting());
		lineLabel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 20));

		titlePane.add(titleLabel, BorderLayout.WEST);
		titlePane.add(lineLabel, BorderLayout.CENTER);
		return titlePane;
	}

	private JPanel createContentPanel() {
		JPanel jPanel = new JPanel();
		jPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
		createContentPanel(jPanel);
		return jPanel;
	}

	protected void createContentPanel(JPanel contentPanel) {
	}
}
