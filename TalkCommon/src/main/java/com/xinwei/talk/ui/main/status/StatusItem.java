/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 上午11:37:03
 * 
 ***************************************************************/
package com.xinwei.talk.ui.main.status;

import org.jivesoftware.smack.packet.Presence;

import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * Represents a single UI instance in the isInputing bar.
 */
public class StatusItem extends JLabel {
	private static final long serialVersionUID = 725324886254656704L;
	private Presence presence;

	/**
	 * Creates a single StatusItem UI object.
	 *
	 * @param presence
	 *            the presence.
	 * @param icon
	 *            the icon
	 */
	public StatusItem(Presence presence, Icon icon) {
		this.presence = presence;
		setIcon(icon);
		setText(presence.getStatus());
	}

	/**
	 * Returns the Presence related to this item.
	 *
	 * @return the presence associated with this item.
	 */
	public Presence getPresence() {
		return presence;
	}

}
