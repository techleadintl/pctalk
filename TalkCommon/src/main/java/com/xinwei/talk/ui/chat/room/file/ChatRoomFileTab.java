/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月14日 上午10:17:25
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room.file;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.xinwei.talk.common.Res;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.util.component.list.XWList;

public class ChatRoomFileTab extends JPanel {
	private XWList<FileUI> fileList;
	private CommonChatRoom room;

	private String tabTitle;

	public ChatRoomFileTab() {
		tabTitle = Res.getMessage("button.chatroom.chat.history.transfer");
		fileList = new XWList<FileUI>();

		JScrollPane scrollPane = new JScrollPane(fileList);

		setLayout(new BorderLayout());

		add(scrollPane);
	}

	public void addFileComponent(FileUI component) {
		fileList.add(component);
	}

	public void removeFileComponent(FileUI component) {
		fileList.remove(component);
	}

	public boolean isEmpty() {
		return fileList.getComponentCount() == 0;
	}

}
