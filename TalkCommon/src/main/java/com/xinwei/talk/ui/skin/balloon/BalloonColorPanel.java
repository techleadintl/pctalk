package com.xinwei.talk.ui.skin.balloon;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import com.xinwei.common.lookandfeel.McWillLookAndFeel;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.component.McWillRolloverButton;
import com.xinwei.common.lookandfeel.layout.WrapFlowLayout;
import com.xinwei.common.lookandfeel.util.ColorUtil;
import com.xinwei.common.ui.panel.SwatchPanel;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.conf.UserConfig;

@SuppressWarnings("serial")
public class BalloonColorPanel extends JPanel {

	private static final Dimension PREFERRED_SIZE = new Dimension(45, 45);

	public BalloonColorPanel(final Window window, List<Color> skins) {
		setOpaque(false);

		setBorder(BorderFactory.createEmptyBorder(0, 5, 10, 5));
		WrapFlowLayout mgr = new WrapFlowLayout(5, 0);
		mgr.setHgap(WrapFlowLayout.LEFT);
		setLayout(mgr);

		final UserConfig userConfig = TalkManager.getUserConfig();
		for (final Color color : skins) {
			JButton item = new McWillRolloverButton() {
				@Override
				protected void paintComponent(Graphics g) {
					g.setColor(ColorUtil.darker(color, 30));
					g.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 10, 10);
					g.setColor(color);
					g.fillRoundRect(1, 1, getWidth() - 2, getHeight() - 2, 10, 10);
				}
			};
			item.setPreferredSize(PREFERRED_SIZE);
			add(item);
			item.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					userConfig.setBalloon(String.valueOf(color.getRGB()));
					TalkManager.saveUserConfig();
					window.setVisible(false);
				}
			});
			item.setBorder(BorderFactory.createEmptyBorder());
		}
		URL resource = McWillLookAndFeel.class.getResource("images/chooser/colorchooser1.png");
		final ImageIcon icon = new ImageIcon(resource);
		final Image image = icon.getImage();
		// image = image.getScaledInstance(60, 50, Image.SCALE_SMOOTH);
		JButton colorChooseButton = new McWillIconButton() {
			@Override
			protected void paintComponent(Graphics g) {
				g.setColor(Color.LIGHT_GRAY);
				g.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 10, 10);
				g.drawImage(image, 1, 1, getWidth() - 2, getHeight() - 2, null);
				super.paintComponent(g);
			}
		};
		colorChooseButton.setPreferredSize(PREFERRED_SIZE);
		add(colorChooseButton);

		final JLabel foregroundLabel = new JLabel() {
			@Override
			protected void paintComponent(Graphics g) {
				//				g.setColor(McWillTheme.getThemeColor().getSubLightColor());
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, getWidth(), getHeight());

				g.setColor(getForeground());
				int a = Math.min(getWidth(), getHeight()) - 4;
				//				g.fill3DRect((getWidth() - a) / 2, (getHeight() - 3*a) / 2, a, 3*a,true);
				g.fill3DRect((getWidth() - a) / 2, (getHeight() - 1 * a) / 2, a, 1 * a, true);
				super.paintComponent(g);
			}
		};
		String balloon = userConfig.getBalloon();
		if (balloon != null) {
			try {
				foregroundLabel.setForeground(LAF.getColor(Integer.valueOf(balloon)));
			} catch (Exception ex) {
			}
		}

		final JPopupMenu popup = new JPopupMenu();
		JPanel p = new JPanel(new BorderLayout());
		final SwatchPanel swatchPanel = new SwatchPanel();
		foregroundLabel.setPreferredSize(new Dimension(30, 30));
		p.add(swatchPanel, BorderLayout.CENTER);
		p.add(foregroundLabel, BorderLayout.WEST);
		popup.add(p);
		swatchPanel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				Color color = swatchPanel.getColorForLocation(e.getX(), e.getY());
				if (color != null) {
					foregroundLabel.setForeground(color);
					userConfig.setBalloon(String.valueOf(color.getRGB()));
					TalkManager.saveUserConfig();
				}
			}
		});

		colorChooseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (popup.getHeight() == 0) {
					popup.setVisible(true);
				}
				popup.show(window, (window.getWidth() - popup.getWidth()) / 2, (window.getHeight() - popup.getHeight()) / 2);
			}
		});
	}

	private void setThemeColor(final Color color) {
		try {
			McWillTheme.setThemeColor(color);
		} catch (Exception e1) {
		}
	}
}
