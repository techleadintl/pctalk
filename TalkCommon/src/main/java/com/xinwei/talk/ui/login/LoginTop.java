package com.xinwei.talk.ui.login;

import java.awt.Dimension;
/**
 */
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.talk.common.LAF;

@SuppressWarnings("serial")
public class LoginTop extends JPanel implements MouseListener {
	private JButton min, close;
	private JFrame frame;

	public LoginTop(JFrame frame) {
		this.frame = frame;

		Dimension loginBackgroundSize = LAF.getLoginBackgroundSize();
		this.setLayout(null);
		this.setOpaque(false);

		Dimension iconifyIconSize = LAF.getDialogIconifyIconSize();

		//关闭按钮
		close = new McWillIconButton(LAF.getCloseIcons());
		close.setBounds(loginBackgroundSize.width - iconifyIconSize.width - 15, 5, iconifyIconSize.width, iconifyIconSize.height);
		close.addMouseListener(this);

		//缩小按钮
		min = new McWillIconButton(LAF.getIconifyIcons());
		min.setBounds(loginBackgroundSize.width - 2 * iconifyIconSize.width - 30, 5, iconifyIconSize.width, iconifyIconSize.height);
		min.addMouseListener(this);

		ImageIcon loginLogoIcon = LAF.getLoginLogoIcon();
		JLabel logoLabel = new JLabel(loginLogoIcon);
		logoLabel.setBounds((loginBackgroundSize.width - loginLogoIcon.getIconWidth()) / 2, 27, loginLogoIcon.getIconWidth(), loginLogoIcon.getIconHeight());

		add(min);
		add(close);

		add(logoLabel);
	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getSource() == min) {
			frame.setExtendedState(JFrame.ICONIFIED);
		}
		if (e.getSource() == close) {
			System.exit(0);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
