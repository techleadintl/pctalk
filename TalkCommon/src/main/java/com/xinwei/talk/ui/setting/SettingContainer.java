/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月20日 上午10:54:54
 * 
 ***************************************************************/
package com.xinwei.talk.ui.setting;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JSplitPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.border.Border;

import com.xinwei.common.lookandfeel.component.McWillList;
import com.xinwei.common.lookandfeel.component.McWillSplitPane;
import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.ui.util.WindowDragListener;
import com.xinwei.talk.ui.util.component.XWScrollPane;

public class SettingContainer extends JPanel {
	//	private SettingFrame settingFrame;

	private McWillList<SettingItem> list;

	private DefaultListModel<SettingItem> listModel;

	private JPanel itemPanel;

	private XWScrollPane rightPane;

	private JScrollBar rightVerticalScrollBar;

	private WindowDragListener dragListener;

	public SettingContainer(SettingFrame settingFrame) {
		setLayout(new BorderLayout());
		setOpaque(false);

		dragListener = new WindowDragListener();

		McWillSplitPane horizontalSplit = new McWillSplitPane(JSplitPane.HORIZONTAL_SPLIT) {
			@Override
			public Color getSplitLineColor() {
				return LAF.getChatSplitPaneColor();
			}
		};
		horizontalSplit.setBorder(BorderFactory.createEmptyBorder());
		horizontalSplit.setDividerLocation(155);
		horizontalSplit.setContinuousLayout(false);

		listModel = new DefaultListModel<SettingItem>();
		list = new McWillList<SettingItem>(listModel);
		list.setCellRenderer(new SettingItemListCellRenderer());

		XWScrollPane leftPane = new XWScrollPane(list);
		leftPane.setPreferredSize(new Dimension(140, Short.MAX_VALUE));

		leftPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		itemPanel = new JPanel();
		itemPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.LEFT));

		rightPane = new XWScrollPane(itemPanel) {
			@Override
			public int getScrollBarWidth() {
				return 11;
			}

			@Override
			public boolean isButtonVisible() {
				return false;
			}

			@Override
			public boolean isPaintTrack() {
				return false;
			}
		};
		rightVerticalScrollBar = rightPane.getScrollPane().getVerticalScrollBar();

		//Add Chat Panel to Split Pane
		horizontalSplit.setLeftComponent(leftPane);
		horizontalSplit.setRightComponent(rightPane);

		add(horizontalSplit, BorderLayout.CENTER);

		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Point mousePoint = e.getPoint();
				int locationToIndex = list.getValidIndex(mousePoint);
				if (locationToIndex == -1)
					return;
				SettingItem settingItem = list.getModel().getElementAt(locationToIndex);
				if (settingItem == null) {
					return;
				}
				JPanel right = settingItem.getRight();
				rightVerticalScrollBar.setValue(right.getLocation().y);
			}
		});

		rightVerticalScrollBar.addAdjustmentListener(new AdjustmentListener() {
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				if (!e.getValueIsAdjusting())
					return;
				ListModel<SettingItem> model = list.getModel();
				int size = model.getSize();
				for (int i = 0; i < size; i++) {
					SettingItem settingItem = model.getElementAt(i);
					if (settingItem.getRight().getVisibleRect().height > 0) {
						if (list.getSelectedIndex() != i) {
							list.setSelectedIndex(i);
						}
						break;
					}
				}
			}
		});
		itemPanel.addComponentListener(new ComponentAdapter() {
			boolean flag = false;

			@Override
			public void componentResized(ComponentEvent e) {
				if (flag)
					return;
				int height3 = rightPane.getHeight();
				Component component = itemPanel.getComponent(itemPanel.getComponentCount() - 1);
				int height4 = component.getHeight();
				if (height3 > height4) {
					JLabel comp = new JLabel();
					comp.setPreferredSize(new Dimension(0, height3 - height4 - 35));
					itemPanel.add(comp);
				}
				rightVerticalScrollBar.setValue(itemPanel.getComponent(0).getLocation().y);

				flag = true;
			}
		});
		addMouseListener(dragListener);
		addMouseMotionListener(dragListener);

		list.addMouseListener(dragListener);
		list.addMouseMotionListener(dragListener);
		rightPane.addMouseListener(dragListener);
		rightPane.addMouseMotionListener(dragListener);

		dragListener.setWindow(settingFrame);
	}

	public void addSettingItem(final SettingItem settingItem) {
		SettingItemPanel settingItemPanel = new SettingItemPanel(settingItem.getImage(), settingItem.getName());
		settingItem.setLeft(settingItemPanel);

		itemPanel.add(settingItem.getRight());

		listModel.addElement(settingItem);
	}

	public void clearSettingItems() {

		itemPanel.removeAll();

		listModel.removeAllElements();
	}

	public void updateList() {
		list.updateUI();
	}

	public JPanel getRightPane() {
		return rightPane;
	}

	public class SettingItemListCellRenderer extends JPanel implements ListCellRenderer<SettingItem> {
		@Override
		public Component getListCellRendererComponent(JList<? extends SettingItem> list, SettingItem settingItem, int index, boolean isSelected, boolean cellHasFocus) {
			SettingItemPanel panel = (SettingItemPanel) settingItem.getLeft();
			panel.setOpaque(true);
			panel.setSelected(isSelected || cellHasFocus);
			panel.setRollovered(((McWillList) list).getRolloverIndex() == index);
			return panel;
		}
	}

}
