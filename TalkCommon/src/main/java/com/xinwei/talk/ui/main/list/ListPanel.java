package com.xinwei.talk.ui.main.list;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillList;
import com.xinwei.common.lookandfeel.component.McWillListCellRenderer;
import com.xinwei.common.vo.Entry;
import com.xinwei.talk.listener.AvatarListener;
import com.xinwei.talk.listener.ObjectListener;
import com.xinwei.talk.listener.UnreadListener;
import com.xinwei.talk.manager.ChatManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.XWObject;
import com.xinwei.talk.model.avatar.Avatar;
import com.xinwei.talk.model.message.TalkMessage;

public class ListPanel<T extends XWObject> extends JPanel {
	private static final long serialVersionUID = -8486385306563904232L;
	protected LocalManager localManager = TalkManager.getLocalManager();

	protected JList<ListItem> list;

	protected DefaultListModel<ListItem> listModel;

	protected final Comparator<ListItem> comparator = new Comparator<ListItem>() {

		@Override
		public int compare(ListItem o1, ListItem o2) {
			String name1 = o1.getDisplayName();
			String name2 = o2.getDisplayName();
			if (name1 == null && name2 == null)
				return 0;
			if (name1 == null)
				return -1;
			if (name2 == null)
				return 1;
			return 0 - name1.compareTo(name2);
		}
	};

	public ListPanel() {
		this.setBackground((Color) UIManager.get("ContactItem.background"));

		this.setLayout(new GridLayout(1, 1));

		listModel = new DefaultListModel<ListItem>();
		list = new McWillList<ListItem>(listModel);
		list.setCellRenderer(new McWillListCellRenderer());
		list.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		SwingUtil.setOpaqueFalse(list);

		JScrollPane listScrollPane = new JScrollPane(list);

		listScrollPane.setBorder(BorderFactory.createEmptyBorder());
		listScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		add(listScrollPane);

		addListeners();
	}

	protected void addListeners() {
		list.addMouseListener(new MyMouseListener());
		list.addKeyListener(new MyKeyListener());

		ListenerManager.addAvatarListener(new MyAvatarListener());

		ListenerManager.addUnreadListener(new MyUnreadListener());
	}

	public void removeListItem(String jid) {
		List<ListItem> removes = new ArrayList<ListItem>();

		Enumeration<ListItem> elements = listModel.elements();
		while (elements.hasMoreElements()) {
			ListItem listItem = elements.nextElement();
			if (StringUtil.equals(listItem.getObject().getJid(), jid)) {
				removes.add(listItem);
			}
		}
		for (ListItem listItem : removes) {
			listModel.removeElement(listItem);
		}
		list.updateUI();
	}

	public ListItem getListItem(String jid) {
		for (int i = 0; i < listModel.getSize(); i++) {
			ListItem item = listModel.get(i);
			if (item.getObject().getJid().equals(jid)) {
				return item;
			}
		}
		return null;
	}

	private void checkPopup(MouseEvent e) {
		if (!e.isPopupTrigger())
			return;

		// Otherwise, handle single selection
		int index = list.locationToIndex(e.getPoint());
		if (index != -1) {
			int[] indexes = list.getSelectedIndices();
			boolean selected = false;
			for (int o : indexes) {
				if (index == o) {
					selected = true;
				}
			}

			if (!selected) {
				list.setSelectedIndex(index);
			}
		}

		List<Object> items = new ArrayList<Object>();

		for (Object item : list.getSelectedValues()) {
			items.add(item);
		}

		firePopupEvent(e, items);
	}

	final JPopupMenu popup = new JPopupMenu();

	private void firePopupEvent(MouseEvent e, final Collection<Object> items) {
		if (CollectionUtil.isEmpty(items)) {
			return;
		}

		T object = null;
		for (Object item : items) {
			ListItem contactItem = (ListItem) item;
			object = (T) contactItem.getObject();
			if (object != null) {
				break;
			}
		}

		if (object == null) {
			return;
		}
		popup.removeAll();
		addPopuMenuItemList(popup, object);

		popup.show(e.getComponent(), e.getX(), e.getY());

	}

	protected void addPopuMenuItemList(JPopupMenu popup, T object) {
	}

	protected void doubleClicked(ListItem item) {
		if (item == null)
			return;
		XWObject object = item.getObject();

		ChatManager.getInstance().activateChatRoom(object.getJid());
	}

	protected void repaintList() {
		list.invalidate();
		list.repaint();
	}

	//头像为空或者包含camtalk头像，则更新所有list界面
	class MyAvatarListener implements AvatarListener {
		@Override
		public void update(final List<Avatar> avatars) {
			//			boolean update = false;
			//			if (avatars == null) {
			//				update = true;
			//			} else {
			//				for (Avatar avatar : avatars) {
			//					if (avatar instanceof TalkAvatar) {
			//						update = true;
			//						break;
			//					}
			//				}
			//			}
			//			if (!update) {
			//				return;
			//			}
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					repaintList();
				}
			});
		}
	}

	class MyUnreadListener implements UnreadListener {
		@Override
		public void doUnreadMessage(String jid, Entry<Integer, TalkMessage> unreadMessage) {
			for (int i = 0; i < listModel.getSize(); i++) {
				ListItem item = listModel.get(i);
				if (!item.getObject().getJid().equals(jid)) {
					continue;
				}
				Rectangle cellBounds = list.getCellBounds(i, i);
				list.repaint(cellBounds);
			}
		}
	}

	public class MyObjectListener implements ObjectListener {
		@Override
		public void add(final Collection<? extends XWObject> objects) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					for (XWObject object : objects) {
						removeListItem(object.getJid());

						ListItem[] items = new ListItem[listModel.size()];
						listModel.copyInto(items);
						List<ListItem> asList = new ArrayList<ListItem>(Arrays.asList(items));

						ListItem item = new ListItem();
						item.setObject(object);

						asList.add(item);

						Collections.sort(asList, comparator);

						int index = asList.indexOf(item);

						if (index <= listModel.getSize()) {
							listModel.insertElementAt(item, index);
						}
					}
					repaintList();
				}

			});
		}

		@Override
		public void update(final Collection<? extends XWObject> objects) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					if (objects != null) {
						for (XWObject object : objects) {
							for (int index = 0; index < listModel.getSize(); index++) {
								ListItem item = listModel.get(index);
								if (!item.getObject().getJid().equals(object.getJid())) {
									continue;
								}
								item.setObject(object);
								//								Rectangle cellBounds = list.getCellBounds(index, index);
								//								list.repaint(cellBounds);
							}
						}
					}
					list.updateUI();
				}
			});
		}

		@Override
		public void delete(final Collection<? extends XWObject> objects) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					if (objects != null) {
						for (XWObject object : objects) {
							removeListItem(object.getJid());
						}
					}
					list.updateUI();
					//					repaintList();
				}
			});
		}
	}

	class MyMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getButton() == MouseEvent.BUTTON1) {
				if (e.getClickCount() == 2) {
					ListItem item = (ListItem) list.getSelectedValue();
					doubleClicked(item);
				}
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			checkPopup(e);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			checkPopup(e);
		}
	}

	class MyKeyListener extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyChar() == KeyEvent.VK_ENTER) {
				ListItem item = (ListItem) list.getSelectedValue();
				doubleClicked(item);
			}
		}
	}
}
