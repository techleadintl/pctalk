/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月24日 上午10:38:09
 * 
 ***************************************************************/
package com.xinwei.talk.ui.util.component;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.component.McWillIconButton;

@SuppressWarnings("serial")
public class XWEnableButton extends JPanel {
	private JButton button;
	private JLabel label;

	public XWEnableButton(ImageIcon buttonIcon, ImageIcon labelIcon) {
		button = new McWillIconButton(buttonIcon);
		label = new JLabel(labelIcon);
		setLayout(new BorderLayout());
	}

}
