package com.xinwei.talk.ui.util.component;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import com.xinwei.talk.common.LAF;

@SuppressWarnings("serial")
public class XWAvatarLabel extends JLabel {
	@Override
	protected void paintComponent(Graphics g) {
		LAF.paintAvatar((ImageIcon) getIcon(), this, g);
	};
}