/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月20日 下午5:55:55
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room.history;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

import com.xinwei.common.lang.DateUtil;
import com.xinwei.common.lookandfeel.component.McWillDatePicker;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.ui.McWillMenuItemColorUI;
import com.xinwei.common.lookandfeel.util.Constants;
import com.xinwei.common.vo.Entry;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.manager.MessageManager;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.chat.TranscriptWindow;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;

@SuppressWarnings("serial")
public class HistoryTranscriptWinow extends JPanel implements ActionListener {
	private static final int ERROR_ID = -100;
	TranscriptWindow transcriptWindow;
	JScrollPane transcriptWindowScrollPanel;

	private CommonChatRoom room;

	private int pageSize = 25;

	/** 记录的最大时间 **/
	private long maxDate;
	/** 记录的最小时间 **/
	private long minDate;
	/** 记录的最大ID **/
	private long maxId;
	/** 记录的最小ID **/
	private long minId;
	/** 当前界面记录的最大ID **/
	private long currentMaxId;
	/** 当前界面记录的最小ID **/
	private long currentMinId;
	/** 记录总数 **/
	private long count;

	private Date searchStartTime;
	private Date searchEndTime;
	private String searchStr;

	private JPanel infoPanel;
	private JLabel infoLabel;
	private JButton returnButton;

	private JPanel searchPanel;
	JComboBox<Entry<Integer, String>> searchTimeComboBox;
	private TextField searchTextField;

	private JButton searchButton;
	private JButton searchOkButton;
	private JButton dateButton;
	private JLabel dateLabel;
	private JButton preButton;
	private JButton nextButton;
	private JButton firstButton;
	private JButton lastButton;

	public HistoryTranscriptWinow(CommonChatRoom room) {
		this.room = room;
		setLayout(new BorderLayout());

		infoLabel = new JLabel();
		returnButton = new JButton(Res.getMessage("button.back"));

		infoPanel = new JPanel(new BorderLayout());
		infoPanel.putClientProperty(Constants.COLOR_TYPE, Constants.COLOR_MIDDLE);
		infoPanel.add(infoLabel, BorderLayout.WEST);
		infoPanel.add(returnButton, BorderLayout.EAST);
		infoPanel.setBackground(Color.RED);

		transcriptWindow = new TranscriptWindow();
		transcriptWindowScrollPanel = new JScrollPane(transcriptWindow);
		transcriptWindowScrollPanel.setAutoscrolls(true);
		transcriptWindowScrollPanel.getVerticalScrollBar().setBlockIncrement(200);
		transcriptWindowScrollPanel.getVerticalScrollBar().setUnitIncrement(20);
		transcriptWindowScrollPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		transcriptWindowScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		searchPanel = new QueryPanel();
		searchPanel.setVisible(false);

		JPanel centerPanel = new JPanel(new BorderLayout());
		centerPanel.add(infoPanel, BorderLayout.NORTH);
		centerPanel.add(transcriptWindowScrollPanel, BorderLayout.CENTER);
		centerPanel.add(searchPanel, BorderLayout.SOUTH);

		JPanel bottomPanel = new JPanel(new BorderLayout());
		JPanel bottomLeftPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel rigthLeftPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		bottomPanel.add(bottomLeftPanel, BorderLayout.WEST);
		bottomPanel.add(rigthLeftPanel, BorderLayout.EAST);

		preButton = new McWillIconButton(LAF.getPrevPageImageIcon());
		nextButton = new McWillIconButton(LAF.getNextPageImageIcon());
		firstButton = new McWillIconButton(LAF.getFirstPageImageIcon());
		lastButton = new McWillIconButton(LAF.getLastPageImageIcon());
		searchButton = new McWillIconButton(LAF.getQueryImageIcon());
		dateButton = new McWillIconButton(LAF.getDatePickerImageIcon());
		dateLabel = new JLabel();

		bottomLeftPanel.add(searchButton);
		bottomLeftPanel.add(dateButton);
		bottomLeftPanel.add(dateLabel);
		rigthLeftPanel.add(firstButton);
		rigthLeftPanel.add(preButton);
		rigthLeftPanel.add(nextButton);
		rigthLeftPanel.add(lastButton);

		add(centerPanel, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.SOUTH);
		//		pageSize = 2;

		initUI(false, false);

		dateButton.addActionListener(this);
		searchButton.addActionListener(this);
		preButton.addActionListener(this);
		nextButton.addActionListener(this);
		firstButton.addActionListener(this);
		lastButton.addActionListener(this);
		searchOkButton.addActionListener(this);
		returnButton.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == dateButton) {
			popupDataPicker();
		} else if (e.getSource() == searchButton) {
			boolean visible = searchPanel.isVisible();
			searchPanel.setVisible(!visible);
			if (visible) {
				initUI(false, false);
			}
		} else if (e.getSource() == preButton) {
			onButtonClick(false, null);
		} else if (e.getSource() == nextButton) {
			onButtonClick(true, null);
		} else if (e.getSource() == firstButton) {
			currentMaxId = minId - 1;
			onButtonClick(true, null);
		} else if (e.getSource() == lastButton) {
			currentMinId = maxId + 1;
			onButtonClick(false, null);
		} else if (e.getSource() == searchOkButton) {
			String text = searchTextField.getText();
			if (text.isEmpty()) {
				initUI(false, false);
			} else {
				initUI(true, true);
			}
		} else if (e.getSource() == returnButton) {//返回
			initUI(false, false);
		}

	}

	private void initUI(final boolean next, final boolean search) {
		searchStartTime = null;
		searchEndTime = null;
		searchStr = null;
		count = 0;
		maxDate = 0;
		minDate = 0;
		maxId = 0;
		minId = 0;
		currentMaxId = 0;
		currentMinId = 0;

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				infoPanel.setVisible(false);
				preButton.setEnabled(false);
				nextButton.setEnabled(false);
				firstButton.setEnabled(false);
				lastButton.setEnabled(false);

				if (search) {
					searchStr = searchTextField.getText();
				}
				searchEndTime = TalkManager.getXmppService().getServerDate();

				searchStartTime = getStartTime(search);

				final MessageManager historyManager = TalkManager.getMessageManager();

				final String roomId = room.getRoomId();

				Map<String, Object> messageTimeInfo = historyManager.getMessageTimeInfo(roomId, searchStr, searchStartTime, searchEndTime);

				Object maxObj = messageTimeInfo.get("MAXTID");
				Object minObj = messageTimeInfo.get("MINID");
				Object maxTimeObj = messageTimeInfo.get("MAXTIME");
				Object minTimeObj = messageTimeInfo.get("MINTIME");
				Object countObj = messageTimeInfo.get("COUNT");

				if (maxObj != null && minObj != null && maxTimeObj != null && minTimeObj != null && countObj != null) {
					maxId = ((Number) maxObj).longValue();
					minId = ((Number) minObj).longValue();
					maxDate = ((Number) maxTimeObj).longValue();
					minDate = ((Number) minTimeObj).longValue();
					count = ((Number) countObj).longValue();
				}

				if (search) {
					infoPanel.setVisible(true);
					searchPanel.setVisible(true);
					dateLabel.setVisible(false);
					dateButton.setVisible(false);
					infoLabel.setText(Res.getMessage("button.chatroom.chat.history.query.count", String.valueOf(count)));
				} else {
					dateLabel.setText(DateUtil.format(searchEndTime, "yyyy-MM-dd"));
					searchPanel.setVisible(false);
					dateLabel.setVisible(true);
					dateButton.setVisible(true);
				}

				if (count == 0) {
					query(ERROR_ID, true, null);
				} else {
					if (next) {
						query(minId - 1, true, null);
					} else {
						query(maxId + 1, false, null);
					}
				}

			}
		});
	}

	public void onButtonClick(boolean next, Date date) {
		long id;
		if (next) {
			id = currentMaxId;
		} else {
			id = currentMinId;
		}
		query(id, next, date);
	}

	private Date getStartTime(boolean search) {
		Integer key = 1000;
		if (search) {
			Entry<Integer, String> selectedItem = (Entry) searchTimeComboBox.getSelectedItem();
			key = selectedItem.getKey();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(searchEndTime);
		cal.add(Calendar.MONTH, 0 - key);
		return cal.getTime();
	}

	public void query(long msgMaxId, boolean next, Date date) {
		getTranscriptWindow().clear();

		if (msgMaxId == ERROR_ID) {
			return;
		}

		final MessageManager historyManager = TalkManager.getMessageManager();
		final String roomId = room.getRoomId();
		List<? extends TalkMessage> talkMessageList = null;
		if (date == null) {
			talkMessageList = historyManager.getMessageList(roomId, searchStr, msgMaxId, pageSize, next);
		} else {
			talkMessageList = historyManager.getMessageList(roomId, date, pageSize);
		}

		if (talkMessageList == null || talkMessageList.isEmpty()) {
			return;
		}

		long minD = 0;
		long maxD = 0;
		for (TalkMessage transcriptMessage : talkMessageList) {
			long id = transcriptMessage.getId();
			if (id < 1) {
				continue;
			}
			if (minD < 1 || id < minD) {
				minD = id;
			}
			if (maxD < 1 || id > maxD) {
				maxD = id;
			}
			getTranscriptWindow().insertHistroyMessage(room, transcriptMessage);
		}
		if (!next) {
			scrollToBottom();
		} else {
			scrollToTop();
		}
		if (minD < 1 || maxD < 1)
			return;

		currentMaxId = maxD;
		currentMinId = minD;

		if (currentMaxId < maxId) {
			nextButton.setEnabled(true);
			lastButton.setEnabled(true);
		} else {
			nextButton.setEnabled(false);
			lastButton.setEnabled(false);
		}

		if (currentMinId > minId) {
			preButton.setEnabled(true);
			firstButton.setEnabled(true);
		} else {
			preButton.setEnabled(false);
			firstButton.setEnabled(false);
		}

	}

	private void popupDataPicker() {
		final JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem();
		menuItem.setUI(new McWillMenuItemColorUI());
		menuItem.setLayout(new GridLayout(1, 1));

		final McWillDatePicker datePicker = new McWillDatePicker() {
			@Override
			protected String[] getWeekTitle() {
				List<String> list = new ArrayList<String>(7);
				for (int i = 0; i < 7; i++) {
					list.add(Res.getMessage("week." + i, ""));
				}
				String[] array = list.toArray(new String[7]);
				return array;
			}

			protected ImageIcon getNextYearImageIcon() {
				return LAF.getNextYearImageIcon();
			}

			protected ImageIcon getNextMonthImageIcon() {
				return LAF.getNextMonthImageIcon();
			}

			protected ImageIcon getPreMonthImageIcon() {
				return LAF.getPreMonthImageIcon();
			}

			protected ImageIcon getPreYearImageIcon() {
				return LAF.getPreYearImageIcon();
			}
		};
		if (maxDate > 0 && minDate > 0) {
			datePicker.setMaxCalendar(DateUtil.getDate(new Date(maxDate), "yyyy-MM-dd"));
			datePicker.setMinCalendar(DateUtil.getDate(new Date(minDate), "yyyy-MM-dd"));
		}
		datePicker.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				popupMenu.setVisible(false);
				Calendar selectedDate = datePicker.getSelectedDate();
				String format = DateUtil.format(selectedDate.getTime(), "yyyy-MM-dd");
				dateLabel.setText(format);

				onButtonClick(false, DateUtil.getDate(format, "yyyy-MM-dd"));
			}
		});

		menuItem.add(datePicker);

		popupMenu.add(menuItem);

		menuItem.setPreferredSize(new Dimension(230, 200));

		searchPanel.setVisible(false);

		//解决界面闪动问题
		menuItem.setEnabled(false);

		popupMenu.show(dateButton, 0, 0 - popupMenu.getPreferredSize().height);
		popupMenu.updateUI();
	}

	public void addListeners() {
		searchButton.addActionListener(new ActionListener() {
			boolean flag = false;

			@Override
			public void actionPerformed(ActionEvent e) {
				flag = !flag;
				searchPanel.setVisible(flag);
			}
		});
	}

	private TranscriptWindow getTranscriptWindow() {
		return transcriptWindow;
	}

	public void scrollToBottom() {
		int lengthOfChat = transcriptWindow.getDocument().getLength();
		transcriptWindow.setCaretPosition(lengthOfChat);

		try {
			final JScrollBar scrollBar = transcriptWindowScrollPanel.getVerticalScrollBar();
			EventQueue.invokeLater(new Runnable() {

				@Override
				public void run() {
					scrollBar.setValue(scrollBar.getMaximum());
				}
			});

		} catch (Exception e) {
			Log.error(e);
		}
	}

	public void scrollToTop() {
		transcriptWindow.setCaretPosition(0);

		try {
			final JScrollBar scrollBar = transcriptWindowScrollPanel.getVerticalScrollBar();
			EventQueue.invokeLater(new Runnable() {

				@Override
				public void run() {
					scrollBar.setValue(scrollBar.getMinimum());
				}
			});

		} catch (Exception e) {
			Log.error(e);
		}
	}

	class QueryPanel extends JPanel {
		public QueryPanel() {
			setOpaque(true);
			setLayout(new FlowLayout(FlowLayout.LEFT));
			Dimension preferredSize = new Dimension(120, 25);

			searchTimeComboBox = new JComboBox<Entry<Integer, String>>();
			searchTextField = new TextField();
			searchTimeComboBox.addItem(new Entry<Integer, String>(1, Res.getMessage("button.chatroom.chat.history.query.month")));
			searchTimeComboBox.addItem(new Entry<Integer, String>(3, Res.getMessage("button.chatroom.chat.history.query.month3")));
			searchTimeComboBox.addItem(new Entry<Integer, String>(12, Res.getMessage("button.chatroom.chat.history.query.year")));
			searchTimeComboBox.addItem(new Entry<Integer, String>(1000, Res.getMessage("button.chatroom.chat.history.query.all")));

			searchTimeComboBox.setPreferredSize(preferredSize);

			add(new JLabel(Res.getMessage("button.chatroom.chat.history.query.scope")));
			add(searchTimeComboBox);
			add(new JLabel(Res.getMessage("button.chatroom.chat.history.query.content")));
			searchTextField.setPreferredSize(preferredSize);
			add(searchTextField);

			searchOkButton = new JButton(Res.getMessage("button.ok"));
			add(searchOkButton);
			searchOkButton.setPreferredSize(new Dimension(60, 24));
		}

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			LAF.paintChatMessageSearchPane(this, g);
		}
	}

}
