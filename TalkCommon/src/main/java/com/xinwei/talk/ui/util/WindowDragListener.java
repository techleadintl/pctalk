/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月12日 下午2:33:50
 * 
 ***************************************************************/
package com.xinwei.talk.ui.util;

import java.awt.Point;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class WindowDragListener extends MouseAdapter {
	private Window window;
	private Point mouseDownCompCoords = null;

	public WindowDragListener() {
	}

	public WindowDragListener(Window window) {
		this.window = window;
	}

	public void mouseReleased(MouseEvent e) {
		mouseDownCompCoords = null;
	}

	public void mousePressed(MouseEvent e) {
		mouseDownCompCoords = e.getPoint();
	}

	public void mouseDragged(MouseEvent e) {
		Point currCoords = e.getLocationOnScreen();
		window.setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y - mouseDownCompCoords.y);
	}

	public Window getDialog() {
		return window;
	}

	public void setWindow(Window window) {
		this.window = window;
	}
}
