package com.xinwei.talk.ui.skin;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.RenderingHints;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.ThemeImage;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.lookandfeel.util.ColorUtil;
import com.xinwei.common.lookandfeel.util.PainterUtil;

@SuppressWarnings("serial")
public class McWillSkinContentPanel extends McWillContentPanel {
	private ThemeImage themeImage = null;
	private Color color1;
	private Color color2;

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		int height = 58;
		ThemeColor themeColor = McWillTheme.getThemeColor();
		if (McWillTheme.isThemeColor()) {

			Color color1 = themeColor.getSrcColor();

			Color color2 = themeColor.getSubLightColor();
			PainterUtil.paintBackgroudColor(g2d, color1, color2, 0, 0, getWidth(), height);

			PainterUtil.paintBackgroudColor(g2d, color2, 0, height, getWidth(), getHeight());
		} else if (McWillTheme.isThemeImage()) {
			ThemeImage themeImage = McWillTheme.getThemeImage();
			Image image = themeImage.getImage();
			if (image != null) {
				PainterUtil.tileBackgroudImage(this, image, g2d, 1f);
				if (this.themeImage != themeImage) {
					this.themeImage = themeImage;
					Color color = themeColor.getSrcColor();

					color1 = ColorUtil.getAlphaColor(color, 0);

					color2 = ColorUtil.getAlphaColor(Color.WHITE, 230);
				}
				Paint paint = g2d.getPaint();
				GradientPaint gp = new GradientPaint(0, 0, color1, 0, height, color2);
				g2d.setPaint(gp);
				g2d.fillRect(0, 0, getWidth(), getHeight());
				g2d.setPaint(paint);
			}
		}
//		// 绘制窗体边框
//		g2d.setStroke(new BasicStroke(1));
//		g2d.setPaint(McWillTheme.getThemeColor().getSubLightColor());
//		g2d.drawLine(0, WindowUtil.ARC / 2, 0, getHeight() - WindowUtil.ARC / 2);
//		g2d.drawLine(getWidth() - 1, WindowUtil.ARC / 2, getWidth() - 1, getHeight() - WindowUtil.ARC / 2);
//		g2d.drawLine(WindowUtil.ARC / 2, 0, getWidth() - WindowUtil.ARC / 2, 0);
//		g2d.drawLine(WindowUtil.ARC / 2, getHeight() - 1, getWidth() - WindowUtil.ARC / 2, getHeight() - 1);
//		g2d.setStroke(new BasicStroke(1.5f));
//		g2d.drawArc(0, 0, WindowUtil.ARC, WindowUtil.ARC, 90, 90);
////		g2d.drawArc(0, getHeight() - WindowUtil.ARC - 1, WindowUtil.ARC, WindowUtil.ARC, 180, 90);
//		g2d.drawArc(getWidth() - WindowUtil.ARC - 1, 0, WindowUtil.ARC, WindowUtil.ARC, 0, 90);
////		g2d.drawArc(getWidth() - WindowUtil.ARC - 1, getHeight() - WindowUtil.ARC - 1, WindowUtil.ARC, WindowUtil.ARC, 270, 90);

	}
}
