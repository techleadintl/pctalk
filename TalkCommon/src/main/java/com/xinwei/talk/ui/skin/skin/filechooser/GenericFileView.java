package com.xinwei.talk.ui.skin.skin.filechooser;

import java.awt.Image;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.filechooser.FileSystemView;
import javax.swing.filechooser.FileView;

public class GenericFileView extends FileView {
	private String[] fileExts;

	private String ext = "";

	private int fileType = DEFAULT_FILEVIEW;

	public final static int IMAGE_FILEVIEW = 80;

	public final static int DEFAULT_FILEVIEW = 70;

	public GenericFileView(String[] fileExts, int i) {
		super();
		if (fileExts.length >= 1) {
			this.fileExts = fileExts;

			if (i == IMAGE_FILEVIEW)
				fileType = IMAGE_FILEVIEW;
		}
	}

	public Icon getIcon(File f) {
		ext = getExtension(f);

		if (ext.equals("") || ext.equals("lnk")) {
			FileSystemView sv = FileSystemView.getFileSystemView();

			if (sv != null)
				return sv.getSystemIcon(f);

			return super.getIcon(f);
		}
		return getImageIcon(f);
	}

	private Icon getImageIcon(File f) {
		ImageIcon icon;
		for (int i = 0; i < fileExts.length; i++) {
			if (ext.equalsIgnoreCase(fileExts[i])) {

				icon = new ImageIcon(f.getAbsolutePath());

				Image image = icon.getImage();
				image = image.getScaledInstance(50, 50, Image.SCALE_DEFAULT);

				icon = new ImageIcon(image);

				return icon;
			}

		}
		return super.getIcon(f);
	}

	private String getExtension(File file) {
		String filename = file.getName();
		int length = filename.length();
		int i = filename.lastIndexOf('.');
		if (i > 0 && i < length - 1)
			return filename.substring(i + 1).toLowerCase();
		return new String("");
	}

}
