package com.xinwei.talk.ui.chat;
public class ChatRoomNotFoundException extends Exception {
	private static final long serialVersionUID = 517234944941907783L;

	public ChatRoomNotFoundException() {
        super();
    }

    public ChatRoomNotFoundException(String msg) {
        super(msg);
    }
}