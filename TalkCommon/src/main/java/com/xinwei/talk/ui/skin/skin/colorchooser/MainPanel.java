package com.xinwei.talk.ui.skin.skin.colorchooser;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.colorchooser.AbstractColorChooserPanel;

import com.xinwei.common.lookandfeel.McWillTheme;

@SuppressWarnings("serial")
public class MainPanel extends javax.swing.JPanel {

	public MainPanel() {
		initComponents();
	}

	public void setPreviewPanel(JComponent c) {
		previewPanelHolder.removeAll();
		if (c != null) {
			previewPanelHolder.add(c);
		}
	}

	public void addColorChooserPanel(final AbstractColorChooserPanel ccp) {
		final String displayName = ccp.getDisplayName();

		JPanel centerView = new JPanel(new BorderLayout());
		centerView.add(ccp);
		chooserPanelHolder.add(centerView, displayName);
	}

	public void removeAllColorChooserPanels() {
		chooserPanelHolder.removeAll();

		northPanel.removeAll();
		northPanel.add(previewPanelHolder);
	}

	private void initComponents() {
		mainPanel = new javax.swing.JPanel();
		northPanel = new javax.swing.JPanel();
		previewPanelHolder = new javax.swing.JPanel();
		chooserPanelHolder = new javax.swing.JPanel();

		setLayout(new java.awt.BorderLayout());

		mainPanel.setLayout(new java.awt.BorderLayout());

		mainPanel.setBorder(new javax.swing.border.EmptyBorder(new java.awt.Insets(5, 4, 7, 4)));
		northPanel.setLayout(new java.awt.BorderLayout());

		previewPanelHolder.setLayout(new java.awt.BorderLayout());

		northPanel.add(previewPanelHolder, java.awt.BorderLayout.CENTER);

		mainPanel.add(northPanel, java.awt.BorderLayout.NORTH);

		chooserPanelHolder.setLayout(new java.awt.CardLayout());

		chooserPanelHolder.setBorder(new javax.swing.border.EmptyBorder(new java.awt.Insets(5, 0, 0, 0)));
		mainPanel.add(chooserPanelHolder, java.awt.BorderLayout.CENTER);

		add(mainPanel, java.awt.BorderLayout.CENTER);
		
//		setOpaque(false);
		
		setBackground(McWillTheme.getThemeColor().getSrcColor());

	}

	// Variables declaration
	private javax.swing.JPanel chooserPanelHolder;
	private javax.swing.JPanel mainPanel;
	private javax.swing.JPanel northPanel;
	private javax.swing.JPanel previewPanelHolder;

}
