package com.xinwei.talk.ui.main.dialog.group;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextField;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillRadioGroup;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.ui.util.component.XWDialogTitle;
import com.xinwei.talk.ui.util.component.XWMcHintTextField;
import com.xinwei.talk.ui.util.component.dialog.XWDialog;

@SuppressWarnings("serial")
public class GroupDialog extends XWDialog {

	private JTextField groupNameField;

	private McWillRadioGroup<Integer> sizeRadioGroup;

	private ListPanel memberPanel;

	/**
	 * Create a new instance of RosterDialog.
	 */
	public GroupDialog() {
		super(TalkManager.getMainWindow(), "", false);
		createDialog();
		JRootPane rootPane = getRootPane();
		((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);
		setSize(new Dimension(575, 450));
	}

	@Override
	protected JPanel createDialogContainer() {
		return new McWillContentPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				LAF.paintMainAddGroupDialog(this, (Graphics2D) g);
			}
		};
	}

	@Override
	protected JPanel createDialogContent() {
		JPanel dialogPanel = new JPanel(new BorderLayout());
		XWDialogTitle titlePanel = new XWDialogTitle(this, Res.getMessage("tabTitle.group.add"));
		titlePanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 0));
		dialogPanel.add(titlePanel, BorderLayout.NORTH);

		dialogPanel.add(createInputPanel(), BorderLayout.CENTER);

		return dialogPanel;
	}

	private JPanel createInputPanel() {
		JPanel inputPanel = new JPanel();

		//组名称
		JLabel groupNameLabel = new JLabel();
		groupNameLabel.setText(Res.getMessage("field.group.name"));

		groupNameField = new XWMcHintTextField(getGroupNameDefaultText());
		groupNameField.setPreferredSize(new Dimension(300, 27));

		JLabel groupSizeLabel = new JLabel();
		groupSizeLabel.setText(Res.getMessage("field.group.size"));

		JPanel groupSizePanel = new JPanel();
		groupSizePanel.setLayout(new FlowLayout(FlowLayout.LEFT));

		sizeRadioGroup = new McWillRadioGroup<Integer>();
		sizeRadioGroup.addRadio(groupSizePanel, Res.getMessage("field.group.size.unit", "50"), false, 50);
		sizeRadioGroup.addRadio(groupSizePanel, Res.getMessage("field.group.size.unit", "100"), true, 100);
		sizeRadioGroup.addRadio(groupSizePanel, Res.getMessage("field.group.size.unit", "150"), false, 150);
		sizeRadioGroup.addRadio(groupSizePanel, Res.getMessage("field.group.size.unit", "200"), false, 200);

		memberPanel = new ListPanel();

		inputPanel.setLayout(new GridBagLayout());
		inputPanel.add(groupNameLabel, new GridBagConstraints(0, 0, 1, 1, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		inputPanel.add(groupNameField, new GridBagConstraints(1, 0, 1, 1, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

		//		inputPanel.add(groupDescLabel, new GridBagConstraints(0, 1, 1, 1, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		//		inputPanel.add(groupDescField, new GridBagConstraints(1, 1, 1, 1, 1.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));

		inputPanel.add(groupSizeLabel, new GridBagConstraints(0, 2, 1, 1, 0.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		inputPanel.add(groupSizePanel, new GridBagConstraints(1, 2, 1, 1, 1.0D, 0.0D, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));

		inputPanel.add(memberPanel, new GridBagConstraints(0, 4, 2, 1, 1.0D, 1.0D, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

		return inputPanel;
	}

	@Override
	protected boolean onValidate() {
		String text = groupNameField.getText().trim();
		if (getGroupNameDefaultText().equals(text) || "".equals(text)) {
			JOptionPane.showMessageDialog(groupNameField, Res.getMessage("warn.group.name"), Res.getMessage("tabTitle.notification"), JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	@Override
	protected boolean onOkPressed() {
		Integer size = sizeRadioGroup.getSelected();

		VCard vCard = TalkManager.getVCard();
		String creator = String.valueOf(vCard.getUid());

		Group xwGroup = new Group();
		xwGroup.setCreatorUid(creator);
		//		xwGroup.setDescribe(groupDescField.getText());
		xwGroup.setMaxCount(size);
		xwGroup.setName(groupNameField.getText());

		List<Member> memberList = new ArrayList<>();

		Member self = new Member();
		self.setUid(creator);
		self.setMemNick(vCard.getNick());
		self.setMemJid(vCard.getJid());
		self.setMemRole(Member.ROLE_ADMIN);

		memberList.add(self);

		Enumeration<Talk> selectedContacts = memberPanel.getSelectedContacts();
		while (selectedContacts.hasMoreElements()) {
			Talk camTalk = selectedContacts.nextElement();
			if (StringUtil.notEquals(camTalk.getUid(), creator)) {
				Member m2 = new Member();
				m2.setUid(camTalk.getUid());
				m2.setMemNick(TalkUtil.getDisplayName(camTalk.getJid()));
				m2.setMemJid(camTalk.getJid());
				m2.setMemRole(Member.ROLE_USER);
				memberList.add(m2);
			}
		}
		try {
			String groupId = TalkManager.getTalkService().createGroup(xwGroup, memberList);
			xwGroup.setId(groupId);
			return super.onOkPressed();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Sets the default jid to show in the jid field.
	 *
	 * @param jid
	 *            the jid.
	 */
	public void setDefaultJID(String jid) {
		groupNameField.setText(jid);
	}

	private String getGroupNameDefaultText() {
		return Res.getMessage("tip.group.name");
	}

	@Override
	protected LayoutManager getButtonBarLayout() {
		return new FlowLayout(FlowLayout.RIGHT, 30, 5);
	}

	protected JPanel createButtonBar() {
		JPanel bottomButtonPanel = new JPanel(getButtonBarLayout());
		bottomButtonPanel.setOpaque(false);

		closeButton = SwingUtil.newButton(closeTxt);
		okButton = SwingUtil.newButton(Res.getMessage("button.group.add.ok"));

		okButton.addActionListener(this);
		closeButton.addActionListener(this);

		bottomButtonPanel.add(closeButton);
		bottomButtonPanel.add(okButton);

		return bottomButtonPanel;
	}

	/**
	 * Display the RosterDialog using the MainWindow as the parent.
	 */
	public void showDialog() {
		setLocationRelativeTo(null);

		setVisible(true);
		toFront();
		requestFocus();

		//		groupNameField.requestFocus();
	}

}