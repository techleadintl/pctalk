/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月23日 下午3:17:53
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room.history;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.xinwei.common.lang.DateUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillEditList;
import com.xinwei.common.lookandfeel.component.McWillEditListCellComponent;
import com.xinwei.common.lookandfeel.component.McWillLinkButton;
import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
import com.xinwei.common.lookandfeel.util.Constants;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.common.vo.Entry;
import com.xinwei.spark.ui.MessageDialog;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.MessageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;

@SuppressWarnings("serial")
public class HistoryFileWindow extends JPanel {

	private CommonChatRoom room;

	public HistoryFileWindow(CommonChatRoom room) {
		this.room = room;

		setOpaque(true);

		setLayout(new BorderLayout(0, 0));

		Map<String, List<TalkFileSpecific>> fileMsgMap = null;
		try {
			fileMsgMap = getFileMsgMap();
		} catch (Exception e) {
			e.printStackTrace();
		}

		JScrollPane panel = createPanel(fileMsgMap);

		add(panel, BorderLayout.CENTER);

	}

	public Map<String, List<TalkFileSpecific>> getFileMsgMap() throws Exception {
		MessageManager messageManager = TalkManager.getMessageManager();
		List<TalkMessage> talkMessageList = messageManager.getMessageList(room.getRoomId(), com.xinwei.talk.common.Constants.MSG_TYPE_FILE);
		if (talkMessageList == null) {
			return null;
		}

		Map<String, List<TalkFileSpecific>> fileMap = new HashMap<>();

		for (TalkMessage message : talkMessageList) {
			Date time = message.getTalkBody().getTime();
			if (time == null) {
				time = new Date();
			}
			String timeStr = DateUtil.format(time, DateUtil.PATTERN_DEFAULT);

			List<TalkSpecific> fileList = message.getSpecificList(com.xinwei.talk.common.Constants.MSG_TYPE_FILE);
			if (fileList == null) {
				continue;
			}
			for (TalkSpecific msg : fileList) {
				TalkFileSpecific fileMsg = (TalkFileSpecific) msg;
				int fileType = fileMsg.getFileType();
				if (fileType == TalkFileSpecific.SEND_COMPLETE || fileType == TalkFileSpecific.RECEIVE_COMPLETE) {
					List<TalkFileSpecific> list = fileMap.get(timeStr);
					if (list == null) {
						list = new ArrayList<>();
						fileMap.put(timeStr, list);
					}
					list.add(fileMsg);
				}
			}
		}
		return fileMap;
	}

	public JScrollPane createPanel(Map<String, List<TalkFileSpecific>> fileMap) {
		McWillEditList<FileMsgPanel> fileListUI = new McWillEditList<FileMsgPanel>(ListCellComponent.class);
		fileListUI.setRowHeight(70);
		if (fileMap != null) {
			ArrayList<String> timeList = new ArrayList<String>(fileMap.keySet());

			Collections.sort(timeList);

			for (final String timeStr : timeList) {
				List<TalkFileSpecific> fileList = fileMap.get(timeStr);
				if (fileList == null) {
					continue;
				}

				for (TalkFileSpecific fileMsg : fileList) {
					fileListUI.addElement(new FileMsgPanel(new Entry<String, TalkFileSpecific>(timeStr, fileMsg)));
				}
			}
		}
		JScrollPane jsp = new JScrollPane(fileListUI);
		jsp.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		return jsp;
	}

	public static class ListCellComponent extends McWillEditListCellComponent<FileMsgPanel> {
		@Override
		protected void onSelected(FileMsgPanel component, JTable table, Object value, boolean isSelected, int row, int column) {
			component.setOpaque(true);
			super.onSelected(component, table, value, isSelected, row, column);
		}

		@Override
		protected void onHovered(FileMsgPanel compoent, JTable table, Object value, boolean isSelected, int row, int column) {
			compoent.getCardLayout().show(compoent.getButtonPanel(), "two"); // 显示指定的内容
		}

		@Override
		protected void onDefault(FileMsgPanel compoent, JTable table, Object value, boolean isSelected, int row, int column) {
			compoent.getCardLayout().show(compoent.getButtonPanel(), "one"); // 显示指定的内容
		}

		@Override
		protected FileMsgPanel getTableCellComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			return (FileMsgPanel) value;
		}
	}

	public static class FileMsgPanel extends JPanel {
		McWillRolloverLabel avatarIconLabel;

		JLabel fileNameLabel;
		JLabel timeLabel;

		McWillLinkButton openLabel;
		McWillLinkButton openDirLabel;
		//		McWillLinkButton deleteLabel;

		JPanel buttonPanel;
		CardLayout cardLayout;

		File file;
		Entry<String, TalkFileSpecific> entry;

		public FileMsgPanel(Entry<String, TalkFileSpecific> entry) {
			this.entry = entry;
			createUI();
			initData();
			initListeners();
		}

		protected void createUI() {
			setOpaque(true);
			setLayout(new GridBagLayout());

			avatarIconLabel = new McWillRolloverLabel();
			avatarIconLabel.setPreferredSize(new Dimension(50, 50));
			avatarIconLabel.setOpaque(false);

			fileNameLabel = new JLabel();
			fileNameLabel.setHorizontalTextPosition(JLabel.LEFT);
			fileNameLabel.setHorizontalAlignment(JLabel.LEFT);

			timeLabel = new JLabel();
			timeLabel.setHorizontalTextPosition(JLabel.LEFT);
			timeLabel.setHorizontalAlignment(JLabel.LEFT);

			openLabel = new McWillLinkButton(Res.getMessage("button.open"));
			openDirLabel = new McWillLinkButton(Res.getMessage("button.opendir"));
			//			deleteLabel = new McWillLinkButton(Res.getMessage("button.delete"));

			JLabel infoLabel = new JLabel("OK");
			infoLabel.setHorizontalTextPosition(JLabel.RIGHT);
			infoLabel.setHorizontalAlignment(JLabel.RIGHT);

			JPanel jPanel = new JPanel(new GridLayout(2, 1));
			jPanel.add(fileNameLabel);
			jPanel.add(timeLabel);

			JPanel jp = new JPanel(new FlowLayout(FlowLayout.TRAILING));
			jp.add(openLabel);
			jp.add(openDirLabel);
			//			jp.add(deleteLabel);

			buttonPanel = new JPanel();
			buttonPanel.setOpaque(false);
			cardLayout = new CardLayout(); // 创建卡片布局的对象
			buttonPanel.setLayout(cardLayout);// 重新设置面板的布局 为卡片布局
			buttonPanel.add(jp, "two"); // 必须指定 标识符 ，如果没有标识符		
			buttonPanel.add(infoLabel, "one"); // 必须指定 标识符 ，如果没有标识符

			add(avatarIconLabel, new GridBagConstraints(0, 0, 1, 1, 0.0D, 0.0D, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
			add(jPanel, new GridBagConstraints(1, 0, 1, 1, 0.0D, 1.0D, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 0), 0, 0));
			add(buttonPanel, new GridBagConstraints(2, 0, 1, 1, 1.0D, 1.0D, GridBagConstraints.EAST, GridBagConstraints.VERTICAL, new Insets(5, 0, 5, 0), 0, 0));

		}

		private void initData() {
			TalkFileSpecific value = entry.getValue();
			file = new File(entry.getValue().getFileAbsolutePath());

			String fileName = file.getName();
			fileNameLabel.setToolTipText(fileName);
			String text = SwingUtil.format(file.length());
			fileName = SwingUtil.getString(fileName, fileNameLabel.getFont(), 180, " (" + text + ")");
			fileNameLabel.setText(fileName);

			timeLabel.setText(entry.getKey());

			avatarIconLabel.setIcon(ImageUtil.resizeImageIcon(SwingUtil.getFileImageIcon(value.getFilename()), 50, 50));
		}

		private void initListeners() {
			openLabel.putClientProperty(Constants.CLICK, true);

			avatarIconLabel.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2) {
						SwingUtil.openFile(file);
					}
				}
			});
			fileNameLabel.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2) {
						SwingUtil.openFile(file);
					}
				}
			});
			openLabel.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					boolean exists = file.exists();
					if (!exists) {
						MessageDialog.showAlert(TalkManager.getChatFrame(), Res.getMessage("message.file.exists.not", file.getAbsolutePath()));
						return;
					}
					String errorMsg = SwingUtil.openFile(file);
					if (errorMsg != null) {
						MessageDialog.showAlert(TalkManager.getChatFrame(), errorMsg, 400, 300);
					}
				}
			});

			openDirLabel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					boolean exists = file.getParentFile().exists();
					if (!exists) {
						MessageDialog.showAlert(TalkManager.getChatFrame(), Res.getMessage("message.file.exists.not", file.getParentFile()));
						return;
					}
					SwingUtil.openFile(file.getParentFile());
				}
			});
		}

		public JPanel getButtonPanel() {
			return buttonPanel;
		}

		public CardLayout getCardLayout() {
			return cardLayout;
		}
	}
}
