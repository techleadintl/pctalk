/**
 * $RCSfile: ,v $
 * $Revision: $
 * $Date: $
 * 
 * Copyright (C) 2004-2011 Jive Software. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xinwei.talk.ui.util.component.label;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;

/**
 * The <code>LinkLabel</code> class is a JLabel subclass to mimic an html link. When clicked, it launches the specified url in the default browser.
 */
final public class LinkLabel extends JLabel implements MouseListener {

	private static final long serialVersionUID = 454820993140807217L;
	// cursors used in url-link related displays and default display
	private Cursor DEFAULT_CURSOR = new Cursor(Cursor.DEFAULT_CURSOR);
	private Cursor LINK_CURSOR = new Cursor(Cursor.HAND_CURSOR);
	private Color rolloverTextColor;
	private Color foregroundTextColor;

	public LinkLabel() {
		this(null);
	}

	public LinkLabel(String text) {
		this(text, Color.BLUE, Color.RED);
	}

	/**
	 * @param text
	 *            the text for the label.
	 * @param url
	 *            the url to launch when this label is clicked on.
	 * @param foregroundColor
	 *            the text color for the label.
	 * @param rolloverColor
	 *            the text color to be used when the mouse is over the label.
	 */

	public LinkLabel(String text, Color foregroundColor, Color rolloverColor) {
		super(text);

		rolloverTextColor = rolloverColor;
		foregroundTextColor = foregroundColor;

		this.addMouseListener(this);

		this.setForeground(foregroundTextColor);
		this.setVerticalTextPosition(JLabel.TOP);
	}

	/**
	 * MouseListener implementation.
	 *
	 * @param me
	 *            the MouseEvent.
	 */
	public void mouseClicked(MouseEvent me) {
	}

	public void mousePressed(MouseEvent me) {
	}

	public void mouseReleased(MouseEvent me) {
	}

	public void mouseEntered(MouseEvent me) {
		this.setForeground(rolloverTextColor);
		this.setCursor(LINK_CURSOR);
	}

	public void mouseExited(MouseEvent me) {
		this.setForeground(foregroundTextColor);
		this.setCursor(DEFAULT_CURSOR);
	}

}
