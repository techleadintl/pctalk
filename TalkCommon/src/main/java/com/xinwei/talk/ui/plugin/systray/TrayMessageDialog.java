/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月7日 上午9:49:33
 * 
 ***************************************************************/
package com.xinwei.talk.ui.plugin.systray;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.component.McWillList;
import com.xinwei.common.lookandfeel.component.McWillListCellRenderer;
import com.xinwei.common.lookandfeel.component.McWillScrollPane;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.vo.Entry;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.MessageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.util.component.avatar.AvatarPanel2;

public class TrayMessageDialog extends JDialog {
	McWillList<MsgPanel> list;
	DefaultListModel<MsgPanel> listModel;

	int DIALOG_WIDTH = 265;

	JButton eastButton;
	JButton centerButton;

	public TrayMessageDialog(final SysTrayPlugin sysTrayPlugin) {
		setSize(DIALOG_WIDTH, 250);
		//		setDefaultCloseOperation(3);
		setLayout(new BorderLayout());
		setUndecorated(true);//去除窗体
		setAlwaysOnTop(true); //设置界面悬浮

		JRootPane rootPane = getRootPane();
		((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

		//		rootPane.setBorder(BorderFactory.createEmptyBorder());

		setLocationRelativeTo(new JFrame());

		setResizable(false);
		listModel = new DefaultListModel<MsgPanel>();
		list = new McWillList<MsgPanel>(listModel);
		list.setCellRenderer(new McWillListCellRenderer());
		list.addMouseListener(new MyMouseListener());
		list.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		McWillScrollPane listScrollPane = new McWillScrollPane(list);

		JPanel contentPane = createDialogContainer();
		setContentPane(contentPane);

		contentPane.add(listScrollPane, BorderLayout.CENTER);

		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(0, 13, 5, 13));
//		boolean avatarFlash = sysTrayPlugin.isAvatarFlash();
		String text =  TalkManager.getUserConfig().getAvatarFlash()? Res.getMessage("tray.button.stopflash") : Res.getMessage("tray.button.openflash");
		eastButton = createButton(LAF.getTrayIgnoreIcon(), Res.getMessage("tray.button.ignoreall"));
		centerButton = createButton(LAF.getGreenStar(), Res.getMessage("tray.button.openall"));

		JLabel line = new JLabel(LAF.getLine());

		panel.add(line, BorderLayout.NORTH);
		//		panel.add(westButton, BorderLayout.WEST);
		panel.add(eastButton, BorderLayout.EAST);
		panel.add(centerButton, BorderLayout.WEST);
		panel.setOpaque(false);

		panel.setPreferredSize(new Dimension(-1, 30));
		contentPane.add(panel, BorderLayout.SOUTH);

		eastButton.setFont(LAF.getFont(12));
		centerButton.setFont(LAF.getFont(12));

		eastButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Enumeration<MsgPanel> elements = listModel.elements();
				while (elements.hasMoreElements()) {
					MsgPanel msgPanel = elements.nextElement();
					TalkManager.getMessageManager().clearUnreadMessageCount(msgPanel.getJid());
				}
				setVisible(false);
			}
		});

		centerButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				while (!listModel.isEmpty()) {
					MsgPanel msgPanel = listModel.getElementAt(0);
					activateChatRoom(msgPanel);
				}
				setVisible(false);
			}
		});
	}

	private JButton createButton(ImageIcon icon, String text) {
		McWillIconButton button = new McWillIconButton(icon);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.setText(text);
		button.setCursor(new Cursor(Cursor.HAND_CURSOR));
		button.setFocusPainted(false);
		button.setBorder(BorderFactory.createEmptyBorder());
		return button;
	}

	protected JPanel createDialogContainer() {
		return new McWillContentPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				LAF.paintTrayDialog(this, g);
			}
		};
	}

	@Override
	public void setVisible(boolean b) {
		if (!listModel.isEmpty()) {
			listModel.clear();
		}
		if (b) {
			MessageManager messageManager = TalkManager.getMessageManager();
			//添加监听器
			Map<String, Entry<Integer, TalkMessage>> unreadMessageMap = messageManager.getUnreadMessageMap();
			if (unreadMessageMap.isEmpty()) {
				b = false;
			} else {
				Set<Map.Entry<String, Entry<Integer, TalkMessage>>> entrySet = unreadMessageMap.entrySet();
				for (Map.Entry<String, Entry<Integer, TalkMessage>> entry : entrySet) {
					String jid = entry.getKey();
					Entry<Integer, TalkMessage> message = entry.getValue();
					listModel.addElement(new MsgPanel(jid, message));
				}
				int height = listModel.size() * 48;
				if (listModel.size() * 48 > 600) {
					height = 600;
				}

				setSize(DIALOG_WIDTH, height + 35);
				list.updateUI();
			}
		} else {
			//删除监听器

		}
		super.setVisible(b);
	}

	public void activateChatRoom(MsgPanel msgPanel) {
		if (msgPanel == null) {
			return;
		}
		String jid = msgPanel.getJid();
		TalkManager.getChatManager().activateChatRoom(jid);
		listModel.removeElement(msgPanel);
		list.updateUI();
		if (listModel.isEmpty()) {
			setVisible(false);
		}
	}

	public void activateFirst() {
		if (listModel.isEmpty()) {
			return;
		}
		MsgPanel msgPanel = listModel.get(0);
		activateChatRoom(msgPanel);
	}

	class MyMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getButton() == MouseEvent.BUTTON1) {
				if (e.getClickCount() == 1) {
					MsgPanel msgPanel = (MsgPanel) list.getSelectedValue();
					activateChatRoom(msgPanel);
				}
			}
		}

	}

	public class MsgPanel extends JPanel {

		private AvatarPanel2 imageLabel;
		private JLabel displayNameLabel;
		private JLabel descriptionLabel;
		private JLabel specialLabel;
		private String jid;
		private Entry<Integer, TalkMessage> unreadMessage;

		public MsgPanel(String jid, Entry<Integer, TalkMessage> message) {
			this.jid = jid;
			this.unreadMessage = message;

			setOpaque(true);

			setLayout(new GridBagLayout());

			setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

			createUI(jid, message);
			updateUIData();
		}

		private void createUI(String jid, Entry<Integer, TalkMessage> message) {
			imageLabel = new AvatarPanel2();

			displayNameLabel = new JLabel();
			displayNameLabel.setFont(LAF.getFont(13));
			descriptionLabel = new JLabel();
			descriptionLabel.setFont(LAF.getFont(12));
			specialLabel = new JLabel() {
				@Override
				protected void paintComponent(Graphics g) {
					Graphics2D g2d = (Graphics2D) g;
					g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
					g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
					g.setColor(McWillTheme.getThemeColor().getSubDarkColor());
					Rectangle2D stringBounds = SwingUtil.getStringBounds("13", g.getFont());
					double width2 = stringBounds.getWidth();
					double height2 = stringBounds.getHeight();
					g.fillRoundRect((int) (getWidth() - width2) / 2 - 4 + 1, (int) (getHeight() - height2) / 2 + 1, (int) width2 + 8, (int) height2, 10, 10);
					super.paintComponent(g);
				}
			};
			specialLabel.setFont(LAF.getFont(10));
			specialLabel.setForeground(Color.WHITE);
			specialLabel.setHorizontalAlignment(SwingConstants.CENTER);

			displayNameLabel.setPreferredSize(new Dimension(165, 20));
			descriptionLabel.setPreferredSize(new Dimension(200, 20));
			specialLabel.setPreferredSize(new Dimension(50, 20));
			imageLabel.setPreferredSize(new Dimension(40, 40));
			descriptionLabel.setForeground(LAF.getColor(150, 150, 150));

			add(imageLabel, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
			add(displayNameLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
			add(descriptionLabel, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
			add(specialLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		}

		public void updateUIData() {
			TalkMessage message = unreadMessage.getValue();

			Integer count = unreadMessage.getKey();
			if (count == null) {
				count = 0;
			}

			specialLabel.setText(String.valueOf(count));
			imageLabel.setJid(jid);
			displayNameLabel.setText(TalkUtil.getDisplayName(jid));

			descriptionLabel.setText(MessageUtil.getMessagePrompt(message));
		}

		public Entry<Integer, TalkMessage> getUnreadMessage() {
			return unreadMessage;
		}

		public String getJid() {
			return jid;
		}

		protected Color DEFAULT_COLOR = new Color(240, 240, 240);

		protected Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 11);

	}
}
