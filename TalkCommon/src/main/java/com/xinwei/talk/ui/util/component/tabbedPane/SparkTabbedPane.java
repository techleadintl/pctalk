package com.xinwei.talk.ui.util.component.tabbedPane;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.xinwei.common.lookandfeel.util.Constants;
import com.xinwei.spark.ModelUtil;
import com.xinwei.spark.SwingWorker;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;

public class SparkTabbedPane extends JPanel {
	private static final long serialVersionUID = -9007068462231539973L;
	private static final String NAME = "TabbedPane";
	private List<SparkTabbedPaneListener> listeners = new ArrayList<SparkTabbedPaneListener>();
	private JTabbedPane pane = null;
	private Icon closeInactiveButtonIcon;
	private Icon closeActiveButtonIcon;
	private boolean closeEnabled = false;
	private int dragTabIndex = -1;

	/**
	 * The default Hand cursor.
	 */
	public static final Cursor HAND_CURSOR = new Cursor(Cursor.HAND_CURSOR);

	/**
	 * The default Text Cursor.
	 */
	public static final Cursor DEFAULT_CURSOR = new Cursor(Cursor.DEFAULT_CURSOR);

	public SparkTabbedPane() {
		this(JTabbedPane.TOP);
	}

	public SparkTabbedPane(final Integer type) {
		this(type.intValue());
	}

	public SparkTabbedPane(final int type) {

		pane = buildTabbedPane(type);
		pane.setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);

		setLayout(new BorderLayout());
		add(pane);
		ChangeListener changeListener = new ChangeListener() {
			public void stateChanged(ChangeEvent changeEvent) {
				JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
				int index = sourceTabbedPane.getSelectedIndex();
				if (index >= 0) {
					fireTabSelected(getTabAt(index), getTabAt(index).getComponent(), index);
				}
			}
		};
		pane.addChangeListener(changeListener);

		//		closeInactiveButtonIcon = SparkRes.getImageIcon(SparkRes.CLOSE_WHITE_X_IMAGE);
		//		closeActiveButtonIcon = SparkRes.getImageIcon(SparkRes.CLOSE_DARK_X_IMAGE);
		closeInactiveButtonIcon = LAF.getImageIcon("images/cootalk/chat/closeInactive.png");
		closeActiveButtonIcon = LAF.getImageIcon("images/cootalk/chat/closeActive.png");

		pane.setOpaque(false);

	}

	public void show(Component comp, boolean show) {
		if (show) {
			remove(pane);
			add(comp);
		} else {
			remove(comp);
			add(pane);
		}
	}

	public SparkTab getTabContainingComponent(Component component) {
		for (Component comp : pane.getComponents()) {
			if (comp instanceof SparkTab) {
				SparkTab tab = (SparkTab) comp;
				if (tab.getComponent() == component)
					return tab;
			}
		}
		return null;
	}

	public SparkTab addTab(String title, Icon icon, final Component component) {
		return addTab(title, icon, component, null);
	}

	public SparkTab addTab(String title, Icon icon, final Component component, String tip) {
		final SparkTab sparktab = new SparkTab(this, component);

		TabPanel tabpanel = new TabPanel(sparktab, title, icon);
		pane.addTab(title, null, sparktab, tip);

		pane.setTabComponentAt(pane.getTabCount() - 1, tabpanel);
		fireTabAdded(sparktab, component, getTabPosition(sparktab));

		return sparktab;
	}

	public SparkTab getTabAt(int index) {
		return ((SparkTab) pane.getComponentAt(index));
	}

	public int getTabPosition(SparkTab tab) {
		return pane.indexOfComponent(tab);
	}

	public Component getComponentInTab(SparkTab tab) {
		return tab.getComponent();
	}

	public void setIconAt(int index, Icon icon) {
		Component com = pane.getTabComponentAt(index);
		if (com instanceof TabPanel) {
			TabPanel panel = (TabPanel) com;
			panel.setIcon(icon);
		}
	}

	public void setTitleAt(int index, String title) {
		if (index > 0) {
			Component com = pane.getTabComponentAt(index);
			if (com instanceof TabPanel) {
				TabPanel panel = (TabPanel) com;
				panel.setTitle(title);
			}
		}
	}

	public void setTitleColorAt(int index, Color color) {

		Component com = pane.getTabComponentAt(index);
		if (com instanceof TabPanel) {
			TabPanel panel = (TabPanel) com;
			panel.setTitleColor(color);
		}
	}

	/*
	 * Updates the tab colors for unread,active and inactive tabs
	 */
	public void updateActiveTab() {

	}

	public void setTitleBoldAt(int index, boolean bold) {
		Component com = pane.getTabComponentAt(index);
		if (com instanceof TabPanel) {
			TabPanel panel = (TabPanel) com;
			panel.setTitleBold(bold);
		}
	}

	public void setTitleFontAt(int index, Font font) {
		Component com = pane.getTabComponentAt(index);
		if (com instanceof TabPanel) {
			TabPanel panel = (TabPanel) com;
			panel.setTitleFont(font);
		}
	}

	public Font getDefaultFontAt(int index) {
		Component com = pane.getTabComponentAt(index);
		if (com instanceof TabPanel) {
			TabPanel panel = (TabPanel) com;
			return panel.getDefaultFont();
		}
		return null;
	}

	public String getTitleAt(int index) {
		return pane.getTitleAt(index);
	}

	public int getTabCount() {
		return pane.getTabCount();
	}

	public void setSelectedIndex(int index) {
		pane.setSelectedIndex(index);
	}

	public int indexOfComponent(Component component) {
		for (Component comp : pane.getComponents()) {
			if (comp instanceof SparkTab) {
				SparkTab tab = (SparkTab) comp;
				if (tab.getComponent() == component)
					return pane.indexOfComponent(tab);
			}
		}
		return -1;
	}

	public Component getComponentAt(int index) {
		return ((SparkTab) pane.getComponentAt(index)).getComponent();
	}

	public Component getTabComponentAt(int index) {
		return pane.getTabComponentAt(index);
	}

	public Component getTabComponentAt(SparkTab tab) {
		return pane.getTabComponentAt(indexOfComponent(tab));
	}

	public Component getSelectedComponent() {
		if (pane.getSelectedComponent() instanceof SparkTab) {
			SparkTab tab = (SparkTab) pane.getSelectedComponent();
			return tab.getComponent();
		}
		return null;
	}

	public void removeTabAt(int index) {
		pane.remove(index);
	}

	public int getSelectedIndex() {
		return pane.getSelectedIndex();
	}

	public void setCloseButtonEnabled(boolean enable) {
		closeEnabled = enable;
	}

	public void addSparkTabbedPaneListener(SparkTabbedPaneListener listener) {
		listeners.add(listener);
	}

	public void removeSparkTabbedPaneListener(SparkTabbedPaneListener listener) {
		listeners.remove(listener);
	}

	protected void fireTabAdded(SparkTab tab, Component component, int index) {
		final Iterator<SparkTabbedPaneListener> list = ModelUtil.reverseListIterator(listeners.listIterator());
		while (list.hasNext()) {
			(list.next()).tabAdded(tab, component, index);
		}
	}

	public JPanel getMainPanel() {
		return this;
	}

	public void removeComponent(Component comp) {
		int index = indexOfComponent(comp);
		if (index != -1) {
			removeTabAt(index);
		}
	}

	protected void fireTabRemoved(SparkTab tab, Component component, int index) {
		final Iterator<SparkTabbedPaneListener> list = ModelUtil.reverseListIterator(listeners.listIterator());
		while (list.hasNext()) {
			(list.next()).tabRemoved(tab, component, index);
		}
	}

	protected void fireTabSelected(SparkTab tab, Component component, int index) {
		final Iterator<SparkTabbedPaneListener> list = ModelUtil.reverseListIterator(listeners.listIterator());
		while (list.hasNext()) {
			(list.next()).tabSelected(tab, component, index);
		}
	}

	protected void allTabsClosed() {
		final Iterator<SparkTabbedPaneListener> list = ModelUtil.reverseListIterator(listeners.listIterator());
		while (list.hasNext()) {
			list.next().allTabsRemoved();
		}
	}

	public void close(SparkTab sparktab) {
		int closeTabNumber = pane.indexOfComponent(sparktab);
		pane.removeTabAt(closeTabNumber);
		fireTabRemoved(sparktab, sparktab.getComponent(), closeTabNumber);

		if (pane.getTabCount() == 0) {
			allTabsClosed();
		}
	}

	public class TabPanel extends JPanel {
		private static final long serialVersionUID = -8249981130816404360L;
		private final BorderLayout layout = new BorderLayout(0, 0);
		private final Font defaultFontPlain = new Font("Dialog", Font.PLAIN, 11);
		private final Font defaultFontBold = new Font("Dialog", Font.BOLD, 11);
		private JLabel iconLabel;
		private JLabel titleLabel;
		private JLabel tabCloseButton = new JLabel(closeInactiveButtonIcon);

		private SparkTab sparktab;

		public TabPanel(final SparkTab sparktab, String title, Icon icon) {
			setOpaque(false);
			this.setLayout(layout);
			this.sparktab = sparktab;
			titleLabel = new JLabel(title);

			titleLabel.setFont(closeEnabled ? defaultFontBold : defaultFontPlain);
			if (icon != null) {
				iconLabel = new JLabel(icon);
				add(iconLabel, BorderLayout.WEST);
			}

			add(titleLabel, BorderLayout.CENTER);

			if (closeEnabled) {
				JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(pane);
				McWillTabTipListener listener = new McWillTabTipListener(frame, sparktab, this, tabCloseButton);
				this.addPropertyChangeListener(listener);
				tabCloseButton.addPropertyChangeListener(listener);
				tabCloseButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseEntered(MouseEvent e) {
						super.mouseEntered(e);
						tabCloseButton.firePropertyChange("scrollBarMouseEntered", 1, 2);
					}

					@Override
					public void mouseExited(MouseEvent e) {
						super.mouseExited(e);
						tabCloseButton.firePropertyChange("mouseExited", 1, 2);
					}

					public void mousePressed(MouseEvent mouseEvent) {
						final SwingWorker closeTimerThread = new SwingWorker() {
							public Object construct() {
								try {
									Thread.sleep(100);
								} catch (InterruptedException e) {
									Log.error(e);
								}
								return true;
							}

							public void finished() {
								tabCloseButton.firePropertyChange("mouseExited", 1, 2);
								close(sparktab);
							}
						};
						closeTimerThread.start();
					}
				});
				//		add(tabCloseButton, BorderLayout.EAST);
			}
		}

		class McWillTabTipListener implements PropertyChangeListener {
			JFrame frame;
			JLabel tabCloseLabel;
			JComponent component;
			SparkTab sparktab;

			public McWillTabTipListener(JFrame frame, SparkTab sparktab, JComponent component, JLabel tabCloseButton) {
				this.tabCloseLabel = tabCloseButton;
				this.frame = frame;
				this.component = component;
				this.sparktab = sparktab;
				tabCloseButton.setOpaque(false);
				// 将组件放在弹出层中，浮现在其它组件之上  
				JLayeredPane lp = frame.getLayeredPane();
				lp.add(tabCloseButton, new Integer(JLayeredPane.POPUP_LAYER));
				tabCloseButton.setSize(tabCloseButton.getPreferredSize());
				tabCloseButton.setVisible(false);

			}

			@Override
			public void propertyChange(PropertyChangeEvent e) {
				String name = e.getPropertyName();
				if ("scrollBarMouseEntered".equals(name)) {
					if (e.getSource() == tabCloseLabel) {
						tabCloseLabel.setIcon(closeActiveButtonIcon);
						setCursor(HAND_CURSOR);
					} else if (sparktab != pane.getSelectedComponent()) {
						component.putClientProperty(Constants.SKIN_BUTTON, 2);// over not select
					} else {
						component.putClientProperty(Constants.SKIN_BUTTON, 4);// over select
					}
					showTip();
				} else if ("mouseExited".equals(name)) {
					if (sparktab != pane.getSelectedComponent()) {
						component.putClientProperty(Constants.SKIN_BUTTON, 1);// default not select
					} else {
						component.putClientProperty(Constants.SKIN_BUTTON, 3);// default select
					}
					tabCloseLabel.setIcon(closeInactiveButtonIcon);
					setCursor(DEFAULT_CURSOR);
					tabCloseLabel.setVisible(false);
				}
			}

			private void showTip() {
				Point p = component.getLocationOnScreen();
				/** 设置显示的位置 */
				SwingUtilities.convertPointFromScreen(p, frame.getContentPane());
				int maxX = frame.getWidth() - 115;

				int x = p.x - tabCloseLabel.getWidth() + component.getWidth() + 10;

				x = x > maxX ? maxX : x;

				tabCloseLabel.setLocation(new Point(x, p.y - tabCloseLabel.getHeight() + 7));
				tabCloseLabel.setVisible(true);
			}
		}

		@Override
		public Dimension getPreferredSize() {
			Dimension dim = super.getPreferredSize();

			if (closeEnabled && titleLabel.getText() != null && titleLabel.getText().length() < 6 && dim.getWidth() < 80) {
				return new Dimension(80, dim.height);

			} else {
				return dim;
			}

		}

		public Font getDefaultFont() {
			return defaultFontPlain;
		}

		public void setIcon(Icon icon) {
			iconLabel.setIcon(icon);
		}

		public void setTitle(String title) {
			titleLabel.setText(title);
		}

		public void setTitleColor(Color color) {
			titleLabel.setForeground(color);
			//			titleLabel.validate();
			//			titleLabel.repaint();   //解决cpu过高问题
		}

		public void setTitleBold(boolean bold) {
			Font oldFont = titleLabel.getFont();
			Font newFont;
			if (bold) {
				newFont = new Font(oldFont.getFontName(), Font.BOLD, oldFont.getSize());
			} else {
				newFont = new Font(oldFont.getFontName(), Font.PLAIN, oldFont.getSize());
			}

			titleLabel.setFont(newFont);
			titleLabel.validate();
			titleLabel.repaint();
			titleLabel.revalidate();
		}

		public void setTitleFont(Font font) {
			titleLabel.setFont(font);
			titleLabel.validate();
			titleLabel.repaint();
			titleLabel.revalidate();
		}

		public boolean isSelected() {
			return sparktab == pane.getSelectedComponent();
		}

	}

	/**
	 * Drag and Drop
	 */
	public void enableDragAndDrop() {
		final DragSourceListener dsl = new DragSourceListener() {

			@Override
			public void dragDropEnd(DragSourceDropEvent event) {
				dragTabIndex = -1;
			}

			@Override
			public void dragEnter(DragSourceDragEvent event) {
				event.getDragSourceContext().setCursor(DragSource.DefaultMoveDrop);
			}

			@Override
			public void dragExit(DragSourceEvent event) {
			}

			@Override
			public void dragOver(DragSourceDragEvent event) {
			}

			@Override
			public void dropActionChanged(DragSourceDragEvent event) {
			}

		};

		final Transferable t = new Transferable() {
			private final DataFlavor FLAVOR = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType, NAME);

			@Override
			public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
				return pane;
			}

			@Override
			public DataFlavor[] getTransferDataFlavors() {
				DataFlavor[] f = new DataFlavor[1];
				f[0] = this.FLAVOR;
				return f;
			}

			@Override
			public boolean isDataFlavorSupported(DataFlavor flavor) {
				return flavor.getHumanPresentableName().equals(NAME);
			}

		};

		final DragGestureListener dgl = new DragGestureListener() {

			@Override
			public void dragGestureRecognized(DragGestureEvent event) {
				dragTabIndex = pane.indexAtLocation(event.getDragOrigin().x, event.getDragOrigin().y);
				if (dragTabIndex == -1)
					return;
				try {
					event.startDrag(DragSource.DefaultMoveDrop, t, dsl);
				} catch (Exception idoe) {
					Log.error(idoe);
				}
			}

		};

		final DropTargetListener dtl = new DropTargetListener() {

			@Override
			public void dragEnter(DropTargetDragEvent event) {
			}

			@Override
			public void dragExit(DropTargetEvent event) {
			}

			@Override
			public void dragOver(DropTargetDragEvent event) {
			}

			@Override
			public void drop(DropTargetDropEvent event) {
				int dropTabIndex = getTargetTabIndex(event.getLocation());
				moveTab(dragTabIndex, dropTabIndex);
			}

			@Override
			public void dropActionChanged(DropTargetDragEvent event) {
			}

		};

		new DropTarget(pane, DnDConstants.ACTION_COPY_OR_MOVE, dtl, true);
		new DragSource().createDefaultDragGestureRecognizer(pane, DnDConstants.ACTION_COPY_OR_MOVE, dgl);
	}

	private void moveTab(int prev, int next) {
		if (next < 0 || prev == next) {
			return;
		}
		Component cmp = pane.getComponentAt(prev);
		Component tab = pane.getTabComponentAt(prev);
		String str = pane.getTitleAt(prev);
		Icon icon = pane.getIconAt(prev);
		String tip = pane.getToolTipTextAt(prev);
		boolean flg = pane.isEnabledAt(prev);
		int tgtindex = prev > next ? next : next - 1;
		pane.remove(prev);
		pane.insertTab(str, icon, cmp, tip, tgtindex);
		pane.setEnabledAt(tgtindex, flg);

		if (flg)
			pane.setSelectedIndex(tgtindex);

		pane.setTabComponentAt(tgtindex, tab);
	}

	private int getTargetTabIndex(Point point) {
		Point tabPt = SwingUtilities.convertPoint(pane, point, pane);
		boolean isTB = pane.getTabPlacement() == JTabbedPane.TOP || pane.getTabPlacement() == JTabbedPane.BOTTOM;
		for (int i = 0; i < getTabCount(); i++) {
			Rectangle r = pane.getBoundsAt(i);
			if (isTB)
				r.setRect(r.x - r.width / 2, r.y, r.width, r.height);
			else
				r.setRect(r.x, r.y - r.height / 2, r.width, r.height);
			if (r.contains(tabPt))
				return i;
		}
		Rectangle r = pane.getBoundsAt(getTabCount() - 1);
		if (isTB)
			r.setRect(r.x + r.width / 2, r.y, r.width, r.height);
		else
			r.setRect(r.x, r.y + r.height / 2, r.width, r.height);
		return r.contains(tabPt) ? getTabCount() : -1;
	}

	protected JTabbedPane buildTabbedPane(final int type) {
		return new JTabbedPane(type);
	}

	public JTabbedPane getTabbedPane() {
		return pane;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.getRootPane().putClientProperty(Constants.TABBEDPANE_BOUNDS, this.getBounds());
	}
}
