package com.xinwei.talk.ui.main.list.group.bak;
//package com.xinwei.talk.ui.main.list.group.bak;
//
///**
// * The <code>GroupListListener</code> interface is used to listen for model changes within the Group McWillList2.
// * <p/>
// * In general, you implement this interface in order to listen for adding and removal of both GroupItems .
// */
//public interface GroupListPanelListener {
//
//	/**
//	 * Notified when a <code>GroupItem</code> has been added to the GroupList.
//	 *
//	 * @param item
//	 *            the GroupItem added.
//	 */
//	void groupItemAdded(GroupItem item);
//
//	/**
//	 * Notified when a <code>GroupItem</code> has been removed from the GroupList.
//	 *
//	 * @param item
//	 *            the GroupItem removed.
//	 */
//	void groupItemRemoved(GroupItem item);
//	
//	/**
//	 * Notified when a <code>GroupItem</code> has been updated from the GroupList.
//	 *
//	 * @param item
//	 *            the GroupItem updated.
//	 */
//	void groupItemUpdated(GroupItem item);
//
//	
//	void groupItemClicked(GroupItem item);
//
//	/**
//	 * Called when a <code>GroupItem</code> has been double clicked in the Group McWillList2.
//	 *
//	 * @param item
//	 *            the <code>GroupItem</code> double clicked.
//	 */
//	void groupItemDoubleClicked(GroupItem item);
//}
