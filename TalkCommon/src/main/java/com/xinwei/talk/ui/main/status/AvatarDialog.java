package com.xinwei.talk.ui.main.status;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lookandfeel.component.McWillImageDialog;
import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
import com.xinwei.common.lookandfeel.layout.TileAutoLayout;
import com.xinwei.common.lookandfeel.ui.tabbedpane.PPTTabbedPaneUI;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.spark.SwingWorker;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.ui.skin.McWillSkinContentPanel;

public class AvatarDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private static final Dimension PREFERRED_SIZE = new Dimension(45, 45);
	private JPanel panel;
	private JButton imageButton;
	private JButton sendButton;
	private JButton closeButton;

	private JLabel label60;
	private JLabel label100;
	private JLabel usedIconLabel;

	private byte[] icon;
	private byte[] selectIcon;

	JTabbedPane tabbedPane;

	private CustomAvatarPanel avatarPanel;

	public AvatarDialog(final Window dialog, byte[] icon) {
		//"更改头像"
		super(dialog, Res.getMessage("avatar.change"));

		this.icon = icon;

		Map<String, List<byte[]>> iconsMap = getIconMap();

		this.setModal(true);

		McWillSkinContentPanel skinContentPanel = new McWillSkinContentPanel();

		skinContentPanel.add(createHeadPanel(iconsMap), BorderLayout.CENTER);
		skinContentPanel.add(createButtonPanel(), BorderLayout.SOUTH);
		skinContentPanel.add(createPreviewPanel(), BorderLayout.EAST);
		setContentPane(skinContentPanel);

		setIcon(icon, true);

		setSize(600, 450);
		setMaximumSize(new Dimension(1000, 800));
		setMinimumSize(new Dimension(400, 320));

		setResizable(false);
		setLocationRelativeTo(dialog);
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				super.componentResized(e);
				panel.updateUI();
			}
		});

	}

	private JPanel createPreviewPanel() {
		JPanel previewPanel = new JPanel(new GridBagLayout());
		previewPanel.setOpaque(false);
		previewPanel.add(new JLabel(Res.getMessage("avatar.preview")), new GridBagConstraints(0, 0, 0, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		//60*60
		JPanel panel60 = new JPanel(new GridBagLayout());

		label60 = new McWillRolloverLabel();
		panel60.add(label60, new GridBagConstraints(0, 1, 0, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		panel60.add(new JLabel("60 × 60"), new GridBagConstraints(0, 2, 0, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

		//100*100
		JPanel panel100 = new JPanel(new GridBagLayout());

		label100 = new McWillRolloverLabel();
		panel100.add(label100, new GridBagConstraints(0, 1, 0, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		panel100.add(new JLabel("100 × 100"), new GridBagConstraints(0, 2, 0, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

		JPanel usedPanel = new JPanel(new BorderLayout(0, 5));

		JLabel usedLabel = new JLabel(Res.getMessage("avatar.current"));
		usedIconLabel = new McWillRolloverLabel();
		usedIconLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				selectIcon = null;
				setIcon(icon);
			}
		});

		usedPanel.add(usedLabel, BorderLayout.NORTH);
		usedPanel.add(usedIconLabel, BorderLayout.WEST);

		previewPanel.add(panel60, new GridBagConstraints(0, 1, 0, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

		previewPanel.add(panel100, new GridBagConstraints(0, 2, 0, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

		previewPanel.add(usedPanel, new GridBagConstraints(0, 3, 0, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 15, 5, 5), 0, 0));

		previewPanel.add(new JLabel(), new GridBagConstraints(0, 4, 0, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

		return previewPanel;
	}

	private JPanel createButtonPanel() {
		JPanel bottomButtonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		bottomButtonPanel.setOpaque(false);

		sendButton = new JButton();
		closeButton = new JButton();
		imageButton = new JButton();

		String stext = Res.getMessage("button.ok");
		sendButton.setText(stext);
		String ctext = Res.getMessage("button.close");
		closeButton.setText(ctext);
		imageButton.setText(Res.getMessage("avatar.button.custom.avatar"));

		imageButton.setPreferredSize(new Dimension(100, 24));
		sendButton.setPreferredSize(new Dimension(60, 24));
		closeButton.setPreferredSize(new Dimension(60, 24));

		imageButton.addActionListener(this);
		sendButton.addActionListener(this);
		closeButton.addActionListener(this);

		bottomButtonPanel.add(imageButton);
		bottomButtonPanel.add(sendButton);
		bottomButtonPanel.add(closeButton);

		return bottomButtonPanel;
	}

	public Map<String, List<byte[]>> getIconMap() {
		List<byte[]> defaultImages = new ArrayList<byte[]>();
		defaultImages.add(LAF.getDefaultAvatarImageBytes());
		for (int i = 0; i < 74; i++) {
			defaultImages.add(LAF.getImageBytes("talk/images/avatar/system/" + (i + 1) + "_100.gif"));
		}
		List<byte[]> gameImages = new ArrayList<byte[]>();
		for (int i = 0; i < 8; i++) {
			gameImages.add(LAF.getImageBytes(("talk/images/avatar/game/" + (i + 1) + "_100.gif")));
		}

		Map<String, List<byte[]>> iconMap = new LinkedHashMap<String, List<byte[]>>();
		iconMap.put(Res.getMessage("avatar.recommend", defaultImages.size()), defaultImages);
		iconMap.put(Res.getMessage("avatar.game", gameImages.size()), gameImages);

		return iconMap;
	}

	public JComponent createHeadPanel(Map<String, List<byte[]>> iconsMap) {
		final PPTTabbedPaneUI pptTabbedPaneUI = new PPTTabbedPaneUI();
		tabbedPane = new JTabbedPane() {
			public void updateUI() {
				setUI(pptTabbedPaneUI);
			}
		};
		avatarPanel = new CustomAvatarPanel() {
			protected void setSubimage(byte[] subimage) {
				if (subimage == null)
					return;
				selectIcon = subimage;
				setIcon(subimage);
			}
		};

		panel = new JPanel();
		panel.setOpaque(false);
		panel.setLayout(new GridBagLayout());

		Set<Entry<String, List<byte[]>>> entrySet = iconsMap.entrySet();
		int i = 0;
		for (Entry<String, List<byte[]>> entry : entrySet) {
			JPanel entryPanel = new JPanel(new GridBagLayout());
			entryPanel.setOpaque(false);
			String key = entry.getKey();
			List<byte[]> icons = entry.getValue();
			JLabel titleLabel = new JLabel(key);

			TileAutoLayout tileLayout = new TileAutoLayout(this, 130, 5, 5);
			JPanel iconPanel = new JPanel(tileLayout);
			iconPanel.setOpaque(false);
			for (final byte[] bytes : icons) {
				JLabel item = new McWillRolloverLabel(new ImageIcon(ImageUtil.resize(bytes, 45, 45)));
				item.setPreferredSize(PREFERRED_SIZE);
				item.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						selectIcon = bytes;
						setIcon(bytes);
					}
				});
				iconPanel.add(item);
			}

			entryPanel.add(titleLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
			entryPanel.add(iconPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

			panel.add(entryPanel, new GridBagConstraints(0, i++, 0, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		}
		panel.add(new JLabel(), new GridBagConstraints(0, i++, 0, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		JScrollPane jScrollPane = new JScrollPane(panel);
		jScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPane.setBorder(null);

		tabbedPane.addTab(Res.getMessage("avatar.classics"), jScrollPane);

		tabbedPane.addTab(Res.getMessage("avatar.personality"), avatarPanel);

		return tabbedPane;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == sendButton) {
			if (selectIcon != null) {
				changeAvatar(selectIcon);
			}
			this.dispose();
		} else if (e.getSource() == closeButton) {
			this.dispose();
		} else if (e.getSource() == imageButton) {
			tabbedPane.setSelectedIndex(1);
			JFileChooser choose = new McWillImageDialog(this);

			setCustomAvatar(this, choose);
		}
	}

	private void setCustomAvatar(final Window window, JFileChooser choose) {
		if (choose.showOpenDialog(window) == JFileChooser.APPROVE_OPTION) {
			File file = choose.getSelectedFile();
			if (file != null && file.exists()) {
				String imageType = ImageUtil.getImageType(file);
				if (imageType != null) {
					avatarPanel.setImage(IOUtil.readBytes(file));
					avatarPanel.repaint();
					return;
				} else {
					JOptionPane.showMessageDialog(window, "Please choose a valid image file.", "", JOptionPane.WARNING_MESSAGE);
				}
			}
		}
	}

	public void setIcon(byte[] bytes) {
		setIcon(bytes, false);
	}

	public void setIcon(final byte[] bytes, final boolean flag) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				label60.setIcon(new ImageIcon(ImageUtil.resize(bytes, 60, 60)));
				label100.setIcon(new ImageIcon(ImageUtil.resize(bytes, 100, 100)));
				if (flag) {
					usedIconLabel.setIcon(new ImageIcon(ImageUtil.resize(bytes, 45, 45)));
				}
			}
		});
	}

	private void changeAvatar(final byte[] icon) {
		SwingWorker worker = new SwingWorker() {
			public Object construct() {
				return icon;
			}

			public void finished() {
				if (icon == null)
					return;
				try {
					String imageFormat = ImageUtil.getImageFormat(icon);

					VCard vCard = TalkManager.getVCard();
					String tel = vCard.getTel();
					String fileName = tel + "_avatar_" + UUID.randomUUID() + "." + imageFormat;

					TalkManager.getFileService().uploadAvatarFile(fileName, icon, vCard);

					vCard.setAvatar(icon);

					ImageManager.setAvatar(vCard.getJid(), icon);

					TalkManager.saveVCard();

					ListenerManager.fireVcardChanged();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		worker.start();
	}
}
