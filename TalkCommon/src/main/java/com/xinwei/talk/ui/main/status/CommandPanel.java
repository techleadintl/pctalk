/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 下午4:59:20
 * 
 ***************************************************************/
package com.xinwei.talk.ui.main.status;

import java.awt.FlowLayout;

import javax.swing.JPanel;

import com.xinwei.common.lang.SwingUtil;

/**
 *
 */
public class CommandPanel extends JPanel {

	private static final long serialVersionUID = -720715661649067658L;
	//private final Image backgroundImage;

	public CommandPanel() {
		this(true);
	}

	public CommandPanel(boolean doLayout) {
		if (doLayout) {
			if (SwingUtil.isWindows()) {
				setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
			} else {
				setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
			}

			setOpaque(false);

			// backgroundImage = Default.getImageIcon(Default.TOP_BOTTOM_BACKGROUND_IMAGE).getImage();
		}
		//   setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, new Color(197, 213, 230)));
	}

	//    public void paintComponent(Graphics g) {
	//        double scaleX = getWidth() / (double) backgroundImage.getWidth(null);
	//        double scaleY = getHeight() / (double) backgroundImage.getHeight(null);
	//        AffineTransform xform = AffineTransform.getScaleInstance(scaleX, scaleY);
	//        ((Graphics2D) g).drawImage(backgroundImage, xform, this);
	//    }
}
