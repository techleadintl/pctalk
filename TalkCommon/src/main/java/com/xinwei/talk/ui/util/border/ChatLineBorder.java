package com.xinwei.talk.ui.util.border;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.AbstractButton;
import javax.swing.border.LineBorder;

import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.ColorHelper;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillRolloverButton;

public class ChatLineBorder extends LineBorder {
	public ChatLineBorder() {
		super(null);
	}

	public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
		AbstractButton button = (AbstractButton) c;
		boolean isOverButton = false;
		if (button instanceof McWillRolloverButton) {
			isOverButton = true;
		}
		Color frameColor = AbstractLookAndFeel.getTheme().getFrameColor();
		if (isOverButton && button.getModel().isRollover()) {
			frameColor = McWillTheme.DEFAULT_COLOR;
		} else {
			frameColor = ColorHelper.brighter(frameColor, 40);
		}
		g.setColor(frameColor);
		g.drawLine(x, 0, width, 0);
		g.drawLine(0, height - 2, width, height - 2);
		g.setColor(Color.LIGHT_GRAY);
		g.drawLine(2, 4, 2, height - 6);
	}
}