package com.xinwei.talk.ui.main.list.group.bak;
//package com.xinwei.talk.ui.main.list.group.bak;
//
//import java.awt.Color;
//import java.awt.GridLayout;
//import java.awt.event.KeyEvent;
//import java.awt.event.MouseEvent;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.Iterator;
//import java.util.List;
//
//import javax.swing.JPanel;
//import javax.swing.JPopupMenu;
//import javax.swing.SwingUtilities;
//import javax.swing.UIManager;
//
//import org.jivesoftware.smackx.muc.MultiUserChat;
//
//import com.xinwei.talk.action.XAction;
//import com.xinwei.talk.action.group.GroupDissolveAction;
//import com.xinwei.talk.action.group.GroupOpenRoomAction;
//import com.xinwei.talk.action.group.GroupQuitAction;
//import com.xinwei.talk.common.TalkUtil;
//import com.xinwei.talk.manager.TalkManager;
//import com.xinwei.talk.manager.LocalManager;
//import com.xinwei.talk.model.Group;
//import com.xinwei.talk.model.Member;
//import com.xinwei.talk.model.listener.GroupListener;
//import com.xinwei.talk.model.listener.MemberListener;
//import com.xinwei.talk.plugin.ListItem2.ListItemKeyListener;
//import com.xinwei.talk.plugin.ListItem2.ListItemMouseListener;
//import com.xinwei.talk.ui.util.component.list.XWList;
//import com.xinwei.common.lang.StringUtil;
//import com.xinwei.common.lang.SwingUtil;
//import com.xinwei.common.lookandfeel.component.McWillScrollPane;
//
//public class GroupMemberListPanel extends JPanel implements GroupListener, MemberListener {
//	private static final long serialVersionUID = -8486385306563904232L;
//
//	private LocalManager localManager = TalkManager.getLocalManager();
//
//	private final McWillList2<GroupListPanelListener> groupListPanelListeners = new ArrayList<GroupListPanelListener>();
//
//	private McWillList2<GroupItem> groupItemList;
//
//	private final McWillList2<Group> groupList = new ArrayList<Group>();
//
//	//	private static Map<String, MultiUserChat> chatMap = new HashMap<String, MultiUserChat>();
//
//	private final Comparator<GroupItem> comparator = new Comparator<GroupItem>() {
//		@Override
//		public int compare(GroupItem o1, GroupItem o2) {
//			String name1 = o1.getName();
//			String name2 = o2.getName();
//			if (name1 == null && name2 == null)
//				return 0;
//			if (name1 == null)
//				return 1;
//			if (name2 == null)
//				return -1;
//			return name1.compareTo(name2);
//		}
//	};
//
//	public GroupMemberListPanel() {
//		this.setBackground((Color) UIManager.get("ContactItem.background"));
//
//		this.setLayout(new GridLayout(1, 1));
//
//		groupItemList = new McWillList2<GroupItem>();
//
//		SwingUtil.setOpaqueFalse(groupItemList);
//
//		McWillScrollPane listScrollPane = new McWillScrollPane(groupItemList, 200);
//		//		listScrollPane = new JScrollPane(list);
//		//		listScrollPane.setAutoscrolls(true);
//		//
//		//		listScrollPane.setBorder(BorderFactory.createEmptyBorder());
//		//		listScrollPane.getVerticalScrollBar().setBlockIncrement(200);
//		//		listScrollPane.getVerticalScrollBar().setUnitIncrement(20);
//
//		add(listScrollPane);
//	}
//
//	public GroupItem getGroupItemByJID(String groupJid) {
//		String groupId = TalkUtil.getGroupId(groupJid);
//		return getGroupItemByID(groupId);
//	}
//
//	public GroupItem getGroupItemByID(String groupId) {
//		//		Enumeration<GroupItem> elements = listModel.elements();
//		//
//		//		while (elements.hasMoreElements()) {
//		//			GroupItem element = elements.nextElement();
//		//			Group group = element.getObject();
//		//			String elementId = group.getId();
//		//			if (StringUtil.equals(groupId, elementId)) {
//		//				return element;
//		//			}
//		//		}
//		return null;
//	}
//
//
//	
//	public void buildList() {
//		groupItemList.removeAll();
//
//		groupList.clear();
//
//		localManager.addGroupListener(this);
//
//		localManager.addMemberListener(this);
//
//		TalkManager.getVCardManager().initGroupList();
//	}
//
//	//	public static void joinConferenceOnSeperateThread(final String roomName, String roomJID) {
//	//		ChatManager chatManager = SparkManager.getChatManager();
//	//		LocalPreferences pref = SettingsManager.getLocalPreferences();
//	//
//	//		//		final MultiUserChat groupChat = new MultiUserChat(SparkManager.getConnection(), roomJID);
//	//
//	//		MultiUserChat groupChat = chatMap.get(roomJID);
//	//		final String nickname = pref.getNickname().trim();
//	//
//	//		//		final String nickname = XWManager.getVCard().getTelNo();
//	//
//	//		// Check if room already exists. If so, join room again.
//	//		try {
//	//			GroupChatRoom chatRoom = (GroupChatRoom) chatManager.getChatContainer().getChatRoom(roomJID);
//	//			MultiUserChat muc = chatRoom.getMultiUserChat();
//	//			if (!muc.isJoined()) {
//	//				ConferenceUtils.joinRoom(muc, nickname, null);
//	//			}
//	//
//	//			chatManager.getChatContainer().activateChatRoom(chatRoom);
//	//			return;
//	//		} catch (ChatRoomNotFoundException e) {
//	//			// Nothing to do
//	//		}
//	//
//	//		final GroupChatRoom room = UIComponentRegistry.createGroupChatRoom(groupChat);
//	//		room.setTabTitle(roomName);
//	//
//	//		final McWillList2<String> errors = new ArrayList<String>();
//	//
//	//		final SwingWorker startChat = new SwingWorker() {
//	//			public Object construct() {
//	//				//				if (!groupChat.isJoined()) {
//	//				//					int groupChatCounter = 0;
//	//				//					while (true) {
//	//				//						groupChatCounter++;
//	//				//						String joinName = nickname;
//	//				//						if (groupChatCounter > 1) {
//	//				//							joinName = joinName + groupChatCounter;
//	//				//						}
//	//				//						if (groupChatCounter < 10) {
//	//				//							try {
//	//				//								groupChat.join(joinName);
//	//				//								break;
//	//				//							} catch (XMPPException ex) {
//	//				//								int code = 0;
//	//				//								if (ex.getXMPPError() != null) {
//	//				//									code = ex.getXMPPError().getCode();
//	//				//								}
//	//				//
//	//				//								if (code == 0) {
//	//				//									errors.add("No response from server.");
//	//				//								} else if (code == 401) {
//	//				//									errors.add("The password did not match the rooms password.");
//	//				//								} else if (code == 403) {
//	//				//									errors.add("You have been banned from this room.");
//	//				//								} else if (code == 404) {
//	//				//									errors.add("The room you are trying to enter does not exist.");
//	//				//								} else if (code == 407) {
//	//				//									errors.add("You are not a member of this room.\nThis room requires you to be a member to join.");
//	//				//								} else if (code != 409) {
//	//				//									break;
//	//				//								}
//	//				//							}
//	//				//						} else {
//	//				//							break;
//	//				//						}
//	//				//					}
//	//				//				}
//	//
//	//				return "ok";
//	//			}
//	//
//	//			public void finished() {
//	//				UIManager.put("OptionPane.okButtonText", Res.getString("ok"));
//	//				//				if (errors.size() > 0) {
//	//				//					String error = errors.get(0);
//	//				//					JOptionPane.showMessageDialog(SparkManager.getMainWindow(), error, "Unable to join the room at this time.", JOptionPane.ERROR_MESSAGE);
//	//				//				} else if (groupChat.isJoined()) {
//	//				//					ChatManager chatManager = SparkManager.getChatManager();
//	//				//					chatManager.getChatContainer().addChatRoom(room);
//	//				//					chatManager.getChatContainer().activateChatRoom(room);
//	//				//				} else {
//	//				//					JOptionPane.showMessageDialog(SparkManager.getMainWindow(), "Unable to join the room.", "Error", JOptionPane.ERROR_MESSAGE);
//	//				//				}
//	//				ChatManager chatManager = SparkManager.getChatManager();
//	//				chatManager.getChatContainer().addChatRoom(room);
//	//				chatManager.getChatContainer().activateChatRoom(room);
//	//			}
//	//		};
//	//
//	//		startChat.start();
//	//	}
//
//	@Override
//	public void add(final McWillList2<String> gids) {
//		SwingUtilities.invokeLater(new Runnable() {
//			public void run() {
//				for (String gid : gids) {
//					Group group = localManager.getGroup(gid);
//					if (group == null) {
//						continue;
//					}
//
//					McWillList2<GroupItem> copyInto = groupItemList.copyInto();
//					Iterator<GroupItem> iterator = copyInto.iterator();
//					while (iterator.hasNext()) {
//						GroupItem item = iterator.next();
//						if (StringUtil.equals(item.getObject().getId(), gid)) {
//							iterator.remove();
//							groupItemList.remove(item);
//						}
//					}
//
//					final GroupItem groupItem = new GroupItem(group);
//					copyInto.add(groupItem);
//					Collections.sort(copyInto, comparator);
//					int index = copyInto.indexOf(groupItem);
//
//					groupItemList.add(groupItem, index);
//
//					updateListUI();
//
//					String roomJID = group.getJid();
//					final String telNo = TalkManager.getVCard().getTel();
//					final MultiUserChat groupChat = new MultiUserChat(TalkManager.getConnection(), roomJID);
//
//					localManager.putMultiUserChat(group.getId(), groupChat);
//
//					new Thread(new Runnable() {
//						@Override
//						public void run() {
//							try {
//								//								ConferenceUtils.joinRoom(groupChat, telNo, null);
//							} catch (Exception e) {
//								e.printStackTrace();
//							}
//						}
//					}).start();
//
//					fireGroupItemAdded(groupItem);
//				}
//			}
//		});
//
//	}
//
//	@Override
//	public void update(final McWillList2<String> gids) {
//		SwingUtilities.invokeLater(new Runnable() {
//			public void run() {
//				McWillList2<GroupItem> copyInto = groupItemList.copyInto();
//				for (String gid : gids) {
//					Group group = localManager.getGroup(gid);
//					if (group == null) {
//						continue;
//					}
//					for (int i = 0; i < copyInto.size(); i++) {
//						GroupItem groupItem = copyInto.get(i);
//						if (StringUtil.equals(groupItem.getObject().getId(), gid)) {
//							groupItem.setUIInfo(group);
//
//							fireGroupItemUpdated(groupItem);
//						}
//					}
//				}
//				updateListUI();
//			}
//		});
//
//	}
//
//	@Override
//	public void delete(final McWillList2<String> gids) {
//		SwingUtilities.invokeLater(new Runnable() {
//			public void run() {
//				for (String gid : gids) {
//					Group group = localManager.getGroup(gid);
//					if (group == null) {
//						continue;
//					}
//					McWillList2<GroupItem> copyInto = groupItemList.copyInto();
//					for (int i = 0; i < copyInto.size(); i++) {
//						GroupItem groupItem = copyInto.get(i);
//						if (StringUtil.equals(groupItem.getObject().getId(), gid)) {
//							fireGroupItemRemoved(groupItem);
//							groupItemList.remove(groupItem);
//							localManager.removeMultiUserChat(gid);
//						}
//					}
//				}
//				updateListUI();
//			}
//
//		});
//
//	}
//
//	private void updateListUI() {
//		groupItemList.invalidate();
//		groupItemList.repaint();
//	}
//	/*
//	 * Adding GroupListPanelListener support.
//	 */
//
//	public void addGroupListPanelListener(GroupListPanelListener listener) {
//		groupListPanelListeners.add(listener);
//	}
//
//	public void removeGroupListPanelListener(GroupListPanelListener listener) {
//		groupListPanelListeners.remove(listener);
//	}
//
//	public void fireGroupItemAdded(GroupItem item) {
//		for (GroupListPanelListener groupListPanelListener : new ArrayList<GroupListPanelListener>(groupListPanelListeners)) {
//			groupListPanelListener.groupItemAdded(item);
//		}
//	}
//
//	public void fireGroupItemUpdated(GroupItem item) {
//		for (GroupListPanelListener groupListPanelListener : new ArrayList<GroupListPanelListener>(groupListPanelListeners)) {
//			groupListPanelListener.groupItemUpdated(item);
//		}
//	}
//
//	public void fireGroupItemRemoved(GroupItem item) {
//		for (GroupListPanelListener groupListPanelListener : new ArrayList<GroupListPanelListener>(groupListPanelListeners)) {
//			groupListPanelListener.groupItemRemoved(item);
//		}
//	}
//
//	public void fireGroupItemClicked(GroupItem groupItem) {
//		for (GroupListPanelListener groupListPanelListener : new ArrayList<GroupListPanelListener>(groupListPanelListeners)) {
//			groupListPanelListener.groupItemClicked(groupItem);
//		}
//	}
//
//	public void fireGroupItemDoubleClicked(GroupItem groupItem) {
//		for (GroupListPanelListener groupListPanelListener : new ArrayList<GroupListPanelListener>(groupListPanelListeners)) {
//			groupListPanelListener.groupItemDoubleClicked(groupItem);
//		}
//	}
//
//	@Override
//	public void add(final String gid, final McWillList2<Member> members) {
//		SwingUtilities.invokeLater(new Runnable() {
//			@Override
//			public void run() {
//				GroupItem groupItem = getGroupItemByID(gid);
//				if (groupItem == null) {
//					return;
//				}
//				groupItem.updateAvatar();
//				revalidate();
//			}
//		});
//	}
//
//	@Override
//	public void update(final String gid, final McWillList2<Member> members) {
//		SwingUtilities.invokeLater(new Runnable() {
//			@Override
//			public void run() {
//				GroupItem groupItem = getGroupItemByID(gid);
//				if (groupItem == null) {
//					return;
//				}
//				groupItem.updateAvatar();
//				revalidate();
//			}
//		});
//
//	}
//
//	@Override
//	public void delete(final String gid, final McWillList2<Member> members) {
//		SwingUtilities.invokeLater(new Runnable() {
//			@Override
//			public void run() {
//				GroupItem groupItem = getGroupItemByID(gid);
//				if (groupItem == null) {
//					return;
//				}
//				groupItem.updateAvatar();
//				revalidate();
//			}
//		});
//	}
//}