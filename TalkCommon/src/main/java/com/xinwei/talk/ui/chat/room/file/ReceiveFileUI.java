/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月6日 下午5:19:43
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room.file;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JLabel;
import javax.swing.JProgressBar;

import com.xinwei.common.lang.ByteFormat;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.http.service.file.InFileTransfer;
import com.xinwei.http.service.file.TransferStatus;
import com.xinwei.talk.common.Downloads;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.manager.TransferManager;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.ui.chat.room.ChatRoomWorking;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.util.component.label.LinkLabel;

public class ReceiveFileUI extends FileUI {
	int FILE_NAME_MAX_LENGTH = 200;
	private static final long serialVersionUID = -2974192409566650923L;
	private FileDragLabel imageLabel = new FileDragLabel();
	private JLabel fileLabel = new JLabel();

	private LinkLabel acceptButton = new LinkLabel();
	private LinkLabel saveAsButton = new LinkLabel();
	private LinkLabel cancelButton = new LinkLabel();
	private JProgressBar progressBar = new JProgressBar();
	private InFileTransfer transfer;
	private JLabel progressLabel = new JLabel();
	private long bytesRead;
	private long _starttime;
	private long _endtime;

	private TalkFileSpecific fileMsg;
	private CommonChatRoom room;
	private TalkMessage talkMessage;


	private File downloadedFile;
	
	protected ChatRoomWorking working;

	public void init(TalkMessage talkMessage, CommonChatRoom room, final TalkFileSpecific fileMsg) {
		this.fileMsg = fileMsg;
		this.room = room;
		this.talkMessage = talkMessage;
		//		setLayout(new BorderLayout());
		setLayout(new GridBagLayout());

		imageLabel = new FileDragLabel();

		acceptButton.setText(Res.getMessage("button.accept"));

		saveAsButton.setText(Res.getMessage("button.save.as"));

		cancelButton.setText(Res.getMessage("button.cancel"));

		progressBar.setPreferredSize(new Dimension(250, 12));
		progressBar.setMaximum(100);
		progressBar.setStringPainted(true);

		add(imageLabel, new GridBagConstraints(0, 0, 1, 5, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		add(fileLabel, new GridBagConstraints(1, 0, 5, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		add(progressBar, new GridBagConstraints(1, 2, 5, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(3, 5, 0, 5), 0, 0));
		add(progressLabel, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		add(acceptButton, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		add(saveAsButton, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
		add(cancelButton, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));

		String fileName = fileMsg.getFilename();
		String text = SwingUtil.format(fileMsg.getLength());
		fileName = SwingUtil.getString(fileName, imageLabel.getFont(), FILE_NAME_MAX_LENGTH, " (" + text + ")");
		fileLabel.setText(fileName);

		imageLabel.setIcon(SwingUtil.getFileImageIcon(fileName));

		addListeners();

	}

	private void addListeners() {
		acceptButton.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				try {
					Downloads.checkDownloadDirectory();
					acceptRequest();
				} catch (Exception ex) {
					setBackground(new Color(239, 245, 250));
					acceptButton.setVisible(false);
					cancelButton.setVisible(false);
					saveAsButton.setVisible(false);
					invalidate();
					validate();
					repaint();
				}
			}
		});

		cancelButton.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (transfer == null) {
					updateOnFinished();
				} else {
					cancelTransfer();
				}
			}
		});
		
		saveAsButton.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				 FileDialog fileDialog = new FileDialog(new Frame(), "Save", FileDialog.SAVE);
				 
				 	if( fileMsg.getFilename() != null){
				 		fileDialog.setFile( fileMsg.getFilename());
				 	}
				 	else{
			        fileDialog.setFile("");
				 	}
			        fileDialog.setVisible(true);
			        
			        if(fileDialog.getDirectory() != null && fileDialog.getFile() != null){
			        saveFile(new File(fileDialog.getDirectory()+fileDialog.getFile()));
			        }
			}
		});

	}

	public void saveFile(final File file){
		
		setBackground(new Color(239, 245, 250));
		acceptButton.setVisible(false);
		saveAsButton.setVisible(false);
		
		final long fileSize = fileMsg.getLength();
		transfer = new InFileTransfer(fileMsg.getUrl(), fileMsg.getFilename(), fileSize);
		
		downloadedFile = file;
		
		working = new ChatRoomWorking(room, Res.getMessage("chatroom.file.working")) {
			@Override
			public void close() {
				cancelTransfer();
			}
		};
		room.addChatRoomWorking(working);
		
		try {
			_starttime = System.currentTimeMillis();
			transfer.recieveFile(downloadedFile);
		} catch (Exception e) {
			Log.error(e);
			room.removeChatRoomWorking(working);
		}

		progressBar.setMaximum(100); // setting it to percent
		progressBar.setStringPainted(true);

		final Timer timer = new Timer();
		TimerTask updateProgessBar = new TimerTask() {
			@Override
			public void run() {
				if (transfer.getAmountWritten() >= fileSize || transfer.getStatus() == TransferStatus.error || transfer.getStatus() == TransferStatus.cancelled
						|| transfer.getStatus() == TransferStatus.complete) {
					this.cancel();
					timer.cancel();
					_endtime = System.currentTimeMillis();
					updateOnFinished();
				} else {
					// 100 % = Filesize
					// x %   = Currentsize	    
					long p = (transfer.getAmountWritten() * 100 / transfer.getFileSize());
					progressBar.setValue(Math.round(p));
				}

			}
		};

		final Timer timer2 = new Timer();
		TimerTask updatePrograssBarText = new TimerTask() {
			long timenow;
			long timeearlier;
			long bytesnow;
			long bytesearlier;

			@Override
			public void run() {
				if (transfer.getAmountWritten() >= fileSize || transfer.getStatus() == TransferStatus.error || transfer.getStatus() == TransferStatus.cancelled
						|| transfer.getStatus() == TransferStatus.complete) {
					this.cancel();
					timer2.cancel();
				} else {

					timenow = System.currentTimeMillis();
					bytesnow = transfer.getAmountWritten();
					bytesRead = transfer.getAmountWritten();
					if (bytesRead == -1) {
						bytesRead = 0;
					}
					ByteFormat format = new ByteFormat();
					String text = format.format(bytesRead);

					TransferStatus status = transfer.getStatus();
					if (status == TransferStatus.in_progress) {
						String speed = SwingUtil.calculateSpeed(bytesnow - bytesearlier, timenow - timeearlier);
						String est = SwingUtil.calculateEstimate(bytesnow, transfer.getFileSize(), _starttime, System.currentTimeMillis());
						progressLabel.setText(Res.getMessage("label.chatroom.fileupload.progress", speed, est));
					}
					bytesearlier = bytesnow;
					timeearlier = timenow;
				}
			}
		};

		timer.scheduleAtFixedRate(updateProgessBar, 10, 10);
		timer2.scheduleAtFixedRate(updatePrograssBarText, 10, 500);

	}
	
	
	public void acceptRequest() {
		setBackground(new Color(239, 245, 250));
		acceptButton.setVisible(false);
		saveAsButton.setVisible(false);

		//定义变量
		final long fileSize = fileMsg.getLength();
		transfer = new InFileTransfer(fileMsg.getUrl(), fileMsg.getFilename(), fileSize);

		downloadedFile = new File(Downloads.getDownloadDirectory(), fileMsg.getFilename());

		working = new ChatRoomWorking(room, Res.getMessage("chatroom.file.working")) {
			@Override
			public void close() {
				cancelTransfer();
			}
		};
		room.addChatRoomWorking(working);
		
		try {
			_starttime = System.currentTimeMillis();
			transfer.recieveFile(downloadedFile);
		} catch (Exception e) {
			Log.error(e);
			room.removeChatRoomWorking(working);
		}

		progressBar.setMaximum(100); // setting it to percent
		progressBar.setStringPainted(true);

		final Timer timer = new Timer();
		TimerTask updateProgessBar = new TimerTask() {
			@Override
			public void run() {
				if (transfer.getAmountWritten() >= fileSize || transfer.getStatus() == TransferStatus.error || transfer.getStatus() == TransferStatus.cancelled
						|| transfer.getStatus() == TransferStatus.complete) {
					this.cancel();
					timer.cancel();
					_endtime = System.currentTimeMillis();
					updateOnFinished();
				} else {
					// 100 % = Filesize
					// x %   = Currentsize	    
					long p = (transfer.getAmountWritten() * 100 / transfer.getFileSize());
					progressBar.setValue(Math.round(p));
				}

			}
		};

		final Timer timer2 = new Timer();
		TimerTask updatePrograssBarText = new TimerTask() {
			long timenow;
			long timeearlier;
			long bytesnow;
			long bytesearlier;

			@Override
			public void run() {
				if (transfer.getAmountWritten() >= fileSize || transfer.getStatus() == TransferStatus.error || transfer.getStatus() == TransferStatus.cancelled
						|| transfer.getStatus() == TransferStatus.complete) {
					this.cancel();
					timer2.cancel();
				} else {

					timenow = System.currentTimeMillis();
					bytesnow = transfer.getAmountWritten();
					bytesRead = transfer.getAmountWritten();
					if (bytesRead == -1) {
						bytesRead = 0;
					}
					ByteFormat format = new ByteFormat();
					String text = format.format(bytesRead);

					TransferStatus status = transfer.getStatus();
					if (status == TransferStatus.in_progress) {
						String speed = SwingUtil.calculateSpeed(bytesnow - bytesearlier, timenow - timeearlier);
						String est = SwingUtil.calculateEstimate(bytesnow, transfer.getFileSize(), _starttime, System.currentTimeMillis());
						progressLabel.setText(Res.getMessage("label.chatroom.fileupload.progress", speed, est));
					}
					bytesearlier = bytesnow;
					timeearlier = timenow;
				}
			}
		};

		timer.scheduleAtFixedRate(updateProgessBar, 10, 10);
		timer2.scheduleAtFixedRate(updatePrograssBarText, 10, 500);

	}

	private void updateOnFinished() {
		TransferStatus status = TransferStatus.cancelled;
		if (transfer != null) {
			status = transfer.getStatus();
			if (status == TransferStatus.error) {
				if (transfer.getException() != null) {
					Log.error("Error occured during file transfer.", transfer.getException());
				}
			}
		}

		room.receiveFile(talkMessage, fileMsg, downloadedFile, talkMessage.getTalkBody().getTime(), status == TransferStatus.complete);
		room.removeChatRoomWorking(working);
		
		TransferManager.getInstance().removeFileComponent(room, this);
		TalkManager.getChatManager().getChatContainer().activateChatRoom(room);
	}

	public void cancelTransfer() {
		if (transfer != null && !transfer.isDone()) {
			transfer.cancel();
		}
	}

}
