/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年2月28日 下午1:25:25
 * 
 ***************************************************************/
package com.xinwei.talk.ui.setting.detail;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.component.McWillCheckLabel;
import com.xinwei.common.lookandfeel.component.McWillRadioGroup;
import com.xinwei.common.lookandfeel.layout.WrapFlowLayout;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.conf.UserConfig;
import com.xinwei.talk.ui.setting.SettingDetailPanel;
import com.xinwei.talk.ui.setting.SettingSubPanel;

public class SettingCommonPanel extends SettingDetailPanel {
	//自动登录
	McWillCheckLabel loginCheckLabel;
	//开启新消息提示声音
	McWillCheckLabel soundCheckLabel;
	//有更新时自动升级Talk
	McWillCheckLabel updateCheckLabel;
	//开机时自动启动Talk
	McWillCheckLabel powerOnCheckLabel;
	//接到新消息时,false:自动弹出;true:闪烁提示
	McWillRadioGroup<Boolean> receiveMsgRadioGroup;
	//	关闭主面板时,true:退出程序;false:最小化到任务栏
	McWillRadioGroup<Boolean> closeMainWindowRadioGroup;

	public SettingCommonPanel() {
		super();
	}

	protected void addSubPanels() {
		final Dimension preferredSize = new Dimension(0, 50);
		add(new SettingSubPanel(Res.getMessage("setting.general.title1")) {
			@Override
			protected void createContentPanel(JPanel contentPanel) {

				loginCheckLabel = new McWillCheckLabel(LAF.getLoginCheck()[0], LAF.getLoginCheck()[1]);
				loginCheckLabel.setText(Res.getMessage("setting.general.autologin"));

				soundCheckLabel = new McWillCheckLabel(LAF.getLoginCheck()[0], LAF.getLoginCheck()[1]);
				soundCheckLabel.setText(Res.getMessage("setting.general.promptsound"));

				updateCheckLabel = new McWillCheckLabel(LAF.getLoginCheck()[0], LAF.getLoginCheck()[1]);
				updateCheckLabel.setText(Res.getMessage("setting.general.autoupgrade"));

				powerOnCheckLabel = new McWillCheckLabel(LAF.getLoginCheck()[0], LAF.getLoginCheck()[1]);
				powerOnCheckLabel.setText(Res.getMessage("setting.general.autostart"));

				contentPanel.add(loginCheckLabel);
				contentPanel.add(soundCheckLabel);
				contentPanel.add(updateCheckLabel);
				//				contentPanel.add(powerOnCheckLabel);

				contentPanel.setPreferredSize(new Dimension(0, 2 * (18 + 10) + 25));
				contentPanel.setLayout(new GridLayout(2, 2));

			}
		});

		add(new SettingSubPanel(Res.getMessage("setting.general.receivenewmessage")) {
			@Override
			protected void createContentPanel(JPanel contentPanel) {
				receiveMsgRadioGroup = new McWillRadioGroup<Boolean>();
				receiveMsgRadioGroup.addRadio(contentPanel, Res.getMessage("setting.general.receivenewmessage.autopop"), false, false);
				receiveMsgRadioGroup.addRadio(contentPanel, Res.getMessage("setting.general.receivenewmessage.flash"), true, true);

				contentPanel.setPreferredSize(new Dimension(0, 1 * (18 + 10) + 25));
				contentPanel.setLayout(new GridLayout(1, 2));
			}
		});

		add(new SettingSubPanel(Res.getMessage("setting.general.closemainwindow")) {
			@Override
			protected void createContentPanel(JPanel contentPanel) {
				closeMainWindowRadioGroup = new McWillRadioGroup<Boolean>();
				closeMainWindowRadioGroup.addRadio(contentPanel, Res.getMessage("setting.general.closemainwindow.exit"), false, true);
				closeMainWindowRadioGroup.addRadio(contentPanel, Res.getMessage("setting.general.closemainwindow.tray"), true, false);

				contentPanel.setPreferredSize(new Dimension(0, 1 * (18 + 10) + 25));
				contentPanel.setLayout(new GridLayout(1, 2));
			}
		});
	}

	protected void initValues() {
		UserConfig userConfig = TalkManager.getUserConfig();
		//自动登录
		loginCheckLabel.setSelected(TalkManager.getVCard().isAutoLogin());
		//开启新消息提示声音
		soundCheckLabel.setSelected(userConfig.getMsgSound());
		//有更新时自动升级Talk
		updateCheckLabel.setSelected(userConfig.getAutoUpdater());
		//开机时自动启动Talk
		powerOnCheckLabel.setSelected(userConfig.getAutoPowerOn());
		//接到新消息时,0:自动弹出;1:闪烁提示
		receiveMsgRadioGroup.setSelected(userConfig.getAvatarFlash());
		//	关闭主面板时,0:退出程序;1:最小化到任务栏
		closeMainWindowRadioGroup.setSelected(userConfig.getCloseMainWindow());
	}

	protected void addListeners() {
		ActionListener al = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UserConfig userConfig = TalkManager.getUserConfig();
				//自动登录
				if (e.getSource() == loginCheckLabel) {//开启新消息提示声音
					TalkManager.getVCard().setAutoLogin(loginCheckLabel.isSelected());
					TalkManager.saveVCard();
				} else {
					if (e.getSource() == soundCheckLabel) {
						userConfig.setMsgSound(soundCheckLabel.isSelected());
					} else if (e.getSource() == updateCheckLabel) {//有更新时自动升级Talk
						userConfig.setAutoUpdater(updateCheckLabel.isSelected());
					} else if (e.getSource() == powerOnCheckLabel) {//开机时自动启动Talk
						userConfig.setAutoPowerOn(powerOnCheckLabel.isSelected());
					} else if (e.getSource() == receiveMsgRadioGroup) {//接到新消息时,false:自动弹出;true:闪烁提示
						userConfig.setAvatarFlash(receiveMsgRadioGroup.getSelected());
					} 
					else if (e.getSource() == closeMainWindowRadioGroup) {//	关闭主面板时,true:退出程序;false:最小化到任务栏
						userConfig.setCloseMainWindow(closeMainWindowRadioGroup.getSelected());
					}
					TalkManager.saveUserConfig();
				}
			}
		};

		//自动登录
		loginCheckLabel.addActionListener(al);
		//开启新消息提示声音
		soundCheckLabel.addActionListener(al);
		//有更新时自动升级Talk
		updateCheckLabel.addActionListener(al);
		//开机时自动启动Talk
		powerOnCheckLabel.addActionListener(al);
		//接到新消息时,false:自动弹出;true:闪烁提示
		receiveMsgRadioGroup.addActionListener(al);
		//	关闭主面板时,true:退出程序;false:最小化到任务栏
		closeMainWindowRadioGroup.addActionListener(al);

	}

}
