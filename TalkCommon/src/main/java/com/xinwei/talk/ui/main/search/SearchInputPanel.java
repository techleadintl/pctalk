/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 上午11:36:23
 * 
 ***************************************************************/
package com.xinwei.talk.ui.main.search;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;

import com.xinwei.common.lookandfeel.component.McWillCardPanel;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.vo.Entry;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil2;
import com.xinwei.talk.listener.UnreadListener;
import com.xinwei.talk.manager.ChatManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.message.TalkMessage;

public class SearchInputPanel extends JPanel {
	private static final long serialVersionUID = -4322806442034868526L;
	JTextField textField;

	JButton button;
	SearchListPanel searchListPanel;

	boolean isInputing;

	boolean isBacking;

	String lastText;

	private String preCardName;

	McWillCardPanel workspacePane = null;

	String DEFAULT_TEXT = Res.getMessage("input.text.search");

	final ActionListener l = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			workspacePane.show(preCardName);

			doBack();

		}
	};

	public SearchInputPanel(McWillCardPanel workspacePane) {
		this.workspacePane = workspacePane;

		createUI();

		addListeners();
	}

	private void createUI() {
		setOpaque(false);
		setPreferredSize(new Dimension(getWidth(), 20));

		setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 15));

		setLayout(new BorderLayout());

		searchListPanel = new SearchListPanel() {
			//双击打开聊天窗口
			protected void doubleClickedItem(Object object) {
				ChatManager chatManager = TalkManager.getChatManager();
				chatManager.activateChatRoom(object);
			}

			@Override
			protected void addPopuMenuItemList(JPopupMenu popup, Object object) {
				if (object instanceof Talk) {
					TalkUtil2.addPopunActions(popup, "talk", object);
				} else if (object instanceof Group) {
					TalkUtil2.addPopunActions(popup, "group", object);
				}
			}

		};
		workspacePane.addCard("search", searchListPanel);
		searchListPanel.putClientProperty("card-vertical", true);

		textField = new JTextField(DEFAULT_TEXT);
		textField.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		textField.setForeground(Color.WHITE);

		button = new McWillIconButton(LAF.getSearchIcon());

		add(textField, BorderLayout.CENTER);
		add(button, BorderLayout.EAST);

	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		LAF.paintMainSearchInputPanel(this, (Graphics2D) g);
	}

	public void doSearch() {
		if (isBacking) {
			return;
		}

		if (!isInputing) {
			preCardName = workspacePane.getSelectedName();
			workspacePane.show("search");
			isInputing = true;
			button.addActionListener(l);
			button.setIcon(LAF.getDeleteIcon()[0]);
			textField.setForeground(Color.BLACK);
			revalidate();
			repaint();
		}

		if (isInputing) {
			String text = textField.getText();
			if (text.equals(lastText)) {
				return;
			}
			lastText = text;
			searchListPanel.updateUI(text);
		}
	}

	public void doBack() {
		isBacking = true;

		button.setIcon(LAF.getSearchIcon());
		button.removeActionListener(l);

		textField.setText(DEFAULT_TEXT);
		textField.setForeground(Color.WHITE);

		isBacking = false;
		isInputing = false;

		preCardName = null;
		revalidate();
		repaint();

	}

	public void addListeners() {

		ListenerManager.addUnreadListener(new UnreadListener() {
			@Override
			public void doUnreadMessage(String jid, Entry<Integer, TalkMessage> unreadMessage) {
				searchListPanel.updateUI();
			}
		});

		workspacePane.addCardChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (workspacePane.getSelectedComponent() == searchListPanel) {
					return;
				}
				doBack();
			}
		});

		textField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!isInputing && textField.getText().equals(DEFAULT_TEXT)) {
					textField.setText("");
				}
				doSearch();
			}
		});

		//		textField.getDocument().addDocumentListener(new DocumentListener() {
		//			@Override
		//			public void removeUpdate(DocumentEvent e) {
		//				doSearch();
		//			}
		//
		//			@Override
		//			public void insertUpdate(DocumentEvent e) {
		//				doSearch();
		//			}
		//
		//			@Override
		//			public void changedUpdate(DocumentEvent e) {
		//				doSearch();
		//			}
		//		});
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				//回车打开聊天窗口
				if (textField.getText().equals(DEFAULT_TEXT)) {
					textField.setText("");
				}
				if (e.getKeyChar() == KeyEvent.VK_ENTER && textField.hasFocus()) {
					searchListPanel.doubleClicked();

				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
				super.keyTyped(e);
			}

			@Override
			public void keyReleased(KeyEvent e) {
				doSearch();
			}
		});
	}

	public boolean isStatus() {
		return isInputing;
	}

	public void setStatus(boolean status) {
		this.isInputing = status;
	}

}
