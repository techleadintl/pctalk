/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月19日 上午10:54:50
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room.history;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.UIManager;

import com.jtattoo.plaf.BaseTabbedPaneUI;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.util.component.tab.TabbedPane;

public class ChatRoomTabbedPane extends TabbedPane {
	CommonChatRoom room;

	@Override
	public Font getDefaultFontPlain() {
		return LAF.getFont(15);
	}

	public ChatRoomTabbedPane(final CommonChatRoom room) {
		this.room = room;

		setCloseButtonEnabled(true);
		setTabbedPaneUI(new ChatRoomTabbedPaneUI());
	}

	public class ChatRoomTabbedPaneUI extends BaseTabbedPaneUI {
		protected void installDefaults() {
			UIManager.put("TabbedPane.contentOpaque", false);
			super.installDefaults();

			ThemeColor themeColor = McWillTheme.getThemeColor();

			selectedColor = themeColor.getSubLightColor();

			McWillTheme.setOpaque(tabPane);

			tabInsets = new Insets(5, 5, 6, 5);
			tabAreaInsets = new Insets(0, 0, 0, 0);
		}

		protected void paintRoundedTopTabBorder(int tabIndex, Graphics g, int x1, int y1, int x2, int y2, boolean isSelected) {
		}

		protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h) {
			int tabAreaHeight = calculateTabAreaHeight(tabPlacement, runCount, maxTabHeight);
			Graphics2D g2d = (Graphics2D) g;
			g2d.setStroke(new BasicStroke(1));
			g2d.setPaint(McWillTheme.getThemeColor().getMiddleColor());
			g.drawLine(x, y + tabAreaHeight, x + w, y + tabAreaHeight);
		}

		protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
		}

		protected Insets getContentBorderInsets(int tabPlacement) {
			return new Insets(0, 0, 0, 0);
		}

		protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
			if (isSelected || tabIndex == rolloverIndex) {
				Graphics2D g2D = (Graphics2D) g;
				Color borderColor = null;
				borderColor = McWillTheme.getThemeColor().getMiddleColor();
				g.setColor(borderColor);
				//				g.drawLine(x, y, x, y + h);
				g.drawLine(x + w, y, x + w, y + h);
				//				if (tabIndex == rolloverIndex) {
				//					Object savedRederingHint = g2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
				//					g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				//					borderColor = McWillTheme.getThemeColor().getMiddleColor();
				//					g.setColor(borderColor);
				//					g.fillRoundRect(x, y, w, h, 8, 8);
				//					g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, savedRederingHint);
				//				}

			}
		}

	}
}
