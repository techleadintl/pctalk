//package com.xinwei.talk.ui.util.component.roombak;
//
//import java.awt.BorderLayout;
//import java.awt.Component;
//import java.awt.Image;
//import java.awt.Insets;
//import java.io.InputStream;
//import java.net.URL;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//
//import javax.swing.BorderFactory;
//import javax.swing.ImageIcon;
//import javax.swing.JFrame;
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//import javax.swing.SwingConstants;
//import javax.swing.border.Border;
//
//import org.jivesoftware.smack.packet.Message;
//
//import com.xinwei.common.lang.GsonUtil;
//import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
//import com.xinwei.talk.common.LAF;
//import com.xinwei.talk.manager.TalkManager;
//import com.xinwei.talk.model.xmpp.XmppBody;
//import com.xinwei.talk.model.xmpp.MsgType;
//import com.xinwei.talk.model.xmpp.XmppBodyMessage;
//
//@SuppressWarnings("serial")
//public class ChatMsgPanel extends JPanel {
//	public enum MsgType {
//		TEXT, IMAGE, VOICE, TEL, MAIL, ERROR;
//	}
//
//	private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
//	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd");
//	private Insets margin = new Insets(5, 0, 5, 0);
//	private JLabel nickNameLabel;
//	private JLabel timeLabel;
//	private JLabel avatarLabel;
//	private ChatMsgTextPane msgTP;
//
//	public ChatMsgPanel(Component component, ImageIcon avatar, String nickName, Date date, boolean isLeft) {
//		setOpaque(false);
//		if (isLeft)
//			setLayout(new BorderLayout());
//		else
//			setLayout(new BorderLayout());
//		//		setLayout(new GridBagLayout());
//		initComponent(isLeft);
//		setContent(component, avatar, nickName, date);
//	}
//
//	public ChatMsgPanel(Message message, String body, ImageIcon avatar, String nickName, Date date, boolean isLeft) {
//		setOpaque(false);
//		setLayout(new BorderLayout());
//		//		setLayout(new GridBagLayout());
//		initComponent(isLeft);
//		setContent(message, body, avatar, nickName, date);
//	}
//
//	private void setContent(Component component, ImageIcon avatar, String nickName, Date date) {
//		setInfo(date);
//
//		nickNameLabel.setText(nickName);
//
//		if (avatar != null) {
//			Image smallImage = avatar.getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH);
//			avatarLabel.setIcon(new ImageIcon(smallImage));
//			avatarLabel.setToolTipText(nickName);
//		}
//		setComponent(component);
//	}
//
//	private void setContent(Message message, String body, ImageIcon avatar, String nickName, Date date) {
//		setInfo(date);
//
//		nickNameLabel.setText(nickName);
//
//		if (avatar != null) {
//			Image smallImage = avatar.getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH);
//			avatarLabel.setIcon(new ImageIcon(smallImage));
//			avatarLabel.setToolTipText(nickName);
//		}
//		try {
//			setMsg(message, body);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private void initComponent(boolean isLeft) {
//
//		nickNameLabel = new JLabel();
//		Border border = BorderFactory.createEmptyBorder(margin.top, margin.left, margin.bottom, margin.right);
//		//		nickNameLabel.setBorder(McWillBorders.getLabelBorder());
//		//		nickNameLabel.setForeground(McWillTheme.getThemeColor().getDarkColor());
//
//		timeLabel = new JLabel();
//		timeLabel.setBorder(border);
//
//		msgTP = new ChatMsgTextPane();
//		msgTP.setEditable(false);
//		msgTP.setOpaque(false);
//		msgTP.setMargin(margin);
//
//		JPanel avatarPanel = new JPanel(new VerticalFlowLayout());
//		avatarLabel = new JLabel();
//		avatarLabel.setOpaque(false);
//		//		avatarPanel.add(nickNameLabel);
//		avatarPanel.add(avatarLabel);
//		//		avatarPanel.add(timeLabel);
//
//		JPanel rightPanel = new JPanel(new VerticalFlowLayout());
//		JPanel msgBg = new BalloonPanel(isLeft);
//		msgBg.setLayout(new BorderLayout());
//		msgBg.add(msgTP, BorderLayout.CENTER);
//
//		//		GroupLayout groupLayout = new GroupLayout(msgBg);
//		//		msgBg.setLayout(groupLayout);
//		//		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(msgTP, 20, -1, Short.MAX_VALUE));
//		//		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(msgTP, 20, -1, Short.MAX_VALUE));
//
//		if (!isLeft) {
//			//			msgTP.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
//		}
//		rightPanel.add(nickNameLabel);
//		rightPanel.add(msgBg);
//		if (isLeft) {
//			nickNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
//			add(avatarPanel, BorderLayout.WEST);
//			add(rightPanel, BorderLayout.CENTER);
//		} else {
//			nickNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
//			add(avatarPanel, BorderLayout.EAST);
//			add(rightPanel, BorderLayout.CENTER);
//		}
//
//	}
//
//	private boolean setMsg(Message message, String body) throws Exception {
//		XmppBodyMessage xwMessage = GsonUtil.fromJson(body, XmppBodyMessage.class);
//		if (xwMessage == null) {
//			return false;
//		}
//		List<XmppBody> bodies = xwMessage.getBodies();
//
//		if (bodies == null)
//			return false;
//
//		for (XmppBody messageBody : bodies) {
//			String type = messageBody.getType();
//			MsgType messageBodyType = MsgType.toType(type);
//
//			if (messageBodyType == MsgType.txt) {
//				msgTP.insertText(messageBody.getMsg());
//			} else if (messageBodyType == MsgType.img) {
//				msgTP.insertImage(new ImageIcon(TalkManager.getHttpService().downloadImage(messageBody.getUrl()), messageBody.getFilename()));
//			} else if (messageBodyType == MsgType.audio) {
//				URL url = new URL(messageBody.getUrl());
//				InputStream wavVoiceStream = url.openStream();
//				msgTP.insertAudio(wavVoiceStream);
//			} else if (messageBodyType == MsgType.offlinefile) {
//				insertOfflineFile(message, messageBody);
//			} else if (messageBodyType == MsgType.voicemail) {
//				msgTP.insertText(messageBody.toString());
//			} else {
//				msgTP.insertText(messageBody.toString());
//			}
//		}
//		return true;
//	}
//
//	private void setComponent(Component component) {
//		msgTP.addComponent(component);
//	}
//
//	protected void insertOfflineFile(Message message, XmppBody messageBody) {
//
////		final Document doc = msgTP.getDocument();
////		msgTP.select(doc.getLength(), doc.getLength());
////		boolean result = false;
////		try {
////			String httpUrl = messageBody.getUrl();
////			String fileName = messageBody.getFilename();
////			long fileSize = messageBody.getLength();
////			String hash = messageBody.getHash();
////			String requestor = message.getFrom();
////
////			msgTP.insertCustomText(Res.getString("message.file.transfer.chat.window"), true, false, Color.BLACK);
////
////			final ReceiveFileTransfer receivingMessageUI = new ReceiveFileTransfer();
////
////			FileTransferRequest request = new FileTransferRequest(httpUrl, fileName, fileSize, hash, requestor);
////			receivingMessageUI.acceptFileTransfer(request);
////
////			ChatRoom chatRoom = XW.getChatRoom(requestor);
////
////			chatRoom.addClosingListener(new ChatRoomClosingListener() {
////				public void closing() {
////					receivingMessageUI.cancelTransfer();
////				}
////			});
////
////			msgTP.addComponent(receivingMessageUI);
////
////			String fileTransMsg = chatRoom.getRoomname() + " " + Res.getString("message.file.transfer.short.message") + " " + fileName;
////			SparkManager.getChatManager().getChatContainer().fireNotifyOnMessage(chatRoom, true, fileTransMsg, Res.getString("message.file.transfer.notification"));
////
////			result = true;
////		} catch (Exception ex) {
////		}
////		msgTP.setCaretPosition(doc.getLength());
//
//	}
//
//	public void setInfo(Date date) {
//		String time = DATE_FORMAT.format(date);
//		if (System.currentTimeMillis() - date.getTime() < 24 * 3600 * 1000) {
//			time = TIME_FORMAT.format(date);
//		}
//		timeLabel.setText(time);
//	}
//
//	public static void main(String[] args) {
//		JFrame frame = new JFrame();
//		frame.getContentPane().add(new ChatMsgPanel(new JLabel("fdaf"), LAF.getDefaultAvatarIcon(), "newx", new Date(), true));
//		frame.setSize(800, 600);
//		frame.setVisible(true);
//	}
//}