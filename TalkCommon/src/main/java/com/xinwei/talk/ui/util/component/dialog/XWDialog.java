package com.xinwei.talk.ui.util.component.dialog;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.talk.common.Res;

public class XWDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	protected JButton okButton;
	protected JButton closeButton;

	protected String okText = Res.getMessage("button.ok");

	protected String closeTxt = Res.getMessage("button.cancel");

	public XWDialog(Dialog dialog, String title) {
		this(dialog, title, true);
	}

	public XWDialog(Frame dialog, String title) {
		this(dialog, title, true);
	}

	public XWDialog(Dialog owner, String title, boolean modal) {
		super(owner, title, modal);
	}

	public XWDialog(Frame owner, String title, boolean modal) {
		super(owner, title, modal);
	}

	protected void createDialog() {
		Container contentPanel = createDialogContainer();

		contentPanel.setLayout(new BorderLayout());

		contentPanel.add(createDialogContent(), BorderLayout.CENTER);
		contentPanel.add(createButtonBar(), BorderLayout.SOUTH);

		setContentPane(contentPanel);

		pack();
	}

	protected Container createDialogContainer() {
		return new JPanel();
	}

	protected JComponent createDialogContent() {
		return new JPanel();
	}

	protected JPanel createButtonBar() {
		JPanel bottomButtonPanel = new JPanel(getButtonBarLayout());
		bottomButtonPanel.setOpaque(false);

		okButton = SwingUtil.newButton(okText);
		closeButton = SwingUtil.newButton(closeTxt);

		okButton.addActionListener(this);
		closeButton.addActionListener(this);

		bottomButtonPanel.add(okButton);
		bottomButtonPanel.add(closeButton);

		return bottomButtonPanel;
	}

	protected LayoutManager getButtonBarLayout() {
		return new FlowLayout(FlowLayout.CENTER);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton) {
			if (!onValidate()) {
				return;
			}
			if (!onOkPressed()) {
				return;
			}
			afterOkPressed();
			this.dispose();
			return;
		}
		if (e.getSource() == closeButton) {
			if (!beforeClosePressed()) {
				return;
			}
			afterClosePressed();
			this.dispose();
			return;
		}
	}

	protected void setOkLabel(String txt) {
		this.okText = txt;
	}

	protected void setCancelLabel(String txt) {
		this.closeTxt = txt;
	}

	protected boolean onValidate() {
		return true;
	}

	protected boolean onOkPressed() {
		return true;
	}

	protected void afterOkPressed() {
	}

	protected boolean beforeClosePressed() {
		return true;
	}

	protected void afterClosePressed() {
	}
}