package com.xinwei.talk.ui.main.dialog.user;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.RenderingHints;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillWindowButtonUI;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.lookandfeel.util.PainterUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Contact;
import com.xinwei.talk.model.Talk;

public class TalkInfoDialog extends JFrame implements McWillWindowButtonUI{
	Talk talk;
	JPanel avatarPanel;

	@Override
	protected JRootPane createRootPane() {
		JRootPane createRootPane = super.createRootPane();
		createRootPane.putClientProperty("x", "y");
		return createRootPane;
	}

	public TalkInfoDialog(Talk talk) {
		this.talk = talk;
		setIconImages(LAF.getApplicationImages());

		getContentPane().setLayout(new BorderLayout());

		createDialog();
		//		JRootPane rootPane = getRootPane();
		//		((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

		setSize(new Dimension(350, 385));

		SwingUtil.centerWindowOnScreen(this);
	}

	protected void createDialog() {

		setLayout(new BorderLayout());

		add(createDialogContent(), BorderLayout.CENTER);

		pack();
	}

	protected JPanel createDialogContainer() {
		return new McWillContentPanel() {
			public void paintComponent(Graphics g) {
				LAF.paintMainAddGroupDialog(this, (Graphics2D) g);
				super.paintComponent(g);
			}
		};
	}

	protected JPanel createDialogContent() {
		String title = Res.getMessage("dialog.title.namecard.talk");
		JPanel dialogPanel = new JPanel(new BorderLayout());
		avatarPanel = new JPanel(new BorderLayout()) {
			protected void paintComponent(Graphics g) {
				g.drawImage(ImageManager.getTalkAvatar(talk.getJid(),50,50,true).getImage(), (getWidth() - 60) / 2, (getHeight() - 60) / 2, 50, 50, this);
				super.paintComponent(g);
			}
		};
		avatarPanel.setPreferredSize(new Dimension(0, 80));
		//		avatarPanel.add(label,BorderLayout.CENTER);
		dialogPanel.add(avatarPanel, BorderLayout.NORTH);

		dialogPanel.add(createInputPanel(), BorderLayout.CENTER);

		setTitle(title);

		return dialogPanel;
	}

	public void updateAvatar() {
		avatarPanel.repaint();
	}

	private Component createInputPanel() {
		JTextPane panel = new JTextPane();
		panel.setEditable(false);
		panel.setContentType("text/html");

		String table = "<html>" + "<table>" //
				+ "<tr><td>{0}</td><td>{1}</td></tr>" //
				+ "<tr><td>{2}</td><td>{3}</td></tr>" //
				+ "<tr><td>{4}</td><td>{5}</td></tr>" //
				+ "<tr><td>{6}</td><td>{7}</td></tr>"//
				+ "<tr><td>{8}</td><td>{9}</td></tr>" //
				+ "<tr><td>{10}</td><td>{11}</td></tr>" //
				+ "<tr><td>{12}</td><td>{13}</td></tr>" //
				+ "<tr><td>{14}</td><td>{15}</td></tr>"//
				+ "<tr><td>{16}</td><td>{17}</td></tr>" //
				+ "<tr><td>{18}</td><td>{19}</td></tr>" //
				+ "</table>" + "</html>";
		String signature = talk.getSignature();
		String firstName = null;
		String lastName = null;
		String emaill = null;
		String addr = null;
		String birthday = null;
		String remark = null;
		Contact contact = TalkManager.getLocalManager().getContactByTel(talk.getTel());
		if (contact != null) {
			firstName = contact.getFirstName();
			lastName = contact.getLastName();
			emaill = contact.getEmaill();
			addr = contact.getAddr();
			birthday = contact.getBirthday();
			remark = contact.getRemark();
		}
		String html = MessageFormat.format(table, Res.getMessage("namecard.display"), TalkUtil.getDisplayName(talk.getJid()), Res.getMessage("namecard.nick"), getString(talk.getNick()),
				Res.getMessage("namecard.tel"), getTel(talk.getTel()), Res.getMessage("namecard.signature"), getString(signature), Res.getMessage("namecard.firstname"), getString(firstName),
				Res.getMessage("namecard.lastname"), getString(lastName), Res.getMessage("namecard.emaill"), getString(emaill), Res.getMessage("namecard.addr"), getString(addr),
				Res.getMessage("namecard.birthday"), getString(birthday), Res.getMessage("namecard.note"), getString(remark));

		panel.setText(html);
		return panel;
	}

	public String getString(String str) {
		if (str == null)
			return "";
		return str;
	}

	public String getTel(String tel) {
		String[] countryCodeAndTel = TalkUtil.getCountryCodeAndTel(tel);
		if (countryCodeAndTel == null)
			return getString(tel);
		return countryCodeAndTel[0] + " " + countryCodeAndTel[1];
	}

	protected LayoutManager getButtonBarLayout() {
		return new FlowLayout(FlowLayout.RIGHT);
	}

	/**
	 * Brings the ChatFrame into focus on the desktop.
	 */
	public void bringFrameIntoFocus() {
		if (!isVisible()) {
			setVisible(true);
		}

		if (getState() == Frame.ICONIFIED) {
			setState(Frame.NORMAL);
		}

		toFront();
		requestFocus();
	}

	/**
	 * set if the chatFrame should always stay on top
	 * 
	 * @param active
	 */
	public void setWindowAlwaysOnTop(boolean active) {
		setAlwaysOnTop(active);
	}

	@Override
	public boolean isMax() {
		return false;
	}

	@Override
	public boolean isIconify() {
		return true;
	}
}
