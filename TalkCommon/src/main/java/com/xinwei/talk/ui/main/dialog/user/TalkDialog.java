package com.xinwei.talk.ui.main.dialog.user;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextField;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.HttpUtils;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.ui.util.component.XWDialogTitle;
import com.xinwei.talk.ui.util.component.XWMcHintTextField;
import com.xinwei.talk.ui.util.component.dialog.XWDialog;

@SuppressWarnings("serial")
public class TalkDialog extends XWDialog {

	private JTextField accountField;

	private SearchResultPanel searchResultPanel;

	private McWillIconButton searchUserButton;

	private Talk camTalk = null;

	/**
	 * Create a new instance of RosterDialog.
	 */
	public TalkDialog() {
		super(TalkManager.getMainWindow(), "", false);
		createDialog();

		JRootPane rootPane = getRootPane();
		((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

		setSize(new Dimension(350, 200));
	}

	@Override
	protected JPanel createDialogContainer() {
		return new McWillContentPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				LAF.paintMainAddGroupDialog(this, (Graphics2D) g);
			}
		};
	}

	@Override
	protected JPanel createDialogContent() {
		JPanel dialogPanel = new JPanel(new BorderLayout());
		XWDialogTitle titlePanel = new XWDialogTitle(this, Res.getMessage("tabTitle.contact.add"));
		titlePanel.setBorder(BorderFactory.createEmptyBorder(6, 10, 6, 0));
		dialogPanel.add(titlePanel, BorderLayout.NORTH);

		dialogPanel.add(createInputPanel(), BorderLayout.CENTER);

		return dialogPanel;
	}

	private JPanel createInputPanel() {
		JPanel inputPanel = new JPanel(new BorderLayout());

		searchResultPanel = new SearchResultPanel();

		//电话号码，包括国家码
		accountField = new XWMcHintTextField(getUserTeleDefaultText());
		accountField.setPreferredSize(new Dimension(200, 27));

		searchUserButton = new McWillIconButton(LAF.getQueryImageIcon());

		searchUserButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				final String tel = accountField.getText().trim();
				if (tel.length() == 0 || getUserTeleDefaultText().equals(tel)) {
					searchResultPanel.sertResult(Res.getMessage("tip.contact.tel.isempty"));
				} else {
					Talk ct = TalkManager.getLocalManager().getTalk(tel);
					if (ct != null) {
						searchResultPanel.sertResult(Res.getMessage("tip.contact.search.exist", tel));
					} else {
						searchResultPanel.sertResult(Res.getMessage("tip.contact.search", tel));
						TaskEngine.getInstance().submit(new Runnable() {
							@Override
							public void run() {
								try {
									camTalk = TalkManager.getTalkService().getTalk(tel);
									try {
										byte[] avatar = HttpUtils.download(camTalk.getAvatarUrl());
										camTalk.setAvatar(avatar);
									} catch (Exception ex) {
										Log.error(ex);
									}

									searchResultPanel.sertResult(camTalk);
								} catch (Exception e1) {
									if (camTalk != null) {
										Log.error(e1);
									}
									searchResultPanel.sertResult(Res.getMessage("tip.contact.search.noresult", tel));
								}
							}
						});
					}
				}
			}
		});

		JPanel searchPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		searchPanel.add(accountField);
		searchPanel.add(searchUserButton);

		inputPanel.add(searchPanel, BorderLayout.NORTH);
		inputPanel.add(searchResultPanel, BorderLayout.CENTER);

		return inputPanel;
	}

	@Override
	protected boolean onValidate() {
		final String tel = accountField.getText().trim();
		if (tel.length() == 0 || getUserTeleDefaultText().equals(tel)) {
			searchResultPanel.sertResult(Res.getMessage("tip.contact.tel.isempty"));
			return false;
		} else {
			Talk ct = TalkManager.getLocalManager().getTalk(tel);
			if (ct != null) {
				searchResultPanel.sertResult(Res.getMessage("tip.contact.search.exist", tel));
				return false;
			}
		}

		return true;
	}

	@Override
	protected boolean onOkPressed() {
		if (camTalk == null)
			return false;
		String useTel = TalkUtil.getUserTel(camTalk.getJid());
		Talk camTalk2 = TalkManager.getLocalManager().getTalk(useTel);
		if (camTalk2 != null) {//已经存在
			return false;
		}

		final List<Talk> list = new ArrayList<Talk>();
		list.add(camTalk);

		TalkManager.getVCardManager().addTalk(list);

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				try {
					TalkManager.getTalkService().addTalk(camTalk, null);
				} catch (Exception e) {//如果上传失败，则删除
					Log.error(e);
					TalkManager.getVCardManager().deleteTalk(list);
				}
			}
		});
		return true;
	}

	private String getUserTeleDefaultText() {
		return Res.getMessage("tip.contact.tel");
	}

	@Override
	protected LayoutManager getButtonBarLayout() {
		return new FlowLayout(FlowLayout.CENTER, 30, 5);
	}

	protected JPanel createButtonBar() {
		JPanel bottomButtonPanel = new JPanel(getButtonBarLayout());
		bottomButtonPanel.setOpaque(false);
		
		closeButton = SwingUtil.newButton(closeTxt);
		okButton = SwingUtil.newButton(Res.getMessage("button.contact.add.ok"));

		okButton.addActionListener(this);
		closeButton.addActionListener(this);

		bottomButtonPanel.add(closeButton);
		bottomButtonPanel.add(okButton);

		return bottomButtonPanel;
	}

	/**
	 * Display the RosterDialog using the MainWindow as the parent.
	 */
	public void showDialog() {
		setLocationRelativeTo(null);

		setVisible(true);
		toFront();
		requestFocus();

		//		accountField.requestFocus();
	}

	@Override
	public void setVisible(boolean b) {
		camTalk = null;
		super.setVisible(b);
	}

}