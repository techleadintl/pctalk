package com.xinwei.talk.ui.chat.relay;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JRootPane;

import org.jivesoftware.smack.packet.Message;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.conf.UserConfig;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkParam;
import com.xinwei.talk.model.message.specific.TalkTextSpecific;
import com.xinwei.talk.ui.chat.balloon.BalloonMessage;
import com.xinwei.talk.ui.util.component.XWDialogTitle;
import com.xinwei.talk.ui.util.component.dialog.XWDialog;

@SuppressWarnings("serial")
public class RelayDialog extends XWDialog {
	private RelayPanel relayPanel;

	private BalloonMessage balloonMsg;

	/**
	 * Create a new instance of RosterDialog.
	 */
	public RelayDialog(BalloonMessage balloonMsg) {
		super(TalkManager.getChatFrame(), "", false);
		this.balloonMsg = balloonMsg.clone();
		this.balloonMsg.clear();
		createDialog();
		JRootPane rootPane = getRootPane();
		((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

		setSize(new Dimension(670, 450));

		SwingUtil.centerWindowOnComponent(this, TalkManager.getChatFrame());
	}

	public void getForwardMsg(BalloonMessage msg) {
		BalloonMessage newMsg = new BalloonMessage(msg.getId());
		balloonMsg.setStatus(BalloonMessage.STATUS_YES);
		newMsg.setAvatar(msg.getAvatar());
		newMsg.setTalkMessage(msg.getTalkMessage());
		newMsg.setDate(msg.getDate());
		newMsg.setAlignment(msg.getAlignment());
		newMsg.setAvatar(msg.isAvatar());
		newMsg.setBackground(msg.isBackground());
	}

	@Override
	protected JPanel createDialogContainer() {
		return new McWillContentPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				LAF.paintMainAddGroupDialog(this, (Graphics2D) g);
			}
		};
	}

	@Override
	protected JPanel createDialogContent() {
		JPanel dialogPanel = new JPanel(new BorderLayout());
		XWDialogTitle titlePanel = new XWDialogTitle(this, Res.getMessage("dialog.title.relay"));
		titlePanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 0));
		dialogPanel.add(titlePanel, BorderLayout.NORTH);

		dialogPanel.add(createInputPanel(), BorderLayout.CENTER);

		return dialogPanel;
	}

	private JPanel createInputPanel() {
		JPanel inputPanel = new JPanel();

		relayPanel = new RelayPanel(this, balloonMsg);

		inputPanel.setLayout(new GridBagLayout());

		inputPanel.add(relayPanel, new GridBagConstraints(0, 4, 2, 1, 1.0D, 1.0D, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

		return inputPanel;
	}

	@Override
	protected boolean onValidate() {
		return true;
	}

	@Override
	protected boolean onOkPressed() {
		String note = relayPanel.getNote();
		List<String> jidList = relayPanel.getJidList();
		TalkMessage talkMessage = balloonMsg.getTalkMessage();
		List<TalkSpecific> msgList = talkMessage.getSpecificList();
		List<TalkSpecific> noteList = new ArrayList<>();
		if (StringUtil.isNotEmpty(note)) {
			noteList.add(new TalkTextSpecific(note));
		}

		for (String jid : jidList) {
			forwardMessage(msgList, jid);
			if (!noteList.isEmpty()) {
				forwardMessage(noteList, jid);
			}
		}

		return true;
	}

	public void forwardMessage(List<TalkSpecific> msgList, String jid) {
		if (CollectionUtil.isEmpty(msgList)) {
			return;
		}

		boolean group = TalkUtil.isGroup(jid);
		String from = TalkManager.getVCard().getTel();
		String to = group ? TalkUtil.getGroupId(jid) : TalkUtil.getUserTel(jid);

		TalkBody dstTalkBody = new TalkBody(from, to);
		for (TalkSpecific msg : msgList) {
			dstTalkBody.addSpecific(msg);
		}
		UserConfig userConfig = TalkManager.getUserConfig();
		TalkParam param = new TalkParam();
		param.setB(userConfig.getBold() ? 1 : 0);
		param.setI(userConfig.getItalic() ? 1 : 0);
		param.setU(userConfig.getUnderline() ? 1 : 0);
		param.setS(userConfig.getFontSize());
		param.setF(userConfig.getFontFamily());
		param.setFg(userConfig.getForeground());
		param.setBg(userConfig.getBackground());
		param.setBalloon(userConfig.getBalloon());
		dstTalkBody.setParam(param);

		///////////////////////
		String packetId = TalkManager.getMessageManager().nextPackageId();
		dstTalkBody.setPacketId( packetId);

		TalkMessage talkMessage = new TalkMessage();
		talkMessage.setId(TalkManager.getMessageManager().nextMessageId());
		talkMessage.setJid(jid);
		talkMessage.setSendOrReceive(TalkMessage.TYPE_SEND);
		talkMessage.setReaded(TalkMessage.STATUS_READED);

		talkMessage.setIsGroup(false);
		talkMessage.setTalkBody(dstTalkBody);
		if (group) {
			talkMessage.setUserTel(TalkManager.getVCard().getTel());
			talkMessage.setIsGroup(true);
		}

		/////////////
		final Message message = new Message();
		message.setPacketID(packetId);
		//封装消息的类型、发送方、接收方
		message.setTo(jid);
		message.setFrom(TalkManager.getSessionManager().getJID());
		if (group) {
			message.setType(Message.Type.groupchat);
		} else {
			message.setType(Message.Type.chat);
		}

		message.setBody(MessageUtil.getXmppBodyString(talkMessage.getTalkBody(),message.getType()));

		//发送消息
		try {
			// Notify users that message has been sent,如：保存到历史记录中
			ListenerManager.fireMessageSent(jid, talkMessage);
			//发送消息
			TalkManager.getConnection().sendPacket(message);
		} catch (Exception e) {
			Log.error(e);
		}
	}

	@Override
	protected LayoutManager getButtonBarLayout() {
		return new FlowLayout(FlowLayout.RIGHT, 30, 5);
	}

	protected JPanel createButtonBar() {
		JPanel bottomButtonPanel = new JPanel(getButtonBarLayout());
		bottomButtonPanel.setOpaque(false);

		closeButton = SwingUtil.newButton(closeTxt);
		okButton = SwingUtil.newButton(Res.getMessage("button.relay"));
		enableOkButton(false);

		okButton.addActionListener(this);
		closeButton.addActionListener(this);

		bottomButtonPanel.add(closeButton);
		bottomButtonPanel.add(okButton);

		return bottomButtonPanel;
	}

	public void enableOkButton(boolean enable) {
		okButton.setEnabled(enable);
	}

	/**
	 * Display the RosterDialog using the MainWindow as the parent.
	 */
	public void showDialog() {
		setLocationRelativeTo(null);

		setVisible(true);
		toFront();
		requestFocus();

		//		groupNameField.requestFocus();
	}

	public BalloonMessage getBalloonMsg() {
		return balloonMsg;
	}

}