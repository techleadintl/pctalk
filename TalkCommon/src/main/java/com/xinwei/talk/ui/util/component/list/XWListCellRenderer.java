/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月4日 下午5:55:08
 * 
 ***************************************************************/
package com.xinwei.talk.ui.util.component.list;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;

public class XWListCellRenderer extends JPanel implements ListCellRenderer {
	private int mHoveredJListIndex = -1;

	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		addMyMouseMotionListener(list);
	
		JPanel panel = (JPanel) value;
		ThemeColor themeColor = McWillTheme.getThemeColor();
		if (isSelected || cellHasFocus) {
			panel.setForeground((Color) UIManager.get("McWillList2.selectionForeground"));
			//				panel.setBackground((Color) UIManager.get("McWillList2.selectionBackground"));
			panel.setBackground(themeColor.getMiddleColor());
		} else {
			Color backgroundColor = mHoveredJListIndex == index ? themeColor.getLightColor() : list.getBackground();
			panel.setBackground(backgroundColor);
			panel.setForeground(list.getForeground());
		}

		return panel;
	}

	private void addMyMouseMotionListener(final JList list) {
		boolean flag = false;
		MouseListener[] mouseListeners = list.getMouseListeners();
		for (MouseListener mouseListener : mouseListeners) {
			if (mouseListener instanceof MyMouseMotionListener) {
				flag = true;
			}
		}
		if (!flag) {
			list.addMouseMotionListener(new MyMouseMotionListener(list));
		}
	}

	class MyMouseMotionListener extends MouseAdapter {
		private JList list;

		public MyMouseMotionListener(JList list) {
			this.list = list;
		}

		public void mouseMoved(MouseEvent me) {
			Point p = new Point(me.getX(), me.getY());
			int index = list.locationToIndex(p);
			if (index != mHoveredJListIndex) {
				mHoveredJListIndex = index;
				list.repaint();
			}
		}
	}
}
