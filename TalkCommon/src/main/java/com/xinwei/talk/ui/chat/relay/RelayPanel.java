package com.xinwei.talk.ui.chat.relay;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import com.xinwei.common.lang.PinyinUtil;
import com.xinwei.common.lookandfeel.McWillBorders;
import com.xinwei.common.lookandfeel.component.McWillFilterList.MatchComparator;
import com.xinwei.common.lookandfeel.component.text.McWillSearchTextField;
import com.xinwei.common.lookandfeel.component.text.McWillTextInputPanel;
import com.xinwei.common.lookandfeel.layout.WrapFlowLayout;
import com.xinwei.spark.ui.MessageDialog;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.XWObject;
import com.xinwei.talk.ui.chat.TalkTextPane;
import com.xinwei.talk.ui.chat.balloon.BalloonMessage;
import com.xinwei.talk.ui.chat.component.ForwardComponent;
import com.xinwei.talk.ui.main.search.SearchListPanel;

@SuppressWarnings("serial")
public class RelayPanel extends JPanel {
	private static final long serialVersionUID = 3575273326105549609L;

	private McWillTextInputPanel leftTextField;

	private SearchListPanel searchListPanel;

	private JLabel rightCenterLabel;

	JPanel rightUserPane;

	private String HOLD_TEXT = Res.getMessage("input.text.filter.telandname");

	List<String> jidList = new ArrayList<>();
	McWillTextInputPanel noteTextInput;

	public RelayPanel(final RelayDialog relayDialog, BalloonMessage balloonMsg) {
		this.setLayout(new BorderLayout());

		//左侧
		leftTextField = new McWillSearchTextField(HOLD_TEXT, LAF.getTextSearchIcon(), LAF.getDeleteIcon()[0]) {
			@Override
			public void doSearch(String text) {
				searchListPanel.updateUI(text);
			}
		};
		leftTextField.setPreferredSize(new Dimension(0, 27));

		searchListPanel = new SearchListPanel() {
			@Override
			protected void doubleClickedItem(Object object) {
				if (object instanceof XWObject) {
					if (jidList.size() > 7) {
						MessageDialog.showAlert(relayDialog, Res.getMessage("warn.message.relay", 8));
						return;
					}

					final String jid = ((XWObject) object).getJid();
					if (jidList.contains(jid))
						return;
					jidList.add(jid);

					rightUserPane.add(new ForwardComponent(jid) {
						public void doDelete() {
							jidList.remove(jid);
							rightUserPane.remove(this);
							rightUserPane.updateUI();
							rightCenterLabel.setText(Res.getMessage("label.tip.relay", jidList.size(), 8));
							relayDialog.enableOkButton(jidList.size() > 0);
						};
					});
					rightUserPane.updateUI();
					rightCenterLabel.setText(Res.getMessage("label.tip.relay", jidList.size(), 8));
					relayDialog.enableOkButton(jidList.size() > 0);
				}
			}
		};

		JPanel leftPanel = new JPanel(new BorderLayout(5, 5));
		leftPanel.setPreferredSize(new Dimension(230, 0));
		leftPanel.add(leftTextField, BorderLayout.NORTH);
		leftPanel.add(searchListPanel, BorderLayout.CENTER);

		//右侧
		JPanel rightTopPanel = new JPanel(new BorderLayout(5, 5));
		rightTopPanel.setPreferredSize(new Dimension(0, 200));

		noteTextInput = new McWillTextInputPanel(Res.getMessage("input.text.relay.note"));
		noteTextInput.setPreferredSize(new Dimension(0, 27));

		TalkTextPane textPane = new TalkTextPane();
		textPane.setEditable(false);
		JScrollPane transcriptWindowScrollPanel = new JScrollPane(textPane);
		transcriptWindowScrollPanel.setAutoscrolls(true);
		transcriptWindowScrollPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		transcriptWindowScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		//		rightTopPanel.add(rightLabel, BorderLayout.NORTH);
		rightTopPanel.add(transcriptWindowScrollPanel, BorderLayout.CENTER);
		rightTopPanel.add(noteTextInput, BorderLayout.SOUTH);

		/////////
		JPanel rightCenterPanel = new JPanel(new BorderLayout(5, 5));
		rightCenterLabel = new JLabel(Res.getMessage("label.tip.relay", 0, 8));
		rightCenterLabel.setPreferredSize(new Dimension(0, 27));

		WrapFlowLayout layout = new WrapFlowLayout(5, 5);
		layout.setHalign(SwingConstants.LEFT);
		rightUserPane = new JPanel(layout);

		//		rightUserPane.add(new ForwardComponent(t))
		//		rightUserPane.setEditable(false);

		rightCenterPanel.add(rightCenterLabel, BorderLayout.NORTH);
		rightCenterPanel.add(rightUserPane, BorderLayout.CENTER);

		JPanel rightPanel = new JPanel(new BorderLayout(5, 5));
		rightPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

		rightPanel.add(rightTopPanel, BorderLayout.NORTH);
		rightPanel.add(rightCenterPanel, BorderLayout.CENTER);

		add(leftPanel, BorderLayout.WEST);
		add(rightPanel, BorderLayout.CENTER);

		//

		searchListPanel.updateUI("");

		try {
			textPane.addBalloonMessage(balloonMsg);
			textPane.setCaretPosition(0);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String getNote() {
		return noteTextInput.getText();
	}

	public List<String> getJidList() {
		return jidList;
	}

	class MyMatchComparator implements MatchComparator<Talk> {
		@Override
		public int compare(Talk o1, Talk o2) {
			return TalkUtil.getDisplayName(o1.getJid()).compareTo(TalkUtil.getDisplayName(o2.getJid()));
		}

		@Override
		public boolean isMatch(Talk camtalk, String text) {
			if (text == null)
				return true;
			String tel = TalkUtil.getUserTel(camtalk.getJid());
			String displayName = TalkUtil.getDisplayName(camtalk.getJid());
			return PinyinUtil.isMatch(text, displayName, tel);
		}
	};
}
