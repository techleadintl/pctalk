/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月13日 下午1:06:47
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.balloon;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import javax.swing.SizeRequirements;
import javax.swing.border.Border;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.BoxView;
import javax.swing.text.ComponentView;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.IconView;
import javax.swing.text.LabelView;
import javax.swing.text.ParagraphView;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;

import com.android.ninepatch.NinePatch;
import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lookandfeel.util.ColorUtil;
import com.xinwei.common.lookandfeel.util.TalkHighlighter;
import com.xinwei.common.ui.vo.chat.Balloon;
import com.xinwei.talk.common.Constants;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkImageSpecific;
import com.xinwei.talk.model.message.specific.TalkVoiceMailSpecific;
import com.xinwei.talk.model.message.specific.TalkVoiceSpecific;
import com.xinwei.talk.ui.chat.component.ImageComponent;
import com.xinwei.talk.ui.chat.component.VoiceComponent;
import com.xinwei.talk.ui.chat.component.VoiceMailComponent;
import com.xinwei.talk.ui.util.component.room.ChatRoomComponent;

public class BalloonTextPane extends JTextPane {
	public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");

	final StyleContext sc = new StyleContext();

	final List<BalloonMessage> messages = new LinkedList<BalloonMessage>();

	private Point mousePoint;
	private Object lock = new Object();

	private static int TEXTPANE_BORDER = 65;
	private static int HEADSIZE = 40;

	public BalloonTextPane() {
		super();

		setStyledDocument(new DefaultStyledDocument(sc));
		this.setEditorKit(new TalkStyledEditorKit());

		setBorder(BorderFactory.createEmptyBorder(0, TEXTPANE_BORDER, 0, TEXTPANE_BORDER));
		//		setEditable(false); // 只读模式，不允许用户修改内容 
		setOpaque(false); // 设置成背景透明后，完全自绘才会看到效果 
		//		setContentType("text/rtf");
		// Build the styles
		createDocumentStyles();

		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				mousePoint = e.getPoint();
				repaint();
			}
		});

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Point point = e.getPoint();

				synchronized (lock) {
					for (BalloonMessage bm : messages) {
						Rectangle busyRect = bm.getButtonRect();
						if (bm.getStatus() == BalloonMessage.STATUS_NO && busyRect != null && busyRect.contains(point)) {
							bm.redo();
						}
					}
				}
			}
		});

		setHighlighter(new TalkHighlighter(this));
	}

	public void clear() {
		synchronized (lock) {
			messages.clear();
			mousePoint = null;
		}
	}

	public List<BalloonMessage> getMessageList() {
		synchronized (lock) {
			return new ArrayList<BalloonMessage>(messages);
		}
	}

	public void delete(BalloonMessage message) {
		if (message != null) {
			synchronized (lock) {
				int index = getBalloonMessageIndex(message);
				try {
					getDocument().remove(index, message.getLength());
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
				messages.remove(message);
			}
			repaint();
		}
	}

	protected void delete(long messageId) {
		synchronized (lock) {
			Object[] objects = getBalloonMessageIndex(messageId);
			int index = (int) objects[0];
			BalloonMessage message = (BalloonMessage) objects[1];
			if (message == null)
				return;
			try {
				getDocument().remove(index, message.getLength());
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
			messages.remove(message);
		}
		repaint();
	}

	protected void seletct(long messageId) {
		synchronized (lock) {
			Object[] objects = getBalloonMessageIndex(messageId);
			int index = (int) objects[0];
			BalloonMessage message = (BalloonMessage) objects[1];
			if (message == null)
				return;
			try {
				int textStartPosition = message.getTextStartPosition();
				int textEndPosition = message.getTextEndPosition();
				select(index + textStartPosition, index + textEndPosition);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void addBalloonMessage(final BalloonMessage balloonMsg) throws Exception {
		StyledDocument doc = getStyledDocument();

		int msl = doc.getLength();
		if (doc.getLength() > 0) {
			addLine();
		}

		//添加名称和时间信息
		insertNameAndTime(balloonMsg, doc, msl);
		//添加消息
		insertText(balloonMsg, doc, msl);

		addLine();

		//泡泡消息总长度
		balloonMsg.setLength(doc.getLength() - msl);

		synchronized (lock) {
			this.messages.add(balloonMsg);
		}
	}

	protected void insertNameAndTime(final BalloonMessage balloonMsg, StyledDocument doc, int msl) throws BadLocationException {
		String nick = balloonMsg.getNick();
		Date date = balloonMsg.getDate();
		if (nick == null && date == null) {
			//名称起始位置
			balloonMsg.setNickStartPosition(doc.getLength() - msl);
			//名称结束位置
			balloonMsg.setNickEndPosition(doc.getLength() - msl);
			//时间起始位置
			balloonMsg.setTimeStartPosition(doc.getLength() - msl);
			//时间结束位置
			balloonMsg.setTimeEndPosition(doc.getLength() - msl);
			return;
		}
		
		// Name & time will flip
		if (balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_RIGHT) {			// BalloonMessage.ALIGNMENT_LEFT change May 17
			//名称
			insertDocName(balloonMsg, doc, msl);

			doc.insertString(doc.getLength(), "  ", SimpleAttributeSet.EMPTY);

			//时间
			insertDocTime(balloonMsg, doc, msl);

			doc.setParagraphAttributes(msl + balloonMsg.getNickStartPosition(), msl + balloonMsg.getTimeEndPosition(), sc.getStyle(STYLE_LEFT_NAME), true);

			//插入其他信息
			insertOtherComponent(balloonMsg);

		} else if (balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_CENTER) {
			//名称
			insertDocName(balloonMsg, doc, msl);

			doc.insertString(doc.getLength(), "  ", SimpleAttributeSet.EMPTY);

			//时间
			insertDocTime(balloonMsg, doc, msl);

			doc.setParagraphAttributes(msl + balloonMsg.getNickStartPosition(), msl + balloonMsg.getTimeEndPosition(), sc.getStyle(STYLE_CENTER_NAME), true);

			//插入其他信息
			insertOtherComponent(balloonMsg);

		} else if (balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_LEFT) { 		// BalloonMessage.ALIGNMENT_RIGHT changed May 17
			//插入其他信息
			insertOtherComponent(balloonMsg);

			//时间
			insertDocTime(balloonMsg, doc, msl);

			doc.insertString(doc.getLength(), "  ", SimpleAttributeSet.EMPTY);

			//名称
			insertDocName(balloonMsg, doc, msl);

			doc.setParagraphAttributes(msl + balloonMsg.getTimeStartPosition(), msl + balloonMsg.getNickEndPosition(), sc.getStyle(STYLE_RIGHT_NAME), true);
		}

		addLine();
	}

	//	protected void insertTime(final BalloonTMessage balloonMsg, StyledDocument doc, int msl) throws BadLocationException {
	//		if (balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_LEFT) {
	//			insertDocTime(balloonMsg, doc, msl);
	//
	//			doc.setParagraphAttributes(msl + balloonMsg.getTimeStartPosition(), msl + balloonMsg.getTimeEndPosition(), sc.getStyle(STYLE_LEFT_NAME), true);
	//
	//			//插入其他信息
	//			insertOtherComponent(balloonMsg);
	//
	//		} else if (balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_CENTER) {
	//			insertDocTime(balloonMsg, doc, msl);
	//
	//			doc.setParagraphAttributes(msl + balloonMsg.getTimeStartPosition(), msl + balloonMsg.getTimeEndPosition(), sc.getStyle(STYLE_CENTER_NAME), true);
	//
	//			//插入其他信息
	//			insertOtherComponent(balloonMsg);
	//		} else if (balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_RIGHT) {
	//			//插入其他信息
	//			insertOtherComponent(balloonMsg);
	//
	//			insertDocTime(balloonMsg, doc, msl);
	//
	//			doc.setParagraphAttributes(msl + balloonMsg.getTimeStartPosition(), msl + balloonMsg.getTimeEndPosition(), sc.getStyle(STYLE_RIGHT_NAME), true);
	//		}
	//
	//		addLine();
	//	}

	public void addLine() throws BadLocationException {
		Document doc = getDocument();

		doc.insertString(doc.getLength(), "\n", sc.getStyle(STYLE_LINE));
	}

	///名称
	protected void insertDocName(final BalloonMessage balloonMsg, StyledDocument doc, int msl) throws BadLocationException {
		//名称起始位置
		balloonMsg.setNickStartPosition(doc.getLength() - msl);
		doc.insertString(doc.getLength(), balloonMsg.getNick(), SimpleAttributeSet.EMPTY);
		//名称结束位置
		balloonMsg.setNickEndPosition(doc.getLength() - msl);
	}

	//时间
	protected void insertDocTime(final BalloonMessage balloonMsg, StyledDocument doc, int msl) throws BadLocationException {
		//时间起始位置
		balloonMsg.setTimeStartPosition(doc.getLength() - msl);
		String timepattern = balloonMsg.getTimepattern();
		if (timepattern == null) {
			timepattern = "HH:mm:ss";
		}
		SimpleDateFormat TIME_FORMAT = new SimpleDateFormat(timepattern);
		if (balloonMsg.getDate() != null) {
			doc.insertString(doc.getLength(), TIME_FORMAT.format(balloonMsg.getDate()), SimpleAttributeSet.EMPTY);
		} else {//如果时间为空，使用空格占位。以便后续更新
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < timepattern.length(); i++) {
				sb.append(" ");
			}
			doc.insertString(doc.getLength(), sb.toString(), SimpleAttributeSet.EMPTY);
		}
		//时间结束位置
		balloonMsg.setTimeEndPosition(doc.getLength() - msl);
	}

	protected void insertText(final BalloonMessage balloonMsg, StyledDocument doc, int msl) throws Exception {
		//消息起始位置
		balloonMsg.setTextStartPosition(doc.getLength() - msl);

		//消息样式
		SimpleAttributeSet msgStyles = new SimpleAttributeSet();
		{
			StyleConstants.setFontFamily(msgStyles, balloonMsg.getFontFamily());
			StyleConstants.setFontSize(msgStyles, balloonMsg.getFontSize());
			StyleConstants.setUnderline(msgStyles, balloonMsg.isUnderline());
			StyleConstants.setItalic(msgStyles, balloonMsg.isItalic());
			StyleConstants.setBold(msgStyles, balloonMsg.isBold());
			StyleConstants.setForeground(msgStyles, LAF.getColor(balloonMsg.getForeground()));
		}

		MessageUtil.addTextPaneMsg(this, msl, msgStyles, balloonMsg);

		//消息结束位置
		balloonMsg.setTextEndPosition(doc.getLength() - msl);

		if (balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_RIGHT) {	// BalloonMessage.ALIGNMENT_LEFT
			doc.setParagraphAttributes(msl + balloonMsg.getTextStartPosition(), msl + balloonMsg.getTextEndPosition(), sc.getStyle(STYLE_LEFT_MSG), true);
		} else if (balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_LEFT) {   // BalloonMessage.ALIGNMENT_RIGTH
			doc.setParagraphAttributes(msl + balloonMsg.getTextStartPosition(), msl + balloonMsg.getTextEndPosition(), sc.getStyle(STYLE_RIGHT_MSG), true);
		} else if (balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_CENTER) {
			doc.setParagraphAttributes(msl + balloonMsg.getTextStartPosition(), msl + balloonMsg.getTextEndPosition(), sc.getStyle(STYLE_CENTER_MSG), true);
		}
	}

	protected void insertOtherComponent(BalloonMessage balloonMsg) {
	}

	public void addComponent(Component component) {
		StyledDocument doc = getStyledDocument();
		select(doc.getLength(), doc.getLength());

		insertComponent(component);

		setCaretPosition(doc.getLength());
		//		updateUI();
		revalidate();
	}

	public synchronized void updateMessageDate(BalloonMessage message, Date date) {
		if (date != null) {
			int balloonMessageIndex = getBalloonMessageIndex(message);
			int timeStartPosition = balloonMessageIndex + message.getTimeStartPosition();
			int timeEndPosition = balloonMessageIndex + message.getTimeEndPosition();

			int selectionStart = getSelectionStart();
			int selectionEnd = getSelectionEnd();

			String timepattern = message.getTimepattern();
			if (timepattern == null) {
				timepattern = "HH:mm:ss";
			}
			SimpleDateFormat TIME_FORMAT = new SimpleDateFormat(timepattern);
			String format = TIME_FORMAT.format(date);

			select(timeStartPosition, timeEndPosition);

			setEditable(true);
			replaceSelection(format);
			setEditable(false);

			select(selectionStart, selectionEnd);
		}
	}

	public void updateMessage(BalloonMessage message, TalkSpecific msg, byte[] bytes) {
		if (msg instanceof TalkImageSpecific) {
			updateMessageImage(message, (TalkImageSpecific) msg, bytes);
		} else if (msg instanceof TalkVoiceSpecific) {
			updateMessageVoice(message, (TalkVoiceSpecific) msg, bytes);
		}
	}

	public synchronized void updateMessageImage(BalloonMessage message, TalkImageSpecific msg, byte[] bytes) {
		ImageComponent imageLabel = (ImageComponent) message.getComponent(msg);
		if (imageLabel == null) {
			return;
		}
		final ImageIcon icon;
		if (bytes == null) {
			icon = LAF.getMsgImage();
		} else {
			icon = new ImageIcon(bytes);
		}
		imageLabel.setLabelIcon(icon, bytes);

	}

	public void updateMessageVoice(BalloonMessage message, TalkVoiceSpecific msg, byte[] bytes) {
		VoiceComponent component = (VoiceComponent) message.getComponent(msg);
		if (component == null) {
			return;
		}
		if (bytes == null) {//失败
			component.setDefaultIcon(false);
		} else {
			component.setDefaultIcon(true);
			component.setVoice(bytes);
		}
	}

	public void updateMessageVoice(BalloonMessage message, TalkVoiceMailSpecific msg, byte[] bytes) {
		VoiceMailComponent component = (VoiceMailComponent) message.getComponent(msg);
		if (component == null) {
			return;
		}
		if (bytes == null) {//失败
			component.setDefaultIcon(false);
		} else {
			component.setDefaultIcon(true);
			component.setVoice(bytes);
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		synchronized (lock) {
			Graphics2D g2D = (Graphics2D) g;
			g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // 反锯齿平滑绘制  

			// 通过对并发消息链表遍历绘制全部消息气泡、消息发出者头像  
			for (BalloonMessage message : messages) {
				Rectangle rec = getMessageRectangle(message);
				if (rec == null)
					break;

				int border = 8;//气泡边框
				int bubbleTriangleWidth = 6;//消息气泡小箭头直角宽度
				int bubbleTriangleHeight = 10;//消息气泡小箭头直角高度
				int bubbleTriangleY = 2;//消息气泡小箭头上边距
				int arcWidth = 15;//背景圆角度
				int arcHeight = 15;//背景圆角度
				Component component = null;
				List<Component> componentList = MessageUtil.getComponentList(message, Constants.MSG_TYPE_FILE);
				if (CollectionUtil.isNotEmpty(componentList)) {
					component = componentList.get(0);
				}
				if (component instanceof ChatRoomComponent) {
					Rectangle bounds = ((ChatRoomComponent) component).getContentPanel().getBounds();
					rec.width = bounds.width + border;
					rec.height = bounds.height;
					rec.x = bounds.x + getInsets().right;
					border = 0;
					bubbleTriangleY = 5;
				} else {
				}
				// 绘制消息发出者头像  
				if (message.isAvatar) {
					BalloonMessage balloonHeadMessage = (BalloonMessage) message;
					ImageIcon avatar = balloonHeadMessage.getAvatar();
					if (avatar != null) {
						if (message.getAlignment() == BalloonMessage.ALIGNMENT_RIGHT) { 						//BalloonMessage.ALIGNMENT_LEFT   change May 17 Avatar will flip 
							// 头像靠左显示  
							g2D.drawImage(avatar.getImage(), 9, rec.y - 25, HEADSIZE, HEADSIZE, null);
						} else {
							g2D.drawImage(avatar.getImage(), this.getWidth() - HEADSIZE - 9, rec.y - 25, HEADSIZE, HEADSIZE, null);
						}
					}
				}
				//文字区域
				Rectangle textRect = new Rectangle(rec.x - border, rec.y - border, rec.width + 2 * border, rec.height + 2 * border);
				message.setTextRect(textRect);

				// 绘制消息泡泡背景
				int[] padding = null;
				if (message.isBackground) {
					String backgroundBalloon = message.getBackgroundBalloon();
					//					backgroundBalloon = "[3]";
					Balloon balloon = BalloonManager.getInstance().getBalloon(backgroundBalloon);
					if (balloon != null) {
						NinePatch ninePatch = message.getAlignment() == BalloonMessage.ALIGNMENT_RIGHT ? balloon.getL() : balloon.getR();	//////////////BalloonMessage.ALIGNMENT_LEFT
						padding = ninePatch.getChunk().getPadding();
						if (padding[0] > TEXTPANE_BORDER - HEADSIZE) {
							padding[0] = TEXTPANE_BORDER - HEADSIZE;
						}

						if (padding[1] > 0) {
							padding[1] = 0;
						}
						if (padding[2] > TEXTPANE_BORDER) {
							padding[2] = TEXTPANE_BORDER;
						}
						if (padding[3] > 10) {
							padding[3] = 10;
						}

						ninePatch.draw(g2D, textRect.x - padding[0], textRect.y, textRect.width + padding[0] + padding[2], textRect.height + padding[3]);
					} else {
						Color color = null;
						if (backgroundBalloon != null) {
							color = LAF.getColor(Integer.valueOf(backgroundBalloon));
						}
						if (color == null) {
							color = TextPanePainter.getColor(message);
						}
						Color borderColor = ColorUtil.darker(color, 10);

						// 绘制自己消息圆角消息矩形  
						if (mousePoint != null && textRect.contains(mousePoint)) {
							color = ColorUtil.darker(color, 10);
						}
						//绘制背景
						TextPanePainter.drawColorBalloon(g2D, message, rec, border, bubbleTriangleWidth, bubbleTriangleHeight, bubbleTriangleY, arcWidth, arcHeight, textRect, color, borderColor);
					}
				}

				//绘制busy图标
				int buttonStatus = message.getStatus();
				ImageIcon imageIcon = null;
				if (buttonStatus == BalloonMessage.STATUS_YES) {

				} else if (buttonStatus == BalloonMessage.STATUS_NO) {
					imageIcon = LAF.getMsgLoadFail();
				} else {
					imageIcon = LAF.getMsgLoading();
				}

				Rectangle iconRec = null;
				int ICON_W = 20;
				int oo = 8;
				if (message.getAlignment() == BalloonMessage.ALIGNMENT_LEFT) { // BalloonMessage.ALIGNMENT_RIGHT
					if (padding != null) {
						oo = padding[0];
					}
					iconRec = new Rectangle(rec.x - oo - bubbleTriangleWidth - ICON_W, rec.y + rec.height / 2 - ICON_W / 2, ICON_W, ICON_W);
				} else {// 消息发出者是自己，则头像靠右显示  
					if (padding != null) {
						oo = padding[2];
					}
					iconRec = new Rectangle(rec.x + rec.width + oo + bubbleTriangleWidth, rec.y + rec.height / 2 - ICON_W / 2, ICON_W, ICON_W);
				}

				boolean flag = true;
				if (imageIcon != null) {
					g2D.drawImage(imageIcon.getImage(), iconRec.x, iconRec.y, iconRec.width, iconRec.height, this);

					if (buttonStatus == BalloonMessage.STATUS_NO && mousePoint != null && iconRec.contains(mousePoint)) {
						g2D.setColor(Color.GRAY);
						g2D.drawRect(iconRec.x - 1, iconRec.y - 1, iconRec.width + 1, iconRec.height + 1);
					} else {
						flag = false;//便于后边设置为空
					}
					if (flag) {
						message.setButtonRect(iconRec);
					} else {
						message.setButtonRect(null);
					}
				}
				if (imageIcon == null && StringUtil.isNotBlank(message.getSpecMsg())) {//绘制语音文件或视频文件时长
					g2D.setColor(Color.DARK_GRAY);
					g2D.drawString(message.getSpecMsg(), iconRec.x + 5, iconRec.y + iconRec.height / 2);
				}

			} // while  
		}
		super.paintComponent(g); // 执行默认组件绘制（消息文本、图片以及段落显示等内容）  
	}

	public int getBalloonMessageIndex(BalloonMessage message) {
		int len = 0;
		synchronized (lock) {
			for (BalloonMessage m : messages) {
				if (m == message) {
					break;
				} else {
					len += m.getLength();
				}
			}
		}
		return len;
	}

	public Object[] getBalloonMessageIndex(long messageId) {
		int len = 0;
		BalloonMessage balloonMessage = null;
		synchronized (lock) {
			for (BalloonMessage m : messages) {
				if (m.getId() == messageId) {
					balloonMessage = m;
					break;
				} else {
					len += m.getLength();
				}
			}
		}
		return new Object[] { len, balloonMessage };
	}

	public Rectangle getMessageRectangle(BalloonMessage message) {
		int balloonMessageLength = getBalloonMessageIndex(message);
		try {
			int textStartPosition = balloonMessageLength + message.getTextStartPosition();
			int textEndPosition = balloonMessageLength + message.getTextEndPosition();
			Rectangle modelToView = modelToView(textStartPosition);
			if (modelToView == null)
				return null;
			int x1 = modelToView.x;
			int y1 = modelToView.y;
			int x2 = modelToView.x + modelToView.width;
			int y2 = modelToView.y + modelToView.height;
			for (int i = textStartPosition + 1; i <= textEndPosition; i++) {
				Rectangle v = modelToView(i);
				if (v == null) {
					return null;
				}

				x1 = Math.min(x1, v.x);
				y1 = Math.min(y1, v.y);
				x2 = Math.max(x2, v.x + v.width);
				y2 = Math.max(y2, v.y + v.height);
			}

			return new Rectangle(x1, y1, x2 - x1, y2 - y1);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return null;
	}

	public BalloonMessage getBalloonMessage(int pos) {
		int len = 0;
		synchronized (lock) {
			for (BalloonMessage m : messages) {
				if (len < pos && pos < m.getLength() + len) {
					return m;
				}
				len += m.getLength();
			}
		}
		return null;
	}

	public BalloonMessage getMouseBalloonMessage() {
		synchronized (lock) {
			for (BalloonMessage m : messages) {
				Rectangle textRect = m.getTextRect();
				if (mousePoint != null && textRect != null && textRect.contains(mousePoint)) {
					return m;
				}
			}
		}
		return null;
	}

	public boolean hasInitMessage() {
		synchronized (lock) {
			for (BalloonMessage m : messages) {
				if (m.getStatus() == BalloonMessage.STATUS_INIT) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isInitMessage(long id) {
		synchronized (lock) {
			for (BalloonMessage m : messages) {
				if (m.getId() == id && m.getStatus() == BalloonMessage.STATUS_INIT) {
					return true;
				}
			}
		}
		return false;
	}

	public void createDocumentStyles() {
		Style defaultStyle = sc.getStyle(StyleContext.DEFAULT_STYLE);

		// Create and add the main document style
		Style mainStyle = sc.addStyle("MainStyle", defaultStyle);
		StyleConstants.setFontFamily(mainStyle, "serif");
		StyleConstants.setFontSize(mainStyle, 12);

		Style leftStyle = sc.addStyle(STYLE_LEFT_MSG, null);
		StyleConstants.setFontFamily(leftStyle, "serif");
		StyleConstants.setFontSize(leftStyle, 18);
		StyleConstants.setAlignment(leftStyle, StyleConstants.ALIGN_LEFT);
		StyleConstants.setLeftIndent(leftStyle, 10);

		Style rightStyle = sc.addStyle(STYLE_RIGHT_MSG, null);
		StyleConstants.setFontFamily(rightStyle, "serif");
		StyleConstants.setFontSize(rightStyle, 18);
		StyleConstants.setAlignment(rightStyle, StyleConstants.ALIGN_RIGHT);

		StyleConstants.setRightIndent(rightStyle, 10);

		Style centerStyle = sc.addStyle(STYLE_CENTER_MSG, null);
		StyleConstants.setFontFamily(centerStyle, "serif");
		StyleConstants.setFontSize(centerStyle, 18);
		StyleConstants.setAlignment(centerStyle, StyleConstants.ALIGN_CENTER);

		Style leftNameStyle = sc.addStyle(STYLE_LEFT_NAME, null);
		StyleConstants.setFontFamily(leftNameStyle, "serif");
		StyleConstants.setFontSize(leftNameStyle, 12);
		StyleConstants.setAlignment(leftNameStyle, StyleConstants.ALIGN_LEFT);
		StyleConstants.setForeground(leftNameStyle, Color.LIGHT_GRAY);
		StyleConstants.setSpaceAbove(leftNameStyle, 10);
		StyleConstants.setSpaceBelow(leftNameStyle, 10);

		Style rightNameStyle = sc.addStyle(STYLE_RIGHT_NAME, null);
		StyleConstants.setFontFamily(rightNameStyle, "serif");
		StyleConstants.setFontSize(rightNameStyle, 12);
		StyleConstants.setAlignment(rightNameStyle, StyleConstants.ALIGN_RIGHT);
		StyleConstants.setForeground(rightNameStyle, Color.LIGHT_GRAY);
		StyleConstants.setSpaceAbove(rightNameStyle, 10);
		StyleConstants.setSpaceBelow(rightNameStyle, 10);

		Style centerNameStyle = sc.addStyle(STYLE_CENTER_NAME, null);
		StyleConstants.setFontFamily(centerNameStyle, "serif");
		StyleConstants.setFontSize(centerNameStyle, 12);
		StyleConstants.setAlignment(centerNameStyle, StyleConstants.ALIGN_CENTER);
		StyleConstants.setForeground(centerNameStyle, Color.LIGHT_GRAY);
		StyleConstants.setSpaceAbove(centerNameStyle, 10);
		StyleConstants.setSpaceBelow(centerNameStyle, 10);

		Style lineStyle = sc.addStyle(STYLE_LINE, null);
		StyleConstants.setFontSize(lineStyle, 12);
	}

	// Style names
	public static final String STYLE_LINE = "LineStyle";

	public static final String STYLE_LEFT_NAME = "LeftNameStyle";

	public static final String STYLE_RIGHT_NAME = "RightNameStyle";

	public static final String STYLE_CENTER_NAME = "CenterNameStyle";

	public static final String STYLE_LEFT_MSG = "LeftStyle";

	public static final String STYLE_RIGHT_MSG = "RightStyle";

	public static final String STYLE_CENTER_MSG = "CenterStyle";
	/**
	 * The default Hand cursor.
	 */
	public static final Cursor HAND_CURSOR = new Cursor(Cursor.HAND_CURSOR);

	/**
	 * The default Text Cursor.
	 */
	public static final Cursor DEFAULT_CURSOR = new Cursor(Cursor.DEFAULT_CURSOR);
}

class TalkStyledEditorKit extends StyledEditorKit {
	public Object clone() {
		return new TalkStyledEditorKit();
	}

	public ViewFactory getViewFactory() {
		return defaultFactory;
	}

	/* The extended view factory */
	static class ExtendedStyledViewFactory implements ViewFactory {
		public View create(Element elem) {
			String kind = elem.getName();
			if (kind != null) {
				if (kind.equals(AbstractDocument.ContentElementName)) {
					return new TalkLabelView(elem);
				} else if (kind.equals(AbstractDocument.ParagraphElementName)) {
					return new TalkParagraphView(elem);
				} else if (kind.equals(AbstractDocument.SectionElementName)) {
					return new BoxView(elem, View.Y_AXIS);
				} else if (kind.equals(StyleConstants.ComponentElementName)) {
					return new ComponentView(elem);
				} else if (kind.equals(StyleConstants.IconElementName)) {
					return new IconView(elem);
				}
			}
			// Delegate others to StyledEditorKit
			return styledEditorKitFactory.create(elem);
		}

	}

	protected static View getParagraphView(Element elem) {
		return new ParagraphView(elem);
	}

	private static final ViewFactory styledEditorKitFactory = (new StyledEditorKit()).getViewFactory();

	private static final ViewFactory defaultFactory = new ExtendedStyledViewFactory();
}

class TalkParagraphView extends ParagraphView {
	private Color borderColor;
	protected int alignment = StyleConstants.ALIGN_CENTER;
	// Attribute cache
	protected Color bgColor; // Background color, or null for transparent.

	protected Border border; // Border, or null for no border

	protected static final Border smallBorder = BorderFactory.createEmptyBorder(0, 0, 0, 0);

	public TalkParagraphView(Element elem) {
		super(elem);
	}

	//	r.minimum = 0;解决 控件显示不全和头像不显示问题
	protected SizeRequirements calculateMinorAxisRequirements(int axis, SizeRequirements r) {
		SizeRequirements x = super.calculateMinorAxisRequirements(axis, r);
		x.minimum = 0;
		return x;
	}

	protected void layoutMinorAxis(int targetSpan, int axis, int[] offsets, int[] spans) {
		int n = getViewCount();
		int m = 0;
		for (int i = 0; i < n; i++) {
			View v = getView(i);
			m = Math.max(m, (int) v.getMaximumSpan(axis));
		}
		for (int i = 0; i < n; i++) {
			View v = getView(i);
			int max = (int) v.getMaximumSpan(axis);
			if (max < targetSpan) {
				// can't make the child this wide, align it
				float align = v.getAlignment(axis);
				offsets[i] = (int) ((targetSpan - m) * align);
				spans[i] = max;
			} else {
				// make it the target width, or as small as it can get.
				int min = (int) v.getMinimumSpan(axis);
				offsets[i] = 0;
				spans[i] = Math.max(min, targetSpan);
			}
		}
	}
}

class TalkLabelView extends LabelView {
	public TalkLabelView(Element elem) {
		super(elem);
	}

	public float getMinimumSpan(int axis) {
		switch (axis) {
		case View.X_AXIS:
			return 0;
		case View.Y_AXIS:
			return super.getMinimumSpan(axis);
		default:
			throw new IllegalArgumentException("Invalid axis: " + axis);
		}
	}

	public int getBreakWeight(int axis, float pos, float len) {
		return GoodBreakWeight;
	}

	public View breakView(int axis, int p0, float pos, float len) {
		if (axis == View.X_AXIS) {
			checkPainter();
			int p1 = getGlyphPainter().getBoundedPosition(this, p0, pos, len);
			if (p0 == getStartOffset() && p1 == getEndOffset()) {
				return this;
			}
			return createFragment(p0, p1);
		}
		return this;
	}

}
