package com.xinwei.talk.ui.chat.component.file;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FileDialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillLine;
import com.xinwei.common.lookandfeel.component.McWillTextPane;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.ui.chat.balloon.BalloonMessage;
import com.xinwei.talk.ui.chat.relay.RelayDialog;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.chat.room.file.ReceiveFileUI;
import com.xinwei.talk.ui.util.component.label.LinkLabel;
import com.xinwei.talk.ui.util.component.room.ChatRoomComponent;

public class ReceiveFileCancelUI extends ChatRoomComponent {
	private static final long serialVersionUID = -4403839897649365671L;

	int FILE_NAME_MAX_LENGTH = 200;
	protected JLabel fileIconLabel;
	//	protected Color leftColor1 = LAF.getColor(188, 237, 245);
	//	protected Color leftBorderColor = LAF.getColor(155, 155, 155);
	protected Color leftColor1 = Color.WHITE;
	protected Color leftBorderColor = LAF.getColor(155, 155, 155);
	protected McWillTextPane fileNameTextPane;

	protected LinkLabel acceptLabel;
	protected LinkLabel saveAsLabel;
	protected LinkLabel forwardLabel;

	private TalkFileSpecific fileMsg;
	
	private TalkMessage talkMessage;
	protected BalloonMessage balloonMsg;

	private CommonChatRoom chatRoom;


		
	public ReceiveFileCancelUI(BalloonMessage balloonMsg, TalkFileSpecific fileMsg) {
		
		
		this.balloonMsg = balloonMsg;																			// added may 17
		
		this.fileMsg = fileMsg;
		this.chatRoom = (CommonChatRoom) TalkManager.getChatManager().getChatContainer().getActiveChatRoom();

		setLeftColor(LAF.getColor(188, 237, 245));
		setLeftBorderColor(LAF.getColor(155, 155, 155));

		fileIconLabel = new JLabel();

		fileNameTextPane = new McWillTextPane();
		fileNameTextPane.setEditable(false);

		acceptLabel = new LinkLabel(Res.getMessage("button.accept"), Color.blue, Color.red);
		saveAsLabel = new LinkLabel(Res.getMessage("button.save.as"), Color.blue, Color.red);
		forwardLabel = new LinkLabel(Res.getMessage("button.forward"), Color.blue, Color.red);

		initMyData(chatRoom);

		addMyListeners();

		JPanel buttonPanel = new JPanel(new BorderLayout());
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(15, 0, 0, 0));

		JPanel downButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
		downButtonPanel.add(acceptLabel);
		downButtonPanel.add(saveAsLabel);
		downButtonPanel.add(forwardLabel);
		buttonPanel.add(new McWillLine(), BorderLayout.NORTH);
		buttonPanel.add(downButtonPanel, BorderLayout.CENTER);

		JPanel infoPanel = new JPanel(new BorderLayout());
		infoPanel.add(fileNameTextPane, BorderLayout.NORTH);

		contentPanel.add(fileIconLabel, BorderLayout.WEST);
		contentPanel.add(infoPanel, BorderLayout.CENTER);
		contentPanel.add(buttonPanel, BorderLayout.SOUTH);
		//		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 5));

	}

	private void initMyData(CommonChatRoom room) {
		String fileName = fileMsg.getFilename();
		fileIconLabel.setIcon(SwingUtil.getFileImageIcon(fileName));

		String text = SwingUtil.format(fileMsg.getLength());
		fileNameTextPane.setText(Res.getMessage("label.chatroom.filedownload.cancel") + " \n" + fileName + " (" + text + ")");

		//		filePathTextPane.setText(Res.getMessage("label.chatroom.filedownload.cancel"));
		//		filePathTextPane.setForeground(Color.gray);
	}

	protected void initData(final File file) {
	}

	private void addMyListeners() {
		acceptLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				acceptLabel.setEnabled(false);
				MouseListener[] mouseListeners = acceptLabel.getMouseListeners();
				for (MouseListener listener : mouseListeners) {
					acceptLabel.removeMouseListener(listener);
				}
				final TalkMessage talkMessage = balloonMsg.getTalkMessage();									// added may 17
				TalkManager.getTransferManager().receiveFile(talkMessage, chatRoom, fileMsg, true);
			}
		});
		
		saveAsLabel.addMouseListener(new MouseAdapter() {													// added may 17 from here
			public void mouseClicked(MouseEvent e) {
				 FileDialog fileDialog = new FileDialog(new Frame(), "Save", FileDialog.SAVE);
				 if( fileMsg.getFilename() != null){
				 		fileDialog.setFile( fileMsg.getFilename());
				 	}
				 	else{
			        fileDialog.setFile("");
				 	}
			        fileDialog.setVisible(true);
			        if(fileDialog.getDirectory() != null && fileDialog.getFile() != null){
			        	final TalkMessage talkMessage = balloonMsg.getTalkMessage();
			        TalkManager.getTransferManager().saveAsFile(talkMessage, chatRoom, fileMsg, true,(new File(fileDialog.getDirectory()+fileDialog.getFile())));
			        }

			}
		});
		

		forwardLabel.addMouseListener(new MouseAdapter() {										
			public void mouseClicked(MouseEvent e) {
				RelayDialog relayDialog = TalkManager.getChatFrame().getRelayDialog();
				if (relayDialog != null && relayDialog.isVisible()) {
					
					if (relayDialog.getBalloonMsg().getId() == balloonMsg.getId()) { 
						return;
					}
					relayDialog.setVisible(false);
				}


				relayDialog = new RelayDialog(balloonMsg);

				relayDialog.setVisible(true);
				TalkManager.getChatFrame().setRelayDialog(relayDialog);
				
			}
		});																						// to here
	}

}
