/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月3日 上午10:43:52
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room;

public class ChatRoomWorking {
	private String message;
	private ChatRoom chatRoom;

	public ChatRoomWorking(ChatRoom chatRoom) {
		this.chatRoom = chatRoom;
	}

	public ChatRoomWorking(ChatRoom chatRoom, String message) {
		this.chatRoom = chatRoom;
		this.message = message;
	}

	public boolean canClose() {
		return false;
	}

	public void close() {

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
