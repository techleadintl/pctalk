package com.xinwei.talk.ui.main.list.group.bak;
//package com.xinwei.talk.ui.main.list.group.bak;
//
//import java.awt.Image;
//import java.awt.event.KeyEvent;
//import java.awt.event.MouseEvent;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//
//import javax.swing.ImageIcon;
//import javax.swing.JPopupMenu;
//
//import com.xinwei.talk.action.XAction;
//import com.xinwei.talk.action.group.GroupDissolveAction;
//import com.xinwei.talk.action.group.GroupOpenRoomAction;
//import com.xinwei.talk.action.group.GroupQuitAction;
//import com.xinwei.talk.manager.TalkManager;
//import com.xinwei.talk.model.VCard;
//import com.xinwei.talk.model.Group;
//import com.xinwei.talk.model.Member;
//import com.xinwei.talk.plugin.ListItem;
//import com.xinwei.common.lang.CollectionUtil;
//
//public class GroupItem extends ListItem<Group> {
//	private static final long serialVersionUID = 1514044406550293152L;
//
//	public GroupItem(Group group) {
//		super(group);
//		
//		addListItemMouseListener(new XMouseListener());
//		addListItemKeyListener(new XKeyListener());
//		addChildrenMouseListener();
//		addChildrenKeyListener();
//	}
//
//	public void updateAvatar() {
//		McWillList2<Member> groupMembers = localManager.getMemberList(obj.getId());
//		if (CollectionUtil.isEmpty(groupMembers)) {
//			return;
//		}
//
//		McWillList2<Image> avatars = new ArrayList<Image>();
//		VCard vCard = TalkManager.getVCard();
//		String slefAvatarUrl = vCard.getPicture();
//
//		for (Member member : groupMembers) {
//			byte[] avatar = member.getAvatar();
//
//			if (avatar == null)
//				continue;
//
//			String avatarUrl = member.getAvatarUrl();
//			if (avatarUrl == null || avatarUrl.equals(slefAvatarUrl)) {
//				continue;
//			}
//
//			avatars.add(new ImageIcon(avatar).getImage());
//		}
//
//		images = avatars;
//
//		super.updateAvatar();
//	}
//
//	public String getDisplayName() {
//		final String displayName;
//		if (obj.getName() != null) {
//			displayName = obj.getName();
//		} else {
//			displayName = obj.getId();
//		}
//
//		if (displayName != null) {
//			return displayName;
//		} else {
//			return ""; // weird, but happens.
//		}
//	}
//
//	public void doubleClicked(GroupItem item) {
//		Group group = localManager.getGroup(item.getObject().getId());
//		if (group == null) {
//			return;
//		}
//		GroupOpenRoomAction action = new GroupOpenRoomAction();
//		action.setGroup(group);
//		action.actionPerformed(null);
//	}
//
//	public void firePopupEvent(MouseEvent e, GroupItem item) {
//		if (item == null) {
//			return;
//		}
//		Group group = localManager.getGroup(item.getObject().getId());
//
//		if (group == null)
//			return;
//
//		final JPopupMenu popup = new JPopupMenu();
//
//		//打开聊天窗口
//		XAction openAction = new GroupOpenRoomAction(group);
//		if (openAction.isVisible()) {
//			popup.add(openAction);
//		}
//		XAction delAction = new GroupDissolveAction(group);
//		if (delAction.isVisible()) {
//			popup.add(delAction);
//		}
//		XAction quitAction = new GroupQuitAction(group);
//		if (quitAction.isVisible()) {
//			popup.add(quitAction);
//		}
//
//		popup.show(e.getComponent(), e.getX(), e.getY());
//
//	}
//
//	public class XMouseListener implements ListItemMouseListener<GroupItem> {
//
//		@Override
//		public void mouseClicked(GroupItem item, MouseEvent e) {
//			if (e.getButton() == MouseEvent.BUTTON1) {
//				if (e.getClickCount() == 2) {
//					doubleClicked(item);
//				}
//			}
//		}
//
//		@Override
//		public void mousePressed(GroupItem item, MouseEvent e) {
//			if (!e.isPopupTrigger())
//				return;
//			firePopupEvent(e, item);
//		}
//
//		@Override
//		public void mouseReleased(GroupItem x, MouseEvent e) {
//
//		}
//
//		@Override
//		public void scrollBarMouseEntered(GroupItem x, MouseEvent e) {
//			// TODO Auto-generated method stub
//
//		}
//
//		@Override
//		public void mouseExited(GroupItem x, MouseEvent e) {
//
//		}
//	}
//
//	public class XKeyListener implements ListItemKeyListener<GroupItem> {
//
//		@Override
//		public void keyTyped(GroupItem x, KeyEvent e) {
//			// TODO Auto-generated method stub
//
//		}
//
//		@Override
//		public void keyPressed(GroupItem item, KeyEvent e) {
//			if (e.getKeyChar() == KeyEvent.VK_ENTER) {
//				doubleClicked(item);
//			}
//		}
//
//		@Override
//		public void keyReleased(GroupItem x, KeyEvent e) {
//			// TODO Auto-generated method stub
//
//		}
//
//	}
//
//}
