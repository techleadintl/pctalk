/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月21日 上午9:45:59
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room;

import java.awt.Font;
import java.util.Date;

import javax.swing.JLabel;

import org.jivesoftware.smack.packet.Message.Type;

import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;

public class TalkChatRoom extends CommonChatRoom {
	private static final long serialVersionUID = 1L;

	public TalkChatRoom(String roomId, String roomTitle, String roomDesc) {
		createRoom(true);
		
		this.roomId = roomId;
		this.roomTitle = roomTitle;
		this.roomDesc = roomDesc;
		// Add VCard Panel
		addVcardPanel();
	}

	protected void addVcardPanel() {
		JLabel label = getInfoComponent();
		if (label != null) {
			getToolBar().addInfoComponent(label);
		}
	}

	protected JLabel getInfoComponent() {
		final JLabel label = new JLabel();
		label.setFont(new Font("Dialog", Font.BOLD, 14));
		label.setText(getRoomTitle());
		return label;
	}

	@Override
	public void leaveChatRoom() {
		// TODO Auto-generated method stub
	}

	@Override
	public Type getChatType() {
		return Type.chat;
	}

	@Override
	public void updateStatus(boolean active) {
		// TODO Auto-generated method stub

	}

	public String getMessageFrom() {
		return TalkManager.getVCard().getTel();
	}

	public String getMessageTo() {
		return TalkUtil.getUserTel(getRoomId());
	}

	@Override
	public TalkMessage getTalkMessage(String packetId, TalkBody bodyMessage, int transcriptMessageType, Date time) {
		TalkMessage m = new TalkMessage();
		m.setId(TalkManager.getMessageManager().nextMessageId());
		m.setJid(getRoomId());
		m.setSendOrReceive(transcriptMessageType);
		m.setReaded(TalkMessage.STATUS_READED);
		m.setIsGroup(false);
		
		m.setTalkBody(bodyMessage);
		bodyMessage.setTime(time);
		bodyMessage.setPacketId(packetId);
		return m;
	}
}
