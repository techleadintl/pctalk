package com.xinwei.talk.ui.util.component.list;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.ui.util.component.avatar.AvatarPanel2;

public class MemberListCellPanel extends JPanel {
	private AvatarPanel2 avatarPanel;
	private JLabel nameLabel;
	private JLabel specialLabel;
	private Member member;

	public MemberListCellPanel(Member member) {

		setOpaque(true);

		setLayout(new BorderLayout());

		setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

		createUI();

		updateData();

	}

	private void createUI() {
		avatarPanel = new AvatarPanel2();

		nameLabel = new JLabel();
		nameLabel.setFont(LAF.getFont(13));

		specialLabel.setFont(LAF.getFont(10));
		specialLabel.setForeground(Color.WHITE);
		specialLabel.setHorizontalAlignment(SwingConstants.CENTER);

		nameLabel.setPreferredSize(new Dimension(20, 20));
		specialLabel.setPreferredSize(new Dimension(20, 20));
		avatarPanel.setPreferredSize(new Dimension(50, 50));

		add(nameLabel, BorderLayout.CENTER);
		add(avatarPanel, BorderLayout.WEST);
		add(specialLabel, BorderLayout.EAST);
	}

	public void updateData() {
		int memRole = member.getMemRole();

		if (memRole == Member.ROLE_ADMIN) {
			specialLabel.setText("A");
		}

		String memJid = member.getMemJid();
		nameLabel.setText(TalkUtil.getDisplayNameMember(member.getGid(),memJid));

		avatarPanel.setJid(memJid);

	}

	protected Color DEFAULT_COLOR = new Color(240, 240, 240);

	protected Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 11);

}