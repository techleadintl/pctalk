/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月14日 下午7:47:20
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.balloon;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.ImageIcon;

import com.android.ninepatch.NinePatch;
import com.xinwei.common.ui.vo.chat.Balloon;
import com.xinwei.talk.common.LAF;

public class BalloonManager {
	private static BalloonManager singleton;
	private Map<String, Balloon> ninePathcMap = new LinkedHashMap<String, Balloon>();

	public static synchronized BalloonManager getInstance() {
		if (null == singleton) {
			singleton = new BalloonManager();
		}
		return singleton;
	}

	public Collection<Balloon> getBalloons() {
		return ninePathcMap.values();
	}

	public Balloon getBalloon(String description) {
		return ninePathcMap.get(description);
	}

	private BalloonManager() {
		Map<String[], String> map = new LinkedHashMap<>();
		map.put(new String[] { "talk/images/balloon/1.l.9.png"/* 左侧 */
				, "talk/images/balloon/1.r.9.png"/* 右侧 */ , "talk/images/balloon/1.png"/* 选择 */ }, "[1]");
		map.put(new String[] { "talk/images/balloon/2.l.9.png"/* 左侧 */
				, "talk/images/balloon/2.r.9.png"/* 右侧 */ , "talk/images/balloon/2.png"/* 选择 */ }, "[2]");
		map.put(new String[] { "talk/images/balloon/3.l.9.png"/* 左侧 */
				, "talk/images/balloon/3.r.9.png"/* 右侧 */ , "talk/images/balloon/3.png"/* 选择 */ }, "[3]");
		//		
		//		//TODO:是否中文
		//		Locale locale = Locale.getDefault();
		//		if (locale.getLanguage().equals("zh")) {
		//			
		//		}

		Set<Entry<String[], String>> entrySet = map.entrySet();
		for (Entry<String[], String> entry : entrySet) {
			try {
				NinePatch mPatchL = NinePatch.load(LAF.getURL(entry.getKey()[0]), false /* convert */);
				NinePatch mPatchR = NinePatch.load(LAF.getURL(entry.getKey()[1]), false /* convert */);
				ImageIcon bk = new ImageIcon(LAF.getURL(entry.getKey()[2]));
				ninePathcMap.put(entry.getValue(), new Balloon(entry.getValue(), mPatchL, mPatchR, bk));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
