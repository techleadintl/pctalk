package com.xinwei.talk.ui.main.list.group.bak;
//package com.xinwei.plugin.group;
//
//import java.awt.Color;
//import java.awt.Dimension;
//import java.awt.GradientPaint;
//import java.awt.Graphics;
//import java.awt.Graphics2D;
//import java.awt.Paint;
//import java.awt.Rectangle;
//import java.awt.RenderingHints;
//import java.awt.geom.Rectangle2D;
//import java.util.List;
//
//import javax.swing.ImageIcon;
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//
//import org.jivesoftware.spark.util.GraphicUtils;
//
//import com.xinwei.talk.lookandfeel.McWillBorderFactory;
//import com.xinwei.talk.lookandfeel.layout.TileLayout;
//import com.xinwei.talk.lookandfeel.util.SwingUtil;
//import com.xinwei.util.UIUtil;
//
//public class GroupAvatarPanel extends JPanel {
//	private static final int GAP = 2;
//
//	private Dimension _SIZE9 = null;
//	private Dimension _SIZE4 = null;
//
//	public GroupAvatarPanel(int WIDTH) {
//		setOpaque(false);
//
//		setMinimumSize(new Dimension(WIDTH, WIDTH));
//		setMaximumSize(new Dimension(WIDTH, WIDTH));
//		setPreferredSize(new Dimension(WIDTH, WIDTH));
//		WIDTH -= 2;
//
//		_SIZE9 = new Dimension((WIDTH - 2 * GAP) / 3, (WIDTH - 2 * GAP) / 3);
//
//		_SIZE4 = new Dimension((WIDTH - 1 * GAP) / 2, (WIDTH - 1 * GAP) / 2);
//	}
//
//	public void updateAvatar(McWillList2<byte[]> icons) {
//		removeAll();
//
//		int num = icons.size();
//		Dimension SIZE = _SIZE9;
//		if (num <= 4) {
//			num = 4;
//			SIZE = _SIZE4;
//		} else {
//			num = 9;
//		}
//		setLayout(new TileLayout(GAP, GAP));
//		for (int i = 0; i < num; i++) {
//			JLabel label = new JLabel();
//			label.setPreferredSize(SIZE);
//			label.setOpaque(true);
//			add(label);
//
//			//			Image image = icons.get(i).getImage();
//			//			Image scaledInstance = image.getScaledInstance(SIZE.width, SIZE.height, Image.SCALE_DEFAULT);
//			//			scaledInstance.flush();
//			//			ImageIcon icon2 = new ImageIcon(scaledInstance);
//
//			if (i < icons.size()) {
//				ImageIcon icon2 = GraphicUtils.scale(new ImageIcon(icons.get(i)), SIZE.width, SIZE.height);
//				label.setIcon(icon2);
//				//				label.setBorder(McWillBorderFactory.getInstance().getLabelBorder(Color.GRAY, 0));
//			}
//		}
//	}
//
//	@Override
//	protected void paintComponent(Graphics g) {
//		super.paintComponent(g);
//		if (unreadMessageCount < 1)
//			return;
//
//		Graphics2D g2d = (Graphics2D) g;
//		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
//
//		final int borderW = 2;
//		String unreadMessageCountStr = String.valueOf(unreadMessageCount);
//		if (unreadMessageCountStr.length() > 2) {
//			unreadMessageCountStr = "...";
//		}
//		Rectangle bounds = avatarPanel.getBounds();
//
//		int w = 16;
//		int h = 16;
//		int x = bounds.x + bounds.width - w + 3;
//		//		int x =this.getWidth() - w - 3;
//		int y = borderW;
//
//		Paint paint = g2d.getPaint();
//		GradientPaint gp = new GradientPaint(x, y, Color.RED, w, h, Color.RED);
//		g2d.setPaint(gp);
//		g2d.fillOval(x, y, w, h);
//		g2d.setPaint(paint);
//
//		g2d.setColor(Color.WHITE);
//
//		g2d.setFont(UIUtil.getDefaultFont());
//		Rectangle2D stringBounds = SwingUtil.getStringBounds(unreadMessageCountStr, g2d.getFont());
//
//		g2d.drawString(unreadMessageCountStr, x + (int) (w - stringBounds.getWidth()) / 2, 14);
//	}
//
//}
