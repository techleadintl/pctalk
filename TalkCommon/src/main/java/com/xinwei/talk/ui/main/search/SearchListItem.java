package com.xinwei.talk.ui.main.search;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.component.McWillScrollPaneLabel;
import com.xinwei.talk.model.XWObject;
import com.xinwei.talk.ui.util.component.avatar.AvatarPanel2;

public class SearchListItem extends JPanel {
	private static final long serialVersionUID = 1514044406550293152L;

	private SearchItemInfo searchItemInfo;

	//显示名称 文字标签
	protected JLabel textabel;

	//状态 图标标签
	protected AvatarPanel2 avatarPanel;

	protected int iconSize;

	public SearchListItem(SearchItemInfo itemInfo) {
		this.searchItemInfo = itemInfo;
		avatarPanel = new AvatarPanel2(true);
		if (itemInfo.obj instanceof XWObject) {
			avatarPanel.setJid(((XWObject)itemInfo.obj).getJid());
		} else {
			return;
		}

		avatarPanel.setPreferredSize(new Dimension(45, 45));

		setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 10));

		setLayout(new BorderLayout());

		textabel = new McWillScrollPaneLabel();
		textabel.setHorizontalTextPosition(JLabel.LEFT);
		textabel.setHorizontalAlignment(JLabel.LEFT);

		add(avatarPanel, BorderLayout.WEST);
		add(textabel, BorderLayout.CENTER);

		setDisplayText();
	}

	public String getJID() {
		return searchItemInfo.jid;
	}

	protected void setDisplayText() {
		String displayName = getDisplayName();
		String displayId = getDisplayId();
		textabel.setText(displayName);
		//		if (StringUtil.isNotEmpty(displayId) && !StringUtil.equals(displayName, displayId)) {
		//			displayName = displayName + "(" + displayId + ")";
		//		}
		//		textabel.setToolTipText(displayName);
	}

	public String getDisplayName() {
		return searchItemInfo.displayName;
	}

	public String getDisplayId() {
		final String displayId = searchItemInfo.displayId;
		if (displayId == null) {
			return "";
		} else {
			return displayId;
		}
	}

	public SearchItemInfo getSearchItemInfo() {
		return searchItemInfo;
	}
}
