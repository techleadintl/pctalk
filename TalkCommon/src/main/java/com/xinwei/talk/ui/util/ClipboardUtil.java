package com.xinwei.talk.ui.util;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import com.xinwei.common.lang.Callback;
import com.xinwei.common.lookandfeel.component.McWillImageDialog;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.chat.component.ImageComponent;

import sun.awt.image.ToolkitImage;

public class ClipboardUtil {
	private static Clipboard cli;

	private ClipboardUtil() {
		// TODO Auto-generated constructor stub
	}

	public static void copy(JTextPane textPane) {
		///////////////////
		StyledDocument styledDocument = textPane.getStyledDocument();
		int selectionStart = textPane.getSelectionStart();
		int selectionEnd = textPane.getSelectionEnd();

		StringBuilder sb = new StringBuilder();
		final List<Object> imageList = new ArrayList<>();
		try {
			for (int i = selectionStart; i < selectionEnd; i++) {
				if (styledDocument.getCharacterElement(i).getName().equals(StyleConstants.ComponentElementName)) {
					//第 i位置插入了圖片
					Component component = StyleConstants.getComponent(styledDocument.getCharacterElement(i).getAttributes());
					if (!(component instanceof ImageComponent)) {
						continue;
					}
					String txt = sb.toString();
					if (txt.length() > 0) {
						imageList.add(sb.toString());
						sb = new StringBuilder();
					}

					ImageComponent label = (ImageComponent) component;
					ImageIcon icon = (ImageIcon) label.getHoldIcon();
					Image image = icon.getImage();
					if (image instanceof ToolkitImage) {
						image = ((ToolkitImage) image).getBufferedImage();
					}
					imageList.add(image);
				} else {
					sb.append(styledDocument.getText(i, 1));
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
		}
		String txt = sb.toString();
		if (txt.length() > 0) {
			imageList.add(sb.toString());
		}
		if (imageList.isEmpty()) {
			return;
		}

		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

		clipboard.setContents(new Transferable() {
			List<DataFlavor> dfList = new ArrayList<DataFlavor>();

			{
				int i = 0;
				for (Object obj : imageList) {
					if (obj instanceof String) {
						dfList.add(new StringFlavor(i++));
					} else if (obj instanceof Image) {
						dfList.add(new ImageFlavor(i++));
					}
				}
			}

			@Override
			public boolean isDataFlavorSupported(DataFlavor flavor) {
				return dfList.contains(flavor);
			}

			@Override
			public DataFlavor[] getTransferDataFlavors() {
				DataFlavor[] array = dfList.toArray(new DataFlavor[0]);
				return array;
			}

			@Override
			public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
				int index = dfList.indexOf(flavor);
				if (index != -1) {
					return imageList.get(index);
				}
				return null;
			}
		}, null);

	}

	public static void cut(JTextPane textPane) {
		copy(textPane);
		textPane.replaceSelection("");
	}

	public static void paste(JTextPane textPane, Callback callback) {
		try {
			Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
			DataFlavor[] dataFlavors = transferable.getTransferDataFlavors();

			for (int count = 0; count < dataFlavors.length; count++) {
				if (DataFlavor.stringFlavor == dataFlavors[count]) {
					Object object = transferable.getTransferData(dataFlavors[count]);
					if (object instanceof String) {
						textPane.replaceSelection(String.valueOf(object));
					}
				} else {
					Object object = transferable.getTransferData(dataFlavors[count]);
					callback.call(object);
				}
				//				System.out.println(dataFlavors[count]);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public static boolean hasContent() {
		try {
			Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
			DataFlavor[] dataFlavors = transferable.getTransferDataFlavors();

			for (int count = 0; count < dataFlavors.length; count++) {
				if (DataFlavor.stringFlavor == dataFlavors[count]) {
					Object object = transferable.getTransferData(dataFlavors[count]);
					if (object instanceof String) {
						return true;
					}
				} else {
					Object object = transferable.getTransferData(dataFlavors[count]);
					if (object instanceof String) {
						return true;
					} else if (object instanceof Image) {
						return true;
					}
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return false;
	}

	public static void writeToClipboard(String str) {
		if (str != null) {
			cli = Toolkit.getDefaultToolkit().getSystemClipboard();
			Transferable text = new StringSelection(str);
			cli.setContents(text, null);
		}
	}

	public static void writeToClipboard(Image image) {
		if (image instanceof ToolkitImage) {
			image = ((ToolkitImage) image).getBufferedImage();
		}
		cli = Toolkit.getDefaultToolkit().getSystemClipboard();
		cli.setContents(new ImageChange(image), null);
	}

	public static void writeToFile(Image image) {
		try {
			JFileChooser jfc = new McWillImageDialog(false);//创建文件选择器
			jfc.setDialogTitle(Res.getMessage("button.chatroom.chat.picture"));

			jfc.addChoosableFileFilter(new PNGfilter());
			jfc.addChoosableFileFilter(new JPGfilter());
			jfc.addChoosableFileFilter(new Giffilter());
			jfc.addChoosableFileFilter(new BMPfilter());
			if (jfc.showSaveDialog(TalkManager.getChatFrame()) == JFileChooser.APPROVE_OPTION) {
				File file = jfc.getSelectedFile();
				String about = "png";
				String ext = file.toString().toLowerCase();
				FileFilter ff = jfc.getFileFilter();
				if (ff instanceof BMPfilter) {
					if (!ext.endsWith(".bmp")) {
						String ns = ext + ".bmp";
						file = new File(ns);
						about = "bmp";
					}
				} else if (ff instanceof PNGfilter) {
					if (!ext.endsWith(".png")) {
						String ns = ext + ".png";
						file = new File(ns);
						about = "png";
					}
				} else if (ff instanceof JPGfilter) {
					if (!ext.endsWith(".jpg")) {
						String ns = ext + ".jpg";
						file = new File(ns);
						about = "jpg";
					}
				} else if (ff instanceof Giffilter) {
					if (!ext.endsWith(".gif")) {
						String ns = ext + ".gif";
						file = new File(ns);
						about = "gif";
					}
				}
				int w = image.getWidth(null);
				int h = image.getHeight(null);
				int type = BufferedImage.TYPE_INT_RGB; // other options
				BufferedImage dest = new BufferedImage(w, h, type);
				Graphics g = dest.getGraphics();
				g.drawImage(image, 0, 0, null);
				g.dispose();
				ImageIO.write(dest, about, file);
				// if (ImageIO.write(icon, about, file)) {
				// JOptionPane.showMessageDialog(this, "保存成功！");
				// save.setEnabled(false);
				// } else
				// JOptionPane.showMessageDialog(this, "保存失败！");
			}
		} catch (Exception exe) {
			exe.printStackTrace();
		}
	}

	public String getSysClipboardText() {
		String ret = "";
		cli = Toolkit.getDefaultToolkit().getSystemClipboard();
		// 获取剪切板中的内容
		Transferable clipTf = cli.getContents(null);
		if (clipTf != null) {
			// 检查内容是否是文本类型
			if (clipTf.isDataFlavorSupported(DataFlavor.stringFlavor)) {
				try {
					ret = (String) clipTf.getTransferData(DataFlavor.stringFlavor);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return ret;
	}

	static class ImageChange implements Transferable {
		private Image theImage;

		public ImageChange(Image image) {
			theImage = image;
		}

		public DataFlavor[] getTransferDataFlavors() {
			return new DataFlavor[] { DataFlavor.imageFlavor };
		}

		public boolean isDataFlavorSupported(DataFlavor flavor) {
			return flavor.equals(DataFlavor.imageFlavor);
		}

		public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
			if (flavor.equals(DataFlavor.imageFlavor)) {
				return theImage;
			} else {
				throw new UnsupportedFlavorException(flavor);
			}
		}
	}

	// 一个文件后缀名选择器
	static private class JPGfilter extends FileFilter {
		public boolean accept(File file) {
			return file.toString().toLowerCase().endsWith(".jpg") || file.toString().toLowerCase().endsWith(".jpeg") || file.isDirectory();
		}

		public String getDescription() {
			return "*.jpg,*.jpeg";
		}
	}

	static private class PNGfilter extends FileFilter {
		public boolean accept(File file) {
			return file.toString().toLowerCase().endsWith(".png") || file.isDirectory();
		}

		public String getDescription() {
			return "*.png";
		}
	}

	static private class BMPfilter extends FileFilter {
		public boolean accept(File file) {
			return file.toString().toLowerCase().endsWith(".bmp") || file.isDirectory();
		}

		public String getDescription() {
			return "*.bmp";
		}
	}

	static private class Giffilter extends FileFilter {
		public boolean accept(File file) {
			return file.toString().toLowerCase().endsWith(".gif") || file.isDirectory();
		}

		public String getDescription() {
			return "*.gif";
		}
	}

	static class StringFlavor extends DataFlavor {
		public int index;

		public StringFlavor(int index) {
			super(java.lang.String.class, "Unicode String");
			this.index = index;
		}

		@Override
		public boolean equals(DataFlavor that) {
			boolean equals = super.equals(that);
			if (equals && that instanceof StringFlavor) {
				equals = ((StringFlavor) that).index == this.index;
			}
			return equals;
		}
	}

	static class ImageFlavor extends DataFlavor {
		public int index;

		public ImageFlavor(int index) {
			super("image/x-java-image; class=java.awt.Image", "Image");
			this.index = index;
		}

		@Override
		public boolean equals(DataFlavor that) {
			boolean equals = super.equals(that);
			if (equals && that instanceof ImageFlavor) {
				equals = ((ImageFlavor) that).index == this.index;
			}
			return equals;
		}
	}
}
