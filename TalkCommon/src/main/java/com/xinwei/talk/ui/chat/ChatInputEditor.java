/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月20日 上午11:25:45
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.StyleConstants;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import com.xinwei.talk.manager.TalkManager;

/**
 * This is implementation of ChatArea that should be used as the sendField in any chat room implementation.
 */
public class ChatInputEditor extends ChatArea implements DocumentListener {
	private static final long serialVersionUID = -3085035737908538581L;
	private final UndoManager undoManager;
	private KeyStroke undoKeyStroke;
	private KeyStroke ctrlbackspaceKeyStroke;
	private KeyStroke escapeKeyStroke;

	/**
	 * Creates a new Default ChatSendField.
	 */
	public ChatInputEditor() {
		undoManager = new UndoManager();

		this.setDragEnabled(true);
		this.getDocument().addUndoableEditListener(undoManager);
		Action undo = new AbstractAction() {
			private static final long serialVersionUID = -8897769620508545403L;

			public void actionPerformed(ActionEvent e) {
				try {
					undoManager.undo();
				} catch (CannotUndoException cue) {
					// no more undoing for you
				}
			}
		};

		Action escape = new AbstractAction() {
			private static final long serialVersionUID = -2973535045376312313L;

			@Override
			public void actionPerformed(ActionEvent e) {
				TalkManager.getChatManager().getChatContainer().getActiveChatRoom().close(false);
			}

		};

		Action ctrlbackspace = new AbstractAction() {
			private static final long serialVersionUID = -2973535045376312313L;

			@Override
			public void actionPerformed(ActionEvent e) {

				// We have Text selected, remove it
				if (getSelectedText() != null && getSelectedText().length() > 0) {
					ChatInputEditor.this.removeWordInBetween(getSelectionStart(), getSelectionEnd());

					// We are somewhere in betwee 0 and str.length
				} else if (getCaretPosition() < getText().length()) {

					String preCaret = getText().substring(0, getCaretPosition());

					int lastSpace = preCaret.lastIndexOf(" ") != -1 ? preCaret.lastIndexOf(" ") : 0;

					if (lastSpace != -1 && lastSpace != 0) {
						// Do we have anymore spaces before the current one?
						for (int i = lastSpace; getText().charAt(i) == ' '; --i) {
							lastSpace--;
						}
						lastSpace++;
					}
					ChatInputEditor.this.removeWordInBetween(lastSpace, getCaretPosition());

					if (lastSpace <= getText().length()) {
						setCaretPosition(lastSpace);
					} else {
						setCaretPosition(getText().length());
					}

					// We are at the end and will remove until the next SPACE
				} else if (getText().contains(" ")) {
					int untilhere = getText().lastIndexOf(" ");

					// Do we have anymore spaces before the last one?
					for (int i = untilhere; getText().charAt(i) == ' '; --i) {
						untilhere--;
					}
					untilhere++;
					ChatInputEditor.this.removeLastWord(getText().substring(untilhere));
				} else {
					ChatInputEditor.this.removeLastWord(getText());
				}
			}
		};

		undoKeyStroke = KeyStroke.getKeyStroke('z', ActionEvent.CTRL_MASK);
		ctrlbackspaceKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, KeyEvent.CTRL_MASK);
		escapeKeyStroke = KeyStroke.getKeyStroke("ESCAPE");

		getInputMap().put(ctrlbackspaceKeyStroke, "ctrlbackspace");
		getInputMap().put(undoKeyStroke, "undo");
		getInputMap().put(escapeKeyStroke, "escape");
		getInputMap().put(KeyStroke.getKeyStroke("Ctrl limitW"), "escape");

		registerKeyboardAction(undo, KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_MASK), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		registerKeyboardAction(ctrlbackspace, KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, KeyEvent.CTRL_MASK), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		registerKeyboardAction(escape, KeyStroke.getKeyStroke("ESCAPE"), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

//		getDocument().addDocumentListener(this);

		addMouseListener(this);

		//		setEditorKit(new HTMLEditorKit() {
		//			@Override
		//			public ViewFactory getViewFactory() {
		//
		//				return new HTMLFactory() {
		//					public View create(Element e) {
		//						View v = super.create(e);
		//						if (v instanceof InlineView) {
		//							return new InlineView(e) {
		//								public int getBreakWeight(int axis, float pos, float len) {
		//									return GoodBreakWeight;
		//								}
		//
		//								public View breakView(int axis, int p0, float pos, float len) {
		//									if (axis == View.X_AXIS) {
		//										checkPainter();
		//										int p1 = getGlyphPainter().getBoundedPosition(this, p0, pos, len);
		//										if (p0 == getStartOffset() && p1 == getEndOffset()) {
		//											return this;
		//										}
		//										return createFragment(p0, p1);
		//									}
		//									return this;
		//								}
		//							};
		//						} else if (v instanceof ParagraphView) {
		//							return new ParagraphView(e) {
		//								protected SizeRequirements calculateMinorAxisRequirements(int axis, SizeRequirements r) {
		//									if (r == null) {
		//										r = new SizeRequirements();
		//									}
		//									float pref = layoutPool.getPreferredSpan(axis);
		//									float min = layoutPool.getMinimumSpan(axis);
		//									// Don't include insets, Box.getXXXSpan will include them.   
		//									r.minimum = (int) min;
		//									r.preferred = Math.max(r.minimum, (int) pref);
		//									r.maximum = Integer.MAX_VALUE;
		//									r.alignment = 0.5f;
		//									return r;
		//								}
		//
		//							};
		//						}
		//						return v;
		//					}
		//				};
		//			}
		//		});

	}

	public void insertUpdate(DocumentEvent e) {
		//    this.setCaretPosition(e.getOffset());
//		this.requestFocusInWindow();
	}

	public void removeUpdate(DocumentEvent e) {
	}

	public void changedUpdate(DocumentEvent e) {
	}

	/**
	 * Remove dependices when no longer in use.
	 */
	public void close() {
		getDocument().removeDocumentListener(this);
		getDocument().removeUndoableEditListener(undoManager);
		removeMouseListener(this);
		getInputMap().remove(undoKeyStroke);
		getInputMap().remove(ctrlbackspaceKeyStroke);
		getInputMap().remove(escapeKeyStroke);
		getInputMap().remove(KeyStroke.getKeyStroke("Ctrl limitW"));
	}

	/**
	 * Disables the Chat Editor, rendering it to the system default color.
	 */
	public void showAsDisabled() {
		this.setEditable(false);
		this.setEnabled(false);

		clear();

		final Color disabledColor = (Color) UIManager.get("Button.disabled");

		this.setBackground(disabledColor);
	}

	/**
	 * Enable the Chat Editor.
	 */
	public void showEnabled() {
		this.setEditable(true);
		this.setEnabled(true);

		//        this.setBackground(Color.white);
	}
}