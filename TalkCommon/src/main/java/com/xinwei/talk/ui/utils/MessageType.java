package com.xinwei.talk.ui.utils;

/**
 * @author Sachini
 *
 */
public enum MessageType {
	 FILE, TEXT, VOICE, IMAGE, NOTIFY, NAMECARD
}
