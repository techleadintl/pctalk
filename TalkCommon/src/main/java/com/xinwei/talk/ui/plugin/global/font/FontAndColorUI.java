package com.xinwei.talk.ui.plugin.global.font;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillCheckButton;
import com.xinwei.common.lookandfeel.component.McWillComboBox;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.ui.panel.SwatchPanel;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.conf.UserConfig;
import com.xinwei.talk.ui.chat.ChatArea;
import com.xinwei.talk.ui.skin.WindowSkinDialog;

public class FontAndColorUI extends JPanel {
	// 字体名称;字号大小;文字样式;文字颜色;文字背景颜色

	public FontAndColorUI(final ChatArea chatInputEditor) {
		final UserConfig userConfig = TalkManager.getUserConfig();

		chatInputEditor.setFontBold(userConfig.getBold());
		chatInputEditor.setFontItalic(userConfig.getItalic());
		chatInputEditor.setFontUnderline(userConfig.getUnderline());
		chatInputEditor.setFontForeground(LAF.getColor(userConfig.getForeground()));
		chatInputEditor.setFontSize(userConfig.getFontSize());
		chatInputEditor.setFontFamily(userConfig.getFontFamily());

		String[] fontNameArray = SwingUtil.getFontNameArray();
		String[] sizeArray = new String[23 - 9];
		for (int i = 9; i < 23; i++) {
			sizeArray[i - 9] = String.valueOf(i);
		}

		// 字体名称
		final JComboBox fontFamilyComboBox = new McWillComboBox(fontNameArray) {
			@Override
			protected ImageIcon[] getPopIcons() {
				return LAF.getLoginUserPopIcon();
			}
		};
		fontFamilyComboBox.setSelectedItem(userConfig.getFontFamily());

		// 字号
		final JComboBox fontSizeComboBox = new McWillComboBox(sizeArray) {
			@Override
			protected ImageIcon[] getPopIcons() {
				return LAF.getLoginUserPopIcon();
			}
		};
		fontSizeComboBox.setSelectedItem(String.valueOf(userConfig.getFontSize()));

		// 粗体
		ImageIcon[] boldImageIcons = LAF.getRoomFontBoldImageIcon();
		final McWillCheckButton boldButton = new McWillCheckButton(boldImageIcons[0], boldImageIcons[1],
				userConfig.getBold());

		// 斜体
		ImageIcon[] italicImageIcons = LAF.getRoomFontItalicImageIcon();
		JButton italicButton = new McWillCheckButton(italicImageIcons[0], italicImageIcons[1], userConfig.getItalic());

		// 下划线
		ImageIcon[] underlineImageIcons = LAF.getRoomFontUnderlineImageIcon();
		JButton underlineButton = new McWillCheckButton(underlineImageIcons[0], underlineImageIcons[1],
				userConfig.getUnderline());

		// 文字颜色
		final JButton foregroundButton = new McWillIconButton(LAF.getRoomFontColorImageIcon());
		final JLabel foregroundLabel = new JLabel() {
			@Override
			protected void paintComponent(Graphics g) {
				// g.setColor(McWillTheme.getThemeColor().getSubLightColor());
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, getWidth(), getHeight());

				g.setColor(getForeground());
				int a = Math.min(getWidth(), getHeight()) - 4;
				// g.fill3DRect((getWidth() - a) / 2, (getHeight() - 3*a) / 2,
				// a, 3*a,true);
				g.fill3DRect((getWidth() - a) / 2, (getHeight() - 1 * a) / 2, a, 1 * a, true);
				super.paintComponent(g);
			}
		};
		foregroundLabel.setForeground(LAF.getColor(userConfig.getForeground()));

		// 气泡
		final JButton popoButton = new McWillIconButton(LAF.getRoomFontBalloonIcon());

		add(fontFamilyComboBox); // 字体
		add(fontSizeComboBox);// 字号
		add(boldButton);// 粗体
		add(italicButton);// 斜体
		add(underlineButton);// 下划线
		add(foregroundButton);// 颜色
		add(popoButton);
		int H = 25;
		fontFamilyComboBox.setPreferredSize(new Dimension(80, H));
		fontSizeComboBox.setPreferredSize(new Dimension(50, H));

		setLayout(new FlowLayout(FlowLayout.LEFT, 5, 4));

		boldButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				userConfig.setBold(!userConfig.getBold());
				TalkManager.saveUserConfig();

				chatInputEditor.setFontBold(userConfig.getBold());
			}
		});

		italicButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				userConfig.setItalic(!userConfig.getItalic());
				TalkManager.saveUserConfig();

				chatInputEditor.setFontItalic(userConfig.getItalic());
			}
		});
		underlineButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				userConfig.setUnderline(!userConfig.getUnderline());
				TalkManager.saveUserConfig();

				chatInputEditor.setFontUnderline(userConfig.getUnderline());
			}
		});
		fontFamilyComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				int stateChange = e.getStateChange();
				if (stateChange == ItemEvent.SELECTED) {
					Object selectedItem = fontFamilyComboBox.getSelectedItem();
					chatInputEditor.setFontFamily(selectedItem.toString());
					userConfig.setFontFamily(selectedItem.toString());
					TalkManager.saveUserConfig();
				}
			}
		});
		fontSizeComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				int stateChange = e.getStateChange();
				if (stateChange == ItemEvent.SELECTED) {
					Object selectedItem = fontSizeComboBox.getSelectedItem();
					chatInputEditor.setFontSize(Integer.valueOf(selectedItem.toString()));
					userConfig.setFontSize(Integer.valueOf(selectedItem.toString()));
					TalkManager.saveUserConfig();
				}
			}
		});
		final JPopupMenu popup = new JPopupMenu();
		JPanel p = new JPanel(new BorderLayout());
		final SwatchPanel swatchPanel = new SwatchPanel();
		foregroundLabel.setPreferredSize(new Dimension(30, 30));
		p.add(swatchPanel, BorderLayout.CENTER);
		p.add(foregroundLabel, BorderLayout.WEST);
		popup.add(p);
		swatchPanel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				Color color = swatchPanel.getColorForLocation(e.getX(), e.getY());
				if (color != null) {
					chatInputEditor.setFontForeground(color);
					foregroundLabel.setForeground(color);
					userConfig.setForeground(color.getRGB());
					TalkManager.saveUserConfig();
				}
			}
		});
		foregroundButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (popup.getHeight() == 0) {
					popup.setVisible(true);
				}
				popup.show(foregroundButton, 0 - popup.getWidth() / 2, 0 - popup.getHeight());

			}
		});

		popoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				WindowSkinDialog windowSkinDialog = WindowSkinDialog.getInstance();
				if (windowSkinDialog.isVisible()) {
					windowSkinDialog.showBalloonTab();
					return;
				} else {
					windowSkinDialog.showBalloonTab();
					SwingUtil.centerWindowOnScreen(windowSkinDialog);
				}
			}
		});
	}
}