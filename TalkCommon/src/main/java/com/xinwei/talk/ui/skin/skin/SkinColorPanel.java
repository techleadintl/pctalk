package com.xinwei.talk.ui.skin.skin;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.McWillLookAndFeel;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillRolloverButton;
import com.xinwei.common.lookandfeel.layout.WrapFlowLayout;
import com.xinwei.talk.common.Res;

@SuppressWarnings("serial")
public class SkinColorPanel extends JPanel {

	private static final Dimension PREFERRED_SIZE = new Dimension(45, 45);

	public SkinColorPanel(final Window window, List<Color> skins) {
		setOpaque(false);

		setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		WrapFlowLayout mgr = new WrapFlowLayout(5, 0);
		mgr.setHgap(WrapFlowLayout.LEFT);
		setLayout(mgr);

		for (final Color color : skins) {
			JButton item = new McWillRolloverButton() {
				@Override
				protected void paintComponent(Graphics g) {
					//					g.setColor(Color.RED);
					//					g.drawRoundRect(0, 0, getWidth()-1, getHeight()-1, 10, 10);
					g.setColor(color);
					g.fillRoundRect(1, 1, getWidth() - 2, getHeight() - 2, 10, 10);
				}
			};
			item.setPreferredSize(PREFERRED_SIZE);
			add(item);
			item.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					setThemeColor(color);
				}

			});
			item.setBorder(BorderFactory.createEmptyBorder());
		}
		URL resource = McWillLookAndFeel.class.getResource("images/chooser/colorchooser.png");
		final ImageIcon icon = new ImageIcon(resource);
		final Image image = icon.getImage();
		// image = image.getScaledInstance(60, 50, Image.SCALE_SMOOTH);
		JButton colorChooseButton = new McWillRolloverButton() {
			@Override
			protected void paintComponent(Graphics g) {
				g.setColor(Color.RED);
				g.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 10, 10);
				g.drawImage(image, 1, 1, getWidth() - 2, getHeight() - 2, null);

			}
		};
		colorChooseButton.setPreferredSize(PREFERRED_SIZE);
		add(colorChooseButton);

		colorChooseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new SkinColorChooserDialog(window, Res.getString("skin.choosecolor"));
				dialog.setVisible(true);
			}
		});
	}

	private void setThemeColor(final Color color) {
		try {
			McWillTheme.setThemeColor(color);
		} catch (Exception e1) {
		}
	}
}
