/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月20日 下午1:15:30
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat;

import java.io.File;
import java.io.IOException;

import javax.swing.filechooser.FileSystemView;

public class WindowsFileSystemView extends FileSystemView {

	/**
	 * Constructor WindowsFileSystemView
	 */
	public WindowsFileSystemView() {

	}

	/**
	 * isRoot
	 *
	 * @param value0
	 * @return boolean
	 */
	public boolean isRoot(File value0) {
		return false;
	}

	/**
	 * createNewFolder
	 *
	 * @param value0
	 * @throws IOException
	 * @return File
	 */
	public File createNewFolder(File value0) throws IOException {
		return null;
	}

	/**
	 * isHiddenFile
	 *
	 * @param value0
	 * @return boolean
	 */
	public boolean isHiddenFile(File value0) {
		return false;
	}

	/**
	 * getRoots
	 *
	 * @return File[]
	 */
	public File[] getRoots() {
		return null;
	}

}
