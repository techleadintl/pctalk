package com.xinwei.talk.ui.util.component;
//package com.xinwei.talk.ui.util.component;
//
//import java.awt.Color;
//import java.awt.Cursor;
//import java.awt.Desktop;
//import java.awt.Font;
//import java.awt.GridBagConstraints;
//import java.awt.GridBagLayout;
//import java.awt.Insets;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.io.File;
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.net.URL;
//
//import javax.swing.BorderFactory;
//import javax.swing.Icon;
//import javax.swing.ImageIcon;
//import javax.swing.JButton;
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//import javax.swing.JProgressBar;
//import javax.swing.SwingUtilities;
//
//import com.xinwei.talk.service.file.XWOutFileTransfer;
//import com.xinwei.spark.FileDragLabel;
//
//
//public class XWSendFileTransfer extends JPanel {
//	private static final long serialVersionUID = -4403839897649365671L;
//	private FileDragLabel imageLabel = new FileDragLabel();
//	private JLabel titleLabel = new JLabel();
//	private JLabel fileLabel = new JLabel();
//
//	private TransferButton cancelButton = new TransferButton();
//	private JProgressBar progressBar = new JProgressBar();
//	private File fileToSend;
//	private XWOutFileTransfer transfer;
//
//	private TransferButton retryButton = new TransferButton();
//
//	//	private FileTransferManager transferManager;
//	private String nickname;
//	private JLabel progressLabel = new JLabel();
//	private long _starttime;
//
//	public XWSendFileTransfer() {
//		setLayout(new GridBagLayout());
//
//		setBackground(new Color(250, 249, 242));
//		add(imageLabel, new GridBagConstraints(0, 0, 1, 3, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
//
//		add(titleLabel, new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
//		titleLabel.setFont(new Font("Dialog", Font.BOLD, 11));
//		titleLabel.setForeground(new Color(211, 174, 102));
//		add(fileLabel, new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 0, 0));
//
//		cancelButton.setText(Res.getString("cancel"));
//		retryButton.setText(Res.getString("retry"));
//		cancelButton.setIcon(SparkRes.getImageIcon(SparkRes.SMALL_DELETE));
//		retryButton.setIcon(SparkRes.getImageIcon(SparkRes.REFRESH_IMAGE));
//
//		add(cancelButton, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
//		add(retryButton, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 0, 0));
//		retryButton.setVisible(false);
//
//		retryButton.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				try {
//					File file = transfer.getFile();
//					transfer = new XWOutFileTransfer(file);
//				} catch (Exception e1) {
//					Log.error(e1);
//				}
//				sendFile(transfer, nickname);
//			}
//		});
//
//		cancelButton.setForeground(new Color(73, 113, 196));
//		cancelButton.setFont(new Font("Dialog", Font.BOLD, 11));
//		cancelButton.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(73, 113, 196)));
//
//		retryButton.setForeground(new Color(73, 113, 196));
//		retryButton.setFont(new Font("Dialog", Font.BOLD, 11));
//		retryButton.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(73, 113, 196)));
//
//		//		setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.white));
//	}
//
//	public void sendFile(final XWOutFileTransfer transfer, final String nickname) {
//		this.transfer = transfer;
//
//		//发送文件
//		sendFile(transfer);
//
//		cancelButton.setVisible(true);
//		retryButton.setVisible(false);
//		this.nickname = nickname;
//
//		String fileName = transfer.getFilename();
//		long fileSize = transfer.getContentLength();
//		ByteFormat format = new ByteFormat();
//		String text = format.format(fileSize);
//
//		fileToSend = transfer.getFile();
//		imageLabel.setFile(fileToSend);
//
//		fileLabel.setText(fileName + " (" + text + ")");
//
//		titleLabel.setText(Res.getString("message.transfer.waiting.on.user", nickname));
//
//		if (isImage(fileName)) {
//			try {
//				URL imageURL = transfer.getFile().toURI().toURL();
//				ImageIcon image = new ImageIcon(imageURL);
//				image = GraphicUtils.scaleImageIcon(image, 64, 64);
//				imageLabel.setIcon(image);
//			} catch (MalformedURLException e) {
//				Log.error("Could not locate image.", e);
//				imageLabel.setIcon(SparkRes.getImageIcon(SparkRes.DOCUMENT_INFO_32x32));
//			}
//		} else {
//			Icon icon = GraphicUtils.getIcon(transfer.getFile());
//			imageLabel.setIcon(icon);
//		}
//
//		cancelButton.addMouseListener(new MouseAdapter() {
//			public void mouseClicked(MouseEvent mouseEvent) {
//				transfer.cancel();
//			}
//		});
//
//		cancelButton.addMouseListener(new MouseAdapter() {
//			public void scrollBarMouseEntered(MouseEvent e) {
//				cancelButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
//
//			}
//
//			public void mouseExited(MouseEvent e) {
//				cancelButton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
//			}
//		});
//
//		progressBar.setMaximum(100);
//		progressBar.setVisible(false);
//		progressBar.setStringPainted(true);
//		add(progressBar, new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 150, 0));
//		add(progressLabel, new GridBagConstraints(1, 3, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 5), 150, 0));
//
//		SwingWorker worker = new SwingWorker() {
//
//			public Object construct() {
//				while (true) {
//					try {
//						if (transfer.getBytesSent() > 0 && _starttime == 0) {
//							_starttime = System.currentTimeMillis();
//						}
//
//						long starttime = System.currentTimeMillis();
//						long startbyte = transfer.getBytesSent();
//						Thread.sleep(500);
//						TransferStatus isInputing = transfer.getStatus();
//						if (isInputing == TransferStatus.error || isInputing == TransferStatus.complete || isInputing == TransferStatus.cancelled) {
//							break;
//						}
//						long endtime = System.currentTimeMillis();
//						long endbyte = transfer.getBytesSent();
//
//						long timediff = endtime - starttime;
//						long bytediff = endbyte - startbyte;
//
//						updateBar(transfer, nickname, TransferUtils.calculateSpeed(bytediff, timediff));
//					} catch (InterruptedException e) {
//						Log.error("Unable to sleep thread.", e);
//					}
//
//				}
//				return "";
//			}
//
//			public void finished() {
//				updateBar(transfer, nickname, "??MB/s");
//			}
//		};
//
//		worker.startCamTalk();
//
//		makeClickable(imageLabel);
//		makeClickable(titleLabel);
//		makeClickable(fileLabel);
//	}
//
//	private void sendFile(final XWOutFileTransfer transfer) {
//		Thread transferThread = new Thread(new Runnable() {
//			public void run() {
//				try {
//					HttpFileResponseInfo response = XWManager.getXWService().uploadChatFile(transfer);
//					//文件上传的地方
//					String fileAbsolutePath = response.getFileAbsolutePath();
//
//					doFinish(transfer, fileAbsolutePath);
//
//				} catch (Exception e) {
//					return;
//				}
//				//					updateStatus(Status.in_progress, FileTransfer.Status.complete);
//			}
//
//		}, "File Transfer " + System.currentTimeMillis());
//		transferThread.start();
//	}
//
//	private void makeClickable(final JLabel label) {
//		label.setToolTipText(Res.getString("message.click.to.open"));
//
//		label.addMouseListener(new MouseAdapter() {
//			public void mouseClicked(MouseEvent e) {
//				openFile(fileToSend);
//			}
//
//			public void scrollBarMouseEntered(MouseEvent e) {
//				label.setCursor(new Cursor(Cursor.HAND_CURSOR));
//
//			}
//
//			public void mouseExited(MouseEvent e) {
//				label.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
//			}
//		});
//	}
//
//	private void openFile(File downloadedFile) {
//		try {
//			Desktop.getDesktop().open(downloadedFile);
//		} catch (IOException e) {
//			Log.error(e);
//		}
//	}
//
//	private void updateBar(final XWOutFileTransfer transfer, String nickname, String kBperSecond) {
//		TransferStatus isInputing = transfer.getStatus();
//		if (isInputing == TransferStatus.initial) {
//			titleLabel.setText(Res.getString("message.negotiation.file.transfer", nickname));
//		} else if (isInputing == TransferStatus.error) {
//			if (transfer.getException() != null) {
//				Log.error("Error occured during file transfer.", transfer.getException());
//			}
//			progressBar.setVisible(false);
//			progressLabel.setVisible(false);
//			titleLabel.setText(Res.getString("message.unable.to.send.file", nickname));
//			cancelButton.setVisible(false);
//			retryButton.setVisible(true);
//			showAlert(true);
//		} else if (isInputing == TransferStatus.in_progress) {
//			titleLabel.setText(Res.getString("message.sending.file.to", nickname));
//			showAlert(false);
//			if (!progressBar.isVisible()) {
//				progressBar.setVisible(true);
//				progressLabel.setVisible(true);
//			}
//
//			try {
//				SwingUtilities.invokeAndWait(new Runnable() {
//					public void run() {
//						// 100 % = Filesize
//						// x %   = Currentsize	    
//						long p = (transfer.getBytesSent() * 100 / transfer.getContentLength());
//						progressBar.setValue(Math.round(p));
//					}
//				});
//			} catch (Exception e) {
//				Log.error(e);
//			}
//
//			ByteFormat format = new ByteFormat();
//			String bytesSent = format.format(transfer.getBytesSent());
//			String est = TransferUtils.calculateEstimate(transfer.getBytesSent(), transfer.getContentLength(), _starttime, System.currentTimeMillis());
//
//			progressLabel.setText(Res.getString("message.transfer.progressbar.text.sent", bytesSent, kBperSecond, est));
//		} else if (isInputing == TransferStatus.complete) {
//			progressBar.setVisible(false);
//
//			String fin = TransferUtils.convertSecondstoHHMMSS(Math.round(System.currentTimeMillis() - _starttime) / 1000);
//			progressLabel.setText(Res.getString("label.time", fin));
//			titleLabel.setText(Res.getString("message.you.have.sent", nickname));
//			cancelButton.setVisible(false);
//			showAlert(true);
//		} else if (isInputing == TransferStatus.cancelled) {
//			progressBar.setVisible(false);
//			progressLabel.setVisible(false);
//			titleLabel.setText(Res.getString("message.file.transfer.canceled"));
//			cancelButton.setVisible(false);
//			retryButton.setVisible(true);
//			showAlert(true);
//		}
//
//	}
//
//	protected void doFinish(XWOutFileTransfer transfer, String url) {
//		// TODO Auto-generated method stub
//
//	}
//
//	private class TransferButton extends JButton {
//
//		private static final long serialVersionUID = 8807434179541503654L;
//
//		public TransferButton() {
//			decorate();
//		}
//
//		/**
//		 * Decorates the button with the approriate UI configurations.
//		 */
//		private void decorate() {
//			setBorderPainted(false);
//			setOpaque(true);
//
//			setContentAreaFilled(false);
//			setMargin(new Insets(1, 1, 1, 1));
//		}
//
//	}
//
//	private boolean isImage(String fileName) {
//		fileName = fileName.toLowerCase();
//
//		String[] imageTypes = { "jpeg", "gif", "jpg", "png" };
//		for (String imageType : imageTypes) {
//			if (fileName.endsWith(imageType)) {
//				return true;
//			}
//		}
//
//		return false;
//	}
//
//	private void showAlert(boolean alert) {
//		if (alert) {
//			titleLabel.setForeground(new Color(211, 174, 102));
//			setBackground(new Color(250, 249, 242));
//		} else {
//			setBackground(new Color(239, 245, 250));
//			titleLabel.setForeground(new Color(65, 139, 179));
//		}
//	}
//
//	public void cancelTransfer() {
//		if (transfer != null && !transfer.isDone()) {
//			transfer.cancel();
//		}
//	}
//
//}
