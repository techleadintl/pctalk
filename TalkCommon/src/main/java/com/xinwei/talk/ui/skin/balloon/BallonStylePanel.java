package com.xinwei.talk.ui.skin.balloon;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
import com.xinwei.common.lookandfeel.layout.WrapFlowLayout;
import com.xinwei.common.ui.vo.chat.Balloon;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.chat.balloon.BalloonManager;

@SuppressWarnings("serial")
public class BallonStylePanel extends JPanel {

	private static final Dimension PREFERRED_SIZE = new Dimension(85, 85);

	public BallonStylePanel(final Window parentDialog) {
		setOpaque(false);
		setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		WrapFlowLayout mgr = new WrapFlowLayout(5, 5);
		mgr.setHgap(WrapFlowLayout.LEFT);
		setLayout(mgr);

		addIconButtons(parentDialog);
	}

	private void addIconButtons(final Window parentDialog) {
		Collection<Balloon> balloons = BalloonManager.getInstance().getBalloons();
		for (final Balloon balloon : balloons) {
			final ImageIcon icon =balloon.getBackground();
			Image image = icon.getImage();

			Image scaledInstance = image.getScaledInstance(PREFERRED_SIZE.width, PREFERRED_SIZE.height, Image.SCALE_DEFAULT);
			scaledInstance.flush();
			ImageIcon icon2 = new ImageIcon(scaledInstance);
			JLabel item = new McWillRolloverLabel(icon2, false);
			item.setPreferredSize(PREFERRED_SIZE);
			add(item);
			item.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					TalkManager.getUserConfig().setBalloon(balloon.getKey());
					TalkManager.saveUserConfig();
					parentDialog.setVisible(false);
				}
			});
		}
	}
}
