package com.xinwei.talk.ui.util.component.button;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

import com.xinwei.talk.common.LAF;


/**
 * Button UI for handling of rollover buttons.
 *
 */
public class RolloverButton extends JButton {
	private static final long serialVersionUID = 6351541211385798436L;

	/**
	 * Create a new RolloverButton.
	 */
	public RolloverButton() {
		decorate();
	}

	public RolloverButton(String text) {
		super(text);
		decorate();
	}

	public RolloverButton(Action action) {
		super(action);
		decorate();
	}

	/**
	 * Create a new RolloverButton.
	 *
	 * @param icon the icon to use on the button.
	 */
	public RolloverButton(Icon icon) {
		super(icon);
		decorate();
	}

	/**
	 * Create a new RolloverButton.
	 *
	 * @param text the button text.
	 * @param icon the button icon.
	 */
	public RolloverButton(String text, Icon icon) {
		super(text, icon);
		decorate();
	}

	/**
	 * Decorates the button with the approriate UI configurations.
	 */
	protected void decorate() {
		setBorderPainted(false);
		setOpaque(true);

		setContentAreaFilled(false);
		setMargin(new Insets(1, 1, 1, 1));

		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				if (isEnabled()) {
					image = _IMAGE;
				} else {
					image = null;
				}
			}

			public void mouseExited(MouseEvent e) {
				image = null;
			}
		});

	}

	@Override
	protected void paintComponent(Graphics g) {
		if (image != null) {
			g.drawImage(image, 0, 0, getWidth() - 1, getHeight() - 1, null);
		}
		super.paintComponent(g);
	}

	private Image image = null;

	private static Image _IMAGE = LAF.getButtonBackgroudImage();

}