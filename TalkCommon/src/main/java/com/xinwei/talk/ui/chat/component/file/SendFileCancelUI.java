package com.xinwei.talk.ui.chat.component.file;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.component.McWillTextPane;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.manager.TransferManager;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.util.component.label.LinkLabel;
import com.xinwei.talk.ui.util.component.room.ChatRoomComponent;

public class SendFileCancelUI extends ChatRoomComponent {
	private static final long serialVersionUID = -4403839897649365671L;

	int FILE_NAME_MAX_LENGTH = 200;

	protected Color leftColor1 = Color.WHITE;
	protected Color leftBorderColor = LAF.getColor(155, 155, 155);
	protected McWillTextPane infoLabel;

	protected LinkLabel resendButton;

	private File fileToSend;


	private MouseListener resendButtonListener;

	public SendFileCancelUI(final File file) {
		this.fileToSend = file;

		//////////////
		infoLabel = new McWillTextPane();
		infoLabel.setEditable(false);

		resendButton = new LinkLabel(Res.getMessage("button.open"), Color.blue, Color.red);

		JPanel buttonPanel = new JPanel(new BorderLayout());
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(15, 0, 0, 0));

		JPanel infoPanel = new JPanel(new BorderLayout());
		JPanel iconPanel = new JPanel(new BorderLayout());
		iconPanel.setBorder(BorderFactory.createEmptyBorder(10, 5, 0, 2));
		iconPanel.add(new JLabel(LAF.getDeleteIcon()[1]), BorderLayout.NORTH);
		infoPanel.add(infoLabel, BorderLayout.CENTER);
		infoPanel.add(iconPanel, BorderLayout.WEST);

		contentPanel.add(infoPanel, BorderLayout.CENTER);
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 5));

		String fileName = file.getName();

		String text = SwingUtil.format(file.length());
		fileName += " (" + text + ")";
		infoLabel.setText(Res.getMessage("label.chatroom.fileupload.cancel", fileName));
		if (fileToSend.exists()) {
			resendButton = new LinkLabel(Res.getMessage("button.resend"), Color.blue, Color.red);
			resendButton.setAlignmentY(0.75f);
			resendButtonListener = new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					resendFile(fileToSend);
				}
			};
			resendButton.addMouseListener(resendButtonListener);

			infoLabel.insertComponent(resendButton);
		}

		infoLabel.requestFocus();

		infoLabel.updateUI();
	}

	@Override
	protected boolean isBalloon() {
		return false;
	}

	private void resendFile(File file) {
		resendButton.setEnabled(false);
		resendButton.removeMouseListener(resendButtonListener);
		TransferManager transferManager = TalkManager.getTransferManager();
		String roomId = TalkManager.getChatManager().getChatContainer().getActiveChatRoom().getRoomId();
		transferManager.sendFile(file, roomId);
	}

}
