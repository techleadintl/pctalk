/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月23日 下午3:17:53
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat.room.history;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import com.xinwei.common.lang.DateUtil;
import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lookandfeel.component.McWillRolloverLabel;
import com.xinwei.common.lookandfeel.layout.TileAutoLayout;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.Constants;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.manager.MessageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.TalkFile;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkImageSpecific;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;

public class HistoryImageWindow extends JPanel {

	private static int ROW = 3;

	private int COLUM_WIDTH;

	private Dimension PREFERRED_SIZE;

	CommonChatRoom room;

	public HistoryImageWindow(CommonChatRoom room) {
		this.room = room;

		COLUM_WIDTH = (LAF.CHAT_ROOM_RIGHT_WIDTH - 35) / ROW;
		PREFERRED_SIZE = new Dimension(COLUM_WIDTH, COLUM_WIDTH);

		setOpaque(false);

		setLayout(new BorderLayout(0, 0));

		final JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setLayout(new GridBagLayout());

		JScrollPane jScrollPane = new JScrollPane(panel);
		jScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		add(jScrollPane, BorderLayout.CENTER);

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				Map<String, List<TalkImageSpecific>> imageMsgs = null;
				try {
					imageMsgs = getImageMsgs();
				} catch (Exception e) {
					e.printStackTrace();
				}

				createPanel(panel, imageMsgs);

				updateUI();
			}
		});

	}

	public Map<String, List<TalkImageSpecific>> getImageMsgs() throws Exception {
		MessageManager messageManager = TalkManager.getMessageManager();
		List<TalkMessage> talkMessageList = messageManager.getMessageList(room.getRoomId(), Constants.MSG_TYPE_IMG);
		if (talkMessageList == null) {
			return null;
		}

		Map<String, List<TalkImageSpecific>> imageMap = new HashMap<String, List<TalkImageSpecific>>();

		for (TalkMessage message : talkMessageList) {
			Date time = message.getTalkBody().getTime();
			if (time == null) {
				time = new Date();
			}
			String timeStr = DateUtil.format(time, DateUtil.PATTERN_YYYY_MM_DD);

			List<TalkSpecific> msgList = message.getSpecificList(Constants.MSG_TYPE_IMG);
			if (msgList == null) {
				continue;
			}
			for (TalkSpecific body : msgList) {
				TalkImageSpecific imageMsg = (TalkImageSpecific) body;
				String url = imageMsg.getUrl();
				if (StringUtil.isBlank(url))
					continue;

				List<TalkImageSpecific> list = imageMap.get(timeStr);
				if (list == null) {
					list = new ArrayList<>();
					imageMap.put(timeStr, list);
				}
				list.add(imageMsg);
			}
		}
		return imageMap;
	}

	public void createPanel(JPanel panel, Map<String, List<TalkImageSpecific>> iconsMap) {
		if (iconsMap == null)
			return;
		ArrayList<String> timeList = new ArrayList<String>(iconsMap.keySet());

		Collections.sort(timeList);

		int i = 0;
		for (final String timeStr : timeList) {
			List<TalkImageSpecific> iconList = iconsMap.get(timeStr);
			if (iconList == null) {
				continue;
			}
			JPanel entryPanel = new JPanel(new GridBagLayout());
			entryPanel.setOpaque(false);

			JLabel titleLabel = new JLabel(timeStr);
			titleLabel.setIcon(LAF.getGreyStar());
			titleLabel.setForeground(LAF.getColor(128, 128, 128));

			TileAutoLayout tileLayout = new TileAutoLayout(this, 0, ROW, ROW);
			JPanel iconPanel = new JPanel(tileLayout);
			iconPanel.setOpaque(false);
			List<String> urlList = new ArrayList<String>();
			for (final TalkImageSpecific imageMsg : iconList) {
				urlList.add(imageMsg.getUrl());
			}
			if (urlList.isEmpty()) {
				continue;
			}
			List<TalkFile> talkFileList = TalkManager.getFileManager().getImageFileList(urlList);
			for (final TalkFile talkFile : talkFileList) {
				final JLabel label = new McWillRolloverLabel();
				label.setPreferredSize(PREFERRED_SIZE);
				iconPanel.add(label);

				byte[] bytes = talkFile.getData();
				if (!talkFile.getDownload()) {
					if (bytes == null && talkFile.getPath() != null) {
						bytes = IOUtil.readBytes(new File(talkFile.getPath()));
					}
				}

				if (bytes != null && bytes.length > 0) {
					label.setIcon(ImageUtil.resizeImageIcon2(new ImageIcon(bytes), COLUM_WIDTH, COLUM_WIDTH));
				} else {
					label.setIcon(ImageUtil.resizeImageIcon2(LAF.getMsgImage(), COLUM_WIDTH, COLUM_WIDTH));
				}
			}
			entryPanel.add(titleLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
			entryPanel.add(iconPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

			panel.add(entryPanel, new GridBagConstraints(0, i++, 0, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		}
		panel.add(new JLabel(), new GridBagConstraints(0, i++, 0, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

	}

	public void createPanel1(JPanel panel, Map<String, List<ImageIcon>> iconsMap) {
		if (iconsMap == null)
			return;
		ArrayList<String> timeList = new ArrayList<String>(iconsMap.keySet());

		Collections.sort(timeList);

		int i = 0;
		for (final String timeStr : timeList) {
			List<ImageIcon> iconList = iconsMap.get(timeStr);
			if (iconList == null) {
				continue;
			}
			JPanel entryPanel = new JPanel(new GridBagLayout());
			entryPanel.setOpaque(false);

			JLabel titleLabel = new JLabel(timeStr);
			titleLabel.setIcon(LAF.getGreyStar());
			titleLabel.setForeground(LAF.getColor(128, 128, 128));

			TileAutoLayout tileLayout = new TileAutoLayout(this, 0, ROW, ROW);
			JPanel iconPanel = new JPanel(tileLayout);
			iconPanel.setOpaque(false);
			for (ImageIcon icon : iconList) {
				JLabel item = new McWillRolloverLabel(new ImageIcon(icon.getImage().getScaledInstance(PREFERRED_SIZE.width, PREFERRED_SIZE.height, Image.SCALE_DEFAULT)));
				item.setPreferredSize(PREFERRED_SIZE);
				iconPanel.add(item);
			}

			entryPanel.add(titleLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
			entryPanel.add(iconPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

			panel.add(entryPanel, new GridBagConstraints(0, i++, 0, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		}
		panel.add(new JLabel(), new GridBagConstraints(0, i++, 0, 1, 0.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

	}
}
