package com.xinwei.talk.ui.chat.component.file;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lang.URLFileSystem;
import com.xinwei.talk.common.Downloads;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.ui.chat.balloon.BalloonMessage;

public class ReceiveFileCompleteUI extends FileCompleteUI {

	public ReceiveFileCompleteUI(BalloonMessage balloonMsg, TalkFileSpecific fileMsg, final File downloadedFile) {
		super(balloonMsg, fileMsg, downloadedFile);
	}

	@Override
	protected void initData(File file) {
		Icon fileImageIcon = null;
		if (SwingUtil.isImage(file.getName())) {
			try {
				URL imageURL = file.toURI().toURL();
				ImageIcon image = new ImageIcon(imageURL);
				fileImageIcon = SwingUtil.scaleImageIcon(image, 32, 32);
			} catch (MalformedURLException e) {
				Log.error("Could not locate image.", e);
			}
		} else {
			fileImageIcon = SwingUtil.getFileImageIcon(file);
		}
		if (fileImageIcon == null) {
			fileImageIcon = LAF.getDocumentInfoImageIcon();
		}
		fileIconLabel.setIcon(fileImageIcon);

		filePathTextPane.setText(Res.getMessage("label.chatroom.filedownload.complete", file.getAbsolutePath()));
	}

	protected void addListeners() {
		fileIconLabel.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				showPopup(e, file);
			}

			public void mouseReleased(MouseEvent e) {
				showPopup(e, file);
			}
		});
	}

	private void showPopup(MouseEvent e, final File downloadedFile) {
		if (e.isPopupTrigger()) {
			final JPopupMenu popup = new JPopupMenu();

			final Component ui = this;
			Action saveAsAction = new AbstractAction() {
				private static final long serialVersionUID = -3010501340128285438L;

				public void actionPerformed(ActionEvent e) {
					final JFileChooser chooser = Downloads.getFileChooser();
					File selectedFile = chooser.getSelectedFile();
					if (selectedFile != null) {
						selectedFile = new File(selectedFile.getParent(), downloadedFile.getName());
					} else {
						selectedFile = downloadedFile;
					}
					chooser.setSelectedFile(selectedFile);

					int ok = chooser.showSaveDialog(ui);
					if (ok == JFileChooser.APPROVE_OPTION) {
						File file = chooser.getSelectedFile();
						try {
							if (file.exists()) {
								int confirm = JOptionPane.showConfirmDialog(ui, Res.getMessage("message.file.exists.question"), Res.getString("tabTitle.file.exists"), JOptionPane.YES_NO_OPTION,
										JOptionPane.QUESTION_MESSAGE);
								if (confirm == JOptionPane.NO_OPTION) {
									return;
								}
							}
							URLFileSystem.copy(downloadedFile.toURI().toURL(), file);
						} catch (IOException e1) {
							Log.error(e1);
						}
					}
				}
			};

			saveAsAction.putValue(Action.NAME, Res.getMessage("menuitem.save.as"));
			popup.add(saveAsAction);
			popup.show(this, e.getX(), e.getY());
		}
	}

}
