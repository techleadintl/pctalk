/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年2月28日 下午1:25:25
 * 
 ***************************************************************/
package com.xinwei.talk.ui.setting.detail;

import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.talk.common.Res;
import com.xinwei.talk.ui.setting.SettingDetailPanel;
import com.xinwei.talk.ui.setting.SettingSubPanel;

public class AboutPanel extends SettingDetailPanel {
	protected void addSubPanels() {
		add(new SettingSubPanel(Res.getMessage("setting.about.talk.version")) {
			@Override
			protected void createContentPanel(JPanel contentPanel) {
				JLabel label = new JLabel(Res.getString("talk.version"));
				contentPanel.add(label);

				contentPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

				contentPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 0, 0));
			}
		});
	}
}
