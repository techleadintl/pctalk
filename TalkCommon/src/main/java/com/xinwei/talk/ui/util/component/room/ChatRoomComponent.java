package com.xinwei.talk.ui.util.component.room;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.xinwei.common.lookandfeel.layout.WrapFlowLayout;
import com.xinwei.talk.common.LAF;

public class ChatRoomComponent extends JPanel {
	private static final long serialVersionUID = -4403839897649365671L;

	//	protected Color leftColor1 = LAF.getColor(188, 237, 245);
	//	protected Color leftBorderColor = LAF.getColor(155, 155, 155);
	protected Color leftColor = Color.WHITE;
	protected Color leftBorderColor = LAF.getColor(155, 155, 155);

	protected WrapFlowLayout balloonWrapFlowLayout = new WrapFlowLayout();

	protected JPanel contentPanel;

	public ChatRoomComponent() {
		JPanel wrapPanel = new JPanel();
		balloonWrapFlowLayout.setHalign(SwingConstants.LEFT);

		wrapPanel.setLayout(balloonWrapFlowLayout);

		contentPanel = new JPanel(new BorderLayout()) {
			@Override
			protected void paintComponent(Graphics g) {
//				if (isBalloon()) {
//					Graphics2D g2D = (Graphics2D) g;
//					g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // 反锯齿平滑绘制  
//
//					Rectangle b = getContentPanel().getBounds();
//										b.x += 5;
//										b.y += 5;
//										b.width -= 10;
//										b.height -= 10;
//
//					if (leftBorderColor != null) {
//						// 绘制圆角消息气泡边框  
//						g2D.setColor(leftBorderColor);
//						g2D.drawRoundRect(b.x, b.y, b.width - 1, b.height - 1, 10, 10);
//					}
//					if (leftColor != null) {
//						// 绘制自己消息圆角消息气泡矩形  
//						g2D.setColor(leftColor);
//						g2D.fillRoundRect(b.x + 1, b.y + 1, b.width - 2, b.height - 2, 10, 10);
//					}
//				}
				super.paintComponent(g);
			}
		};

		wrapPanel.add(contentPanel);

		setLayout(new BorderLayout());
		add(wrapPanel, BorderLayout.CENTER);
		//		setPreferredSize(new Dimension(400, 150));
		//		setMaximumSize(new Dimension(400, 400));
	}

	protected boolean isBalloon() {
		return true;
	}

	public Color getLeftColor() {
		return leftColor;
	}

	public void setLeftColor(Color leftColor1) {
		this.leftColor = leftColor1;
	}

	public Color getLeftBorderColor() {
		return leftBorderColor;
	}

	public void setLeftBorderColor(Color leftBorderColor) {
		this.leftBorderColor = leftBorderColor;
	}

	public void setAlignment(int alignment) {
		balloonWrapFlowLayout.setHalign(alignment);
	}

	public JPanel getContentPanel() {
		return contentPanel;
	}

	public void setContentPanel(JPanel contentPanel) {
		this.contentPanel = contentPanel;
	}

}
