/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月12日 下午1:08:56
 * 
 ***************************************************************/
package com.xinwei.talk.manager;

import com.xinwei.common.lang.GsonUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lookandfeel.util.ScreenWindowHide;
import com.xinwei.http.service.HttpFileService;
import com.xinwei.http.service.HttpFindFriendsService;
import com.xinwei.http.service.HttpInviteFriendService;
import com.xinwei.http.service.HttpLoginService;
import com.xinwei.http.service.HttpPasswordService;
import com.xinwei.http.service.HttpTalkService;
import com.xinwei.spark.ui.NativeManager;
import com.xinwei.talk.action.XAction;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.dao.AppDao;
import com.xinwei.talk.dao.TalkDao;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.conf.AppConfig;
import com.xinwei.talk.model.conf.UserConfig;
import com.xinwei.talk.model.message.MessageConverter;
import com.xinwei.talk.ui.chat.ChatFrame;
import com.xinwei.talk.ui.main.MainWindow;
import com.xinwei.talk.ui.main.Workspace;
import com.xinwei.talk.ui.updater.CheckUpdates;
import com.xinwei.xmpp.service.XmppService;
import com.xinwei.xmpp.smack.XWXMPPConnection;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.filter.PacketFilter;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.util.*;
import java.util.List;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class TalkManager {
	// 防止被JVM回收，导致文件锁失效
	public static FileOutputStream LOCK;
	private static ScreenWindowHide screenWindowHide;
	private static ConfigManager configManager;
	private static ChatManager chatManager;
	private static NativeManager nativeManager;

	private static SessionManager sessionManager;
	private static SoundManager soundManager;

	private static XmppService xmppService = new XmppService();

	private static LocalManager localManager = new LocalManager();

	private static VCard vcard = new VCard();

	private static AppConfig appConfig;

	private static UserConfig userConfig;

	private static AppDao appDao;

	private static FileManager fileManager;

	private static CheckUpdates checkUpdates;

	private static List<PacketFilter> filters;

	public static ScreenWindowHide getScreenWindowHide() {
		return screenWindowHide;
	}

	public static void setScreenWindowHide(ScreenWindowHide sw) {
		screenWindowHide = sw;
	}

	public static CheckUpdates getCheckUpdates() {
		if (checkUpdates == null) {
			checkUpdates = new CheckUpdates();
		}
		return checkUpdates;
	}

	public static ConfigManager getConfigManager() {
		if (configManager == null) {
			configManager = new ConfigManager();
		}
		return configManager;
	}

	public synchronized static FileManager getFileManager() {
		if (fileManager == null) {
			fileManager = new FileManager();
		}
		return fileManager;
	}

	public static MessageManager getMessageManager() {
		return MessageManager.getInstance();
	}

	/**
	 * Returns the ChatManager. The ChatManager is responsible for creation and
	 * removal of chat rooms, transcripts, and transfers and room invitations.
	 *
	 * @return the <code>ChatManager</code> for this instance.
	 */
	public static ChatManager getChatManager() {
		if (chatManager == null) {
			chatManager = ChatManager.getInstance();
		}
		return chatManager;
	}

	/**
	 * Returns the NativeManager. The NativeManager allows for native handling of
	 * Talk.
	 *
	 */
	public static NativeManager getNativeManager() {
		if (nativeManager == null) {
			nativeManager = new NativeManager();
		}

		return nativeManager;
	}

	/**
	 * Gets the {@link SessionManager} instance.
	 *
	 * @return the SessionManager instance.
	 */
	public static SessionManager getSessionManager() {
		if (sessionManager == null) {
			sessionManager = new SessionManager();
		}
		return sessionManager;
	}

	public static SoundManager getSoundManager() {
		if (soundManager == null) {
			soundManager = new SoundManager();
		}
		return soundManager;
	}

	public static TransferManager getTransferManager() {
		return TransferManager.getInstance();
	}

	public static XWXMPPConnection getConnection() {
		return (XWXMPPConnection) getSessionManager().getConnection();
	}

	public static MainWindow getMainWindow() {
		return MainWindow.getInstance();
	}

	public static ChatFrame getChatFrame() {
		return getChatManager().getChatFrame();
	}

	public static Workspace getWorkspace() {
		return Workspace.getInstance();
	}

	public synchronized static AppDao getAppDao() {
		
		if (appDao == null) {
			appDao = new AppDao();
			appDao.init();
		}
		return appDao;
	}

	public static AppConfig getAppConfig() {
		if (appConfig == null) {
			String value = getAppDao().getAppConfig("app.config");
			if (StringUtil.isNotEmpty(value)) {
				appConfig = GsonUtil.fromJson(value, AppConfig.class);
			} else {
				appConfig = new AppConfig();
			}
		}
		return appConfig;
	}

	public static void saveAppConfig() {
		if (appConfig == null) {
			return;
		}
		getAppDao().saveAppConfig("app.config", GsonUtil.toJson(appConfig));
	}

	public static UserConfig getUserConfig() {
		if (userConfig == null) {
			String value = getTalkDao().getUserConfig("user.config");
			if (StringUtil.isNotEmpty(value)) {
				userConfig = GsonUtil.fromJson(value, UserConfig.class);
			} else {
				userConfig = new UserConfig();
			}
		}
		return userConfig;
	}

	public static void saveUserConfig() {
		if (userConfig == null) {
			return;
		}
		getTalkDao().saveUserConfig("user.config", GsonUtil.toJson(userConfig));
	}

	public static VCard getVCard() {
		return vcard;
	}

	public static VCard getLastVCard() {
		String lastUsername = getAppConfig().getLastUsername();
		return getAppDao().getLoginByTel(lastUsername);
	}

	public static void saveVCard() {
		try {
			getAppDao().saveLogin(vcard);
		} catch (Exception e) {
			Log.error(e);
		}
	}

	public static void updateVCard(String uid, byte[] avatar) {
		try {
			getAppDao().updateLoginAvatar(uid, avatar);
		} catch (Exception e) {
			Log.error(e);
		}
	}

	public static void deleteVCard(VCard card) {
		try {
			getAppDao().deleteLogin(card.getUid());
			File userHome = TalkUtil.getUserHome(card.getTel());
			userHome.delete();
		} catch (Exception e) {
			Log.error(e);
		}
	}

	public static Map<String, VCard> getVCardMap() {
		List<VCard> loginList = getAppDao().getLoginList();

		Map<String, VCard> vCardMap = new LinkedHashMap<String, VCard>();

		for (final VCard u : loginList) {
			vCardMap.put(u.getTel(), u);
		}
		return vCardMap;
	}

	public static XmppService getXmppService() {
		return xmppService;
	}

	public static LocalManager getLocalManager() {
		return localManager;
	}

	public static void addPacketFilter(PacketFilter filter) {
		if (filters == null) {
			filters = new ArrayList<PacketFilter>();
		}
		filters.add(filter);
	}

	public static void addPacketListener() {
		if (filters == null) {
			return;
		}
		for (PacketFilter filter : filters) {
			addPacketListener(packetListener, filter);
		}
	}

	private static void addPacketListener(PacketListener packetListener, PacketFilter filter) {
		// Add the packetListener to this instance
		getConnection().addPacketListener(packetListener, filter);
	}

	/**
	 * 登录服务
	 */
	private static HttpLoginService httpLoginService;

	public static void setLoginService(HttpLoginService httpLoginService) {
		TalkManager.httpLoginService = httpLoginService;
	}

	public static HttpLoginService getLoginService() {
		return TalkManager.httpLoginService;
	}

	/**
	 * 好友及群组服务
	 */
	// Added by Charith
	// password reset
	private static HttpPasswordService httpPasswordService;

	public static HttpPasswordService getHttpPasswordService() {
		return httpPasswordService;
	}

	public static void setHttpPasswordService(HttpPasswordService httpPasswordService) {
		TalkManager.httpPasswordService = httpPasswordService;
	}

	/*
	 * Added by Charith Invite Friends
	 */
	private static HttpInviteFriendService httpInviteFriendService;

	public static HttpInviteFriendService getHttpInviteFriendService() {
		return httpInviteFriendService;
	}

	public static void setHttpInviteFriendService(HttpInviteFriendService httpInviteFriendService) {
		TalkManager.httpInviteFriendService = httpInviteFriendService;
	}

	private static HttpFindFriendsService httpfindFriendsService;

	public static HttpFindFriendsService getHttpfindFriendsService() {
		return httpfindFriendsService;
	}

	public static void setHttpfindFriendsService(HttpFindFriendsService httpfindFriendsService) {
		TalkManager.httpfindFriendsService = httpfindFriendsService;
	}

	private static HttpTalkService httpTalkService;

	public static void setTalkService(HttpTalkService httpTalkService) {
		TalkManager.httpTalkService = httpTalkService;
	}

	public static HttpTalkService getTalkService() {
		return TalkManager.httpTalkService;
	}

	/**
	 * 文件服务,文件上传下载
	 */
	private static HttpFileService httpFileService;

	public static void setFileService(HttpFileService httpFileService) {
		TalkManager.httpFileService = httpFileService;
	}

	public static HttpFileService getFileService() {
		return TalkManager.httpFileService;
	}

	/**
	 * 本地数据库
	 */
	private static TalkDao talkDao;

	public static void setTalkDao(TalkDao dao) {
		TalkManager.talkDao = dao;
	}

	public static TalkDao getTalkDao() {
		return TalkManager.talkDao;
	}

	/**
	 * 名片管理,管理用户信息
	 */
	private static VCardManager vcardManager;

	public static VCardManager getVCardManager() {
		return TalkManager.vcardManager;
	}

	public static void setVCardManager(VCardManager vcardManager) {
		TalkManager.vcardManager = vcardManager;
	}

	/**
	 * xmpp消息监听
	 */
	private static PacketListener packetListener;

	public static PacketListener getPacketListener() {
		return TalkManager.packetListener;
	}

	public static void setPacketListener(PacketListener packetListener) {
		TalkManager.packetListener = packetListener;
	}

	/**
	 * xmpp和talk消息转换器
	 */
	private static MessageConverter messageConvert;

	public static MessageConverter getMessageConverter() {
		return TalkManager.messageConvert;
	}

	public static void setMessageConverter(MessageConverter messageConverter) {
		TalkManager.messageConvert = messageConverter;
	}

	/**
	 * action
	 */
	private static Map<String, List<Class<? extends XAction>>> actionClassListMap = new HashMap<String, List<Class<? extends XAction>>>();

	public static List<Class<? extends XAction>> getActionClasses(String key) {
		return actionClassListMap.get(key);
	}

	public static void addActionClasses(String key, List<Class<? extends XAction>> actionList) {
		if (actionList == null)
			return;
		List<Class<? extends XAction>> list = actionClassListMap.get(key);
		if (list == null) {
			list = new ArrayList<>();
			actionClassListMap.put(key, list);
		}
		list.addAll(actionList);
	}

	public static void addActionClasses(String key, Class<? extends XAction>... actions) {
		if (actions == null)
			return;
		addActionClasses(key, Arrays.asList(actions));
	}

	/**
	 * 控件
	 */
	private static Map<String, Class<? extends Component>> componentClassMap = new HashMap<String, Class<? extends Component>>();

	public static Class<? extends Component> getComponentClass(String key) {
		return componentClassMap.get(key);
	}

	public static void addComponentClass(String key, Class<? extends Component> componentClass) {
		if (componentClass == null)
			return;
		componentClassMap.put(key, componentClass);
	}
}
