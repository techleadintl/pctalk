/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 下午3:50:04
 * 
 ***************************************************************/
package com.xinwei.talk.manager;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.xinwei.talk.common.Log;
import com.xinwei.talk.listener.MainWindowAdapter;
import com.xinwei.talk.ui.plugin.Plugin;

public class PluginManager extends MainWindowAdapter {
	private final List<Plugin> plugins = new ArrayList<Plugin>();

	private static PluginManager singleton;

	public synchronized static PluginManager getInstance() {
		if (null == singleton) {
			singleton = new PluginManager();
		}
		return singleton;
	}

	private PluginManager() {

	}

	/**
	 * Registers a plugin.
	 *
	 * @param plugin
	 *            the plugin to register.
	 */
	public void put(Plugin plugin) {
		plugins.add(plugin);
	}

	/**
	 * Removes a plugin from the plugin list.
	 *
	 * @param plugin
	 *            the plugin to remove.
	 */
	public void remove(Plugin plugin) {
		plugins.remove(plugin);
	}

	/**
	 * Returns a Collection of Plugins.
	 *
	 * @return a Collection of Plugins.
	 */
	public Collection<Plugin> gets() {
		return plugins;
	}

	/**
	 * Returns the instance of the plugin class initialized during startup.
	 *
	 * @param communicatorPlugin
	 *            the plugin to find.
	 * @return the instance of the plugin.
	 */
	public Plugin getPlugin(Class<? extends Plugin> communicatorPlugin) {
		for (Object o : gets()) {
			Plugin plugin = (Plugin) o;
			if (plugin.getClass() == communicatorPlugin) {
				return plugin;
			}
		}
		return null;
	}

	/**
	 * Loads and initalizes all Plugins.
	 *
	 * @see Plugin
	 */
	public void initializes() {
		//		try {
		//			EventQueue.invokeLater(new Runnable() {
		//				public void run() {
		//					for (Plugin plugin1 : plugins) {
		//						long start = System.currentTimeMillis();
		//						Log.debug("Trying to initialize " + plugin1);
		//						try {
		//							plugin1.initialize();
		//						} catch (Throwable e) {
		//							Log.error(e);
		//						}
		//
		//						long end = System.currentTimeMillis();
		//						Log.debug("Took " + (end - start) + " ms. to load " + plugin1);
		//					}
		//				}
		//			});
		//		} catch (Exception e) {
		//			e.printStackTrace();
		//		}
		try {
			for (Plugin plugin1 : plugins) {
				long start = System.currentTimeMillis();
				Log.debug("Trying to initialize " + plugin1);
				try {
					plugin1.initialize();
				} catch (Throwable e) {
					Log.error(e);
				}

				long end = System.currentTimeMillis();
				Log.debug("Took " + (end - start) + " ms. to load " + plugin1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void shutdown() {
		for (Plugin plugin : plugins) {
			try {
				plugin.shutdown();
			} catch (Exception e) {
				Log.warn("Exception on shutdown of plugin.", e);
			}
		}
	}

}