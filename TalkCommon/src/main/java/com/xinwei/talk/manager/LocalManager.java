package com.xinwei.talk.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.model.Contact;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.MyTalk;
import com.xinwei.talk.model.Talk;

public class LocalManager {
	//	private static Map<String, MultiUserChat> multiUserChatMap = new HashMap<String, MultiUserChat>();

	//	//联系人
	//	//id,contact
	//	public Map<Long, Contact> contactMap = new LinkedHashMap<>();
	//	//uid,Talk
	//	public Map<Long, Talk> talkMap = new LinkedHashMap<>();

	//	public Map<String, ImageIcon> avatarMap = Collections.synchronizedMap(new LinkedHashMap<String, ImageIcon>());

	//<UUID,HttpContact>
	public Map<String, Contact> contactMap = Collections.synchronizedMap(new LinkedHashMap<String, Contact>());
	//<Phone,HttpContact>
	public Map<String, Map<String, Contact>> contactPhoneMap = Collections.synchronizedMap(new LinkedHashMap<String, Map<String, Contact>>());

	//<userTel,Talk>
	public Map<String, Talk> talkMap = Collections.synchronizedMap(new LinkedHashMap<String, Talk>());

	//<userTel,Talk>
	public Map<String, Talk> otherTalkMap = Collections.synchronizedMap(new LinkedHashMap<String, Talk>());

	//<resource,Talk>
	public Map<String, Talk> myTalkMap = Collections.synchronizedMap(new LinkedHashMap<String, Talk>());

	//<groupId,group>
	public Map<String, Group> groupMap = Collections.synchronizedMap(new LinkedHashMap<String, Group>());

	//<gid, Map<memberTel, Member>>
	public Map<String, Map<String, Member>> groupMemberMap = Collections.synchronizedMap(new LinkedHashMap<String, Map<String, Member>>());

	//<UID,ImageIcon>
	public Map<String, ImageIcon> avatarMap = Collections.synchronizedMap(new LinkedHashMap<String, ImageIcon>());

	public Map<Object, Object> objectMap = new HashMap<Object, Object>();

	public void put(Object key, Object value) {
		objectMap.put(key, value);
	}

	public <T> T get(Object key) {
		return (T) objectMap.get(key);
	}

	//	public void putMultiUserChat(String gid, MultiUserChat multiUserChat) {
	//		multiUserChatMap.put(gid, multiUserChat);
	//	}
	//
	//	public MultiUserChat getMultiUserChat(String gid) {
	//		return multiUserChatMap.get(gid);
	//	}
	//
	//	public MultiUserChat removeMultiUserChat(String gid) {
	//		return multiUserChatMap.remove(gid);
	//	}

	//	public ImageIcon getAvatar(String id) {
	//		return avatarMap.get(id);
	//	}
	//
	//	public void addAvatar(Avatar avatar) {
	//		if (avatar == null)
	//			return;
	//		String id = avatar.getId();
	//
	//		avatarMap.put(id, new ImageIcon(avatar.getAvatar()));
	//
	//		Set<String> entries = new HashSet<String>();
	//		entries.add(avatar.getId());
	//		for (AvatarListener listener : avatarListeners) {
	//			listener.updateAvatar(entries);
	//		}
	//	}
	//
	//	public void addAvatars(McWillList2<Avatar> avatarList) {
	//		if (avatarList == null)
	//			return;
	//
	//		Set<String> entries = new HashSet<String>();
	//		for (Avatar avatar : avatarList) {
	//			String id = avatar.getId();
	//			avatarMap.put(id, new ImageIcon(avatar.getAvatar()));
	//			entries.add(avatar.getId());
	//		}
	//
	//		for (AvatarListener listener : avatarListeners) {
	//			listener.updateAvatar(entries);
	//		}
	//	}
	public void putMyTalk(MyTalk talk) {
		if (talk == null)
			return;

		myTalkMap.put(talk.getRes(), talk);
	}

	public List<Talk> getMyTalkList() {
		return new ArrayList<>(myTalkMap.values());
	}

	public Talk getMyTalk(String res) {
		if (res == null)
			return null;
		return myTalkMap.get(res);
	}

	public void putOtherTalk(Talk talk) {
		if (talk == null)
			return;

		otherTalkMap.put(TalkUtil.getUserTel(talk.getJid()), talk);
	}

	public void putTalk(Talk talk) {
		if (talk == null)
			return;
		talkMap.put(TalkUtil.getUserTel(talk.getJid()), talk);
	}

	public void putTalkList(List<? extends Talk> talkList) {
		if (talkList == null)
			return;

		for (Talk talk : talkList) {
			putTalk(talk);
		}
	}

	public void removeTalk(Talk talk) {
		if (talk == null)
			return;

		talkMap.remove(TalkUtil.getUserTel(talk.getJid()));
	}

	public void removeTalkList(List<? extends Talk> talkList) {
		for (Talk talk : talkList) {
			removeTalk(talk);
		}
	}

	public Collection<Talk> getTalkList() {
		return talkMap.values();
	}

	public Talk getTalk(String tel) {
		if (tel == null)
			return null;
		Talk talk = talkMap.get(tel);
		if (talk == null) {
			talk = otherTalkMap.get(tel);
		}
		//		if (talk == null) {
		//			talk = myTalkMap.get(tel);
		//		}
		return talk;
	}

	public boolean containsTalk(String tel) {
		if (tel == null)
			return false;
		Talk talk = talkMap.get(tel);
		if (talk == null) {
			talk = otherTalkMap.get(tel);
		}
		//		if (talk == null) {
		//			talk = myTalkMap.get(tel);
		//		}
		return talk != null;
	}

	public Talk getTalkByJid(String jid) {
		String tel = TalkUtil.getUserTel(jid);
		return getTalk(tel);
	}

	public Talk queryTalkByJid(String jid) {
		String tel = TalkUtil.getUserTel(jid);
		return getTalk(tel);
	}
	//	public List<Talk> getTalk(Contact contact) {
	//		if (contact == null) {
	//			return null;
	//		}
	//
	//		List<String> phoneList = contact.getPhoneList();
	//		if (phoneList == null || phoneList.isEmpty()) {
	//			return null;
	//		}
	//		List<Talk> result = new ArrayList<Talk>();
	//		for (String phone : phoneList) {
	//			Talk camTalk = getTalk(phone);
	//			if (camTalk != null) {
	//				result.add(camTalk);
	//			}
	//		}
	//		return result;
	//	}

	public String getContactName(String talkTel) {
		if (talkTel == null) {
			return null;
		}

		Map<String, Contact> map = contactPhoneMap.get(talkTel);
		if (map == null || map.isEmpty())
			return null;
		Collection<Contact> values = map.values();
		for (Contact c : values) {
			String displayName = c.getDisplayName();
			if (StringUtil.isNotBlank(displayName)) {
				return displayName;
			}
			String firstName = c.getFirstName();
			if (StringUtil.isNotBlank(firstName)) {
				return firstName;
			}
			String lastName = c.getLastName();
			if (StringUtil.isNotBlank(lastName)) {
				return lastName;
			}
		}
		return "";
	}

	public Contact getContactByTel(String talkTel) {
		if (talkTel == null) {
			return null;
		}

		Map<String, Contact> map = contactPhoneMap.get(talkTel);
		if (map == null || map.isEmpty())
			return null;
		Collection<Contact> values = map.values();
		for (Contact c : values) {
			return c;
		}
		return null;
	}

	public void putContactList(Collection<Contact> contactList) {
		if (contactList == null)
			return;

		for (Contact contact : contactList) {
			String id = contact.getId();
			contactMap.put(id, contact);

			putContactPhone(contact);
		}
	}

	protected void putContactPhone(Contact contact) {
		List<String> phoneList = contact.getPhoneList();
		if (phoneList == null || phoneList.isEmpty()) {
			return;
		}
		for (String phone : phoneList) {
			if (StringUtil.isNotBlank(phone)) {
				Map<String, Contact> map = contactPhoneMap.get(phone.trim());
				if (map == null) {
					map = new HashMap<String, Contact>();
					contactPhoneMap.put(phone.trim(), map);
				}
				map.put(contact.getId(), contact);
			}
		}
	}

	protected void removeContactPhone(Contact contact) {
		List<String> phoneList = contact.getPhoneList();
		if (phoneList == null || phoneList.isEmpty()) {
			return;
		}
		for (String phone : phoneList) {
			if (StringUtil.isNotBlank(phone)) {
				Map<String, Contact> map = contactPhoneMap.get(phone.trim());
				if (map == null) {
					continue;
				}
				map.remove(contact.getId());
			}
		}
	}

	public void putContact(Contact contact) {
		if (contact == null)
			return;
		String id = contact.getId();

		contactMap.put(id, contact);

		putContactPhone(contact);
	}

	public void removeContactList(List<Contact> contactList) {
		if (contactList == null)
			return;

		for (Contact contact : contactList) {
			removeContact(contact);
		}
	}

	public void removeContact(Contact contact) {
		if (contact == null)
			return;
		contactMap.remove(contact.getId());
		removeContactPhone(contact);
	}

	public boolean containsKey(String id) {
		return contactMap.containsKey(id);
	}

	public List<Contact> getContactList() {
		return new ArrayList<Contact>(contactMap.values());
	}

	public void replaceContactList(List<Contact> contactList) {
		contactMap.clear();
		contactPhoneMap.clear();
		putContactList(contactList);
	}
	//	public XWUser getUserByTel(String tel) {
	//		Collection<XWUser> values = userMap.values();
	//		for (XWUser camTalk : values) {
	//			if (tel.equals(camTalk.getTel())) {
	//				return camTalk;
	//			}
	//		}
	//		return null;
	//	}
	//
	//	public XWUser getUserByUid(String uid) {
	//		Collection<XWUser> values = userMap.values();
	//		for (XWUser user : values) {
	//			if (uid.equals(user.getUid())) {
	//				return user;
	//			}
	//		}
	//		return null;
	//	}

	/**************************************** 组 ************************************/
	public void putGroupList(List<Group> groupList) {
		if (groupList == null)
			return;

		for (Group group : groupList) {
			putGroup(group);
		}
	}

	public void putGroup(Group group) {
		if (group == null)
			return;
		groupMap.put(group.getId(), group);
	}

	public void removeGroupList(List<Group> groupList) {
		if (groupList == null)
			return;

		for (Group group : groupList) {
			removeGroup(group);
		}
	}

	public void removeGroup(Group group) {
		if (group == null)
			return;
		String id = group.getId();
		groupMap.remove(id);
	}

	public boolean containsGroup(Group group) {
		if (group == null)
			return false;
		String id = group.getId();
		return groupMap.containsKey(id);
	}

	public List<Group> getGroupList() {
		return new ArrayList<>(groupMap.values());
	}

	public Group getGroup(String gid) {
		return groupMap.get(gid);
	}

	public Group getGroupByJid(String jid) {
		String gid = TalkUtil.getGroupId(jid);
		return groupMap.get(gid);
	}

	/**************************************** 组成员 ************************************/
	public String getMemberNick(String gid, String userTel) {
		Member member = getMemberByTel(gid, userTel);

		if (member == null) {
			return "";
		}
		String memNick = member.getMemNick();
		if (memNick == null) {
			return "";
		}
		return memNick;
	}

	public Member getMemberByUid(String gid, String uid) {
		if (uid == null) {
			return null;
		}

		Map<String, Member> map = groupMemberMap.get(gid);
		if (map == null)
			return null;

		Collection<Member> values = map.values();
		for (Member member : values) {
			String uid2 = member.getUid();
			if (uid.equals(uid2)) {
				return member;
			}
		}
		return null;
	}

	public Member getMemberByJid(String gid, String jid) {
		if (jid == null) {
			return null;
		}
		String memberTel = TalkUtil.getUserTel(jid);

		return getMemberByTel(gid, memberTel);
	}

	public Member getMemberByGroupJidAndUserTel(String groupJid, String userTel) {
		if (userTel == null || groupJid == null) {
			return null;
		}
		String gid = TalkUtil.getGroupId(groupJid);
		return getMemberByTel(gid, userTel);
	}

	public Member getMemberByTel(String gid, String userTel) {
		if (userTel == null) {
			return null;
		}

		Map<String, Member> map = groupMemberMap.get(gid);
		if (map == null)
			return null;
		return map.get(userTel);
	}

	public List<Member> getMemberList() {
		List<Member> r = new ArrayList<Member>();
		Collection<Map<String, Member>> values = groupMemberMap.values();

		for (Map<String, Member> map : values) {
			Collection<Member> values2 = map.values();
			r.addAll(values2);
		}
		return r;
	}

	public List<Member> getMemberList(String gid) {
		Map<String, Member> map = groupMemberMap.get(gid);
		List<Member> r = new ArrayList<Member>();
		if (map != null) {
			r.addAll(map.values());
		}
		return r;
	}

	public List<Member> getMemberListByJid(String jid) {
		String groupId = TalkUtil.getGroupId(jid);
		return getMemberList(groupId);
	}

	public void putMember(Member member) {
		List<Member> list = new ArrayList<Member>();
		list.add(member);
		putMemberList(list);
	}

	public void putMemberList(List<Member> members) {
		if (CollectionUtil.isEmpty(members))
			return;
		for (Member member : members) {
			String gid = member.getGid();
			Map<String, Member> map = groupMemberMap.get(gid);
			if (map == null) {
				map = new HashMap<String, Member>();
				groupMemberMap.put(gid, map);
			}
			map.put(TalkUtil.getUserTel(member.getMemJid()), member);
		}
	}

	public void removeMemberList(List<Group> groupList) {
		if (groupList == null)
			return;

		for (Group group : groupList) {
			removeMemberListByGid(group.getId());
		}
	}

	public void removeMemberListByGid(String gid) {
		groupMemberMap.remove(gid);
	}

	public void removeMemberList(String gid, List<Member> members) {
		Map<String, Member> map = groupMemberMap.get(gid);
		if (map == null) {
			return;
		}
		for (Member member : members) {
			map.remove(TalkUtil.getUserTel(member.getMemJid()));
		}
	}

	public void removeMember(Member member) {
		Map<String, Member> map = groupMemberMap.get(member.getGid());
		if (map == null) {
			return;
		}
		map.remove(TalkUtil.getUserTel(member.getMemJid()));
	}

	/*************************************************************************************/

}
