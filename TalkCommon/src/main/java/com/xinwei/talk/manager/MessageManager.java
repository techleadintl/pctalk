/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月24日 上午9:42:34
 * 
 ***************************************************************/
package com.xinwei.talk.manager;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import com.xinwei.common.lang.ByteArrayUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SyncLock;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.common.vo.Entry;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.Constants;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.dao.TalkDao;
import com.xinwei.talk.listener.MessageListener;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.utils.MessageType;

/**
 * 消息记录
 * 
 * @author liyong
 *
 */
public class MessageManager {
	private static MessageManager INSTANCE;

	BlockingQueue<TalkMessage> storeQueue = new LinkedBlockingQueue<TalkMessage>();

	Object unreadCacheLock = new Object();
	Map<String, Entry<Integer, TalkMessage>> unreadCache = new HashMap<String, Entry<Integer, TalkMessage>>();

	Object sendCacheLock = new Object();
	Map<String, TalkMessage> sendCache = new HashMap<String, TalkMessage>();

	Object messageCacheLock = new Object();
	Map<String, List<TalkMessage>> messageCache = new HashMap<String, List<TalkMessage>>();

	TalkDao dao;

	long maxMessageId;

	private String PACKAGE_PREFIX = "P";
	//
	private long packageId = 0;

	private MessageManager() {
		dao = TalkManager.getTalkDao();
		maxMessageId = dao.getMessageMaxId();

		ListenerManager.addMessageListener(new MessageListener() {
			@Override
			public void messageSent(String to, TalkMessage message) {
				if (StringUtil.isNotEmpty(message.getTalkBody().getPacketId())) {
					synchronized (sendCacheLock) {
						if(null!= message.getMessageType() && message.getMessageType().equals(MessageType.NOTIFY))
							return;
						sendCache.put(message.getTalkBody().getPacketId(), message);
					}
				}
				cacheTranscriptMessage(message);
			}

			@Override
			public void messageReceived(String from, TalkMessage message) {
				cacheTranscriptMessage(message);

				int status = message.getReaded();
				String jid = message.getJid();

				if (jid == null) {
					return;
				}
				storeQueue.add(message);

				if (status == TalkMessage.STATUS_UNREAD && !MessageUtil.isChatMessage(message.getMsgType(), Constants.MSG_TYPE_FILE)) {
					increaseUnreadMessageCount(jid, message);
				}
			}
		});

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				startStore();
			}
		});
		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				startClearTimeout();
			}
		});

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				initUnreadCache();
			}
		});

	}

	public static synchronized MessageManager getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new MessageManager();
		}
		return INSTANCE;
	}

	private void cacheTranscriptMessage(TalkMessage message) {
		String transcriptId = message.getJid();
		if (transcriptId == null) {
			return;
		}
		synchronized (messageCacheLock) {
			List<TalkMessage> list = messageCache.get(transcriptId);
			if (list == null) {
				list = new ArrayList<TalkMessage>();
				messageCache.put(transcriptId, list);
			}
			list.add(message);
		}
	}

	public List<TalkMessage> getCacheMessageList(String jid) {
		synchronized (messageCacheLock) {
			return messageCache.get(jid);
		}
	}

	public Map<String, Object> getMessageTimeInfo(String jid, String searchStr, Date searchStartTime, Date searchEndTime) {
		return dao.getMessageTimeInfo(jid, searchStr, searchStartTime, searchEndTime);
	}

	public List<TalkMessage> getMessageList(String jid, String searchStr, long id, int num, boolean next) {
		return dao.getMessageList(jid, searchStr, id, num, next);
	}

	public List<TalkMessage> getMessageList(String jid, Date date, int num) {
		return dao.getMessageList(jid, date, num);
	}

	public List<TalkMessage> getMessageList(String jid, int msgType) {
		return dao.getMessageList(jid, msgType);
	}

	public void receiptSendedMessage(String packetId, Date date) {
		if (packetId == null || date == null)
			return;
		TalkMessage message = null;
		synchronized (sendCacheLock) {
			message = sendCache.remove(packetId);
		}
		if (message != null && message.getTalkBody() != null) {
			message.getTalkBody().setTime(date);
			storeQueue.add(message);
		}

		SyncLock.setContent(packetId, date);
		SyncLock.unlock(packetId);
	}

	public Entry<Integer, TalkMessage> getUnreadMessage(String jid) {
		synchronized (unreadCacheLock) {
			Entry<Integer, TalkMessage> entry = unreadCache.get(jid);
			return entry;
		}
	}

	public Map<String, Entry<Integer, TalkMessage>> getUnreadMessageMap() {
		return unreadCache;
	}

	/**
	 * Increases the number of unread messages by 1.
	 */
	public void increaseUnreadMessageCount(String jid, TalkMessage message) {
		Entry<Integer, TalkMessage> unreadMessage = getUnreadMessage(jid);
		if (unreadMessage == null) {
			unreadMessage = new Entry<Integer, TalkMessage>(1, message);
		} else {
			Integer key = unreadMessage.getKey();
			unreadMessage.setKey(key + 1);
			unreadMessage.setValue(message);
		}
		synchronized (unreadCacheLock) {
			unreadCache.put(jid, unreadMessage);
		}
		ListenerManager.fireUnreadListener(jid, unreadMessage);
	}

	/**
	 * Resets the number of unread messages.
	 */
	public void clearUnreadMessageCount(String jid) {
		dao.updateMessageStatus(jid, TalkMessage.STATUS_READED);
		synchronized (unreadCacheLock) {
			unreadCache.remove(jid);
		}
		ListenerManager.fireUnreadListener(jid, null);
	}

	public boolean hasSendMessage(String jid) {
		synchronized (sendCacheLock) {
			Collection<TalkMessage> values = sendCache.values();
			for (TalkMessage msg : values) {
				if (StringUtil.equals(msg.getJid(), jid)) {
					return true;
				}
			}
			return false;
		}
	}

	public boolean isSendMessage(long msgId) {
		synchronized (sendCacheLock) {
			Collection<TalkMessage> values = sendCache.values();
			for (TalkMessage msg : values) {
				if (msg.getId() == msgId) {
					return true;
				}
			}
			return false;
		}
	}

	public boolean clearMessage(String jid) {
		synchronized (sendCacheLock) {
			int r = dao.removeMessageList(jid);
			if (r == -1)
				return false;
			synchronized (unreadCacheLock) {
				unreadCache.remove(jid);
			}
			synchronized (messageCacheLock) {
				messageCache.remove(jid);
			}
			ListenerManager.fireUnreadListener(jid, null);
			return true;
		}
	}

	public boolean deleteMessage(String jid, long msgId) {
		synchronized (sendCacheLock) {
			//只删消息不删相关资源文件
			int r = dao.deleteMessage(jid, msgId);
			if (r == -1)
				return false;
			synchronized (messageCacheLock) {
				List<TalkMessage> list = messageCache.get(jid);
				if (list != null) {
					Iterator<TalkMessage> iterator = list.iterator();
					while (iterator.hasNext()) {
						TalkMessage next = iterator.next();
						if (next.getId() == msgId) {
							iterator.remove();
						}
					}
				}
			}
			return true;
		}
	}

	public synchronized long nextMessageId() {
		return ++maxMessageId;
	}

	public synchronized String nextPackageId() {
		return StringUtil.join("_", PACKAGE_PREFIX + TalkManager.getVCard().getTel(), System.currentTimeMillis(), packageId++);
//		return StringUtil.join("_", "M" + TalkManager.getVCard().getTel(), System.currentTimeMillis());
	}

	public boolean isSlefPackageId(String id) {
		return id.startsWith(PACKAGE_PREFIX);
	}

	public static long getIpLong() {
		try {
			InetAddress ia = InetAddress.getLocalHost();

			String ipAddress = ia.getHostAddress();

			String[] addrArray = ipAddress.split("\\.");

			long num = 0;
			for (int i = 0; i < addrArray.length; i++) {

				int power = 3 - i;

				num += ((Integer.parseInt(addrArray[i]) % 256 * Math.pow(256, power)));

			}
			return num;

		} catch (Exception e) {
		}
		return System.currentTimeMillis();
	}

	private void startStore() {
		TalkDao camTalkDao = TalkManager.getTalkDao();
		while (true) {
			if (!storeQueue.isEmpty()) {
				List<TalkMessage> cl = new ArrayList<TalkMessage>();
				Date date = null;
				do {
					TalkMessage historyMessage = storeQueue.remove();
					Date d = historyMessage.getTalkBody().getTime();
					if (d != null) {
						date = d;
					}
					cl.add(historyMessage);
				} while (!storeQueue.isEmpty());

				if (!cl.isEmpty()) {
					camTalkDao.addMessageList(cl);
					fireRecentMessage(date, cl);
				}
			} else {
				SystemUtil.sleep(100);
			}
		}
	}

	private void startClearTimeout() {
		while (true) {
			boolean flag = false;
			synchronized (sendCacheLock) {
				flag = sendCache.size() < 50;
			}

			if (flag) {
				SystemUtil.sleep(2000);
			} else {
				synchronized (sendCacheLock) {
					Map<String, TalkMessage> cache = new ConcurrentHashMap<String, TalkMessage>(sendCache);
					Set<Map.Entry<String, TalkMessage>> entrySet = cache.entrySet();
					long currentTimeMillis = System.currentTimeMillis();
					for (Map.Entry<String, TalkMessage> entry : entrySet) {
						TalkMessage value = entry.getValue();
						String key = entry.getKey();
						Date time = value.getTalkBody().getTime();
						if (time != null && currentTimeMillis - time.getTime() > 1000 * 300) {
							sendCache.remove(key);
						}
					}
				}
			}
		}
	}

	private void initUnreadCache() {
		int num = 25;

		//最近消息
		List<TalkMessage> recentList = dao.getMessageRecentList(num);
		if (recentList != null && !recentList.isEmpty()) {
			Date serverTime = TalkManager.getXmppService().getServerDate();
			if (recentList.size() > num) {
				List<TalkMessage> subList = recentList.subList(0, num);
				fireRecentMessage(serverTime, subList);
			} else {
				fireRecentMessage(serverTime, recentList);
			}
			for (TalkMessage message : recentList) {
				cacheTranscriptMessage(message);
			}
		}

		//未阅读消息
		List<TalkMessage> lastMessageList = dao.getLastMessageList(TalkMessage.STATUS_UNREAD);
		if (lastMessageList != null) {
			for (TalkMessage msg : lastMessageList) {
				unreadCache.put(msg.getJid(), new Entry<Integer, TalkMessage>(msg.getCount(), msg));
			}
			List<TalkMessage> messageList = dao.getMessageList(TalkMessage.STATUS_UNREAD);
			for (TalkMessage message : messageList) {
				cacheTranscriptMessage(message);
			}
		}

		//抛出事件，通知界面更新
		Set<Map.Entry<String, Entry<Integer, TalkMessage>>> entrySet = unreadCache.entrySet();
		for (Map.Entry<String, Entry<Integer, TalkMessage>> entry : entrySet) {
			ListenerManager.fireUnreadListener(entry.getKey(), entry.getValue());
		}
	}

	public void fireRecentMessage(final Date serverTime, final List<TalkMessage> tmList) {
		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				ListenerManager.fireRecentMessage(serverTime, tmList);
			}
		});
	}
}
