/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 下午2:28:52
 * 
 ***************************************************************/
package com.xinwei.talk.manager;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JButton;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.spark.ChatPrinter;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.listener.ChatMessageHandler;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.ui.chat.ChatContainer;
import com.xinwei.talk.ui.chat.ChatFrame;
import com.xinwei.talk.ui.chat.TranscriptWindow;
import com.xinwei.talk.ui.chat.TranscriptWindowInterceptor;
import com.xinwei.talk.ui.chat.room.ChatRoom;
import com.xinwei.talk.ui.chat.room.GroupChatRoom;
import com.xinwei.talk.ui.chat.room.MessageChatRoom;
import com.xinwei.talk.ui.chat.room.TalkChatRoom;
import com.xinwei.talk.ui.util.component.XWConfirmDialog;

public class ChatManager implements ChatManagerListener {
	private static ChatManager singleton;

	private List<ChatMessageHandler> chatMessageHandlers = new ArrayList<ChatMessageHandler>();
	/** 消息拦截器 **/
	private List<TranscriptWindowInterceptor> interceptors = new ArrayList<>();
	private final ChatContainer chatContainer;

	public static synchronized ChatManager getInstance() {
		if (null == singleton) {
			singleton = new ChatManager();
		}
		return singleton;
	}

	/**
	 * Create a new instance of ChatManager.
	 */
	private ChatManager() {
		chatContainer = new ChatContainer();

		// Add Default Chat Room Decorator
		//		addSparkTabHandler(new DefaultTabHandler());

		// Add a Msg Handler        
		TalkManager.getConnection().getChatManager().addChatListener(this);
	}

	/**
	 * Displays a print dialog to print the transcript found in a
	 * <code>TranscriptWindow</code>
	 *
	 * @param transcriptWindow
	 *            the <code>TranscriptWindow</code> containing the transcript.
	 */
	public void printChatTranscript(TranscriptWindow transcriptWindow) {
		final ChatPrinter printer = new ChatPrinter();
		printer.print(transcriptWindow);
	}

	public void addChatMessageHandler(ChatMessageHandler handler) {
		chatMessageHandlers.add(handler);
	}

	public void removeChatMessageHandler(ChatMessageHandler handler) {
		chatMessageHandlers.remove(handler);
	}

	public void addTranscriptWindowInterceptor(TranscriptWindowInterceptor interceptor) {
		interceptors.add(interceptor);
	}

	/**
	 * Removes a TranscriptWindowInterceptor.
	 *
	 * @param interceptor
	 *            the interceptor.
	 */
	public void removeTranscriptWindowInterceptor(TranscriptWindowInterceptor interceptor) {
		interceptors.remove(interceptor);
	}

	/**
	 * Returns the list of <code>TranscriptWindowInterceptors</code>.
	 *
	 * @return the list of interceptors.
	 */
	public Collection<TranscriptWindowInterceptor> getTranscriptWindowInterceptors() {
		return interceptors;
	}

	/**
	 * Returns all ChatRooms currently active.
	 *
	 * @return all ChatRooms.
	 */
	public ChatContainer getChatContainer() {
		return chatContainer;
	}

	@Override
	public void chatCreated(Chat arg0, boolean arg1) {

	}

	public void activateChatRoom(String jid) {
		LocalManager localManager = TalkManager.getLocalManager();
		if (TalkUtil.isGroup(jid)) {
			Group group = localManager.getGroupByJid(jid);
			if (group == null) {
				group = new Group();
				group.setJid(jid);
				group.setId(TalkUtil.getGroupId(jid));
			}
			activateChatRoom(group);
		} else {
			Talk camTalk = localManager.getTalkByJid(jid);
			if (camTalk == null) {
				camTalk = new Talk();
				camTalk.setJid(jid);
				camTalk.setTel(TalkUtil.getUserTel(jid));
			}
			activateChatRoom(camTalk);
		}
	}

	public ChatRoom activateChatRoom(Object obj) {
		if (obj instanceof Talk) {
			Talk camTalk = (Talk) obj;
			String roomId = camTalk.getJid();
			String roomTitle = TalkUtil.getDisplayName(camTalk.getJid());
			String roomDesc = camTalk.getSignature();
			return activateChatRoom(roomId, roomTitle, roomDesc, false, true);			// changed may 18
		} else if (obj instanceof Group) {
			Group group = (Group) obj;
			String roomId = TalkUtil.getGroupFullJid(group.getId());
			String roomTitle = TalkUtil.getDisplayName(group.getJid());
			String roomDesc = group.getDescribe();
			String creator = group.getCreatorUid();										// added may 18
			if(creator != null){
				return activateChatRoom(roomId, roomTitle, roomDesc, true, true);			// changed may 18
			}
			else{
				return activateChatRoom(roomId, roomTitle, roomDesc, true, false);			// added may 18
			}
		}
		return null;
	}

	public ChatRoom activateChatRoom(final String roomId, String roomTitle, final String roomDesc, final boolean isGroup, boolean panel) {		// changed may 18
		if (roomId == null) {
			return null;
		}

		//如果房间没有标题，从房间ID析出标题
		if (StringUtil.isBlank(roomTitle)) {
			if (StringUtil.isNotBlank(roomId)) {
				if (isGroup) {
					roomTitle = TalkUtil.getGroupId(roomId);
				} else {
					roomTitle = TalkUtil.getUserTel(roomId);
				}
			} else {
				roomTitle = "";
			}
		}
		final String roomDisplayName = roomTitle;

		final ChatManager chatManager = TalkManager.getChatManager();

		ChatContainer chatContainer = chatManager.getChatContainer();

		ChatRoom talkRomm = chatContainer.getChatRoom(roomId);

		if (talkRomm == null) {
			if (isGroup) {
				talkRomm = new GroupChatRoom(roomId, roomDisplayName, roomDesc, panel ); // changed may 18
			} else {
				boolean messageJid = TalkUtil.isMessageJid(roomId);
				if (messageJid) {
					talkRomm = new MessageChatRoom(roomId, roomDisplayName, roomDesc);
				} else {
					talkRomm = new TalkChatRoom(roomId, roomDisplayName, roomDesc);
				}
			}

			talkRomm.initTalkMessages();

			chatManager.getChatContainer().addChatRoom(talkRomm);
		}

		chatManager.getChatContainer().activateChatRoom(talkRomm);

		return talkRomm;

	}

	public boolean closeChatRoom(ChatRoom activeChatRoom) {
		if (activeChatRoom == null)
			return true;
		String message = activeChatRoom.canClose();
		//当前有文件未传输完成
		if (message == null) {
			activeChatRoom.close(true);
			return false;
		}
		XWConfirmDialog d = new XWConfirmDialog(getChatFrame(), Res.getMessage("title.info"), message);
		d.setVisible(true);
		if (d.getResult() == XWConfirmDialog.OK) {//选择是
			activeChatRoom.close(true);
			return false;
		} else {//选择否
			return false;
		}
	}

	public boolean closeAllChatRoom(ChatRoom[] rooms) {
		if (rooms.length == 1) {//只有一个窗口
			return closeChatRooms(rooms);
		} else {//多个窗口
			//关闭此窗口所有会话还是仅关闭当前会话
			ChatFrameCloseConfirmDialog dialog = new ChatFrameCloseConfirmDialog();
			dialog.setVisible(true);
			if (dialog.selected == 0) {//关闭此窗口所有会话
				return closeChatRooms(rooms);
			} else if (dialog.selected == 1) {//仅关闭当前会话
				return closeChatRoom(chatContainer.getActiveChatRoom());
			}
		}
		return false;
	}

	private boolean closeChatRooms(ChatRoom[] rooms) {
		String message = null;
		for (ChatRoom chatRoom : rooms) {
			message = chatRoom.canClose();
			if (message != null) {
				break;
			}
		}
		//当前有文件未传输完成
		if (message == null) {
			chatContainer.clearChatRoomList();
			return true;
		}

		XWConfirmDialog d = new XWConfirmDialog(getChatFrame(), Res.getMessage("title.info"), message);
		d.setVisible(true);
		if (d.getResult() == XWConfirmDialog.OK) {//选择是
			chatContainer.clearChatRoomList();
			return true;
		} else {//选择否
			return false;
		}
	}

	public ChatFrame getChatFrame() {
		return chatContainer.getChatFrame();
	}

	public class ChatFrameCloseConfirmDialog extends XWConfirmDialog {
		private static final long serialVersionUID = 8447361099667316655L;

		private int selected = -1;

		public ChatFrameCloseConfirmDialog() {
			super(TalkManager.getChatFrame(), Res.getMessage("chatroom.close.title"), Res.getMessage("chatroom.close.label.all"));
		}

		public int getSelected() {
			return selected;
		}

		protected JButton createButton1() {
			JButton button = new JButton(Res.getMessage("chatroom.close.button.all"));
			button.setPreferredSize(new Dimension(100, 24));
			button.addActionListener(this);
			return button;
		}

		protected JButton createButton2() {
			JButton button = new JButton(Res.getMessage("chatroom.close.button.current"));
			button.setPreferredSize(new Dimension(100, 24));
			button.addActionListener(this);
			return button;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			result = OK;
			if (e.getSource() == button1) {
				selected = 0;
			} else {
				selected = 1;
			}
			setVisible(false);
			dispose();
		}
	}
}
