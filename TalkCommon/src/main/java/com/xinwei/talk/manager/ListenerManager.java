/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月27日 上午11:02:04
 * 
 ***************************************************************/
package com.xinwei.talk.manager;

import com.xinwei.common.vo.Entry;
import com.xinwei.talk.listener.*;
import com.xinwei.talk.model.Contact;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.avatar.Avatar;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.chat.room.ChatRoom;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import org.jivesoftware.smack.packet.Presence;

import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class ListenerManager {
	/** 托盘监听器， **/
	private static final List<TrayListener> trayListeners = new ArrayList<TrayListener>();

	/** 聊天窗口监听器， **/
	private static final List<WindowAdapter> chatWindowListeners = new ArrayList<WindowAdapter>();

	/** 窗口监听器，打开、关闭 **/
	private static final List<ChatRoomListener> chatRoomListeners = new ArrayList<ChatRoomListener>();
	/** 消息收發监听器 **/
	private static final List<MessageListener> messageListeners = new ArrayList<MessageListener>();

	private static final List<PresenceListener> presenceListeners = new ArrayList<PresenceListener>();
	/** 文件拖拽监听器 **/
	private static final List<ChatRoomFileDropListener> fileDropListeners = new ArrayList<>();
	/** 键盘按键拖拽监听器 **/
	private static final List<ChatRoomKeyListener> chatInputKeyListeners = new ArrayList<ChatRoomKeyListener>();
	/** 未读消息监听器 **/
	private static final List<UnreadListener> unreadListeners = new ArrayList<>();
	/** 自己名片监听器 **/
	private static final List<VCardListener> vcardListeners = new ArrayList<VCardListener>();
	/** 用户头像监听器 **/
	private static final List<AvatarListener> avatarListeners = new ArrayList<AvatarListener>();
	/** 联系人监听器 **/
	private static final List<ContactListener> contactListeners = new ArrayList<ContactListener>();
	/** Talk监听器 **/
	private static final List<ObjectListener> talkListeners = new ArrayList<ObjectListener>();
	/** 组监听器 **/
	private static List<ObjectListener> groupListeners = new ArrayList<ObjectListener>();
	/** 组成员监听器 **/
	private static List<MemberListener> memberListeners = new ArrayList<MemberListener>();

	/** 最近消息监听器 **/
	private static List<RecentMessageListener> recentMessageListeners = new ArrayList<RecentMessageListener>();

	public static void addChatWindowListener(WindowAdapter listener) {
		if (!chatWindowListeners.contains(listener)) {
			chatWindowListeners.add(listener);
		}
	}

	public static List<WindowAdapter> getChatWindowListeners() {
		return chatWindowListeners;
	}

	public static void fireChatRoomOpened(ChatRoom room) {
		for (ChatRoomListener chatRoomListener : new ArrayList<ChatRoomListener>(chatRoomListeners)) {
			chatRoomListener.chatRoomOpened(room);
		}
	}

	public static void fireChatRoomClosed(ChatRoom room) {
		if (fireChatRoomCanClosed(room)) {
			for (ChatRoomListener chatRoomListener : new ArrayList<ChatRoomListener>(chatRoomListeners)) {
				chatRoomListener.chatRoomClosed(room);
			}
		}
	}

	public static boolean fireChatRoomCanClosed(ChatRoom room) {
		for (ChatRoomListener chatRoomListener : new ArrayList<ChatRoomListener>(chatRoomListeners)) {
			if (!chatRoomListener.canChatRoomClosed(room)) {
				return false;
			}
		}
		return true;
	}

	//	public static void fireChatRoomFileTabClosed(ChatRoom room) {
	//		for (ChatRoomListener chatRoomListener : new ArrayList<ChatRoomListener>(chatRoomListeners)) {
	//			chatRoomListener.chatRoomFileTabClosed(room);
	//		}
	//	}

	public static void addChatRoomListener(ChatRoomListener listener) {
		if (!chatRoomListeners.contains(listener)) {
			chatRoomListeners.add(listener);
		}
	}

	/**
	 * Removes the specified <code>ChatRoomListener</code>.
	 * 
	 * @param listener
	 *            the <code>ChatRoomListener</code> to remove
	 */
	public static void removeChatRoomListener(ChatRoomListener listener) {
		if (listener == null)
			return;
		chatRoomListeners.remove(listener);
	}

	/**
	 * Notifies all message vcardListeners that
	 * 
	 * @param message
	 *            the message received.
	 */
	public static void fireMessageSent(String toJid, TalkMessage message) {
		for (MessageListener messageListener : messageListeners) {
			messageListener.messageSent(toJid, message);
		}
	}

	/**
	 * 
	 * @param message
	 */
	public static void fireMessageReceived(String from, TalkMessage message) {
		for (MessageListener messageListener : messageListeners) {
			messageListener.messageReceived(from, message);
		}
	}

	public static void fireRecentMessage(Date serverTime, List<TalkMessage> tmList) {
		for (RecentMessageListener messageListener : recentMessageListeners) {
			messageListener.message(serverTime, tmList);
		}
	}

	/**
	 * Add a {@link MessageListener} to the current ChatRoom.
	 * 
	 * @param listener
	 *            - the MessageListener to add to the current ChatRoom.
	 */
	public static void addMessageListener(MessageListener listener) {
		messageListeners.add(listener);
	}

	public static void addPresenceListener(PresenceListener listener) {
		presenceListeners.add(listener);
	}

	public static void firePresenceReceived(String from, Presence presence) {
		for (PresenceListener presenceListener : presenceListeners) {
			presenceListener.presenceReceived(from,presence);
		}
	}

	public static void addRecentMessageListener(RecentMessageListener listener) {
		recentMessageListeners.add(listener);
	}

	/**
	 * Remove the specified {@link MessageListener } from the current ChatRoom.
	 * 
	 * @param listener
	 *            - the MessageListener to remove from the current ChatRoom.
	 */
	public static void removeMessageListener(MessageListener listener) {
		messageListeners.remove(listener);
	}

	/**
	 * Adds a new <code>FileDropListener</code> to allow for Drag and Drop notifications of objects onto the ChatWindow.
	 * 
	 * @param listener
	 *            the listener.
	 */
	public static void addFileDropListener(ChatRoomFileDropListener listener) {
		fileDropListeners.add(listener);
	}

	/**
	 * Remove the <code>FileDropListener</code> from ChatRoom.
	 * 
	 * @param listener
	 *            the listener.
	 */
	public static void removeFileDropListener(ChatRoomFileDropListener listener) {
		fileDropListeners.remove(listener);
	}

	/**
	 * Notify all users that a collection of files has been dropped onto the ChatRoom.
	 * 
	 * @param files
	 *            the files dropped.
	 */
	public static void fireFileDropListeners(CommonChatRoom chatRoom, Collection<File> files) {
		for (ChatRoomFileDropListener fileDropListener : fileDropListeners) {
			fileDropListener.filesDropped(files, chatRoom);
		}
	}

	public static void addChatInputKeyListener(ChatRoomKeyListener listener) {
		chatInputKeyListeners.add(listener);
	}

	/**
	 * Remove the <code>ChatInputKeyListener</code> from ChatRoom.
	 * 
	 * @param listener
	 *            the listener.
	 */
	public static void removeChatInputKeyListener(ChatRoomKeyListener listener) {
		chatInputKeyListeners.remove(listener);
	}

	public static void fireChatInputKeyTyped(CommonChatRoom room, KeyEvent e) {
		for (ChatRoomKeyListener listener : chatInputKeyListeners) {
			listener.keyTyped(room, e);
		}

	}

	public static void fireChatInputKeyPressed(CommonChatRoom room, KeyEvent e) {
		for (ChatRoomKeyListener listener : chatInputKeyListeners) {
			listener.keyPressed(room, e);
		}

	}

	public static void fireChatInputKeyReleased(CommonChatRoom room, KeyEvent e) {
		for (ChatRoomKeyListener listener : chatInputKeyListeners) {
			listener.keyReleased(room, e);
		}

	}

	/**
	 * Add a {@link UnreadListener}
	 * 
	 */
	public static void addUnreadListener(UnreadListener listener) {
		unreadListeners.add(listener);
	}

	/**
	 * Remove the specified {@link UnreadListener }
	 * 
	 */
	public static void removeUnreadListener(UnreadListener listener) {
		unreadListeners.remove(listener);
	}

	/**
	 * Notifies all unread vcardListeners that
	 * 
	 */
	public static void fireUnreadListener(String jid, Entry<Integer, TalkMessage> unreadMessage) {
		for (UnreadListener listener : unreadListeners) {
			listener.doUnreadMessage(jid, unreadMessage);
		}
	}

	public static void addContactListener(ContactListener listener) {
		contactListeners.add(listener);
	}

	public static void fireContactAdd(List<Contact> list) {
		for (ContactListener listener : contactListeners) {
			listener.add(list);
		}
	}

	public static void fireContactDelete(List<Contact> list) {
		for (ContactListener listener : contactListeners) {
			listener.delete(list);
		}
	}

	public static void fireContactUpdate(List<Contact> list) {
		for (ContactListener listener : contactListeners) {
			listener.update(list);
		}
	}

	public static void addTalkListener(ObjectListener listener) {
		talkListeners.add(listener);
	}

	public static void fireTalkAdd(List<? extends Talk> list) {
		for (ObjectListener listener : talkListeners) {
			listener.add(list);
		}
	}

	public static void fireTalkDelete(List<? extends Talk> list) {
		for (ObjectListener listener : talkListeners) {
			listener.delete(list);
		}
	}

	public static void fireTalkUpdate(List<? extends Talk> list) {
		for (ObjectListener listener : talkListeners) {
			listener.update(list);
		}
	}

	/**
	 * Add <code>VCardListener</code>. Listens to the personalVCard.
	 *
	 * @param listener
	 *            the listener to add.
	 */
	public static void addVCardListener(VCardListener listener) {
		vcardListeners.add(listener);
	}

	/**
	 * Remove <code>VCardListener</code>.
	 *
	 * @param listener
	 *            the listener to remove.
	 */
	public static void removeVCardListener(VCardListener listener) {
		vcardListeners.remove(listener);
	}

	/**
	 * Notify all <code>VCardListener</code> implementations.
	 */
	public static void fireVcardChanged() {
		for (VCardListener listener : vcardListeners) {
			listener.vcardChanged(TalkManager.getVCard());
		}
	}

	public static void addAvatarListener(AvatarListener listener) {
		avatarListeners.add(listener);
	}

	/**
	 * Notify all <code>AvatarListener</code> implementations.
	 */
	public static void fireAvatarChanged(List<Avatar> avatars) {
		for (AvatarListener listener : avatarListeners) {
			listener.update(avatars);
		}
	}

	public static void addGroupListener(ObjectListener listener) {
		groupListeners.add(listener);
	}

	public static void fireGroupAdd(List<Group> list) {
		for (ObjectListener listener : groupListeners) {
			listener.add(list);
		}
	}

	public static void fireGroupDelete(List<Group> list) {
		for (ObjectListener listener : groupListeners) {
			listener.delete(list);
		}
	}

	public static void fireGroupUpdate(List<Group> list) {
		for (ObjectListener listener : groupListeners) {
			listener.update(list);
		}
	}

	public static void addMemberListener(MemberListener listener) {
		memberListeners.add(listener);
	}

	public static void removeMemberListener(MemberListener listener) {
		memberListeners.remove(listener);
	}

	public static void fireMemberAdd(String gid, List<Member> list) {
		for (MemberListener listener : memberListeners) {
			listener.add(gid, list);
		}
	}

	public static void fireMemberDelete(String gid, List<Member> list) {
		for (MemberListener listener : memberListeners) {
			listener.delete(gid, list);
		}
	}

	public static void fireMemberUpdate(String gid, List<Member> list) {
		for (MemberListener listener : memberListeners) {
			listener.update(gid, list);
		}
	}

	public static void addTrayListener(TrayListener listener) {
		trayListeners.add(listener);
	}

	public static void fireTrayLogin() {
		for (TrayListener listener : trayListeners) {
			listener.login();
		}
	}
}
