/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月31日 上午8:56:46
 * 
 ***************************************************************/
package com.xinwei.talk.manager;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.http.service.HttpFileService;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.Constants;
import com.xinwei.talk.dao.TalkDao;
import com.xinwei.talk.listener.FileDownloadListener;
import com.xinwei.talk.listener.FileUploadListener;
import com.xinwei.talk.model.TalkFile;
import com.xinwei.talk.ui.chat.voice.VoiceManager;

public class FileManager {
	protected LinkedBlockingQueue<UDFile> queue = new LinkedBlockingQueue<UDFile>();

	private TalkDao dao;

	private HttpFileService service;

	private boolean stop = false;

	public FileManager() {
		dao = TalkManager.getTalkDao();
		service = TalkManager.getFileService();

		// Start 5 uploads and downloads
		for (int i = 0; i < 5; i++) {
			TaskEngine.getInstance().submit(new DownloadAndUpload());
		}
	}

	public TalkFile getFile(long msgId, String url, int type) {
		return dao.getFile(msgId, url, type);
	}

	public List<TalkFile> getFileList(int type, List<String> urls) {
		return dao.getFileList(type, urls);
	}

	public List<TalkFile> getImageFileList(List<String> urls) {
		return getFileList(Constants.MSG_TYPE_IMG, urls);
	}

	public byte[] getImageFile(long msgId, String url, int type) {
		TalkFile file = getFile(msgId, url, Constants.MSG_TYPE_IMG);
		if (file == null)
			return null;
		return file.getData();
	}

	public byte[] getVoiceFile(long msgId, String url, int type) {
		TalkFile file = getFile(msgId, url, Constants.MSG_TYPE_VOICE);
		if (file == null)
			return null;
		return file.getData();
	}
	// public byte[] downloadImage(long msgId, String url) {
	// return downloadFile(msgId, url, Constants.MSG_TYPE_IMG);
	// }
	//
	// public byte[] downloadAvatar(String url) {
	// return downloadFile(-1, url, Constants.MSG_TYPE_AVATAR);
	// }

	public void addFile(long msgId, String url, int fileType, File path, boolean dowload, boolean finish)
			throws Exception {
		dao.addFile(msgId, url, fileType, null, path.getAbsolutePath(), dowload, finish);
	}

	public void updateFile(long msgId, String url, boolean finish) throws Exception {
		dao.updateFile(msgId, url, finish);
	}

	public Object[] uploadFile(Object to, long msgId, int fileType, File file) throws Exception {
		// upload files
		HttpResponseInfo responseInfo = service.uploadChatFile(to, file);
		String url = service.getFileUrl(responseInfo);
		// save document
		dao.addFile(msgId, url, fileType, null, file.getAbsolutePath(), false, true);

		return new Object[] { url, responseInfo.getAttachment() };
	}

	public Object[] uploadFile(Object to, long msgId, int fileType, String fileName, byte[] bytes) throws Exception {
		if (bytes == null)
			return null;
		if (fileName == null) {
			fileName = UUID.randomUUID().toString();
		}
		// upload files
		byte[] data = bytes;
		if (fileType == Constants.MSG_TYPE_VOICE) {
			data = VoiceManager.wavToAmr(bytes);
		}
		HttpResponseInfo responseInfo = service.uploadChatFile(to, data, fileName);
		String url = service.getFileUrl(responseInfo);
		// save document
		dao.addFile(msgId, url, fileType, bytes, null, false, true);

		return new Object[] { url, responseInfo.getAttachment() };
	}

	private byte[] downloadFile(long msgId, String url, int type) throws Exception {
		if (StringUtil.isEmpty(url)) {
			return null;
		}
		byte[] bytes = service.download(url);

		byte[] data = bytes;
		if (bytes != null && bytes.length > 0) {
			if (type == Constants.MSG_TYPE_VOICE) {
				data = VoiceManager.amrToWav(bytes);
			}
			dao.addFile(msgId, url, type, data, null, true, true);
		} else {
			return null;
		}
		return data;
	}

	public void asynDownloadFile(long msgId, int type, String url, FileDownloadListener callback) {
		queue.add(new FileDownload(msgId, type, url, callback));
	}

	public void asynDownloadFile(long msgId, int type, String url, FileDownloadListener callback, boolean isRemote) {
		queue.add(new FileDownload(msgId, type, url, callback, isRemote));
	}

	public void asynDownloadFile(long msgId, int type, String url) {
		asynDownloadFile(msgId, type, url, null);
	}

	public void asynUploadFile(Object obj, long msgId, int fileType, File file) {
		asynUploadFile(obj, msgId, fileType, file, null);
	}

	public void asynUploadFile(Object obj, long msgId, int fileType, File file, FileUploadListener callback) {
		// upload files
		FileUpload e = new FileUpload(msgId, fileType, file, callback);
		e.setToObj(obj);
		queue.add(e);
	}

	public void asynUploadFile(Object obj, long msgId, int fileType, String fileName, byte[] bytes) {
		// upload files
		asynUploadFile(obj, msgId, fileType, fileName, bytes, null);
	}

	public void asynUploadFile(Object obj, long msgId, int fileType, String fileName, byte[] bytes,
			FileUploadListener callback) {
		// upload files
		FileUpload fu = new FileUpload(msgId, fileType, fileName, bytes, callback);
		fu.setToObj(obj);
		queue.add(fu);
	}

	// public void asynDownloadAvatar(String url, Listener listener) {
	// queue.add(new TalkDownLoad(url, IMAGE_AVATAR, listener));
	// }

	class DownloadAndUpload implements Runnable {
		public void run() {
			while (!stop) {
				try {
					final UDFile file = queue.take();
					int type = file.getType();
					long msgId = file.getMsgId();
					if (file instanceof FileDownload) {
						FileDownload download = (FileDownload) file;
						final String url = download.getUrl();
						byte[] bytes = null;
						if (download.isRemote()) {
							try {// Take from the server
								bytes = downloadFile(msgId, url, type);
							} catch (Exception e) {
								e.printStackTrace();
							}
						} else {
							try {// Take from the database
								TalkFile talkFile = getFile(msgId, url, type);
								if (talkFile != null) {
									bytes = talkFile.getData();
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						FileDownloadListener callback = download.getDownloadListener();
						if (callback != null) {
							callback.call(msgId, type, url, bytes);
						}
						// Send notification message
					} else if (file instanceof FileUpload) {
						FileUpload upload = (FileUpload) file;
						Object[] result = null;
						try {
							if (upload.getFile() != null) {
								result = uploadFile(upload.getToObj(), msgId, type, upload.getFile());
							} else if (upload.getBytes() != null) {
								result = uploadFile(upload.getToObj(), msgId, type, upload.getName(),
										upload.getBytes());
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						FileUploadListener callback = file.getUploadListener();
						if (callback != null) {
							callback.call(msgId, type, (String) result[0], upload.getBytes(), upload.getFile(),
									(Map) result[1]);
						}
						// Send notification message
					}

				} catch (InterruptedException e) {
					e.printStackTrace();
					break;
				}
			}
		}
	}

	class FileDownload extends UDFile {
		private String url;
		private boolean remote = true;

		public FileDownload(long msgId, int type, String url, FileDownloadListener listener) {
			this(msgId, type, url, listener, true);
		}

		public FileDownload(long msgId, int type, String url, FileDownloadListener listener, boolean remote) {
			super(msgId, type, listener);
			this.url = url;
			this.remote = remote;
		}

		public String getUrl() {
			return url;
		}

		public boolean isRemote() {
			return remote;
		}
	}

	class FileUpload extends UDFile {
		private Object toObj;// Receiver
		private File file;
		private String name;
		private byte[] bytes;

		public FileUpload(long msgId, int type, File file, FileUploadListener listener) {
			super(msgId, type, listener);
			this.file = file;
		}

		public FileUpload(long msgId, int type, String name, byte[] bytes, FileUploadListener listener) {
			super(msgId, type, listener);
			this.name = name;
			this.bytes = bytes;
		}

		public File getFile() {
			return file;
		}

		public String getName() {
			return name;
		}

		public byte[] getBytes() {
			return bytes;
		}

		public Object getToObj() {
			return toObj;
		}

		public void setToObj(Object toObj) {
			this.toObj = toObj;
		}

	}

	class UDFile {
		private long msgId;
		private int type;
		private FileUploadListener uploadListener;
		private FileDownloadListener downloadListener;

		public UDFile(long msgId, int type, FileUploadListener uploadListener) {
			this.msgId = msgId;
			this.type = type;
			this.uploadListener = uploadListener;
		}

		public UDFile(long msgId, int type, FileDownloadListener downloadListener) {
			this.msgId = msgId;
			this.type = type;
			this.downloadListener = downloadListener;
		}

		public int getType() {
			return type;
		}

		public FileUploadListener getUploadListener() {
			return uploadListener;
		}

		public FileDownloadListener getDownloadListener() {
			return downloadListener;
		}

		public long getMsgId() {
			return msgId;
		}
	}
}
