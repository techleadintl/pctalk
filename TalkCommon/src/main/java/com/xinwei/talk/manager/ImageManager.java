package com.xinwei.talk.manager;

import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.VCard;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ImageManager {
	public static void setBackground(String jid, byte[] background) {
		if (jid == null || background == null || background.length <= 0)
			return;
		jid = TalkUtil.getBareJid(jid);
		String key = "@#talk.background" + jid;

		LAF.clearImageByPrefix(key);
		LAF.cacheImage(key, background);
	}

	public static void deleteBackground(String jid) {
		String key = "@#talk.background" + jid;
		LAF.clearImageByPrefix(key);
	}

	public static ImageIcon getBackground(String jid) {
		jid = "@#talk.background" + jid;
		return LAF.getImageIconByKey(jid);
	}

	public static void setAvatar(String jid, byte[] avatar) {
		if (jid == null || avatar == null || avatar.length <= 0)
			return;
		jid = TalkUtil.getBareJid(jid);

		String key = "@#talk.avatar" + jid;
		LAF.clearImageByPrefix(key);
		LAF.cacheImage(key, avatar);
	}

	public static void deleteAvatar(String jid) {
		String key = "@#talk.avatar" + jid;
		LAF.clearImageByPrefix(key);
	}

	public static List<byte[]> getMemberAvatarList(String groupJid, boolean isDefault) {
		List<Member> memberList = TalkManager.getLocalManager().getMemberListByJid(groupJid);
		List<byte[]> avatars = new ArrayList<byte[]>();
		List<String> urls = new ArrayList<String>();
		if (CollectionUtil.isNotEmpty(memberList)) {
			VCard vCard = TalkManager.getVCard();
			for (Member member : memberList) {
				//				if (StringUtil.equals(member.getUid(), vCard.getUid())) {
				//					continue;
				//				}
				String avatarUrl = member.getAvatarUrl();
				byte[] talkAvatar = getTalkAvatar(member.getMemJid(), false);
				if (talkAvatar != null && !urls.contains(avatarUrl)) {
					urls.add(avatarUrl);
					avatars.add(talkAvatar);
				}
			}
		}
		if (avatars.isEmpty() && isDefault) {
			avatars.add(LAF.getGroupDefaultAvatarBytes());
		}
		return avatars;
	}

	public static Image getGroupAvatarImage(String jid, int W, int H, boolean isDefault) {
		List<byte[]> avatars = getMemberAvatarList(jid, false);
		if (avatars.isEmpty()) {
			if (isDefault) {
				return LAF.getGroupDefaultAvatarImage();
			} else {
				return null;
			}
		}
		return ImageUtil.getMergeImage(W, H, avatars);
		//		return LAF.getGroupDefaultAvatarImage();
	}

//	public static ImageIcon getTalkAvatar(String jid) {
//		return getTalkAvatar(jid, true);
//	}

	public static Image getAvatarImage(String jid, int w, int h, boolean isDefault) {
		boolean group = TalkUtil.isGroup(jid);
		if (group) {
			return getGroupAvatarImage(jid, w, h, isDefault);
		} else {
			ImageIcon avatar = getTalkAvatar(jid, w, h, isDefault);
			if (avatar != null) {
				return avatar.getImage();
			} else {
				return null;
			}
		}
	}

	public static ImageIcon getTalkAvatar(String jid, int w, int h) {
		return getTalkAvatar(jid, w, h, true);
	}

	public static ImageIcon getTalkAvatar(String jid, int w, int h, boolean isDefault) {
		jid = "@#talk.avatar" + jid;

		ImageIcon defaultAvatarIcon = null;
		if (isDefault) {
			defaultAvatarIcon = LAF.getDefaultAvatarIcon();
		}
		return LAF.getImageIconByKey(jid, w, h, defaultAvatarIcon);

	}

	public static byte[] getTalkAvatar(String jid, boolean isDefault) {
		jid = "@#talk.avatar" + jid;

		byte[] imageBytes = LAF.getImageBytes(jid);
		if (imageBytes == null && isDefault) {
			imageBytes = LAF.getDefaultAvatarImageBytes();
		}

		return imageBytes;
	}

	public static byte[] getGroupAvatar(String jid, boolean isDefault) {
		jid = "@#talk.avatar" + jid;

		byte[] imageBytes = LAF.getImageBytes(jid);
		if (imageBytes == null && isDefault) {
			imageBytes = LAF.getGroupDefaultAvatarBytes();
		}

		return imageBytes;
	}

	public static ImageIcon getTalkAvatar(byte[] avatarBytes) {

		ImageIcon avatarIcon = null;

		if (avatarBytes != null && avatarBytes.length > 0) {
			avatarIcon = new ImageIcon(avatarBytes);
		}
		if (avatarIcon == null || avatarIcon.getIconWidth() < 0) {
			avatarIcon = LAF.getDefaultAvatarIcon();
		}
		return avatarIcon;
	}

}
