/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月30日 上午9:33:45
 * 
 ***************************************************************/
package com.xinwei.talk.manager;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.xinwei.common.ini.SimpleInitRW;

public class ConfigManager {
	private String language;

	public String getLanguage() {
		if (language == null) {
			File file = new File(System.getProperty("user.home"), "AppData/Local/CamTalk/src/main/resources/talk/language.properties");
			if (file.exists()) {
				try {
					language = SimpleInitRW.getValue(file.getAbsolutePath(), "Talk_info", "language");
				} catch (Exception e) {
				
				}
			}
		}
	
		return language;
	}

	public void setLanguage(String language, Object status) {
	
		this.language = language;
		if (language != null) {
			File file = new File(System.getProperty("user.home"), "AppData/Local/CamTalk/src/main/resources/talk/language.properties");
			if (status == null) {
				
				try {
					
					file.getParentFile().mkdirs();
					FileWriter writer = new FileWriter(file);
					
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}
			
			SimpleInitRW.setValue(file.getAbsolutePath(), "Talk_info", "language", language);
		
		
		
	}
}
}
