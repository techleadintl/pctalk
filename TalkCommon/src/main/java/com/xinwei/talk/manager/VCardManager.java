/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 下午5:32:02
 * 
 ***************************************************************/
package com.xinwei.talk.manager;

import java.util.List;
import java.util.Map;

import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;

public interface VCardManager {
	//缓存群组
	public void initGroup();

	//缓存用户
	public void initTalk();

	//缓存头像
	public void initAvatar();

	public VCard getVCard();

	public void sysTalk() throws Exception;

	//新增
	public void addTalk(List<Talk> list);

	//删除
	public void deleteTalk(List<Talk> list);

	public void updateTalk(List<Talk> talkList);

	public void sysGroup();

	public void addGroup(List<Group> groupList) throws Exception;

	public void deleteGroup(List<Group> groupList);

	public void updateGroup(List<Group> groupList) throws Exception;

	public void addMember(String gid, List<Member> memberList);

	public void deleteMember(String gid, List<Member> memberList);

	public void updateMember(String gid, List<Member> memberList);
}
