/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月26日 上午10:39:11
 * 
 ***************************************************************/
package com.xinwei.talk.manager;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.http.service.file.OutFileTransfer;
import com.xinwei.talk.common.Constants;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.ui.chat.room.ChatRoom;
import com.xinwei.talk.ui.chat.room.CommonChatRoom;
import com.xinwei.talk.ui.chat.room.file.ChatRoomFileTab;
import com.xinwei.talk.ui.chat.room.file.FileUI;
import com.xinwei.talk.ui.chat.room.file.ReceiveFileUI;
import com.xinwei.talk.ui.chat.room.file.SendFileUI;
import com.xinwei.talk.ui.chat.room.history.ChatRoomTabbedPane;
import com.xinwei.talk.ui.util.UIComponentRegistry;
import com.xinwei.talk.ui.util.component.ImageSelectionPanel;
import com.xinwei.talk.ui.util.component.tab.Tab;

public class TransferManager {
	protected static TransferManager singleton;

	protected Map<String, ArrayList<File>> waitMap = new HashMap<String, ArrayList<File>>();
	protected BufferedImage bufferedImage;
	protected ImageSelectionPanel selectionPanel;
	protected Robot robot;

	/**
	 * Returns the singleton instance of <CODE>SparkTransferManager</CODE>, creating it if necessary.
	 * <p/>
	 *
	 * @return the singleton instance of <Code>SparkTransferManager</CODE>
	 */
	public static TransferManager getInstance() {
		if (null == singleton) {
			singleton = new TransferManager();
			return singleton;
		}
		return singleton;
	}

	protected TransferManager() {
		try {
			robot = new Robot();
			selectionPanel = new ImageSelectionPanel();
		} catch (AWTException e) {
			Log.error(e);
		}

		//		contactList.addFileDropListener(new FileDropListener() {
		//			public void filesDropped(Collection<File> files, Component component) {
		//				if (component instanceof ContactItem) {
		//					ContactItem item = (ContactItem) component;
		//
		//					ChatRoom chatRoom = null;
		//					for (File file : files) {
		//						chatRoom = sendFile(file, item.getJID());
		//					}
		//
		//					if (chatRoom != null) {
		//						SparkManager.getChatManager().getChatContainer().activateChatRoom(chatRoom);
		//					}
		//				}
		//			}
		//		});
		//
		//		if (defaultDirectory == null) {
		//			defaultDirectory = new File(System.getProperty("user.home"));
		//		}
	}

	public void receiveFile(TalkMessage talkMessage) {
		TalkBody bodyMessage = talkMessage.getTalkBody();
		if (bodyMessage == null)
			return;

		List<TalkSpecific> fileList = talkMessage.getSpecificList(Constants.MSG_TYPE_FILE);
		if (fileList == null || fileList.isEmpty()) {
			return;
		}

		String transcriptId = talkMessage.getJid();
		ChatRoom chatRoom = TalkManager.getChatManager().getChatContainer().getChatRoom(transcriptId);
		if (chatRoom == null) {
			return;
		}
		TalkFileSpecific fileMsg = (TalkFileSpecific) fileList.get(0);
//		String httpUrl = fileMsg.getUrl();
//		String fileName = fileMsg.getFilename();
//		long fileSize = fileMsg.getLength();
//		String hash = fileMsg.getHash();
//
//		//		String from = chatMessage.getFrom();
//		//		String to = chatMessage.getTo();
//		FileTransferRequest request = new FileTransferRequest(httpUrl, fileName, fileSize, hash, transcriptId, message.getTime());

		receiveFile(talkMessage, (CommonChatRoom) chatRoom, fileMsg, false);

	}

	public void receiveFile(TalkMessage talkMessage, CommonChatRoom chatRoom,TalkFileSpecific fileMsg , boolean auto) {
		final ReceiveFileUI receiveFileUI = new ReceiveFileUI();

		receiveFileUI.init(talkMessage, chatRoom, fileMsg);

		addFileComponent(chatRoom, receiveFileUI);
		chatRoom.updateUI();
		if (auto) {
			receiveFileUI.acceptRequest();
		}
	}
	
	public void saveAsFile(TalkMessage talkMessage, CommonChatRoom chatRoom,TalkFileSpecific fileMsg , boolean auto,File file) {
		final ReceiveFileUI receiveFileUI = new ReceiveFileUI();

		receiveFileUI.init(talkMessage, chatRoom, fileMsg);

		addFileComponent(chatRoom, receiveFileUI);
		chatRoom.updateUI();
		if (auto) {
			receiveFileUI.saveFile(file);
		}
	}

	/**
	 * 发送文件
	 */
	public CommonChatRoom sendFile(final File file, String jid) {
		long maxsize = Res.getLong("file.transfer.maximum.size", -1L);
		long warningsize = Res.getLong("file.transfer.warning.size", -1L);

		if (file.length() >= maxsize && maxsize != -1) {
			String maxsizeString = SwingUtil.getAppropriateByteWithSuffix(maxsize);
			String yoursizeString = SwingUtil.getAppropriateByteWithSuffix(file.length());
			String output = Res.getMessage("error.chatroom.fileupload.toobig", maxsizeString, yoursizeString);
			UIManager.put("OptionPane.okButtonText", Res.getMessage("button.ok"));
			JOptionPane.showMessageDialog(null, output, Res.getMessage("tabTitle.error"), JOptionPane.ERROR_MESSAGE);
			return null;
		}

		if (file.length() >= warningsize && warningsize != -1) {
			int result = JOptionPane.showConfirmDialog(null, Res.getMessage("warning.chatroom.fileupload.toobig"), Res.getMessage("title.warn"), JOptionPane.YES_NO_OPTION);

			if (result != 0) {
				return null;
			}
		}

		// Create the outgoing file transfer
		final OutFileTransfer transfer = new OutFileTransfer(file);

		ChatRoom chatRoom = TalkManager.getChatManager().getChatContainer().getChatRoom(jid);

		if (chatRoom == null)
			return null;

		final CommonChatRoom room = (CommonChatRoom) chatRoom;

		final SendFileUI sendingUI = UIComponentRegistry.getSendFileUI();
		sendingUI.init(room, transfer);

		addFileComponent(room, sendingUI);

		//		room.updateUI();

		try {
			sendingUI.sendFile();
		} catch (NullPointerException e) {
			Log.error(e);
		}

		return room;
		//		return null;
	}

	//	public void sendImage(final BufferedImage image, final CommonChatRoom room) {
	//		// Write image to system.
	//		room.setCursor(new Cursor(Cursor.WAIT_CURSOR));
	//
	//		SwingWorker writeImageThread = new SwingWorker() {
	//			public Object construct() {
	//				return true;
	//			}
	//
	//			public void finished() {
	//				try {
	//					room.sendImage(image);
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//				TalkManager.getChatManager().getChatContainer().activateChatRoom(room);
	//				room.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	//			}
	//		};
	//		writeImageThread.start();
	//	}

	public void addFileComponent(CommonChatRoom room, FileUI fileUI) {
		final String tabTitle = Res.getMessage("button.chatroom.chat.history.transfer");
		ChatRoomTabbedPane tabbedPane = room.getTabbedPane();
		final ChatRoomFileTab chatRoomFileTab;
		Tab tab = tabbedPane.getTab(tabTitle);
		if (tab == null) {
			chatRoomFileTab = new ChatRoomFileTab();
			tabbedPane.addTab(new Tab(tabbedPane, chatRoomFileTab) {
				@Override
				public boolean canClose() {
					return chatRoomFileTab.isEmpty();
				}
			}, tabTitle, null);
		} else {
			chatRoomFileTab = (ChatRoomFileTab) tab.getComponent();
		}
		chatRoomFileTab.addFileComponent(fileUI);

	}

	public void removeFileComponent(CommonChatRoom room, FileUI fileUI) {
		String tabTitle = Res.getMessage("button.chatroom.chat.history.transfer");
		ChatRoomTabbedPane tabbedPane = room.getTabbedPane();
		Tab tab = tabbedPane.getTab(tabTitle);
		if (tab == null) {
			return;
		}
		ChatRoomFileTab fileTab = (ChatRoomFileTab) tab.getComponent();

		fileTab.removeFileComponent(fileUI);
		if (fileTab.isEmpty()) {
			tabbedPane.close(tab);
		}
	}
}
