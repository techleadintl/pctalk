package com.xinwei.talk.common;

import java.util.Map;

public class HttpException extends Exception {

	private int errorCode;

	private String url;
	private Map<String, String> params;

	public HttpException() {
		super();
	}

	public HttpException(String message) {
		super(message);
	}

	public HttpException(int code) {
		super();
		errorCode = code;
	}

	/**
	 * Creates a new XMPPException with the Throwable that was the root cause of the exception.
	 *
	 * @param wrappedThrowable
	 *            the root cause of the exception.
	 */
	public HttpException(Throwable wrappedThrowable) {
		super(wrappedThrowable);
	}

	public HttpException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public HttpException(String message, int errorCode, Throwable throwable) {
		super(message, throwable);
		this.errorCode = errorCode;
	}

	public HttpException(String message, int errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public HttpException(String message, int errorCode, String url, Map<String, String> params) {
		super(message);
		this.errorCode = errorCode;
		this.url = url;
		this.params = params;
	}

	public void printStackTrace() {
		printStackTrace(System.err);
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
