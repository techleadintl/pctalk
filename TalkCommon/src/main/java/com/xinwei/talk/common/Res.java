/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月12日 上午11:02:35
 * 
 ***************************************************************/
package com.xinwei.talk.common;

import com.xinwei.common.resource.XRes;

public class Res extends XRes{
	public static boolean getBoolean(String propertyName) {
		return getString(propertyName).replace(" ", "").equals("true");
	}
}
