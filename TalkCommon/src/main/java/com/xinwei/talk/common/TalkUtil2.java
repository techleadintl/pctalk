package com.xinwei.talk.common;

import java.awt.Component;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPopupMenu;

import com.xinwei.talk.action.XAction;
import com.xinwei.talk.manager.TalkManager;

public class TalkUtil2 {
	public static void addPopunActions(JPopupMenu popup, String key, Object... objs) {
		List<Class<? extends XAction>> actionList = TalkManager.getActionClasses(key);
		if (actionList == null || objs == null)
			return;
		List<XAction> actions = new ArrayList<>();
		for (Class<? extends XAction> actionClass : actionList) {
			try {
				XAction action = actionClass.newInstance();
				action.setObjs(objs);

				actions.add(action);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		for (XAction action : actions) {
			if (action.isVisible()) {
				popup.add(action);
			}
		}
	}

	public static <T> T getComponent(String key) {
		Class<? extends Component> componentClass = TalkManager.getComponentClass(key);
		if (componentClass == null)
			return null;
		try {
			return (T) componentClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	public static <T> T getComponent(String key, Class[] clazz, Object[] obj) {
		Class<? extends Component> componentClass = TalkManager.getComponentClass(key);
		if (componentClass == null)
			return null;
		try {
			Constructor<? extends Component> constructor = componentClass.getConstructor(clazz);
			return (T) constructor.newInstance(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
