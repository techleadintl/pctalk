/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月27日 下午4:14:58
 * 
 ***************************************************************/
package com.xinwei.talk.common;

import java.awt.Component;
import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledDocument;

import org.jivesoftware.smack.packet.Message;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.GsonUtil;
import com.xinwei.common.lang.SyncLock;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.listener.FileDownloadListener;
import com.xinwei.talk.listener.FileUploadListener;
import com.xinwei.talk.manager.FileManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.model.message.specific.TalkImageSpecific;
import com.xinwei.talk.model.message.specific.TalkLocationSpecific;
import com.xinwei.talk.model.message.specific.TalkNameCardSpecific;
import com.xinwei.talk.model.message.specific.TalkResourceSpecific;
import com.xinwei.talk.model.message.specific.TalkTextSpecific;
import com.xinwei.talk.model.message.specific.TalkVoiceMailSpecific;
import com.xinwei.talk.model.message.specific.TalkVoiceSpecific;
import com.xinwei.talk.ui.chat.balloon.BalloonMessage;
import com.xinwei.talk.ui.chat.balloon.BalloonTextPane;
import com.xinwei.talk.ui.chat.component.FireComponent;
import com.xinwei.talk.ui.chat.component.ImageComponent;
import com.xinwei.talk.ui.chat.component.LocationComponent;
import com.xinwei.talk.ui.chat.component.NameCardComponent;
import com.xinwei.talk.ui.chat.component.VoiceComponent;
import com.xinwei.talk.ui.chat.component.VoiceMailComponent;
import com.xinwei.talk.ui.chat.component.file.ReceiveFileCancelUI;
import com.xinwei.talk.ui.chat.component.file.ReceiveFileCompleteUI;
import com.xinwei.talk.ui.chat.component.file.SendFileCancelUI;
import com.xinwei.talk.ui.chat.component.file.SendFileCompleteUI;
import com.xinwei.talk.ui.chat.room.ChatRoom;
import com.xinwei.talk.ui.chat.voice.VoiceManager;
import com.xinwei.talk.ui.plugin.emoticon.EmoticonManager;
import com.xinwei.talk.ui.util.component.room.ChatRoomComponent;

public class MessageUtil {
	private static GsonBuilder gsonBuilder = new GsonBuilder();

	static {
		final Map<String, Class<? extends TalkSpecific>> map = new HashMap<String, Class<? extends TalkSpecific>>();
		map.put("txt", TalkTextSpecific.class);
		map.put("offlinefile", TalkFileSpecific.class);
		map.put("img", TalkImageSpecific.class);
		map.put("audio", TalkVoiceSpecific.class);
		map.put("voicemail", TalkVoiceMailSpecific.class);
		map.put("loc", TalkLocationSpecific.class);
		map.put("namecard", TalkNameCardSpecific.class);
		gsonBuilder.registerTypeAdapter(new TypeToken<List<TalkSpecific>>() {
		}.getType(), new JsonDeserializer<List<TalkSpecific>>() {
			@Override
			public List<TalkSpecific> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
					throws JsonParseException {
				List<TalkSpecific> list = new ArrayList<TalkSpecific>();
				JsonArray ja = json.getAsJsonArray();
				for (JsonElement je : ja) {
					String type = je.getAsJsonObject().get("type").getAsString();
					list.add((TalkSpecific) context.deserialize(je, map.get(type)));
				}
				return list;
			}
		});
	}

	public static GsonBuilder getGsonBuilder() {
		return gsonBuilder;
	}

	public static List<TalkSpecific> getResourceMsgList(List<TalkSpecific> msgList) {
		List<TalkSpecific> r = new ArrayList<TalkSpecific>();
		for (TalkSpecific msg : msgList) {
			boolean flag = false;
			if (msg instanceof TalkImageSpecific) {
				flag = true;
			} else if (msg instanceof TalkVoiceSpecific) {
				flag = true;
			} else if (msg instanceof TalkVoiceMailSpecific) {
				flag = true;
			}
			if (flag) {
				r.add(msg);
			}
		}
		return r;
	}

	// //1:上传资源到服务器；2：从服务器下载资源；3：历史消息已读，从本地查询；4：历史消息未读，从服务器下载资源
	// public static boolean handResourceMsgList(final BalloonTextPane textPane,
	// final List<BalloonMessage> balloonMessageList, int updownType) {
	// for (final BalloonMessage balloonMessage : balloonMessageList) {
	// TalkMessage talkMessage = balloonMessage.getTalkMessage();
	// List<TalkSpecific> msgList = talkMessage.getMsgList();
	//
	// if (msgList == null)
	// return true;
	// final List<TalkSpecific> resourceMsgList = getResourceMsgList(msgList);
	// if (CollectionUtil.isEmpty(resourceMsgList))
	// return true;
	// final FileManager fileManager = TalkManager.getFileManager();
	//
	// final AtomicInteger trueNumber = new AtomicInteger();
	// final AtomicInteger falseNumber = new AtomicInteger();
	// final List<byte[]> wavList = new ArrayList<byte[]>();
	// for (TalkSpecific msg : resourceMsgList) {
	// final TalkResourceSpecific resourceMsg = (TalkResourceSpecific) msg;
	// int type = com.xinwei.talk.common.Constants.MSG_TYPE_IMG;
	// if (resourceMsg instanceof TalkVoiceSpecific) {
	// type = com.xinwei.talk.common.Constants.MSG_TYPE_VOICE;
	// }
	//
	// if (updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_UPLOAD)
	// {//发送消息
	// fileManager.asynUploadFile(TalkUtil.getObject(talkMessage.getJid()),
	// balloonMessage.getId(), type, resourceMsg.getFilename(),
	// resourceMsg.getData(), new FileUploadListener() {
	// @Override
	// public void call(long msgId, int type, String url, byte[] data, File file,
	// Map map) {
	// if (url == null) {
	// falseNumber.incrementAndGet();
	// } else {
	// if (type == com.xinwei.talk.common.Constants.MSG_TYPE_VOICE) {
	// wavList.add(data);
	// }
	// resourceMsg.setUrl(url);
	// resourceMsg.setAttachment(map);
	// textPane.updateMessage(balloonMessage, resourceMsg, data);
	// }
	// trueNumber.incrementAndGet();
	// }
	// });
	// } else if (updownType ==
	// com.xinwei.talk.common.Constants.UPDOWN_TYPE_DOWNLOAD) {//接收消息
	// fileManager.asynDownloadFile(balloonMessage.getId(), type,
	// resourceMsg.getUrl(), new FileDownloadListener() {
	// @Override
	// public void call(long msgId, int type, String url, byte[] data) {
	// if (data == null) {
	// falseNumber.incrementAndGet();
	// } else {
	// if (type == com.xinwei.talk.common.Constants.MSG_TYPE_VOICE) {
	// wavList.add(data);
	// }
	// resourceMsg.setData(data);
	// textPane.updateMessage(balloonMessage, resourceMsg, data);
	// }
	// trueNumber.incrementAndGet();
	// }
	// });
	// } else if (updownType ==
	// com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DB || updownType ==
	// com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DOWNLOAD) {//本地消息
	// fileManager.asynDownloadFile(balloonMessage.getId(), type,
	// resourceMsg.getUrl(), new FileDownloadListener() {
	// @Override
	// public void call(long msgId, int type, String url, byte[] data) {
	// if (data == null) {
	// falseNumber.incrementAndGet();
	// } else {
	// if (type == com.xinwei.talk.common.Constants.MSG_TYPE_VOICE) {
	// wavList.add(data);
	// }
	// resourceMsg.setData(data);
	// textPane.updateMessage(balloonMessage, resourceMsg, data);
	// }
	// trueNumber.incrementAndGet();
	// }
	// }, updownType ==
	// com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DOWNLOAD);
	// }
	//
	// }
	// balloonMessage.setStatus(BalloonMessage.STATUS_INIT);
	// SystemUtil.execute(new Runnable() {
	// @Override
	// public void run() {
	// SystemUtil.waitForSize(trueNumber, resourceMsgList.size(), 300 * 1000);
	// //计算时长
	// if (!wavList.isEmpty()) {
	// TaskEngine.getInstance().submit(new Runnable() {
	// @Override
	// public void run() {
	// long duration = VoiceManager.getDuration(wavList);
	// if (duration != 0) {
	// balloonMessage.setSpecMsg(String.valueOf(duration) + "\"");
	// }
	// }
	// });
	// }
	// //上传失败
	// if (falseNumber.get() > 0) {//
	// balloonMessage.setStatus(BalloonMessage.STATUS_NO);
	// return false;
	// }
	// balloonMessage.setStatus(BalloonMessage.STATUS_YES);
	// }
	// });
	//
	// }
	// return true;
	// }

	// 1:上传资源到服务器；2：从服务器下载资源；3：历史消息已读，从本地查询；4：历史消息未读，从服务器下载资源

//1: upload resources to the server; 2: download resources from the server; 3: history messages have been read, from local queries; 4: history messages are not read, download resources from the server

	public static boolean handResourceMsg(final BalloonTextPane textPane, final BalloonMessage balloonMessage,
			int updownType) {
		TalkMessage talkMessage = balloonMessage.getTalkMessage();
		List<TalkSpecific> msgList = talkMessage.getSpecificList();

		if (msgList == null)
			return true;
		List<TalkSpecific> resourceMsgList = getResourceMsgList(msgList);
		if (CollectionUtil.isEmpty(resourceMsgList)) {
			return true;
		}
		final FileManager fileManager = TalkManager.getFileManager();

		final AtomicInteger trueNumber = new AtomicInteger();
		final AtomicInteger falseNumber = new AtomicInteger();
		final List<byte[]> wavList = new ArrayList<byte[]>();
		for (TalkSpecific msg : resourceMsgList) {
			final TalkResourceSpecific resourceMsg = (TalkResourceSpecific) msg;
			int type = com.xinwei.talk.common.Constants.MSG_TYPE_IMG;
			if (resourceMsg instanceof TalkVoiceSpecific) {
				type = com.xinwei.talk.common.Constants.MSG_TYPE_VOICE;
			}

			if (updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_UPLOAD) {// 发送消息
				fileManager.asynUploadFile(TalkUtil.getObject(talkMessage.getJid()), balloonMessage.getId(), type,
						resourceMsg.getFilename(), resourceMsg.getData(), new FileUploadListener() {
							@Override
							public void call(long msgId, int type, String url, byte[] data, File file, Map map) {
								if (url == null) {
									falseNumber.incrementAndGet();
								} else {
									if (type == com.xinwei.talk.common.Constants.MSG_TYPE_VOICE) {
										wavList.add(data);
										resourceMsg.setLength(VoiceManager.getDuration(data));
									}
									resourceMsg.setUrl(url);
									resourceMsg.setAttachment(map);
									textPane.updateMessage(balloonMessage, resourceMsg, data);
								}
								trueNumber.incrementAndGet();
							}
						});
			} else if (updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_DOWNLOAD) {// 接收消息
				fileManager.asynDownloadFile(balloonMessage.getId(), type, resourceMsg.getUrl(),
						new FileDownloadListener() {
							@Override
							public void call(long msgId, int type, String url, byte[] data) {
								if (data == null) {
									falseNumber.incrementAndGet();
								} else {
									if (type == com.xinwei.talk.common.Constants.MSG_TYPE_VOICE) {
										wavList.add(data);
									}
									resourceMsg.setData(data);
									textPane.updateMessage(balloonMessage, resourceMsg, data);
								}
								trueNumber.incrementAndGet();
							}
						});
			} else if (updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DB
					|| updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DOWNLOAD) {// 本地消息
				fileManager.asynDownloadFile(balloonMessage.getId(), type, resourceMsg.getUrl(),
						new FileDownloadListener() {
							@Override
							public void call(long msgId, int type, String url, byte[] data) {
								if (data == null) {
									falseNumber.incrementAndGet();
								} else {
									if (type == com.xinwei.talk.common.Constants.MSG_TYPE_VOICE) {
										wavList.add(data);
									}
									resourceMsg.setData(data);
									textPane.updateMessage(balloonMessage, resourceMsg, data);
								}
								trueNumber.incrementAndGet();
							}
						}, updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DOWNLOAD);
			}

		}
		// 5分钟
		SystemUtil.waitForSize(trueNumber, resourceMsgList.size(), 300 * 1000);
		// 计算时长
		if (!wavList.isEmpty()) {
			TaskEngine.getInstance().submit(new Runnable() {
				@Override
				public void run() {
					long duration = VoiceManager.getDuration(wavList);
					if (duration != 0) {
						balloonMessage.setSpecMsg(String.valueOf(duration) + "\"");
					}
				}
			});
		}
		// 上传失败
		if (falseNumber.get() > 0) {//
			return false;
		}
		return true;
	}

	// --------------------------------------------------------------------->>>>>>>>>
	// July 26

	/**
	 * when message received or sent, it decides to download or upload the resources
	 **/
	public static boolean handResourceMsg(TalkMessage talkMessage, int updownType) {

		List<TalkSpecific> msgList = talkMessage.getSpecificList();

		if (msgList == null)
			return true;
		List<TalkSpecific> resourceMsgList = getResourceMsgList(msgList);
		if (CollectionUtil.isEmpty(resourceMsgList)) {
			return true;
		}
		final FileManager fileManager = TalkManager.getFileManager();

		final AtomicInteger trueNumber = new AtomicInteger();
		final AtomicInteger falseNumber = new AtomicInteger();
		final List<byte[]> wavList = new ArrayList<byte[]>();
		for (TalkSpecific msg : resourceMsgList) {
			final TalkResourceSpecific resourceMsg = (TalkResourceSpecific) msg;
			int type = com.xinwei.talk.common.Constants.MSG_TYPE_IMG;
			if (resourceMsg instanceof TalkVoiceSpecific) {
				type = com.xinwei.talk.common.Constants.MSG_TYPE_VOICE;
			}

			if (updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_UPLOAD) {// Send a message
				fileManager.asynUploadFile(TalkUtil.getObject(talkMessage.getJid()), talkMessage.getId(), type,
						resourceMsg.getFilename(), resourceMsg.getData(), new FileUploadListener() {
							@Override
							public void call(long msgId, int type, String url, byte[] data, File file, Map map) {
								if (url == null) {
									falseNumber.incrementAndGet();
								} else {
									if (type == com.xinwei.talk.common.Constants.MSG_TYPE_VOICE) {
										wavList.add(data);
										resourceMsg.setLength(VoiceManager.getDuration(data));
									}
									resourceMsg.setUrl(url);
									System.out.println("FX-Engine : upload completed - url : " + url);
									resourceMsg.setAttachment(map);
								}
								trueNumber.incrementAndGet();
							}
						});
			} else if (updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_DOWNLOAD) {// Receive message
				fileManager.asynDownloadFile(1, type, resourceMsg.getUrl(), new FileDownloadListener() {
					@Override
					public void call(long msgId, int type, String url, byte[] data) {
						if (data == null) {
							falseNumber.incrementAndGet();
						} else {
							if (type == com.xinwei.talk.common.Constants.MSG_TYPE_VOICE) {
								wavList.add(data);
							}
							resourceMsg.setData(data);
						}
						trueNumber.incrementAndGet();
					}
				});
			} else if (updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DB
					|| updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DOWNLOAD) {// Local message
				fileManager.asynDownloadFile(1, type, resourceMsg.getUrl(), new FileDownloadListener() {
					@Override
					public void call(long msgId, int type, String url, byte[] data) {
						if (data == null) {
							falseNumber.incrementAndGet();
						} else {
							if (type == com.xinwei.talk.common.Constants.MSG_TYPE_VOICE) {
								wavList.add(data);
							}
							resourceMsg.setData(data);

						}
						trueNumber.incrementAndGet();
					}
				}, updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DOWNLOAD);
			}

		}
		// 5 minutes
		SystemUtil.waitForSize(trueNumber, resourceMsgList.size(), 300 * 1000);
		// 计算时长
		if (!wavList.isEmpty()) {
			TaskEngine.getInstance().submit(new Runnable() {
				@Override
				public void run() {
					long duration = VoiceManager.getDuration(wavList);
					if (duration != 0) {
						// balloonMessage.setSpecMsg(String.valueOf(duration) + "\"");
					}
				}
			});
		}
		// 上传失败
		if (falseNumber.get() > 0) {//
			return false;
		}
		return true;
	}

	// --------------------------------------------------------------------->>>>>>>>>
	// July 26

	public static boolean sendBalloonMessage(final ChatRoom chatRoom, final BalloonTextPane textPane,
			final BalloonMessage balloonMessage) {
		boolean sendMsgFailed = false;
		TalkMessage talkMessage = balloonMessage.getTalkMessage();
		String packetId = talkMessage.getTalkBody().getPacketId();
		final SyncLock lock = new SyncLock(packetId);
		try {
			// Notify users that message has been sent,如：保存到历史记录中
			chatRoom.fireMessageSent(talkMessage);

			final Message message = new Message();
			message.setPacketID(packetId);
			// 封装消息的类型、发送方、接收方
			message.setType(chatRoom.getChatType());

			message.setBody(MessageUtil.getXmppBodyString(talkMessage.getTalkBody(), chatRoom.getChatType()));

			message.setTo(chatRoom.getRoomId());
			message.setFrom(TalkManager.getSessionManager().getJID());

			// 发送消息
			TalkManager.getConnection().sendPacket(message);

		} catch (Exception e) {
			sendMsgFailed = true;
			Log.error(e);
		}
		// 设置发送消息失败
		if (sendMsgFailed) {
			balloonMessage.setStatus(BalloonMessage.STATUS_NO);
			return false;
		} else {
			TaskEngine.getInstance().submit(new Runnable() {
				@Override
				public void run() {
					// 等待服务器响应
					// 300秒钟没有消息回复，认为发送消息失败
					lock.lock(300, TimeUnit.SECONDS);
					Date date = (Date) lock.getContent();
					if (date != null) {
						// textPane.removeMsg(talkMessage.getId());//清除缓存,
						textPane.updateMessageDate(balloonMessage, date);
						balloonMessage.setStatus(BalloonMessage.STATUS_YES);
					} else {
						balloonMessage.setStatus(BalloonMessage.STATUS_NO);
					}
					textPane.repaint();
				}
			});
			return true;
		}
	}

	// Method is overloaded July 24 here ------------------------ >>>

	/**
	 * send a new message to given user
	 * 
	 * @param talkMessage user entered new message object
	 * @param From        sender's reference
	 * @param To          receiver's reference
	 **/
	public static boolean sendBalloonMessage(TalkMessage talkMessage, String From, String To) {
		boolean sendMsgFailed = false;
		String packetId = talkMessage.getTalkBody().getPacketId();
		// final SyncLock lock = new SyncLock(packetId);

		try {
			final Message message = new Message();

			message.setPacketID(packetId);

			if (talkMessage.getIsGroup()) {
				message.setType(Message.Type.groupchat);
				message.setBody(MessageUtil.getXmppBodyString(talkMessage.getTalkBody(), Message.Type.groupchat));
			} else {
				message.setType(Message.Type.chat);
				message.setBody(MessageUtil.getXmppBodyString(talkMessage.getTalkBody(), Message.Type.chat));
			}

			message.setTo(To);
			message.setFrom(From);

			TalkManager.getConnection().sendPacket(message);

		} catch (Exception e) {
			sendMsgFailed = true;
			Log.error(e);
		}

		return sendMsgFailed;
	}

	// To here ---------------------------- >>>

	// 设置聊天窗口内容
	public static void addTextPaneMsg(BalloonTextPane textPane, int msl, SimpleAttributeSet msgStyles,
			BalloonMessage balloonMsg) throws Exception {
		StyledDocument doc = textPane.getStyledDocument();
		final TalkMessage talkMessage = balloonMsg.getTalkMessage();
		if (talkMessage.isFire()) {
			FireComponent component = new FireComponent(textPane, balloonMsg);
			textPane.addComponent(component);
			return;
		}

		List<TalkSpecific> msgList = talkMessage.getSpecificList();

		if (msgList != null) {
			msgList = new ArrayList<>(msgList);
		}

		// 名片
		List<TalkSpecific> namecardMsgList = getMsgList(msgList, Constants.MSG_TYPE_NAMECARD);

		if (CollectionUtil.isNotEmpty(namecardMsgList)) {
			NameCardComponent component = new NameCardComponent(namecardMsgList);
			textPane.addComponent(component);
			msgList.removeAll(namecardMsgList);
			if (!msgList.isEmpty()) {
				textPane.addLine();
			}
		}

		for (TalkSpecific body : msgList) {
			if (body instanceof TalkTextSpecific) {
				String msg = ((TalkTextSpecific) body).getMsg();
				if (msg == null) {
					continue;
				}
				String text = msg.trim();

				List<Object> textAndEmoticon = EmoticonManager.getInstance().getTextAndEmoticon(text);
				if (textAndEmoticon != null) {
					for (Object obj : textAndEmoticon) {
						if (obj instanceof String) {
							doc.insertString(doc.getLength(), (String) obj, msgStyles);
						} else if (obj instanceof ImageIcon) {// 表情
							ImageComponent label = new ImageComponent((ImageIcon) obj, null);
							label.setBorder(BorderFactory.createEmptyBorder());
							label.setOpaque(true);
							textPane.addComponent(label);
						}
					}
				}
			} else if (body instanceof TalkImageSpecific) {// 图片
				final TalkImageSpecific imageMsg = (TalkImageSpecific) body;
				ImageComponent component = new ImageComponent(null, imageMsg.getFilename());
				component.setBorder(BorderFactory.createEmptyBorder());
				component.setOpaque(true);
				textPane.addComponent(component);
				balloonMsg.putComponent(imageMsg, component);

				byte[] bytes = imageMsg.getData();
				if (bytes != null && bytes.length > 0) {
				}
				textPane.updateMessageImage(balloonMsg, imageMsg, bytes);

			} else if (body instanceof TalkVoiceSpecific) {// 录音
				TalkVoiceSpecific msg = (TalkVoiceSpecific) body;
				VoiceComponent component = new VoiceComponent(msg,
						balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_RIGHT, LAF.getLoading());// BalloonMessage.ALIGNMENT_LEFT
				component.setBorder(BorderFactory.createEmptyBorder());
				textPane.addComponent(component);

				balloonMsg.putComponent(msg, component);

				byte[] data = msg.getData();
				if (data != null && data.length > 0) {
					textPane.updateMessageVoice(balloonMsg, msg, data);
				}
			} else if (body instanceof TalkLocationSpecific) {// 位置
				TalkLocationSpecific msg = (TalkLocationSpecific) body;
				String nick = ((BalloonMessage) balloonMsg).getNick();
				LocationComponent component = new LocationComponent(msg, nick);
				textPane.addComponent(component);

			} else if (body instanceof TalkVoiceMailSpecific) {
				TalkVoiceMailSpecific msg = (TalkVoiceMailSpecific) body;
				VoiceMailComponent component = new VoiceMailComponent(msg,
						balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_RIGHT, LAF.getLoading());// BalloonMessage.ALIGNMENT_LEFT
				component.setBorder(BorderFactory.createEmptyBorder());
				textPane.addComponent(component);

				balloonMsg.putComponent(msg, component);

				byte[] data = msg.getData();
				if (data != null && data.length > 0) {
					textPane.updateMessageVoice(balloonMsg, msg, data);
				}
			} else if (body instanceof TalkFileSpecific) {
				TalkFileSpecific fileMsg = (TalkFileSpecific) body;
				int fileType = fileMsg.getFileType();
				ChatRoomComponent component = null;
				if (fileType == TalkFileSpecific.SEND_COMPLETE) {// 发送文件文件消息,完成
					File file = new File(fileMsg.getFileAbsolutePath());
					if (file.exists()) {
						component = new SendFileCompleteUI(balloonMsg, fileMsg, file);
					}
				} else if (fileType == TalkFileSpecific.SEND_CANCEL) {// 发送文件文件消息,取消
					File file = new File(fileMsg.getFileAbsolutePath());
					if (file.exists()) {
						component = new SendFileCancelUI(file);
					}
				} else if (fileType == TalkFileSpecific.RECEIVE_COMPLETE) {// 接收文件文件消息,完成
					File file = new File(fileMsg.getFileAbsolutePath());
					if (file.exists()) {
						component = new ReceiveFileCompleteUI(balloonMsg, fileMsg, file);
					}

				} else if (fileType == TalkFileSpecific.RECEIVE_CANCEL) {// 接收文件文件消息,取消
					component = new ReceiveFileCancelUI(balloonMsg, fileMsg); // changed @ May 17
					component.setAlignment(SwingConstants.CENTER);
				}
				if (component != null) {
					if (balloonMsg.getAlignment() == BalloonMessage.ALIGNMENT_RIGHT) {// BalloonMessage.ALIGNMENT_LEFT
						component.setAlignment(SwingConstants.LEFT);
					} else {
						component.setAlignment(SwingConstants.RIGHT);
					}
					balloonMsg.putComponent(fileMsg, component);
					textPane.addComponent(component);
				}

				// if (obj instanceof ChatRoomComponent) {
				// if (balloonType == Param.BALLOON_TYPE_HINT) {
				// ((ChatRoomComponent) obj).setAlignment(SwingConstants.CENTER);
				// } else if (balloonType == Param.BALLOON_TYPE_NORMAL) {
				// if ((Boolean) userInfos[0]) {
				// ((ChatRoomComponent) obj).setAlignment(SwingConstants.LEFT);
				// } else {
				// ((ChatRoomComponent) obj).setAlignment(SwingConstants.RIGHT);
				// }
				// } else {
				// ((ChatRoomComponent) obj).setAlignment(SwingConstants.CENTER);
				// }
				// }

			} else {
				// balloonTextPane.insertText(body.toString());
			}
		}

	}

	public static List<TalkSpecific> getMsgList(List<TalkSpecific> msgList, Integer... types) {
		List<TalkSpecific> r = new ArrayList<TalkSpecific>();
		List<Integer> typeList = Arrays.asList(types);
		for (TalkSpecific msg : msgList) {
			boolean flag = false;
			if (msg instanceof TalkTextSpecific && typeList.contains(Constants.MSG_TYPE_TXT)) {
				flag = true;
			} else if (msg instanceof TalkImageSpecific && typeList.contains(Constants.MSG_TYPE_IMG)) {
				flag = true;
			} else if (msg instanceof TalkLocationSpecific && typeList.contains(Constants.MSG_TYPE_LOCTION)) {
				flag = true;
			} else if (msg instanceof TalkFileSpecific && typeList.contains(Constants.MSG_TYPE_FILE)) {
				flag = true;
			} else if (msg instanceof TalkVoiceSpecific && typeList.contains(Constants.MSG_TYPE_VOICE)) {
				flag = true;
			} else if (msg instanceof TalkVoiceMailSpecific && typeList.contains(Constants.MSG_TYPE_VOICE_MAIL)) {
				flag = true;
			} else if (msg instanceof TalkNameCardSpecific && typeList.contains(Constants.MSG_TYPE_NAMECARD)) {
				flag = true;
			}
			if (flag) {
				r.add(msg);
			}
		}
		return r;
	}

	public static List<Component> getComponentList(BalloonMessage message, Integer... types) {
		TalkMessage talkMessage = message.getTalkMessage();
		if (talkMessage == null) {
			return null;
		}
		List<TalkSpecific> msgList = talkMessage.getSpecificList();
		if (msgList == null) {
			return null;
		}
		List<Component> r = new ArrayList<Component>();
		List<Integer> typeList = Arrays.asList(types);
		for (TalkSpecific msg : msgList) {
			boolean flag = false;
			if (msg instanceof TalkTextSpecific && typeList.contains(Constants.MSG_TYPE_TXT)) {
				flag = true;
			} else if (msg instanceof TalkImageSpecific && typeList.contains(Constants.MSG_TYPE_IMG)) {
				flag = true;
			} else if (msg instanceof TalkLocationSpecific && typeList.contains(Constants.MSG_TYPE_LOCTION)) {
				flag = true;
			} else if (msg instanceof TalkFileSpecific && typeList.contains(Constants.MSG_TYPE_FILE)) {
				flag = true;
			} else if (msg instanceof TalkVoiceSpecific && typeList.contains(Constants.MSG_TYPE_VOICE)) {
				flag = true;
			} else if (msg instanceof TalkVoiceMailSpecific && typeList.contains(Constants.MSG_TYPE_VOICE_MAIL)) {
				flag = true;
			} else if (msg instanceof TalkNameCardSpecific && typeList.contains(Constants.MSG_TYPE_NAMECARD)) {
				flag = true;
			}
			if (flag) {
				ChatRoomComponent component = (ChatRoomComponent) message.getComponent(msg);
				if (component != null) {
					r.add(component);
				}
			}
		}
		return r;
	}

	public static int getMessageType(List<TalkSpecific> msgList) {
		int msgType = 0;

		for (TalkSpecific body : msgList) {
			if (body instanceof TalkTextSpecific) {
				msgType = msgType | (int) Math.pow(2, (Constants.MSG_TYPE_TXT - 1));
			} else if (body instanceof TalkImageSpecific) {
				msgType = msgType | (int) Math.pow(2, (Constants.MSG_TYPE_IMG - 1));
			} else if (body instanceof TalkFileSpecific) {
				msgType = msgType | (int) Math.pow(2, (Constants.MSG_TYPE_FILE - 1));
			} else if (body instanceof TalkVoiceSpecific) {
				msgType = msgType | (int) Math.pow(2, (Constants.MSG_TYPE_VOICE - 1));
			} else if (body instanceof TalkLocationSpecific) {
				msgType = msgType | (int) Math.pow(2, (Constants.MSG_TYPE_LOCTION - 1));
			} else if (body instanceof TalkVoiceMailSpecific) {
				msgType = msgType | (int) Math.pow(2, (Constants.MSG_TYPE_VOICE_MAIL - 1));
			} else if (body instanceof TalkNameCardSpecific) {
				msgType = msgType | (int) Math.pow(2, (Constants.MSG_TYPE_NAMECARD - 1));
			} else {
				msgType = 0;
			}
		}
		return msgType;
	}

	public static String getMessagePrompt(TalkMessage talkMessage) {
		if (MessageUtil.isChatMessage(talkMessage.getMsgType(), Constants.MSG_TYPE_TXT)) {
			List<TalkSpecific> msgList = talkMessage.getSpecificList(Constants.MSG_TYPE_TXT);
			if (CollectionUtil.isEmpty(msgList))
				return "";
			TalkSpecific msg = msgList.get(0);

			List<Object> textAndEmoticon = EmoticonManager.getInstance()
					.getTextAndEmoticon(((TalkTextSpecific) msg).getMsg());
			ImageIcon emoticon = null;
			if (textAndEmoticon != null) {
				for (Object obj : textAndEmoticon) {
					if (obj instanceof ImageIcon) {// 表情
						emoticon = (ImageIcon) obj;
						break;
					}
				}
			}
			if (emoticon != null) {
				return Res.getMessage("tray.message.emoticon");
			} else {
				return ((TalkTextSpecific) msg).getMsg();
			}
		} else {
			return getMessagePrompt(talkMessage.getMsgType());
		}
	}

	public static String getMessagePrompt(int msgType) {
		if (isChatMessage(msgType, Constants.MSG_TYPE_IMG)) {
			return Res.getMessage("tray.message.image");
		} else if (isChatMessage(msgType, Constants.MSG_TYPE_FILE)) {
			return Res.getMessage("tray.message.file");
		} else if (isChatMessage(msgType, Constants.MSG_TYPE_LOCTION)) {
			return Res.getMessage("tray.message.location");
		} else if (isChatMessage(msgType, Constants.MSG_TYPE_VOICE_MAIL)) {
			return Res.getMessage("tray.message.voicemail");
		} else if (isChatMessage(msgType, Constants.MSG_TYPE_NAMECARD)) {
			return Res.getMessage("tray.message.namecard");
		} else if (isChatMessage(msgType, Constants.MSG_TYPE_EMOTICON)) {
			return Res.getMessage("tray.message.emoticon");
		} else {
			return "";
		}
	}

	// talkMessage.getMsgType(), Constants.MSG_TYPE_FILE
	public static boolean isChatMessage(int msgType, int type) {
		if (type < 1) {
			return false;
		}
		return ((int) Math.pow(2, (type - 1)) & msgType) >> (type - 1) == 1;
	}

	// public static TalkSpecific getMsg(Object obj) {
	// return TalkManager.getMessageConverter().getTalkSpecific(obj);
	// }
	//
	// public static XmppBody getMsg(TalkSpecific msg) {
	// return null;
	// }

	/**
	 * convert server response to class objects according to message type
	 **/
	public static TalkBody[] conver2TalkBody(Object obj) {
		if (obj == null)
			return null;
		return TalkManager.getMessageConverter().conver2TalkBody(obj);
	}

	public static Object conver2XmppBody(TalkBody talkBody, Message.Type type) {
		if (talkBody == null)
			return null;
		return TalkManager.getMessageConverter().conver2XmppBody(talkBody, type);
	}

	public static String getXmppBodyString(TalkBody talkBody, Message.Type type) {
		if (talkBody == null)
			return null;
		Object msg = conver2XmppBody(talkBody, type);
		return GsonUtil.toJson(msg);
	}

	public static void main(String[] args) {
		String str = "SUM({MEMORYAVERAGEUSAGE}+{ABC})/SUM({NUMBER})";
		Pattern p = Pattern.compile("(\\{(.*?)\\})");
		Matcher matcher = p.matcher(str);
		while (matcher.find()) {
			String group = matcher.group(2);
			System.out.println(group);
		}
	}

}
