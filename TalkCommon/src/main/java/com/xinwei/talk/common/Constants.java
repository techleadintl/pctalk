package com.xinwei.talk.common;

public class Constants {
	public static int BALLOON_SPAN = 10;

	public static String DOMAIN;

	public static String USER_PREFIX = "";

	public static String USER_SUBFIX = "";

	public static String GROUP_SUBFIX = "@conference";

	public static String GROUP_PREFIX = "";

	public static int MOUSE_NONE = 0;
	public static int MOUSE_CICKED = 1;
	public static int MOUSE_PRESSED = 2;
	public static int MOUSE_ENTERED = 3;
	public static int MOUSE_EXITED = 4;
	public static int MOUSE_RELEASED = 5;
	public static int MOUSE_MOVED = 6;

	public static String CUSTOM_TALK_TEL_GROUP_MESSAGE = "1000";
	public static String CUSTOM_TALK_TEL_SYSTEM_MESSAGE = "1001";
	public static String MY_TALK_RES_MOBILE = "mobile";

	public static int MSG_TYPE_TXT = 1;//第一位-文本消息

	public static int MSG_TYPE_IMG = 2;//第2位-图片消息

	public static int MSG_TYPE_FILE = 3;//第3位-文件消息

	public static int MSG_TYPE_VOICE = 4;//第4位-语音消息

	public static int MSG_TYPE_VOICE_MAIL = 5;////第5位-语音信箱消息

	public static int MSG_TYPE_LOCTION = 6;//位置
	
	public static int MSG_TYPE_NAMECARD = 7;//名片
	
	public static int MSG_TYPE_AVATAR = 8;//头像
	
	public static int MSG_TYPE_EMOTICON = 9;//表情
	
	
	//1:上传资源到服务器；2：从服务器下载资源；3：历史消息已读，从本地查询；4：历史消息未读，从服务器下载资源
	public static int UPDOWN_TYPE_UPLOAD = 1;//1:上传资源到服务器

	public static int UPDOWN_TYPE_DOWNLOAD = 2;//2：从服务器下载资源

	public static int UPDOWN_TYPE_HISTORY_DB = 3;//3:历史消息已读，从本地查询；

	public static int UPDOWN_TYPE_HISTORY_DOWNLOAD = 4;//4：历史消息未读，从服务器下载资源

}
