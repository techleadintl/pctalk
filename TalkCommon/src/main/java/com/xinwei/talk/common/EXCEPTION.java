package com.xinwei.talk.common;

public class EXCEPTION extends Exception {

	private int errorCode;

	public EXCEPTION() {
		super();
	}

	public EXCEPTION(String message) {
		super(message);
	}

	public EXCEPTION(int code) {
		super();
		errorCode = code;
	}

	/**
	 * Creates a new XMPPException with the Throwable that was the root cause of the exception.
	 *
	 * @param wrappedThrowable
	 *            the root cause of the exception.
	 */
	public EXCEPTION(Throwable wrappedThrowable) {
		super(wrappedThrowable);
	}

	public EXCEPTION(String message, Throwable throwable) {
		super(message, throwable);
	}

	public EXCEPTION(String message, int errorCode, Throwable throwable) {
		super(message, throwable);
		this.errorCode = errorCode;
	}

	public EXCEPTION(String message, int errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public void printStackTrace() {
		printStackTrace(System.err);
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
