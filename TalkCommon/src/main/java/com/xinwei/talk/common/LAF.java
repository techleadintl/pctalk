/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月12日 上午11:02:35
 * 
 ***************************************************************/
package com.xinwei.talk.common;

import com.xinwei.common.lang.XLookAndFeel;
import com.xinwei.common.lookandfeel.McWillBorders;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.lookandfeel.util.ColorUtil;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.common.lookandfeel.util.PainterUtil;
import com.xinwei.talk.ui.main.content.ButtonPanel;
import com.xinwei.talk.ui.main.search.SearchInputPanel;
import com.xinwei.talk.ui.main.status.StatusPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class LAF extends XLookAndFeel {
	public static int CHAT_ROOM_RIGHT_WIDTH = 400;

	static Font FONT_UNREADMESSAGE = new Font("serif", Font.PLAIN, 11);
	//	static Font FONT_UNREADMESSAGE = new Font("Dialog", Font.PLAIN, 11);

	public static ImageIcon getTestImageIcon() {
		return new ImageIcon(LAF.class.getResource("test.png"));
	}

	public static Font getUnreadMessageFont() {
		return FONT_UNREADMESSAGE;
	}

	private static ImageIcon[] getImageIcons(String prefix, String imageName, int w, int h) {
		return getImageIcons(prefix, imageName, 3, w, h);
	}

	private static ImageIcon[] getImageIcons(String prefix, String imageName) {
		return getImageIcons(prefix, imageName, 3);
	}

	private static ImageIcon[] getImageIcons(String prefix, String imageName, int num) {
		String format = "{0}/{1}{2}.png";
		List<ImageIcon> ii = new ArrayList<ImageIcon>();
		for (int i = 0; i < num; i++) {
			ImageIcon imageIcon = getImageIcon(MessageFormat.format(format, prefix, imageName, (i + 1)));
			if (imageIcon != null) {
				ii.add(imageIcon);
			}
		}
		return ii.toArray(new ImageIcon[0]);
	}

	private static ImageIcon[] getImageIcons(String prefix, String imageName, int num, int w, int h) {
		String format = "{0}/{1}{2}.png";
		List<ImageIcon> ii = new ArrayList<ImageIcon>();
		for (int i = 0; i < num; i++) {
			ImageIcon imageIcon = getImageIcon(MessageFormat.format(format, prefix, imageName, (i + 1)), w, h);
			if (imageIcon != null) {
				ii.add(imageIcon);
			}
		}
		return ii.toArray(new ImageIcon[0]);
	}

	/***************************************** 公共 ****************************************************/
	public static void paintTextFieldBackgroud(JComponent component, Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		PainterUtil.paintRoundBackgroudColor(g, Color.WHITE, 0, 0, component.getWidth(), component.getHeight());
		component.setBorder(McWillBorders.getRoundRectBorder(Color.GRAY));
	}

	public static void paintDialogTitle(JComponent comp, Graphics g) {
		PainterUtil.paintBackgroudColor(g, getDialogTitleColor(), 0, 0, comp.getWidth(), comp.getHeight());
	}

	public static void paintAvatar(ImageIcon icon, JComponent comp, Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		g.drawImage(LAF.getAvatarBorder().getImage(), 0, 0, comp.getWidth(), comp.getHeight(), comp);
		g.drawImage(icon.getImage(), 1, 1, comp.getWidth() - 2, comp.getHeight() - 2, comp);
	}

	public static void paintLoginTextField(JPanel component, Graphics g) {
		//		g.drawImage(LAF.getLoginInputImage().getImage(), 0, 0, 250, 40, null);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
		Rectangle b = component.getBounds();
		PainterUtil.paintRoundBackgroudColor(g, new Color(0x3e88ca), 0, 0, b.width, b.height, 10, 10);
	}

	public static void paintMainFrame(JComponent component, Graphics g) {
		component.setOpaque(true);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		//		PainterUtil.paintBackgroudColor(g, getMainStatusColor(), 0, 0, component.getWidth(), component.getHeight());
	}

	public static void paintMainStatusPanel(StatusPanel component, Graphics2D g) {
		//		PainterUtil.paintBackgroudColor(g, getMainStatusColor(), 0, 0, component.getWidth(), component.getHeight());
	}

	public static void paintMainSearchInputPanel(SearchInputPanel component, Graphics g) {
		component.setOpaque(true);
		if (component.isStatus()) {
			//			g.setColor(getColor(150, 150, 150));
			PainterUtil.paintBackgroudColor(g, Color.WHITE, 0, 0, component.getWidth(), component.getHeight());
		} else {
			PainterUtil.paintBackgroudColor(g, McWillTheme.getThemeColor().getSrcColor(), 0, 0, component.getWidth() - 0, component.getHeight() - 0);
			PainterUtil.paintRoundBackgroudColor(g, ColorUtil.darker(McWillTheme.getThemeColor().getSrcColor(), 20), 0, 0, component.getWidth(), component.getHeight(), 7, 7);
		}
	}

	public static void paintMainWorkspacePane(JComponent component, Graphics g) {
		Insets insets = component.getInsets();
		PainterUtil.paintBackgroudColor(g, getMainContactColor(), 0, 0, component.getWidth(), component.getHeight());
		PainterUtil.paintBackgroudColor(g, Color.WHITE, insets.left, insets.top, component.getWidth() - insets.left - insets.right, component.getHeight() - insets.top - insets.bottom);
	}

	public static void paintMainButtonPanel(ButtonPanel component, Graphics2D g) {
		PainterUtil.paintBackgroudColor(g, getMainContactColor(), 0, 0, component.getWidth(), component.getHeight());
		PainterUtil.paintBackgroudColor(g, Color.WHITE, 0, 0, component.getWidth(), component.getHeight());
	}

	//新建组对话框
	public static void paintMainAddGroupDialog(McWillContentPanel component, Graphics g) {
		component.setOpaque(true);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		PainterUtil.paintBackgroudColor(g, getDialogContentColor(), 0, 0, component.getWidth(), component.getHeight());
	}

	//托盘消息界面
	public static void paintTrayDialog(McWillContentPanel component, Graphics g) {
		component.setOpaque(true);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		PainterUtil.paintBackgroudColor(g, Color.WHITE, 0, 0, component.getWidth(), component.getHeight());
	}

	//聊天窗口消息界面
	public static void paintChatPane(JComponent component, Graphics g) {
		//		component.setOpaque(true);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		PainterUtil.paintBackgroudColor(g, LAF.getColor(0xf5f6f6), 0, 0, component.getWidth(), component.getHeight());
		//		PainterUtil.paintBackgroudColor(g, McWillTheme.getThemeColor().getSubLightColor(), 0, 0, component.getWidth(), component.getHeight());

	}

	//聊天窗口标题头界面
	public static void paintChatTitlePane(JComponent component, Graphics g) {
		component.setOpaque(true);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		PainterUtil.paintBackgroudColor(g, getChatTitlePaneColor(), 0, 0, component.getWidth(), component.getHeight());

	}

	//聊天窗口消息记录中搜索界面
	public static void paintChatMessageSearchPane(JComponent component, Graphics g) {
		component.setOpaque(true);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		PainterUtil.paintBackgroudColor(g, McWillTheme.getThemeColor().getMiddleColor1(), 0, 0, component.getWidth(), component.getHeight());
		//		PainterUtil.paintBackgroudColor(g, McWillTheme.getThemeColor().getSubLightColor(), 0, 0, component.getWidth(), component.getHeight());

	}

	//----------------------------------公共部分------------------------------------------
	/**
	 * Returns the image to use with most dialogs.
	 *
	 * @return the image to use with most dialogs.
	 */
	public static ImageIcon getApplicationImage() {
		return getImageIcon("talk/images/tray/tray-24x24.png");
	}

	public static List<Image> getApplicationImages() {
		List<Image> l = new ArrayList<Image>();
		l.add(getApplicationImage().getImage());
		l.add(getApplicationImage().getImage());
		l.add(getApplicationImage().getImage());
		return l;
	}

	public static Color getDialogTitleColor() {
		return LAF.getColor(0x3b98fe);
	}

	public static Color getDialogContentColor() {
		return LAF.getColor(0xe5edfd);
	}

	public static Color getDialogTitleForegroundColor() {
		return Color.WHITE;
	}

	public static Dimension getDialogIconifyIconSize() {
		return new Dimension(13, 12);
	}

	public static ImageIcon[] getIconifyIcons() {
		return getImageIcons("talk/images/buttons", "dialog-min");
	}

	public static ImageIcon[] getCloseIcons() {
		return getImageIcons("talk/images/buttons", "dialog-close");
	}

	public static Image getCursorImage() {
		return getImage("talk/images/room/cursor.png");
	}

	public static ImageIcon getCopyImageIcon() {
		return getImageIcon("talk/images/action/copy.png");
	}

	public static ImageIcon getSaveAsImageIcon() {
		return getImageIcon("talk/images/action/save_as.png");
	}

	public static ImageIcon getPrintImageIcon() {
		return getImageIcon("talk/images/action/printer.png");
	}

	public static ImageIcon getEraseImageIcon() {
		return getImageIcon("talk/images/action/eraser.png");
	}

	public static ImageIcon getAvatarBorder() {
		return getImageIcon("talk/images/common/avatar-border.png");
	}

	public static ImageIcon getButtonBackgroudImageIcon() {
		return getImageIcon("talk/images/buttons/button_bkg.png");
	}

	public static ImageIcon[] getRemoveIcon() {
		return getImageIcons("talk/images/buttons", "remove1");
	}

	public static ImageIcon[] getRemove2Icon() {
		return getImageIcons("talk/images/buttons", "remove2");
	}

	public static ImageIcon[] getDelete3Icon() {
		return getImageIcons("talk/images/buttons", "delete3");
	}

	public static ImageIcon[] getDelete2Icon() {
		return getImageIcons("talk/images/buttons", "delete2");
	}

	public static ImageIcon[] getDeleteIcon() {
		return getImageIcons("talk/images/buttons", "delete1");
	}

	public static ImageIcon[] getUpdateIcon() {
		return getImageIcons("talk/images/buttons", "update1");
	}

	public static ImageIcon[] getGroupUpdateIcon() {
		return getImageIcons("talk/images/buttons", "group-update");
	}

	public static ImageIcon[] getTalkUpdateIcon() {
		return getImageIcons("talk/images/buttons", "talk-update");
	}

	public static ImageIcon[] getGroupDissolveIcon() {
		return getImageIcons("talk/images/buttons", "group-dissolve");
	}

	public static ImageIcon[] getGroupQuitIcon() {
		return getImageIcons("talk/images/buttons", "group-quit");
	}

	public static ImageIcon[] getGroupKickIcon() {
		return getImageIcons("talk/images/buttons", "group-kick");
	}

	public static ImageIcon[] getGroupInviteIcon() {
		return getImageIcons("talk/images/buttons", "group-invite");
	}

	public static ImageIcon[] getGroupChatIcon() {
		return getImageIcons("talk/images/buttons", "group-chat");
	}

	public static ImageIcon[] getTalkChatIcon() {
		return getImageIcons("talk/images/buttons", "talk-chat");
	}

	public static ImageIcon[] getSelectAllIcon() {
		return getImageIcons("talk/images/buttons", "select-all");
	}

	public static ImageIcon[] getClearIcon() {
		return getImageIcons("talk/images/buttons", "clear");
	}

	public static ImageIcon getSearchIcon() {
		return getImageIcon("talk/images/main/search.png");
	}

	public static ImageIcon getTextSearchIcon() {
		return getImageIcon("talk/images/common/text-search.png");
	}

	public static ImageIcon getNextYearImageIcon() {
		return getImageIcon("talk/images/buttons/datepicker/nextyear.png");
	}

	public static ImageIcon getNextMonthImageIcon() {
		return getImageIcon("talk/images/buttons/datepicker/nextmon.png");
	}

	public static ImageIcon getPreMonthImageIcon() {
		return getImageIcon("talk/images/buttons/datepicker/premon.png");
	}

	public static ImageIcon getPreYearImageIcon() {
		return getImageIcon("talk/images/buttons/datepicker/preyear.png");
	}

	public static Icon getDatePickerImageIcon() {
		return getImageIcon("talk/images/buttons/datepicker/date.png");
	}

	public static Icon getPrevPageImageIcon() {
		return getImageIcon("talk/images/buttons/page/prev.png", 12, 18);
	}

	public static Icon getNextPageImageIcon() {
		return getImageIcon("talk/images/buttons/page/next.png", 12, 18);
	}

	public static Icon getFirstPageImageIcon() {
		return getImageIcon("talk/images/buttons/page/first.png", 13, 18);
	}

	public static Icon getLastPageImageIcon() {
		return getImageIcon("talk/images/buttons/page/last.png", 13, 18);
	}

	public static Icon getQueryImageIcon() {
		return getImageIcon("talk/images/buttons/query.png");
	}

	//----------------------------------登录------------------------------------------
	public static ImageIcon getLoginBackgroundImage() {
		return getImageIcon("talk/images/login/bk.png");
	}

	public static Dimension getLoginBackgroundSize() {
		return new Dimension(472, 332 + 40);
	}

	public static ImageIcon getLoginLogoIcon() {
		return getImageIcon("talk/images/login/logo.png");
	}

	public static ImageIcon getLoginCountryIcon() {
		return getImageIcon("talk/images/country/no_flags.png", 24, 58 * 24 / 70);
	}

	public static ImageIcon getLoginUserIcon() {
		return getImageIcon("talk/images/login/user.png");
	}

	public static ImageIcon getLoginLockIcon() {
		return getImageIcon("talk/images/login/lock.png");
	}

	public static ImageIcon getLoginButtonIcon() {
		return getImageIcon("talk/images/login/button-login.png");
	}

	public static ImageIcon getLoginUpIcon() {
		return getImageIcon("talk/images/login/up.png");
	}

	public static ImageIcon getLoginingIcon() {
		return getImageIcon("talk/images/login/logining.gif");
	}

	public static ImageIcon[] getLoginKeyBoardIcon() {
		return getImageIcons("talk/images/login", "keyboard");
	}

	public static ImageIcon[] getLoginUserPopIcon() {
		return getImageIcons("talk/images/login", "userpop");
	}

	public static ImageIcon[] getLoginCheck() {
		return getImageIcons("talk/images/login", "check");
	}

	//----------------------------------主界面------------------------------------------
	public static ImageIcon getMainSearch() {
		return getImageIcon("talk/images/main/search.png");
	}

	public static ImageIcon[] getMainTabRecent() {
		return getImageIcons("talk/images/main/tab", "recent");
	}

	public static ImageIcon[] getMainTabContact() {
		return getImageIcons("talk/images/main/tab", "contact");
	}

	public static ImageIcon[] getMainTabGroup() {
		return getImageIcons("talk/images/main/tab", "group");
	}

	public static ImageIcon[] getMainTabSkin() {
		return getImageIcons("talk/images/main/tab", "skin");
	}

	public static ImageIcon[] getMainTabSkin(int w, int h) {
		return getImageIcons("talk/images/main/tab", "skin", w, h);
	}

	public static ImageIcon[] getMainTabSetting() {
		return getImageIcons("talk/images/main/tab", "setting");
	}

	public static ImageIcon[] getMainTabAddContact() {
		return getImageIcons("talk/images/main/tab", "addcontact");
	}

	public static ImageIcon[] getMainTablogout() {
		return getImageIcons("talk/images/main/tab", "logout");
	}

	
	public static ImageIcon[] getMainTabAddGroup() {
		return getImageIcons("talk/images/main/tab", "addgroup");
	}

	public static ImageIcon getAddSkinImageIcon() {
		return getImageIcon("talk/images/skin/add-skin.png");
	}

	public static Color getMainContactColor() {
		return getColor(0xf3f5f6);
	}

	public static Color getMainSearchInputColor() {
		return getColor(0x3482b7);
	}

	public static Color getMainStatusColor() {
		return getColor(0x3b98fe);
	}

	//---------------------------------------------------------------------------------------
	public static ImageIcon getTrayIgnoreIcon() {
		return getImageIcon("talk/images/tray/ignore.png");
	}

	//----------------------------------聊天窗口------------------------------------------
	public static Color getChatTitlePaneColor() {
		return LAF.getColor(0x3b98fe);
	}

	//字体 ，粗体
	public static ImageIcon[] getRoomFontBoldImageIcon() {
		return getImageIcons("talk/images/room/font", "bold");
	}

	//字体 ，斜体
	public static ImageIcon[] getRoomFontItalicImageIcon() {
		return getImageIcons("talk/images/room/font", "italic");
	}

	//字体 ，下划线
	public static ImageIcon[] getRoomFontUnderlineImageIcon() {
		return getImageIcons("talk/images/room/font", "underline");
	}

	//字体 ，颜色
	public static ImageIcon[] getRoomFontColorImageIcon() {
		return getImageIcons("talk/images/room/font", "color");
	}

	public static ImageIcon[] getRoomFontBalloonIcon() {
		return getImageIcons("talk/images/room/font", "balloon");
	}

	public static ImageIcon[] getRoomFontBalloonIcon(int w, int h) {
		return getImageIcons("talk/images/room/font", "balloon", w, h);
	}

	//字体
	public static ImageIcon[] getRoomFontImageIcon() {
		return getImageIcons("talk/images/room", "font");
	}

	//截图
	public static ImageIcon[] getRoomScreenshotImageIcon() {
		return getImageIcons("talk/images/room", "screenshot");
	}

	//发送图片
	public static ImageIcon[] getRoomPictureImageIcon() {
		return getImageIcons("talk/images/room", "picture");
	}

	//上传文件
	public static ImageIcon[] getRoomFileImageIcon() {
		return getImageIcons("talk/images/room", "uploadfile");
	}

	//上传文件
	public static ImageIcon[] getRoomEmoticonImageIcon() {
		return getImageIcons("talk/images/room", "emoticon");
	}

	//短音
	public static ImageIcon[] getRoomShortVoiceImageIcon() {
		return getImageIcons("talk/images/room", "shortvoice");
	}

	public static ImageIcon[] getRoomShortVoiceAnimateImageIcon() {
		return getImageIcons("talk/images/room/component", "shortvoice_animate", 14, 18, 22);
	}

	//消息记录
	public static ImageIcon[] getRoomHistoryImageIcon() {
		return getImageIcons("talk/images/room", "history");
	}

	//振动
	public static ImageIcon[] getRoomBuzzImageIcon() {
		return getImageIcons("talk/images/room", "buzz");
	}

	//组成员
	public static ImageIcon getMemberListImageIcon() {
		return getImageIcon("talk/images/room/group-member-list.png", 22, 22);
	}

	//邀请好友
	public static ImageIcon getMemberInviteImageIcon() {
		return getImageIcon("talk/images/room/group-member-invite.png", 22, 22);
	}

	public static ImageIcon getMsgLoading() {
		return getImageIcon("talk/images/room/loading.gif");
	}

	public static ImageIcon getMsgLoadFail() {
		return getImageIcon("talk/images/room/load-fail.png", 14, 14);
	}

	public static ImageIcon getMsgImage() {
		return getImageIcon("talk/images/room/component/image.png", 120, 120);
	}

	public static ImageIcon getMsgLocation() {
		return getImageIcon("talk/images/room/component/location.png");
	}

	public static ImageIcon getMsgVoiceLeft() {
		return getImageIcon("talk/images/room/component/voice_left.png");
	}

	public static ImageIcon[] getMsgVoiceLefts() {
		return getImageIcons("talk/images/room/component", "voice_left");
	}

	public static ImageIcon getMsgVoiceRight() {
		return getImageIcon("talk/images/room/component/voice_right.png");
	}

	public static ImageIcon[] getMsgVoiceRights() {
		return getImageIcons("talk/images/room/component", "voice_right");
	}

	public static ImageIcon getMsgNameCard() {
		return getImageIcon("talk/images/room/component/namecard.png", 40, 40);
	}

	public static Color getChatSplitPaneColor() {
		return getChatRoomColor();
	}

	public static Color getChatLeftColor() {
		return Color.WHITE;
	}

	public static Color getChatRoomColor() {
		return LAF.getColor(0xf5f6f6);
	}

	public static Color getChatInputSelectedTextColor() {
		return Color.WHITE;
	}

	public static Color getChatInputSelectionColor() {
		return getColor(0x316ac5);
	}

	//----------------------------------XXXXX------------------------------------------

	public static Icon getStatusBarAvatar(Icon icon) {
		if (icon == null) {
			icon = getDefaultAvatarIcon();
		}
		Image image = ImageUtil.iconToImage(icon);
		if (icon.getIconHeight() > 58 || icon.getIconWidth() > 58) {
			return new ImageIcon(image.getScaledInstance(58, 58, Image.SCALE_SMOOTH));
		} else {
			return icon;
		}
	}

	public static Icon getStatusBarAvatar(byte[] avatarBytes) {
		ImageIcon avatarIcon = null;
		if (avatarBytes != null && avatarBytes.length > 0) {
			avatarIcon = new ImageIcon(avatarBytes);
		}
		return getStatusBarAvatar(avatarIcon);
	}

	public static ImageIcon getContactTabIcon() {
		return getImageIcon("talk/images/main/contact_tab.png", 22, 22);
	}

	public static ImageIcon getGroupTabIcon() {
		return getImageIcon("talk/images/main/group_tab.png", 22, 22);
	}

	public static ImageIcon getCheckIcon() {
		return getImageIcon("talk/images/buttons/check.png");
	}

	public static ImageIcon getUncheckIcon() {
		return getImageIcon("talk/images/buttons/check2.png");
	}

	public static ImageIcon getSendKeyCheckIcon() {
		return getImageIcon("talk/images/buttons/check3.png");
	}

	public static ImageIcon getDefaultAvatarIcon() {
		return getImageIcon("talk/images/avatar/default_avatar_user.png");
	}

	public static BufferedImage getDefaultAvatarImage() {
		return getImage("talk/images/avatar/default_avatar_user.png");
	}

	public static byte[] getDefaultAvatarImageBytes() {
//		return getImageBytes("talk/images/avatar/default_avatar_user.png");
		return getImageBytes("talk/images/avatar/user-default.png");
	}

	public static ImageIcon getGroupDefaultAvatarIcon() {
//		return getImageIcon("talk/images/avatar/default_avatar_group.png");
		return getImageIcon("talk/images/avatar/group-default.png");
	}

	public static Image getGroupDefaultAvatarImage() {
//		return getImage("talk/images/avatar/default_avatar_group.png");
		return getImage("talk/images/avatar/group-default.png");
	}

	public static byte[] getGroupDefaultAvatarBytes() {
//		return getImageBytes("talk/images/avatar/default_avatar_group.png");
		return getImageBytes("talk/images/avatar/group-default.png");
	}

	public static Image getButtonBackgroudImage() {
		return getImage("talk/images/buttons/button_bkg.png");
	}

	public static ImageIcon getDocumentInfoImageIcon() {
		return getImageIcon("talk/images/room/document-info.png");
	}

	public static ImageIcon getInformationImageIcon() {
		return getImageIcon("talk/images/common/information.png");
	}

	public static ImageIcon getInformationDialogImageIcon() {
		return getImageIcon("talk/images/common/info.png");
	}

	public static ImageIcon getTitlePanelImageIcon() {
		return getImageIcon("talk/images/common/tabTitle-panel.png");
	}

	public static ImageIcon getTransparent() {
		return getImageIcon("talk/images/common/transparent.png");
	}

	public static ImageIcon getLine() {
		return getImageIcon("talk/images/common/line-short.png");
	}

	public static ImageIcon getLineLong() {
		return getImageIcon("talk/images/common/line-long.png");
	}

	public static ImageIcon getLineSetting() {
		return getImageIcon("talk/images/common/line-setting.png");
	}

	public static ImageIcon getLoading() {
		return getImageIcon("talk/images/common/loading.gif");
	}

	public static ImageIcon getLoadFail() {
		return getImageIcon("talk/images/common/load-fail.png");
	}

	public static ImageIcon getGreenStar() {
		return getImageIcon("talk/images/common/star/star_green.png");
	}

	public static ImageIcon getBlueStar() {
		return getImageIcon("talk/images/common/star/star_blue.png");
	}

	public static ImageIcon getYellowStar() {
		return getImageIcon("talk/images/common/star/star_yellow.png");
	}

	public static ImageIcon getGreyStar() {
		return getImageIcon("talk/images/common/star/star_grey.png");
	}

	public static Color getBalloonRightColor1() {
		return getColor(152, 225, 101);
	}

	public static Color getBalloonRightColor2() {
		return getColor(2, 179, 0);
	}

	public static Color getBalloonBorderRightColor() {
		return getColor(131, 217, 39);
	}

	public static Color getBalloonLeftColor1() {
		return getColor(240, 240, 240);
	}

	public static Color getBalloonLeftColor2() {
		return getColor(220, 220, 220);
	}

	public static Color getBalloonBorderLeftColor() {
		return getColor(180, 180, 180);
	}

	public static Color getImageBackgroud() {
		return imageBackgroud;
	}

	static Color imageBackgroud = new Color(240, 240, 240);
}
