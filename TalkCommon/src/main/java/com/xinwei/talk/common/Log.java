/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月12日 上午11:02:35
 * 
 ***************************************************************/
package com.xinwei.talk.common;

import java.text.MessageFormat;

import com.xinwei.common.lang.Log4j;

public class Log extends Log4j {
	public static void printPushMsg(long id, String msg) {
		debug("push", "id:" + id + "," + msg);
	}

	public static void printPushMsg(long id, String msg, Object... arguments) {
		debug("push", "id:" + id + "," + MessageFormat.format(msg, arguments));
	}
}
