package com.xinwei.talk.common;

import com.xinwei.common.lang.FileUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.TalkManager;
//import com.xinwei.talk.model.CooTalk;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.message.TalkMessage;

import javax.swing.*;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TalkUtil {
	private static File TALK_HOME;
	
	public static File USER;
	
	public static File LOCAL;
	
	public static File HOME;

	public static File USERS_HOME;

	private static File ALL_USERS_HOME;

	private static File BIN_HOME;

	public static File USER_HOME;
	
	public static File SRC;
	
	public static File MAIN;
	
	public static File RESOURCES;
	
	public static File TALK;

	static {
		if (SwingUtil.isLinux()) {
			TALK_HOME = new File(Res.getProperty("talk.home.linux"));
		} else if (SwingUtil.isMac()) {
			TALK_HOME = new File(Res.getProperty("talk.home.mac"));
		} else {
			//			TALK_HOME = new File(Res.getProperty("talk.home.windows"));
			TALK_HOME = new File(System.getProperty("user.home"));
		}

		USER = FileUtil.initDirectory(TALK_HOME, "AppData");
		
		LOCAL = FileUtil.initDirectory(USER, "Local");
		
		//USER = FileUtil.initDirectory(TALK_HOME, "AppData");
		
		HOME = FileUtil.initDirectory(LOCAL, "CamTalk");
		
		USERS_HOME = FileUtil.initDirectory(HOME, "Users");

		ALL_USERS_HOME = FileUtil.initDirectory(USERS_HOME, "All Users");

		BIN_HOME = FileUtil.initDirectory(ALL_USERS_HOME, "bin");
		
		SRC = FileUtil.initDirectory(HOME, "src");
		
		MAIN = FileUtil.initDirectory(SRC, "main");
		
		RESOURCES = FileUtil.initDirectory(MAIN, "resources");
		
		TALK = FileUtil.initDirectory(RESOURCES, "talk");
	}

	public static Object getObject(String jid) {
		Object obj = null;
		if (isGroup(jid)) {
			Group group = TalkManager.getLocalManager().getGroupByJid(jid);
			if (group != null) {
				group = new Group();
				group.setJid(jid);
				group.setId(getGroupId(jid));
			}
			obj = group;

		} else {
			Talk talk = TalkManager.getLocalManager().queryTalkByJid(jid);
			if (talk != null) {
				talk = new Talk();
				talk.setJid(jid);
				talk.setTel(getUserTel(jid));
			}
			obj = talk;
		}
		return obj;
	}

	public static String getXmppResource() {
		return Res.getString("xmpp.resource", "PC").trim();
	}

	public static Object[] getTalkMessageUserInfos(TalkMessage talkMessage) {
		if (talkMessage == null)
			return null;

		boolean isSelf = true;
		ImageIcon avatar = null;
		String nick = null;

		if (talkMessage.getIsGroup()) {
			String userTel = talkMessage.getUserTel();
			String groupJid = talkMessage.getJid();
			isSelf = talkMessage.getSendOrReceive() == TalkMessage.TYPE_SEND;
			avatar = ImageManager.getTalkAvatar(getUserFullJid(userTel), 40, 40);
			Member member = TalkManager.getLocalManager().getMemberByGroupJidAndUserTel(groupJid, userTel);
			if (member == null) {
				nick = userTel;
			} else {
				nick = getDisplayName(member.getMemJid());
			}
		} else {
			isSelf = talkMessage.getSendOrReceive() == TalkMessage.TYPE_SEND;
			if (isSelf) {
				String jid = TalkManager.getVCard().getJid();
				avatar = ImageManager.getTalkAvatar(jid, 40, 40);
				nick = getDisplayName(jid);
			} else {
				String userJid = talkMessage.getJid();
				avatar = ImageManager.getTalkAvatar(userJid, 40, 40);
				nick = getDisplayName(userJid);
			}
		}

		return new Object[] { isSelf, avatar, nick };
	}

	public static String[] getCountryCodeAndTel(String tel) {
		if (tel == null)
			return null;
		String startWith = "countrycode.";
		Map<String, String> map = TalkManager.getLocalManager().get("countrycode");
		Set<Entry<String, String>> entrySet = map.entrySet();
		int length = startWith.length();
		for (Entry<String, String> entry : entrySet) {
			String key = entry.getKey();
			String code = key.substring(length);
			if (tel.startsWith("00" + code)) {
				tel = tel.substring(code.length() + 2);
				return new String[] { "+" + code, tel };
			}
		}
		return null;
	}

	public static boolean isMessageJid(String jid) {
		String userTel = getUserTel(jid);
		if (Constants.CUSTOM_TALK_TEL_GROUP_MESSAGE.equals(userTel)) {
			return true;
		}
		if (Constants.CUSTOM_TALK_TEL_SYSTEM_MESSAGE.equals(userTel)) {
			return true;
		}
		return false;
	}

	public static boolean isSelfJid(String jid) {
		return isSelfTel(getUserTel(jid));
	}

	public static boolean isSelfTel(String tel) {
		return StringUtil.equals(tel, TalkManager.getVCard().getTel());
	}

	public static boolean isSelfUId(String uid) {
		return StringUtil.equals(uid, TalkManager.getVCard().getUid());
	}

	//camtalk,group
	public static String getDisplayName(String jid) {
		LocalManager localManager = TalkManager.getLocalManager();
		if (isGroup(jid)) {//
			Group group = localManager.getGroupByJid(jid);
			if (group != null) {
				if (StringUtil.isNotBlank(group.getName())) {
					return group.getName();
				} else if (StringUtil.isNotBlank(group.getId())) {
					List<Member> memberList = TalkManager.getLocalManager().getMemberList(group.getId());
					StringBuilder sb = new StringBuilder();
					for (Member member : memberList) {
						String memNick = member.getMemNick();
						if (StringUtil.isNotBlank(memNick)) {
							if (sb.length() > 0) {
								sb.append(",");
							}
							sb.append(memNick);
						}
					}
					Integer maxLength = Res.getInteger("talk.group.maxlength", 20);
					if (sb.length() > maxLength) {
						return sb.substring(0, maxLength);
					} else if (sb.length() > 0) {
						return sb.toString();
					} else {
						return group.getId();
					}
				}
			}
			return getGroupId(jid);
		} else {
			/*VCard vCard = TalkManager.getVCard();
			if (jid.equals(vCard.getJid())) {
				String name = vCard.getName();
				String nickname = vCard.getNick();
				if (StringUtil.isNotBlank(nickname)) {
					return nickname;
				} else if (StringUtil.isNotBlank(name)) {
					return name;
				} else {
					return TalkManager.getSessionManager().getUsername();
				}
			} else {
				Talk camtalk = localManager.getTalkByJid(jid);
				if (camtalk != null) {
					String tel = getUserTel(jid);
					String displayName = TalkManager.getLocalManager().getContactName(tel);
					if (StringUtil.isNotBlank(displayName)) {
						return displayName;
					}

					if (StringUtil.isNotBlank(camtalk.getNick())) {
						return camtalk.getNick();
					}
					return tel;
				}
			}
*/
			Talk talk = localManager.getTalkByJid(jid);
			/*if (talk != null) {
				CooTalk cooTalk = (CooTalk) talk;
				if (StringUtil.isNotBlank(cooTalk.getNick())) {
					return cooTalk.getNick();
				}else if (StringUtil.isNotBlank(cooTalk.getRemarkNick())) {
					return cooTalk.getRemarkNick();
				}else if (StringUtil.isNotBlank(cooTalk.getName())) {
					return cooTalk.getName();
				}
			}*/
			return getUserTel(jid);
		}
	}

	public static String getDisplayNameMember(String gid, String jid) {
		LocalManager localManager = TalkManager.getLocalManager();

		Member member = localManager.getMemberByJid(gid, jid);

		if (member == null) {
			return getUserTel(jid);
		}
		if (StringUtil.isNotBlank(member.getMemNick())) {
			return member.getMemNick();
		}
		if (StringUtil.isNotBlank(member.getMemJid())) {
			return getUserTel(member.getMemJid());
		}
		if (StringUtil.isNotBlank(member.getUid())) {
			return member.getUid();
		}

		return "";
	}
	//
	//	public static String getDisplayName(VCard vCard) {
	//		if (vCard != null) {
	//			String name = vCard.getName();
	//			String nickname = vCard.getNick();
	//			if (StringUtil.hasLength(name)) {
	//				return name;
	//			} else if (StringUtil.hasLength(nickname)) {
	//				return nickname;
	//			} else {
	//				return TalkManager.getSessionManager().getUsername();
	//			}
	//		}
	//		return TalkManager.getSessionManager().getUsername();
	//	}

	//	public static String getCamTalkDisplayName(String jid) {
	//		Talk camTalk = TalkManager.getLocalManager().getCamTalkByJid(jid);
	//		if (camTalk != null) {
	//			return getDisplayName(camTalk);
	//		} else {
	//			return getUserTel(jid);
	//		}
	//	}
	//
	//	public static String getDisplayName(Talk camtalk) {
	//		if (StringUtil.isNotBlank(camtalk.getNick())) {
	//			return camtalk.getNick();
	//		} else if (StringUtil.isNotBlank(camtalk.getTel())) {
	//			return camtalk.getTel();
	//		} else if (StringUtil.isNotBlank(camtalk.getJid())) {
	//			return getUserTel(camtalk.getJid());
	//		}
	//
	//		return "";
	//	}
	//
	//	public static String getDisplayName(Group group) {
	//		if (StringUtil.isNotBlank(group.getName())) {
	//			return group.getName();
	//		} else if (StringUtil.isNotBlank(group.getId())) {
	//			return group.getId();
	//		} else if (StringUtil.isNotBlank(group.getJid())) {
	//			return getGroupId(group.getJid());
	//		}
	//
	//		return "";
	//	}

	//	public static String getGroupDisplayName(String jid) {
	//		Group group = TalkManager.getLocalManager().getGroupByJid(jid);
	//		if (group != null) {
	//			return getDisplayName(group);
	//		} else {
	//			return getGroupId(jid);
	//		}
	//	}

	//	public static String getDisplayName(Contact contact) {
	//		if (contact != null) {
	//			if (StringUtil.isNotBlank(contact.getDisplayName())) {
	//				return contact.getDisplayName();
	//			} else if (StringUtil.isNotBlank(contact.getFirstName())) {
	//				return contact.getFirstName();
	//			} else if (StringUtil.isNotBlank(contact.getLastName())) {
	//				return contact.getLastName();
	//			}
	//		}
	//		return "";
	//	}

	public static boolean isGroup(String jid) {
		String server = "@" + getServer(jid);
		if (server.contains(Constants.GROUP_SUBFIX)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the name portion of a XMPP address. For example, for the address
	 * "matt@jivesoftware.com/Smack", "matt" would be returned. If no username
	 * is present in the address, the empty string will be returned.
	 *
	 * @param XMPPAddress
	 *            the XMPP address.
	 * @return the name portion of the XMPP address.
	 */
	public static String getName(String XMPPAddress) {
		if (XMPPAddress == null) {
			return null;
		}
		int atIndex = XMPPAddress.lastIndexOf("@");
		if (atIndex <= 0) {
			return "";
		} else {
			return XMPPAddress.substring(0, atIndex);
		}
	}

	public static String getTalkName(String XMPPAddress) {
		String name = getName(XMPPAddress);
		name = name.replace(Constants.USER_PREFIX, "");
		name = name.replace(Constants.GROUP_PREFIX, "");
		return name;
	}

	/**
	 * Returns the server portion of a XMPP address. For example, for the
	 * address "matt@jivesoftware.com/Smack", "jivesoftware.com" would be
	 * returned. If no server is present in the address, the empty string will
	 * be returned.
	 *
	 * @param XMPPAddress
	 *            the XMPP address.
	 * @return the server portion of the XMPP address.
	 */
	public static String getServer(String XMPPAddress) {
		if (XMPPAddress == null) {
			return null;
		}
		int atIndex = XMPPAddress.lastIndexOf("@");
		// If the String ends with '@', return the empty string.
		if (atIndex + 1 > XMPPAddress.length()) {
			return "";
		}
		int slashIndex = XMPPAddress.indexOf("/");
		if (slashIndex > 0 && slashIndex > atIndex) {
			return XMPPAddress.substring(atIndex + 1, slashIndex);
		} else {
			return XMPPAddress.substring(atIndex + 1);
		}
	}

	/**
	 * Returns the XMPP address with any resource information removed. For
	 * example, for the address "matt@jivesoftware.com/Smack",
	 * "matt@jivesoftware.com" would be returned.
	 *
	 * @param XMPPAddress
	 *            the XMPP address.
	 * @return the bare XMPP address without resource information.
	 */
	public static String getBareJid(String XMPPAddress) {
		if (XMPPAddress == null) {
			return null;
		}
		int slashIndex = XMPPAddress.indexOf("/");
		if (slashIndex < 0) {
			return XMPPAddress;
		} else if (slashIndex == 0) {
			return "";
		} else {
			return XMPPAddress.substring(0, slashIndex);
		}
	}

	/**
	 * Returns the resource portion of a XMPP address. For example, for the
	 * address "matt@jivesoftware.com/Smack", "Smack" would be returned. If no
	 * resource is present in the address, the empty string will be returned.
	 *
	 * @param XMPPAddress
	 *            the XMPP address.
	 * @return the resource portion of the XMPP address.
	 */
	public static String getResource(String XMPPAddress) {
		if (XMPPAddress == null) {
			return null;
		}
		int slashIndex = XMPPAddress.indexOf("/");
		if (slashIndex + 1 > XMPPAddress.length() || slashIndex < 0) {
			return "";
		} else {
			return XMPPAddress.substring(slashIndex + 1);
		}
	}

	public static String getUserFullJid(String tel) {
		return new StringBuilder(Constants.USER_PREFIX).append(tel).append(Constants.USER_SUBFIX).toString();
	}

	public static String getGroupFullJid(String groupId) {
		return new StringBuilder(Constants.GROUP_PREFIX).append(groupId).append(Constants.GROUP_SUBFIX).toString();
	}

	public static String getGroupFullJid(String groupId, String res) {
		return new StringBuilder(Constants.GROUP_PREFIX).append(groupId).append(Constants.GROUP_SUBFIX).append("/").append(res).toString();
	}

	public static String getUserTel(Talk talk) {
		if (talk == null) {
			return null;
		}
		return getUserTel(talk.getJid());
	}

	public static String getUserTel(String XMPPAddress) {
		if (XMPPAddress == null) {
			return null;
		}
		String name = XMPPAddress;
		int atIndex = XMPPAddress.lastIndexOf("@");
		if (atIndex > 0) {
			name = XMPPAddress.substring(0, atIndex);
		}
		name = name.replace(Constants.USER_PREFIX, "");
		return name;
	}

	public static String getGroupId(String XMPPAddress) {
		if (XMPPAddress == null) {
			return null;
		}
		String name = XMPPAddress;
		int atIndex = XMPPAddress.lastIndexOf("@");
		if (atIndex > 0) {
			name = XMPPAddress.substring(0, atIndex);
		}
		name = name.replace(Constants.GROUP_PREFIX, "");
		return name;
	}

	public static String getUserNick(String XMPPAddress) {
		if (XMPPAddress == null) {
			return null;
		}
		String userTel = getUserTel(XMPPAddress);
		if (userTel == null)
			return null;
		Talk camTalk = TalkManager.getLocalManager().getTalk(userTel);
		if (camTalk != null) {
			return camTalk.getNick();
		}
		String tel = TalkManager.getVCard().getTel();
		if (userTel.equals(tel)) {
			return TalkManager.getVCard().getNick();
		}
		return userTel;
	}

	public static boolean isUserSelf(String XMPPAddress) {
		return isSelfJid(XMPPAddress);
	}

	//	public static Talk getCamTalk(String XMPPAddress) {
	//		if (XMPPAddress == null) {
	//			return null;
	//		}
	//		String userTel = getUserTel(XMPPAddress);
	//		if (userTel == null)
	//			return null;
	//		Talk camTalk = TalkManager.getLocalManager().getCamTalkByTel(userTel);
	//		if (camTalk != null) {
	//			return camTalk;
	//		}
	//		return null;
	//	}

	public static File getUserHome() {
		return getUserHome(TalkManager.getVCard().getTel());
	}

	public static File getUserHome(String tel) {
		USER_HOME = FileUtil.initDirectory(USERS_HOME, tel);
		return USER_HOME;
	}

	public static File getUserFile(String fileName) {
		return new File(getUserHome(), fileName);
	}

	public static File getFilesDirectory() {
		return getUserDirectory("downloads", "files");
	}

	public static File getImagesDirectory() {
		return getUserDirectory("downloads", "images");
	}

	public static File getUserDirectory(String... fileName) {
		return FileUtil.initDirectory(getUserHome(), fileName);
	}

	public static File getAllUsersFile(String fileName) {
		return new File(ALL_USERS_HOME, fileName);
	}

	public static File getBinFile(String fileName) {
		return new File(BIN_HOME, fileName);
	}

	public static File getBinDirectory() {
		return BIN_HOME;
	}

}
