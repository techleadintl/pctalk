package com.xinwei.talk.model.message.specific;

import com.xinwei.talk.model.message.TalkSpecific;

//离线文件消息
public class TalkFileSpecific extends TalkSpecific {
	public static final int SEND_COMPLETE = 1;
	public static final int SEND_CANCEL = 2;
	public static final int RECEIVE_COMPLETE = 3;
	public static final int RECEIVE_CANCEL = 4;

	protected String url;
	protected String filename;
	protected long length;
	public long size;
	public String fileName;

	// added by Nisal to keep sender's details
	protected String senderName;
	protected String senderAvatar;
	protected String senderUID;

	// the MD5 sum of the file's contents
	protected String hash;
	/**
	 * 1:发送成功,2:取消发送
	 */
	private int fileType = 0;

	private String fileAbsolutePath;

	public TalkFileSpecific(String url, String filename, long length) {
		setType("offlinefile");
		setUrl(url);
		setFilename(filename);
		setLength(length);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
		this.fileName = filename;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
		this.size = length;
	}

	public String getFileAbsolutePath() {
		return fileAbsolutePath;
	}

	public void setFileAbsolutePath(String fileAbsolutePath) {
		this.fileAbsolutePath = fileAbsolutePath;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderAvatar() {
		return senderAvatar;
	}

	public void setSenderAvatar(String senderAvatar) {
		this.senderAvatar = senderAvatar;
	}

	public String getSenderUID() {
		return senderUID;
	}

	public void setSenderUID(String senderUID) {
		this.senderUID = senderUID;
	}

}
