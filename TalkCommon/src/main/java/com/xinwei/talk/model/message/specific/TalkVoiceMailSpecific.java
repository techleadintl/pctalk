package com.xinwei.talk.model.message.specific;

//语音信箱消息
public class TalkVoiceMailSpecific extends TalkResourceSpecific {
	protected String date;
	protected String telNo;
	protected String nickName;
	protected String avatarUrl;

	protected String serviceName;

	public TalkVoiceMailSpecific(String url, String date, String telNo, String nickName, String avatarUrl, String serviceName) {
		setType("voicemail");
		setUrl(url);
		setDate(date);
		setTelNo(telNo);
		setNickName(nickName);
		setAvatarUrl(avatarUrl);
		setServiceName(serviceName);
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTelNo() {
		return telNo;
	}

	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

}
