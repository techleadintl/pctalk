package com.xinwei.talk.model.message;

import com.google.gson.Gson;
import com.xinwei.common.lang.GsonUtil;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.model.message.specific.TalkParam;
import com.xinwei.talk.ui.utils.MessageType;

import java.io.Serializable;
import java.util.List;

public class TalkMessage implements Serializable, Cloneable {
	public static final int STATUS_INIT = 1;
	public static final int STATUS_NO = 2;
	public static final int STATUS_YES = 3;
	public static int STATUS_READED = 1;
	public static int STATUS_UNREAD = 0;

	public static int TYPE_SEND = 0;
	public static int TYPE_RECEIVE = 1;

	protected long id;

	protected String jid;
	protected int status = 0;
	// 是否为发送消息
	protected int sendOrReceive;

	// 是否阅读
	protected int readed;

	// 是否组消息
	protected boolean isGroup;

	// 消息来源标识,用户号码
	protected String userTel;

	// 图片消息类型:3, first digit - text message, 2 - picture message, 3 - file message,
	// 4 - voice message
	// Nisal - I used this variable to send XMPP message type to detect other
	// operations that chat(group add/remove, etc...).
	protected int msgType;

	// 消息内容
	protected TalkBody talkBody;

	// 用于未读消息时使用，Nowhere else, it is a redundant field.
	protected int count;

	/*
	 * Sachini - for notifications
	 */
	protected MessageType messageType;

	public TalkMessage() {
		// TODO Auto-generated constructor stub
	}

	public TalkMessage(TalkBody talkBody) {
		this.talkBody = talkBody;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getJid() {
		return this.jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}

	public int getSendOrReceive() {
		return sendOrReceive;
	}

	public void setSendOrReceive(int type) {
		this.sendOrReceive = type;
	}

	public int getReaded() {
		return readed;
	}

	public void setReaded(int status) {
		this.readed = status;
	}

	public int getMsgType() {
		return msgType;
	}

	public void setMsgType(int msgType) {
		this.msgType = msgType;
	}

	public boolean getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}

	public String getUserTel() {
		return userTel;
	}

	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public TalkBody getTalkBody() {
		return talkBody;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	// 消息内容,为chatMessage的内容
	public byte[] getTxt() {
		if (this.talkBody != null) {
			return GsonUtil.toJson(talkBody).getBytes();
		} else {
			return null;
		}
	}

	public void setTxt(byte[] txt) {
		if (txt != null) {
			Gson gson = MessageUtil.getGsonBuilder().create();

			setTalkBody(GsonUtil.fromJson(gson, new String(txt), TalkBody.class));
		}
	}

	public void setTalkBody(TalkBody talkBody) {
		this.talkBody = talkBody;

		if (talkBody == null)
			return;

		List<TalkSpecific> msgList = talkBody.getSpecificList();

		int msgType = MessageUtil.getMessageType(msgList);

		setMsgType(msgType);
	}

	public boolean isFire() {
		if (talkBody == null)
			return false;
		return talkBody.isFire();
	}

	public List<TalkSpecific> getSpecificList() {
		if (talkBody == null)
			return null;
		return talkBody.getSpecificList();
	}

	public List<TalkSpecific> getSpecificList(Integer... types) {
		if (talkBody == null || talkBody.getSpecificList().isEmpty())
			return null;
		List<TalkSpecific> specificList = talkBody.getSpecificList();
		return MessageUtil.getMsgList(specificList, types);
	}

	public TalkParam getMsgParam() {
		if (talkBody == null || talkBody.getSpecificList().isEmpty())
			return null;
		return talkBody.getParam();
	}

	@Override
	public TalkMessage clone() {
		TalkMessage talkMessage = this;
		try {
			talkMessage = (TalkMessage) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return talkMessage;
	}

	public void setStatus(int paintStatus) {
		this.status = paintStatus;
	}
}
