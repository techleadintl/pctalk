/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月12日 下午5:41:42
 * 
 ***************************************************************/
package com.xinwei.talk.model.conf;

import com.xinwei.common.lang.SerializableUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lang.XMap;

import java.awt.*;
import java.util.*;
import java.util.List;

public class UserConfig extends XMap {

	public Object getTheme() {
		String theme = getString("theme", null);
		if (StringUtil.isNotEmpty(theme)) {
			return SerializableUtil.getObjFromStr(theme);
		}

		return null;
	}

	public void setTheme(Object theme) {
		if (theme == null)
			return;
		String value = SerializableUtil.setObjToStr(theme);
		if (value == null)
			return;
		put("theme", value);
	}

	/**
	 * 聊天字体颜色
	 */
	public int getForeground() {
		Integer color = getInteger("Foreground", null);
		if (color != null) {
			return color.intValue();
		} else {
			return Color.BLACK.getRGB();
		}
	}

	public void setForeground(int color) {
		put("Foreground", color);
	}

	/**
	 * 聊天背景颜色
	 */
	public int getBackground() {
		Integer color = getInteger("Background", null);
		if (color != null) {
			return color.intValue();
		} else {
			return Color.WHITE.getRGB();
		}
	}

	public void setBackground(int color) {
		put("Background", color);
	}

	public String getBalloon() {
		return getString("Balloon", null);
	}

	public void setBalloon(String balloon) {
		put("Balloon", balloon);
	}

	public int getFontSize() {
		Integer fontSize = getInteger("FontSize", null);
		if (fontSize != null) {
			return fontSize.intValue();
		} else {
			return 12;
		}
	}

	public void setFontSize(int fontSize) {
		put("FontSize", fontSize);
	}

	public String getFontFamily() {
		String fontFamily = getString("FontFamily");
		if (StringUtil.isBlank(fontFamily)) {
			fontFamily = SwingUtil.getFontNameArray()[0];
			setFontFamily(fontFamily);
		}
		return fontFamily;
	}

	public void setFontFamily(String fontFamily) {
		put("FontFamily", fontFamily);
	}

	public boolean getUnderline() {
		return getBoolean("Underline", false);
	}

	public void setUnderline(boolean underline) {
		put("Underline", underline);
	}

	public boolean getItalic() {
		return getBoolean("Italic", false);
	}

	public void setItalic(boolean italic) {
		put("Italic", italic);
	}

	public void setBold(boolean bold) {
		put("Bold", bold);
	}

	public boolean getBold() {
		return getBoolean("Bold", false);
	}

	public boolean getAvatarFlash() {
		return getBoolean("AvatarFlash", true);
	}

	public void setAvatarFlash(boolean flag) {
		put("AvatarFlash", flag);
	}

	public boolean getMsgSound() {
		return getBoolean("MsgSound", true);
	}

	public void setMsgSound(boolean flag) {
		put("MsgSound", flag);
	}

	public boolean getAutoUpdater() {
		return getBoolean("AutoUpdater", false);
	}

	public void setAutoUpdater(boolean flag) {
		put("AutoUpdater", flag);
	}

	public boolean getAutoPowerOn() {
		return getBoolean("AutoPowerOn", false);
	}

	public void setAutoPowerOn(boolean flag) {
		put("AutoPowerOn", flag);
	}

	public boolean getCloseMainWindow() {
		return getBoolean("CloseMainWindow", false);
	}

	public void setCloseMainWindow(boolean flag) {
		put("CloseMainWindow", flag);
	}

	public void setMuteStatus(boolean flag) {
		put("MuteStatus", flag);
	}
	
	public boolean getMuteStatus() {
		return getBoolean("MuteStatus", false);
	}
	//截图
	public boolean getShortKeyEnable(String key) {
		Map map = getMap("ShortKey");
		if (map == null) {
			return false;
		}
		List list = getList(map, key);
		if (list == null) {
			return false;
		}
		return (Boolean) list.get(0);
	}

	public void setShortKeyEnable(String key, boolean flag) {
		Map map = getMap("ShortKey");
		if (map == null) {
			map = new HashMap<String, Object>();
			put("ShortKey", map);
		}
		List list = getList(map, key);
		if (list == null) {
			list = new ArrayList<>(2);
			list.add(false);				// added may 09
			list.add("");					// added may 09
			map.put(key, list);
		}
		
		//list.add(0,flag);
		list.set(0, flag);
		map.put(key, list);
	}

	public String getShortKeyValue(String key) {
		Map map = getMap("ShortKey");
		if (map == null) {
			return "";
		}
		List list = getList(map, key);
		if (list == null || list.size() < 2) {
			return "";
		}
		//return (String) list.get(1);
		return String.valueOf(list.get(1));				// added may 09
	}

	public boolean hasShortKeyValue(String key, String value) {
		Map map = getMap("ShortKey");
		if (map == null) {
			return false;
		}
		if (value == null || value.length() == 0) {
			return false;
		}
		Set<Entry> entrySet = map.entrySet();
		for (Entry entry : entrySet) {
			Object key2 = entry.getKey();
			if (key.equals(key2)) {
				continue;
			}
			List list = (List) entry.getValue();
			//if ((Boolean) list.get(0) && value.equals((String) list.get(1))) {
			if ( value.equals((String) list.get(1))) {								// changed may 11
				return true;
			}
		}
		return false;
	}

	public void setShortKeyValue(String key, String value) {
		Map map = getMap("ShortKey");
		if (map == null) {
			map = new HashMap<String, Object>();
			put("ShortKey", map);
		}
		List list = getList(map, key);
		if (list == null) {
			list = new ArrayList(2);
			list.add(false);					// added may 09
			list.add("");						// added may 09
			map.put(key, list);
		}
		
		list.set(1, value);
		
		
		
	}

	public String getShortKeyMethod(String value) {
		Map map = getMap("ShortKey");
		if (map == null) {
			return null;
		}
		if (value == null || value.length() == 0) {
			return null;
		}
		Set<Entry> entrySet = map.entrySet();
		for (Entry entry : entrySet) {
			List list = (List) entry.getValue();
			if ((Boolean) list.get(0) && value.equals((String) list.get(1))) {
				return String.valueOf(entry.getKey());									// added may 09
			}
		}
		return null;
	}


}