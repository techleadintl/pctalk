package com.xinwei.talk.model;

public class TalkFile {
	private long msgId;
	private String url;
	private int type;
	private byte[] data;
	private String path;

	private boolean download = true;
	private boolean finish = false;

	public TalkFile() {
	}

	public TalkFile(long msgId, String url, int type, byte[] data, boolean download) {
		this.url = url;
		this.type = type;
		this.msgId = msgId;
		this.data = data;
		this.download = download;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public boolean getDownload() {
		return download;
	}

	public void setDownload(boolean download) {
		this.download = download;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isFinish() {
		return finish;
	}

	public void setFinish(boolean finish) {
		this.finish = finish;
	}

}