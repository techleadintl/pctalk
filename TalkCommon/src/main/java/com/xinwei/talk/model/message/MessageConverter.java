package com.xinwei.talk.model.message;

import org.jivesoftware.smack.packet.Message;

public interface MessageConverter {
	/**
	 * convert server response to class objects according to message type
	 **/
	public TalkBody[] conver2TalkBody(Object xmppBody);

	/**
	 * convert class object to server acceptable object
	 **/
	public Object conver2XmppBody(TalkBody talkBody, Message.Type type);
}
