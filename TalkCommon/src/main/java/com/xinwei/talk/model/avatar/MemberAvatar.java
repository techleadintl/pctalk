package com.xinwei.talk.model.avatar;

public class MemberAvatar extends Avatar {
	private String gid;
	private String uid;

	public MemberAvatar(String gid, String uid, String url) {
		super(url);
		this.uid = uid;
		this.gid = gid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}
}
