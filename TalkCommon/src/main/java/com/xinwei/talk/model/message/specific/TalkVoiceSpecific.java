package com.xinwei.talk.model.message.specific;

//语音消息
public class TalkVoiceSpecific extends TalkResourceSpecific {

	// added by Nisal to keep sender's details
	protected String senderName;
	protected String senderAvatar;
	protected String senderUID;

	public TalkVoiceSpecific() {
		setType("audio");
	}

	public TalkVoiceSpecific(String url, String filename, long length) {
		super("audio", url, filename, length);
	}

	public TalkVoiceSpecific(byte[] data, String filename) {
		super("audio", data, filename);
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderAvatar() {
		return senderAvatar;
	}

	public void setSenderAvatar(String senderAvatar) {
		this.senderAvatar = senderAvatar;
	}

	public String getSenderUID() {
		return senderUID;
	}

	public void setSenderUID(String senderUID) {
		this.senderUID = senderUID;
	}

}
