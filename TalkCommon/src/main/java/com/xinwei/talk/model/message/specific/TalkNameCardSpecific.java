package com.xinwei.talk.model.message.specific;

import com.xinwei.talk.model.message.TalkSpecific;

public class TalkNameCardSpecific extends TalkSpecific {
	private String picture;
	private String middlename;
	private String nick;
	private String lastname;
	private String firstname;
	private String note;
	private String signature;
	private String mobile;
	private String uid;
	private String area;
	private int sex;

	// added by Nisal to keep sender's details
	protected String senderName;
	protected String senderAvatar;
	protected String senderUID;

	public TalkNameCardSpecific() {
		setType("namecard");
	}

	public TalkNameCardSpecific(String nick, String firstname, String middlename, String lastname, String mobile,
			String signature, String picture, String note) {
		setType("namecard");
		this.nick = nick;
		this.firstname = firstname;
		this.middlename = middlename;
		this.lastname = lastname;
		this.picture = picture;
		this.note = note;
		this.mobile = mobile;
		this.signature = signature;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderAvatar() {
		return senderAvatar;
	}

	public void setSenderAvatar(String senderAvatar) {
		this.senderAvatar = senderAvatar;
	}

	public String getSenderUID() {
		return senderUID;
	}

	public void setSenderUID(String senderUID) {
		this.senderUID = senderUID;
	}

}