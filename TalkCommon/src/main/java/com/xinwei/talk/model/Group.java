package com.xinwei.talk.model;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;

import java.io.ByteArrayInputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Group extends XWObject {
	// groupId
	protected String id;

	// 头像地址
	protected String avatarUrl;

	// 组成员数
	protected int memberSize;

	// 群组创建者UID
	protected String creatorUid;
	// 组名称
	protected String name;

	// 群组类型
	protected String groupType;
	// 房间允许的最大人数
	protected int maxCount;
	// 群组描述
	protected String describe;
	// 是否支持邀请 0：支持（默认） 1：不支持
	protected int isAllowInvited;
	// 消息免打扰
	protected int msgDisturbFlag;
	// //组成员

	private ImageView image;

	private boolean hasMsg;
	private boolean isActiveRoom = false;

	// added ny Nisal to keep group member list
	private List<Member> groupMembers;

	/**
	 * added by: Vaasu for update the user list with new received messages
	 **/
	private int newMessageCount;
	private String newMessage;

	public int getNewMessageCount() {
		return newMessageCount;
	}

	public void setNewMessageCount(int newMessageCount) {
		this.newMessageCount = newMessageCount;
	}

	public String getNewMessage() {
		return newMessage;
	}

	public void setNewMessage(String newMessage) {
		this.newMessage = newMessage;
	}

	protected Map<String, Member> memberMap = new HashMap<String, Member>();

	public String getCreatorUid() {
		return creatorUid;
	}

	public void setCreatorUid(String creator) {
		this.creatorUid = creator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public int getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(int maxCount) {
		this.maxCount = maxCount;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		if ("\"null\"".equals(describe)) {
			return;
		}
		this.describe = describe;
	}

	public int getIsAllowInvited() {
		return isAllowInvited;
	}

	public void setIsAllowInvited(int isAllowInvited) {
		this.isAllowInvited = isAllowInvited;
	}

	public int getMsgDisturbFlag() {
		return msgDisturbFlag;
	}

	public void setMsgDisturbFlag(int msgDisturbFlag) {
		this.msgDisturbFlag = msgDisturbFlag;
	}

	public String getId() {
		return id;
	}

	public void setId(String groupId) {
		this.id = groupId;
		if (groupId == null) {
			return;
		}
		setJid(TalkUtil.getGroupFullJid(groupId));
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public int getMemberSize() {
		return memberSize;
	}

	public void setMemberSize(int memberSize) {
		this.memberSize = memberSize;
	}

	public void addMember(Member member) {
		memberMap.put(member.getMemJid(), member);
	}

	public Collection<Member> getMemberList() {
		return memberMap.values();
	}

	public Image getImg() {
		Image img = new Image(new ByteArrayInputStream(getGroupAvatarImages(jid)), 50, 50, true, true);
		return img;
	}

	public ImageView getImage() {
		ImageView pictureImageView = new ImageView();
		Image img = new Image(new ByteArrayInputStream(getAvatarImages(jid)), 50, 50, true, true);
		pictureImageView.setClip(new Circle(25, 25, 25));
		pictureImageView.setImage(img);

		this.image = pictureImageView;
		return image;
	}

	public void setImage() {
		ImageView pictureImageView = new ImageView();
		Image img = new Image(new ByteArrayInputStream(getAvatarImages(jid)), 50, 50, true, true);
		pictureImageView.setClip(new Circle(25, 25, 25));
		pictureImageView.setImage(img);

		this.image = pictureImageView;
	}

	protected static byte[] getAvatarImages(String jid) {
		byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
		return talkAvatar;
	}

	protected static byte[] getGroupAvatarImages(String jid) {
		byte[] talkAvatar = ImageManager.getGroupAvatar(jid, true);
		return talkAvatar;
	}

	public boolean isHasMsg() {
		return hasMsg;
	}

	public void setHasMsg(boolean hasMsg) {
		this.hasMsg = hasMsg;
	}

	public boolean isActiveRoom() {
		return isActiveRoom;
	}

	public void setActiveRoom(boolean isActiveRoom) {
		this.isActiveRoom = isActiveRoom;
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof Group)) {
			return false;
		}
		Group other = (Group) obj;

		return StringUtil.equals(this.getId(), other.getId());
	}

	public List<Member> getGroupMembers() {
		return groupMembers;
	}

	public void setGroupMembers(List<Member> groupMembers) {
		this.groupMembers = groupMembers;
	}

	public boolean equals2(Object obj) {
		if (!(obj instanceof Group)) {
			return false;
		}
		Group other = (Group) obj;

		boolean equals = StringUtil.equals(this.getId(), other.getId());
		if (!equals) {
			return false;
		}

		equals = StringUtil.equals(this.getAvatarUrl(), other.getAvatarUrl());
		if (!equals) {
			return false;
		}

		equals = StringUtil.equals(this.getCreatorUid(), other.getCreatorUid());
		if (!equals) {
			return false;
		}
		equals = StringUtil.equals(this.getDescribe(), other.getDescribe());
		if (!equals) {
			return false;
		}
		equals = StringUtil.equals(this.getGroupType(), other.getGroupType());
		if (!equals) {
			return false;
		}
		equals = StringUtil.equals(this.getName(), other.getName());
		if (!equals) {
			return false;
		}
		equals = this.getIsAllowInvited() == other.getIsAllowInvited();
		if (!equals) {
			return false;
		}
		equals = this.getMaxCount() == other.getMaxCount();
		if (!equals) {
			return false;
		}
		equals = this.getMemberSize() == other.getMemberSize();
		if (!equals) {
			return false;
		}
		equals = this.getMsgDisturbFlag() == other.getMsgDisturbFlag();
		if (!equals) {
			return false;
		}
		return equals;

	}

	@Override
	public String toString() {
		return "Group [id=" + id + ", avatarUrl=" + avatarUrl + ", memberSize=" + memberSize + ", creator=" + creatorUid
				+ ", name=" + name + ", groupType=" + groupType + ", maxCount=" + maxCount + ", describe=" + describe
				+ ", isAllowInvited=" + isAllowInvited + ", msgDisturbFlag=" + msgDisturbFlag + "]";
	}

}
