/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 *
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月16日 下午3:26:21
 *
 ***************************************************************/
package com.xinwei.talk.model;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.manager.ImageManager;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;

import java.io.ByteArrayInputStream;

public class Talk extends XWObject {
    protected String uid;

    //mainaccount
    protected String tel;

    protected String nick;//昵称

    protected String avatarUrl;//头像地址

    protected String signature;//签名

    protected byte[] avatar;//头像
    /**
     * added by: Vaasu
     * for update the user list with new presence
     **/
    protected int presence ;

    private ImageView image;

    private boolean hasMsg;
    private boolean isActiveRoom = false;

    /**
     * added by: Vaasu
     * for update the user list with new received messages
     **/
    private int newMessageCount;
    private String newMessage;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public Image getImg() {
        Image img = new Image(new ByteArrayInputStream(getAvatarImages(jid)), 50, 50, true, true);
        return img;
    }

    public ImageView getImage() {
        ImageView pictureImageView = new ImageView();
        Image img = new Image(new ByteArrayInputStream(getAvatarImages(jid)), 50, 50, true, true);
        pictureImageView.setClip(new Circle(15, 15, 15));
        pictureImageView.setImage(img);

        this.image = pictureImageView;
        return image;
    }

    public void setImage() {
        ImageView pictureImageView = new ImageView();
        Image img = new Image(new ByteArrayInputStream(getAvatarImages(jid)), 50, 50, true, true);
        pictureImageView.setClip(new Circle(25, 25, 25));
        pictureImageView.setImage(img);

        this.image = pictureImageView;
    }

    protected static byte[] getAvatarImages(String jid) {
        byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
        return talkAvatar;
    }

    public boolean isHasMsg() {
        return hasMsg;
    }

    public void setHasMsg(boolean hasMsg) {
        this.hasMsg = hasMsg;
    }

    public boolean isActiveRoom() {
        return isActiveRoom;
    }

    public void setActiveRoom(boolean isActiveRoom) {
        this.isActiveRoom = isActiveRoom;
    }

    public int getNewMessageCount() {
        return newMessageCount;
    }

    public void setNewMessageCount(int newMessageCount) {
        this.newMessageCount = newMessageCount;
    }

    public String getNewMessage() {
        return newMessage;
    }

    public void setNewMessage(String newMessage) {
        this.newMessage = newMessage;
    }

    public int getPresence() {
        return presence;
    }

    public void setPresence(int presence) {
        this.presence = presence;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Talk)) {
            return false;
        }
        Talk other = (Talk) obj;
        return StringUtil.equals(this.getUid(), other.getUid());
    }

    public boolean equals2(Object obj) {
        if (!(obj instanceof Talk)) {
            return false;
        }
        Talk other = (Talk) obj;
        boolean equals = StringUtil.equals(this.getUid(), other.getUid());
        if (!equals) {
            return false;
        }

        equals = StringUtil.equals(this.getTel(), other.getTel());
        if (!equals) {
            return false;
        }

        equals = StringUtil.equals(this.getJid(), other.getJid());
        if (!equals) {
            return false;
        }
        equals = StringUtil.equals(this.getNick(), other.getNick());
        if (!equals) {
            return false;
        }
        equals = StringUtil.equals(this.getSignature(), other.getSignature());
        if (!equals) {
            return false;
        }
        equals = StringUtil.equals(this.getAvatarUrl(), other.getAvatarUrl());
        if (!equals) {
            return false;
        }
        return equals;
    }

    @Override
    public String toString() {
        return "Talk [uid=" + uid + ", tel=" + tel + ", nick=" + nick + ", avatar=" + avatarUrl + "]\n";
    }


}
