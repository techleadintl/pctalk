package com.xinwei.talk.model.avatar;

import com.xinwei.common.lang.StringUtil;

public class Avatar {
	protected String url;

	public Avatar(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Avatar)) {
			return false;
		}
		Avatar other = (Avatar) obj;
		boolean equals = StringUtil.equals(this.getUrl(), other.getUrl());
		if (!equals) {
			return false;
		}
		return equals;
	}
}