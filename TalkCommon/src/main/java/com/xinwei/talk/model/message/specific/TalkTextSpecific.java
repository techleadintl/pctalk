package com.xinwei.talk.model.message.specific;

import com.xinwei.talk.model.message.TalkSpecific;

//Text message
public class TalkTextSpecific extends TalkSpecific {
	protected String msg;

	// added by Nisal to keep sender's details
	protected String senderName;
	protected String senderAvatar;
	protected String senderUID;

	public TalkTextSpecific() {
		setType("txt");
	}

	public TalkTextSpecific(String msg) {
		setType("txt");
		setMsg(msg);
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderAvatar() {
		return senderAvatar;
	}

	public void setSenderAvatar(String senderAvatar) {
		this.senderAvatar = senderAvatar;
	}

	public String getSenderUID() {
		return senderUID;
	}

	public void setSenderUID(String senderUID) {
		this.senderUID = senderUID;
	}

}
