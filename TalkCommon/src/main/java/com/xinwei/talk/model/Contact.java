/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月16日 下午3:21:35
 * 
 ***************************************************************/
package com.xinwei.talk.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.StringUtil;

public class Contact {
	private String id;
	private String displayName; // 手机显示名字
	private String firstName; // 用户名称
	private String lastName; // 用户名称
	private String emaill; // 电子邮箱
	private String addr; // 家庭住址
	private String birthday; // 生日
	private String company; // 公司
	private String remark; // 备注信息
	private String phones;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmaill() {
		return emaill;
	}

	public void setEmaill(String emaill) {
		this.emaill = emaill;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPhones() {
		return phones;
	}

	public List<String> getPhoneList() {
		if (phones == null)
			return null;
		String[] split = phones.split(",");
		return Arrays.asList(split);
	}

	public void setPhoneList(Collection<String> phoneList) {
		if (phoneList == null)
			return;
		for (String tel : phoneList) {
			if (tel == null || tel.trim().length() == 0) {
				continue;
			}
			if (phones == null) {
				phones = tel;
			} else {
				phones += "," + tel;
			}
		}
	}

	public void addPhone(String tel) {
		if (tel == null || tel.trim().length() == 0) {
			return;
		}
		if (phones == null) {
			phones = tel;
		} else if (tel != null && tel.trim().length() > 0) {
			phones += "," + tel;
		}
	}

	public void setPhones(String phones) {
		this.phones = phones;
	}

	//	public McWillList2<Talk> getCamtalkList() {
	//		return camtalkList;
	//	}
	//
	//	public void addCamTalk(Talk camTalk) {
	//		if (this.camtalkList == null) {
	//			this.camtalkList = new ArrayList<Talk>();
	//		}
	//		this.camtalkList.add(camTalk);
	//
	//		String tel = camTalk.getTel();
	//		if (phones == null) {
	//			phones = tel;
	//		} else if (tel != null && tel.trim().length() > 0) {
	//			phones += "," + tel;
	//		}
	//	}

	public boolean equals(Object obj) {
		if (!(obj instanceof Contact)) {
			return false;
		}
		Contact other = (Contact) obj;
		boolean equals = StringUtil.equals(this.getDisplayName(), other.getDisplayName());
		if (!equals) {
			return false;
		}

		equals = StringUtil.equals(this.getFirstName(), other.getFirstName());
		if (!equals) {
			return false;
		}

		equals = StringUtil.equals(this.getLastName(), other.getLastName());
		if (!equals) {
			return false;
		}

		equals = StringUtil.equals(this.getEmaill(), other.getEmaill());
		if (!equals) {
			return false;
		}

		equals = StringUtil.equals(this.getAddr(), other.getAddr());
		if (!equals) {
			return false;
		}

		equals = StringUtil.equals(this.getBirthday(), other.getBirthday());
		if (!equals) {
			return false;
		}

		equals = StringUtil.equals(this.getCompany(), other.getCompany());
		if (!equals) {
			return false;
		}

		equals = StringUtil.equals(this.getRemark(), other.getRemark());
		if (!equals) {
			return false;
		}
		equals = CollectionUtil.removeAll(this.getPhoneList(), other.getPhoneList()).isEmpty();
		if (!equals) {
			return false;
		}

		return equals;
	}

	@Override
	public String toString() {
		return "Contact [displayName=" + displayName + ", phones=" + phones + "]";
	}
}
