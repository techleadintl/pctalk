package com.xinwei.talk.model.avatar;

public class BackgroundAvatar extends Avatar {
	private String userTel;

	public BackgroundAvatar(String userTel, String url) {
		super(url);
		this.userTel = userTel;
	}

	public String getUserTel() {
		return userTel;
	}

	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

}