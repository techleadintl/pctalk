/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月17日 下午3:43:25
 * 
 ***************************************************************/
package com.xinwei.talk.model;

public class XWObject {
	//jid
	protected String jid;

	public XWObject() {
	}

	public XWObject(String jid) {
		this.jid = jid;
	}

	public String getJid() {
		return jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}
}
