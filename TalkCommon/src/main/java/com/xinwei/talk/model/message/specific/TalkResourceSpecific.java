/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月23日 上午10:51:14
 * 
 ***************************************************************/
package com.xinwei.talk.model.message.specific;

import java.util.HashMap;
import java.util.Map;

import com.xinwei.talk.model.message.TalkSpecific;

public class TalkResourceSpecific extends TalkSpecific {
	protected String url;
	protected String filename;
	protected long length;
	protected Map attachment;

	protected transient byte[] data;

	public TalkResourceSpecific() {
	}

	public TalkResourceSpecific(String type, String url, String filename, long length) {
		setType(type);
		setUrl(url);
		setFilename(filename);
		setLength(length);
	}

	public TalkResourceSpecific(String type, byte[] data, String filename) {
		setType(type);
		setData(data);
		setFilename(filename);
		setLength(data.length);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public Map getAttachment() {
		return attachment;
	}

	public void setAttachment(Map attachment) {
		this.attachment = attachment;
	}

	public void addAttachment(String key, Object value) {
		if (this.attachment == null) {
			this.attachment = new HashMap<String, Object>();
		}
		this.attachment.put(key, value);
	}

	public Object getAttachment(String key) {
		if (this.attachment == null) {
			return null;
		}
		return this.attachment.get(key);
	}
}
