/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月12日 下午5:41:42
 * 
 ***************************************************************/
package com.xinwei.talk.model.conf;

import com.xinwei.common.lang.XMap;
import com.xinwei.spark.Encryptor;

public class AppConfig extends XMap {
	/**
	 * Returns the last used Username
	 *
	 * @return the username of the agent.
	 */
	public String getLastUsername() {
		return getString("username");
	}

	/**
	 * Sets the Agents username.
	 *
	 * @param username
	 *            the agents username.
	 */
	public void setLastUsername(String username) {
		put("username", username);
	}

	/**
	 * returns the password for an encrypted jid
	 * 
	 * @param barejid
	 * @return
	 */
	public String getPasswordForUser(String barejid) {
		try {
			String pw = "password" + Encryptor.encrypt(barejid);
			return Encryptor.decrypt(getString(pw));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Sets the password for barejid<br>
	 * both will be encrypted
	 * 
	 * @param barejid
	 * @param password
	 * @throws Exception
	 */
	public void setPasswordForUser(String barejid, String password) throws Exception {
		String user = "password" + Encryptor.encrypt(barejid);
		String pw = Encryptor.encrypt(password);
		put(user, pw);
	}

}
