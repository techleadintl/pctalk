/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月5日 上午9:00:39
 * 
 ***************************************************************/
package com.xinwei.talk.model.message.specific;

import java.io.Serializable;

public class TalkParam implements Serializable {
	public static final int BALLOON_TYPE_NORMAL = 0;
	public static final int BALLOON_TYPE_HINT = 1;
	//	family
	private String f;
	//	size
	private int s = 12;
	//	underline  0=false;1=true
	private int u;
	//italic 0=false;1=true
	private int i;
	//bold 0=false;1=true
	private int b;
	//foreground
	private int fg;
	//background
	private int bg;

	private String balloon;

	private int balloonType = BALLOON_TYPE_NORMAL;

	public TalkParam() {
	}

	public TalkParam(String f, int s, int u, int i, int b, int fg, int bg, String balloon, int balloonType) {
		this.f = f;
		this.s = s;
		this.u = u;
		this.i = i;
		this.b = b;
		this.fg = fg;
		this.bg = bg;
		this.balloon = balloon;
		this.balloonType = balloonType;
	}

	public String getF() {
		return f;
	}

	public void setF(String f) {
		this.f = f;
	}

	public int getS() {
		return s;
	}

	public void setS(int s) {
		this.s = s;
	}

	public int getU() {
		return u;
	}

	public void setU(int u) {
		this.u = u;
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public int getFg() {
		return fg;
	}

	public void setFg(int fg) {
		this.fg = fg;
	}

	public int getBg() {
		return bg;
	}

	public void setBg(int bg) {
		this.bg = bg;
	}

	public String getBalloon() {
		return balloon;
	}

	public void setBalloon(String balloon) {
		this.balloon = balloon;
	}

	public int getBalloonType() {
		return balloonType;
	}

	public void setBalloonType(int msgType) {
		this.balloonType = msgType;
	}
}
