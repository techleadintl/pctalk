/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月8日 上午10:59:20
 * 
 ***************************************************************/
package com.xinwei.talk.model.message;

import java.io.Serializable;

public class TalkSpecific implements Serializable {
	protected String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
