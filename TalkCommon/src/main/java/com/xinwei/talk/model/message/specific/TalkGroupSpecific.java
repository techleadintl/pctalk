package com.xinwei.talk.model.message.specific;

import java.util.List;
import java.util.Map;

import com.xinwei.common.vo.McWillMap;
import com.xinwei.talk.model.message.TalkSpecific;

public class TalkGroupSpecific extends TalkSpecific {
	McWillMap map;

	public TalkGroupSpecific(McWillMap map) {
		this.map = map;
	}

	public int getOperType() {
		return map.getInteger("type",0);
	}

	public String getFromCid() {
		return map.getString("fromCid");
	}

	public String getFromNick() {
		return map.getString("fromNick");
	}

	public String getString(String key) {
		return map.getString(key);
	}

	public List getList(String key) {
		return map.getList(key);
	}
	public Map getMap(String key) {
		return map.getMap(key);
	}
}