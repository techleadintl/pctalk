package com.xinwei.talk.model;

import com.xinwei.spark.Encryptor;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class VCard implements Serializable {
	private int bookVersion;

	private String loginServer;
	
	private String passwordServer;

	private int loginPort = 80;
	//名称
	private String name;
	private String autoMsg;
	//昵称
	private String nick;
	//个性签名
	private String signature;

	private String tel;
	//密码
	private String password;

	private String uid;
	private String jid;

	private int countryCode;
	private int networkId;

	private String picture;

	private byte[] avatar;

	private List<String> sipServers;
	private List<String> imServers;
	private List<String> appServers;
	private List<String> fileServers;

	//是否记住密码
	private boolean savePassword;
	//是否自动登录
	private boolean autoLogin;

	private long timestamp;

	private Map<String, Object> KV = new LinkedHashMap<String, Object>();
	
	private boolean switchedOn;

	public boolean isSwitchedOn() {
		return switchedOn;
	}

	public void setSwitchedOn(boolean switchedOn) {
		this.switchedOn = switchedOn;
	}

	public String getImServer() {
		if (imServers == null || imServers.isEmpty())
			return null;

		return imServers.get(0);
	}

	public String getServer() {
		return getAppServer();
	}

	public String getAppServer() {
		if (appServers == null || appServers.isEmpty())
			return null;

		return appServers.get(0);
	}

	public String getFileServer() {
		if (fileServers == null || fileServers.isEmpty())
			return null;

		return fileServers.get(0);
	}

	public String getName() {
		return name;
	}

	public void setName(String username) {
		this.name = username;
	}

	public String getAutoMsg() {
		return autoMsg;
	}

	public void setAutoMsg(String autoMsg) {
		this.autoMsg = autoMsg;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String telNo) {
		this.tel = telNo;
	}

	public int getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}

	public int getNetworkId() {
		return networkId;
	}

	public void setNetworkId(int networkId) {
		this.networkId = networkId;
	}

	public List<String> getSipServers() {
		return sipServers;
	}

	public void setSipServers(List<String> sipServers) {
		this.sipServers = sipServers;
	}

	public List<String> getImServers() {
		return imServers;
	}

	public void setImServers(List<String> imServers) {
		this.imServers = imServers;
	}

	public List<String> getAppServers() {
		return appServers;
	}

	public void setAppServers(List<String> appServers) {
		this.appServers = appServers;
	}

	public List<String> getFileServers() {
		return fileServers;
	}

	public void setFileServers(List<String> fileServers) {
		this.fileServers = fileServers;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getJid() {
		return jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}

	public String getLoginServer() {
		return loginServer;
	}

	public void setLoginServer(String loginServer) {
		this.loginServer = loginServer;
	}

	public int getLoginPort() {
		return loginPort;
	}

	public void setLoginPort(int loginPort) {
		this.loginPort = loginPort;
	}

	public void putV(String key, Object value) {
		KV.put(key, value);
	}

	public <T> T getV(String key) {
		return (T) KV.get(key);
	}

	public boolean isSelf(String otherTel) {
		if (otherTel == null) {
			return false;
		}

		return otherTel.equals(tel);
	}

	public boolean isSelf2(String otherUid) {
		if (otherUid == null) {
			return false;
		}

		return otherUid.equals(uid);
	}

	public byte[] getAvatar() {
		return avatar;
	}

	public void setAvatar(byte[] avatar) {
		this.avatar = avatar;
	}

	public Map<String, Object> getKV() {
		return KV;
	}

	public void setKV(Map<String, Object> kV) {
		KV = kV;
	}

	public String getPassword() {
		try {
			return Encryptor.decrypt(password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setPassword(String password) {
		try {
			this.password = Encryptor.encrypt(password);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isAutoLogin() {
		return autoLogin;
	}

	public void setAutoLogin(boolean autoLogin) {
		this.autoLogin = autoLogin;
	}

	public boolean isSavePassword() {
		return savePassword;
	}

	public void setSavePassword(boolean savePassword) {
		this.savePassword = savePassword;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public int getBookVersion() {
		return bookVersion;
	}

	public void setBookVersion(int bookVersion) {
		this.bookVersion = bookVersion;
	}
	
	public String getPasswordServer() {
		return passwordServer;
	}

	public void setPasswordServer(String passwordServer) {
		this.passwordServer = passwordServer;
	}
}
