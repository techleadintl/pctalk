/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月27日 上午10:34:47
 * 
 ***************************************************************/
package com.xinwei.talk.model.message.specific;

import com.xinwei.talk.model.message.TalkSpecific;

//	   "type": "loc",
//       "addr": "北京市海淀区东北旺路2号",
//       "lat": 40.044958,
//       "lng": 116.3021
public class TalkLocationSpecific extends TalkSpecific {
	private String title;
	private String addr;
	private String lat;
	private String lng;

	public TalkLocationSpecific() {
		setType("loc");
	}

	public TalkLocationSpecific(String lat, String lng, String addr) {
		setType("loc");
		setLat(lat);
		setLng(lng);
		setAddr(addr);
	}

	public TalkLocationSpecific(String lat, String lng, String title, String addr) {
		setType("loc");
		setLat(lat);
		setLng(lng);
		setAddr(addr);
		setTitle(type);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

}
