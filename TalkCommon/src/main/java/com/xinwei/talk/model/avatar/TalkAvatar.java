package com.xinwei.talk.model.avatar;

public class TalkAvatar extends Avatar {
	private String userTel;

	public TalkAvatar(String userTel, String url) {
		super(url);
		this.userTel = userTel;
	}

	public String getUserTel() {
		return userTel;
	}

	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

}