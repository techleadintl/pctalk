package com.xinwei.talk.model.message.specific;

import com.xinwei.talk.model.message.TalkSpecific;

/**
 * keep message sender details in this class object
 * 
 * @author nisal
 **/
public class SenderSpecific extends TalkSpecific {
	// added by Nisal to keep sender's details
	protected String senderName;
	protected String senderAvatar;
	protected String senderUID;

	public SenderSpecific() {
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderAvatar() {
		return senderAvatar;
	}

	public void setSenderAvatar(String senderAvatar) {
		this.senderAvatar = senderAvatar;
	}

	public String getSenderUID() {
		return senderUID;
	}

	public void setSenderUID(String senderUID) {
		this.senderUID = senderUID;
	}

}
