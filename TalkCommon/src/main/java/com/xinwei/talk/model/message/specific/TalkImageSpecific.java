package com.xinwei.talk.model.message.specific;

//图片消息
public class TalkImageSpecific extends TalkResourceSpecific {

	// added by Nisal to keep sender's details
	protected String senderName;
	protected String senderAvatar;
	protected String senderUID;

	public TalkImageSpecific() {
		setType("img");
	}

	public TalkImageSpecific(String url, String filename) {
		super("img", url, filename, 0);
	}

	public TalkImageSpecific(byte[] data, String filename) {
		super("img", data, filename);
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderAvatar() {
		return senderAvatar;
	}

	public void setSenderAvatar(String senderAvatar) {
		this.senderAvatar = senderAvatar;
	}

	public String getSenderUID() {
		return senderUID;
	}

	public void setSenderUID(String senderUID) {
		this.senderUID = senderUID;
	}

}
