/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月16日 下午3:26:21
 * 
 ***************************************************************/
package com.xinwei.talk.model;

import com.xinwei.common.lang.StringUtil;

public class MyTalk extends Talk {
	private String res;

	public boolean equals(Object obj) {
		if (!(obj instanceof MyTalk)) {
			return false;
		}
		MyTalk other = (MyTalk) obj;
		boolean equals = super.equals(obj);
		if (!equals)
			return false;
		equals = StringUtil.equals(this.getRes(), other.getRes());
		if (!equals) {
			return false;
		}
		return equals;
	}

	public boolean equals2(Object obj) {
		if (!(obj instanceof MyTalk)) {
			return false;
		}
		MyTalk other = (MyTalk) obj;
		boolean equals = super.equals2(obj);
		if (!equals)
			return false;
		equals = StringUtil.equals(this.getRes(), other.getRes());
		if (!equals) {
			return false;
		}
		return equals;
	}

	public String getRes() {
		return res;
	}

	public void setRes(String res) {
		this.res = res;
	}
}
