package com.xinwei.talk.model;

import com.xinwei.common.lang.StringUtil;

public class Member {
	//	1:管理员
	public final static int ROLE_ADMIN = 1;
	//	2：普通成员
	public final static int ROLE_USER = 2;

	//groupId
	protected String gid;

	//用户UID
	protected String uid;

	//头像地址
	protected String avatarUrl;

	protected byte[] avatar;

	//	用户信息最新标识
	protected String userKey;

	//该用户在该组内角色 ,1:管理员，2：普通成员
	protected int memRole = ROLE_ADMIN;
	//该组内用户的备注名
	protected String memNickName;
	//
	protected String memJid;

	public Member() {
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String groupId) {
		this.gid = groupId;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String userId) {
		this.uid = userId;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public byte[] getAvatar() {
		return avatar;
	}

	public void setAvatar(byte[] avatar) {
		this.avatar = avatar;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public int getMemRole() {
		return memRole;
	}

	public void setMemRole(int memRole) {
		this.memRole = memRole;
	}

	public String getMemNick() {
		return memNickName;
	}

	public void setMemNick(String memNick) {
		if (memNick == null)
			return;
		if (memNick.trim().length() == 0) {
			return;
		}
		this.memNickName = memNick;
	}

	public String getMemJid() {
		return memJid;
	}

	public void setMemJid(String memJid) {
		if (memJid == null)
			return;
		this.memJid = memJid;
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof Member)) {
			return false;
		}
		Member other = (Member) obj;

		boolean equals = StringUtil.equals(this.getGid(), other.getGid());
		if (!equals) {
			return false;
		}

		equals = StringUtil.equals(this.getUid(), other.getUid());
		if (!equals) {
			return false;
		}

		return equals;
	}

	public boolean equals2(Object obj) {
		if (!(obj instanceof Member)) {
			return false;
		}
		Member other = (Member) obj;

		boolean equals = StringUtil.equals(this.getGid(), other.getGid());
		if (!equals) {
			return false;
		}
		equals = StringUtil.equals(this.getUid(), other.getUid());
		if (!equals) {
			return false;
		}
		equals = StringUtil.equals(this.getMemJid(), other.getMemJid());
		if (!equals) {
			return false;
		}
		equals = StringUtil.equals(this.getAvatarUrl(), other.getAvatarUrl());
		if (!equals) {
			return false;
		}
		equals = StringUtil.equals(this.getMemNick(), other.getMemNick());
		if (!equals) {
			return false;
		}

		equals = this.getMemRole() == other.getMemRole();
		if (!equals) {
			return false;
		}

		return equals;
	}

	@Override
	public String toString() {
		return "Member [groupId=" + gid + ", userId=" + uid + ", avatarUrl=" + avatarUrl + ", userKey=" + userKey + ", memRole=" + memRole + ", memNickName=" + memNickName + ", memJid=" + memJid
				+ "]";
	}

}
