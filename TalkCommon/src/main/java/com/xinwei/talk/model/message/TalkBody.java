/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月6日 上午9:09:03
 * 
 ***************************************************************/
package com.xinwei.talk.model.message;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.xinwei.talk.model.message.specific.TalkParam;

public class TalkBody implements Cloneable {
	public static int MESSAGE_MODEL_CHAT = 0; //单聊
	public static int MESSAGE_MODEL_GROUPCHAT = 1; //群聊

	private String from;
	private String to;

	//阅读后销毁
	private boolean isFire;

	protected String packetId;

	private List<TalkSpecific> specificList;

	private TalkParam param;;

	private Date time;

	/** 单聊还是群聊 chat:0,groupchat:1 **/
	private int type;

	public TalkBody() {
	}

	public TalkBody(String from, String to) {
		this.from = from;
		this.to = to;
	}

	public List<TalkSpecific> getSpecificList() {
		return specificList;
	}

	public void clear() {
		if (specificList != null)
			specificList.clear();
	}

	public void setSpecificList(List<TalkSpecific> bodyList) {
		this.specificList = bodyList;
	}

	public void addSpecific(TalkSpecific messageBody) {
		if (specificList == null) {
			specificList = new ArrayList<TalkSpecific>();
		}
		specificList.add(messageBody);
	}

	public boolean isEmpty() {
		return specificList == null || specificList.isEmpty();
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getPacketId() {
		return packetId;
	}

	public void setPacketId(String packetId) {
		this.packetId = packetId;
	}

	public boolean isFire() {
		return isFire;
	}

	public void setFire(boolean isFire) {
		this.isFire = isFire;
	}

	public TalkParam getParam() {
		return param;
	}

	public void setParam(TalkParam param) {
		this.param = param;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public TalkBody clone() {
		TalkBody talkBody = this;
		try {
			talkBody = (TalkBody) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return talkBody;
	}
}
