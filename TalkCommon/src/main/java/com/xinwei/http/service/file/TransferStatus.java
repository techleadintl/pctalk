package com.xinwei.http.service.file;
public enum TransferStatus {

		/**
		 * An error occurred during the transfer.
		 */
		error("Error"),

		/**
		 * The initial isInputing of the file transfer.
		 */
		initial("Initial"),

		/**
		 * The transfer is in progress.
		 */
		in_progress("In Progress"),

		/**
		 * The transfer has completed successfully.
		 */
		complete("Complete"),

		/**
		 * The file transfer was cancelled
		 */
		cancelled("Cancelled");

		private String status;

		private TransferStatus(String status) {
			this.status = status;
		}

		public String toString() {
			return status;
		}
	}
