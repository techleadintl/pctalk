package com.xinwei.http.service;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.entity.mime.content.FileBody;

import com.google.gson.Gson;
import com.xinwei.common.lang.GsonUtil;
import com.xinwei.common.lang.HttpUtils;
import com.xinwei.http.listener.DownloadListener;
import com.xinwei.http.model.HttpRequestInfo;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.talk.model.VCard;

public abstract class AbstractHttpFileService extends VCardHelper implements HttpFileService {

	public AbstractHttpFileService() {
	}

	/** 獲取文件字节數組 **/
	public byte[] download(String url) throws Exception {
		return HttpUtils.download(url);
	}

	/** 获得文件输入流 **/
	public byte[] downloadFile(String url, DownloadListener listener) throws Exception {
		return HttpUtils.download(url, listener);
	}

	/** 獲取文件字节數組 **/
	public byte[] downloadImage(String url) throws Exception {
		return HttpUtils.download(url);
	}

	/** 头像上传 **/
	public HttpResponseInfo uploadAvatarFile(String fileFullName, byte[] icon, VCard vCard) throws Exception {
		String filePartName = "file";

		Map<String, String> params = new HashMap<String, String>();

		String fileType = "";

		String fileName = fileFullName;
		int index = fileFullName.lastIndexOf(".");
		if (index != -1) {
			fileName = fileFullName.substring(0, index);
			fileType = fileFullName.substring(index + 1);
		}

		HttpRequestInfo fileRequestInfo = getHttpAvatarRequestInfo(fileName, fileType, vCard);

		params.put("requestInfo", GsonUtil.toJson(fileRequestInfo));

		String url = getAvatarFileServerUrl();
		String responseStr = HttpUtils.upload(url, icon, fileFullName, filePartName, params);

		HttpResponseInfo fileResponseInfo = GsonUtil.fromJson(responseStr, HttpResponseInfo.class);

		getTalkServiceHelper().handleResponse(fileResponseInfo, url, params);

		return fileResponseInfo;

	}

	/** 聊天文件上传 **/
	public HttpResponseInfo uploadChatFile(Object obj, File file) throws Exception {
		HttpResponseInfo fileResponseInfo = uploadFile(obj, getChatFileServerUrl(), file, file.getName());
		handleFileResponseInfo(fileResponseInfo);
		return fileResponseInfo;
	}

	/** 聊天文件上传 **/
	public HttpResponseInfo uploadChatFile(Object obj, FileBody fileBody) throws Exception {
		HttpResponseInfo fileResponseInfo = uploadFile(obj, getTransferFileServerUrl(), fileBody, fileBody.getFilename());
		handleFileResponseInfo(fileResponseInfo);
		return fileResponseInfo;
	}
	

	/** 聊天文件上传 **/
	public HttpResponseInfo uploadChatFile(Object obj, byte[] bytes, String fileFullName) throws Exception {
		String filePartName = "file";

		Map<String, String> params = new HashMap<String, String>();

		String fileType = "";

		String fileName = fileFullName;
		int index = fileFullName.lastIndexOf(".");
		if (index != -1) {
			fileName = fileFullName.substring(0, index);
			fileType = fileFullName.substring(index + 1);
		}

		HttpRequestInfo fileRequestInfo = getHttpFileChatRequestInfo(fileName, fileType, obj);

		params.put("requestInfo", GsonUtil.toJson(fileRequestInfo));

		String fileServerUrl = getChatFileServerUrl();
		String responseStr = HttpUtils.upload(fileServerUrl, bytes, fileFullName, filePartName, params);

		HttpResponseInfo fileResponseInfo = GsonUtil.fromJson(responseStr, HttpResponseInfo.class);

		getTalkServiceHelper().handleResponse(fileResponseInfo, fileServerUrl, params);
		handleFileResponseInfo(fileResponseInfo);
		return fileResponseInfo;
	}

	private void handleFileResponseInfo(HttpResponseInfo fileResponseInfo) {
		HashMap<Object, Object> attachment = new HashMap<>();
		fileResponseInfo.setAttachment(attachment);
		setAttachment(fileResponseInfo, attachment);
	}

	protected HttpResponseInfo uploadFile(Object obj, String url, Object file, String fileFullName, String[]... otherParams) throws Exception {
		Gson gson = new Gson();

		String filePartName = "file";

		Map<String, String> params = new HashMap<String, String>();

		String fileType = "";

		String fileName = fileFullName;
		int index = fileFullName.lastIndexOf(".");
		if (index != -1) {
			fileName = fileFullName.substring(0, index);
			fileType = fileFullName.substring(index + 1);
		}

		HttpRequestInfo fileRequestInfo = getHttpFileChatRequestInfo(fileName, fileType, obj);

		for (String[] entry : otherParams) {
			getTalkServiceHelper().addRequestParam(fileRequestInfo, entry[0], entry[1]);
		}

		params.put("requestInfo", gson.toJson(fileRequestInfo));

		String responseStr = HttpUtils.upload(url, file, filePartName, params);

		HttpResponseInfo fileResponseInfo = gson.fromJson(responseStr, HttpResponseInfo.class);

		getTalkServiceHelper().handleResponse(fileResponseInfo, url, params);
		handleFileResponseInfo(fileResponseInfo);
		return fileResponseInfo;
	}

	protected HttpResponseInfo deleteFile(String url, String[]... otherParams) throws Exception {
		Gson gson = new Gson();

		Map<String, String> params = new HashMap<String, String>();

		HttpRequestInfo fileRequestInfo = new HttpRequestInfo();

		for (String[] entry : otherParams) {
			getTalkServiceHelper().addRequestParam(fileRequestInfo, entry[0], entry[1]);
		}

		params.put("requestInfo", gson.toJson(fileRequestInfo));

		String responseStr = HttpUtils.post(url, params);

		HttpResponseInfo fileResponseInfo = gson.fromJson(responseStr, HttpResponseInfo.class);

		getTalkServiceHelper().handleResponse(fileResponseInfo, url, params);

		return fileResponseInfo;
	}
}
