package com.xinwei.http.service;

import java.util.Map;

import com.xinwei.http.model.HttpRequestInfo;
import com.xinwei.http.model.HttpResponseInfo;

public interface ITalkServiceHelper {
	void addRequestParam(HttpRequestInfo requestInfo, String key, Object value);

	void handleResponse(HttpResponseInfo responseInfo, String url, Map<String, String> params) throws Exception;

	public Object getResponseParam(HttpResponseInfo responseInfo, String key);

	public int getResult(HttpResponseInfo responseInfo);

	public String getResultMsg(HttpResponseInfo responseInfo);

}
