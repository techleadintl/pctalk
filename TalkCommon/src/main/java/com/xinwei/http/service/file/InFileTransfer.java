package com.xinwei.http.service.file;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.xinwei.common.lang.HttpUtils;
import com.xinwei.talk.manager.TalkManager;

public class InFileTransfer {
	private boolean cancle = false;
	private TransferStatus status = TransferStatus.initial;

	private Throwable exception;
	private XWInputStream inputStream;

	private String httpUrl;
	private String fileName;
	private long fileSize;

	public InFileTransfer(String httpUrl, String fileName, long fileSize) {
		this.httpUrl = httpUrl;
		this.fileSize = fileSize;
		this.fileName = fileName;
	}

	public void recieveFile(final File file) throws Exception {
		if (file != null) {
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					throw new Exception("Could not create file to write too", e);
				}
			}
			if (!file.canWrite()) {
				throw new IllegalArgumentException("Cannot write to provided file");
			}
		} else {
			throw new IllegalArgumentException("File cannot be null");
		}

		Thread transferThread = new Thread(new Runnable() {
			public void run() {
				OutputStream outputStream = null;
				try {
					outputStream = new FileOutputStream(file);

					inputStream = new XWInputStream(outputStream);
					HttpUtils.download(httpUrl, inputStream);
					outputStream.flush();
					if (!cancle) {
						status = TransferStatus.complete;
					} else {
						status = TransferStatus.cancelled;
					}
				} catch (Exception e) {
					status = TransferStatus.error;
					exception = e;
					return;
				} finally {
					close(outputStream);
				}
			}

		}, "File Transfer " + System.currentTimeMillis());
		transferThread.start();

	}

	private void close(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (Throwable io) {
				/* Ignore */
			}
		}
	}

	public long getAmountWritten() {
		if (inputStream == null) {
			return 0;
		}
		return inputStream.getAmountWritten();
	}

	public long getFileSize() {
		return fileSize;
	}

	public boolean isDone() {
		return status == TransferStatus.cancelled || status == TransferStatus.error || status == TransferStatus.complete;
	}

	public void cancel() {
		this.cancle = true;
		this.inputStream.close();
		status = TransferStatus.cancelled;
	}

	public TransferStatus getStatus() {
		return status;
	}

	public void setStatus(TransferStatus status) {
		this.status = status;
	}

	public Throwable getException() {
		return exception;
	}

	public String getFileName() {
		return fileName;
	}

	public String getHttpUrl() {
		return httpUrl;
	}
}