package com.xinwei.http.service.file;

import java.io.OutputStream;
import java.net.SocketException;

import com.xinwei.http.listener.DownloadListener;

public class XWInputStream implements DownloadListener {
	private long amountWritten;
	private OutputStream outputStream;

	private boolean isClosed;

	protected XWInputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
		this.amountWritten = 0;
	}

	@Override
	public void read(byte[] b, int length) throws Exception {
		if (isClosed) {
			throw new SocketException("xwinputstream close");
		}
		this.amountWritten += length;
		outputStream.write(b, 0, length);
	}

	public long getAmountWritten() {
		return amountWritten;
	}

	public boolean isClosed() {
		return isClosed;
	}

	public void close() {
		isClosed = true;
	}
}
