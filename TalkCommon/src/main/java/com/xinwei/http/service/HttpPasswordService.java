package com.xinwei.http.service;

import com.xinwei.http.model.HttpResponseInfo;

public interface HttpPasswordService {
	public HttpResponseInfo sendAuthCode(String telNo,String tag) throws Exception;
	public HttpResponseInfo checkAuthCode(String telNo,String authCode,String seqCode) throws Exception;
	public HttpResponseInfo modifyPassword(String telNo,String password,String sessionId) throws Exception;
}
