/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月31日 下午1:25:43
 * 
 ***************************************************************/
package com.xinwei.http.service.file;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.content.InputStreamBody;

public class InputStreamTransfer extends InputStreamBody {
	private XWOutputStream outstream;
	private boolean cancle = false;
	private TransferStatus status = TransferStatus.initial;
	
	private Throwable exception;
	
	public InputStreamTransfer(InputStream in, ContentType contentType) {
		super(in, contentType);
	}


	public void writeTo(OutputStream arg0) throws IOException {
		this.outstream = new XWOutputStream(arg0);
		InputStream in = null;
		try {
			in = getInputStream();
			byte[] tmp = new byte[4096];
			int l;
			setStatus(TransferStatus.in_progress);
			while ((l = in.read(tmp)) != -1 && !cancle) {
				outstream.write(tmp, 0, l);
			}
			outstream.flush();
			if (!cancle) {
				setStatus(status = TransferStatus.complete);
			} else {
				setStatus(TransferStatus.cancelled);
			}
		} catch (IOException ex) {
			setStatus(TransferStatus.error);
			exception = ex;
			throw ex;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public long getBytesSent() {
		if (outstream == null) {
			return 0;
		}
		long writtenLength = outstream.getWrittenLength();
		return writtenLength;
	}

	public boolean isDone() {
		return status == TransferStatus.cancelled || status == TransferStatus.error || status == TransferStatus.complete;
	}

	public void cancel() {
		this.cancle = true;
		if (this.outstream == null) {
			return;
		}
		try {
			this.outstream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setStatus(TransferStatus status) {
		this.status = status;
	}

	public TransferStatus getStatus() {
		return status;
	}

	public Throwable getException() {
		return exception;
	}

}
