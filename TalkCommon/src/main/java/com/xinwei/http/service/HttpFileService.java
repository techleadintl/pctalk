package com.xinwei.http.service;

import java.io.File;
import java.util.Map;

import org.apache.http.entity.mime.content.FileBody;

import com.xinwei.http.listener.DownloadListener;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.talk.model.VCard;

public interface HttpFileService extends IFileServiceHelper {
	/** 获得文件输入流 **/
	public byte[] downloadFile(String url, DownloadListener listener) throws Exception;

	/** 獲取文件字节數組 **/
	public byte[] downloadImage(String url) throws Exception;

	/** 獲取文件字节數組 **/
	public byte[] download(String url) throws Exception;

	/** 删除文件 **/
	public HttpResponseInfo deleteFile(String groupName, String remoteFileName) throws Exception;

	/** 通讯录文件上传 **/
	public HttpResponseInfo uploadContactFile(File file) throws Exception;

	/** 头像上传 **/
	public HttpResponseInfo uploadAvatarFile(String fileFullName, byte[] icon, VCard vCard) throws Exception;

	/** 聊天文件上传 **/
	public HttpResponseInfo uploadChatFile(Object obj, File file) throws Exception;

	/** 聊天文件上传 **/
	public HttpResponseInfo uploadChatFile(Object obj, FileBody fileBody) throws Exception;

	/** 聊天文件上传 **/
	public HttpResponseInfo uploadChatFile(Object obj, byte[] bytes, String fileFullName) throws Exception;

	public void setAttachment(HttpResponseInfo fileResponseInfo, Map<Object, Object> attachment);

}
