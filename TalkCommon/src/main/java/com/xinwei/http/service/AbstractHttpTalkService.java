package com.xinwei.http.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.Gson;
import com.xinwei.common.lang.GsonUtil;
import com.xinwei.common.lang.HttpUtils;
import com.xinwei.http.model.HttpRequestInfo;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.talk.common.Log;

public abstract class AbstractHttpTalkService extends VCardHelper implements ITalkServiceHelper {

	public AbstractHttpTalkService() {
	}

	protected <T> T postWithValue(String url, Class<T> clazz, Object valueParam) throws Exception {
		Gson gson = new Gson();

		String param = null;

		if (valueParam instanceof String) {
			param = (String) valueParam;
		} else {
			param = gson.toJson(valueParam);
		}

		String responseStr = HttpUtils.post(url, param);

		T response = gson.fromJson(responseStr, clazz);

		return response;
	}

	protected <T> T postWithNameValue(String url, Class<T> clazz, Object[]... nameValueParams) throws Exception {
		Map<String, Object> paramMap = new HashMap<String, Object>();

		for (Object[] entry : nameValueParams) {
			paramMap.put(String.valueOf(entry[0]), entry[1]);
		}

		return postWithNameValue(url, clazz, paramMap);
	}

	protected <T> T postWithNameValue(String url, Class<T> clazz, Map<String, Object> nameValueParamMap) throws Exception {
		Map<String, String> params = null;

		if (nameValueParamMap.size() > 0) {
			params = new LinkedHashMap<String, String>();

			Set<Entry<String, Object>> entrySet = nameValueParamMap.entrySet();
			for (Entry<String, Object> entry : entrySet) {
				String param = null;
				String key = entry.getKey();
				Object value = entry.getValue();
				if (value instanceof String) {
					param = (String) value;
				} else {
					param = GsonUtil.toJson(value);
				}
				params.put(key, param);
			}
		}

		String responseStr = HttpUtils.post(url, params);

		T responseInfo = GsonUtil.fromJson(responseStr, clazz);

		if (responseInfo instanceof HttpResponseInfo) {
			handleResponse((HttpResponseInfo) responseInfo, url, params);
		}

		return responseInfo;
	}

	/**
	 * 
	 * @param url
	 * @param otherParams
	 *            [[key1,value1],[key2,value2],[key3,value3],...]]
	 * @return
	 * @throws Exception
	 */
	protected HttpResponseInfo postRequestInfo(String url, Object[]... otherParams) throws Exception {
		return postRequestInfo(url, HttpResponseInfo.class, otherParams);
	}

	protected <T> T postRequestInfo(String url, Class<T> clazz, Object[]... otherParams) throws Exception {
		LinkedHashMap<String, Object> requestParam = new LinkedHashMap<String, Object>();

		if (otherParams.length > 0) {
			for (Object[] entry : otherParams) {
				requestParam.put(String.valueOf(entry[0]), entry[1]);
			}
		}
		return postRequestInfo(url, clazz, requestParam);
	}

	protected HttpResponseInfo postRequestInfo(String url, LinkedHashMap<String, Object> otherParams) throws Exception {
		return postRequestInfo(url, HttpResponseInfo.class, otherParams);
	}

	protected <T> T postRequestInfo(String url, Class<T> clazz, LinkedHashMap<String, Object> otherParams) throws Exception {
		Map<String, String> params = null;

		if (otherParams != null && !otherParams.isEmpty()) {
			params = new HashMap<String, String>();
			HttpRequestInfo requestInfo = new HttpRequestInfo();

			Set<Entry<String, Object>> entrySet = otherParams.entrySet();
			for (Entry<String, Object> entry : entrySet) {
				String key = entry.getKey();
				Object value = entry.getValue();
				addRequestParam(requestInfo, key, value);
			}

			params.put("requestInfo", GsonUtil.toJson(requestInfo));
		}

		String responseStr = HttpUtils.post(url, params);

		Log.info("request:" + url + "---params:" + params + "---response:" + responseStr);

		T responseInfo = GsonUtil.fromJson(responseStr, clazz);

		if (responseInfo instanceof HttpResponseInfo) {
			handleResponse((HttpResponseInfo) responseInfo, url, params);
		}

		return responseInfo;
	}

	public String get(String url) throws Exception {
		return HttpUtils.get(url);
	}

	protected HttpResponseInfo deleteFile(String url, String[]... otherParams) throws Exception {
		Gson gson = new Gson();

		Map<String, String> params = new HashMap<String, String>();

		HttpRequestInfo fileRequestInfo = new HttpRequestInfo();

		for (String[] entry : otherParams) {
			addRequestParam(fileRequestInfo, entry[0], entry[1]);
		}

		params.put("requestInfo", gson.toJson(fileRequestInfo));

		String responseStr = HttpUtils.post(url, params);

		HttpResponseInfo fileResponseInfo = gson.fromJson(responseStr, HttpResponseInfo.class);

		handleResponse(fileResponseInfo, url, params);

		return fileResponseInfo;
	}
}
