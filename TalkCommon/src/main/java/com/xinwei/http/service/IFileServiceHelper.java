package com.xinwei.http.service;

import com.xinwei.http.model.HttpRequestInfo;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.talk.model.VCard;

public interface IFileServiceHelper {
	public String getFileUrl(HttpResponseInfo responseInfo);

	public HttpRequestInfo getHttpFileChatRequestInfo(String fileName, String fileType, Object obj);

	public String getChatFileServerUrl();

	public String getTransferFileServerUrl();

	public HttpRequestInfo getHttpAvatarRequestInfo(String fileName, String fileType, VCard vCard);

	public String getAvatarFileServerUrl();

	public ITalkServiceHelper getTalkServiceHelper();

}
