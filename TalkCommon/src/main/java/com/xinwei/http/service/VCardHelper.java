package com.xinwei.http.service;

import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.TalkManager;

public class VCardHelper {
	public String getVCardUid() {
		return TalkManager.getVCard().getUid();
	}

	public String getVCardTel() {
		return TalkManager.getVCard().getTel();
	}

	public String getVCardJid() {
		return TalkManager.getVCard().getJid();
	}

	public String getJid(String tel) {
		return TalkUtil.getUserFullJid(tel);
	}

	public String getServer() {
		return TalkManager.getVCard().getAppServer();
	}

	public String getFileServer() {
		return TalkManager.getVCard().getFileServer();
	}
}
