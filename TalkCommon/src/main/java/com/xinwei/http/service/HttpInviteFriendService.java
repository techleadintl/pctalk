package com.xinwei.http.service;

import java.util.List;
import java.util.Map;

import com.xinwei.http.model.HttpResponseInfo;

public interface HttpInviteFriendService {
	public HttpResponseInfo inviteFriends(String inviterTel, List<Map<String,String>>  list) throws Exception;
}
