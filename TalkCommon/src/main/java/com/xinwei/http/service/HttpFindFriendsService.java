package com.xinwei.http.service;

import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.talk.dto.FindFriendsDTO;

public interface HttpFindFriendsService {
	public HttpResponseInfo querryFindFriendParams(FindFriendsDTO ffDTO) throws Exception;
}
