package com.xinwei.http.service;

import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface HttpTalkService {
	public Object[] getTalkList() throws Exception;

	public List<Talk> getTalkList(Collection<String> phones) throws Exception;

	public int updateTalk(Talk talk, LinkedHashMap<String, Object> attMap) throws Exception;

	public void addTalk(Talk talk, String display) throws Exception;

	public int deleteTalk(Talk talk, LinkedHashMap<String, Object> attMap) throws Exception;

	public void addTalk(Map<Talk, String> talkMap) throws Exception;

	public Talk getTalk(String phone) throws Exception;

	public Talk getTalkByUid(String uid) throws Exception;

	/**
	 * 查询群组
	 */
	public List<Group> getGroupList() throws Exception;

	/**
	 * 查询单个组信息
	 * 
	 * @param groupId
	 * @return
	 * @throws Exception
	 */
	public Group getGroup(String groupId) throws Exception;

	public Map<Group, List<Member>> getGroupMemberList(Collection<Group> xwGroupList) throws Exception;

	/**
	 * 查询组内指定成员信息
	 */
	public List<Member> getGroupMemberList(String groupId, List<String> memJidList) throws Exception;

	/**
	 * 创建群组信息
	 * 
	 * @param group
	 * @return 组ID
	 * @throws Exception
	 */

	public String createGroup(Group group, List<Member> groupMemberList) throws Exception;

	/**
	 * 解散群组
	 * 
	 * @param subsId
	 *            群创建者UID
	 * @param groupId
	 *            群组ID
	 */
	public int deleteGroup(String subsId, String groupId) throws Exception;

	/**
	 * 退出群组
	 * 
	 * @param subsId
	 *            群创建者UID
	 * @param groupId
	 *            群组ID
	 */
	public int quitMember(String subsId, String groupId) throws Exception;

	/**
	 * 删除组成员
	 * 
	 * @param subsId
	 *            群创建者UID
	 * @param groupId
	 *            群组ID
	 */
	public int deleteMembers(String subsId, String groupId, List<String> uids) throws Exception;

	public int deleteMember(String subsId, String groupId, String uid) throws Exception;

	public int addGroupMembers(Group group, List<Member> groupMemberList) throws Exception;

	/** 更新群组信息 **/
	public int updateGroup(Group group, String groupName, String describe) throws Exception;

	/** 更新群组成员信息 **/
	public int updateMember(String groupId, String memberUid, String memberNickName) throws Exception;

	
	public int editName(String name) throws Exception;

}
