package com.xinwei.http.model;

import com.xinwei.common.vo.McWillMap;

public class HttpRequestInfo extends McWillMap<String, Object> {
	public static String DEVICE_ID=String.valueOf(System.currentTimeMillis());
	public HttpRequestInfo() {
		put("deviceType", "PC");
		put("deviceId", DEVICE_ID);
		put("deviceInfo", "");
		put("version", "");
	}
}
