package com.xinwei.http.model;

import java.util.Map;

import com.xinwei.common.vo.McWillMap;

public class HttpResponseInfo extends McWillMap<String, Object> {

	public Map getAttachment() {
		return getMap("$attachment$");
	}

	public void setAttachment(Map attachment) {
		put("$attachment$", attachment);
	}
}
