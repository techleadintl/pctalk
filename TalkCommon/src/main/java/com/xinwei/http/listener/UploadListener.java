package com.xinwei.http.listener;

public interface UploadListener {

	public void write(byte[] b, int length) throws Exception;

	public boolean isClosed();
}
