package com.xinwei.http.listener;

public interface DownloadListener {

	public void read(byte[] b, int length) throws Exception;

	public boolean isClosed();
}
