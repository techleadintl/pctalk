/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月12日 上午11:02:35
 * 
 ***************************************************************/
package com.xinwei;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.common.lookandfeel.McWillLookAndFeel;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.ConfigManager;
import com.xinwei.talk.manager.PluginManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.login.LoginDialog;
import com.xinwei.talk.ui.plugin.buzz.BuzzPlugin;
import com.xinwei.talk.ui.plugin.global.GlobalPlugin;
import com.xinwei.talk.ui.plugin.systray.SysTrayPlugin;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class Startup {
	public static void main(String[] args) {
		new Startup().startup();
	}

	public void startup() {
		try {
			String current = System.getProperty("java.library.path");
			String classPath = System.getProperty("java.class.path");

			// Set UIManager properties for JTree
			System.setProperty("apple.laf.useScreenMenuBar", "true");

			/** Update Library Path **/
			StringBuffer buf = new StringBuffer();
			buf.append(current).append(";");
			buf.append(classPath).append(";");

			//
			// Set default language set by the user.
			addResources();

			setLanguage();

			addI18ns();

			addServices();

			addActionClass();

			addComponentClass();

			addMessageListenersAndMessageFilters();
			
			addCountry(); 
			
			// Loads the LookandFeel

			// removed to stop loading swing components
			loadLookAndFeel();
			loadColorAndImage();

			// Update System Properties
			System.setProperty("java.library.path", buf.toString());

			System.setProperty("sun.java2d.noddraw", "true");
			System.setProperty("file.encoding", "UTF-8");

			// removed to stop loading swing components
			installBaseUIProperties();

			EventQueue.invokeAndWait(new Runnable() {
				public void run() {

					addPlugins();
					System.out.println(" > Cootel-Engine : initiation completed ");
					// invokeLoginDialog();
				}

			});
		} catch (Exception ex) {
			ex.printStackTrace();
			String stackTrace = SystemUtil.getStackTrace(ex);
			File f = new File("x.log");
			FileOutputStream fo = null;
			try {
				fo = new FileOutputStream(f);
				fo.write(stackTrace.getBytes());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				if (fo != null) {
					try {
						fo.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}

	protected void addServices() {

	}

	protected void addComponentClass() {

	}

	protected void addActionClass() {
	}

	protected void addMessageListenersAndMessageFilters() {

	}

	protected void invokeLoginDialog() {
		final LoginDialog dialog = new LoginDialog();
		dialog.invoke(new JFrame());
	}

	protected void addPlugins() {
		final PluginManager pluginManager = PluginManager.getInstance();
		pluginManager.put(new SysTrayPlugin());
		pluginManager.put(new GlobalPlugin());
		pluginManager.put(new BuzzPlugin());

	}

	protected void addResources() {
		Res.addClassLoader(Startup.class.getClassLoader());
		Res.addResource("talk/properties/talk");
	}

	protected void addI18ns() {
		Res.addI18n("talk/i18n/countrycode");

		Res.addI18n("talk/i18n/talk");
	}

	protected void setLanguage() {
		ConfigManager configManager = TalkManager.getConfigManager();
		String language = configManager.getLanguage();
		if (StringUtil.isBlank(language)) {
			language = Res.getProperty("talk.language");
		}

		if (StringUtil.isNotBlank(language)) {
			Locale[] locales = Locale.getAvailableLocales();
			for (Locale locale : locales) {
				if (locale.toString().equals(language)) {
					Locale.setDefault(locale);
					break;
				}
			}
		}

	}

	/**
	 * Sets Spark specific colors
	 */
	public static void installBaseUIProperties() {
		setApplicationFont(new Font("Dialog", Font.PLAIN, 13));
		UIManager.put("ContactItem.border", BorderFactory.createLineBorder(Color.white));
		// UIManager.put("TextField.font", new Font("Dialog", Font.PLAIN, 11));
		// UIManager.put("Label.font", new Font("Dialog", Font.PLAIN, 11));

	}

	public static void setApplicationFont(Font f) {
		UIDefaults defaults = UIManager.getLookAndFeelDefaults();
		synchronized (defaults) {
			for (Object ui_property : defaults.keySet()) {
				if (ui_property.toString().endsWith(".font")) {
					UIManager.put(ui_property, f);
				}
			}
		}
	}

	/**
	 * Handles the Loading of the Look And Feel
	 */
	protected void loadLookAndFeel() {
		try {
			if (SwingUtil.isWindows()) {
				JFrame.setDefaultLookAndFeelDecorated(true);
				JDialog.setDefaultLookAndFeelDecorated(true);
			}
			UIManager.setLookAndFeel(McWillLookAndFeel.class.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected static void loadColorAndImage() {
		// 皮肤背景颜色
		List<Color> colors = new ArrayList<Color>();
		colors.add(new Color(243, 148, 0));
		colors.add(new Color(254, 185, 0));
		colors.add(new Color(255, 228, 1));
		colors.add(new Color(155, 114, 239));
		colors.add(new Color(241, 158, 189));
		colors.add(new Color(238, 87, 67));
		colors.add(new Color(164, 0, 0));
		colors.add(new Color(206, 67, 0));
		colors.add(new Color(137, 190, 55));
		colors.add(new Color(99, 140, 11));
		colors.add(new Color(59, 152, 254));
		// colors.add(new Color(73, 165, 236));
		colors.add(new Color(119, 198, 255));
		UIManager.put("McWill.SkinColor", colors);

		// 皮肤背景图片
		List<String> images = new ArrayList<String>();
		images.add("talk/images/skin/1.jpg");
		images.add("talk/images/skin/2.jpg");
		images.add("talk/images/skin/3.jpg");
		images.add("talk/images/skin/4.jpg");
		images.add("talk/images/skin/5.jpg");
		images.add("talk/images/skin/6.jpg");
		images.add("talk/images/skin/7.jpg");
		images.add("talk/images/skin/8.jpg");
		images.add("talk/images/skin/9.jpg");
		images.add("talk/images/skin/10.jpg");
		images.add("talk/images/skin/11.jpg");
		images.add("talk/images/skin/12.jpg");
		images.add("talk/images/skin/13.jpg");
		images.add("talk/images/skin/14.jpg");
		images.add("talk/images/skin/15.gif");
		UIManager.put("McWill.SkinIcon", images);

		// //聊天信息窗口左侧背景
		// List<String> leftBalloonImages = new ArrayList<String>();
		// leftBalloonImages.add("talk/images/balloon/left/left_green_1.png");
		// leftBalloonImages.add("talk/images/balloon/left/icon_qipao.9.png");
		// leftBalloonImages.add("talk/images/balloon/left/left.9.png");
		// leftBalloonImages.add("talk/images/balloon/left/left_blue_1.png");
		// leftBalloonImages.add("talk/images/balloon/left/left_white_1.png");
		//
		// UIManager.put("McWill.BalloonLeftIcon", leftBalloonImages);
		//
		// //聊天信息窗口右侧背景
		// List<String> rightBalloonImages = new ArrayList<String>();
		// rightBalloonImages.add("talk/images/balloon/right/right_grey_1.png");
		// rightBalloonImages.add("talk/images/balloon/right/right.9.png");
		// rightBalloonImages.add("talk/images/balloon/right/right_blue_1.png");
		//
		// UIManager.put("McWill.BalloonRightIcon", rightBalloonImages);
	}

	protected static void addCountry() {
		Map<String, String> map = new LinkedHashMap<>();
		map.put("countrycode.855", "cambodia_855.png");
		map.put("countrycode.505", "nicaragua_505.png");
		map.put("countrycode.86", "china_86.png");
		map.put("countrycode.255", "tanzania_255.png");
		map.put("countrycode.93", "afghanistan_93.png");
		map.put("countrycode.355", "albania_355.png");
		map.put("countrycode.213", "algeria_213.png");
		map.put("countrycode.376", "andorra_376.png");
		map.put("countrycode.244", "angola_244.png");
		map.put("countrycode.1264", "anguilla_1264.png");
		map.put("countrycode.1268", "antigua_and_barbuda_1268.png");
		map.put("countrycode.54", "argentina_54.png");
		map.put("countrycode.374", "armenia_374.png");
		map.put("countrycode.61", "australia_61.png");
		map.put("countrycode.43", "austria_43.png");
		map.put("countrycode.1242", "bahamas_1242.png");
		map.put("countrycode.973", "bahrain_973.png");
		map.put("countrycode.880", "bangladesh_880.png");
		map.put("countrycode.1246", "barbados_1246.png");
		map.put("countrycode.375", "belarus_375.png");
		map.put("countrycode.32", "belgium_32.png");
		map.put("countrycode.501", "belize_501.png");
		map.put("countrycode.229", "benin_229.png");
		map.put("countrycode.591", "bolivia_591.png");
		map.put("countrycode.267", "botswana_267.png");
		map.put("countrycode.55", "brazil_55.png");
		map.put("countrycode.673", "brunei_673.png");
		map.put("countrycode.359", "bulgaria_359.png");
		map.put("countrycode.226", "burkina_faso_226.png");
		map.put("countrycode.95", "burma_95.png");
		map.put("countrycode.257", "burundi_257.png");
		map.put("countrycode.237", "cameroon_237.png");
		map.put("countrycode.1", "canada_1.png");
		map.put("countrycode.236", "central_african_republic_236.png");
		map.put("countrycode.235", "chad_235.png");
		map.put("countrycode.56", "chile_56.png");
		map.put("countrycode.57", "colombia_57.png");
		map.put("countrycode.242", "congo_242.png");
		map.put("countrycode.682", "cook_is_682.png");
		map.put("countrycode.506", "costa_rica_506.png");
		map.put("countrycode.53", "cuba_53.png");
		map.put("countrycode.357", "cyprus_357.png");
		map.put("countrycode.420", "czech_republic_420.png");
		map.put("countrycode.45", "denmark_45.png");
		map.put("countrycode.253", "djibouti_253.png");
		map.put("countrycode.1890", "dominica_rep_1890.png");
		map.put("countrycode.593", "ecuador_593.png");
		map.put("countrycode.20", "egypt_20.png");
		map.put("countrycode.503", "eisalvador_503.png");
		map.put("countrycode.372", "estonia_372.png");
		map.put("countrycode.251", "ethiopia_251.png");
		map.put("countrycode.679", "fiji_679.png");
		map.put("countrycode.358", "finland_358.png");
		map.put("countrycode.33", "france_33.png");
		map.put("countrycode.594", "french_guiana_594.png");
		map.put("countrycode.689", "french_polynesia_689.png");
		map.put("countrycode.241", "gabon_241.png");
		map.put("countrycode.220", "gambia_220.png");
		map.put("countrycode.995", "georgia_995.png");
		map.put("countrycode.49", "germany_49.png");
		map.put("countrycode.233", "ghana_233.png");
		map.put("countrycode.350", "gibraltar_350.png");
		map.put("countrycode.30", "greece_30.png");
		map.put("countrycode.1809", "grenada_1809.png");
		map.put("countrycode.1671", "guam_1671.png");
		map.put("countrycode.502", "guatemala_502.png");
		map.put("countrycode.224", "guinea_224.png");
		map.put("countrycode.592", "guyana_592.png");
		map.put("countrycode.509", "haiti_509.png");
		map.put("countrycode.504", "honduras_504.png");
		map.put("countrycode.852", "hongkong_852.png");
		map.put("countrycode.36", "hungary_36.png");
		map.put("countrycode.354", "iceland_354.png");
		map.put("countrycode.91", "india_91.png");
		map.put("countrycode.62", "indonesia_62.png");
		map.put("countrycode.98", "iran_98.png");
		map.put("countrycode.964", "iraq_964.png");
		map.put("countrycode.353", "ireland_353.png");
		map.put("countrycode.972", "israel_972.png");
		map.put("countrycode.39", "italy_39.png");
		map.put("countrycode.225", "ivorycoast_225.png");
		map.put("countrycode.1876", "jamaica_1876.png");
		map.put("countrycode.81", "japan_81.png");
		map.put("countrycode.962", "jordan_962.png");
		map.put("countrycode.327", "kazakstan_327.png");
		map.put("countrycode.254", "kenya_254.png");
		map.put("countrycode.82", "korea_82.png");
		map.put("countrycode.965", "kuwait_965.png");
		map.put("countrycode.331", "kyrgyzstan_331.png");
		map.put("countrycode.856", "laos_856.png");
		map.put("countrycode.371", "latvia_371.png");
		map.put("countrycode.961", "lebanon_961.png");
		map.put("countrycode.266", "lesotho_266.png");
		map.put("countrycode.231", "liberia_231.png");
		map.put("countrycode.218", "libya_218.png");
		map.put("countrycode.423", "liechtenstein_423.png");
		map.put("countrycode.370", "lithuania_370.png");
		map.put("countrycode.352", "luxembourg_352.png");
		map.put("countrycode.853", "macao_853.png");
		map.put("countrycode.261", "madagascar_261.png");
		map.put("countrycode.265", "malawi_265.png");
		map.put("countrycode.60", "malaysia_60.png");
		map.put("countrycode.960", "maldives_960.png");
		map.put("countrycode.223", "mali_223.png");
		map.put("countrycode.356", "malta_356.png");
		map.put("countrycode.596", "martinique_596.png");
		map.put("countrycode.230", "mauritius_230.png");
		map.put("countrycode.52", "mexico_52.png");
		map.put("countrycode.377", "monaco_377.png");
		map.put("countrycode.976", "mongolia_976.png");
		map.put("countrycode.212", "morocco_212.png");
		map.put("countrycode.258", "mozambique_258.png");
		map.put("countrycode.264", "namibia_264.png");
		map.put("countrycode.674", "nauru_674.png");
		map.put("countrycode.977", "nepal_977.png");
		map.put("countrycode.599", "netheriands_antilles_599.png");
		map.put("countrycode.31", "netherlands_31.png");
		map.put("countrycode.64", "newzealand_64.png");
		map.put("countrycode.234", "nigeria_234.png");
		map.put("countrycode.227", "niger_227.png");
		map.put("countrycode.850", "north_korea_850.png");
		map.put("countrycode.47", "norway_47.png");
		map.put("countrycode.968", "oman_968.png");
		map.put("countrycode.92", "pakistan_92.png");
		map.put("countrycode.507", "panama_507.png");
		map.put("countrycode.675", "papua_new_cuinea_675.png");
		map.put("countrycode.595", "paraguay_595.png");
		map.put("countrycode.51", "peru_51.png");
		map.put("countrycode.63", "philippines_63.png");
		map.put("countrycode.48", "poland_48.png");
		map.put("countrycode.351", "portugal_351.png");
		map.put("countrycode.1787", "puertorico_1787.png");
		map.put("countrycode.974", "qatar_974.png");
		map.put("countrycode.262", "reunion_262.png");
		map.put("countrycode.40", "romania_40.png");
		map.put("countrycode.7", "russia_7.png");
		map.put("countrycode.1758", "saint_lucia_1758.png");
		map.put("countrycode.685", "samoa_western_685.png");
		map.put("countrycode.378", "san_marino_378.png");
		map.put("countrycode.239", "sao_tome_and_principe_239.png");
		map.put("countrycode.966", "saudi_arabia_966.png");
		map.put("countrycode.221", "senegal_221.png");
		map.put("countrycode.248", "seychelles_248.png");
		map.put("countrycode.232", "sierra_leone_232.png");
		map.put("countrycode.65", "singapore_65.png");
		map.put("countrycode.421", "slovakia_421.png");
		map.put("countrycode.386", "slovenia_386.png");
		map.put("countrycode.677", "solomon_is_677.png");
		map.put("countrycode.252", "somali_252.png");
		map.put("countrycode.27", "south_africa_27.png");
		map.put("countrycode.34", "spain_34.png");
		map.put("countrycode.94", "sri_lanka_94.png");
		map.put("countrycode.249", "sudan_249.png");
		map.put("countrycode.597", "suriname_597.png");
		map.put("countrycode.268", "swaziland_268.png");
		map.put("countrycode.46", "sweden_46.png");
		map.put("countrycode.41", "switzerland_41.png");
		map.put("countrycode.963", "syria_963.png");
		map.put("countrycode.992", "tajikstan_992.png");
		map.put("countrycode.66", "thailand_66.png");
		map.put("countrycode.228", "togo_228.png");
		map.put("countrycode.676", "tonga_676.png");
		map.put("countrycode.1809", "trinidad_and_tobago_1809.png");
		map.put("countrycode.216", "tunisia_216.png");
		map.put("countrycode.90", "turkey_90.png");
		map.put("countrycode.993", "turkmenistan_993.png");
		map.put("countrycode.256", "uganda_256.png");
		map.put("countrycode.380", "ukraine_380.png");
		map.put("countrycode.971", "united_arab_emirates_971.png");
		map.put("countrycode.44", "united_kiongdom_44.png");
		map.put("countrycode.1", "united_states_of_america_1.png");
		map.put("countrycode.598", "uruguay_598.png");
		map.put("countrycode.998", "uzbekistan_998.png");
		map.put("countrycode.58", "venezuela_58.png");
		map.put("countrycode.84", "vietnam_84.png");
		map.put("countrycode.967", "yemen_967.png");
		map.put("countrycode.381", "yugoslavia_381.png");
		map.put("countrycode.243", "zaire_243.png");
		map.put("countrycode.260", "zambia_260.png");
		map.put("countrycode.263", "zimbabwe_263.png");

		TalkManager.getLocalManager().put("countrycode", map);
	}
}
