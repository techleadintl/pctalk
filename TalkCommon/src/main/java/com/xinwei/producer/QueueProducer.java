package com.xinwei.producer;

import java.util.concurrent.SynchronousQueue;

/**
 * Created by root on 7/24/2018.
 */
public class QueueProducer implements Runnable {

    private Object object;

    public Object getObject() {
        return object;
    }

    public static SynchronousQueue syncQ = new SynchronousQueue();

    public SynchronousQueue getSyncQ() {
        return syncQ;
    }

    public QueueProducer(Object object) {
        this.object = object;
    }

    public QueueProducer() {
    }

    public String getResponse(){
        return "testing";
    }

    public void run() {
        System.out.println("======SynchronousQueue producer is calling======");
        try {
            System.out.println("=======METHOD CALL FOR TRYING TO GET DATA(RESPONSE) TO PUT INTO QUEUE ======");
            String value = getResponse();
            System.out.println("=====Put=== " + value);
            syncQ.put(value);
            System.out.println("====SynchronousQueue producer has put the value====");
            System.out.println("===Returned from put====");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
