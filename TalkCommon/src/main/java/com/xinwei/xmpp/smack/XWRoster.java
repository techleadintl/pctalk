package com.xinwei.xmpp.smack;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.Roster;

public class XWRoster extends Roster {
	public XWRoster(final Connection connection) {
		super(connection);

		// Mark the roster as initialized.
		synchronized (XWRoster.this) {
			rosterInitialized = true;
			//			XWRoster.this.notifyAll();//---------//liyong
		}
	}

	public void reload() {
		if (!connection.isAuthenticated()) {
			throw new IllegalStateException("Not logged in to server.");
		}
		if (connection.isAnonymous()) {
			throw new IllegalStateException("Anonymous users can't have a roster.");
		}

		//		connection.sendPacket(new RosterPacket());
	}

}
