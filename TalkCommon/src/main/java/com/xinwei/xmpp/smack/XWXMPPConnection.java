package com.xinwei.xmpp.smack;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.Roster.RosterPacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;

import javax.security.auth.callback.CallbackHandler;

public class XWXMPPConnection extends XMPPConnection {

	public XWXMPPConnection(ConnectionConfiguration config, CallbackHandler callbackHandler) {
		super(config);
		config.setCallbackHandler(callbackHandler);
		// //不自动重连
		// config.setReconnectionAllowed(false);
	}

	public void sendPacket(Packet packet) {
		if (!isConnected()) {
			throw new IllegalStateException("Not connected to server.");
		}
		if (packet == null) {
			throw new NullPointerException("Packet is null.");
		}
		System.out.println("send msg:" + packet.toXML());
		packetWriter.sendPacket(packet);
	}

	@Override
	protected Roster newRosterInstance() {
		return new XWRoster(this);
	}

	@Override
	public ConnectionConfiguration getConfiguration() {
		return super.getConfiguration();
	}

	/**
	 * 去掉Roster监听器，以便换成自己所需
	 * 
	 * Remove the Roster listener and replace it with your own
	 */
	public void addPacketListener(PacketListener packetListener, PacketFilter packetFilter) {
		if (packetListener instanceof RosterPacketListener) {
			return;
		}

		System.out.println("PacketListener Class:" + packetListener.getClass());

		super.addPacketListener(packetListener, packetFilter);
	}
}
