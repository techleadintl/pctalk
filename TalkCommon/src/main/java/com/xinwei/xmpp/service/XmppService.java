/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月4日 下午5:05:53
 * 
 ***************************************************************/
package com.xinwei.xmpp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.FromMatchesFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.packet.MUCInitialPresence;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SyncLock;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;

public class XmppService {
	/**
	 * 查询服务器时间
	 */
	public Date getServerDate() {
		Date d;
		Boolean isXmpp = Res.getBoolean("history.xmpp.date", false);
		if (isXmpp) {
			final Message message = new Message();
			String jid = TalkManager.getSessionManager().getJID();

			message.setThread(StringUtils.randomString(6));

			message.setBody("{\"get-server-time\":\"test-date\"}");

			// 封装消息的类型、发送方、接收方
			message.setType(Message.Type.chat);
			// message.setTo(jid);
			message.setFrom(jid);
			String packetID = message.getPacketID();
			// 发送消息
			try {
				TalkManager.getConnection().sendPacket(message);
			} catch (Exception ex) {
				Log.error("Error sending message", ex);
				return new Date();
			}

			SyncLock lock = new SyncLock(packetID);
			lock.lock(30, TimeUnit.SECONDS);
			d = lock.getContent();
			if (d == null) {
				d = new Date();
			}
		} else {
			d = new Date(new Date().getTime());
		}
		return d;
	}

	// Do NOT send presence if disconnected.
	public void sendOnlinePresence() {
		XMPPConnection connection = TalkManager.getConnection();
		if (!connection.isConnected())
			return;

		Presence presence = new Presence(Presence.Type.available, "online", 0, Presence.Mode.available);
		// Send Presence Packet
		connection.sendPacket(presence);
	}

	public List<String> joinGroup(String groupJid) {
		final List<String> errors = new ArrayList<String>();
		int groupChatCounter = 0;
		while (true) {
			groupChatCounter++;
			String joinName = TalkManager.getVCard().getTel();
			// if (groupChatCounter > 1) {
			// joinName = joinName + groupChatCounter;
			// }
			if (groupChatCounter < 10) {
				try {
					joinGroup(groupJid, joinName, 5000);
					break;
				} catch (XMPPException ex) {
					int code = 0;
					if (ex.getXMPPError() != null) {
						code = ex.getXMPPError().getCode();
					}
					if (code == 0) {
						errors.add("No response from server.");
					} else if (code == 401) {
						errors.add("A Password is required to enter this room.");
					} else if (code == 403) {
						errors.add("You have been banned from this room.");
					} else if (code == 404) {
						errors.add("The room you are trying to enter does not exist.");
					} else if (code == 407) {
						errors.add("You are not a member of this room.\nThis room requires you to be a member to join.");
					} else if (code != 409) {
						break;
					}
				}
			} else {
				break;
			}
		}

		return errors;
	}

	private void joinGroup(String groupJid, String nickname, long timeout) throws XMPPException {
		XMPPConnection connection = TalkManager.getConnection();
		if (!connection.isConnected())
			return;
		if (StringUtil.isEmpty(nickname)) {
			throw new IllegalArgumentException("Nickname must not be null or blank.");
		}
		// We join a room by sending a presence packet where the "to"
		// field is in the form "roomName@service/nickname"
		Presence joinPresence = new Presence(Presence.Type.available);
		joinPresence.setTo(groupJid + "/" + nickname);

		// Indicate the the client supports MUC
		MUCInitialPresence mucInitialPresence = new MUCInitialPresence();
		joinPresence.addExtension(mucInitialPresence);
		joinPresence.setFrom(connection.getUser());
		// Wait for a presence packet back from the server.
		PacketFilter responseFilter = new AndFilter(new FromMatchesFilter(groupJid + "/" + nickname), new PacketTypeFilter(Presence.class));
		PacketCollector response = null;
		Presence presence;
		try {
			response = connection.createPacketCollector(responseFilter);
			// Send join packet.
			connection.sendPacket(joinPresence);
			// Wait up to a certain number of seconds for a reply.
			presence = (Presence) response.nextResult(timeout);
		} finally {
			// Stop queuing results
			if (response != null) {
				response.cancel();
			}
		}

		if (presence == null) {
			throw new XMPPException("No response from server.");
		} else if (presence.getError() != null) {
			throw new XMPPException(presence.getError());
		}
	}
}
