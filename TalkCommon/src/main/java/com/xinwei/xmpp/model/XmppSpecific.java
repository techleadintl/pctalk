package com.xinwei.xmpp.model;

import java.util.Map;

import com.xinwei.common.vo.McWillMap;

public class XmppSpecific extends McWillMap<String, Object> {
	public XmppSpecific() {
	}

	public XmppSpecific(Map map) {
		if (map == null) {
			return;
		}
		putAll(map);
	}

	public String getType() {
		return (String) get("type");
	}

	public void setType(String type) {
		put("type", type);
	}
}
