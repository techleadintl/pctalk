/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月12日 下午4:00:23
 * 
 ***************************************************************/
package com.xinwei.common.lang;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Label;
import java.awt.MediaTracker;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.awt.image.RenderedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.JTextComponent;
import javax.swing.text.View;

import com.xinwei.common.lookandfeel.component.McWillFileDialog;
import com.xinwei.common.lookandfeel.file.filter.McWillFileFilter;
import com.xinwei.common.lookandfeel.listener.HandCursorListener;
import com.xinwei.common.lookandfeel.listener.McWillTextFocusListener;
import com.xinwei.common.lookandfeel.util.Constants;

public class SwingUtil {
	// private static ImageIcon documentInfoImageIcon;

	private static final Insets HIGHLIGHT_INSETS = new Insets(1, 1, 1, 1);
	public static final Color SELECTION_COLOR = new java.awt.Color(166, 202, 240);
	public static final Color TOOLTIP_COLOR = new java.awt.Color(166, 202, 240);

	protected final static Component component = new Component() {
		private static final long serialVersionUID = -7556405112141454291L;
	};
	protected final static MediaTracker tracker = new MediaTracker(component);

	private static Map<String, Image> imageCache = new HashMap<String, Image>();

	/**
	 * The default Hand cursor.
	 */
	public static final Cursor HAND_CURSOR = new Cursor(Cursor.HAND_CURSOR);

	/**
	 * The default Text Cursor.
	 */
	public static final Cursor DEFAULT_CURSOR = new Cursor(Cursor.DEFAULT_CURSOR);

	private static HandCursorListener handCursorListener = new HandCursorListener();
	private static AffineTransform atf = new AffineTransform();

	private static FontRenderContext frc = new FontRenderContext(atf, true, true);

	private static String[] fontNames;

	static {
		// 获取系统中可用的字体的名字
		GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
		fontNames = e.getAvailableFontFamilyNames();
		Arrays.sort(fontNames, 0, fontNames.length, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return 0 - o1.compareTo(o2);
			}
		});
	}

	public static boolean contains(Point p, Rectangle rec) {
		if (p == null || rec == null)
			return false;
		return contains(p.x, p.y, rec.x, rec.y, rec.width, rec.height);
	}

	public static boolean contains(int X, int Y, int x, int y, int w, int h) {
		if ((w | h) < 0) {
			// At least one of the dimensions is negative...
			return false;
		}
		// Note: if either dimension is zero, tests below must return false...
		if (X < x || Y < y) {
			return false;
		}
		w += x;
		h += y;
		// overflow || intersect
		return ((w < x || w > X) && (h < y || h > Y));
	}

	public static String[] getFontNameArray() {
		return fontNames;
	}

	/**
	 * Returns maximum insets combined from the specified ones.
	 *
	 * @param insets1
	 *            first insets
	 * @param insets2
	 *            second insets
	 * @return maximum insets
	 */
	public static Insets max(final Insets insets1, final Insets insets2) {
		if (insets1 != null && insets2 != null) {
			return new Insets(Math.max(insets1.top, insets2.top), Math.max(insets1.left, insets2.left), Math.max(insets1.bottom, insets2.bottom), Math.max(insets1.right, insets2.right));
		} else if (insets1 != null) {
			return insets1;
		} else if (insets2 != null) {
			return insets2;
		} else {
			return null;
		}
	}

	/**
	 * Returns minimum insets combined from the specified ones.
	 *
	 * @param insets1
	 *            first insets
	 * @param insets2
	 *            second insets
	 * @return minimum insets
	 */
	public static Insets min(final Insets insets1, final Insets insets2) {
		if (insets1 != null && insets2 != null) {
			return new Insets(Math.min(insets1.top, insets2.top), Math.min(insets1.left, insets2.left), Math.min(insets1.bottom, insets2.bottom), Math.min(insets1.right, insets2.right));
		} else if (insets1 != null) {
			return insets1;
		} else if (insets2 != null) {
			return insets2;
		} else {
			return null;
		}
	}

	/**
	 * Returns maximum dimension combined from specified components dimensions.
	 *
	 * @param component1
	 *            first component
	 * @param component2
	 *            second component
	 * @return maximum dimension
	 */
	public static Dimension max(final Component component1, final Component component2) {
		return max(component1.getPreferredSize(), component2.getPreferredSize());
	}

	/**
	 * Returns maximum dimension combined from specified components dimensions.
	 *
	 * @param components
	 *            components
	 * @return maximum dimension
	 */
	public static Dimension max(final Component... components) {
		Dimension max = components.length > 0 ? components[0].getPreferredSize() : new Dimension(0, 0);
		for (int i = 1; i < components.length; i++) {
			max = max(max, components[i].getPreferredSize());
		}
		return max;
	}

	/**
	 * Returns maximum dimension combined from specified ones.
	 *
	 * @param dimension1
	 *            first dimension
	 * @param dimension2
	 *            second dimension
	 * @return maximum dimension
	 */
	public static Dimension max(final Dimension dimension1, final Dimension dimension2) {
		if (dimension1 == null && dimension2 == null) {
			return null;
		} else if (dimension1 == null) {
			return dimension2;
		} else if (dimension2 == null) {
			return dimension1;
		} else {
			return new Dimension(Math.max(dimension1.width, dimension2.width), Math.max(dimension1.height, dimension2.height));
		}
	}

	/**
	 * Returns minimum dimension combined from specified components dimensions.
	 *
	 * @param component1
	 *            first component
	 * @param component2
	 *            second component
	 * @return minimum dimension
	 */
	public static Dimension min(final Component component1, final Component component2) {
		return min(component1.getPreferredSize(), component2.getPreferredSize());
	}

	/**
	 * Returns minimum dimension combined from specified ones.
	 *
	 * @param dimension1
	 *            first dimension
	 * @param dimension2
	 *            second dimension
	 * @return minimum dimension
	 */
	public static Dimension min(final Dimension dimension1, final Dimension dimension2) {
		if (dimension1 == null || dimension2 == null) {
			return null;
		} else {
			return new Dimension(Math.min(dimension1.width, dimension2.width), Math.min(dimension1.height, dimension2.height));
		}
	}

	/**
	 * Returns maximum component width.
	 *
	 * @param components
	 *            components to process
	 * @return maximum component width
	 */
	public static int maxWidth(final Component... components) {
		int max = 0;
		for (final Component component : components) {
			max = Math.max(max, component.getPreferredSize().width);
		}
		return max;
	}

	/**
	 * Returns maximum component height.
	 *
	 * @param components
	 *            components to process
	 * @return maximum component height
	 */
	public static int maxHeight(final Component... components) {
		int max = 0;
		for (final Component component : components) {
			max = Math.max(max, component.getPreferredSize().height);
		}
		return max;
	}

	/**
	 * Launches a file browser or opens a file with java Desktop.open() if is
	 * supported
	 * 
	 * @param file
	 */
	public static String openFile(File file) {
		if (!Desktop.isDesktopSupported())
			return null;
		Desktop dt = Desktop.getDesktop();
		try {
			dt.open(file);
		} catch (Exception ex) {
			return openFile(file.getPath());
		}
		return null;
	}

	/**
	 * Launches a file browser or opens a file with java Desktop.open() if is
	 * supported
	 * 
	 * @param filePath
	 */
	public static String openFile(String filePath) {
		if (filePath == null || filePath.trim().length() == 0)
			return null;
		if (!Desktop.isDesktopSupported())
			return null;
		Desktop dt = Desktop.getDesktop();
		try {
			dt.browse(getFileURI(filePath));
		} catch (Exception ex) {
			return ex.getMessage();
		}
		return null;
	}

	/**
	 * Return correct URI for filePath. dont mind of local or remote path
	 * 
	 * @param filePath
	 * @return
	 */
	private static URI getFileURI(String filePath) {
		URI uri = null;
		filePath = filePath.trim();
		if (filePath.indexOf("http") == 0 || filePath.indexOf("\\") == 0) {
			if (filePath.indexOf("\\") == 0)
				filePath = "file:" + filePath;
			try {
				filePath = filePath.replaceAll(" ", "%20");
				URL url = new URL(filePath);
				uri = url.toURI();
			} catch (MalformedURLException ex) {
				ex.printStackTrace();
			} catch (URISyntaxException ex) {
				ex.printStackTrace();
			}
		} else {
			File file = new File(filePath);
			uri = file.toURI();
		}
		return uri;
	}

	public static ImageIcon getDocumentInfoImageIcon() {
		return new ImageIcon(SwingUtil.class.getResource("document-info.png"));
	}

	public static Icon getFileImageIcon(final File file) {
		if (!file.exists()) {
			return getDocumentInfoImageIcon();
		}
		Icon imageIcon = null;
		if (isImage(file.getName())) {
			try {
				imageIcon = XLookAndFeel.getImageIcon(IOUtil.readBytes(file), 32, 32);
			} catch (Exception e) {
				Log4j.error("Could not locate image.", e);
			}
		} else {
			imageIcon = getIcon(file);
		}
		if (imageIcon == null) {
			imageIcon = getDocumentInfoImageIcon();
		}
		return imageIcon;
	}

	public static ImageIcon getFileImageIcon(final String fileName) {
		File tempFile = new File(SystemUtil.USER_HOME, "AppData/Local/CamTalk/tmp");
		File file = null;
		try {
			tempFile.mkdirs();
			file = new File(tempFile, fileName);
			file.delete();
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			out.write("a");
			out.close();
			return getIcon(file);
		} catch (IOException e) {
			return getDocumentInfoImageIcon();
		} finally {
			// Delete temp file when program exits.
			try {
				file.delete();
			} catch (Exception e) {
				return getDocumentInfoImageIcon();
			}
		}
	}

	public static boolean isImage(String fileName) {
		fileName = fileName.toLowerCase();

		String[] imageTypes = { "jpeg", "gif", "jpg", "png" };
		for (String imageType : imageTypes) {
			if (fileName.endsWith(imageType)) {
				return true;
			}
		}

		return false;
	}

	public static Rectangle2D getStringBounds(String str, Font font) {
		if (str == null || str.isEmpty() || font == null) {
			return null;
		}
		return font.getStringBounds(str, frc);
	}

	public static int getStringHeight(String str, Font font) {
		if (str == null || str.isEmpty() || font == null) {
			return 0;
		}
		return (int) font.getStringBounds(str, frc).getWidth();

	}

	public static int getStringWidth(String str, Font font) {
		if (str == null || str.isEmpty() || font == null) {
			return 0;
		}
		return (int) font.getStringBounds(str, frc).getWidth();
	}

	public static String getString(String str, Font font, int maxWidth, String substr) {
		if (str == null || str.isEmpty() || font == null || maxWidth < 1) {
			return str;
		}
		int width = (int) font.getStringBounds(str + substr, frc).getWidth();
		boolean flag = false;
		while (width > maxWidth) {
			str = str.substring(0, str.length() - 1);
			width = (int) font.getStringBounds(str + substr, frc).getWidth();
			flag = true;
		}
		if (flag) {
			str += "...";
		}
		return str + substr;
	}

	public static String format(long bytes) {
		ByteFormat format = new ByteFormat();
		return format.format(bytes);
	}

	/**
	 * Calculates the speed when given a timedifference and bytedifference
	 * 
	 * @param bytediff
	 *            in bytes
	 * @param timediff
	 *            in milliseconds
	 * @return <b>HistoryTranscriptWinow,x kB/s</b> or
	 *         <b>HistoryTranscriptWinow,x MB/s</b>
	 */
	public static String calculateSpeed(long bytediff, long timediff) {
		double kB = calculateSpeedLong(bytediff, timediff);

		if (bytediff == 0 && timediff == 0) {
			return "";
		}
		if (kB < 1024) {
			String KB = Double.toString(kB);
			// Convert 3.1415926535897932384626433832795 to 3.1
			KB = splitAtDot(KB, 1);

			return KB + "kB/s";
		} else {
			String MB = Double.toString((kB / 1024.0));
			// Convert 3.1415926535897932384626433832795 to 3.1
			MB = splitAtDot(MB, 1);

			return MB + "MB/s";
		}

	}

	/**
	 * Calculates the speed and returns a long in kB/s
	 * 
	 * @param bytediff
	 * @param timediff
	 * @return kB/s
	 */
	public static double calculateSpeedLong(long bytediff, long timediff) {
		timediff = timediff == 0 ? 1 : timediff;
		double kB = ((bytediff / timediff) * 1000.0) / 1024.0;
		return kB;
	}

	/**
	 * Calculate the estimated time of arrival
	 * 
	 * @param currentsize
	 *            in byte
	 * @param totalsize
	 *            in byte
	 * @param timestart
	 *            in milliseconds
	 * @param timenow
	 *            in milliseconds
	 * @return time in (HH:MM:SS)
	 */
	public static String calculateEstimate(long currentsize, long totalsize, long timestart, long timenow) {
		long timediff = timenow - timestart;
		long sizeleft = totalsize - currentsize;

		// currentsize = timediff
		// sizeleft = x
		currentsize = currentsize == 0 ? 1L : currentsize;
		long x = sizeleft * timediff / currentsize;

		// Make it seconds
		x = x / 1000;

		return convertSecondstoHHMMSS(Math.round(x));
	}

	public static String calculateLeftTime(long sizeleft, long bytediff, long timediff) {
		long x = 0;
		if (bytediff != 0) {
			x = sizeleft * timediff / bytediff;
		}
		// Make it seconds
		x = x / 1000;

		return convertSecondstoHHMMSS(Math.round(x));
	}

	/**
	 * Converts given Seconds to HH:MM:SS
	 * 
	 * @param second
	 *            in seconds
	 * @return (HH:MM:SS)
	 */
	public static String convertSecondstoHHMMSS(int second) {

		int hours = Math.round(second / 3600);
		int minutes = Math.round((second / 60) % 60);
		int seconds = Math.round(second % 60);
		String hh = hours < 10 ? "0" + hours : "" + hours;
		String mm = minutes < 10 ? "0" + minutes : "" + minutes;
		String ss = seconds < 10 ? "0" + seconds : "" + seconds;

		return "(" + hh + ":" + mm + ":" + ss + ")";

	}

	/**
	 * Converts a given Byte into KB or MB or GB if applicable
	 * 
	 * @param bytes
	 * @return "12 KB" or "27 MB" etc
	 */
	public static String getAppropriateByteWithSuffix(long bytes) {
		if (bytes >= 1099511627776L) {
			String x = splitAtDot("" + (bytes / 1099511627776L), 2);
			return x + " TB";
		} else if (bytes >= 1073741824) {
			String x = splitAtDot("" + (bytes / 1073741824L), 2);
			return x + " GB";
		} else if (bytes >= 1048576) {
			String x = splitAtDot("" + (bytes / 1048576L), 2);
			return x + " MB";
		} else if (bytes >= 1024) {
			String x = splitAtDot("" + (bytes / 1024L), 2);
			return x + " KB";
		} else
			return bytes + " B";
	}

	/**
	 * shorten a double or long with sig.digits<br>
	 * splitAtDot("3.123",2) -> "3.12"<br>
	 * does not round!
	 * 
	 * @param string
	 * @param significantdigits
	 * @return
	 */
	private static String splitAtDot(String string, int significantdigits) {
		if (string.contains(".")) {
			// no idea why but string.split doesnt like "."
			String s = string.replace(".", "T").split("T")[1];

			if (s.length() >= significantdigits) {
				return string.substring(0, string.indexOf(".") + 1 + significantdigits);
			} else {
				return string.substring(0, string.indexOf(".") + 1 + s.length());
			}
		} else
			return string;

	}

	/**
	 * 将形如“#FFFFFF”的颜色转换成Color
	 * 
	 * @param hex
	 * @return
	 */
	public static Color getColorFromHex(String hex) {
		if (hex == null || hex.length() != 7) {
			try {
				throw new Exception("不能转换这种类型的颜色");
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		int r = Integer.valueOf(hex.substring(1, 3), 16);
		int g = Integer.valueOf(hex.substring(3, 5), 16);
		int b = Integer.valueOf(hex.substring(5), 16);
		return new Color(r, g, b);
	}

	/**
	 * Sets the resources on a {@link JLabel}. It sets the text, mnemonic, and
	 * labelFor property.
	 *
	 * @param label
	 *            The Label on which to set the properties
	 * @param labelFor
	 *            the {@link Component} to set with the <code>labelFor</code>
	 *            property on the <code>label</code>.
	 * @param labelText
	 *            The text label to set on the <code>label</code>
	 * @see JLabel#setText(String)
	 * @see JLabel#setLabelFor(Component)
	 * @see JLabel#setDisplayedMnemonic(int)
	 */
	public static void resLabel(JLabel label, Component labelFor, String labelText) {
		label.setText(stripMnemonic(labelText));

		if (isWindows()) {
			label.setDisplayedMnemonic(getMnemonicKeyCode(labelText));
		}
		label.setLabelFor(labelFor);
	}

	/**
	 * Sets the resources on a subclass of {@link AbstractButton}. The common
	 * classes are {@link javax.swing.JRadioButton}, {@link javax.swing.JButton}
	 * , and {@link javax.swing.JCheckBox}
	 * <p/>
	 * This method sets the text and mnemonic.
	 *
	 * @param button
	 *            The button on which to set the text and mnemonoic
	 * @param labelText
	 *            the text which contains the displayed text and mnemonic
	 * @see AbstractButton#setText(String)
	 * @see AbstractButton#setMnemonic(int)
	 */
	public static void resButton(AbstractButton button, String labelText) {
		button.setText(stripMnemonic(labelText));

		if (isWindows()) {
			button.setMnemonic(getMnemonicKeyCode(labelText));
		}
	}

	/**
	 * Sets the resources on a subclass of {@link AbstractButton}. The common
	 * classes are {@link javax.swing.JRadioButton}, {@link javax.swing.JButton}
	 * , and {@link javax.swing.JCheckBox}
	 * <p/>
	 * This method sets the text and mnemonic.
	 *
	 * @param button
	 *            The button on which to set the text and mnemonoic
	 * @param labelText
	 *            the text which contains the displayed text and mnemonic
	 * @see AbstractButton#setText(String)
	 * @see AbstractButton#setMnemonic(int)
	 */
	public static void resButton(JButton button, String labelText) {
		button.setText(stripMnemonic(labelText));

		if (isWindows()) {
			button.setMnemonic(getMnemonicKeyCode(labelText));
		}
	}

	public static String stripMnemonic(String label) {
		String text;
		int index = label.indexOf("&");
		if (index != -1) {
			text = label.substring(0, index);
			if (label.length() > index) {
				text = text + label.substring(index + 1);
				return text;
			}
		}
		return label;
	}

	public static int getMnemonicKeyCode(String mnemonic) {
		int mindex = mnemonic.indexOf("&");
		if (mindex > -1) {
			return (int) mnemonic.toUpperCase().charAt(mindex + 1);
		}
		return 0;
	}

	public static void openURL(String url) throws Exception {
		if (url.startsWith("http") || url.startsWith("ftp") || url.startsWith("file") || url.startsWith("www")) {

			if (url.startsWith("file") && url.contains(" ")) {
				url = url.replace(" ", "%20");
			}
			if (url.startsWith("www")) {
				url = "http://" + url;
			}
			Desktop.getDesktop().browse(new URI(url));
		} else {
			File f = new File(url);
			if (f.exists() && Desktop.isDesktopSupported()) {
				try {
					Desktop.getDesktop().open(f);
				} catch (Exception ex) {
					if (!url.toLowerCase().startsWith("//"))
						url = "//" + url;
					Desktop.getDesktop().browse(new URI("http:" + url));
				}
			}
		}
	}

	static final String[] browsers = { "google-chrome", "firefox", "opera", "epiphany", "konqueror", "conkeror", "midori", "kazehakase", "mozilla" };
	static final String errMsg = "Error attempting to launch web browser";

	public static void openBrowseURL(String url) {
		try { // attempt to use Desktop library from JDK 1.6+
			Class<?> d = Class.forName("java.awt.Desktop");
			d.getDeclaredMethod("browse", new Class[] { java.net.URI.class }).invoke(d.getDeclaredMethod("getDesktop").invoke(null), new Object[] { java.net.URI.create(url) });
			// above code mimicks: java.awt.Desktop.getDesktop().browse()
		} catch (Exception ignore) { // library not available or failed
			String osName = System.getProperty("os.name");
			try {
				if (osName.startsWith("Mac OS")) {
					Class.forName("com.apple.eio.FileManager").getDeclaredMethod("openURL", new Class[] { String.class }).invoke(null, new Object[] { url });
				} else if (osName.startsWith("Windows"))
					Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
				else { // assume Unix or Linux
					String browser = null;
					for (String b : browsers)
						if (browser == null && Runtime.getRuntime().exec(new String[] { "which", b }).getInputStream().read() != -1)
							Runtime.getRuntime().exec(new String[] { browser = b, url });
					if (browser == null)
						throw new Exception(Arrays.toString(browsers));
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, errMsg + "\n" + e.toString());
			}
		}
	}

	/**
	 * Returns the String in the system clipboard. If not string is found, null
	 * will be returned.
	 *
	 * @return the contents of the system clipboard. If none found, null is
	 *         returned.
	 */
	public static String getClipboard() {
		Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);

		try {
			if (t != null && t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
				return (String) t.getTransferData(DataFlavor.stringFlavor);
			}
		} catch (Exception e) {
			Log4j.error("Could not retrieve info from clipboard.", e);
		}
		return null;
	}

	/**
	 * Adds a string to the system clipboard.
	 *
	 * @param str
	 *            the string to add the clipboard.
	 */
	public static void setClipboard(String str) {
		StringSelection ss = new StringSelection(str);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
	}

	/**
	 * Highlights words in a string. Words matching ignores case. The actual
	 * higlighting method is specified with the start and end higlight tags.
	 * Those might be beginning and ending HTML bold tags, or anything else.
	 * This method is under the Jive Open Source Software License and was
	 * written by Mark Imbriaco.
	 * 
	 * @param string
	 *            the String to highlight words in.
	 * @param words
	 *            an array of words that should be highlighted in the string.
	 * @param startHighlight
	 *            the tag that should be inserted to start highlighting.
	 * @param endHighlight
	 *            the tag that should be inserted to end highlighting.
	 * @return a new String with the specified words highlighted.
	 */
	public static String highlightWords(String string, String[] words, String startHighlight, String endHighlight) {
		if (string == null || words == null || startHighlight == null || endHighlight == null) {
			return null;
		}

		StringBuffer regexp = new StringBuffer();
		regexp.append("(?i)\\b(");
		// Iterate through each word and generate a word list for the regexp.
		for (int x = 0; x < words.length; x++) {
			// Escape "$", "|", ".", "/", and "?" to keep us out of trouble in
			// our regexp.
			words[x] = words[x].replaceAll("([\\$\\?\\|\\/\\.])", "\\\\$1");
			regexp.append(words[x]);

			if (x != words.length - 1) {
				regexp.append("|");
			}
		}
		regexp.append(")");

		return string.replaceAll(regexp.toString(), startHighlight + "$1" + endHighlight);
	}

	public static boolean isWindows() {
		final String osName = System.getProperty("os.name").toLowerCase();
		return osName.startsWith("windows");
	}

	public static boolean isLinux() {
		final String osName = System.getProperty("os.name").toLowerCase();
		return osName.startsWith("linux");
	}

	public static boolean isMac() {
		String lcOSName = System.getProperty("os.name").toLowerCase();
		return lcOSName.indexOf("mac") != -1;
	}

	public static boolean isVista() {
		final String osName = System.getProperty("os.name").toLowerCase();
		return osName.contains("vista");
	}

	public static void addFocusListener(JTextComponent textField) {
		textField.addFocusListener(new McWillTextFocusListener(textField));
	}

	public static void addHandCurosrListener(Component... components) {
		for (Component component : components) {
			component.addMouseListener(handCursorListener);
		}
	}

	public static void setDefaultText(JTextField textField, String text) {
		textField.setText(text);
		textField.setForeground((Color) UIManager.get("TextField.lightforeground"));
	}

	public static void setDefaultText(JTextArea textField, String text) {
		textField.setText(text);
		textField.setForeground((Color) UIManager.get("TextField.lightforeground"));
	}

	public static JButton newButton(String text) {
		return SwingUtil.newButton(text, 80, 30);
	}

	public static JButton newButton() {
		return newButton(null);
	}

	public static JButton newButton(String text, int w, int h) {
		JButton button = new JButton(text);
		button.setPreferredSize(new Dimension(w, h));
		return button;
	}

	public static JTextField newTextField(String text) {
		JTextField field = new JTextField(text);
		if (text != null) {
			SwingUtil.setOpaqueFalse(field);
			addFocusListener(field);
		}
		return field;
	}

	public static JPasswordField newPasswordField(String text) {
		JPasswordField field = new JPasswordField(text);
		if (text != null) {
			SwingUtil.setOpaqueFalse(field);
			addFocusListener(field);
		}
		return field;
	}

	public static void setOpaqueFalse(JComponent component) {
		component.putClientProperty(Constants.OPAQUE, "");
	}

	public static JLabel setLabelWrap(int width, JLabel label) {
		if (width <= 0 || label == null) {
			return label;
		}
		String text = label.getText();
		if (!text.startsWith("<html>")) {
			StringBuilder strBuilder = new StringBuilder("<html>");
			strBuilder.append(text);
			strBuilder.append("</html>");
			text = strBuilder.toString();
		}
		label.setText(text);
		View labelView = BasicHTML.createHTMLView(label, label.getText());
		labelView.setSize(width, 100);
		label.setPreferredSize(new Dimension(width, (int) labelView.getMinimumSpan(View.Y_AXIS) + 20));
		return label;
	}

	public static String getMaxLengthString(JComponent comp, String longString, int length) {
		if (longString != null && longString.length() > 0) {
			char[] chars = longString.toCharArray();
			FontMetrics fontMetrics = comp.getFontMetrics(comp.getFont());
			if (fontMetrics.charsWidth(chars, 0, chars.length) < length) {
				return longString;
			} else {
				int i = 1;
				int l = 3 * fontMetrics.charWidth('.');
				length -= l;
				while (true) {
					if (fontMetrics.charsWidth(chars, 0, i) > length || i >= chars.length) {
						break;
					}
					i++;
				}
				return longString.substring(0, i) + "...";
			}
		}
		return null;
	}

	/**
	 * Sets the location of the specified window so that it is centered on
	 * screen.
	 * 
	 * @param window
	 *            The window to be centered.
	 */
	public static void centerWindowOnScreen(Window window) {
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		final Dimension size = window.getSize();

		if (size.height > screenSize.height) {
			size.height = screenSize.height;
		}

		if (size.width > screenSize.width) {
			size.width = screenSize.width;
		}

		window.setLocation((screenSize.width - size.width) / 2, (screenSize.height - size.height) / 2);
	}

	public static Rectangle getScreenRectangle(Component component) {
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(component.getGraphicsConfiguration());

		Rectangle rec = new Rectangle(0, 0, screenSize.width, screenSize.height);
		if (insets.top > 0) {
			rec.height -= insets.top;
			rec.y = insets.top;
		} else if (insets.left > 0) {
			rec.width -= insets.left;
			rec.x = insets.left;
		} else if (insets.bottom > 0) {
			rec.height -= insets.bottom;
		} else if (insets.right > 0) {
			rec.width -= insets.right;
		}
		return rec;
	}

	public static Dimension getScreenSize() {
		return Toolkit.getDefaultToolkit().getScreenSize();
	}

	/**
	 * Draws a single-line highlight border rectangle.
	 * 
	 * @param g
	 *            The graphics context to use for drawing.
	 * @param x
	 *            The left edge of the border.
	 * @param y
	 *            The top edge of the border.
	 * @param width
	 *            The width of the border.
	 * @param height
	 *            The height of the border.
	 * @param raised
	 *            <code>true</code> if the border is to be drawn raised,
	 *            <code>false</code> if lowered.
	 * @param shadow
	 *            The shadow color for the border.
	 * @param highlight
	 *            The highlight color for the border.
	 * @see javax.swing.border.EtchedBorder
	 */
	public static void drawHighlightBorder(Graphics g, int x, int y, int width, int height, boolean raised, Color shadow, Color highlight) {
		final Color oldColor = g.getColor();
		g.translate(x, y);

		g.setColor(raised ? highlight : shadow);
		g.drawLine(0, 0, width - 2, 0);
		g.drawLine(0, 1, 0, height - 2);

		g.setColor(raised ? shadow : highlight);
		g.drawLine(width - 1, 0, width - 1, height - 1);
		g.drawLine(0, height - 1, width - 2, height - 1);

		g.translate(-x, -y);
		g.setColor(oldColor);
	}

	/**
	 * Return the amount of space taken up by a highlight border drawn by
	 * <code>drawHighlightBorder()</code>.
	 * 
	 * @return The <code>Insets</code> needed for the highlight border.
	 * @see #drawHighlightBorder
	 */
	public static Insets getHighlightBorderInsets() {
		return HIGHLIGHT_INSETS;
	}

	/**
	 * Creates an ImageIcon from an Image
	 * 
	 * @param image
	 *            {@link Image}
	 * @return {@link ImageIcon}
	 */
	public static ImageIcon createImageIcon(Image image) {
		if (image == null) {
			return null;
		}

		synchronized (tracker) {
			tracker.addImage(image, 0);
			try {
				tracker.waitForID(0, 0);
			} catch (InterruptedException e) {
				Log4j.error("INTERRUPTED while loading Image", e);
			}
			tracker.removeImage(image, 0);
		}

		return new ImageIcon(image);
	}

	/**
	 * Returns a point where the given popup menu should be shown. The point is
	 * calculated by adjusting the X and Y coordinates from the given mouse
	 * event so that the popup menu will not be clipped by the screen
	 * boundaries.
	 * 
	 * @param popup
	 *            the popup menu
	 * @param event
	 *            the mouse event
	 * @return the point where the popup menu should be shown
	 */
	public static Point getPopupMenuShowPoint(JPopupMenu popup, MouseEvent event) {
		Component source = (Component) event.getSource();
		Point topLeftSource = source.getLocationOnScreen();
		Point ptRet = getPopupMenuShowPoint(popup, topLeftSource.x + event.getX(), topLeftSource.y + event.getY());
		ptRet.translate(-topLeftSource.x, -topLeftSource.y);
		return ptRet;
	}

	/**
	 * Returns a point where the given popup menu should be shown. The point is
	 * calculated by adjusting the X and Y coordinates so that the popup menu
	 * will not be clipped by the screen boundaries.
	 * 
	 * @param popup
	 *            the popup menu
	 * @param x
	 *            the x position in screen coordinate
	 * @param y
	 *            the y position in screen coordinates
	 * @return the point where the popup menu should be shown in screen
	 *         coordinates
	 */
	public static Point getPopupMenuShowPoint(JPopupMenu popup, int x, int y) {
		Dimension sizeMenu = popup.getPreferredSize();
		Point bottomRightMenu = new Point(x + sizeMenu.width, y + sizeMenu.height);

		Rectangle[] screensBounds = getScreenBounds();
		int n = screensBounds.length;
		for (int i = 0; i < n; i++) {
			Rectangle screenBounds = screensBounds[i];
			if (screenBounds.x <= x && x <= (screenBounds.x + screenBounds.width)) {
				Dimension sizeScreen = screenBounds.getSize();
				sizeScreen.height -= 32; // Hack to help prevent menu being
				// clipped by Windows/Linux/Solaris
				// Taskbar.

				int xOffset = 0;
				if (bottomRightMenu.x > (screenBounds.x + sizeScreen.width))
					xOffset = -sizeMenu.width;

				int yOffset = 0;
				if (bottomRightMenu.y > (screenBounds.y + sizeScreen.height))
					yOffset = sizeScreen.height - bottomRightMenu.y;

				return new Point(x + xOffset, y + yOffset);
			}
		}

		return new Point(x, y); // ? that would mean that the top left point was
		// not on any screen.
	}

	/**
	 * Centers the window over a component (usually another window). The window
	 * must already have been sized.
	 * 
	 * @param window
	 *            Window to center.
	 * @param over
	 *            Component to center over.
	 */
	public static void centerWindowOnComponent(Window window, Component over) {
		if ((over == null) || !over.isShowing()) {
			centerWindowOnScreen(window);
			return;
		}

		Point parentLocation = over.getLocationOnScreen();
		Dimension parentSize = over.getSize();
		Dimension size = window.getSize();

		// Center it.
		int x = parentLocation.x + (parentSize.width - size.width) / 2;
		int y = parentLocation.y + (parentSize.height - size.height) / 2;

		// Now, make sure it's onscreen
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		// This doesn't actually work on the Mac, where the screen
		// doesn't necessarily start at 0,0
		if (x + size.width > screenSize.width)
			x = screenSize.width - size.width;

		if (x < 0)
			x = 0;

		if (y + size.height > screenSize.height)
			y = screenSize.height - size.height;

		if (y < 0)
			y = 0;

		window.setLocation(x, y);
	}

	/**
	 * @param c
	 *            Component to check on.
	 * @return returns true if the component of one of its child has the focus
	 */
	public static boolean isAncestorOfFocusedComponent(Component c) {
		if (c.hasFocus()) {
			return true;
		} else {
			if (c instanceof Container) {
				Container cont = (Container) c;
				int n = cont.getComponentCount();
				for (int i = 0; i < n; i++) {
					Component child = cont.getComponent(i);
					if (isAncestorOfFocusedComponent(child))
						return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns the first component in the tree of <code>c</code> that can accept
	 * the focus.
	 * 
	 * @param c
	 *            the root of the component hierarchy to search
	 * @see #focusComponentOrChild
	 * @deprecated replaced by
	 *             {@link #getFocusableComponentOrChild(Component,boolean)}
	 * @return Component that was focused on.
	 */
	public static Component getFocusableComponentOrChild(Component c) {
		return getFocusableComponentOrChild(c, false);
	}

	/**
	 * Returns the first component in the tree of <code>c</code> that can accept
	 * the focus.
	 * 
	 * @param c
	 *            the root of the component hierarchy to search
	 * @param deepest
	 *            if <code>deepest</code> is true the method will return the
	 *            first and deepest component that can accept the focus. For
	 *            example, if both a child and its parent are focusable and
	 *            <code>deepest</code> is true, the child is returned.
	 * @see #focusComponentOrChild
	 * @return Component that was focused on.
	 */
	public static Component getFocusableComponentOrChild(Component c, boolean deepest) {
		if (c != null && c.isEnabled() && c.isVisible()) {
			if (c instanceof Container) {
				Container cont = (Container) c;

				if (!deepest) { // first one is a good one
					if (c instanceof JComponent) {
						JComponent jc = (JComponent) c;
						if (jc.isRequestFocusEnabled()) {
							return jc;
						}
					}
				}

				int n = cont.getComponentCount();
				for (int i = 0; i < n; i++) {
					Component child = cont.getComponent(i);
					Component focused = getFocusableComponentOrChild(child, deepest);
					if (focused != null) {
						return focused;
					}
				}

				if (c instanceof JComponent) {
					if (deepest) {
						JComponent jc = (JComponent) c;
						if (jc.isRequestFocusEnabled()) {
							return jc;
						}
					}
				} else {
					return c;
				}
			}
		}

		return null;
	}

	/**
	 * Puts the focus on the first component in the tree of <code>c</code> that
	 * can accept the focus.
	 * 
	 * @see #getFocusableComponentOrChild
	 * @param c
	 *            Component to focus on.
	 * @return Component that was focused on.
	 */
	public static Component focusComponentOrChild(Component c) {
		return focusComponentOrChild(c, false);
	}

	/**
	 * Puts the focus on the first component in the tree of <code>c</code> that
	 * can accept the focus.
	 * 
	 * @param c
	 *            the root of the component hierarchy to search
	 * @param deepest
	 *            if <code>deepest</code> is true the method will focus the
	 *            first and deepest component that can accept the focus. For
	 *            example, if both a child and its parent are focusable and
	 *            <code>deepest</code> is true, the child is focused.
	 * @see #getFocusableComponentOrChild
	 * @return Component that was focused on.
	 */
	public static Component focusComponentOrChild(Component c, boolean deepest) {
		final Component focusable = getFocusableComponentOrChild(c, deepest);
		if (focusable != null) {
			focusable.requestFocus();
		}
		return focusable;
	}

	/**
	 * Loads an {@link Image} named <code>imageName</code> as a resource
	 * relative to the Class <code>cls</code>. If the <code>Image</code> can not
	 * be loaded, then <code>null</code> is returned. Images loaded here will be
	 * added to an internal cache based upon the full {@link URL} to their
	 * location.
	 * <p/>
	 * <em>This method replaces legacy code from JDeveloper 3.x and earlier.</em>
	 * 
	 * @see Class#getResource(String)
	 * @see Toolkit#createImage(URL)
	 * @param imageName
	 *            Name of the resource to load.
	 * @param cls
	 *            Class to pull resource from.
	 * @return Image loaded from resource.
	 */
	public static Image loadFromResource(String imageName, Class<?> cls) {
		try {
			final URL url = cls.getResource(imageName);

			if (url == null) {
				return null;
			}

			Image image = imageCache.get(url.toString());

			if (image == null) {
				image = Toolkit.getDefaultToolkit().createImage(url);
				imageCache.put(url.toString(), image);
			}

			return image;
		} catch (Exception e) {
			Log4j.error(e);
		}

		return null;
	}

	/**
	 * Returns the ScreenBounds
	 * 
	 * @return Array of {@link Rectangle}'s
	 */
	public static Rectangle[] getScreenBounds() {
		GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		final GraphicsDevice[] screenDevices = graphicsEnvironment.getScreenDevices();
		Rectangle[] screenBounds = new Rectangle[screenDevices.length];
		for (int i = 0; i < screenDevices.length; i++) {
			GraphicsDevice screenDevice = screenDevices[i];
			final GraphicsConfiguration defaultConfiguration = screenDevice.getDefaultConfiguration();
			screenBounds[i] = defaultConfiguration.getBounds();
		}

		return screenBounds;
	}

	/**
	 * Makes all Compontens the same Size
	 * 
	 * @param comps
	 */
	public static void makeSameSize(JComponent... comps) {
		if (comps.length == 0) {
			return;
		}

		int max = 0;
		for (JComponent comp1 : comps) {
			int w = comp1.getPreferredSize().width;
			max = w > max ? w : max;
		}

		Dimension dim = new Dimension(max, comps[0].getPreferredSize().height);
		for (JComponent comp : comps) {
			comp.setPreferredSize(dim);
		}
	}

	/**
	 * Return the hexidecimal color from a java.awt.Color
	 * 
	 * @param c
	 *            Color to convert.
	 * @return hexadecimal string
	 */
	public static String toHTMLColor(Color c) {
		int color = c.getRGB();
		color |= 0xff000000;
		String s = Integer.toHexString(color);
		return s.substring(2);
	}

	/**
	 * Creates a Tooltip with specified width
	 * 
	 * @param text
	 *            the Text appearing in the Tooltip
	 * @param width
	 *            the width of the Tooltip
	 * @return HTML-String displaying the Tooltip
	 */
	public static String createToolTip(String text, int width) {
		final String htmlColor = toHTMLColor(TOOLTIP_COLOR);
		return "<html><table width=" + width + " bgColor=" + htmlColor + "><tr><td>" + text + "</td></tr></table></table>";
	}

	/**
	 * Creates a Tooltop
	 * 
	 * @param text
	 *            , the Text appearing in the Tooltip
	 * @return HTML-String displaying the Tooltip
	 */
	public static String createToolTip(String text) {
		final String htmlColor = toHTMLColor(TOOLTIP_COLOR);
		return "<html><table  bgColor=" + htmlColor + "><tr><td>" + text + "</td></tr></table></table>";
	}

	/**
	 * Highlights Words
	 * 
	 * @param text
	 *            , the Text containing Words that should be Highlighted
	 * @param query
	 *            , the Words that should be Highlighted
	 * @return HTML-String, with Highlighted Words
	 */
	public static String getHighlightedWords(String text, String query) {
		final StringTokenizer tkn = new StringTokenizer(query, " ");
		int tokenCount = tkn.countTokens();
		String[] words = new String[tokenCount];
		for (int j = 0; j < tokenCount; j++) {
			String queryWord = tkn.nextToken();
			words[j] = queryWord;
		}

		String highlightedWords;
		try {
			highlightedWords = SwingUtil.highlightWords(text, words, "<font style=background-color:yellow;font-weight:bold;>", "</font>");
		} catch (Exception e) {
			highlightedWords = text;
		}

		return highlightedWords;
	}

	/**
	 * Creates an {@link ImageIcon} with a Shadow
	 * 
	 * @param buf
	 *            , the {@link Image} where a Shadow will be applied
	 * @return {@link ImageIcon} with shadow
	 */
	public static ImageIcon createShadowPicture(Image buf) {
		buf = removeTransparency(buf);

		BufferedImage splash;

		JLabel label = new JLabel();
		int width = buf.getWidth(null);
		int height = buf.getHeight(null);
		int extra = 4;

		splash = new BufferedImage(width + extra, height + extra, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = (Graphics2D) splash.getGraphics();

		BufferedImage shadow = new BufferedImage(width + extra, height + extra, BufferedImage.TYPE_INT_ARGB);
		Graphics g = shadow.getGraphics();
		g.setColor(new Color(0.0f, 0.0f, 0.0f, 0.3f));
		g.fillRoundRect(0, 0, width, height, 12, 12);

		g2.drawImage(shadow, getBlurOp(7), 0, 0);
		g2.drawImage(buf, 0, 0, label);
		return new ImageIcon(splash);
	}

	/**
	 * removes the Transparency of an {@link Image}
	 * 
	 * @param image
	 * @return {@link BufferedImage} without Transparency
	 */
	public static BufferedImage removeTransparency(Image image) {
		int w = image.getWidth(null);
		int h = image.getHeight(null);
		BufferedImage bi2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bi2.createGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, w, h);
		g.drawImage(image, 0, 0, null);
		g.dispose();

		return bi2;
	}

	/**
	 * Converts a {@link BufferedImage} to {@link Image}
	 * 
	 * @param bufferedImage
	 *            , the {@link BufferedImage}
	 * @return {@link Image}
	 */
	public static Image toImage(BufferedImage bufferedImage) {
		return Toolkit.getDefaultToolkit().createImage(bufferedImage.getSource());
	}

	/**
	 * 
	 * @param size
	 * @return
	 */
	private static ConvolveOp getBlurOp(int size) {
		float[] data = new float[size * size];
		float value = 1 / (float) (size * size);
		for (int i = 0; i < data.length; i++) {
			data[i] = value;
		}
		return new ConvolveOp(new Kernel(size, size, data));
	}

	/**
	 * Converting an {@link Image} into a {@link BufferedImage} Transparency
	 * some Images gets lost
	 * 
	 * @param im
	 * @return {@link BufferedImage}
	 * @throws InterruptedException
	 * @throws {@link
	 *             IOException}
	 */
	public static BufferedImage convert(Image im) throws InterruptedException, IOException {
		// load(im);
		BufferedImage bi = new BufferedImage(im.getWidth(null), im.getHeight(null), BufferedImage.TYPE_INT_ARGB_PRE);
		Graphics bg = bi.getGraphics();
		bg.drawImage(im, 0, 0, null);
		bg.dispose();
		return bi;
	}

	/**
	 * Loading an image via a MediaTracker
	 * 
	 * @param image
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static void load(Image image) throws InterruptedException, IOException {
		MediaTracker tracker = new MediaTracker(new Label()); // any component
		// will do
		tracker.addImage(image, 0);
		tracker.waitForID(0);
		if (tracker.isErrorID(0))
			throw new IOException("error loading image");
	}

	/**
	 * Gets the Bytes from an Image using an FileInputStream
	 * 
	 * @param file
	 *            , input {@link File}
	 * @return byte[]
	 */
	public static byte[] getBytesFromFile(File file) {
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fileInputStream.read(data);
			fileInputStream.close();
			return data;
		} catch (IOException e) {
			closeInputStream(fileInputStream);
			return null;
		}

	}

	public static byte[] getBytesFromUrl(URL url) {
		InputStream fis = null;
		ByteArrayOutputStream fos = null;
		try {
			fos = new ByteArrayOutputStream();
			byte[] data = new byte[1024];
			fis = url.openStream();
			int len = -1;
			while ((len = fis.read(data)) != -1) {
				fos.write(data, 0, len);
			}
			return fos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeInputStream(fis);
			closeOutputStream(fos);
		}
		return null;
	}

	private static void closeInputStream(InputStream fileInputStream) {
		if (fileInputStream != null) {
			try {
				fileInputStream.close();
			} catch (IOException e1) {
			}
		}
	}

	private static void closeOutputStream(OutputStream fos) {
		try {
			if (fos != null) {
				fos.close();
			}
		} catch (Exception ex) {
		}
	}

	/**
	 * Returns a scaled down image if the height or width is smaller than the
	 * image size.
	 * 
	 * @param icon
	 *            the image icon.
	 * @param newHeight
	 *            the preferred height.
	 * @param newWidth
	 *            the preferred width.
	 * @return the icon.
	 */
	public static ImageIcon scaleImageIcon(ImageIcon icon, int newHeight, int newWidth) {
		Image img = icon.getImage();
		int height = icon.getIconHeight();
		int width = icon.getIconWidth();

		if (height > newHeight) {
			height = newHeight;
		}

		if (width > newWidth) {
			width = newWidth;
		}
		img = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		return new ImageIcon(img);
	}

	/**
	 * Returns a scaled down image if the height or width is smaller than the
	 * image size.
	 * 
	 * @param icon
	 *            the image icon.
	 * @param newHeight
	 *            the preferred height.
	 * @param newWidth
	 *            the preferred width.
	 * @return the icon.
	 */
	public static ImageIcon scale(ImageIcon icon, int newHeight, int newWidth) {
		Image img = icon.getImage();
		int height = icon.getIconHeight();
		int width = icon.getIconWidth();
		boolean scaleHeight = height * newWidth > width * newHeight;
		if (height > newHeight) {
			// Too tall
			if (width <= newWidth || scaleHeight) {
				// Width is okay or height is limiting factor due to aspect
				// ratio
				height = newHeight;
				width = -1;
			} else {
				// Width is limiting factor due to aspect ratio
				height = -1;
				width = newWidth;
			}
		} else if (width > newWidth) {
			// Too wide and height is okay
			height = -1;
			width = newWidth;
		} else if (scaleHeight) {
			// Height is limiting factor due to aspect ratio
			height = newHeight;
			width = -1;
		} else {
			// Width is limiting factor due to aspect ratio
			height = -1;
			width = newWidth;
		}
		img = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		return new ImageIcon(img);
	}

	/**
	 * Gets the Bytes from an Image using an FileInputStream
	 * 
	 * @param file
	 *            , input {@link File}
	 * @return byte[]
	 */
	public static byte[] getBytesFromImage(File file) {
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fileInputStream.read(data);
			fileInputStream.close();
			return data;
		} catch (IOException e) {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e1) {
				}
			}
			return null;
		}

	}

	/**
	 * Scale an image to fit in a square of the given size, preserving aspect
	 * ratio.
	 *
	 * @param icon
	 * @param newSize
	 * @return
	 */
	public static ImageIcon fitToSquare(ImageIcon icon, int newSize) {
		if (newSize <= 0) {
			return icon;
		}

		final int oldWidth = icon.getIconWidth();
		final int oldHeight = icon.getIconHeight();
		int newWidth;
		int newHeight;

		if (oldHeight >= oldWidth) {
			newWidth = (int) ((float) oldWidth * newSize / oldHeight);
			newHeight = newSize;
		} else {
			newWidth = newSize;
			newHeight = (int) ((float) oldHeight * newSize / oldWidth);
		}

		final Image img = icon.getImage().getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);

		return new ImageIcon(img);
	}

	public static void saveImageFile(final Window window, RenderedImage saveImage) {
		McWillFileDialog d = new McWillFileDialog() {
			@Override
			protected JDialog createDialog(Component parent) throws HeadlessException {
				JDialog dialog = super.createDialog(parent);
				dialog.setIconImages(window.getIconImages());
				return dialog;
			}
		};
		d.setDialogType(JFileChooser.SAVE_DIALOG);
		d.setAcceptAllFileFilterUsed(false);
		d.addChoosableFileFilter(new McWillFileFilter("png"));
		d.addChoosableFileFilter(new McWillFileFilter("jpg"));
		d.addChoosableFileFilter(new McWillFileFilter("gif"));
		d.addChoosableFileFilter(new McWillFileFilter("bmp"));

		int index = d.showSaveDialog(window);
		if (index == JFileChooser.APPROVE_OPTION) {
			File selectedFile = d.getSelectedFile();
			McWillFileFilter fileFilter = (McWillFileFilter) d.getFileFilter();
			String extension = fileFilter.getExtension();
			String fileName = d.getName(selectedFile) + "." + extension;
			String writePath = d.getCurrentDirectory().getAbsolutePath() + File.separator + fileName;
			File pathFile = new File(writePath);
			try {
				ImageIO.write(saveImage, extension, pathFile);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	// public static void centerWindowOnScreen(Runnable runnable) {
	// // This method is never used
	//
	// }
	/**
	 * Returns the native icon, if one exists for the filetype, otherwise
	 * returns a default document icon.
	 * 
	 * @param file
	 *            the file to check icon sendOrReceive.
	 * @return the native icon, otherwise default document icon.
	 */
	public static ImageIcon getIcon(File file) {
		try {
			sun.awt.shell.ShellFolder sf = sun.awt.shell.ShellFolder.getShellFolder(file);
			// Get large icon
			return new ImageIcon(sf.getIcon(true), sf.getFolderType());
		} catch (Exception e) {
			try {
				return (ImageIcon) new JFileChooser().getIcon(file);
			} catch (Exception e1) {
				// Do nothing.
			}
		}
		return null;
	}

	public static Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 12);

	public static Font getDefaultFont() {
		return DEFAULT_FONT;
	}

}
