package com.xinwei.common.lang;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;


public class SerializableUtil {

	//  序列化类为字符串
	public static String setObjToStr(Object obj) {
		String objBody = null;
		ByteArrayOutputStream baops = null;
		ObjectOutputStream oos = null;
		try {
			baops = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baops);
			oos.writeObject(obj);
			byte[] bytes = baops.toByteArray();
			objBody = Base64.encodeBytes(bytes);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeOutputStream(oos);
			closeOutputStream(baops);
		}
		return objBody;
	}

	private static void closeOutputStream(OutputStream oos) {
		try {
			if (oos != null)
				oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	//  反序列化字符串为对象
	public static Object getObjFromStr(String objBody) {
		byte[] bytes = objBody.getBytes();
		ObjectInputStream ois = null;
		Object obj = null;
		try {
			ois = new ObjectInputStream(new BufferedInputStream(new ByteArrayInputStream(Base64.decode(bytes, 0, bytes.length))));
			obj = ois.readObject();
		} catch (Exception e) {
		} finally {
			closeInputStream(ois);
		}
		return obj;
	}

	private static void closeInputStream(InputStream ois) {
		try {
			if (ois != null)
				ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
