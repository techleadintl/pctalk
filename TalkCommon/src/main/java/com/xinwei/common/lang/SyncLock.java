package com.xinwei.common.lang;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class SyncLock {
	private static Map<Object, SyncLock> lockMap = Collections.synchronizedMap(new HashMap());
	private Object lockId;
	private Object content;
	boolean isLock;

	public SyncLock(Object lockId) {
		if (lockId == null)
			throw new IllegalArgumentException("synlock id is null");
		this.lockId = lockId;
		lockMap.put(lockId, this);
	}

	public static boolean hasLock(Object lockid) {
		return lockMap.containsKey(lockid);
	}

	public boolean lock(long timeout) {
		return lock(timeout, TimeUnit.MILLISECONDS);
	}

	public boolean lock(long timeout, TimeUnit unit) {
		if (!hasLock(this.lockId))
			return true;
		try {
			synchronized (this) {
				this.wait(unit.toMillis(timeout));
			}
			return true;
		} catch (Exception localException) {
			return false;
		} finally {
			lockMap.remove(this.lockId);
		}
	}

	public void unlock() {
		synchronized (this) {
			notify();
		}
		lockMap.remove(this.lockId);
	}

	public static boolean unlock(Object lockId, Object content) {
		if (lockId == null) {
			return false;
		}
		SyncLock lock = (SyncLock) lockMap.get(lockId);
		if (lock == null) {
			return false;
		}

		lock.setContent(content);
		lock.unlock();
		return true;
	}

	public static boolean unlock(Object lockId) {
		if (lockId == null) {
			return false;
		}
		SyncLock lock = (SyncLock) lockMap.get(lockId);
		if (lock == null) {
			return false;
		}
		lock.unlock();
		return true;
	}

	public static boolean setContent(Object lockId, Object content) {
		if (lockId == null) {
			return false;
		}
		SyncLock lock = (SyncLock) lockMap.get(lockId);
		if (lock == null) {
			return false;
		}

		lock.setContent(content);
		return true;
	}

	public static boolean addContent(Object lockId, Object obj) {
		if (lockId == null) {
			return false;
		}
		SyncLock lock = (SyncLock) lockMap.get(lockId);
		if (lock == null) {
			return false;
		}
		Object content = lock.getContent();
		if (content == null) {
			content = new ArrayList();
		}
		if ((content instanceof List)) {
			List contentList = (List) content;
			contentList.add(obj);
		}
		lock.setContent(content);
		return true;
	}

	public static int getContentSize(Object lockId) {
		if (lockId == null) {
			return -1;
		}
		SyncLock lock = (SyncLock) lockMap.get(lockId);
		if (lock == null) {
			return -1;
		}
		Object content = lock.getContent();
		if (content == null) {
			return 0;
		}
		if ((content instanceof Collection)) {
			return ((Collection) content).size();
		}
		return 1;
	}

	public Object getLockId() {
		return this.lockId;
	}

	public void setLockId(Object lockId) {
		this.lockId = lockId;
	}

	public <T> T getContent() {
		return (T) content;
	}

	public void setContent(Object content) {
		this.content = content;
	}
}