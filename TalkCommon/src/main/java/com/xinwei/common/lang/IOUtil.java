package com.xinwei.common.lang;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class IOUtil {
	public static final String LINE_SEPARATOR;

	static {
		StringWriter stringWriter = new StringWriter();
		PrintWriter out = new PrintWriter(stringWriter);
		out.println();
		LINE_SEPARATOR = stringWriter.toString();
		out.close();
	}

	public static void closeInputStream(InputStream inputStream) {
		closeStream(inputStream);
	}

	public static void closeStream(Closeable stream) {
		if (stream != null)
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	public static byte[] toByteArray(InputStream input) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		byte[] buffer = new byte[4096];
		long count = 0L;
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		if (count > 2147483647L) {
			return null;
		}
		return output.toByteArray();
	}

	public static void writeObject(String fileName, Object obj) {
		if ((fileName == null) || (obj == null)) {
			return;
		}

		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(openOutputStream(new File(fileName), false));
			out.writeObject(obj);
			out.flush();
		} catch (IOException localIOException) {
		} finally {
			closeQuietly(out);
		}
	}

	public static boolean writeObject(String fileName, Object obj, boolean create) {
		if ((fileName == null) || (obj == null)) {
			return false;
		}
		File file = new File(fileName);
		ObjectOutputStream out = null;
		try {
			if (create) {
				if (!file.exists()) {
					File parentFile = file.getParentFile();
					if (!parentFile.exists()) {
						parentFile.mkdirs();
					}
					file.createNewFile();
				} else if (!file.canWrite()) {
					file.delete();
					file.createNewFile();
				}
			}
			out = new ObjectOutputStream(openOutputStream(new File(fileName), false));
			out.writeObject(obj);
			out.flush();
		} catch (IOException localIOException) {
			return false;
		} finally {
			closeQuietly(out);
		}
		return true;
	}

	private static void rename(File file) {
		try {
			File file2 = new File(file.getName() + ".bak_bysystem");
			if (file2.exists())
				file2.delete();
			file.renameTo(file2);
		} catch (Exception localException) {
		}
	}

	public static FileInputStream openInputStream(File file) throws IOException {
		if (file.exists()) {
			if (file.isDirectory()) {
				throw new IOException("File '" + file + "' exists but is a directory");
			}
			if (!file.canRead())
				throw new IOException("File '" + file + "' cannot be read");
		} else {
			File parentFile = file.getParentFile();
			if ((parentFile != null) && (!parentFile.mkdirs()) && (!parentFile.isDirectory())) {
				throw new IOException("Directory '" + parentFile + "' could not be created");
			}
			file.createNewFile();
		}
		return new FileInputStream(file);
	}

	public static FileOutputStream openOutputStream(File file) throws IOException {
		return openOutputStream(file, false);
	}

	public static FileOutputStream openOutputStream(File file, boolean append) throws IOException {
		if (file.exists()) {
			if (file.isDirectory()) {
				throw new IOException("File '" + file + "' exists but is a directory");
			}
			if (!file.canWrite())
				throw new IOException("File '" + file + "' cannot be written to");
		} else {
			File parent = file.getParentFile();
			if ((parent != null) && (!parent.mkdirs()) && (!parent.isDirectory())) {
				throw new IOException("Directory '" + parent + "' could not be created");
			}
		}

		return new FileOutputStream(file, append);
	}

	public static List<String> readLines(File file) throws IOException {
		return readLines(file, Charset.defaultCharset());
	}

	public static List<String> readLines(File file, String encoding) throws IOException {
		return readLines(file, Charsets.toCharset(encoding));
	}

	public static List<String> readLines(File file, Charset encoding) throws IOException {
		InputStream in = null;
		try {
			in = openInputStream(file);
			List localList = readLines(in, Charsets.toCharset(encoding));
			return localList;
		} catch (Exception e) {
			throw new IOException(e.getMessage());
		} finally {
			closeQuietly(in);
		}
	}

	public static List<String> readLines(InputStream input) throws IOException {
		return readLines(input, Charset.defaultCharset());
	}

	public static List<String> readLines(InputStream input, Charset encoding) throws IOException {
		InputStreamReader reader = new InputStreamReader(input, Charsets.toCharset(encoding));
		return readLines(reader);
	}

	public static List<String> readLines(Reader reader) throws IOException {
		BufferedReader bufferedReader = (reader instanceof BufferedReader) ? (BufferedReader) reader : new BufferedReader(reader);
		List list = new ArrayList();
		String line = bufferedReader.readLine();
		while (line != null) {
			list.add(line);
			line = bufferedReader.readLine();
		}
		return list;
	}

	public static byte[] readBytes(File file) {
		FileInputStream fis = null;
		try {
			byte[] r = new byte[(int) file.length()];
			fis = new FileInputStream(file);
			fis.read(r);
			return r;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static byte[] readBytes(String file) {
		return readBytes(new File(file));
	}

	public static byte[] readBytes(URL url) {
		InputStream openStream = null;
		try {
			openStream = url.openStream();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			int i = 0;
			byte b[] = new byte[1024];
			while ((i = openStream.read(b)) != -1) {
				baos.write(b, 0, i);
			}
			byte[] byteArray = baos.toByteArray();
			baos.close();
			return byteArray;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				openStream.close();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(url);
			}
		}
		return null;
	}

	//
	public static void writeBytes(File file, byte[] data) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			fos.write(data);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static String toString(InputStream input) throws IOException {
		StringWriter sw = new StringWriter();
		InputStreamReader in = new InputStreamReader(input);
		copy(in, sw);
		return sw.toString();
	}

	public static String toString(InputStream input, String charset) throws IOException {
		StringWriter sw = new StringWriter();
		InputStreamReader in = new InputStreamReader(input, charset);
		copy(in, sw);
		return sw.toString();
	}

	public static void writeLines(File file, Collection<?> lines) throws IOException {
		writeLines(file, null, lines, null, false);
	}

	public static void writeLines(File file, Collection<?> lines, boolean append) throws IOException {
		writeLines(file, null, lines, null, append);
	}

	public static void writeLines(File file, String encoding, Collection<?> lines, String lineEnding) throws IOException {
		writeLines(file, encoding, lines, lineEnding, false);
	}

	public static void writeLines(File file, String encoding, Collection<?> lines, String lineEnding, boolean append) throws IOException {
		OutputStream out = null;
		try {
			out = openOutputStream(file, append);
			writeLines(lines, lineEnding, out, encoding);
			out.close();
		} finally {
			closeQuietly(out);
		}
	}

	public static void writeLines(File file, Collection<?> lines, String lineEnding) throws IOException {
		writeLines(file, null, lines, lineEnding, false);
	}

	public static void writeLines(File file, Collection<?> lines, String lineEnding, boolean append) throws IOException {
		writeLines(file, null, lines, lineEnding, append);
	}

	public static void writeLines(Collection<?> lines, String lineEnding, OutputStream output) throws IOException {
		writeLines(lines, lineEnding, output, Charset.defaultCharset());
	}

	public static void writeLines(Collection<?> lines, String lineEnding, OutputStream output, Charset encoding) throws IOException {
		if (lines == null) {
			return;
		}
		if (lineEnding == null) {
			lineEnding = LINE_SEPARATOR;
		}
		Charset cs = Charsets.toCharset(encoding);
		for (Iterator iter = lines.iterator(); iter.hasNext();) {
			Object line = iter.next();
			if (line != null) {
				output.write(line.toString().getBytes(cs.name()));
			}
			output.write(lineEnding.getBytes(cs.name()));
		}
	}

	public static void writeLines(Collection<?> lines, String lineEnding, OutputStream output, String encoding) throws IOException {
		writeLines(lines, lineEnding, output, Charsets.toCharset(encoding));
	}

	public static void writeLines(Collection<?> lines, String lineEnding, Writer writer) throws IOException {
		if (lines == null) {
			return;
		}
		if (lineEnding == null) {
			lineEnding = LINE_SEPARATOR;
		}
		for (Iterator iter = lines.iterator(); iter.hasNext();) {
			Object line = iter.next();
			if (line != null) {
				writer.write(line.toString());
			}
			writer.write(lineEnding);
		}
	}

	public static void writeLine(String line, OutputStream output) throws IOException {
		if (line != null) {
			output.write(line.getBytes());
		}
		output.write(LINE_SEPARATOR.getBytes());
	}

	public static void writeLine(Object[] array, String separator, OutputStream output) throws IOException {
		if (array != null) {
			String line = StringUtil.join(array, separator);
			output.write(line.getBytes());
		}
		output.write(LINE_SEPARATOR.getBytes());
	}

	public static void writeLine(Object[] array, String separator, OutputStream output, Charset charset) throws IOException {
		if (array != null) {
			String line = StringUtil.join(array, separator);
			output.write(line.getBytes(charset));
		}
		output.write(LINE_SEPARATOR.getBytes());
	}

	public static int copy(Reader input, Writer output) throws IOException {
		char[] buffer = new char[4096];
		long count = 0L;
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		if (count > 2147483647L) {
			return -1;
		}
		return (int) count;
	}

	public static void copyFile(File srcFile, File destFile) throws IOException {
		copyFile(srcFile, destFile, true);
	}

	public static void copyFile(File srcFile, File destFile, boolean replace) throws IOException {
		copyFile(srcFile, destFile, replace, false);
	}

	public static void copyFile(File srcFile, File destFile, boolean replace, boolean preserveFileDate) throws IOException {
		if ((!replace) && (destFile.exists())) {
			throw new IOException(destFile + " exists");
		}
		if ((destFile.exists()) && (destFile.isDirectory())) {
			throw new IOException("Destination '" + destFile + "' exists but is a directory");
		}
		FileInputStream fis = null;
		FileOutputStream fos = null;
		FileChannel input = null;
		FileChannel output = null;
		try {
			fis = new FileInputStream(srcFile);
			fos = openOutputStream(destFile);
			input = fis.getChannel();
			output = fos.getChannel();
			long size = input.size();
			long pos = 0L;
			long count = 0L;
			while (pos < size) {
				count = size - pos > 31457280L ? 31457280L : size - pos;
				pos += output.transferFrom(input, pos, count);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeQuietly(output);
			closeQuietly(fos);
			closeQuietly(input);
			closeQuietly(fis);
		}

		if (srcFile.length() != destFile.length()) {
			throw new IOException("Failed to copy full contents from '" + srcFile + "' to '" + destFile + "'");
		}

		if (preserveFileDate)
			destFile.setLastModified(srcFile.lastModified());
	}

	public static void moveFile(File srcFile, File destFile) throws IOException {
		boolean rename = srcFile.renameTo(destFile);
		if (!rename) {
			copyFile(srcFile, destFile);
			if (!srcFile.delete()) {
				srcFile.delete();
				throw new IOException("Failed to delete original file '" + srcFile + "' after copy to '" + destFile + "'");
			}
		}
	}

	public static void forceMkdir(File directory) throws IOException {
		if (directory.exists()) {
			if (!directory.isDirectory()) {
				String message = "File " + directory + " exists and is " + "not a directory. Unable to create directory.";

				throw new IOException(message);
			}
		} else if ((!directory.mkdirs()) && (!directory.isDirectory())) {
			String message = "Unable to create directory " + directory;

			throw new IOException(message);
		}
	}

	public static void closeQuietly(Closeable closeable) {
		try {
			if (closeable != null)
				closeable.close();
		} catch (IOException localIOException) {
		}
	}

	public static String getBaseName(String filename) {
		if (filename == null) {
			return null;
		}

		int index = filename.lastIndexOf('.');

		if (index == -1) {
			return filename;
		}

		return filename.substring(0, index);
	}

	public static String getExtensionName(String filename) {
		if (filename == null) {
			return null;
		}

		int index = filename.lastIndexOf('.');

		if (index == -1||index == filename.length() - 1) {
			return filename;
		}

		return filename.substring(index + 1);
	}

	public static class Charsets {
		public static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");

		public static final Charset US_ASCII = Charset.forName("US-ASCII");

		public static final Charset UTF_16 = Charset.forName("UTF-16");

		public static final Charset UTF_16BE = Charset.forName("UTF-16BE");

		public static final Charset UTF_16LE = Charset.forName("UTF-16LE");

		public static final Charset UTF_8 = Charset.forName("UTF-8");

		public static Charset toCharset(Charset charset) {
			return charset == null ? Charset.defaultCharset() : charset;
		}

		public static Charset toCharset(String charset) {
			return charset == null ? Charset.defaultCharset() : Charset.forName(charset);
		}
	}
}