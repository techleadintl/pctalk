package com.xinwei.common.lang;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.ImageIcon;

import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.common.resource.XRes;

public class XLookAndFeel extends XRes {
	private static Map<String, Color> colors = new HashMap<String, Color>();
	private static Map<String, Font> fonts = new HashMap<String, Font>();
	private static Map<String, McWillImage> images = new HashMap<String, McWillImage>();
	private static Object imageLock = new Object();
	private static Object fontLock = new Object();
	private static Object colorLock = new Object();

	public static Font getFont(int size) {
		return getFont(size, false);
	}

	public static Font getFont(int size, boolean bold) {
		return getFont("serif", size, bold);
	}

	public static Font getFont(String name, int size, boolean bold) {
		String key = String.valueOf(size + "" + bold);
		synchronized (fontLock) {
			Font font = fonts.get(key);
			if (font == null) {
				if (bold) {
					font = new Font(name, Font.BOLD, size);
				} else {
					font = new Font(name, Font.PLAIN, size);
				}
				fonts.put(key, font);
			}
			return font;
		}
	}

	// public static void putImageIcon(String key, BufferedImage icon) {
	// synchronized (imageLock) {
	// images.put(key, icon);
	// }
	// }
	public static McWillImage cacheImage(String key, byte[] bytes) {
		return cacheImage(key, new McWillImage(bytes));
	}

	public static McWillImage cacheImage(String key, McWillImage mcWillImage) {
		synchronized (imageLock) {
			images.put(key, mcWillImage);
			return mcWillImage;
		}
	}

	public static void clearImageByPrefix(String key) {
		synchronized (imageLock) {
			Iterator<Entry<String, McWillImage>> iterator = images.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, McWillImage> next = iterator.next();
				if (next.getKey().startsWith(key)) {
					iterator.remove();
				}
			}
		}
	}

	public static ImageIcon getImageIconByKey(String imageName, int w, int h) {
		return getImageIcon(imageName, w, h);
	}

	public static ImageIcon getImageIconByKey(String imageName, int w, int h, ImageIcon defaultImageIcon) {
		ImageIcon imageIcon = getImageIcon(imageName, w, h);
		if (imageIcon == null) {
			return defaultImageIcon;
		} else {
			return imageIcon;
		}
	}

	public static ImageIcon getImageIconByKey(String imageName, ImageIcon defaultImageIcon) {
		ImageIcon imageIcon = getImageIconByKey(imageName);
		if (imageIcon == null) {
			return defaultImageIcon;
		} else {
			return imageIcon;
		}
	}

	public static BufferedImage getImageByKey(String imageName, BufferedImage defaultImageIcon) {
		BufferedImage bufferedImage = getImageByKey(imageName);
		if (bufferedImage == null) {
			return defaultImageIcon;
		} else {
			return bufferedImage;
		}
	}

	public static ImageIcon getImageIconByKey(String imageName) {
		McWillImage mcWillImage = getMcWillImage(imageName);
		if (mcWillImage == null)
			return null;
		return mcWillImage.getImageIcon();
	}

	public static BufferedImage getImageByKey(String imageName) {
		McWillImage mcWillImage = getMcWillImage(imageName);
		if (mcWillImage == null)
			return null;
		return mcWillImage.getImage();
	}

	public static BufferedImage getImage(String imageName, int w, int h) {
		McWillImage mcWillImage = getMcWillImage(imageName, w, h);
		if (mcWillImage == null)
			return null;
		return mcWillImage.getImage();
	}

	public static ImageIcon getImageIcon(String imageName, int w, int h) {
		McWillImage mcWillImage = getMcWillImage(imageName, w, h);
		if (mcWillImage == null)
			return null;
		return mcWillImage.getImageIcon();
	}

	public static ImageIcon getImageIcon(byte[] bytes) {
		McWillImage mcWillImage = null;
		if (bytes != null && bytes.length > 0) {
			String imageKey = getKey(bytes.hashCode() + "bytes", 0, 0);
			mcWillImage = getMcWillImage(imageKey);
			if (mcWillImage == null) {
				mcWillImage = cacheImage(imageKey, bytes);
			}
		}
		if (mcWillImage != null) {
			return mcWillImage.getImageIcon();
		} else {
			return null;
		}
	}

	public static ImageIcon getImageIcon(byte[] bytes, int w, int h) {
		McWillImage mcWillImage = getMcWillImage(bytes, w, h);
		if (mcWillImage != null) {
			return mcWillImage.getImageIcon();
		} else {
			return null;
		}
	}

	public static Image getImage(byte[] bytes, int w, int h) {
		McWillImage mcWillImage = getMcWillImage(bytes, w, h);
		if (mcWillImage != null) {
			return mcWillImage.getImage();
		} else {
			return null;
		}
	}

	public static ImageIcon getImageIcon(String imageName) {
		McWillImage mcWillImage = getMcWillImage(imageName);
		if (mcWillImage == null)
			return null;
		return mcWillImage.getImageIcon();
	}

	public static BufferedImage getImage(String imageName) {
		McWillImage mcWillImage = getMcWillImage(imageName);
		if (mcWillImage == null)
			return null;
		return mcWillImage.getImage();
	}

	/**
	 * get cached image from memory or download image from given URL
	 * 
	 * @param imageName system defined image name for a image URL
	 **/
	public static McWillImage getMcWillImage(String imageName) {
		Map<String, McWillImage> image = images;
		synchronized (imageLock) {
			McWillImage mcWillImage = images.get(imageName);
			if (mcWillImage == null) {
				try {
					final URL imageURL = getURL(imageName);
					if (imageURL != null) {
						byte[] bytes = IOUtil.readBytes(imageURL);
						mcWillImage = new McWillImage(bytes);
						images.put(imageName, mcWillImage);
					}
				} catch (Exception ex) {
				}
			}
			return mcWillImage;
		}
	}

	public static byte[] getImageBytes(String imageName) {
		Map<String, McWillImage> image = images;
		McWillImage mcWillImage = getMcWillImage(imageName);
		if (mcWillImage != null) {
			return mcWillImage.getBytes();
		}
		return null;
	}

	private static McWillImage getMcWillImage(String imageName, int w, int h) {
		Map<String, McWillImage> image = images;
		String imageKey = getKey(imageName, w, h);
		synchronized (imageLock) {
			McWillImage mcWillImage = images.get(imageKey);
			if (mcWillImage == null) {
				mcWillImage = getMcWillImage(imageName);
				if (mcWillImage != null) {
					mcWillImage = getMcWillImage(mcWillImage, w, h);
					images.put(imageKey, mcWillImage);
				}
			}
			return mcWillImage;
		}
	}

	private static McWillImage getMcWillImage(McWillImage mcWillImage, int w, int h) {
		String format = mcWillImage.getFormat();
		byte[] bytes = ImageUtil.resize(mcWillImage.getImage(), w, h, format);
		return new McWillImage(bytes);
	}

	private static McWillImage getMcWillImage(byte[] bytes, int w, int h) {
		McWillImage mcWillImage = null;
		if (bytes != null && bytes.length > 0) {
			String imageWH = getKey(bytes.hashCode() + "bytes", w, h);
			mcWillImage = getMcWillImage(imageWH);
			if (mcWillImage == null) {
				String imageKey00 = getKey(bytes.hashCode() + "bytes", 0, 0);
				mcWillImage = getMcWillImage(imageKey00);
				if (mcWillImage == null) {
					mcWillImage = cacheImage(imageKey00, bytes);
				}
				mcWillImage = getMcWillImage(mcWillImage, w, h);
				cacheImage(imageWH, mcWillImage);
			}

		}
		return mcWillImage;
	}

	public static URL getImageIconURL(String imageName) {
		try {
			return getURL(imageName);
		} catch (Exception ex) {
			// Log.info("image is not found:" + imageName);
		}
		return null;
	}

	private static String getKey(Object... os) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < os.length; i++) {
			if (i != 0) {
				sb.append("-");
			}
			sb.append(os[i]);
		}
		return sb.toString();

	}

	public static List<ImageIcon> getIconList(String fileUrl) {
		try {
			final URL url = getURL(fileUrl);
			String file = url.getFile();
			File[] listFiles = new File(file).listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					if (name.toLowerCase().endsWith("jpg") || name.toLowerCase().endsWith("png")
							|| name.toLowerCase().endsWith("gif") || name.toLowerCase().endsWith("jpeg"))
						return true;
					return false;
				}
			});
			List<ImageIcon> iconList = new ArrayList<ImageIcon>();

			for (File f : listFiles) {
				URL imageURL = f.toURI().toURL();
				ImageIcon imageIcon = new ImageIcon(imageURL);
				imageIcon.setDescription(Base64.encodeObject(imageURL));
				iconList.add(imageIcon);
			}
			return iconList;
		} catch (Exception ex) {
			Log4j.error(fileUrl + " not found.", ex);
		}
		return null;
	}

	public static Color getColor(String colorKey) {
		synchronized (colorLock) {
			Color color = colors.get(colorKey);
			if (color == null) {
				String rgbValue = getString(colorKey);
				if (rgbValue == null)
					return null;
				Color rgb = getRGB(rgbValue);
				if (rgb != null) {
					colors.put(colorKey, rgb);
					color = rgb;
				}
			}
			return color;
		}
	}

	public static Color getColor(String colorKey, Color defaultColor) {
		synchronized (colorLock) {
			Color color = colors.get(colorKey);
			if ((color == null) && (defaultColor != null)) {
				colors.put(colorKey, defaultColor);
				color = colors.get(colorKey);
			}
			return color;
		}
	}

	public static Color[] getColors(String key) {
		String s = getString(key);
		if (s == null)
			return null;
		String[] split = s.split(";");
		List colors = new ArrayList(split.length);
		for (String colorKey : split) {
			Color rgb = getRGB(colorKey);
			if (rgb != null) {
				Color color = getColor(colorKey, rgb);
				colors.add(color);
			}
		}

		return (Color[]) colors.toArray(new Color[0]);
	}

	public static Color getColor(int r, int g, int b) {
		String colorKey = new StringBuilder().append(r).append(DOT).append(g).append(DOT).append(b).toString();
		Color color = getColor(colorKey);
		if (color == null) {
			color = new Color(r, g, b);
			synchronized (colorLock) {
				colors.put(colorKey, color);
			}
		}
		return color;
	}

	public static Color getColor(int rgb) {
		String colorKey = "color" + rgb;
		Color color = getColor(colorKey);
		if (color == null) {
			color = new Color(rgb);
			synchronized (colorLock) {
				colors.put(colorKey, color);
			}
		}
		return color;
	}

	private static Color getRGB(String rgbKey) {
		if (rgbKey != null) {
			String[] vs = rgbKey.split(DOT);
			if (vs.length == 3)
				try {
					return new Color(getInt(vs[0].trim()), getInt(vs[1].trim()), getInt(vs[2].trim()));
				} catch (Exception localException) {
				}
		}
		return null;
	}

	private static int getInt(String v) {
		if (v == null)
			return 0;
		if (v.toLowerCase().startsWith("0x")) {
			return Integer.parseInt(v.substring(2), 16);
		}
		return Integer.valueOf(v).intValue();
	}

	static String DOT = ",";
}
