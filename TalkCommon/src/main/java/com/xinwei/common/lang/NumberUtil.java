package com.xinwei.common.lang;

public class NumberUtil
{
  public static boolean isInt(String source)
  {
    try
    {
      Integer.parseInt(source);
      return true; } catch (Exception localException) {
    }
    return false;
  }

  public static int getInt(String source, int defaultValue)
  {
    try {
      return Integer.parseInt(source); } catch (Exception localException) {
    }
    return defaultValue;
  }

  public static long getLong(String source, long defaultValue)
  {
    try {
      return Long.parseLong(source); } catch (Exception localException) {
    }
    return defaultValue;
  }

  public static float getFloat(String source, float defaultValue)
  {
    try {
      return Float.parseFloat(source); } catch (Exception localException) {
    }
    return defaultValue;
  }

  public static double getDouble(String source, double defaultValue)
  {
    try {
      return Double.parseDouble(source); } catch (Exception localException) {
    }
    return defaultValue;
  }

  public static short getShort(String source, short defaultValue)
  {
    try {
      return Short.parseShort(source); } catch (Exception localException) {
    }
    return defaultValue;
  }

  public static byte getByte(String s, byte defaultValue)
  {
    try {
      return Byte.parseByte(s); } catch (Exception localException) {
    }
    return defaultValue;
  }

  public static long minimum(long a, long b, long c)
  {
    if (b < a) {
      a = b;
    }
    if (c < a) {
      a = c;
    }
    return a;
  }

  public static int minimum(int a, int b, int c) {
    if (b < a) {
      a = b;
    }
    if (c < a) {
      a = c;
    }
    return a;
  }

  public static long maximum(long a, long b, long c) {
    if (b > a) {
      a = b;
    }
    if (c > a) {
      a = c;
    }
    return a;
  }

  public static int maximum(int a, int b, int c) {
    if (b > a) {
      a = b;
    }
    if (c > a) {
      a = c;
    }
    return a;
  }

  public static boolean isDigits(String str) {
    if ((str == null) || (str.length() == 0)) {
      return false;
    }
    for (int i = 0; i < str.length(); i++) {
      if (!Character.isDigit(str.charAt(i))) {
        return false;
      }
    }
    return true;
  }

  public static boolean isNumber(String str) {
    if (StringUtil.isEmpty(str)) {
      return false;
    }
    char[] chars = str.toCharArray();
    int sz = chars.length;
    boolean hasExp = false;
    boolean hasDecPoint = false;
    boolean allowSigns = false;
    boolean foundDigit = false;

    int start = chars[0] == '-' ? 1 : 0;
    if ((sz > start + 1) && (chars[start] == '0') && (chars[(start + 1)] == 'x')) {
      int i = start + 2;
      if (i == sz) {
        return false;
      }
      do
      {
        if (((chars[i] < '0') || (chars[i] > '9')) && ((chars[i] < 'a') || (chars[i] > 'f')) && ((chars[i] < 'A') || (chars[i] > 'F')))
          return false;
        i++; } while (i < chars.length);

      return true;
    }

    sz--;

    int i = start;

    while ((i < sz) || ((i < sz + 1) && (allowSigns) && (!foundDigit))) {
      if ((chars[i] >= '0') && (chars[i] <= '9')) {
        foundDigit = true;
        allowSigns = false;
      } else if (chars[i] == '.') {
        if ((hasDecPoint) || (hasExp)) {
          return false;
        }
        hasDecPoint = true;
      } else if ((chars[i] == 'e') || (chars[i] == 'E')) {
        if (hasExp) {
          return false;
        }
        if (!foundDigit) {
          return false;
        }
        hasExp = true;
        allowSigns = true;
      } else if ((chars[i] == '+') || (chars[i] == '-')) {
        if (!allowSigns) {
          return false;
        }
        allowSigns = false;
        foundDigit = false;
      } else {
        return false;
      }
      i++;
    }
    if (i < chars.length) {
      if ((chars[i] >= '0') && (chars[i] <= '9')) {
        return true;
      }
      if ((chars[i] == 'e') || (chars[i] == 'E')) {
        return false;
      }
      if ((!allowSigns) && ((chars[i] == 'd') || (chars[i] == 'D') || (chars[i] == 'f') || (chars[i] == 'F'))) {
        return foundDigit;
      }
      if ((chars[i] == 'l') || (chars[i] == 'L')) {
        return (foundDigit) && (!hasExp);
      }

      return false;
    }

    return (!allowSigns) && (foundDigit);
  }
}