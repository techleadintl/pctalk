package com.xinwei.common.lang;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Sqlite {
	private static BeanConverter beanConverter = new BeanConverter();

	private ConnectionFactory connectionFactory = null;

	private boolean autoClose = false;

	static {
		try {// 加载JDBC驱动
			Class.forName("org.sqlite.JDBC");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Sqlite(String filePath) {
		this(new SingleConnectionFactory("jdbc:sqlite:" + filePath, "username", "password"));
	}

	public Sqlite(ConnectionFactory connectionFactory) {
		this.connectionFactory = connectionFactory;
	}

	private int insert(Connection conn, String sql, Object... params) throws SQLException {
		return update(conn, sql, params);
	}

	private int update(Connection conn, String sql, Object... params) throws SQLException {
		PreparedStatement stmt = null;
		int rows = 0;
		try {
			stmt = conn.prepareStatement(sql);
			fillStatement(stmt, params);
			rows = stmt.executeUpdate();
		} catch (SQLException e) {
			rethrow(e, sql, params);
		} finally {
			close(conn, stmt, null);
		}

		return rows;
	}

	private int updateTransaction(Connection conn, String sql, List<Object[]> paramsList) throws SQLException {
		PreparedStatement stmt = null;
		int rows = 0;
		Object[] params = null;
		try {
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			for (int i = 0; i < paramsList.size(); i++) {
				params = paramsList.get(i);
				fillStatement(stmt, params);
				rows += stmt.executeUpdate();
			}
			conn.commit();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (Exception ex) {
			}
			rethrow(e, sql, paramsList.get(0));
		} finally {

			conn.setAutoCommit(true);
			close(conn, stmt, null);
		}
		return rows;
	}

	private int updateTransaction(Connection conn, List<String> sqls, List<Object[]> paramsList) throws SQLException {
		PreparedStatement stmt = null;
		int rows = 0;
		String sql = null;
		Object[] params = null;
		try {
			conn.setAutoCommit(false);
			for (int i = 0; i < Math.min(sqls.size(), paramsList.size()); i++) {
				sql = sqls.get(i);
				params = paramsList.get(i);
				stmt = conn.prepareStatement(sql);
				fillStatement(stmt, params);
				rows += stmt.executeUpdate();
			}
			conn.commit();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (Exception ex) {
			}
			rethrow(e, sql, params);
		} finally {
			conn.setAutoCommit(true);
			close(conn, stmt, null);
		}
		return rows;
	}

	public Connection updateTransaction(String sql, Object... params) throws Exception {
		PreparedStatement stmt = null;
		Connection connection = connectionFactory.getConnection();
		connection.setAutoCommit(false);
		stmt = connection.prepareStatement(sql);
		fillStatement(stmt, params);
		stmt.executeUpdate();
		connection.commit();
		return connection;
	}

	public void rollback(Connection connection) throws Exception {
		try {
			connection.rollback();
		} catch (Exception ex) {
		}
	}

	public void closeTransaction(Connection connection) throws Exception {
		try {
			connection.setAutoCommit(true);
			close(connection, null, null);
		} catch (Exception ex) {
		}
	}

	public <T> T query(Connection conn, String sql, Class<T> clazz, Object... params) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		T result = null;
		try {
			stmt = conn.prepareStatement(sql);
			fillStatement(stmt, params);
			rs = stmt.executeQuery();
			result = beanConverter.toBean(rs, clazz);
		} catch (SQLException e) {
			e.printStackTrace();
			rethrow(e, sql, params);
		} finally {
			close(conn, stmt, rs);
		}

		return result;
	}

	public <T> List<T> queryList(Connection conn, String sql, Class<T> clazz, Object... params) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<T> result = null;
		try {
			stmt = conn.prepareStatement(sql);
			fillStatement(stmt, params);
			rs = stmt.executeQuery();
			result = beanConverter.toBeanList(rs, clazz);
		} catch (SQLException e) {
			rethrow(e, sql, params);
		} finally {
			close(conn, stmt, rs);
		}

		return result;
	}

	// /
	public int createTable(String sql) throws SQLException {
		return update(connectionFactory.getConnection(), sql);
	}

	public int insertList(String sql, List<Object[]> paramsList) throws SQLException {
		return updateList(sql, paramsList);
	}

	public int insertTransaction(List<String> sqls, List<Object[]> paramsList) throws SQLException {
		return updateTransaction(sqls, paramsList);
	}

	public int updateTransaction(List<String> sqls, List<Object[]> paramsList) throws SQLException {
		return updateTransaction(connectionFactory.getConnection(), sqls, paramsList);
	}

	public int updateList(String sql, List<Object[]> paramsList) throws SQLException {
		return updateTransaction(connectionFactory.getConnection(), sql, paramsList);
	}

	public int insert(String sql, Object... params) throws SQLException {
		return update(sql, params);
	}

	public int update(String sql, Object... params) throws SQLException {
		return update(connectionFactory.getConnection(), sql, params);
	}

	public <T> T query(String sql, Class<T> clazz, Object... params) throws SQLException {
		return query(connectionFactory.getConnection(), sql, clazz, params);
	}

	public LinkedHashMap<String, Object> queryMap(String sql, Object... params) throws SQLException {
		return query(connectionFactory.getConnection(), sql, LinkedHashMap.class, params);
	}

	public <T> List<T> queryList(String sql, Class<T> clazz, Object... params) throws SQLException {
		return queryList(connectionFactory.getConnection(), sql, clazz, params);
	}

	public List<LinkedHashMap> queryMapList(String sql, Object... params) throws SQLException {
		return queryList(connectionFactory.getConnection(), sql, LinkedHashMap.class, params);
	}

	public void fillStatement(PreparedStatement stmt, Object[] params) throws SQLException {

		ParameterMetaData pmd = null;
		pmd = stmt.getParameterMetaData();
		if (pmd.getParameterCount() < params.length) {
			throw new SQLException(
					"Too many parameters: expected " + pmd.getParameterCount() + ", was given " + params.length);
		}

		for (int i = 0; i < params.length; i++)
			if (params[i] != null) {
				stmt.setObject(i + 1, params[i]);
			} else {
				int sqlType = 12;
				try {
					sqlType = pmd.getParameterType(i + 1);
				} catch (SQLException e) {
				}
				stmt.setNull(i + 1, sqlType);
			}
	}

	protected void rethrow(SQLException cause, String sql, Object[] params) throws SQLException {
		String causeMessage = cause.getMessage();
		if (causeMessage == null) {
			causeMessage = "";
		}
		StringBuffer msg = new StringBuffer(causeMessage);

		msg.append(" Query: ");
		msg.append(sql);
		msg.append(" Parameters: ");

		if (params == null)
			msg.append("[]");
		else {
			msg.append(Arrays.deepToString(params));
		}

		SQLException e = new SQLException(msg.toString(), cause.getSQLState(), cause.getErrorCode());

		e.setNextException(cause);

		throw e;
	}

	public void close(Connection conn, Statement stmt, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
		}
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (Exception e) {
		}

		try {
			if (autoClose && conn != null) {
				conn.close();
			}
		} catch (Exception e) {
		}
	}

	public boolean register(String driverClassName, String url, String user, String pwd) {
		try {
			Class.forName(driverClassName).newInstance();
			connectionFactory = new SingleConnectionFactory(url, user, pwd);
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return false;
	}

	public void setConnectionFactory(ConnectionFactory factory) {
		connectionFactory = factory;
	}

	public void setAutoClose(boolean autoClose) {
		this.autoClose = autoClose;
	}

	public static void loadDriver(String driverClassName) throws Exception {
		Class.forName(driverClassName).newInstance();
	}

	public static class SingleConnectionFactory implements ConnectionFactory {
		protected String uri = null;

		protected String username = null;

		protected String password = null;

		protected String driverClassName;

		public SingleConnectionFactory(String connectUri, String uname, String passwd) {
			this(connectUri, uname, passwd, null);
		}

		public SingleConnectionFactory(String connectUri, String uname, String passwd, String driverClassName) {
			this.uri = connectUri;
			this.username = uname;
			this.password = passwd;
			this.driverClassName = driverClassName;

		}

		public synchronized Connection getConnection() throws SQLException {
			return createConnection();
		}

		public Connection createConnection() throws SQLException {

			if ((this.username == null) && (this.password == null)) {
				return DriverManager.getConnection(this.uri);
			}
			return DriverManager.getConnection(uri, username, password);
		}

	}

	public static interface ConnectionFactory {

		public abstract Connection getConnection() throws SQLException;

	}

	public static class BeanConverter {
		protected static final int PROPERTY_NOT_FOUND = -1;

		private static final Map<Class<?>, Object> primitiveDefaults = new HashMap<Class<?>, Object>();

		public <T> T toBean(ResultSet rs, Class<T> type) throws SQLException {
			if (!rs.next())
				return null;

			boolean atomicClass = isAtomicClass(type) || type.isArray();
			if (atomicClass) {
				return (T) getValue(rs, 1, type);

			} else if (type == HashMap.class || type == LinkedHashMap.class) {
				Map<String, Object> map = (Map<String, Object>) (newInstance(type));
				ResultSetMetaData rsmd = rs.getMetaData();
				int cols = rsmd.getColumnCount();

				for (int col = 1; col <= cols; col++) {
					String columnName = rsmd.getColumnLabel(col);
					if ((null == columnName) || (0 == columnName.length())) {
						columnName = rsmd.getColumnName(col);
					}
					Object object = rs.getObject(col);
					map.put(columnName.toUpperCase(), object);
				}
				return (T) map;

			} else {
				PropertyDescriptor[] props = propertyDescriptors(type);

				ResultSetMetaData rsmd = rs.getMetaData();
				int[] columnToProperty = mapColumnsToProperties(rsmd, props);

				return createBean(rs, type, props, columnToProperty);
			}
		}

		public <T> List<T> toBeanList(ResultSet rs, Class<T> type) throws SQLException {
			List<T> results = new ArrayList<T>();

			if (!rs.next()) {
				return results;
			}
			boolean atomicClass = isAtomicClass(type) || type.isArray();
			if (atomicClass) {
				do {
					results.add((T) getValue(rs, 1, type));
				} while (rs.next());

			} else {
				if (type.isAssignableFrom(Map.class)) {
					ResultSetMetaData rsmd = rs.getMetaData();
					int cols = rsmd.getColumnCount();
					List<String> columnNames = new ArrayList<String>();
					for (int col = 1; col <= cols; col++) {
						String columnName = rsmd.getColumnLabel(col);
						if ((null == columnName) || (0 == columnName.length())) {
							columnName = rsmd.getColumnName(col);
						}
						columnNames.add(columnName);
					}
					do {
						Map<String, Object> map = (Map<String, Object>) (newInstance(type));
						for (String columnName : columnNames) {
							map.put(columnName.toUpperCase(), rs.getObject(columnName));
						}
						results.add((T) map);
					} while (rs.next());

				} else {
					PropertyDescriptor[] props = propertyDescriptors(type);
					ResultSetMetaData rsmd = rs.getMetaData();
					int[] columnToProperty = mapColumnsToProperties(rsmd, props);
					do {
						results.add(createBean(rs, type, props, columnToProperty));
					} while (rs.next());

				}
			}

			return results;
		}

		private <T> T createBean(ResultSet rs, Class<T> type, PropertyDescriptor[] props, int[] columnToProperty)
				throws SQLException {
			T bean = newInstance(type);

			for (int i = 1; i < columnToProperty.length; i++) {
				if (columnToProperty[i] == -1) {
					continue;
				}
				PropertyDescriptor prop = props[columnToProperty[i]];
				Class propType = prop.getPropertyType();

				Object value = processColumn(rs, i, propType);

				if ((propType != null) && (value == null) && (propType.isPrimitive())) {
					value = primitiveDefaults.get(propType);
				}

				callSetter(bean, prop, value);
			}

			return bean;
		}

		private void callSetter(Object target, PropertyDescriptor prop, Object value) throws SQLException {
			Method setter = prop.getWriteMethod();

			if (setter == null) {
				return;
			}

			Class[] params = setter.getParameterTypes();
			try {
				if ((value != null) && ((value instanceof java.util.Date))) {
					if (params[0].getName().equals("java.sql.Date")) {
						value = new java.sql.Date(((java.util.Date) value).getTime());
					} else if (params[0].getName().equals("java.sql.Time")) {
						value = new Time(((java.util.Date) value).getTime());
					} else if (params[0].getName().equals("java.sql.Timestamp")) {
						value = new Timestamp(((java.util.Date) value).getTime());
					}

				}

				if (isCompatibleType(value, params[0]))
					setter.invoke(target, new Object[] { value });
				else {
					throw new SQLException("Cannot set " + prop.getName() + ": incompatible types.");
				}
			} catch (IllegalArgumentException e) {
				throw new SQLException("Cannot set " + prop.getName() + ": " + e.getMessage());
			} catch (IllegalAccessException e) {
				throw new SQLException("Cannot set " + prop.getName() + ": " + e.getMessage());
			} catch (InvocationTargetException e) {
				throw new SQLException("Cannot set " + prop.getName() + ": " + e.getMessage());
			}
		}

		private boolean isCompatibleType(Object value, Class<?> type) {
			if ((value == null) || (type.isInstance(value))) {
				return true;
			}
			if ((type.equals(Integer.TYPE)) && (Integer.class.isInstance(value))) {
				return true;
			}
			if ((type.equals(Long.TYPE)) && (Long.class.isInstance(value))) {
				return true;
			}
			if ((type.equals(Double.TYPE)) && (Double.class.isInstance(value))) {
				return true;
			}
			if ((type.equals(Float.TYPE)) && (Float.class.isInstance(value))) {
				return true;
			}
			if ((type.equals(Short.TYPE)) && (Short.class.isInstance(value))) {
				return true;
			}
			if ((type.equals(Byte.TYPE)) && (Byte.class.isInstance(value))) {
				return true;
			}
			if ((type.equals(Character.TYPE)) && (Character.class.isInstance(value))) {
				return true;
			}

			return (type.equals(Boolean.TYPE)) && (Boolean.class.isInstance(value));
		}

		private boolean isAtomicClass(Class<?> type) {
			if (type.equals(String.class)) {
				return true;
			}
			if ((type.equals(Integer.TYPE)) || (type.equals(Integer.class))) {
				return true;
			}
			if ((type.equals(Boolean.TYPE)) || (type.equals(Boolean.class))) {
				return true;
			}
			if ((type.equals(Long.TYPE)) || (type.equals(Long.class))) {
				return true;
			}
			if ((type.equals(Double.TYPE)) || (type.equals(Double.class))) {
				return true;
			}
			if ((type.equals(Float.TYPE)) || (type.equals(Float.class))) {
				return true;
			}
			if ((type.equals(Short.TYPE)) || (type.equals(Short.class))) {
				return true;
			}
			if ((type.equals(Byte.TYPE)) || (type.equals(Byte.class))) {
				return true;
			}
			if (type.equals(Timestamp.class)) {
				return true;
			}
			return false;
		}

		protected <T> T newInstance(Class<T> c) throws SQLException {
			try {
				return c.newInstance();
			} catch (InstantiationException e) {
				throw new SQLException("Cannot create " + c.getName() + ": " + e.getMessage());
			} catch (IllegalAccessException e) {
			}
			throw new SQLException("Cannot create " + c.getName() + " instance ");
		}

		private PropertyDescriptor[] propertyDescriptors(Class<?> c) throws SQLException {
			BeanInfo beanInfo = null;
			try {
				beanInfo = Introspector.getBeanInfo(c);
			} catch (IntrospectionException e) {
				throw new SQLException("Bean introspection failed: " + e.getMessage());
			}

			return beanInfo.getPropertyDescriptors();
		}

		protected int[] mapColumnsToProperties(ResultSetMetaData rsmd, PropertyDescriptor[] props) throws SQLException {
			int cols = rsmd.getColumnCount();
			int[] columnToProperty = new int[cols + 1];
			Arrays.fill(columnToProperty, -1);

			for (int col = 1; col <= cols; col++) {
				String columnName = rsmd.getColumnLabel(col);
				if ((null == columnName) || (0 == columnName.length())) {
					columnName = rsmd.getColumnName(col);
				}
				for (int i = 0; i < props.length; i++) {
					if (columnName.equalsIgnoreCase(props[i].getName())) {
						columnToProperty[col] = i;
						break;
					}
				}
			}

			return columnToProperty;
		}

		protected Object processColumn(ResultSet rs, int index, Class<?> propType) throws SQLException {
			if ((!propType.isPrimitive()) && (rs.getObject(index) == null)) {
				return null;
			}

			if ((!propType.isPrimitive()) && (rs.getObject(index) == null)) {
				return null;
			}

			return getValue(rs, index, propType);
		}

		private Object getValue(ResultSet rs, int index, Class<?> propType) throws SQLException {
			if (propType.equals(String.class)) {
				return rs.getString(index);
			}
			if ((propType.equals(Integer.TYPE)) || (propType.equals(Integer.class))) {
				return Integer.valueOf(rs.getInt(index));
			}
			if ((propType.equals(Boolean.TYPE)) || (propType.equals(Boolean.class))) {
				return Boolean.valueOf(rs.getBoolean(index));
			}
			if ((propType.equals(Long.TYPE)) || (propType.equals(Long.class))) {
				return Long.valueOf(rs.getLong(index));
			}
			if ((propType.equals(Double.TYPE)) || (propType.equals(Double.class))) {
				return Double.valueOf(rs.getDouble(index));
			}
			if ((propType.equals(Float.TYPE)) || (propType.equals(Float.class))) {
				return Float.valueOf(rs.getFloat(index));
			}
			if ((propType.equals(Short.TYPE)) || (propType.equals(Short.class))) {
				return Short.valueOf(rs.getShort(index));
			}
			if ((propType.equals(Byte.TYPE)) || (propType.equals(Byte.class))) {
				return Byte.valueOf(rs.getByte(index));
			}
			if (propType.equals(Timestamp.class)) {
				return rs.getTimestamp(index);
			}
			if (propType.equals(Date.class)) {
				return rs.getTimestamp(index);
			}
			return rs.getObject(index);
		}

		static {
			primitiveDefaults.put(Integer.TYPE, Integer.valueOf(0));
			primitiveDefaults.put(Float.TYPE, Float.valueOf(0.0F));
			primitiveDefaults.put(Double.TYPE, Double.valueOf(0.0D));
			primitiveDefaults.put(Long.TYPE, Long.valueOf(0L));
			primitiveDefaults.put(Boolean.TYPE, Boolean.FALSE);
			primitiveDefaults.put(Character.TYPE, Character.valueOf('\000'));
		}
	}

}
