package com.xinwei.common.lang;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
	public static final String EMPTY = "";
	public static final String LINE = "-";
	public static final String UNDER_LINE = "_";
	public static final String BLANK = " ";
	public static final String TAB = "\t";
	public static final String COMMA = ",";
	public static final String COLON = ":";
	public static final String DOT = ".";
	public static final String SEMICOLON = ";";
	public static final String QUESTION = "?";
	public static final String SINGLE_QUOTATION = "'";
	public static final String DOUBLE_QUOTATION = "\"";
	public static final String AMPERSAND = "&";
	public static final String PERCENT = "%";
	public static final String DOLLAR = "$";
	public static final String WAVE = "~";
	public static final String AT = "@";
	public static final String STAR = "*";
	public static final String EXCALMATORY = "!";
	public static final String BRACE_LEFT = "{";
	public static final String BRACE_RIGHT = "}";
	public static final String PARENTHESIS_LEFT = "(";
	public static final String PARENTHESIS_RIGHT = ")";
	public static final String BRACKET_LEFT = "[";
	public static final String BRACKET_RIGHT = "]";
	public static final String OBLIQUE_LINE = "/";
	public static final String SLASHE = "\\";
	public static final String LINE_SEPERATOR = "\n";
	public static final String LINE_SEPERATOR1 = "\r\n";
	public static final String CHARSET = "UTF-8";

	public static String formateTemplate(String template, Map map) {
		int INIT = -1;
		int i1 = 0;
		int i2 = INIT;
		int i3 = INIT;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < template.length(); i++) {
			char c = template.charAt(i);
			if (c == '{')
				i2 = i;
			else if ((c == '}') && (i2 > INIT)) {
				i3 = i;
			}
			if (i3 > INIT) {
				sb.append(template.substring(i1, i2));
				String key = template.substring(i2 + 1, i3).trim();
				Object value = map.get(key);
				if (value != null) {
					sb.append(value);
				}
				i1 = i3 + 1;
				i2 = INIT;
				i3 = INIT;
			}
		}
		if (i1 < template.length()) {
			sb.append(template.substring(i1, template.length()));
		}
		return sb.toString();
	}

	public static boolean isValidIP(String source) {
		String reg = "^(22[0-3]|2[0-1]\\d|1\\d{2}|[1-9]\\d|[1-9])\\.(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d|\\d)\\.(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d|\\d)\\.(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d|\\d)$";

		Pattern p = Pattern.compile(reg);
		Matcher m = p.matcher(source);
		return m.find();
	}

	public static boolean containSpecialChar(String source) {
		String reg = "[`~!@#$%^&*()+=\\\\|{}\\[\\]:;\"',.<>/?]";
		Pattern p = Pattern.compile(reg);
		Matcher m = p.matcher(source);
		return m.find();
	}

	public static boolean isNumber(String source) {
		if (source == null)
			return false;
		String reg = "^[\\d]+$";
		return Pattern.matches(reg, source);
	}

	public static boolean isEmpty(String str) {
		return (str == null) || (str.length() == 0);
	}

	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	public static boolean hasLength(String str) {
		return (str != null) && (str.length() > 0);
	}

	public static boolean isBlank(String str) {
		if (isEmpty(str))
			return true;
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isWhitespace(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public static boolean isNotBlank(String str) {
		return !isBlank(str);
	}

	public static String defaultString(String str) {
		return str == null ? "" : str;
	}

	public static String defaultString(String str, String defaultStr) {
		return str == null ? defaultStr : str;
	}

	public static String defaultIfBlank(String str, String defaultStr) {
		return isBlank(str) ? defaultStr : str;
	}

	public static String defaultIfEmpty(String str, String defaultStr) {
		return isEmpty(str) ? defaultStr : str;
	}

	public static String clean(String in) {
		String out = in;

		if (in != null) {
			out = in.trim();
			if (out.equals("")) {
				out = null;
			}
		}

		return out;
	}

	public static String join(String separator, Object... array) {
		return join(array, separator);
	}

	public static String join(Object[] array, String separator) {
		if (array == null) {
			return null;
		}
		if (array.length == 0) {
			return "";
		}

		separator = defaultString(separator);

		int bufSize = array.length;

		bufSize *= ((array[0] == null ? 16 : array[0].toString().length()) + separator.length());

		StringBuilder buf = new StringBuilder(bufSize);

		for (int i = 0; i < array.length; i++) {
			if (i > 0) {
				buf.append(separator);
			}
			if (array[i] != null) {
				buf.append(array[i]);
			}
		}
		return buf.toString();
	}

	public static String join(Collection<?> collection, String separator) {
		if ((collection == null) || (collection.isEmpty())) {
			return null;
		}
		return join(collection.toArray(), separator);
	}

	private static String REGEX = "\\{(.*?)\\}";

	public static String format(String str, Object... paramArrayOfObject) {
		if (paramArrayOfObject.length == 0)
			return str;

		Pattern p = Pattern.compile(REGEX);

		Matcher matcher = p.matcher(str);
		Set<String> keySet = new LinkedHashSet<String>();

		while (matcher.find()) {
			keySet.add(matcher.group(1));
		}

		Object[] array = keySet.toArray();

		int length = Math.min(array.length, paramArrayOfObject.length);

		for (int i = 0; i < length; i++) {
			str = str.replaceAll("\\{" + String.valueOf(array[i]) + "\\}", String.valueOf(paramArrayOfObject[i]));
		}

		return str;
	}

	public static String toString(Object obj) {
		if (obj == null) {
			return null;
		}
		String string = String.valueOf(obj);
		if (obj instanceof Number) {
			BigDecimal bd = new BigDecimal(string);
			string = bd.toPlainString();
		}
		return string;
	}

	public static String getString(byte[] bt) {
		return getString(bt, "UTF-8");
	}

	public static String getString(byte[] bt, String charset) {
		String ret = "";
		int strlen = 0;
		for (int i = 0; i < bt.length; i++) {
			if (bt[i] == 0)
				break;
			strlen++;
		}

		try {
			if (charset != null)
				ret = new String(bt, 0, strlen, charset).trim();
			else
				ret = new String(bt, 0, strlen).trim();
		} catch (Exception localException) {
		}
		return ret;
	}

	public static boolean equals(String s1, String s2) {
		if ((s1 == null) && (s2 == null)) {
			return true;
		}

		if (s1 == null) {
			return false;
		}

		return s1.equals(s2);
	}

	public static boolean notEquals(String s1, String s2) {
		return !equals(s1, s2);
	}

	/**
	 * 转义正则特殊字符 （$()*+.[]?\^{},|）
	 */
	public static String escapeExprSpecialWord(String keyword) {
		if (isNotBlank(keyword)) {
			String[] fbsArr = { "\\", "$", "(", ")", "*", "+", ".", "[", "]", "?", "^", "{", "}", "|" };
			for (String key : fbsArr) {
				if (keyword.contains(key)) {
					keyword = keyword.replace(key, "\\" + key);
				}
			}
		}
		return keyword;
	}

	public static List<String> split(String content, String[] keywords) {
		StringBuffer regExp = new StringBuffer("(");
		for (String keyword : keywords) {
			regExp.append(escapeExprSpecialWord(keyword)).append("|");
		}
		regExp.deleteCharAt(regExp.length() - 1).append(")");

		Matcher matcher = Pattern.compile(regExp.toString(), Pattern.CASE_INSENSITIVE).matcher(content);

		List<String> result = new ArrayList<String>();
		int start = 0;
		int end = 0;
		while (matcher.find()) {
			int start1 = matcher.start();
			int end1 = matcher.end();

			if (start < start1) {
				result.add(content.substring(start, start1));
			}
			result.add(matcher.group(1));

			start = start1;
			end = end1;
		}
		if (end < content.length()) {
			result.add(content.substring(end, content.length()));
		}
		return result;
	}
}