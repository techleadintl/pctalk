package com.xinwei.common.lang;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class GsonUtil {
	private static Gson gson = new Gson();

	public static <T> T fromJson(String content, Class<T> bean) {
		return fromJson(gson, content, bean);
	}

	public static <T> T fromJson(Gson gson, String content, Class<T> bean) {
		try {
			return gson.fromJson(content, bean);
		} catch (Exception ex) {
//			ex.printStackTrace();
			return null;
		}
	}

	public static <T> T fromJson(String content, Type bean) throws JsonSyntaxException {
		return gson.fromJson(content, bean);
	}

	public static String toJson(Object value) {
		if (value == null)
			return null;
		String json = gson.toJson(value);
		return json;
	}
}