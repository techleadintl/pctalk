/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月12日 上午11:02:35
 * 
 ***************************************************************/
package com.xinwei.common.lang;

import java.io.File;

public class FileUtil {
	public static synchronized File initDirectory(File directoryHome, String... directoryNames) {
		File targetDir = null;
		for (String directoryName : directoryNames) {
			targetDir = new File(directoryHome, directoryName).getAbsoluteFile();
			if (!targetDir.exists()) {
				targetDir.mkdirs();
			}
			directoryHome = targetDir;
		}
		return targetDir;
	}

	public static String getFileName(String path) {
		if (path == null)
			return null;
		int pos1 = path.lastIndexOf('/');
		int pos2 = path.lastIndexOf('\\');
		int pos = Math.max(pos1, pos2);
		if (pos < 0)
			return path;
		else
			return path.substring(pos + 1);
	}

}
