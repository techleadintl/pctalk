package com.xinwei.common.lang;

import java.util.ArrayList;
import java.util.List;

import opensource.jpinyin.PinyinFormat;
import opensource.jpinyin.PinyinHelper;

public class PinyinUtil {

	public static String convertWithTone(String s) {
		return PinyinHelper.convertToPinyinString(s, " ", PinyinFormat.WITH_TONE_MARK);
	}

	public static String convertWithoutTone(String s) {
		return PinyinHelper.convertToPinyinString(s, " ", PinyinFormat.WITHOUT_TONE);
	}

	public static String[] convertWithToneArray(char c) {
		return PinyinHelper.convertToPinyinArray(c, PinyinFormat.WITH_TONE_MARK);
	}

	public static String[] convertWithoutToneArray(char c) {
		return PinyinHelper.convertToPinyinArray(c, PinyinFormat.WITHOUT_TONE);
	}

	public static List<String> getSearchKeys(String displayName, String... others) {
		List<String> searchKeys = new ArrayList<String>();
		
		searchKeys.add(displayName.toLowerCase());

		String pinyin = convertWithoutTone(displayName);
		for (String other : others) {
			if(other!=null){
				searchKeys.add(other.toLowerCase());//原来的字符串
			}
		}

		if (pinyin != null && pinyin.length() > 0) {
			StringBuilder sb = new StringBuilder();
			StringBuilder captionsb = new StringBuilder();

			pinyin = pinyin.toLowerCase();

			String[] strs = pinyin.split(" ");
			for (String s : strs) {
				if (StringUtil.isEmpty(s))
					continue;
				sb.append(s);
				char c = s.charAt(0);
				if (c >= 'a' && c <= 'z') {
					captionsb.append(c);
				}
			}

			searchKeys.add(pinyin);//带间隔符的字符串
			searchKeys.add(sb.toString());//不带间隔符的字符串
			searchKeys.add(captionsb.toString());//各个汉字首字符的字符串
		}

		return searchKeys;
	}

	public static boolean isMatch(String text, String displayName, String... others) {
		List<String> searchKeys = getSearchKeys(displayName, others);
		text = text.toLowerCase();
		for (String searchKey : searchKeys) {
			if (searchKey.contains(text)) {
				return true;
			}
		}
		return false;
	}

}