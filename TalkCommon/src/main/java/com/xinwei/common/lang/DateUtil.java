package com.xinwei.common.lang;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	public static final String PATTERN_DEFAULT = "yyyy-MM-dd HH:mm:ss";
	public static final String PATTERN_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

	public static final String PATTERN_YYYY_MM_DD = "yyyy-MM-dd";

	public static long getTodayZone() {
		String pattern = "yyyy-MM-dd";
		String str = format(new Date(), pattern);
		return parse(str, pattern).getTime();
	}

	public static long getTime() {
		return getTime(getDate());
	}

	public static long getTime(String str) {
		return getTime(str, "yyyy-MM-dd HH:mm:ss");
	}

	public static long getTime(String str, String pattern) {
		try {
			return getTime(getDate(str, pattern));
		} catch (Exception localException) {
		}
		return 0L;
	}

	public static long getTime(Date date) {
		if (date == null) {
			return 0L;
		}
		return date.getTime();
	}

	public static Date getDate(Date date, String pattern) {
		String str = getString(date, pattern);
		return getDate(str, pattern);
	}

	public static String getString() {
		return getString(getDate(), "yyyy-MM-dd HH:mm:ss");
	}

	public static String getString(Date date) {
		return getString(date, "yyyy-MM-dd HH:mm:ss");
	}

	public static String getString(long time) {
		return getString(getDate(time), "yyyy-MM-dd HH:mm:ss");
	}

	public static String getStringyyyyMMddHHmmss(Date date) {
		return getString(date, "yyyyMMddHHmmss");
	}

	public static String getString(Date date, String pattern) {
		if (date == null) {
			return "";
		}
		return format(date, pattern);
	}

	public static Date getDate() {
		return new Date();
	}

	public static Date getDate(String str, String pattern) {
		return parse(str, pattern);
	}

	public static Date getDate(long time) {
		Date date = new Date(time);
		return date;
	}

	public static Date getDate(String str) {
		try {
			long valueOf = Long.parseLong(str);
			return getDate(valueOf);
		} catch (Exception ex) {
			return null;
		}
	}

	public static String format(Date date, String pattern) {
		if (pattern == null)
			pattern = "yyyy-MM-dd HH:mm:ss";
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			return dateFormat.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static Date parse(String str, String pattern) {
		if ((str == null) || (str.equals(""))) {
			return getDate(0L);
		}

		Date date = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			date = dateFormat.parse(str);
		} catch (ParseException e) {
			date = getDate(0L);
			e.printStackTrace();
		}
		return date;
	}

	public static void main(String[] args) {
		System.out.println(parse(null, null));
	}
}