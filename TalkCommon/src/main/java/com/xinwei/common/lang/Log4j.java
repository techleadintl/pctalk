package com.xinwei.common.lang;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.SimpleLayout;

public class Log4j {
	private static final Logger LOGGER = Logger.getLogger(Log4j.class);
	private static String maxFileSize = "50MB";
	private static String defaultLogPath = System.getProperty("user.home") + "/AppData/Local/CamTalk/log";

	public void setLocation(String location) {
		defaultLogPath = location;
	}

	public static Logger getLogger(Class<?> clazz) {
		if (clazz == null)
			return getLogger("");
		return Logger.getLogger(clazz);
	}

	public static Logger getLogger() {
		return getLogger("");
	}

	public static Logger getLogger(String logName) {
		return getLogger(logName, null);
	}

	public static synchronized Logger getLogger(String logName, String logPath) {
		if ((logName == null) || ("".equals(logName)))
			logName = "log4j";
		return newLogger(logName, logPath);
	}

	public static void debug(Logger logger, String s) {
		if (logger.isDebugEnabled())
			logger.debug(s);
	}

	public static void debug(Logger logger, String s, Throwable cause) {
		if (logger.isDebugEnabled())
			logger.debug(s, cause);
	}

	public static void info(Logger logger, String s) {
		if (logger.isInfoEnabled())
			logger.info(s);
	}

	public static void info(Logger logger, String s, Throwable cause) {
		if (logger.isInfoEnabled())
			logger.info(s, cause);
	}

	public static void warn(Logger logger, String s) {
		logger.warn(s);
	}

	public static void warn(Logger logger, String s, Throwable cause) {
		logger.warn(s, cause);
	}

	public static void error(Logger logger, String s) {
		logger.error(s);
	}

	public static void error(Logger logger, String s, Throwable cause) {
		logger.error(s, cause);
	}

	public static void debug(String s) {
		debug("debug", s);
	}

	public static void debug(String logName, String s) {
		Logger logger = getLogger(logName);
		if (logger.isDebugEnabled())
			logger.debug(s);
	}

	public static void debug(String logName, String s, Throwable cause) {
		Logger logger = getLogger(logName);
		if (logger.isDebugEnabled())
			logger.debug(s, cause);
	}

	public static void info(String s) {
		info("info", s);
	}

	public static void info(String logName, String s) {
		Logger logger = getLogger(logName);
		if (logger.isInfoEnabled())
			logger.info(s);
	}

	public static void info(String logName, String s, Throwable cause) {
		Logger logger = getLogger(logName);
		if (logger.isInfoEnabled())
			logger.info(s, cause);
	}

	public static void warn(String s) {
		warn("warn", s);
	}

	public static void warn(String s, Throwable cause) {
		warn("warn", s, cause);
	}

	public static void warn(String logName, String s) {
		Logger logger = getLogger(logName);
		logger.warn(s);
	}

	public static void warn(String logName, String s, Throwable cause) {
		Logger logger = getLogger(logName);
		logger.warn(s, cause);
	}

	public static void error(String s) {
		Logger logger = getLogger("error");
		logger.error(s);
	}

	public static void error(String logName, String s) {
		Logger logger = getLogger(logName);
		logger.error(s);
	}

	public static void error(Throwable cause) {
		Logger logger = getLogger("error");
		logger.error(getStackTrace(cause));
	}

	public static void error(String s, Throwable cause) {
		Logger logger = getLogger("error");
		logger.error(s + ":" + getStackTrace(cause));
	}

	public static void error(Object s, Throwable cause) {
		Logger logger = getLogger("error");
		logger.error(s + ":" + getStackTrace(cause));
	}

	public static void error(String logName, String s, Throwable cause) {
		Logger logger = getLogger(logName);
		logger.error(s, cause);
	}

	public static void debug(String logName, String s, boolean notPrint) {
		if (notPrint)
			return;
		Logger logger = getLogger(logName);
		if (logger.isDebugEnabled())
			logger.debug(s);
	}

	public static void debug(String logName, String s, Throwable cause, boolean notPrint) {
		if (notPrint)
			return;
		Logger logger = getLogger(logName);
		if (logger.isDebugEnabled())
			logger.debug(s, cause);
	}

	public static void info(String logName, String s, boolean notPrint) {
		if (notPrint)
			return;
		Logger logger = getLogger(logName);
		if (logger.isInfoEnabled())
			logger.info(s);
	}

	public static void info(String logName, String s, Throwable cause, boolean notPrint) {
		if (notPrint)
			return;
		Logger logger = getLogger(logName);
		if (logger.isInfoEnabled())
			logger.info(s, cause);
	}

	public static void warn(String logName, String s, boolean notPrint) {
		if (notPrint)
			return;
		Logger logger = getLogger(logName);
		logger.warn(s);
	}

	public static void warn(String logName, String s, Throwable cause, boolean notPrint) {
		if (notPrint)
			return;
		Logger logger = getLogger(logName);
		logger.warn(s, cause);
	}

	public static void error(String logName, String s, boolean notPrint) {
		if (notPrint)
			return;
		Logger logger = getLogger(logName);
		logger.error(s);
	}

	public static void error(String logName, String s, Throwable cause, boolean notPrint) {
		if (notPrint)
			return;
		Logger logger = getLogger(logName);
		logger.error(s, cause);
	}

	public static void debug(Class<?> clazz, String s) {
		Logger logger = getLogger(clazz);
		if (logger.isDebugEnabled())
			logger.debug(s);
	}

	public static void debug(Class<?> clazz, String s, Throwable cause) {
		Logger logger = getLogger(clazz);
		if (logger.isDebugEnabled())
			logger.debug(s, cause);
	}

	public static void info(Class<?> clazz, String s) {
		Logger logger = getLogger(clazz);
		if (logger.isInfoEnabled())
			logger.info(s);
	}

	public static void info(Class<?> clazz, String s, Throwable cause) {
		Logger logger = getLogger(clazz);
		if (logger.isInfoEnabled())
			logger.info(s, cause);
	}

	public static void warn(Class<?> clazz, String s) {
		Logger logger = getLogger(clazz);
		logger.warn(s);
	}

	public static void warn(Class<?> clazz, String s, Throwable cause) {
		Logger logger = getLogger(clazz);
		logger.warn(s, cause);
	}

	public static void error(Class<?> clazz, String s) {
		Logger logger = getLogger(clazz);
		logger.error(s);
	}

	public static void error(Class<?> clazz, String s, Throwable cause) {
		Logger logger = getLogger(clazz);
		logger.error(s, cause);
	}

	private static Logger newLogger(String logName, String logPath) {
		return newRollingFileLogger(logName, logPath);
	}

	public static Logger newRollingFileLogger(String logName, String logPath) {
		Logger logger = Logger.exists(logName);
		if (logger != null)
			return logger;
		synchronized (LOGGER) {
			logger = Logger.getLogger(logName);
			RollingFileAppender rollingFileAppender = new RollingFileAppender();
			String log_Path = getLoggerFilePathWithFileName(logPath, logName);
			rollingFileAppender.setFile(log_Path);
			rollingFileAppender.setAppend(true);
			rollingFileAppender.setMaxFileSize(maxFileSize);
			rollingFileAppender.setMaxBackupIndex(10);
			rollingFileAppender.setLayout(new PatternLayout("%d{yyyy-MM-dd HH:mm:ss SSS} [%t] %-5p %c - %m%n"));

			rollingFileAppender.setImmediateFlush(true);
			rollingFileAppender.activateOptions();
			logger.addAppender(rollingFileAppender);

			ConsoleAppender consoleAppender = new ConsoleAppender(new SimpleLayout());
			logger.addAppender(consoleAppender);
			return logger;
		}
	}

	public static Logger newDailyRollingFileLogger(String logName, String logPath) {
		Logger logger = Logger.exists(logName);
		if (logger != null)
			return logger;
		synchronized (LOGGER) {
			logger = Logger.getLogger(logName);
			String log_Path = getLoggerFilePathWithFileName(logPath, logName);
			PatternLayout patternLayout = new PatternLayout("[%d] %r %-5p -- %m%n");

			DailyRollingFileAppender rollingFileAppender = null;
			try {
				rollingFileAppender = new DailyRollingFileAppender(patternLayout, log_Path, "yyyy-MM-dd");
			} catch (IOException e) {
				e.printStackTrace();
			}
			rollingFileAppender.setAppend(true);
			rollingFileAppender.setImmediateFlush(true);
			rollingFileAppender.activateOptions();
			logger.addAppender(rollingFileAppender);

			ConsoleAppender consoleAppender = new ConsoleAppender(new SimpleLayout());
			logger.addAppender(consoleAppender);
			return logger;
		}
	}

	private static String getLoggerFilePathWithFileName(String logPath, String logName) {
		if ((logPath == null) || ("".equals(logPath)))
			logPath = defaultLogPath;
		File directory = new File(logPath);
		try {
			forceMkdir(directory);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return logPath + File.separator + logName + ".log";
	}

	private static void forceMkdir(File directory) throws IOException {
		if (directory.exists()) {
			if (directory.isFile()) {
				String message = "File " + directory + " exists and is " + "not a directory. Unable to create directory.";
				throw new IOException(message);
			}
		} else if (!directory.mkdirs()) {
			String message = "Unable to create directory " + directory;
			throw new IOException(message);
		}
	}

	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}
}