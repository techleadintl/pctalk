package com.xinwei.common.lang;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

import com.xinwei.common.lookandfeel.util.ImageUtil;

public class McWillImage {
	private BufferedImage image;
	private String format;
	private String url;
	private ImageIcon imageicon;
	byte[] bytes;

	public McWillImage(byte[] bytes) {
		if (bytes == null)
			return;
		this.bytes = bytes;
		this.image = ImageUtil.toBufferedImage(bytes);
		this.format = ImageUtil.getImageFormat(bytes);
	}

	public McWillImage(BufferedImage bufferedImage, String format) {
		this.image = bufferedImage;
		this.format = format;
		this.bytes = ImageUtil.toBytes(bufferedImage, format);
	}

	public byte[] getBytes() {
		return bytes;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public ImageIcon getImageIcon() {
		if (imageicon == null && image != null) {
			imageicon = new ImageIcon(image);
		}
		return imageicon;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
