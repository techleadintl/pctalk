package com.xinwei.common.lang;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CollectionUtil {
	public static boolean arrayContentsEq(Object[] a1, Object[] a2) {
		if (a1 == null) {
			return (a2 == null) || (a2.length == 0);
		}

		if (a2 == null) {
			return a1.length == 0;
		}

		if (a1.length != a2.length) {
			return false;
		}

		for (int i = 0; i < a1.length; i++) {
			if (a1[i] != a2[i]) {
				return false;
			}
		}

		return true;
	}

	public static boolean isEmpty(Collection<?> c) {
		return (c == null) || (c.isEmpty());
	}

	public static boolean isNotEmpty(Collection<?> c) {
		return !isEmpty(c);
	}

	public static boolean isEmpty(Map<?, ?> c) {
		return (c == null) || (c.isEmpty());
	}

	public static boolean isNotEmpty(Map<?, ?> c) {
		return !isEmpty(c);
	}

	public static boolean isEmpty(Object[] c) {
		return (c == null) || (c.length == 0);
	}

	public static boolean isNotEmpty(Object[] c) {
		return !isEmpty(c);
	}

	public static <T> List<T> union(Collection<T> c1, Collection<T> c2) {
		List result = new ArrayList();
		if (isNotEmpty(c1)) {
			result.addAll(c1);
		}
		if (isNotEmpty(c2)) {
			result.addAll(c2);
		}
		return result;
	}

	public static <T> List<T> retainAll(Collection<T> collection, Collection<T> retain) {
		if ((isEmpty(collection)) || (isEmpty(retain))) {
			return new ArrayList(0);
		}
		List list = new ArrayList(Math.min(collection.size(), retain.size()));

		for (Iterator iter = collection.iterator(); iter.hasNext();) {
			Object obj = iter.next();
			if (retain.contains(obj)) {
				list.add(obj);
			}
		}
		return list;
	}

	public static <T> List<T> removeAll(Collection<T> collection, Collection<T> remove) {
		if (isEmpty(collection))
			return new ArrayList(0);
		if (isEmpty(remove)) {
			return new ArrayList(collection);
		}
		List list = new ArrayList();
		for (Iterator iter = collection.iterator(); iter.hasNext();) {
			Object obj = iter.next();
			if (!remove.contains(obj)) {
				list.add(obj);
			}
		}
		return list;
	}

	public static <T> List<T>[] analyse(Collection<T> oldC, Collection<T> newC) {
		return new List[] { removeAll(newC, oldC), removeAll(oldC, newC), retainAll(newC, oldC) };
	}

	public static <T> List<T>[] difference(Collection<T> oldC, Collection<T> newC) {
		return new List[] { removeAll(newC, oldC), removeAll(oldC, newC) };
	}

	public static <T> Map<T, T> equals(Collection<T> collection, Collection<T> retain) {
		if ((isEmpty(collection)) || (isEmpty(retain))) {
			return new HashMap<T, T>();
		}
		List<T> retainList = new ArrayList<T>(retain);
		Map<T, T> list = new HashMap<T, T>();

		for (Iterator<T> iter = collection.iterator(); iter.hasNext();) {
			T obj = iter.next();
			int index = retainList.indexOf(obj);
			if (index != -1) {
				list.put(obj, retainList.get(index));
			}
		}
		return list;
	}
}