/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月31日 下午3:58:46
 * 
 ***************************************************************/
package com.xinwei.common.lang;

public interface Callback {
	public void call(Object... objects);
}
