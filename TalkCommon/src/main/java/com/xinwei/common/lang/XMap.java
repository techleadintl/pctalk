package com.xinwei.common.lang;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class XMap implements Map, Serializable {
	private Map attributeMap;

	public XMap() {
		this(null);
	}

	public XMap(Map attributeMap) {
		if (attributeMap == null)
			attributeMap = new HashMap(0);
		this.attributeMap = attributeMap;
	}

	public <T> T getObject(Object key) {
		return (T) getObject(attributeMap, key);
	}

	public String getString(Object key) {
		return getString(this.attributeMap, key);
	}

	public Boolean getBoolean(Object key) {
		return getBoolean(this.attributeMap, key);
	}

	public Number getNumber(Object key) {
		return getNumber(this.attributeMap, key);
	}

	public Integer getInteger(Object key) {
		return getInteger(this.attributeMap, key);
	}

	public Long getLong(Object key) {
		return getLong(this.attributeMap, key);
	}

	public Float getFloat(Object key) {
		return getFloat(this.attributeMap, key);
	}

	public Double getDouble(Object key) {
		return getDouble(this.attributeMap, key);
	}

	public Byte getByte(Object key) {
		return getByte(this.attributeMap, key);
	}

	public Byte getByte(Object key, byte defaultValue) {
		return getByte(this.attributeMap, key, Byte.valueOf(defaultValue));
	}

	public Map getMap(Object key) {
		return getMap(this.attributeMap, key);
	}

	public List getList(Object key) {
		return getList(this.attributeMap, key);
	}

	public Set getSet(Object key) {
		return getSet(this.attributeMap, key);
	}

	public XMap getProperties(Object key) {
		Object answer = this.attributeMap.get(key);
		if ((answer instanceof Map)) {
			return new XMap((Map) answer);
		}
		if ((answer instanceof XMap)) {
			return (XMap) answer;
		}
		return null;
	}

	public Object getObject(Object key, Object defaultValue) {
		return getObject(this.attributeMap, key, defaultValue);
	}

	public String getString(Object key, String defaultValue) {
		return getString(this.attributeMap, key, defaultValue);
	}

	public Boolean getBoolean(Object key, Boolean defaultValue) {
		return getBoolean(this.attributeMap, key, defaultValue);
	}

	public Number getNumber(Object key, Number defaultValue) {
		return getNumber(this.attributeMap, key, defaultValue);
	}

	public Integer getInteger(Object key, Integer defaultValue) {
		return getInteger(this.attributeMap, key, defaultValue);
	}

	public Long getLong(Object key, Long defaultValue) {
		return getLong(this.attributeMap, key, defaultValue);
	}

	public Float getFloat(Object key, Float defaultValue) {
		return getFloat(this.attributeMap, key, defaultValue);
	}

	public Double getDouble(Object key, Double defaultValue) {
		return getDouble(this.attributeMap, key, defaultValue);
	}

	public static boolean isEmpty(Map map) {
		return (map == null) || (map.isEmpty());
	}

	public static boolean isNotEmpty(Map map) {
		return !isEmpty(map);
	}

	public static Object getObject(Map map, Object key) {
		if (map != null) {
			return map.get(key);
		}
		return null;
	}

	public static String getString(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				String string = String.valueOf(answer);
				if (answer instanceof Number) {
					BigDecimal bd = new BigDecimal(string);
					string = bd.toPlainString();
				}
				if (string.equals("null")) {
					string = null;
				}
				return string;
			}
		}
		return null;
	}

	public static Boolean getBoolean(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				if ((answer instanceof Boolean)) {
					return (Boolean) answer;
				}
				if ((answer instanceof String)) {
					return new Boolean((String) answer);
				}
				if ((answer instanceof Number)) {
					Number n = (Number) answer;
					return n.intValue() != 0 ? Boolean.TRUE : Boolean.FALSE;
				}
			}
		}
		return null;
	}

	public static Number getNumber(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				if ((answer instanceof Number)) {
					return (Number) answer;
				}
				if ((answer instanceof String))
					try {
						String text = (String) answer;
						return NumberFormat.getInstance().parse(text);
					} catch (ParseException localParseException) {
					}
			}
		}
		return null;
	}

	public static Byte getByte(Map map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Byte)) {
			return (Byte) answer;
		}
		return new Byte(answer.byteValue());
	}

	public static Short getShort(Map map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Short)) {
			return (Short) answer;
		}
		return new Short(answer.shortValue());
	}

	public static Integer getInteger(Map map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Integer)) {
			return (Integer) answer;
		}
		return new Integer(answer.intValue());
	}

	public static Long getLong(Map map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Long)) {
			return (Long) answer;
		}
		return new Long(answer.longValue());
	}

	public static Float getFloat(Map map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Float)) {
			return (Float) answer;
		}
		return new Float(answer.floatValue());
	}

	public static Double getDouble(Map map, Object key) {
		Number answer = getNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Double)) {
			return (Double) answer;
		}
		return new Double(answer.doubleValue());
	}

	public static Map getMap(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if ((answer != null) && ((answer instanceof Map))) {
				return (Map) answer;
			}
		}
		return null;
	}

	public static List getList(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if ((answer != null) && ((answer instanceof List))) {
				return (List) answer;
			}
		}
		return null;
	}

	public static Object[] getArray(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if ((answer != null) && ((answer.getClass().isArray()))) {
				return (Object[]) answer;
			}
		}
		return null;
	}

	public static Set getSet(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if ((answer != null) && ((answer instanceof Set))) {
				return (Set) answer;
			}
		}
		return null;
	}

	public static Object getObject(Map map, Object key, Object defaultValue) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				return answer;
			}
		}
		return defaultValue;
	}

	public static String getString(Map map, Object key, String defaultValue) {
		String answer = getString(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Boolean getBoolean(Map map, Object key, Boolean defaultValue) {
		Boolean answer = getBoolean(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Number getNumber(Map map, Object key, Number defaultValue) {
		Number answer = getNumber(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Byte getByte(Map map, Object key, Byte defaultValue) {
		Byte answer = getByte(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Short getShort(Map map, Object key, Short defaultValue) {
		Short answer = getShort(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Integer getInteger(Map map, Object key, Integer defaultValue) {
		Integer answer = getInteger(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Long getLong(Map map, Object key, Long defaultValue) {
		Long answer = getLong(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Float getFloat(Map map, Object key, Float defaultValue) {
		Float answer = getFloat(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Double getDouble(Map map, Object key, Double defaultValue) {
		Double answer = getDouble(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Map getMap(Map map, Object key, Map defaultValue) {
		Map answer = getMap(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static List getList(Map map, Object key, List defaultValue) {
		List answer = getList(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return null;
	}

	public static Set getSet(Map map, Object key, Set defaultValue) {
		Set answer = getSet(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return null;
	}

	public static boolean getBooleanValue(Map map, Object key) {
		Boolean booleanObject = getBoolean(map, key);
		if (booleanObject == null) {
			return false;
		}
		return booleanObject.booleanValue();
	}

	public static byte getByteValue(Map map, Object key) {
		Byte byteObject = getByte(map, key);
		if (byteObject == null) {
			return 0;
		}
		return byteObject.byteValue();
	}

	public static short getShortValue(Map map, Object key) {
		Short shortObject = getShort(map, key);
		if (shortObject == null) {
			return 0;
		}
		return shortObject.shortValue();
	}

	public static int getIntValue(Map map, Object key) {
		Integer integerObject = getInteger(map, key);
		if (integerObject == null) {
			return 0;
		}
		return integerObject.intValue();
	}

	public static long getLongValue(Map map, Object key) {
		Long longObject = getLong(map, key);
		if (longObject == null) {
			return 0L;
		}
		return longObject.longValue();
	}

	public static float getFloatValue(Map map, Object key) {
		Float floatObject = getFloat(map, key);
		if (floatObject == null) {
			return 0.0F;
		}
		return floatObject.floatValue();
	}

	public static double getDoubleValue(Map map, Object key) {
		Double doubleObject = getDouble(map, key);
		if (doubleObject == null) {
			return 0.0D;
		}
		return doubleObject.doubleValue();
	}

	public static boolean getBooleanValue(Map map, Object key, boolean defaultValue) {
		Boolean booleanObject = getBoolean(map, key);
		if (booleanObject == null) {
			return defaultValue;
		}
		return booleanObject.booleanValue();
	}

	public static byte getByteValue(Map map, Object key, byte defaultValue) {
		Byte byteObject = getByte(map, key);
		if (byteObject == null) {
			return defaultValue;
		}
		return byteObject.byteValue();
	}

	public static short getShortValue(Map map, Object key, short defaultValue) {
		Short shortObject = getShort(map, key);
		if (shortObject == null) {
			return defaultValue;
		}
		return shortObject.shortValue();
	}

	public static int getIntValue(Map map, Object key, int defaultValue) {
		Integer integerObject = getInteger(map, key);
		if (integerObject == null) {
			return defaultValue;
		}
		return integerObject.intValue();
	}

	public static long getLongValue(Map map, Object key, long defaultValue) {
		Long longObject = getLong(map, key);
		if (longObject == null) {
			return defaultValue;
		}
		return longObject.longValue();
	}

	public static float getFloatValue(Map map, Object key, float defaultValue) {
		Float floatObject = getFloat(map, key);
		if (floatObject == null) {
			return defaultValue;
		}
		return floatObject.floatValue();
	}

	public static double getDoubleValue(Map map, Object key, double defaultValue) {
		Double doubleObject = getDouble(map, key);
		if (doubleObject == null) {
			return defaultValue;
		}
		return doubleObject.doubleValue();
	}

	public Properties toProperties(Map map) {
		Properties answer = new Properties();

		if (map != null) {
			for (Iterator iter = map.entrySet().iterator(); iter.hasNext();) {
				Map.Entry entry = (Map.Entry) iter.next();
				Object key = entry.getKey();
				Object value = entry.getValue();
				answer.put(key, value);
			}
		}
		return answer;
	}

	public boolean containsKey(Object key) {
		return this.attributeMap.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return this.attributeMap.containsValue(value);
	}

	public Object get(Object key) {
		return this.attributeMap.get(key);
	}

	public Object remove(Object key) {
		return this.attributeMap.remove(key);
	}

	public void putAll(Map m) {
		if (m != null)
			this.attributeMap.putAll(m);
	}

	public void clear() {
		this.attributeMap.clear();
	}

	public Set entrySet() {
		return this.attributeMap.entrySet();
	}

	public Set keySet() {
		return this.attributeMap.keySet();
	}

	public Collection values() {
		return this.attributeMap.values();
	}

	public int size() {
		return this.attributeMap.size();
	}

	public boolean isEmpty() {
		return isEmpty(this.attributeMap);
	}

	public boolean isNotEmpty() {
		return isNotEmpty(this.attributeMap);
	}

	public Object put(Object key, Object value) {
		return this.attributeMap.put(key, value);
	}
}