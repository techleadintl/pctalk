/*
 * Copyright 2010 Beijing Xinwei, Inc. All rights reserved.
 * History:
 * -----------------------------------------------------------------------------
 * -
 * Date | Who | What
 * 2015-2-3 | duanbokan | create the file
 */

package com.xinwei.common.lang;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.xinwei.common.codec.ApacheBase64;

/**
 * DES对称加密解密
 * <p>
 * <p>
 * 类详细描述
 * </p>
 *
 * @author duanbokan
 */
public class DESCoderUtil {
    private static String encoding = "UTF-8";
    // 密钥
    private static String sKey = "!@#xw$&*";
    private static byte[] iv = {1, 2, 3, 4, 5, 6, 7, 8};

    public static String encryptToStr(String str) throws Exception {
        IvParameterSpec zeroIv = new IvParameterSpec(iv);
        SecretKeySpec key = new SecretKeySpec(sKey.getBytes(), "DES");
        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);
        byte[] encodeByte = cipher.doFinal(str.getBytes());
        return ApacheBase64.encodeBase64String(encodeByte);
    }

    public static String decryptStr(String str) {
        try {
            //先进行Base64解码
            byte[] encodeByte = ApacheBase64.decodeBase64(str);
            IvParameterSpec zeroIv = new IvParameterSpec(iv);
            SecretKeySpec key = new SecretKeySpec(sKey.getBytes(), "DES");
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key, zeroIv);
            byte[] decodeByte = cipher.doFinal(encodeByte);
            return new String(decodeByte, encoding);
        } catch (Exception e) {
            System.out.println("exception:" + e.toString());
        }
        return "";
    }
}
