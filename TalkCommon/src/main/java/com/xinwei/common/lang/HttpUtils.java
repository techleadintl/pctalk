package com.xinwei.common.lang;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EntityUtils;

import com.xinwei.http.listener.DownloadListener;

/**
 * 依赖的jar包有：commons-lang、httpclient、httpcore、commons-io
 *
 */
public class HttpUtils {

	public static final String charset = "UTF-8";

	private static PoolingHttpClientConnectionManager connMgr;
	private static RequestConfig requestConfig;
	private static final int MAX_TIMEOUT = 10000;

	private static Map<String, String> headers = new HashMap<String, String>();

	static {
		// 设置连接池
		connMgr = new PoolingHttpClientConnectionManager();
		// 设置连接池大小
		connMgr.setMaxTotal(100);
		connMgr.setDefaultMaxPerRoute(connMgr.getMaxTotal());

		RequestConfig.Builder configBuilder = RequestConfig.custom();
		// 设置连接超时
		configBuilder.setConnectTimeout(MAX_TIMEOUT);
		// 设置读取超时
		configBuilder.setSocketTimeout(MAX_TIMEOUT);
		// 设置从连接池获取连接实例的超时
		configBuilder.setConnectionRequestTimeout(MAX_TIMEOUT);
		// 在提交请求之前 测试连接是否可用
		configBuilder.setStaleConnectionCheckEnabled(true);
		requestConfig = configBuilder.build();
	}

	public static String post(String url, Map<String, String> params) throws Exception {
		return post(url, params, headers);
	}

	public static String post(String url, Map<String, String> params, Map<String, String> headers) throws Exception {
		HttpClient client = null;
		HttpPost post = new HttpPost(url);
		try {
			post.setConfig(requestConfig);
			if (params != null && !params.isEmpty()) {
				List<NameValuePair> formParams = new ArrayList<org.apache.http.NameValuePair>();
				Set<Entry<String, String>> entrySet = params.entrySet();
				for (Entry<String, String> entry : entrySet) {
					formParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
				}
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, charset);
				post.setEntity(entity);
			}
			addHeads(post);
			addHeads(headers, post);

			client = getHttpClient(url);

			HttpResponse res = client.execute(post);

			return EntityUtils.toString(res.getEntity(), charset);

		} finally {
			colse(url, client, post);
		}
	}

	public static String post(String url, String body) throws Exception {
		return post(url, body, null);
	}

	public static String post(String url, String body, String mimeType) throws Exception {
		HttpClient client = null;
		HttpPost post = new HttpPost(url);
		try {
			addHeads(post);
			post.setConfig(requestConfig);
			if (body != null) {
				HttpEntity entity = null;

				if (mimeType != null) {
					entity = new StringEntity(body, ContentType.create(mimeType, charset));
				} else {
					entity = new StringEntity(body, charset);
				}

				post.setEntity(entity);
			}

			client = getHttpClient(url);

			HttpResponse res = client.execute(post);

			return EntityUtils.toString(res.getEntity(), charset);
		} finally {
			colse(url, client, post);
		}
	}

	/**
	 * 发送一个 GET 请求
	 * 
	 */
	public static String get(String url) throws Exception {
		HttpClient client = null;
		HttpRequestBase get = new HttpGet(url);
		try {
			addHeads(get);
			// 设置参数
			get.setConfig(requestConfig);

			client = getHttpClient(url);

			HttpResponse res = client.execute(get);

			return EntityUtils.toString(res.getEntity(), charset);
		} finally {
			colse(url, client, get);
		}
	}

	/**
	 * 下载
	 */
	public static byte[] download(String url) throws Exception {
		return download(url, null);
	}

	public static byte[] download(String url, DownloadListener listener) throws Exception {
		if (StringUtil.isEmpty(url)) {
			return null;
		}
		HttpClient client = null;
		HttpRequestBase get = new HttpGet(url);
		try {
			addHeads(get);
			// 设置参数
			get.setConfig(requestConfig);

			client = getHttpClient(url);

			HttpResponse res = client.execute(get);
			HttpEntity entity = res.getEntity();
			if (listener == null) {
				return EntityUtils.toByteArray(entity);
			} else {
				InputStream instream = entity.getContent();
				if (instream == null) {
					return null;
				}
				try {
					int i = (int) entity.getContentLength();
					if (i < 0) {
						i = 4096;
					}
					ByteArrayBuffer buffer = new ByteArrayBuffer(i);
					byte[] tmp = new byte[4096];
					int l;
					while ((l = instream.read(tmp)) != -1 && !listener.isClosed()) {
						buffer.append(tmp, 0, l);
						listener.read(tmp, l);
					}
					byte[] arrayOfByte1 = buffer.toByteArray();
					return arrayOfByte1;
				} finally {
					instream.close();
				}
			}

		} finally {
			colse(url, client, get);
		}
	}

	public static String upload(String url, Object file, String filePartName, Map<String, String> params) throws Exception {
		return onUpload(url, file, filePartName, params);
	}

	public static String upload(String url, byte[] bytes, String fileName, String filePartName, Map<String, String> params) throws Exception {
		return onUpload(url, new ByteArrayBody(bytes, fileName), filePartName, params);
	}

	private static String onUpload(String url, Object obj, String filePartName, Map<String, String> params) throws Exception {
		HttpClient client = null;
		HttpPost post = new HttpPost(url);

		String result = null;

		MultipartEntityBuilder builder = MultipartEntityBuilder.create();

		if (params != null && !params.isEmpty()) {
			Set<Entry<String, String>> entrySet = params.entrySet();
			for (Entry<String, String> entry : entrySet) {
				builder.addPart(entry.getKey(), new StringBody(entry.getValue(), ContentType.TEXT_PLAIN));
			}
		}

		if (obj instanceof File) {
			File file = (File) obj;
			builder.addPart(filePartName, new FileBody(file));
		} else if (obj instanceof FileBody) {
			builder.addPart(filePartName, (FileBody) obj);
		} else if (obj instanceof InputStreamBody) {
			builder.addPart(filePartName, (ContentBody) obj);
		} else if (obj instanceof ByteArrayBody) {
			builder.addPart(filePartName, (ContentBody) obj);
		} else {
			builder.addPart(filePartName, new StringBody(obj.toString(), ContentType.TEXT_PLAIN));
		}

		HttpEntity reqEntity = builder.build();

		post.setEntity(reqEntity);

		try {
			addHeads(post);

			post.setConfig(requestConfig);

			client = getHttpClient(url);

			HttpResponse res = client.execute(post);

			if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				HttpEntity entity = res.getEntity();
				if (entity != null) {
					result = EntityUtils.toString(entity);
				}
			}
			return result;
		} finally {
			colse(url, client, post);
		}

	}

	public static void colse(String url, HttpClient client, HttpRequestBase request) throws IOException {
		request.releaseConnection();
		//		if (url.startsWith("https") && client != null && client instanceof CloseableHttpClient) {
		//			((CloseableHttpClient) client).close();
		//		}
	}

	private static void addHeads(Map<String, String> headers, HttpRequestBase post) {
		if (headers != null && !headers.isEmpty()) {
			for (Entry<String, String> entry : headers.entrySet()) {
				post.addHeader(entry.getKey(), entry.getValue());
			}
		}
	}

	private static void addHeads(HttpRequestBase post) {
		addHeads(headers, post);
	}

	public static void addHead(String key, String value) {
		headers.put(key, value);
	}

	public static HttpClient getHttpClient(String url) throws Exception {
		HttpClient client;
		if (url.startsWith("https")) {
			// 执行 Https 请求.
			client = createSSLInsecureClient();
		} else {
			// 执行 Http 请求.
			client = HttpClients.createDefault();
		}
		return client;
	}

	/**
	 * 创建 SSL连接 //
	 */
	//	private static synchronized CloseableHttpClient createSSLInsecureClient() throws Exception {
	//		try {
	//			if (ctx == null) {
	//				// System.setProperty("javax.net.ssl.trustStore",
	//				// "D:\\code\\xxx.ks");
	//				// System.setProperty("javax.net.ssl.trustStorePassword",
	//				// "xxxx");
	//				KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
	//				ks.load(HttpUtils.class.getResourceAsStream(trustStoreStr), trustStorePasswordStr.toCharArray());
	//				TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
	//				tmf.init(ks);
	//				ctx = SSLContext.getInstance("TLS");
	//				ctx.init(null, tmf.getTrustManagers(), null);
	//			}
	//			SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(ctx);
	//			return HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig).build();
	//		} catch (Exception e) {
	//			throw e;
	//		}
	//	}
	public static HttpClient createSSLInsecureClient() throws Exception {
		//采用绕过验证的方式处理https请求  
		SSLContext sslcontext = createIgnoreVerifySSL();

		// 设置协议http和https对应的处理socket链接工厂的对象  
		Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create().register("http", PlainConnectionSocketFactory.INSTANCE).register("https", new SSLConnectionSocketFactory(sslcontext)).build();
		PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
		HttpClients.custom().setConnectionManager(connManager);

		//创建自定义的httpclient对象  
		CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build();

		return client;
	}

	/**
	 * 绕过验证
	 * 
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public static SSLContext createIgnoreVerifySSL() throws NoSuchAlgorithmException, KeyManagementException {
		SSLContext sc = SSLContext.getInstance("SSLv3");

		// 实现一个X509TrustManager接口，用于绕过验证，不用修改里面的方法  
		X509TrustManager trustManager = new X509TrustManager() {
			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {
			}

			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		};

		sc.init(null, new TrustManager[] { trustManager }, null);
		return sc;
	}
}