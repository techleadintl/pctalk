package com.xinwei.common.lang;

public class ByteArrayUtil {
	static final char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z' };

	public static void putBoolean(byte[] array, int offset, boolean val) {
		array[offset] = (byte) (val ? 1 : 0);
	}

	public static void putChar(byte[] array, int offset, char val) {
		array[(offset + 1)] = (byte) (val >>> '\000');
		array[(offset + 0)] = (byte) (val >>> '\b');
	}

	public static void putShort(byte[] array, int offset, short val) {
		array[(offset + 1)] = (byte) (val >>> 0);
		array[(offset + 0)] = (byte) (val >>> 8);
	}

	public static void putInt(byte[] array, int offset, int val) {
		array[(offset + 3)] = (byte) (val >>> 0);
		array[(offset + 2)] = (byte) (val >>> 8);
		array[(offset + 1)] = (byte) (val >>> 16);
		array[(offset + 0)] = (byte) (val >>> 24);
	}

	public static void putFloat(byte[] array, int offset, float val) {
		int i = Float.floatToIntBits(val);
		array[(offset + 0)] = (byte) (i >>> 0);
		array[(offset + 1)] = (byte) (i >>> 8);
		array[(offset + 2)] = (byte) (i >>> 16);
		array[(offset + 3)] = (byte) (i >>> 24);
	}

	public static void putLong(byte[] array, int offset, long val) {
		int index = offset + 7;
		array[(index--)] = (byte) (int) (val >>> 0);
		array[(index--)] = (byte) (int) (val >>> 8);
		array[(index--)] = (byte) (int) (val >>> 16);
		array[(index--)] = (byte) (int) (val >>> 24);
		array[(index--)] = (byte) (int) (val >>> 32);
		array[(index--)] = (byte) (int) (val >>> 40);
		array[(index--)] = (byte) (int) (val >>> 48);
		array[(index--)] = (byte) (int) (val >>> 56);
	}

	public static void putDouble(byte[] b, int offset, double val) {
		long j = Double.doubleToLongBits(val);
		b[(offset + 0)] = (byte) (int) (j >>> 0);
		b[(offset + 1)] = (byte) (int) (j >>> 8);
		b[(offset + 2)] = (byte) (int) (j >>> 16);
		b[(offset + 3)] = (byte) (int) (j >>> 24);
		b[(offset + 4)] = (byte) (int) (j >>> 32);
		b[(offset + 5)] = (byte) (int) (j >>> 40);
		b[(offset + 6)] = (byte) (int) (j >>> 48);
		b[(offset + 7)] = (byte) (int) (j >>> 56);
	}

	public static void putHexString(byte[] array, int offset, String val) {
		int size = val.length() / 2;
		int max = offset + size;
		int i = offset;
		for (int j = 0; i < max; j++) {
			array[i] = (byte) Integer.parseInt(val.substring(2 * j, 2 * j + 2), 16);
			i++;
		}
	}

	public static int toInt(byte[] b) {
		return toInt(b, 0, 4);
	}

	public static int toInt(byte[] b, int off) {
		return toInt(b, off, 4);
	}

	public static int toInt(byte[] b, int off, int len) {
		int st = 0;
		if (off < 0) {
			off = 0;
		}
		if (len > 4) {
			len = 4;
		}
		for (int i = 0; (i < len) && (i + off < b.length); i++) {
			st <<= 8;
			st += (b[(i + off)] & 0xFF);
		}
		return st;
	}

	public static long toLong(byte[] b, int off, int len) {
		long result = 0L;
		if (off < 0) {
			off = 0;
		}
		if (len > 8) {
			len = 8;
		}
		for (int i = 0; (i < len) && (i + off < b.length); i++) {
			result <<= 8;
			result += (b[(i + off)] & 0xFF);
		}
		return result;
	}

	public static short toShort(byte[] b) {
		return toShort(b, 0, 2);
	}

	public static short toShort(byte[] b, int off) {
		return toShort(b, off, 2);
	}

	public static short toShort(byte[] b, int off, int len) {
		int st = 0;
		if (off < 0) {
			off = 0;
		}
		if (len > 2) {
			len = 2;
		}
		for (int i = 0; (i < len) && (i + off < b.length); i++) {
			st <<= 8;
			st += (b[(i + off)] & 0xFF);
		}
		return (short) st;
	}

	public static float toFloat(byte[] b) {
		return toFloat(b, 0, 4);
	}

	public static float toFloat(byte[] array, int offset) {
		return toFloat(array, offset, 4);
	}

	public static float toFloat(byte[] array, int offset, int len) {
		int st = 0;
		if (offset < 0) {
			offset = 0;
		}
		if (len > 4) {
			len = 4;
		}
		for (int i = 0; (i < len) && (i + offset < array.length); i++) {
			st += ((array[(i + offset)] & 0xFF) << 8 * i);
		}
		return Float.intBitsToFloat(st);
	}

	public static double toDouble(byte[] array, int offset, int len) {
		long st = 0L;
		if (offset < 0) {
			offset = 0;
		}
		if (len > 8) {
			len = 8;
		}
		for (int i = 0; (i < len) && (i + offset < array.length); i++) {
			st += ((array[(i + offset)] & 0xFF) << 8 * i);
		}
		return Double.longBitsToDouble(st);
	}

	public static int bcdByteToInt(byte[] array, int offset) {
		if (offset > array.length) {
			return 0;
		}
		return (array[offset] & 0xFF) / 16 * 10 + (array[offset] & 0xF);
	}

	public static char toChar(byte[] b) {
		char ch = '\000';
		if (b[0] > 0)
			ch = (char) (ch + b[0]);
		else {
			ch = (char) (ch + (256 + b[0]));
		}
		ch = (char) (ch * 'Ā');
		if (b[1] > 0)
			ch = (char) (ch + b[1]);
		else {
			ch = (char) (ch + (256 + b[1]));
		}
		return ch;
	}

	public static String toBinaryString(byte[] array, int offset, int length) {
		return toUnsignedString(array, offset, length, 1);
	}

	public static String toOctalString(byte[] array, int offset, int length) {
		return toUnsignedString(array, offset, length, 3);
	}

	public static String toHexString(byte[] array, int offset, int length) {
		return toUnsignedString(array, offset, length, 4);
	}

	public static String toHexString(byte[] array) {
		int len = array == null ? 0 : array.length;
		return toUnsignedString(array, 0, len, 4);
	}

	private static String toUnsignedString(byte[] bytes, int offset, int length, int shift) {
		int bytesNum = length;
		int charsNum = shift == 3 ? 3 : 8 / shift;
		int bufSize = charsNum * bytesNum;

		char[] buf = new char[bufSize];
		int charPos = bufSize;
		int radix = 1 << shift;
		int mask = radix - 1;

		int startPos = 0;
		int b = 0;
		int min = offset - 1;
		for (int i = bytesNum - 1 + offset; i > min; i--) {
			b = bytes[i] & 0xFF;
			startPos = charPos - charsNum;
			do {
				charPos--;
				buf[charPos] = digits[(b & mask)];
				b >>>= shift;
			} while (b != 0);
			for (int j = startPos; j < charPos; j++) {
				buf[j] = digits[0];
			}
			charPos = startPos;
		}
		return new String(buf);
	}

	public static void main(String[] agrs) {
		double input = 435.67000000000002D;
		byte[] b = new byte[8];
		putDouble(b, 0, input);
		double output = toDouble(b, 0, 8);
		System.out.println(output);

		System.out.println(CompressNumber(System.currentTimeMillis(), 6));
		System.out.println(UnCompressNumber(CompressNumber(999999999999999999L, 6)));
	}

	final static char[] digitChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
			'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '+', '/', };

	public static String get64String(long number) {
		return CompressNumber(number, 6);
	}

	/**
	 * 把10进制的数字转换成64进制
	 * 
	 * @param number
	 * @param shift
	 * @return
	 */
	private static String CompressNumber(long number, int shift) {
		char[] buf = new char[64];
		int charPos = 64;
		int radix = 1 << shift;
		long mask = radix - 1;
		do {
			buf[--charPos] = digitChars[(int) (number & mask)];
			number >>>= shift;
		} while (number != 0);
		return new String(buf, charPos, (64 - charPos));
	}

	/**
	 * 把64进制的字符串转换成10进制
	 * 
	 * @param decompStr
	 * @return
	 */
	private static long UnCompressNumber(String decompStr) {
		long result = 0;
		for (int i = decompStr.length() - 1; i >= 0; i--) {
			if (i == decompStr.length() - 1) {
				result += getCharIndexNum(decompStr.charAt(i));
				continue;
			}
			for (int j = 0; j < digitChars.length; j++) {
				if (decompStr.charAt(i) == digitChars[j]) {
					result += ((long) j) << 6 * (decompStr.length() - 1 - i);
				}
			}
		}
		return result;
	}

	/**
	 * 
	 * @param ch
	 * @return
	 */
	private static long getCharIndexNum(char ch) {
		int num = ((int) ch);
		if (num >= 48 && num <= 57) {
			return num - 48;
		} else if (num >= 97 && num <= 122) {
			return num - 87;
		} else if (num >= 65 && num <= 90) {
			return num - 29;
		} else if (num == 43) {
			return 62;
		} else if (num == 47) {
			return 63;
		}
		return 0;
	}

}