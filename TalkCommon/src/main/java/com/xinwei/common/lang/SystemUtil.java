package com.xinwei.common.lang;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.nio.channels.FileLock;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class SystemUtil {
	public static final String FILE_ENCODING = getSystemProperty("file.encoding");

	public static final String FILE_SEPARATOR = getSystemProperty("file.separator");

	public static final String JAVA_CLASS_PATH = getSystemProperty("java.class.path");

	public static final String JAVA_HOME = getSystemProperty("java.home");

	public static final String JAVA_IO_TMPDIR = getSystemProperty("java.io.tmpdir");

	public static final String LINE_SEPARATOR = getSystemProperty("line.separator");

	public static final String PATH_SEPARATOR = getSystemProperty("path.separator");

	public static final String OS_ARCH = getSystemProperty("os.arch");

	public static final String OS_NAME = getSystemProperty("os.name");

	public static final String OS_VERSION = getSystemProperty("os.version");

	public static final String USER_COUNTRY = getSystemProperty("user.country") == null ? getSystemProperty("user.region") : getSystemProperty("user.country");

	public static final String USER_DIR = getSystemProperty("user.dir");

	public static final String USER_HOME = getSystemProperty("user.home");

	public static final String USER_LANGUAGE = getSystemProperty("user.language");

	public static final String USER_NAME = getSystemProperty("user.name");

	public static final String USER_TIMEZONE = getSystemProperty("user.timezone");

	public static ExecutorService es = Executors.newCachedThreadPool();

	private static String getSystemProperty(String property) {
		try {
			return System.getProperty(property);
		} catch (SecurityException localSecurityException) {
			System.err.println("Caught a SecurityException reading the system property '" + property + "'; the SystemUtils property object will default to null.");
		}
		return null;
	}

	public static <T> boolean waitForSize(AtomicInteger number, int count, int limit) {
		try {
			int trial = 0;
			int size = number.get();
			while (size != count && trial < limit) {
				TimeUnit.MILLISECONDS.sleep(1);
				trial++;
				size = number.get();
			}
			return trial < limit;
		} catch (Exception e) {
			return false;
		}
	}

	public static void execute(Runnable task) {
		es.submit(task);
	}

	public static void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void sleep(long time, TimeUnit timeUnit) {
		sleep(timeUnit.toMillis(time));
	}

	public static Object newInstance(String className) {
		try {
			Class clazz = Class.forName(className);
			return clazz.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> T newInstance(Class<T> clazz) {
		try {
			if (clazz.isArray()) {
				return (T) Array.newInstance(clazz.getComponentType(), 0);
			}
			return clazz.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Object newInstance(String className, Object[] args) {
		try {
			Class clazz = Class.forName(className);
			Class[] parameterTypes = new Class[args.length];
			for (int i = 0; i < args.length; i++) {
				parameterTypes[i] = args[i].getClass();
			}
			Constructor constructor = clazz.getConstructor(parameterTypes);
			return constructor.newInstance(args);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> T newInstance(Class<T> clazz, Object[] args) {
		try {
			Class[] parameterTypes = new Class[args.length];
			for (int i = 0; i < args.length; i++) {
				parameterTypes[i] = args[i].getClass();
			}
			Constructor constructor = clazz.getConstructor(parameterTypes);
			return (T) constructor.newInstance(args);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> T newInstance(Class<T> clazz, Class<?>[] parameterTypes, Object[] args) {
		try {
			Constructor constructor = clazz.getConstructor(parameterTypes);
			return (T) constructor.newInstance(args);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Object invoke(String methodName, Object object, Class<?>[] argsTypes, Object[] args) {
		Object result = null;
		try {
			Method m = object.getClass().getDeclaredMethod(methodName, argsTypes);
			m.setAccessible(true);
			result = m.invoke(object, args);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static Locale getLocale(String language) {
		if ((StringUtil.isNotBlank(language)) && (language.contains("_"))) {
			String[] split = language.split("_");
			switch (split.length) {
			case 2:
				return new Locale(split[0], split[1]);
			case 3:
				return new Locale(split[0], split[1], split[2]);
			case 1:
				return new Locale(split[0]);
			}
		}

		return Locale.getDefault();
	}

	public static Object getValue(Class<?> clazz, Object value) {
		if (value == null)
			return null;
		if ((clazz == Integer.class) || (clazz == Integer.TYPE))
			return Integer.valueOf(Integer.parseInt(value.toString()));
		if ((clazz == Long.class) || (clazz == Long.TYPE))
			return Long.valueOf(Long.parseLong(value.toString()));
		if ((clazz == Double.class) || (clazz == Double.TYPE))
			return Double.valueOf(Double.parseDouble(value.toString()));
		if ((clazz == Float.class) || (clazz == Float.TYPE))
			return Float.valueOf(Float.parseFloat(value.toString()));
		if ((clazz == Short.class) || (clazz == Short.TYPE))
			return Short.valueOf(Short.parseShort(value.toString()));
		if ((clazz == Boolean.TYPE) || (clazz == Boolean.class)) {
			return Boolean.valueOf(Boolean.getBoolean(value.toString()));
		}
		return value;
	}

	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	public static FileOutputStream lockFile(String filePath) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(filePath);
			//获取文件锁 FileLock 对象  
			FileLock fl = fos.getChannel().tryLock();
			System.out.println(fl);
			System.out.println(filePath);
			System.out.println(fos);
			
			//tryLock是尝试获取锁，有可能为空，所以要判断  
			if (fl != null) {
				return fos;
			} else {
				fos.close();
				return null;
			}
		} catch (Exception e) {
			if (fos != null) {
				try {
					fos.close();
				} catch (Exception e1) {
				}
			}
			return null;
		}


	}
}