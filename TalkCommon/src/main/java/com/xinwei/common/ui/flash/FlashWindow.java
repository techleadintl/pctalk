package com.xinwei.common.ui.flash;

import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;


import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.StdCallLibrary.StdCallCallback;

public class FlashWindow {
	private HashMap<Window, Thread> flashings = new HashMap<Window, Thread>();

	static interface User32 extends StdCallLibrary {
		User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class);

		interface WNDENUMPROC extends StdCallCallback {
			boolean callback(Pointer hWnd, Pointer arg);
		}

		boolean EnumWindows(WNDENUMPROC lpEnumFunc, Pointer userData);

		int GetWindowTextA(Pointer hWnd, byte[] lpString, int nMaxCount);

		Pointer GetWindow(Pointer hWnd, int uCmd);

		boolean FlashWindow(Pointer hWnd, boolean flash);

		Pointer FindWindowW(String clazz, byte[] w);
	}

	public static Pointer FindWindow(final String title) {
		final List<Pointer> windows = new ArrayList<Pointer>();

		User32.INSTANCE.EnumWindows(new User32.WNDENUMPROC() {
			@Override
			public boolean callback(Pointer hWnd, Pointer arg) {
				byte[] windowText = new byte[512];
				User32.INSTANCE.GetWindowTextA(hWnd, windowText, 512);
				try {
					String property = System.getProperty("sun.jnu.encoding");
					String wText = new String(windowText, property).trim();
					if (!wText.isEmpty() && wText.equals(title)) {
						windows.add(hWnd);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return true;
			}
		}, null);
		if (windows.isEmpty())
			return null;
		return windows.get(0);
	}

	public void flash(String name, boolean flash) {
		Pointer hwnd = FindWindow(name);
		if (hwnd == null)
			return;
		User32.INSTANCE.FlashWindow(hwnd, flash);
	}

	/*
	 * @param frame The JFrame to be flashed
	 * 
	 * @param intratime The amount of time between the on and off states of a single flash
	 * 
	 * @param intertime The amount of time between different flashes
	 * 
	 * @param count The number of times to flash the window
	 */
	public void flash(final Window window, final int intratime, final int count) {
		new Thread(new Runnable() {
			public void run() {
				try {
					if (window instanceof JFrame) {
						// flash on and off each time
						for (int i = 0; i < count; i++) {
							flash(((JFrame) window).getTitle(), true);
							Thread.sleep(intratime);
						}
						// turn the flash off
						flash(((JFrame) window).getTitle(), false);
					}
				} catch (Exception ex) {
					// System.out.println(ex.getMessage());
				}
			}
		}).start();
	}

	public void startFlashing(final Window window) {
		if (flashings.get(window) == null) {
			Thread t = new Thread() {
				public void run() {
					try {
						while (true) {
							Thread.sleep(1500);
							// System.out.println("Flash Window");
							if (window instanceof JFrame)
								flash(((JFrame) window).getTitle(), true);
						}
					} catch (Exception ex) {
						flash(((JFrame) window).getTitle(), false);
					}

				}
			};
			t.start();
			flashings.put(window, t);
		}
	}

	public void stopFlashing(final Window window) {
		if (flashings.get(window) != null) {
			flashings.get(window).interrupt();
			flashings.remove(window);
		}
	}

	public static void main(String[] args) throws Exception {
		final JFrame frame = new JFrame();
		frame.setTitle("你好");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new FlowLayout());
		JButton button = new JButton("Temp Flashing");
		frame.getContentPane().add(button);
		final FlashWindow winutil = new FlashWindow();
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				winutil.flash(frame, 750, 5);
			}
		});

		JButton startButton = new JButton("Start Flashing");
		frame.getContentPane().add(startButton);
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				winutil.startFlashing(frame);
			}
		});

		JButton stopButton = new JButton("Stop Flashing");
		frame.getContentPane().add(stopButton);
		stopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				// winutil.flash(frame,750,1500,5);
				winutil.stopFlashing(frame);
			}
		});
		frame.pack();
		frame.setVisible(true);
	}

}
