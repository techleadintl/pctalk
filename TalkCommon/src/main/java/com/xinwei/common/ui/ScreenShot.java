/*************************************************************
 * * Copyright (c) 2012-2020  信威
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2012-8-28 李拥10:56:11
 * 
 ***************************************************************/
package com.xinwei.common.ui;

import java.awt.AWTException;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicRootPaneUI;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
import com.xinwei.common.lookandfeel.util.ColorUtil;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.common.lookandfeel.util.PainterUtil;

public abstract class ScreenShot {
	private static Color sizePanelColor = new Color(104, 104, 100);

	private static Color thumbPanelColor = new Color(0, 174, 255);

	private static Color buttonPanelColor = new Color(222, 238, 255);

	private static Color buttonPanelBorderColor = new Color(76, 151, 210);

	private static Color color1 = new Color(201, 18, 18);
	private static Color color2 = new Color(255, 197, 40);
	private static Color color3 = new Color(38, 181, 0);
	private static Color color4 = new Color(160, 97, 239);
	private static Color color5 = new Color(43, 132, 223);
	private static Color color6 = new Color(160, 160, 160);

	private static Color selectColor = new Color(0, 174, 255);

	private static Font font14 = new Font("Dialog", Font.PLAIN, 14);
	/**
	 * Holds the image of the picker cursor.
	 */
	private BufferedImage cursorImage;

	private Robot robot;

	// 0:初始；1：拉框；2：拉动边角；3：选择
	private int status = 0;

	public void doCapture(final JFrame parentFrame) {
		// parentFrame.setVisible(false);
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				try {
					robot = new Robot();
					Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
					final JDialog screenShotDialog = new JDialog();
					screenShotDialog.getRootPane().setUI(new BasicRootPaneUI());
					screenShotDialog.setIconImages(getWindowImageList());

					BufferedImage screenImage = robot.createScreenCapture(new Rectangle(0, 0, screenSize.width, screenSize.height));

					final ScreenImagePanel screenImagePanel = new ScreenImagePanel(parentFrame, screenShotDialog, screenImage, screenSize.width, screenSize.height);
					screenShotDialog.add(screenImagePanel);

					screenShotDialog.setUndecorated(true);
					screenShotDialog.setSize(screenSize);
					screenShotDialog.setVisible(true);
					screenShotDialog.setAlwaysOnTop(true);
					screenImagePanel.requestFocus();

				} catch (Exception exe) {
					exe.printStackTrace();
				}

			}
		});

	}

	private void doFinishX(BufferedImage captureImage) {
		doClipboard(captureImage);
		doFinish(captureImage);
	}

	protected void doClipboard(BufferedImage captureImage) {
	}

	public abstract void doFinish(BufferedImage captureImage);

	protected String getRgbStr(Color color) {
		return MessageFormat.format("RGB:({0},{1},{2})", color.getRed(), color.getGreen(), color.getBlue());
	}

	protected String getSizeStr(int w, int h) {
		return String.valueOf(w) + "*" + String.valueOf(h);
	}

	protected ImageIcon getDoodleImageIcon() {
		return null;
	};

	protected ImageIcon getArrowImageIcon() {
		return null;
	}

	protected ImageIcon getOvalImageIcon() {
		return null;
	}

	protected ImageIcon getCancelImageIcon() {
		return null;
	}

	protected ImageIcon getOkImageIcon() {
		return null;
	}

	protected ImageIcon getRectangleImageIcon() {
		return null;
	}

	protected ImageIcon getSaveImageIcon() {
		return null;
	}

	protected ImageIcon getUndoImageIcon() {
		return null;
	}

	protected ImageIcon getVerticalLineImageIcon() {
		return null;
	}

	protected ImageIcon getStroke2ImageIcon() {
		return null;
	}

	protected ImageIcon getStroke4ImageIcon() {
		return null;
	}

	protected ImageIcon getStroke6ImageIcon() {
		return null;
	}

	protected ImageIcon getColor1ImageIcon() {
		return null;
	}

	protected ImageIcon getColor2ImageIcon() {
		return null;
	}

	protected ImageIcon getColor3ImageIcon() {
		return null;
	}

	protected ImageIcon getColor4ImageIcon() {
		return null;
	}

	protected ImageIcon getColor5ImageIcon() {
		return null;
	}

	protected ImageIcon getColor6ImageIcon() {
		return null;
	}

	protected ImageIcon getColor7ImageIcon() {
		return null;
	}

	protected java.util.List<? extends Image> getWindowImageList() {
		return null;
	}

	private class ScreenImagePanel extends JPanel implements MouseListener, MouseMotionListener {
		private static final long serialVersionUID = 4313191240679143465L;
		private BufferedImage screenImage;
		private int width, height;
		private int startX, startY, endX, endY, pressedX, pressedY;
		private Window parentWindow;
		private Window screenShotWindow;
		private Rectangle select = new Rectangle(0, 0, 0, 0);
		private States current = States.DEFAULT;//
		private Rectangle[] rec = new Rectangle[8];//
		private JPanel thumbPanel;
		private JLabel thumbLabel;
		private JLabel thumbRgbLabel;
		private JLabel thumbSizeLabel;

		private JLabel toolSizeLabel;

		BufferedImage captureImage;

		BufferedImage srcImage;

		private JPanel sizePanel;
		private JPanel toolPanel;
		private JPanel buttonPanel;
		private McWillIconButton rectangleButton;
		private McWillIconButton ovalButton;
		private McWillIconButton arrowButton;
		private McWillIconButton doodleButton;
		private McWillIconButton undoButton;
		private McWillIconButton saveButton;
		private McWillIconButton cancelButton;
		private McWillIconButton okButton;

		private McWillIconButton currentShapeButton;

		private JPanel fontPanel;
		private McWillIconButton stroke1Button;
		private McWillIconButton stroke2Button;
		private McWillIconButton stroke3Button;
		private McWillIconButton color1Button;
		private McWillIconButton color2Button;
		private McWillIconButton color3Button;
		private McWillIconButton color4Button;
		private McWillIconButton color5Button;
		private McWillIconButton color6Button;

		private int shapeType; // 1:涂鸦,2:画线,3:画圆,4:矩形

		private int shapeStroke = 2;

		private Color shapeColor = Color.BLUE;

		private BufferedImage bi; // 用于双缓冲

		private LinkedList<List<Shape>> shapeList = new LinkedList<List<Shape>>();

		public ScreenImagePanel(final Window parentWindow, final Window screenShotWindow, BufferedImage screenImage, int width, int height) throws AWTException {
			setLayout(null);

			this.screenShotWindow = screenShotWindow;
			this.screenImage = screenImage;
			this.width = width;
			this.height = height;
			this.parentWindow = parentWindow;
			this.addMouseListener(this);
			this.addMouseMotionListener(this);
			DrawMouseListener drawMouseListener = new DrawMouseListener();
			this.addMouseListener(drawMouseListener);
			this.addMouseMotionListener(drawMouseListener);

			addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						screenShotWindow.dispose();
						if (parentWindow != null) {
							parentWindow.setVisible(true);
						}
					}
				}
			});

			createThumbPanel();
			createSizePanel();
			createButtonPanel();
			createFontPanel();

			toolPanel = new JPanel(new VerticalFlowLayout(VerticalFlowLayout.TOP, 0, 2, true, false));
			toolPanel.add(buttonPanel);
			toolPanel.add(fontPanel);

			add(toolPanel);
			add(thumbPanel);
			add(sizePanel);

			thumbPanel.setVisible(false);
			toolPanel.setVisible(false);
			sizePanel.setVisible(false);
		}

		private void createSizePanel() {
			sizePanel = new JPanel(null) {
				@Override
				protected void paintComponent(Graphics g) {
					super.paintComponent(g);
					Graphics g2d = g.create();
					g2d.setColor(sizePanelColor);
					g2d.fillRect(0, 0, getWidth(), getHeight());
					g2d.dispose();
				}
			};
			toolSizeLabel = new JLabel();
			toolSizeLabel.setFont(font14);
			toolSizeLabel.setOpaque(false);
			toolSizeLabel.setForeground(Color.WHITE);
			toolSizeLabel.setHorizontalAlignment(JLabel.CENTER);
			toolSizeLabel.setBounds(0, 0, 75, 22);

			sizePanel.add(toolSizeLabel);

			sizePanel.setSize(new Dimension(75, 22));
		}

		protected void createThumbPanel() {
			thumbPanel = new JPanel();
			thumbPanel.setLayout(new BorderLayout());
			thumbPanel.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));

			thumbLabel = new JLabel() {
				@Override
				protected void paintComponent(Graphics g) {
					super.paintComponent(g);
					Graphics g2 = g.create();
					g2.setColor(thumbPanelColor);
					g2.fillRect((getWidth() - 4) / 2, 0, 4, getHeight());
					g2.fillRect(0, (getHeight() - 4) / 2, getWidth(), 4);
					g2.dispose();
				}
			};
			thumbLabel.setPreferredSize(new Dimension(122, 80));

			JPanel jPanel = new JPanel(null) {
				@Override
				protected void paintComponent(Graphics g) {
					super.paintComponent(g);
					g.setColor(sizePanelColor);
					g.fillRect(0, 0, getWidth(), getHeight());
				}
			};
			jPanel.setPreferredSize(new Dimension(122, 40));

			thumbSizeLabel = new JLabel();
			thumbSizeLabel.setOpaque(false);
			thumbSizeLabel.setForeground(Color.WHITE);
			thumbSizeLabel.setBounds(0, 0, 122, 20);
			thumbSizeLabel.setFont(font14);
			thumbSizeLabel.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));

			thumbRgbLabel = new JLabel();
			thumbRgbLabel.setOpaque(false);
			thumbRgbLabel.setForeground(Color.WHITE);
			thumbRgbLabel.setBounds(0, 20, 122, 20);
			thumbRgbLabel.setFont(font14);
			thumbRgbLabel.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 0));

			jPanel.add(thumbSizeLabel);
			jPanel.add(thumbRgbLabel);

			thumbPanel.setSize(new Dimension(122, 122));
			thumbPanel.add(thumbLabel, BorderLayout.NORTH);
			thumbPanel.add(jPanel, BorderLayout.CENTER);
		}

		// protected void createThumbPanel() {
		// thumbPanel = new JPanel();
		// thumbPanel.setLayout(new BorderLayout());
		// thumbPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		//
		//
		// thumbLabel = new JLabel() {
		// Color color = LAF.getColor(0, 174, 255);
		//
		// @Override
		// protected void paintComponent(Graphics g) {
		// super.paintComponent(g);
		// g.setColor(color);
		// g.fillRect((getWidth() - 4) / 2, 0, 4, getHeight());
		// g.fillRect(0, (getHeight() - 4) / 2, getWidth(), 4);
		// }
		// };
		// thumbLabel.setPreferredSize(new Dimension(122, 80));
		// thumbRgbLabel = new JLabel() {
		// @Override
		// protected void paintComponent(Graphics g) {
		// g.setColor(LAF.getColor(104, 104, 100));
		// g.fillRect(0, 0, getWidth(), getHeight());
		// super.paintComponent(g);
		// }
		// };
		// thumbRgbLabel.setForeground(Color.WHITE);
		// thumbRgbLabel.setPreferredSize(new Dimension(122, 20));
		// thumbSizeLabel = new JLabel() {
		// @Override
		// protected void paintComponent(Graphics g) {
		// g.setColor(LAF.getColor(104, 104, 100));
		// g.fillRect(0, 0, getWidth(), getHeight());
		// super.paintComponent(g);
		// }
		// };
		// thumbSizeLabel.setForeground(Color.WHITE);
		// thumbSizeLabel.setPreferredSize(new Dimension(122, 20));
		//
		// thumbPanel.setSize(new Dimension(122, 122));
		// thumbPanel.add(thumbLabel, BorderLayout.NORTH);
		// thumbPanel.add(thumbSizeLabel, BorderLayout.CENTER);
		// thumbPanel.add(thumbRgbLabel, BorderLayout.SOUTH);
		// }

		protected void createButtonPanel() {
			buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 2)) {
				@Override
				protected void paintComponent(Graphics g) {
					g.setColor(buttonPanelColor);
					g.fillRect(0, 0, getWidth(), getHeight());
					super.paintComponent(g);
				}
			};
			buttonPanel.addMouseMotionListener(new MouseMotionAdapter() {
				@Override
				public void mouseMoved(MouseEvent e) {
					setCursor(States.DEFAULT.getCursor());
				}

			});

			buttonPanel.setBorder(BorderFactory.createLineBorder(buttonPanelBorderColor));

			rectangleButton = new McWillIconButton(getRectangleImageIcon());
			ovalButton = new McWillIconButton(getOvalImageIcon());
			arrowButton = new McWillIconButton(getArrowImageIcon());
			doodleButton = new McWillIconButton(getDoodleImageIcon());
			JLabel verticalLine1 = new JLabel(getVerticalLineImageIcon());
			undoButton = new McWillIconButton(getUndoImageIcon());
			saveButton = new McWillIconButton(getSaveImageIcon());
			JLabel verticalLine2 = new JLabel(getVerticalLineImageIcon());
			cancelButton = new McWillIconButton(getCancelImageIcon());
			okButton = new McWillIconButton(getOkImageIcon());

			int W = 27;
			rectangleButton.setPreferredSize(new Dimension(W, W));
			ovalButton.setPreferredSize(new Dimension(W, W));
			arrowButton.setPreferredSize(new Dimension(W, W));
			doodleButton.setPreferredSize(new Dimension(W, W));
			verticalLine1.setPreferredSize(new Dimension(4, W));
			undoButton.setPreferredSize(new Dimension(W, W));
			saveButton.setPreferredSize(new Dimension(W, W));
			verticalLine2.setPreferredSize(new Dimension(4, W));
			cancelButton.setPreferredSize(new Dimension(W, W));
			okButton.setPreferredSize(new Dimension(W, W));

			buttonPanel.add(doodleButton);
			buttonPanel.add(arrowButton);
			buttonPanel.add(rectangleButton);
			buttonPanel.add(ovalButton);
			buttonPanel.add(verticalLine1);
			buttonPanel.add(undoButton);
			buttonPanel.add(saveButton);
			buttonPanel.add(verticalLine2);
			buttonPanel.add(cancelButton);
			buttonPanel.add(okButton);

			// 1:涂鸦,2:画线,3:画圆,4:矩形
			McWillIconButton[] buttons = new McWillIconButton[] { rectangleButton, ovalButton, arrowButton, doodleButton };
			rectangleButton.addActionListener(new ShapeActionListener(buttons, 4));
			ovalButton.addActionListener(new ShapeActionListener(buttons, 3));
			arrowButton.addActionListener(new ShapeActionListener(buttons, 2));
			doodleButton.addActionListener(new ShapeActionListener(buttons, 1));

			OperatActionListener operatActionListener = new OperatActionListener();
			undoButton.addActionListener(operatActionListener);
			saveButton.addActionListener(operatActionListener);
			cancelButton.addActionListener(operatActionListener);
			okButton.addActionListener(operatActionListener);

		}

		protected void createFontPanel() {
			fontPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 1, 2)) {
				@Override
				protected void paintComponent(Graphics g) {
					g.setColor(buttonPanelColor);
					g.fillRect(0, 0, getWidth(), getHeight());
					super.paintComponent(g);
				}
			};

			fontPanel.setBorder(BorderFactory.createLineBorder(buttonPanelBorderColor));

			stroke1Button = new McWillIconButton(getStroke2ImageIcon());
			stroke2Button = new McWillIconButton(getStroke4ImageIcon());
			stroke3Button = new McWillIconButton(getStroke6ImageIcon());
			JLabel verticalLine1 = new JLabel(getVerticalLineImageIcon());
			color1Button = new McWillIconButton(getColor1ImageIcon());
			color2Button = new McWillIconButton(getColor2ImageIcon());
			color3Button = new McWillIconButton(getColor3ImageIcon());
			color4Button = new McWillIconButton(getColor4ImageIcon());
			color5Button = new McWillIconButton(getColor5ImageIcon());
			color6Button = new McWillIconButton(getColor6ImageIcon());

			int W = 27;
			stroke1Button.setPreferredSize(new Dimension(W, W));
			stroke2Button.setPreferredSize(new Dimension(W, W));
			stroke3Button.setPreferredSize(new Dimension(W, W));
			verticalLine1.setPreferredSize(new Dimension(4, W));
			color1Button.setPreferredSize(new Dimension(W, W));
			color2Button.setPreferredSize(new Dimension(W, W));
			color3Button.setPreferredSize(new Dimension(W, W));
			color4Button.setPreferredSize(new Dimension(W, W));
			color5Button.setPreferredSize(new Dimension(W, W));
			color6Button.setPreferredSize(new Dimension(W, W));

			fontPanel.add(stroke1Button);
			fontPanel.add(stroke2Button);
			fontPanel.add(stroke3Button);
			fontPanel.add(verticalLine1);
			fontPanel.add(color1Button);
			fontPanel.add(color2Button);
			fontPanel.add(color3Button);
			fontPanel.add(color4Button);
			fontPanel.add(color5Button);
			fontPanel.add(color6Button);

			McWillIconButton[] strokeButtons = new McWillIconButton[] { stroke1Button, stroke2Button, stroke3Button };
			stroke1Button.addActionListener(new StrokeActionListener(strokeButtons, 2));
			stroke2Button.addActionListener(new StrokeActionListener(strokeButtons, 4));
			stroke3Button.addActionListener(new StrokeActionListener(strokeButtons, 6));
			setBorderPainted(stroke1Button, strokeButtons);
			shapeStroke = 2;

			McWillIconButton[] colorButtons = new McWillIconButton[] { color1Button, color2Button, color3Button, color4Button, color5Button, color6Button };
			color1Button.addActionListener(new ColorActionListener(colorButtons, color1));
			color2Button.addActionListener(new ColorActionListener(colorButtons, color2));
			color3Button.addActionListener(new ColorActionListener(colorButtons, color3));
			color4Button.addActionListener(new ColorActionListener(colorButtons, color4));
			color5Button.addActionListener(new ColorActionListener(colorButtons, color5));
			color6Button.addActionListener(new ColorActionListener(colorButtons, color6));
			setBorderPainted(color1Button, colorButtons);
			shapeColor = color1;
		}

		protected void moveThumbPanel(final MouseEvent me) {
			thumbPanel.setVisible(true);

			final Point mouseLoc = me.getPoint();

			int x = mouseLoc.x + 5;
			int y = mouseLoc.y + 22 + 5;
			if (x + thumbPanel.getWidth() > screenImage.getWidth()) {
				x = mouseLoc.x - thumbPanel.getWidth() - 2;
			} else if (x < 0) {
				x = 0;
			}
			if (y + thumbPanel.getHeight() > screenImage.getHeight()) {
				y = mouseLoc.y - thumbPanel.getHeight() - 2;
			} else if (y < 0) {
				y = 0;
			}
			thumbPanel.setLocation(x, y);

			int i = 15;
			int j = 10;
			x = mouseLoc.x - i;
			y = mouseLoc.y - j;
			int w = i * 2;
			int h = j * 2;
			if (x < 0) {
				x = 0;
			} else if (x + w > screenImage.getWidth()) {
				x = screenImage.getWidth() - w;
			}
			if (y < 0) {
				y = 0;
			} else if (y + h > screenImage.getHeight()) {
				y = screenImage.getHeight() - h;
			}

			BufferedImage capture = screenImage.getSubimage(x, y, w, h);
			thumbLabel.setIcon(new ImageIcon(ImageUtil.resize(capture, 120, 80, "png")));

			Color pickColor = new Color(screenImage.getRGB(mouseLoc.x, mouseLoc.y));
			thumbRgbLabel.setText(getRgbStr(pickColor));
			thumbSizeLabel.setText(getSizeStr(select.width, select.height));
		}

		protected void moveTooPanel(MouseEvent me, boolean fontPanelVisible) {
			toolPanel.setVisible(true);
			sizePanel.setVisible(true);
			if (fontPanelVisible) {
				fontPanel.setVisible(true);
				toolPanel.setSize(new Dimension(265, 78));
			} else {
				fontPanel.setVisible(false);
				toolPanel.setSize(new Dimension(265, 42));
			}
			/** 工具栏 */
			int x = select.x + select.width - toolPanel.getWidth();
			int y = select.y + select.height + 1;
			if (x + toolPanel.getWidth() > screenImage.getWidth()) {
				x = screenImage.getWidth() - toolPanel.getWidth();
			} else if (x < 0) {
				x = select.x;
			}
			if (y + toolPanel.getHeight() > screenImage.getHeight()) {
				y = select.y - toolPanel.getHeight() - 1 - sizePanel.getHeight();
			}
			if (y < 0) {
				y = select.y;
			}
			toolPanel.setLocation(x, y);

			/** 大小边界 */
			x = select.x;
			y = select.y - sizePanel.getHeight() - 1;
			if (x + sizePanel.getWidth() > screenImage.getWidth()) {
				x = screenImage.getWidth() - sizePanel.getWidth();
			} else if (x < 0) {
				x = select.x;
			}
			if (y < 0) {
				y = select.y;
				if (screenImage.getWidth() - select.x - select.width > sizePanel.getWidth()) {
					x = select.x + select.width;
				} else if (select.x > sizePanel.getWidth()) {
					x = select.x - sizePanel.getWidth();
				} else {
					x = select.x;
				}
			}
			sizePanel.setLocation(x, y);

			if (me != null) {
				toolSizeLabel.setText(getSizeStr(select.width, select.height));
			}
		}

		protected void paintShape() {
			// 鼠标移动时，绘制点
			BufferedImage img = srcImage;
			// 2. 创建一个缓冲图形对象 bi
			bi = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = (Graphics2D) bi.getGraphics();
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

			// 将截图的原始图像画到 bi
			g2d.drawImage(img, 0, 0, ScreenImagePanel.this);

			// 1:涂鸦,2:画线,3:画圆,4:矩形
			for (List<Shape> list : shapeList) {
				for (Shape shape : list) {
					g2d.setStroke(new BasicStroke(shape.stroke));
					g2d.setColor(shape.color); // 设置画笔颜色
					if (shape.type == 1) {
						g2d.drawLine(shape.x1, shape.y1, shape.x2, shape.y2);
					} else if (shape.type == 2) {
						PainterUtil.drawArrowLine(g2d, shape.x1, shape.y1, shape.x2, shape.y2);
					} else if (shape.type == 3 || shape.type == 4) {
						int x1 = shape.x1 < shape.x2 ? shape.x1 : shape.x2;
						int y1 = shape.y1 < shape.y2 ? shape.y1 : shape.y2;
						int x2 = shape.x1 < shape.x2 ? shape.x2 : shape.x1;
						int y2 = shape.y1 < shape.y2 ? shape.y2 : shape.y1;
						if (shape.type == 3) {
							g2d.drawOval(x1, y1, x2 - x1, y2 - y1);
						} else {
							g2d.drawRect(x1, y1, x2 - x1, y2 - y1);
						}
					}
				}
			}

			g2d.dispose();

			captureImage = bi;

			this.repaint();
		}

		public void paintComponent(Graphics g) {
			g.drawImage(screenImage, 0, 0, width, height, this);

			g.setColor(ColorUtil.darker(McWillTheme.getThemeColor().getMiddleColor(), 50));

			if (select.width <= 0 || select.height <= 0)
				return;

			Graphics2D g2d = (Graphics2D) g;
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setColor(new Color(155, 155, 155));

			AlphaComposite composite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 40 / 100.0F);
			g2d.setComposite(composite);
			g2d.fill(new RoundRectangle2D.Float(0, 0, this.getWidth(), this.getHeight(), 0, 0));

			AlphaComposite composite1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1F);
			g2d.setComposite(composite1);

			if (captureImage != null) {
				g.drawImage(captureImage, select.x, select.y, this);
			}

			g2d.setColor(selectColor);
			g.drawRect(select.x, select.y, select.width, select.height);

			if (status != 1) {
				for (int i = 0; i < rec.length; i++) {
					if (rec != null) {
						g.fillRect(rec[i].x, rec[i].y, rec[i].width, rec[i].height);
					}
				}
			}
		}

		public void mouseMoved(final MouseEvent me) {
			Point point = me.getPoint();

			if (sizePanel.isVisible()) {
				toolSizeLabel.setText(getSizeStr(select.width, select.height));
			}

			States[] st = States.values();
			for (int i = 0; i < rec.length; i++) {
				if (rec[i] == null)
					continue;
				if (rec[i].contains(point)) {
					current = st[i];
					this.setCursor(current.getCursor());
					return;
				}
			}

			if (select.contains(point)) {
				current = States.SELECT;
				this.setCursor(current.getCursor());
				thumbPanel.setVisible(false);
				return;
			}

			current = States.DEFAULT;

			setCursor(States.CUSTOM.getCursor());
			if (status == 3) {
				return;
			}
			moveThumbPanel(me);
		}

		public void mouseDragged(MouseEvent me) {
			if (shapeType > 0) {
				return;
			}
			int span = 5;

			int x = me.getX();
			int y = me.getY();

			if (current == States.SELECT) {
				/** 获取屏幕的边界 */
				if (endY + (y - pressedY) > screenImage.getHeight()) {
					return;
				}
				if (endX + (x - pressedX) > screenImage.getWidth()) {
					return;
				}
				status = 3;
				startX += (x - pressedX);
				startY += (y - pressedY);
				endX += (x - pressedX);
				endY += (y - pressedY);
				pressedX = x;
				pressedY = y;

			} else if (current == States.EAST) {
				status = 2;
				endX = x;
			} else if (current == States.WEST) {
				status = 2;
				startX = x;
			} else if (current == States.NORTH) {
				status = 2;
				startY = y;
			} else if (current == States.SOUTH) {
				status = 2;
				endY = y;
			} else if (current == States.NORTH_EAST) {
				status = 2;
				endX = x;
				startY = y;
			} else if (current == States.NORTH_WEST) {
				status = 2;
				startX = x;
				startY = y;
			} else if (current == States.SOUTH_EAST) {
				status = 2;
				endX = x;
				endY = y;
			} else if (current == States.SOUTH_WEST) {
				status = 2;
				startX = x;
				endY = y;
			} else if (status != 3) {
				status = 1;
				startX = pressedX;
				startY = pressedY;
				endX = x;
				endY = y;
			}

			int x1 = startX < endX ? startX : endX;
			int y1 = startY < endY ? startY : endY;
			int x2 = startX < endX ? endX : startX;
			int y2 = startY < endY ? endY : startY;

			if (x1 < 0 || x2 > screenImage.getWidth()) {
				return;
			}
			if (y1 < 0 || y2 > screenImage.getHeight()) {
				return;
			}

			int width = x2 - x1;
			int height = y2 - y1;

			int xm = x1 + width / 2;
			int ym = y1 + height / 2;

			select = new Rectangle(x1, y1, width, height);

			rec[0] = new Rectangle(x1 - span / 2, y1 - span / 2, span, span);// 左上
			rec[1] = new Rectangle(xm - span / 2, y1 - span / 2, span, span);// 上中
			rec[2] = new Rectangle(x2 - span / 2, y1 - span / 2, span, span);// 右上
			rec[3] = new Rectangle(x2 - span / 2, ym - span / 2, span, span);// 右中
			rec[4] = new Rectangle(x2 - span / 2, y2 - span / 2, span, span);// 右下
			rec[5] = new Rectangle(xm - span / 2, y2 - span / 2, span, span);// 下中
			rec[6] = new Rectangle(x1 - span / 2, y2 - span / 2, span, span);// 左下
			rec[7] = new Rectangle(x1 - span / 2, ym - span / 2, span, span);// 左中

			try {
				srcImage = captureImage = screenImage.getSubimage(select.x, select.y, select.width, select.height);
			} catch (Exception ex) {
				return;
			}
			this.repaint();

			if (status == 1 || status == 2) {
				moveThumbPanel(me);
				toolPanel.setVisible(false);
				sizePanel.setVisible(false);
			} else if (status == 3) {
				moveTooPanel(me, false);
			}
			// this.setCursor(current.getCursor());
			// thumbPanel.setVisible(false);

		}

		public void mouseReleased(MouseEvent me) {
			startX = select.x;
			endX = select.x + select.width;
			startY = select.y;
			endY = select.y + select.height;

			if (status == 1) {
				status = 3;
				this.repaint();
				// this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				thumbPanel.setVisible(false);
				moveTooPanel(me, false);
			} else if (status == 2) {
				status = 3;
				moveTooPanel(me, false);
			}
			requestFocus();
		}

		public void mousePressed(MouseEvent me) {
			// if (isDoodle) {
			// return;
			// }
			pressedX = me.getX();
			pressedY = me.getY();
		}

		public void mouseExited(MouseEvent me) {

		}

		public void mouseEntered(MouseEvent me) {

		}

		public void mouseClicked(MouseEvent me) {
			if (me.getButton() == 1 && me.getClickCount() == 2) {
				Point p = me.getPoint();
				if (select.contains(p)) {
					// BufferedImage captureImage;
					// if (select.x + select.width < this.getWidth() && select.y
					// + select.height < this.getHeight()) {
					// captureImage = screenImage.getSubimage(select.x,
					// select.y, select.width, select.height);
					// } else {
					// int wid = select.width, het = select.height;
					// if (select.x + select.width >= this.getWidth()) {
					// wid = this.getWidth() - select.x;
					// }
					// if (select.y + select.height >= this.getHeight()) {
					// het = this.getHeight() - select.y;
					// }
					// captureImage = screenImage.getSubimage(select.x,
					// select.y, wid, het);
					// }
					if (parentWindow != null) {
						parentWindow.setVisible(true);
					}
					screenShotWindow.dispose();
					doFinishX(captureImage);
				}
			} else if (me.getButton() == 3) {
				select = new Rectangle(0, 0, 0, 0);
				current = States.DEFAULT;//
				rec = new Rectangle[8];//
				captureImage = null;
				srcImage = null;
				shapeType = 0;
				status = 0;
				shapeList.clear();

				if (currentShapeButton != null) {
					currentShapeButton.setBorderPainted(false);
					currentShapeButton = null;
				}

				toolPanel.setVisible(false);
				sizePanel.setVisible(false);
				this.repaint();
			}
		}

		protected void setBorderPainted(McWillIconButton source, McWillIconButton[] buttons) {
			for (McWillIconButton button : buttons) {
				button.setBorderPainted(button == source);
			}
		}

		class StrokeActionListener extends XActionListener {
			int stroke;
			McWillIconButton[] buttons;

			public StrokeActionListener(McWillIconButton[] buttons, int stroke) {
				this.stroke = stroke;
				this.buttons = buttons;
			}

			@Override
			public void doAction(ActionEvent e) {
				shapeStroke = stroke;
				setBorderPainted((McWillIconButton) e.getSource(), buttons);
				ScreenImagePanel.this.requestFocus();
			}
		}

		class ColorActionListener extends XActionListener {
			Color color;
			McWillIconButton[] buttons;

			public ColorActionListener(McWillIconButton[] buttons, Color color) {
				this.color = color;
				this.buttons = buttons;
			}

			@Override
			public void doAction(ActionEvent e) {
				shapeColor = color;
				setBorderPainted((McWillIconButton) e.getSource(), buttons);
			}
		}

		class ShapeActionListener extends XActionListener {

			int shape;
			McWillIconButton[] buttons;

			public ShapeActionListener(McWillIconButton[] buttons, int shape) {
				this.shape = shape;
				this.buttons = buttons;
			}

			@Override
			public void doAction(ActionEvent e) {
				shapeType = shape;

				McWillIconButton source = (McWillIconButton) e.getSource();
				if (source == currentShapeButton && fontPanel.isVisible()) {
					moveTooPanel(null, false);
				} else {
					moveTooPanel(null, true);
				}
				currentShapeButton = source;
				setBorderPainted(source, buttons);

			}
		}

		class OperatActionListener extends XActionListener {
			@Override
			public void doAction(ActionEvent e) {
				Object source = e.getSource();
				if (source == undoButton) {
					if (!shapeList.isEmpty()) {
						shapeList.removeLast();
						paintShape();
					}
				} else if (source == saveButton) {
					if (captureImage == null)
						return;
					SwingUtil.saveImageFile(screenShotWindow, captureImage);
				} else if (source == cancelButton) {
					screenShotWindow.dispose();
					if (parentWindow != null) {
						parentWindow.setVisible(true);
					}
				} else if (source == okButton) {
					if (parentWindow != null) {
						parentWindow.setVisible(true);
					}
					screenShotWindow.dispose();
					doFinishX(captureImage);
				}
			}
		}

		abstract class XActionListener implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent e) {
				ScreenImagePanel.this.requestFocus();
				doAction(e);
			}

			public abstract void doAction(ActionEvent e);
		}

		class DrawMouseListener extends MouseAdapter {
			private int x, y, xEnd, yEnd; // 记录鼠标坐标

			public void mouseDragged(MouseEvent me) {
				if (shapeType <= 0) {
					return;
				}

				xEnd = me.getX();
				yEnd = me.getY();

				int x1 = x - select.x;
				int x2 = xEnd - select.x;
				int y1 = y - select.y;
				int y2 = yEnd - select.y;

				// 1:涂鸦,2:画线,3:画圆,4:矩形
				List<Shape> last = shapeList.getLast();
				if (shapeType == 1) {

				} else if (shapeType == 2) { // 画线
					last.clear();
				} else if (shapeType == 3) { // 画圆
					last.clear();
				} else if (shapeType == 4) { // 矩形
					last.clear();
				}
				last.add(new Shape(shapeType, x1, y1, x2, y2, shapeColor, shapeStroke));

				paintShape();

				if (shapeType == 1) { // 下次画线起点是设置为这次画线的终点
					x = xEnd;
					y = yEnd;
				}

			}

			public void mouseReleased(MouseEvent e) {
				if (shapeType > 0) {
					if (!shapeList.isEmpty()) {
						List<Shape> last = shapeList.getLast();
						if (last.isEmpty()) {
							shapeList.removeLast();
						}
					}
				}
			}

			public void mousePressed(MouseEvent me) {
				if (shapeType > 0) {// 鼠标按下的点，作为画线的最初的起点
					x = me.getX();
					y = me.getY();
					shapeList.addLast(new ArrayList<Shape>());
				}
			}

		}

	}

	enum States {
		NORTH_WEST(new Cursor(Cursor.NW_RESIZE_CURSOR)), //
		NORTH(new Cursor(Cursor.N_RESIZE_CURSOR)), //
		NORTH_EAST(new Cursor(Cursor.NE_RESIZE_CURSOR)), //
		EAST(new Cursor(Cursor.E_RESIZE_CURSOR)), //
		SOUTH_EAST(new Cursor(Cursor.SE_RESIZE_CURSOR)), //
		SOUTH(new Cursor(Cursor.S_RESIZE_CURSOR)), //
		SOUTH_WEST(new Cursor(Cursor.SW_RESIZE_CURSOR)), //
		WEST(new Cursor(Cursor.W_RESIZE_CURSOR)), //
		SELECT(new Cursor(Cursor.MOVE_CURSOR)), //
		DEFAULT(new Cursor(Cursor.DEFAULT_CURSOR)), //
		CUSTOM(Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon(States.class.getResource("images/cursor.png")).getImage(), new Point(0, 0), "ColorPicker"));
		private Cursor cursor;

		States(Cursor cursor) {
			this.cursor = cursor;
		}

		public Cursor getCursor() {
			return cursor;
		}
	}

	class Shape {
		public int type;
		public int x1;
		public int x2;
		public int y1;
		public int y2;

		public int stroke;
		public Color color;

		public Shape(int type, int x1, int y1, int x2, int y2, Color color, int stroke) {
			super();
			this.type = type;
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
			this.color = color;
			this.stroke = stroke;
		}
	}

	public Image getCursorImage() {
		// TODO Auto-generated method stub
		return null;
	}
}
