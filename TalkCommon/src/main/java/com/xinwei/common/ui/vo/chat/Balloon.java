/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月23日 下午4:21:54
 * 
 ***************************************************************/
package com.xinwei.common.ui.vo.chat;

import javax.swing.ImageIcon;

import com.android.ninepatch.NinePatch;

public class Balloon {
	NinePatch l;
	NinePatch r;
	ImageIcon background;
	String key;

	public Balloon() {
	}

	public Balloon(String key, NinePatch mPatchL, NinePatch mPatchR, ImageIcon background) {
		super();
		this.key = key;
		this.l = mPatchL;
		this.r = mPatchR;
		this.background = background;
	}

	public NinePatch getL() {
		return l;
	}

	public void setL(NinePatch mPatchL) {
		this.l = mPatchL;
	}

	public NinePatch getR() {
		return r;
	}

	public void setR(NinePatch mPatchR) {
		this.r = mPatchR;
	}

	public ImageIcon getBackground() {
		return background;
	}

	public void setBackground(ImageIcon background) {
		this.background = background;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
