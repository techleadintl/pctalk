package com.xinwei.common.resource;

import java.io.File;
import java.io.PrintStream;

public class I18nUtil
{
  public static void save(String key, String enFile, String enValue, String enComments, String cnFile, String cnValue, String cnComments, int line)
    throws Exception
  {
    boolean alreadyContain = (XinWeiProperties.containKey(new File(enFile), key)) || (XinWeiProperties.containKey(new File(cnFile), key));

    if (!alreadyContain) {
      XinWeiProperties.write(enFile, key, enValue, enComments, line);
      XinWeiProperties.write(cnFile, key, cnValue, cnComments, line);
      System.out.println(key + "-------------保存成功------------");
    } else {
      System.out.println(key + "~~~~~~~~~~~~~已经存在~~~~~~~~~~~~~~~~");
    }
  }

  public static void save(String enFile, String enComments, String cnFile, String cnComments, int line) throws Exception {
    XinWeiProperties.writecomment(enFile, enComments, line);
    XinWeiProperties.writecomment(cnFile, cnComments, line);
  }
}