package com.xinwei.common.resource;

import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

public class XRes {
	protected static Properties properties = new Properties();

	protected static Properties i18nProperties = new Properties();

	protected static Set<ClassLoader> classLoaderSet = new HashSet<ClassLoader>();
	public static XLoader instance;

	public static void addClassLoader(ClassLoader classLoader) {
		if (classLoader == null)
			return;
		synchronized (classLoaderSet) {
			classLoaderSet.add(classLoader);
		}
	}

	public static void addClassLoader(Class clazz) {
		if (clazz == null)
			return;
		addClassLoader(clazz.getClassLoader());
	}

	public static void addI18n(String... names) {
		addI18n(Locale.getDefault(), names);
	}

	public static void addI18n(Locale locale, String... names) {
		for (String name : names) {
			ResourceBundle bundle = getBundle(locale, name);
			if (bundle == null)
				continue;
			Enumeration<String> keys = bundle.getKeys();
			while (keys.hasMoreElements()) {
				String key = (String) keys.nextElement();
				i18nProperties.put(key, bundle.getObject(key));
			}
		}
	}

	public static void addI18n(Class<?> clazz, String... names) {
		addI18n(clazz, Locale.getDefault(), names);
	}

	public static void addI18n(Class<?> clazz, Locale locale, String... names) {
		if (clazz == null)
			return;
		ClassLoader classLoader = clazz.getClassLoader();

		for (String name : names) {
			ResourceBundle bundle = getBundle(classLoader, locale, name);
			if (bundle == null)
				continue;
			Enumeration<String> keys = bundle.getKeys();
			while (keys.hasMoreElements()) {
				String key = (String) keys.nextElement();
				i18nProperties.put(key, bundle.getObject(key));
			}
		}
		addClassLoader(classLoader);
	}

	public static void addResource(String... names) {
		for (String name : names) {
			if (!name.endsWith(".properties"))
				name = name + ".properties";
			try {
				InputStream resourceAsStream = getResourceAsStream(name);
				if (resourceAsStream != null)
					properties.load(resourceAsStream);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void addResource(Class<?> clazz, String... names) {
		if (clazz == null)
			return;
		ClassLoader classLoader = clazz.getClassLoader();
		for (String name : names) {
			if (!name.endsWith(".properties"))
				name = name + ".properties";
			try {
				InputStream resourceAsStream = getResourceAsStream(classLoader, name);
				if (resourceAsStream != null) {
					properties.load(resourceAsStream);
				}
				addClassLoader(classLoader);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected static InputStream getResourceAsStream(String name) {
		return getResourceAsStream(getClassLoader(), name);
	}

	protected static InputStream getResourceAsStream(ClassLoader classLoader, String name) {
		InputStream resourceAsStream = classLoader.getResourceAsStream(name);
		return resourceAsStream;
	}

	public static ResourceBundle getBundle(Locale locale, String name) {
		return getBundle(getClassLoader(), locale, name);
	}

	protected static ResourceBundle getBundle(ClassLoader classLoader, Locale locale, String name) {
		ResourceBundle bundle = null;
		try {
			bundle = ResourceBundle.getBundle(name, locale, classLoader);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bundle;
	}

	public static URL getURL(String path) {
		ClassLoader classLoader = getClassLoader();
		URL url = classLoader.getResource(path);
		return url;
	}

	public static void put(String key, Object value) {
		properties.put(key, value);
	}

	public static Object get(String key) {
		return properties.get(key);
	}

	public static Object get(String key, Object defaultValue) {
		Object val = get(key);
		return val == null ? defaultValue : val;
	}

	public static String getProperty(String key) {
		if (properties == null)
			return null;
		return properties.getProperty(key);
	}

	public static List<String> getPropertys(String key) {
		return getStrings(key);
	}

	public static List<String> getStrings(String startWith) {
		List<String> values = new ArrayList<String>();
		if (properties == null)
			return values;
		Set<String> keys = properties.stringPropertyNames();
		for (String key : keys) {
			boolean flag = key.startsWith(startWith);
			if (flag) {
				values.add(properties.getProperty(key));
			}
		}
		return values;
	}

	public static Map<String, String> getStringMap(String startWith) {
		Map<String, String> values = new LinkedHashMap<String, String>();
		if (properties == null)
			return values;
		Set<String> keys = properties.stringPropertyNames();
		for (String key : keys) {
			boolean flag = key.startsWith(startWith);
			if (flag) {
				values.put(key, properties.getProperty(key));
			}
		}
		return values;
	}

	public static String getProperty(String key, String defaultValue) {
		return (String) getValue(getProperty(key), defaultValue);
	}

	public static String getString(String key) {
		return getProperty(key);
	}

	public static String getString(String key, String defaultValue) {
		return getProperty(key, defaultValue);
	}

	public static Integer getInteger(String key) {
		String property = getProperty(key);
		try {
			return Integer.valueOf(Integer.parseInt(property));
		} catch (Exception localException) {
		}
		return null;
	}

	public static Integer getInteger(String key, Integer defaultValue) {
		return (Integer) getValue(getInteger(key), defaultValue);
	}

	public static Long getLong(String key) {
		String property = getProperty(key);
		try {
			return Long.valueOf(Long.parseLong(property));
		} catch (Exception localException) {
		}
		return null;
	}

	public static Long getLong(String key, Long defaultValue) {
		return (Long) getValue(getLong(key), defaultValue);
	}

	public static Float getFloat(String key) {
		String property = getProperty(key);
		try {
			return Float.valueOf(Float.parseFloat(property));
		} catch (Exception localException) {
		}
		return null;
	}

	public static Float getFloat(String key, Float defaultValue) {
		return (Float) getValue(getFloat(key), defaultValue);
	}

	public static Double getDouble(String key) {
		String property = getProperty(key);
		try {
			return Double.valueOf(Double.parseDouble(property));
		} catch (Exception localException) {
		}
		return null;
	}

	public static Double getDouble(String key, Double defaultValue) {
		return (Double) getValue(getDouble(key), defaultValue);
	}


	public static boolean getBoolean(String key) {
		if (key == null) {
			return false;
		}
		String value = getProperty(key);
		return Boolean.parseBoolean(value);
	}

	public static Boolean getBoolean(String key, boolean defaultValue) {
		if (key == null)
			return Boolean.valueOf(defaultValue);
		String value = getProperty(key);
		if (value == null) {
			return Boolean.valueOf(defaultValue);
		}
		return Boolean.valueOf(Boolean.parseBoolean(value));
	}

	protected static <T> T getValue(T value, T defaultValue) {
		if (value == null)
			return defaultValue;
		return value;
	}

	public static String getMessage(String propertyName) {
		String property = i18nProperties.getProperty(propertyName);
		if (property == null) {
			property = propertyName;
		}
		return property;
	}

	public static String getLabelMessage(String propertyName) {
		return getMessage(propertyName) + ":";
	}

	public static String getLabelMessage(String propertyName, Object... obj) {
		return getMessage(propertyName, obj) + ":";
	}

	public static String getMessage(String propertyName, Object... obj) {
		String msg = i18nProperties.getProperty(propertyName);

		if (msg == null) {
			return propertyName;
		}
		return MessageFormat.format(msg, obj);
	}

	public static synchronized ClassLoader getClassLoader() {
		if (instance == null) {
			instance = new XLoader();
		}
		return instance;
	}

	private static class XLoader extends URLClassLoader {
		public XLoader() {
			this(new URL[0], null);
		}

		public XLoader(URL[] path, ClassLoader parent) {
			super(path, parent);
		}

		protected Class findClass(String name) throws ClassNotFoundException {
			try {
				return super.findClass(name);
			} catch (ClassNotFoundException localClassNotFoundException1) {
				synchronized (XRes.classLoaderSet) {
					for (ClassLoader cl : XRes.classLoaderSet)
						try {
							Class clazz = cl.loadClass(name);
							return clazz;
						} catch (ClassNotFoundException localClassNotFoundException2) {
						}
				}
			}
			throw new ClassNotFoundException(name);
		}

		public URL findResource(String name) {
			URL url = super.findResource(name);
			if (url == null) {
				synchronized (XRes.classLoaderSet) {
					for (ClassLoader cl : XRes.classLoaderSet) {
						url = cl.getResource(name);
						if (url != null) {
							break;
						}
					}
				}
			}
			return url;
		}
	}
}