package com.xinwei.common.resource;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.Properties;

public class XinWeiProperties {
	private static final char[] hexDigit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static boolean containKey(File file, String key) throws Exception {
		FileInputStream inStream = null;
		try {
			Properties p = new Properties();
			inStream = new FileInputStream(file);
			p.load(inStream);
			boolean bool = p.containsKey(key);
			return bool;
		} catch (Exception e) {
			throw e;
		} finally {
			inStream.close();
		}
	}

	public static void write(String filePath, String key, String value) throws Exception {
		write(filePath, key, value, null);
	}

	public static void write(String filePath, String key, String value, String comments) throws Exception {
		write(new File(filePath), key, value, comments);
	}

	public static void write(URL url, String key, String value) throws Exception {
		write(url, key, value, null);
	}

	public static void write(URL url, String key, String value, String comments) throws Exception {
		write(new File(url.toURI()), key, value, comments);
	}

	public static void write(File file, String key, String value, String comments) throws Exception {
		write(file, key, value, comments, 0);
	}

	public static void write(String filePath, String key, String value, int separatorLine) throws Exception {
		write(filePath, key, value, null, separatorLine);
	}

	public static void write(String filePath, String key, String value, String comments, int separatorLine) throws Exception {
		write(new File(filePath), key, value, comments, separatorLine);
	}

	public static void write(URL url, String key, String value, int separatorLine) throws Exception {
		write(url, key, value, null, separatorLine);
	}

	public static void write(URL url, String key, String value, String comments, int separatorLine) throws Exception {
		write(new File(url.toURI()), key, value, comments, separatorLine);
	}

	public static void write(File file, String key, String value, String comments, int separatorLine) throws Exception {
		if (containKey(file, key)) {
			throw new IllegalArgumentException("[" + key + "] already exist!");
		}

		FileOutputStream out = new FileOutputStream(file, true);
		write(out, key, value, comments, separatorLine);
	}

	public static void write(OutputStream out, String key, String value, String comments, int separatorLine) throws Exception {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(out, "8859_1"));

			for (int i = 0; i < separatorLine; i++) {
				bw.newLine();
			}
			if (comments != null) {
				writeComments(bw, comments);
			}
			key = saveConvert(key, true, true);
			value = saveConvert(value, false, true);
			bw.write(key + "=" + value);
			bw.newLine();
			bw.flush();
		} catch (Exception e) {
			throw e;
		} finally {
			bw.close();
			out.close();
		}
	}

	public static void writecomment(String filePath, String comments, int separatorLine) throws Exception {
		FileOutputStream out = new FileOutputStream(filePath, true);
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(out, "8859_1"));

			for (int i = 0; i < separatorLine; i++) {
				bw.newLine();
			}
			if (comments != null) {
				writeComments(bw, comments);
			}
			bw.flush();
		} catch (Exception e) {
			throw e;
		} finally {
			bw.close();
			out.close();
		}
	}

	private static String saveConvert(String theString, boolean escapeSpace, boolean escapeUnicode) {
		int len = theString.length();
		int bufLen = len * 2;
		if (bufLen < 0) {
			bufLen = 2147483647;
		}
		StringBuffer outBuffer = new StringBuffer(bufLen);

		for (int x = 0; x < len; x++) {
			char aChar = theString.charAt(x);

			if ((aChar > '=') && (aChar < '')) {
				if (aChar == '\\') {
					outBuffer.append('\\');
					outBuffer.append('\\');
				} else {
					outBuffer.append(aChar);
				}
			} else
				switch (aChar) {
				case ' ':
					if ((x == 0) || (escapeSpace))
						outBuffer.append('\\');
					outBuffer.append(' ');
					break;
				case '\t':
					outBuffer.append('\\');
					outBuffer.append('t');
					break;
				case '\n':
					outBuffer.append('\\');
					outBuffer.append('n');
					break;
				case '\r':
					outBuffer.append('\\');
					outBuffer.append('r');
					break;
				case '\f':
					outBuffer.append('\\');
					outBuffer.append('f');
					break;
				case '!':
				case '#':
				case ':':
				case '=':
					outBuffer.append('\\');
					outBuffer.append(aChar);
					break;
				default:
					if ((((aChar < ' ') || (aChar > '~')) & escapeUnicode)) {
						outBuffer.append('\\');
						outBuffer.append('u');
						outBuffer.append(toHex(aChar >> '\f' & 0xF));
						outBuffer.append(toHex(aChar >> '\b' & 0xF));
						outBuffer.append(toHex(aChar >> '\004' & 0xF));
						outBuffer.append(toHex(aChar & 0xF));
					} else {
						outBuffer.append(aChar);
					}
				}
		}
		return outBuffer.toString();
	}

	private static void writeComments(BufferedWriter bw, String comments) throws IOException {
		bw.write("#");
		int len = comments.length();
		int current = 0;
		int last = 0;
		char[] uu = new char[6];
		uu[0] = '\\';
		uu[1] = 'u';
		while (current < len) {
			char c = comments.charAt(current);
			if ((c > 'ÿ') || (c == '\n') || (c == '\r')) {
				if (last != current)
					bw.write(comments.substring(last, current));
				if (c > 'ÿ') {
					uu[2] = toHex(c >> '\f' & 0xF);
					uu[3] = toHex(c >> '\b' & 0xF);
					uu[4] = toHex(c >> '\004' & 0xF);
					uu[5] = toHex(c & 0xF);
					bw.write(new String(uu));
				} else {
					bw.newLine();
					if ((c == '\r') && (current != len - 1) && (comments.charAt(current + 1) == '\n')) {
						current++;
					}
					if ((current == len - 1) || ((comments.charAt(current + 1) != '#') && (comments.charAt(current + 1) != '!')))
						bw.write("#");
				}
				last = current + 1;
			}
			current++;
		}
		if (last != current)
			bw.write(comments.substring(last, current));
		bw.newLine();
	}

	private static char toHex(int nibble) {
		return hexDigit[(nibble & 0xF)];
	}
}