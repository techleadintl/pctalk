package com.xinwei.common.resource;

import java.io.File;
import java.io.FileOutputStream;

public class I18nConfigurator {
	public static String dir = System.getProperty("user.dir") + "/";

	public static String i18nDir = "src/main/resources/i18n/";

	public static String baseName = "camtalk";

	public static String enFile = dir + i18nDir + baseName + ".properties";

	public static String cnFile = dir + i18nDir + baseName + "_zh_CN.properties";

	public static void init(String baseName) {
		baseName = baseName;

		enFile = dir + i18nDir + baseName + ".properties";
		cnFile = dir + i18nDir + baseName + "_zh_CN.properties";
	}

	public static void init(String i18nDir, String baseName) {
		i18nDir = i18nDir;
		baseName = baseName;

		enFile = dir + i18nDir + baseName + ".properties";
		cnFile = dir + i18nDir + baseName + "_zh_CN.properties";
	}

	public static void addI18nComment(String cnComment) throws Exception {
		I18nUtil.save(enFile, cnComment, cnFile, cnComment, 0);
	}

	public static void addI18nComment(String enComment, String cnComment) throws Exception {
		I18nUtil.save(enFile, enComment, cnFile, cnComment, 0);
	}

	public static void addI18nNoEmptyLine(String key, String enValue, String cnValue) throws Exception {
		I18nUtil.save(key, enFile, enValue, null, cnFile, cnValue, null, 0);
	}

	public static void addI18nNoEmptyLine(String key, String enValue, String cnValue, String enComments, String chComments) throws Exception {
		I18nUtil.save(key, enFile, enValue, enComments, cnFile, cnValue, chComments, 0);
	}

	public static void addI18n(String key, String enValue, String cnValue) throws Exception {
		I18nUtil.save(key, enFile, enValue, cnValue, cnFile, cnValue, null, 1);
	}

	public static void addI18n(String key, String enValue, String cnValue, String enComments, String chComments) throws Exception {
		I18nUtil.save(key, enFile, enValue, enComments, cnFile, cnValue, chComments, 1);
	}

	public static void clearI18n() throws Exception {
		File file = new File(enFile);
		if (!file.exists()) {
			file.createNewFile();
		}
		FileOutputStream ex = new FileOutputStream(file);
		ex.close();
		File file2 = new File(cnFile);
		if (!file2.exists()) {
			file2.createNewFile();
		}
		FileOutputStream cx = new FileOutputStream(file2);
		cx.close();
	}
}