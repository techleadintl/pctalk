package com.xinwei.common.ini;

import java.util.LinkedHashMap;
import java.util.Scanner;

import com.xinwei.common.lang.StringUtil;

public class Section extends LinkedHashMap<String, String>
{
  private static final long serialVersionUID = -898769225137944923L;
  private final String name;

  private Section(String name)
  {
    if (name == null) {
      throw new NullPointerException("name");
    }
    this.name = name;
  }

  public Section(String name, String sectionContent) {
    if (name == null) {
      throw new NullPointerException("name");
    }
    this.name = name;
    if (StringUtil.isNotBlank(sectionContent))
      toMapProps(sectionContent);
  }

  public Section(Section defaults) {
    this(defaults.getName());
    putAll(defaults);
  }

  protected static boolean isContinued(String line) {
    if (StringUtil.isBlank(line)) {
      return false;
    }
    int length = line.length();

    int backslashCount = 0;
    for (int i = length - 1; (i > 0) && (line.charAt(i) == '\\'); i--) {
      backslashCount++;
    }

    return backslashCount % 2 != 0;
  }

  private static boolean isKeyValueSeparatorChar(char c) {
    return (Character.isWhitespace(c)) || (c == ':') || (c == '=');
  }

  private static boolean isCharEscaped(CharSequence s, int index) {
    return (index > 0) && (s.charAt(index - 1) == '\\');
  }

  protected static String[] splitKeyValue(String keyValueLine)
  {
    String line = StringUtil.clean(keyValueLine);
    if (line == null) {
      return null;
    }
    StringBuilder keyBuffer = new StringBuilder();
    StringBuilder valueBuffer = new StringBuilder();

    boolean buildingKey = true;

    for (int i = 0; i < line.length(); i++) {
      char c = line.charAt(i);

      if (buildingKey) {
        if ((isKeyValueSeparatorChar(c)) && (!isCharEscaped(line, i)))
          buildingKey = false;
        else
          keyBuffer.append(c);
      } else {
        if ((valueBuffer.length() == 0) && (isKeyValueSeparatorChar(c)) && (!isCharEscaped(line, i))) {
          continue;
        }
        valueBuffer.append(c);
      }

    }

    String key = StringUtil.clean(keyBuffer.toString());
    String value = valueBuffer.toString();

    if ((key == null) || (value == null)) {
      String msg = "Line argument must contain a key and a value.  Only one string token was found.";
      throw new IllegalArgumentException(msg);
    }

    return new String[] { key, value };
  }

  private void toMapProps(String content)
  {
    StringBuilder lineBuffer = new StringBuilder();
    Scanner scanner = new Scanner(content);
    while (scanner.hasNextLine()) {
      String line = StringUtil.clean(scanner.nextLine());
      if (isContinued(line)) {
        line = line.substring(0, line.length() - 1);
        lineBuffer.append(line);
      }
      else {
        lineBuffer.append(line);

        line = lineBuffer.toString();
        lineBuffer = new StringBuilder();
        String[] kvPair = splitKeyValue(line);
        put(kvPair[0], kvPair[1]);
      }
    }
    scanner.close();
  }

  public String getName() {
    return this.name;
  }

  public String toString()
  {
    String name = getName();
    if ("".equals(name)) {
      return "<default>";
    }
    return name;
  }

  public boolean equals(Object obj) {
    if ((obj instanceof Section)) {
      Section other = (Section)obj;
      return (getName().equals(other.getName())) && (equals(other));
    }
    return false;
  }

  public int hashCode() {
    return this.name.hashCode() * 31 + hashCode();
  }
}