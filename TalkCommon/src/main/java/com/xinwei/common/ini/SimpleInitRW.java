package com.xinwei.common.ini;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lang.StringUtil;

public class SimpleInitRW {
	public static final String EMPTY_SECTION_NAME = "$EMPTY_SECTION$";
	public static final String D$ = "$";
	private static final String SR$ = "]";
	private static final String SL$ = "[";
	private static final String E$ = "=";
	private static final String $$ = "#";

	public static synchronized void setValue(String file, String sectionName, String key, Object value) {
		LinkedHashMap sectionMap = null;
		try {
			List lines = readFile(file);

			sectionMap = getSectionMap(lines);

			setValue(sectionMap, sectionName, key, value);

			lines = getInitFileContent(sectionMap);

			writeFile(file, lines);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static synchronized String getValue(String file, String sectionName, String key) {
		Map sectionMap = null;
		try {
			List lines = readFile(file);

			sectionMap = getSectionMap(lines);

			return getValue(sectionMap, sectionName, key);
		} catch (Exception localException) {
//			Log.error(localException);
		}
		return null;
	}

	public static synchronized Map<String, String> getValues(String file, String sectionName) {
		Map sectionMap = null;
		try {
			List lines = readFile(file);

			sectionMap = getSectionMap(lines);

			return getValues(sectionMap, sectionName);
		} catch (Exception localException) {
		}
		return null;
	}

	private static void setValue(LinkedHashMap<String, LinkedHashMap<String, String>> sectionMap, String sectionName, String key, Object value) {
		LinkedHashMap entryMap = (LinkedHashMap) sectionMap.get(sectionName);
		if (entryMap == null) {
			ArrayList arrayList = new ArrayList(sectionMap.keySet());
			if (!arrayList.isEmpty()) {
				LinkedHashMap linkedHashMap = (LinkedHashMap) sectionMap.get(arrayList.get(arrayList.size() - 1));
				linkedHashMap.put("#" + linkedHashMap.size(), "");
			}

			entryMap = new LinkedHashMap();
			sectionMap.put(sectionName, entryMap);
		}
		if (value == null)
			entryMap.remove(key);
		else
			entryMap.put(key, String.valueOf(value));
	}

	private static String getValue(Map<String, LinkedHashMap<String, String>> sectionMap, String sectionName, String key) {
		LinkedHashMap entryMap = (LinkedHashMap) sectionMap.get(sectionName);
		if (entryMap == null) {
			return null;
		}
		return (String) entryMap.get(key);
	}

	private static Map<String, String> getValues(Map<String, LinkedHashMap<String, String>> sectionMap, String sectionName) {
		return (Map) sectionMap.get(sectionName);
	}

	private static List<String> getInitFileContent(Map<String, LinkedHashMap<String, String>> sectionMap) {
		List lines = new ArrayList();
		Set<String> sectionSet = sectionMap.keySet();
		for (String section : sectionSet) {
			if (!"$EMPTY_SECTION$".equals(section)) {
				lines.add("[" + section + "]");
			}
			LinkedHashMap itemMap = (LinkedHashMap) sectionMap.get(section);
			Set<String> keySet = itemMap.keySet();
			for (String key : keySet) {
				String value = (String) itemMap.get(key);
				if (key.startsWith("#"))
					lines.add(value);
				else {
					lines.add(key + "=" + value);
				}
			}
		}
		return lines;
	}

	private static LinkedHashMap<String, LinkedHashMap<String, String>> getSectionMap(List<String> lines) {
		LinkedHashMap groupMap = new LinkedHashMap();
		String currentSection = "$EMPTY_SECTION$";
		groupMap.put("$EMPTY_SECTION$", new LinkedHashMap());

		for (String line : lines) {
			if ((line.startsWith("[")) && (line.endsWith("]"))) {
				String newSection = line.substring(1, line.length() - 1);
				LinkedHashMap itemMap = (LinkedHashMap) groupMap.get(newSection);
				if (itemMap == null) {
					itemMap = new LinkedHashMap();
				}
				currentSection = newSection;

				groupMap.put(currentSection, itemMap);
			} else {
				LinkedHashMap linkedHashMap = (LinkedHashMap) groupMap.get(currentSection);
				if ((StringUtil.isBlank(line)) || (line.startsWith("#")) || (line.startsWith(";"))) {
					linkedHashMap.put("#" + linkedHashMap.size(), line);
				} else {
					String[] items = line.split("=");
					if (items.length == 2)
						linkedHashMap.put(items[0].trim(), items[1].trim());
					else {
						linkedHashMap.put("#" + linkedHashMap.size(), line);
					}
				}
			}
		}
		return groupMap;
	}

	public static List<String> readFile(String filepath) throws IOException {
		Scanner scanner = new Scanner(new File(filepath));
		List list = new ArrayList();
		while (scanner.hasNextLine()) {
			list.add(scanner.nextLine());
		}
		return list;
	}

	private static void writeFile(String filepath, List<String> lines) throws IOException {
		IOUtil.writeLines(new File(filepath), lines);
	}
}