package com.xinwei.common.ini;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.Set;

import com.xinwei.common.lang.StringUtil;

public class Ini extends LinkedHashMap<String, Section>
{
  public static final String DEFAULT_SECTION_NAME = "$DEFAULT$";
  public static final String DEFAULT_CHARSET_NAME = "UTF-8";
  public static final String COMMENT_POUND = "#";
  public static final String COMMENT_SEMICOLON = ";";
  public static final String SECTION_PREFIX = "[";
  public static final String SECTION_SUFFIX = "]";
  protected static final char ESCAPE_TOKEN = '\\';

  public Ini()
  {
  }

  public Ini(Ini defaults)
  {
    super(defaults);
  }

  public Set<String> getSectionNames() {
    return keySet();
  }

  public Collection<Section> getSections() {
    return values();
  }

  public Section getSection(String sectionName) {
    String name = cleanName(sectionName);
    return (Section)get(name);
  }

  public Section addSection(String sectionName) {
    String name = cleanName(sectionName);
    Section section = getSection(name);
    if (section == null) {
      section = new Section(name, null);
      put(name, section);
    }
    return section;
  }

  public Section removeSection(String sectionName) {
    String name = cleanName(sectionName);
    return (Section)remove(name);
  }

  private static String cleanName(String sectionName) {
    String name = StringUtil.clean(sectionName);
    if (name == null) {
      name = "";
    }
    return name;
  }

  public void setSectionProperty(String sectionName, String propertyName, String propertyValue) {
    String name = cleanName(sectionName);
    Section section = getSection(name);
    if (section == null) {
      section = addSection(name);
    }
    section.put(propertyName, propertyValue);
  }

  public String getSectionProperty(String sectionName, String propertyName) {
    Section section = getSection(sectionName);
    return section != null ? (String)section.get(propertyName) : null;
  }

  public String getSectionProperty(String sectionName, String propertyName, String defaultValue) {
    String value = getSectionProperty(sectionName, propertyName);
    return value != null ? value : defaultValue;
  }

  private void addSection(String name, StringBuilder content) {
    if (content.length() > 0) {
      String contentString = content.toString();
      String cleaned = StringUtil.clean(contentString);
      if (cleaned != null) {
        Section section = new Section(name, contentString);
        if (!section.isEmpty())
          put(name, section);
      }
    }
  }

  public void load(Scanner scanner) {
    String sectionName = "$DEFAULT$";
    StringBuilder sectionContent = new StringBuilder();

    while (scanner.hasNextLine()) {
      String rawLine = scanner.nextLine();
      String line = StringUtil.clean(rawLine);

      if ((line == null) || (line.startsWith("#")) || (line.startsWith(";")))
      {
        continue;
      }
      String newSectionName = getSectionName(line);
      if (newSectionName != null) {
        addSection(sectionName, sectionContent);

        sectionContent = new StringBuilder();

        sectionName = newSectionName;
      }
      else {
        sectionContent.append(rawLine).append("\n");
      }

    }

    addSection(sectionName, sectionContent);
  }

  protected static String getSectionName(String line) {
    String s = StringUtil.clean(line);
    if ((s != null) && (s.startsWith("[")) && (s.endsWith("]"))) {
      return cleanName(s.substring(1, s.length() - 1));
    }
    return null;
  }

  public boolean equals(Object obj) {
    if ((obj instanceof Ini)) {
      Ini ini = (Ini)obj;
      return equals(ini);
    }
    return false;
  }

  public int hashCode() {
    return hashCode();
  }

  public String toString() {
    if (isEmpty()) {
      return "<empty INI>";
    }
    StringBuilder sb = new StringBuilder("sections=");
    int i = 0;
    for (Section section : values()) {
      if (i > 0) {
        sb.append(",");
      }
      sb.append(section.toString());
      i++;
    }
    return sb.toString();
  }

  public boolean isEmpty()
  {
    Collection<Section> sections = values();
    if (!sections.isEmpty()) {
      for (Section section : sections) {
        if (!section.isEmpty()) {
          return false;
        }
      }
    }
    return true;
  }
}