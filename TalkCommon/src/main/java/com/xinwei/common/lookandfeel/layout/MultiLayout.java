package com.xinwei.common.lookandfeel.layout;


import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Custom layout that allows multiply layout strategies to be applied to single container. Be aware that standard Swing layouts aren't developed to be used together so they aren't suitable for this
 * layout. You will need custom layouts which work only with specific components and won't affect the same components.
 *
 */

public class MultiLayout extends AbstractLayoutManager {
	/**
	 * Applied layout managers list.
	 */
	protected List<LayoutManager> layoutManagers;

	/**
	 * Constructs new MultiLayout.
	 */
	public MultiLayout() {
		super();
		layoutManagers = new ArrayList<LayoutManager>(2);
	}

	/**
	 * Returns applied layout managers.
	 *
	 * @return applied layout managers
	 */
	public List<LayoutManager> getLayoutManagers() {
		return layoutManagers;
	}

	/**
	 * Sets applied layout managers.
	 *
	 * @param layoutManagers
	 *            new applied layout managers
	 */
	public void setLayoutManagers(final List<LayoutManager> layoutManagers) {
		this.layoutManagers = layoutManagers;
	}

	/**
	 * Adds applied layout manager.
	 *
	 * @param layoutManager
	 *            applied layout manager to add
	 */
	public void addLayoutManager(final LayoutManager layoutManager) {
		this.layoutManagers.add(layoutManager);
	}

	/**
	 * Removes applied layout manager.
	 *
	 * @param layoutManager
	 *            applied layout manager to remove
	 */
	public void removeLayoutManager(final LayoutManager layoutManager) {
		this.layoutManagers.remove(layoutManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addLayoutComponent(final Component comp, final Object constraints) {
		for (final LayoutManager layoutManager : layoutManagers) {
			if (layoutManager instanceof LayoutManager2) {
				((LayoutManager2) layoutManager).addLayoutComponent(comp, constraints);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addLayoutComponent(final String name, final Component comp) {
		for (final LayoutManager layoutManager : layoutManagers) {
			layoutManager.addLayoutComponent(name, comp);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeLayoutComponent(final Component comp) {
		for (final LayoutManager layoutManager : layoutManagers) {
			layoutManager.removeLayoutComponent(comp);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addComponent(final Component component, final Object constraints) {
		for (final LayoutManager layoutManager : layoutManagers) {
			if (layoutManager instanceof LayoutManager2) {
				((LayoutManager2) layoutManager).addLayoutComponent(component, constraints);
			} else {
				layoutManager.addLayoutComponent(constraints == null ? null : constraints.toString(), component);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeComponent(final Component component) {
		for (final LayoutManager layoutManager : layoutManagers) {
			layoutManager.removeLayoutComponent(component);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension preferredLayoutSize(final Container parent) {
		Dimension ps = new Dimension(0, 0);
		for (final LayoutManager layoutManager : layoutManagers) {
			ps = max(ps, layoutManager.preferredLayoutSize(parent));
		}
		return ps;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension minimumLayoutSize(final Container parent) {
		Dimension ms = new Dimension(0, 0);
		for (final LayoutManager layoutManager : layoutManagers) {
			ms = max(ms, layoutManager.minimumLayoutSize(parent));
		}
		return ms;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension maximumLayoutSize(final Container parent) {
		Dimension ms = new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
		for (final LayoutManager layoutManager : layoutManagers) {
			if (layoutManager instanceof LayoutManager2) {
				ms = min(ms, ((LayoutManager2) layoutManager).maximumLayoutSize(parent));
			}
		}
		return ms;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void invalidateLayout(final Container parent) {
		for (final LayoutManager layoutManager : layoutManagers) {
			if (layoutManager instanceof LayoutManager2) {
				((LayoutManager2) layoutManager).invalidateLayout(parent);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void layoutContainer(final Container parent) {
		for (final LayoutManager layoutManager : layoutManagers) {
			layoutManager.layoutContainer(parent);
		}
	}

	public static Dimension max(final Dimension dimension1, final Dimension dimension2) {
		if (dimension1 == null && dimension2 == null) {
			return null;
		} else if (dimension1 == null) {
			return dimension2;
		} else if (dimension2 == null) {
			return dimension1;
		} else {
			return new Dimension(Math.max(dimension1.width, dimension2.width), Math.max(dimension1.height, dimension2.height));
		}
	}

	public static Dimension min(final Dimension dimension1, final Dimension dimension2) {
		if (dimension1 == null || dimension2 == null) {
			return null;
		} else {
			return new Dimension(Math.min(dimension1.width, dimension2.width), Math.min(dimension1.height, dimension2.height));
		}
	}
}