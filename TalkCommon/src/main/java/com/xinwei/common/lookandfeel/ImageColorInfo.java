package com.xinwei.common.lookandfeel;

import java.awt.Color;

public class ImageColorInfo {
	private Color appropriateColor;

	private Color bkgColor;

	private Color mostColor;

	private Color lessColor;

	private Color darkColor;

	private Color lightColor;

	private ThemeColor themeColor;

	public ImageColorInfo() {
	}

	public ImageColorInfo(Color color) {
		setMostColor(color);
		setLessColor(color);
		setDarkColor(color);
		setLightColor(color);
		setAppropriateColor(color);
		setBkgColor(color);
	}

	public Color getAppropriateColor() {
		return appropriateColor;
	}

	public void setAppropriateColor(Color appropriateColor) {
		this.appropriateColor = appropriateColor;
	}

	public Color getMostColor() {
		return mostColor;
	}

	public void setMostColor(Color mostColor) {
		this.mostColor = mostColor;
		themeColor = new ThemeColor(mostColor);
	}

	public Color getLessColor() {
		return lessColor;
	}

	public void setLessColor(Color lessColor) {
		this.lessColor = lessColor;
	}

	public Color getDarkColor() {
		return darkColor;
	}

	public void setDarkColor(Color darkColor) {
		this.darkColor = darkColor;
	}

	public Color getLightColor() {
		return lightColor;
	}

	public void setLightColor(Color lightColor) {
		this.lightColor = lightColor;
	}

	public Color getBkgColor() {
		return bkgColor;
	}

	public void setBkgColor(Color bkgColor) {
		this.bkgColor = bkgColor;
	}

	public ThemeColor getThemeColor() {
		return themeColor;
	}

	public void setThemeColor(ThemeColor themeColor) {
		this.themeColor = themeColor;
	}

}
