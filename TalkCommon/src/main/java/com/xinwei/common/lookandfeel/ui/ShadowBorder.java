package com.xinwei.common.lookandfeel.ui;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

public class ShadowBorder extends AbstractBorder {
	private static ShadowBorder instance;

	public static ShadowBorder getInstance() {
		if (instance == null) {
			instance = new ShadowBorder();
		}
		return instance;
	}

	private ShadowBorder() {
	}

	private Insets borderInsets = new Insets(0, 0, 5, 5);
	private Image shadow = new ImageIcon(ShadowBorder.class.getResource("shadow.png")).getImage();
	public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
		g.drawImage(shadow, x + 5, y + height - 5, x + 10, y + height, 0, 6, 5, 11, null, c);
		g.drawImage(shadow, x + 10, y + height - 5, x + width - 5, y + height, 5, 6, 6, 11, null, c);
		g.drawImage(shadow, x + width - 5, y + 5, x + width, y + 10, 6, 0, 11, 5, null, c);
		g.drawImage(shadow, x + width - 5, y + 10, x + width, y + height - 5, 6, 5, 11, 6, null, c);
		g.drawImage(shadow, x + width - 5, y + height - 5, x + width, y + height, 6, 6, 11, 11, null, c);
	}

	public Insets getBorderInsets(Component c) {
		return borderInsets;
	}

	public boolean isBorderOpaque() {
		return true;
	}
}
