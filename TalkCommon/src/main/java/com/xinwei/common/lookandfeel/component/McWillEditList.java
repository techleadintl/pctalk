package com.xinwei.common.lookandfeel.component;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.MouseInputListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;

import com.jtattoo.plaf.BaseTableUI;
import com.xinwei.common.lookandfeel.McWillTheme;

@SuppressWarnings({ "serial", "unchecked" })
public class McWillEditList<T> extends JPanel {
	XTable table;
	Class<T> clazz;

	public McWillEditList(List<T> values) {
		this();
		table.setData(values);
	}

	public McWillEditList(Class cellUIClass) {
		this();
		setCellUIClass(cellUIClass);
	}

	public McWillEditList() {
		this.clazz = getClassType();

		table = new XTable();

		table.setTableHeader(null);

		//		table.setShowGrid(false);
		table.setShowVerticalLines(false);

		table.setGridColor(McWillTheme.getThemeColor().getSrcColor());

		table.addPropertyChangeListener("rolloverChanged", new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				TableCellEditor cellEditor = table.getCellEditor((int) evt.getOldValue(), 0);
				if (cellEditor != null) {
					McWillEditListCellComponent cellEditor1 = (McWillEditListCellComponent) cellEditor;
					cellEditor1.onHoverLosted(table, (int) evt.getOldValue());
				}
			}
		});

		setLayout(new BorderLayout());
		add(table, BorderLayout.CENTER);
	}

	public Class getClassType() {
		Class clazz = getClass();

		while (clazz != Object.class) {
			Type t = clazz.getGenericSuperclass();
			if (t instanceof ParameterizedType) {
				Type[] args = ((ParameterizedType) t).getActualTypeArguments();
				if (args[0] instanceof Class) {
					this.clazz = (Class<T>) args[0];
					break;
				}
			}
			clazz = clazz.getSuperclass();
		}
		return clazz;
	}

	public JTable getTable() {
		return table;
	}

	public void setRowHeight(int rowHeight) {
		table.setRowHeight(rowHeight);
	}

	public void setCellUIClass(Class<? extends McWillEditListCellComponent> tableCell) {
		try {
			table.setDefaultRenderer(clazz, tableCell.newInstance());
			table.setDefaultEditor(clazz, tableCell.newInstance());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addElement(T t) {
		if (t == null)
			return;
		XTableModel model = (XTableModel) table.getModel();
		model.addRow(t);
	}

	public void insertElement(T t, int row) {
		if (t == null)
			return;
		XTableModel model = (XTableModel) table.getModel();
		model.insertRow(row, t);
	}

	public void removeElement(T t) {
		if (t == null)
			return;
		XTableModel model = (XTableModel) table.getModel();
		model.removeRow(t);
	}

	public void removeElement(int row) {
		XTableModel model = (XTableModel) table.getModel();
		model.removeRow(row);
	}

	public void removeAllElements() {
		XTableModel model = (XTableModel) table.getModel();
		model.removeRows();
	}

	public int indexOfElement(T t) {
		if (t == null) {
			return -1;
		}

		XTableModel model = (XTableModel) table.getModel();
		int rowCount = model.getRowCount();
		for (int row = 0; row < rowCount; row++) {
			Object value = table.getValueAt(row, 0);
			if (value == t) {
				return row;
			}
		}
		return -1;
	}

	public List<T> getElements() {
		List<T> list = new ArrayList<T>();
		int rowCount = table.getModel().getRowCount();
		for (int row = 0; row < rowCount; row++) {
			T value = (T) table.getValueAt(row, 0);
			list.add(value);
		}
		return list;
	}

	public T getElement(int index) {
		return (T) table.getModel().getValueAt(index, 0);
	}

	public T getFirstElement() {
		return (T) table.getModel().getValueAt(0, 0);
	}

	public T getLastElement() {
		javax.swing.table.TableModel model = table.getModel();
		return (T) model.getValueAt(model.getRowCount() - 1, 0);
	}

	public int getElementCount() {
		return table.getModel().getRowCount();
	}

	public T getSelectedElement() {
		int selectedRow = table.getSelectedRow();

		return (T) table.getValueAt(selectedRow, 0);
	}

	public int getSeletedElementIndex() {
		return table.getSelectedRow();
	}

	public void setSelectedElement(T t) {
		int rowCount = table.getModel().getRowCount();
		for (int row = 0; row < rowCount; row++) {
			Object value = table.getValueAt(row, 0);
			if (value == t) {
				table.setRowSelectionInterval(row, row);
			}
		}
	}

	public boolean isSelectedElement(T t) {
		if (t == null)
			return false;
		int selectedRow = table.getSelectedRow();

		int selectedColumn = table.getSelectedColumn();

		return t == table.getValueAt(selectedRow, selectedColumn);
	}

	protected boolean isXRowSelected(int row) {
		return true;
	}

	protected boolean isXRowEditable(int row) {
		return true;
	}

	public class XTable extends JTable {
		public int rolloverIndex = -1;

		public XTable() {
			super(new XTableModel());
		}

		public void updateUI() {
			setUI(new ListUI());
			invalidate();
		}

		public void setData(List<T> values) {
			setModel(new XTableModel(values));
		}

		@Override
		public boolean isRowSelected(int row) {
			return isXRowSelected(row);
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return isXRowEditable(row);
		}
	}

	public class XTableModel extends AbstractTableModel {
		List<T> values = new ArrayList<T>();

		public XTableModel() {
		}

		public XTableModel(List<T> values) {
			this.values = values;
		}

		public void removeRows() {
			if (values.isEmpty())
				return;

			int size = values.size();
			values.clear();
			fireTableRowsDeleted(0, size);
		}

		public void removeRow(int row) {
			values.remove(row);
			fireTableRowsDeleted(row, row);
		}

		public void removeRow(T row) {
			int index = values.indexOf(row);
			if (index != -1) {
				removeRow(index);
			}
		}

		public void insertRow(int row, T object) {
			values.add(row, object);
			fireTableRowsInserted(row, row);
		}

		public void addRow(T object) {
			insertRow(getRowCount(), object);
		}

		public Class<T> getColumnClass(int columnIndex) {
			return (Class<T>) clazz;
		}

		public int getColumnCount() {
			return 1;
		}

		public String getColumnName(int columnIndex) {
			return "";
		}

		public int getRowCount() {
			return (values == null) ? 0 : values.size();
		}

		public Object getValueAt(int rowIndex, int columnIndex) {
			return (values == null) ? null : values.get(rowIndex);
		}

		public boolean isCellEditable(int columnIndex, int rowIndex) {
			return true;
		}
	}

	public class ListUI extends BaseTableUI {
		@Override
		protected MouseInputListener createMouseInputListener() {
			return new ListMouseInputHandler();
		}

		private class ListMouseInputHandler extends MouseInputHandler {

			private int selIndex = -1;

			public void mousePressed(MouseEvent e) {
				if (getValidIndex(e.getPoint()) >= 0) {
					selIndex = table.getSelectedRow();
					super.mousePressed(e);
				}
			}

			public void mouseDragged(MouseEvent e) {
				if (getValidIndex(e.getPoint()) >= 0)
					super.mouseDragged(e);
			}

			public void mouseMoved(MouseEvent e) {
				mouseEntered(e);
			}

			public void mouseEntered(MouseEvent e) {
				if (table.isEnabled()) {
					setRolloverIndex(getValidIndex(e.getPoint()));
				}
			}

			public void mouseExited(MouseEvent e) {
				if (table.isEnabled())
					setRolloverIndex(-1);
			}

			public void mouseReleased(MouseEvent e) {
				if (getValidIndex(e.getPoint()) >= 0) {
					super.mouseReleased(e);
					if (selIndex > -1 && table.getSelectedRow() == selIndex) {
						table.removeRowSelectionInterval(selIndex, selIndex);
					}
				}
			}

			private void setRolloverIndex(int index) {
				int oldIndex = ((XTable) table).rolloverIndex;
				if (index != oldIndex) {
					((XTable) table).rolloverIndex = index;
					if (oldIndex > -1) {
						Rectangle r = table.getCellRect(oldIndex, 0, true);
						if (r != null) {
							table.repaint(r.x, r.y, r.width, r.height);
						}
						final TableCellEditor cellEditor = table.getCellEditor(oldIndex, 0);
						if (cellEditor != null) {
							table.firePropertyChange("rolloverChanged", oldIndex, index);
						}
					}
					if (index > -1) {
						Rectangle r = table.getCellRect(index, 0, true);
						if (r != null)
							table.repaint(r.x, r.y, r.width, r.height);
					}
				}
			}

			private int getValidIndex(Point p) {
				int row = table.rowAtPoint(p);
				return row >= 0 && table.getCellRect(row, 0, true).contains(p) ? row : -1;
			}
		}
	}
}
