/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月20日 下午2:37:22
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

public interface McWillWindowButtonUI {
	boolean isMax();

	boolean isIconify();
}
