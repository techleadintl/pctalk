package com.xinwei.common.lookandfeel.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Window;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.layout.VerticalFlowLayout;
import com.xinwei.common.lookandfeel.util.Constants;

public class McWillDialog extends JDialog implements McWillWindowButtonUI {

	private static final long serialVersionUID = 1L;

	public static int OK = 1;

	public static int CANCEL = 0;

	protected int result = CANCEL;

	public McWillDialog(Window window) {
		super(window instanceof JFrame ? (JFrame) window : (JDialog) window);
		
		setLayout(new BorderLayout());

		setResizable(false);

		setModal(true);

		setSize(400, 170);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		createUI();
	}

	public McWillDialog() {
		setLayout(new BorderLayout());

		setResizable(false);

		setModal(true);

		setSize(400, 170);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		createUI();
	}

	protected void createUI() {
		add(createContentPanel(), BorderLayout.CENTER);

		add(createButtonPanel(), BorderLayout.SOUTH);
	}

	public JPanel createContentPanel() {
		JPanel panel = new JPanel();
		panel.putClientProperty(Constants.COLOR_TYPE, Constants.COLOR_LIGHT);
		panel.setLayout(new BorderLayout(15, 0));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 0));

		//提示图标
		Component westPanel = createWest();

		//文本信息
		JPanel inputPanel = new JPanel(new BorderLayout());

		Component inputNorth = createInputNorth();
		if (inputNorth != null) {
			inputPanel.add(inputNorth, BorderLayout.NORTH);
		}

		Component inputInputCenter = createInputCenter();
		if (inputInputCenter != null) {
			inputPanel.add(inputInputCenter, BorderLayout.CENTER);
		}

		if (westPanel != null) {
			panel.add(westPanel, BorderLayout.WEST);
		}
		panel.add(inputPanel, BorderLayout.CENTER);

		return panel;

	}

	protected Component createWest() {
		return null;
	}

	protected Component createInputNorth() {
		return null;
	}

	protected Component createInputCenter() {
		return null;
	}

	public JPanel createButtonPanel() {
		JPanel panel = new JPanel();
		panel.putClientProperty(Constants.COLOR_TYPE, Constants.COLOR_SUBLIGHT);
		panel.setLayout(new FlowLayout(VerticalFlowLayout.RIGHT));

		JButton button1 = createButton1();
		if (button1 != null) {
			panel.add(button1);
		}
		JButton button2 = createButton2();
		if (button2 != null) {
			panel.add(button2);
		}

		List<JButton> buttons = createOtherButtonList();
		if (buttons != null) {
			for (JButton button : buttons) {
				panel.add(button);
			}
		}
		return panel;
	}

	private List<JButton> createOtherButtonList() {
		return null;
	}

	protected JButton createButton2() {
		return null;
	}

	protected JButton createButton1() {
		return null;
	}

	@Override
	public boolean isMax() {
		return false;
	}

	@Override
	public boolean isIconify() {
		return false;
	}

	public int getResult() {
		return result;
	}
}
