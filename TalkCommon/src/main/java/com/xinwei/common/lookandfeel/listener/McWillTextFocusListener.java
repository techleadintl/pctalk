package com.xinwei.common.lookandfeel.listener;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.UIManager;
import javax.swing.text.JTextComponent;

public class McWillTextFocusListener implements FocusListener {
	private Color defalutColor = (Color) UIManager.get("TextField.lightforeground");
	
	private Color foreground;
	
	private JTextComponent textField;
	
	private String defaultText;

	public McWillTextFocusListener(JTextComponent textField) {
		this.foreground = textField.getForeground();
		this.defaultText = textField.getText();
		this.textField = textField;
		textField.setForeground(defalutColor);

	}

	@Override
	public void focusGained(FocusEvent e) {
		if (defaultText.equals(textField.getText())) {
			textField.setText("");
			textField.setForeground(foreground);
		}

	}

	@Override
	public void focusLost(FocusEvent e) {
		if ("".equals(textField.getText())) {
			textField.setText(defaultText);
			textField.setForeground(defalutColor);
		}
	}
}
