package com.xinwei.common.lookandfeel.component.text;

import javax.swing.Icon;

import com.xinwei.common.lang.StringUtil;

public class McWillSearchTextField extends McWillTextInputPanel {
	private static final long serialVersionUID = -7000758637988415370L;

	private SearchListener searchListener;
	protected Icon searchIcon;
	protected Icon deleteIcon;

	public McWillSearchTextField(final Icon searchIcon) {
		this(searchIcon, null);
	}

	public McWillSearchTextField(final Icon searchIcon, final Icon deleteIcon) {
		super("");
		this.searchIcon = searchIcon;
		this.deleteIcon = deleteIcon;
	}

	public McWillSearchTextField(String hint, final Icon searchIcon, final Icon deleteIcon) {
		super(hint);
		this.searchIcon = searchIcon;
		this.deleteIcon = deleteIcon;
		if (searchIcon != null) {
			button.setIcon(searchIcon);
			button.setEnabled(false);;
		}
	}

	protected void onDocumentChanged() {
		if (StringUtil.isNotEmpty(textField.getText())) {
			if (deleteIcon != null) {
				button.setEnabled(true);
				button.setIcon(deleteIcon);
			}
		} else {
			if (searchIcon != null) {
				button.setEnabled(false);
				button.setIcon(searchIcon);
			}
		}
	}

	public void setHint(String hint) {
		textField.setHint(hint);
	}

	protected void onButtonClick() {
		if (StringUtil.isNotEmpty(textField.getText())) {
			textField.setText("");
			button.requestFocus();
		}
		if (searchListener != null) {
			searchListener.doButtonClick();
		}
	}

	public void doSearch(String text) {
		if (searchListener != null) {
			searchListener.doSearch(text);
		}
	}

	public void setText(String text) {
		textField.setText(text);
	}

	public String getText() {
		return textField.getText();
	}

	public void setIcon(Icon icon) {
		button.setIcon(icon);
	}

	public Icon getIcon() {
		return button.getIcon();
	}

	public SearchListener getSearchListener() {
		return searchListener;
	}

	public void setSearchListener(SearchListener searchListener) {
		this.searchListener = searchListener;
	}

	public static interface SearchListener {
		public void doSearch(String text);

		public void doButtonClick();
	}

}
