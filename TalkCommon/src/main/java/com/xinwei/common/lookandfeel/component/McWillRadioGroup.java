package com.xinwei.common.lookandfeel.component;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JRadioButton;

public class McWillRadioGroup<T> {

	private ButtonGroup groupButtons = new ButtonGroup();
	private Map<T, JRadioButton> map = new LinkedHashMap<>();

	private List<ActionListener> actionListeners = new ArrayList<ActionListener>();

	public JRadioButton addRadio(String text, boolean selected, final T t) {
		return addRadio(null, text, selected, t);
	}

	public JRadioButton addRadio(JComponent parent, String text, boolean selected, final T t) {
		final JRadioButton button = new JRadioButton(text, selected);

		groupButtons.add(button);
		map.put(t, button);

		if (parent != null) {
			parent.add(button);
		}

		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (ActionListener actionListener : actionListeners) {
					e.setSource(McWillRadioGroup.this);
					actionListener.actionPerformed(e);
				}
			}
		});
		return button;
	}

	public void addActionListener(ActionListener l) {
		actionListeners.add(l);
	}

	public T getSelected() {
		Set<Entry<T, JRadioButton>> entrySet = map.entrySet();
		for (Entry<T, JRadioButton> entry : entrySet) {
			if (entry.getValue().isSelected()) {
				return entry.getKey();
			}
		}
		return null;
	}

	public void setSelected(T t) {
		if (t == null)
			return;

		JRadioButton button = map.get(t);

		if (button == null) {
			return;
		}
		button.setSelected(true);
	}

}