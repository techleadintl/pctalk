/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月2日 下午6:50:44
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.xinwei.common.lang.Log4j;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.util.ColorUtil;
import com.xinwei.spark.ModelUtil;

public class McWillCollapsiblePane extends JPanel {

	private static final long serialVersionUID = -6770924580102536726L;
	private BaseCollapsibleTitlePane titlePane;
	private JPanel mainPanel;

	private List<CollapsiblePaneListener> listeners = new ArrayList<CollapsiblePaneListener>();

	private boolean subPane;

	public McWillCollapsiblePane() {
		setLayout(new BorderLayout());

		titlePane = new CollapsibleTitlePane();
		mainPanel = new JPanel();

		add(titlePane, BorderLayout.NORTH);
		add(mainPanel, BorderLayout.CENTER);

		mainPanel.setLayout(new BorderLayout());

		titlePane.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.isPopupTrigger()) {
					return;
				}
				boolean isCollapsed = titlePane.isCollapsed();
				setCollapsed(!isCollapsed);
			}
		});
	}

	/**
	 * Creates a CollapsiblePane.
	 * 
	 * @param tabTitle
	 *            the tabTitle to use.
	 */
	public McWillCollapsiblePane(String title) {
		this();
		setTitle(title);
	}

	/**
	 * Set the tabTitle of the Collapsible Pane.
	 * 
	 * @param tabTitle
	 *            the collapsible pane tabTitle.
	 */
	public void setTitle(String title) {
		titlePane.setTitle(title);
	}

	public void setIcon(Icon icon) {
		titlePane.setIcon(icon);
	}

	public void setCollapsed(boolean collapsed) {
		titlePane.setCollapsed(collapsed);
		mainPanel.setVisible(!collapsed);

		if (collapsed) {
			firePaneCollapsed();
		} else {
			firePaneExpanded();
		}
	}

	public void setContentPane(Component comp) {
		mainPanel.add(comp);
	}

	public void addCollapsiblePaneListener(CollapsiblePaneListener listener) {
		listeners.add(listener);
	}

	public void removeCollapsiblePaneListener(CollapsiblePaneListener listener) {
		listeners.remove(listener);
	}

	private void firePaneExpanded() {
		final Iterator<CollapsiblePaneListener> iter = ModelUtil.reverseListIterator(listeners.listIterator());
		while (iter.hasNext()) {
			(iter.next()).paneExpanded();
		}
	}

	private void firePaneCollapsed() {
		final Iterator<CollapsiblePaneListener> iter = ModelUtil.reverseListIterator(listeners.listIterator());
		while (iter.hasNext()) {
			(iter.next()).paneCollapsed();
		}
	}

	public BaseCollapsibleTitlePane getTitlePane() {
		return titlePane;
	}

	protected void setTitlePane(BaseCollapsibleTitlePane titlePane) {
		this.titlePane = titlePane;
	}

	public boolean isCollapsed() {
		return titlePane.isCollapsed();
	}

	public boolean isSubPane() {
		return subPane;
	}

	public void setSubPane(boolean subPane) {
		this.subPane = subPane;

		titlePane.setSubPane(subPane);
	}

	protected ImageIcon getPaneMinuSignImage() {
		return new ImageIcon(McWillCollapsiblePane.class.getResource("images/sign-minus.png"));
	}

	protected ImageIcon getPanePlusSignImage() {
		return new ImageIcon(McWillCollapsiblePane.class.getResource("images/sign-plus.png"));
	}

	protected ImageIcon getPaneUpArrowImage() {
		return new ImageIcon(McWillCollapsiblePane.class.getResource("images/arrow-right.png"));
	}

	protected ImageIcon getPaneDownArrowImage() {
		return new ImageIcon(McWillCollapsiblePane.class.getResource("images/arrow-down.png"));
	}

	public class CollapsibleTitlePane extends BaseCollapsibleTitlePane {
		private static final long serialVersionUID = 2528585101535037612L;
		private JLabel titleLabel;
		private JLabel iconLabel;

		private JLabel preIconLabel;

		private boolean collapsed;

		private Color startColor;
		private Color endColor;

		private Color titleColor;

		private boolean subPane;

		private Image backgroundImage;

		public CollapsibleTitlePane() {
			setLayout(new GridBagLayout());

			titleColor = new Color(33, 93, 198);
			Font titleFont = new Font("Dialog", Font.BOLD, 11);

			// Initialize color
			startColor = new Color(238, 242, 253);
			endColor = Color.white;
			titleLabel = new JLabel();
			iconLabel = new JLabel();

			preIconLabel = new JLabel();

			add(preIconLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 2, 0), 0, 0));
			add(titleLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 2), 0, 0));
			add(iconLabel, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 2), 0, 0));

			setCollapsed(false);

			Color customTitleColor = (Color) UIManager.get("CollapsiblePane.titleColor");
			if (customTitleColor != null) {
				titleLabel.setForeground(customTitleColor);
			} else {
				titleLabel.setForeground(titleColor);
			}

			titleLabel.setFont(titleFont);

			addMouseListener(new MouseAdapter() {
				public void mouseEntered(MouseEvent e) {
					setCursor(SwingUtil.HAND_CURSOR);
				}

				public void mouseExited(MouseEvent e) {
					setCursor(SwingUtil.DEFAULT_CURSOR);
				}
			});

		}

		public void setStartColor(Color color) {
			// Initialize color
			startColor = color;
		}

		public void setEndColor(Color color) {
			endColor = color;
		}

		public void setIcon(Icon icon) {
			titleLabel.setIcon(icon);
		}

		public void setTitle(String title) {
			titleLabel.setText(title);
		}

		public boolean isCollapsed() {
			return collapsed;
		}

		public void setCollapsed(boolean collapsed) {
			this.collapsed = collapsed;

			if (!isSubPane()) {
				if (!collapsed) {
					preIconLabel.setIcon(getPaneDownArrowImage());
				} else {
					preIconLabel.setIcon(getPaneUpArrowImage());
				}
			} else {
				iconLabel.setIcon(null);
				if (collapsed) {
					preIconLabel.setIcon(getPanePlusSignImage());
				} else {
					preIconLabel.setIcon(getPaneMinuSignImage());
				}
			}
		}

		public void setTitleColor(Color color) {
			titleColor = color;

			titleLabel.setForeground(color);
		}

		public Color getTitleColor() {
			return titleColor;
		}

		public void paintComponent(Graphics g) {
			if (backgroundImage != null) {
				double scaleX = getWidth() / (double) backgroundImage.getWidth(null);
				double scaleY = getHeight() / (double) backgroundImage.getHeight(null);
				AffineTransform xform = AffineTransform.getScaleInstance(scaleX, scaleY);
				((Graphics2D) g).drawImage(backgroundImage, xform, this);
				return;
			}

			Color stopColor = endColor;
			Color starterColor = startColor;

			Color customStartColor = (Color) UIManager.get("CollapsiblePane.startColor");
			Color customEndColor = (Color) UIManager.get("CollapsiblePane.endColor");

			if (customEndColor != null) {
				stopColor = customEndColor;
			}

			if (customStartColor != null) {
				starterColor = customStartColor;
			}

			if (isSubPane()) {
				stopColor = ColorUtil.lighter(stopColor, 0.05);
			}

			Graphics2D g2 = (Graphics2D) g;

			int w = getWidth();
			int h = getHeight();

			GradientPaint gradient = new GradientPaint(0, 0, starterColor, w, h, stopColor, true);
			g2.setPaint(gradient);
			g2.fillRect(0, 0, w, h);
		}

		protected boolean isSubPane() {
			return subPane;
		}

		@Override
		public void setSubPane(boolean subPane) {
			this.subPane = subPane;
			setCollapsed(isCollapsed());
		}

		public Color getColor(String commaColorString) {
			Color color;
			try {
				StringTokenizer tkn = new StringTokenizer(commaColorString, ",");
				color = new Color(Integer.parseInt(tkn.nextToken()), Integer.parseInt(tkn.nextToken()), Integer.parseInt(tkn.nextToken()));
			} catch (NumberFormatException e1) {
				Log4j.error(e1);
				return Color.white;
			}
			return color;
		}

		public void setTitleForeground(Color color) {
			titleLabel.setForeground(color);
		}

		public void useImageAsBackground(Image image) {
			this.backgroundImage = image;
		}
	}

	public abstract class BaseCollapsibleTitlePane extends JPanel implements ICollapsibleTitlePane {
		private static final long serialVersionUID = 8353157752528740510L;
	}

	public interface ICollapsibleTitlePane {
		void setIcon(Icon icon);

		void setTitle(String title);

		boolean isCollapsed();

		void setCollapsed(boolean collapsed);

		void setSubPane(boolean subPane);
	}

	public interface CollapsiblePaneListener {

		void paneExpanded();

		void paneCollapsed();
	}
}
