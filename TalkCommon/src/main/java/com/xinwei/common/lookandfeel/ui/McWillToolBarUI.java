package com.xinwei.common.lookandfeel.ui;

import com.jtattoo.plaf.*;
import com.xinwei.common.lookandfeel.McWillBorders;

import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;

public class McWillToolBarUI extends AbstractToolBarUI {

    public static ComponentUI createUI(JComponent c) {
        return new McWillToolBarUI();
    }

    public Border getRolloverBorder() {
        return McWillBorders.getRolloverToolButtonBorder();
    }

    public Border getNonRolloverBorder() {
        return McWillBorders.getToolButtonBorder();
    }

    public boolean isButtonOpaque() {
        return false;
    }

    public void paint(Graphics g, JComponent c) {
        int w = c.getWidth();
        int h = c.getHeight();
        JTattooUtilities.fillHorGradient(g, AbstractLookAndFeel.getTheme().getToolBarColors(), 0, 0, w, h);
        g.setColor(ColorHelper.darker(AbstractLookAndFeel.getToolbarColorDark(), 10));
        g.drawLine(0, 0, w, 0);
    }
}
