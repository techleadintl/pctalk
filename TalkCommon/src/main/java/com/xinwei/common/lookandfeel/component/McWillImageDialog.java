/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月21日 下午1:56:59
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.Window;

import javax.swing.JFileChooser;

import com.xinwei.talk.ui.skin.skin.filechooser.GenericFileFilter;
import com.xinwei.talk.ui.skin.skin.filechooser.GenericFileView;
import com.xinwei.talk.ui.skin.skin.filechooser.ImagePreviewPanel;

public class McWillImageDialog extends McWillFileDialog {
	public McWillImageDialog() {
		this(true);
	}

	public McWillImageDialog(Window parentDialog) {
		this(true);
	}

	public McWillImageDialog(boolean flag) {
		ImagePreviewPanel preview = new ImagePreviewPanel();
		preview.setOpaque(false);
		setAccessory(preview);
		addPropertyChangeListener(preview);
		setFileSelectionMode(JFileChooser.FILES_ONLY);

		// add Filters
		if (flag) {
			setAcceptAllFileFilterUsed(false);
			String imageExts[] = { "bmp", "png", "gif", "jpg", "jpeg" };

			GenericFileFilter filter = new GenericFileFilter(imageExts, getDescriptionPrefix() + "(*.jpg;*.jpeg;*.png;*.gif;*.bmp;)");
			addChoosableFileFilter(filter);
			setFileView(new GenericFileView(imageExts, GenericFileView.IMAGE_FILEVIEW));
		}
	}

	protected String getDescriptionPrefix() {
		return "Images Files";
	}
}
