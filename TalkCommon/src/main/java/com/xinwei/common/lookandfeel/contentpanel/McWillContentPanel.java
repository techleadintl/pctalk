package com.xinwei.common.lookandfeel.contentpanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JComponent;
import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.util.Constants;

@SuppressWarnings("serial")
public class McWillContentPanel extends JPanel {
	protected Component titlePanel;
	private boolean textVisible = true;
	private boolean iconVisible = false;
	private boolean iconFilter = true;
	private boolean skinVisible = false;
	protected Map<Integer, Set<JComponent>> opaqueMap = new HashMap<Integer, Set<JComponent>>();
	protected Map<Integer, Set<JComponent>> rgbMap = new HashMap<Integer, Set<JComponent>>();

	public McWillContentPanel() {
		super();
		this.setLayout(new BorderLayout());
	}

	@Override
	public void add(Component comp, Object constraints) {
		if (BorderLayout.NORTH.equals(constraints)) {
			throw new IllegalArgumentException("BorderLayout.NORTH is not allowed to set");
		}
		super.add(comp, constraints);
	}

	public void addTitlePanle(Component comp) {
		titlePanel = comp;
		super.add(comp, BorderLayout.NORTH);
	}

	public void setTitleTextVisible(boolean visible) {
		this.textVisible = visible;
	}

	public void setTitleIconVisible(boolean iconVisible) {
		this.iconVisible = iconVisible;
	}

	public boolean isTextVisible() {
		return textVisible;
	}

	public boolean isIconVisible() {
		return iconVisible;
	}

	public Component getTitlePanel() {
		return titlePanel;
	}

	@Override
	public void paint(Graphics g) {
		initColor(this);
		init(this);
		super.paint(g);
		dispose(this);
		opaqueMap.clear();
		rgbMap.clear();
	}

	@Override
	protected void paintComponent(Graphics g) {
		rgbMap.clear();
		initColor(this);
		setTypeColor();
		setRgbColor();
		super.paintComponent(g);
	}

	protected void init(JComponent parent) {

	}

	protected void dispose(JComponent parent) {

	}

	protected void initColor(JComponent parent) {
		Object clientProperty = parent.getClientProperty(Constants.COLOR_TYPE);
		if (clientProperty != null && clientProperty instanceof Number) {
			int colorType = ((Number) clientProperty).intValue();
			Set<JComponent> set = opaqueMap.get(colorType);
			if (set == null) {
				set = new HashSet<JComponent>();
				opaqueMap.put(colorType, set);
			}
			set.add(parent);
		}
		clientProperty = parent.getClientProperty(Constants.COLOR_RGB);
		if (clientProperty != null) {
			Integer color = null;
			if (clientProperty instanceof Number) {
				color = ((Number) clientProperty).intValue();
			} else if (clientProperty instanceof Color) {
				color = ((Color) clientProperty).getRGB();
			}
			if (color != null) {
				Set<JComponent> set = opaqueMap.get(color);
				if (set == null) {
					set = new HashSet<JComponent>();
					rgbMap.put(color, set);
				}
				set.add(parent);
			}
		}

		Component[] components = parent.getComponents();
		for (Component child : components) {
			if (child instanceof JComponent) {
				initColor((JComponent) child);
			}
		}

	}

	protected void setRgbColor() {
		Set<Entry<Integer, Set<JComponent>>> rgbEntrySet = rgbMap.entrySet();
		for (Entry<Integer, Set<JComponent>> entry : rgbEntrySet) {
			Integer rgb = entry.getKey();
			Set<JComponent> components = entry.getValue();
			for (JComponent com : components) {
				com.setOpaque(true);
				com.setBackground(new Color(rgb));
			}
		}
	}

	protected void setTypeColor() {
		ThemeColor themeColor = McWillTheme.getThemeColor();
		Set<Entry<Integer, Set<JComponent>>> entrySet = opaqueMap.entrySet();
		for (Entry<Integer, Set<JComponent>> entry : entrySet) {
			Integer key = entry.getKey();
			Set<JComponent> components = entry.getValue();
			for (JComponent com : components) {
				com.setOpaque(true);
				switch (key) {
				case Constants.COLOR_SRC:
					com.setBackground(themeColor.getSrcColor());
					break;
				case Constants.COLOR_LIGHT:
					com.setBackground(themeColor.getLightColor());
					break;
				case Constants.COLOR_SUBLIGHT:
					com.setBackground(themeColor.getSubLightColor());
					break;
				case Constants.COLOR_MIDDLE:
					com.setBackground(themeColor.getMiddleColor());
					break;
				case Constants.COLOR_MIDDLE1:
					com.setBackground(themeColor.getMiddleColor1());
					break;
				case Constants.COLOR_DARK:
					com.setBackground(themeColor.getDarkColor());
					break;
				case Constants.COLOR_SUBDARK:
					com.setBackground(themeColor.getSubDarkColor());
					break;
				case Constants.COLOR_DEST:
					com.setBackground(themeColor.getDestColor());
					break;
				default://指定透明颜色，可以使用负数
					com.setBackground(themeColor.getAlphColor(Math.abs(key)));
					break;
				}
			}
		}
	}

	protected boolean isShowing(JComponent parent) {
		return parent != null && parent.isShowing();
	}

	public boolean isSkinVisible() {
		return skinVisible;
	}

	public void setTitleSkinVisible(boolean skinVisible) {
		this.skinVisible = skinVisible;
	}

	public void setTitleTextIconVisible(boolean textIconVisible) {
		this.textVisible = textIconVisible;
		this.iconVisible = textIconVisible;
	}

	public boolean isIconFilter() {
		return iconFilter;
	}

	public void setTitleIconFilter(boolean iconFilter) {
		this.iconFilter = iconFilter;
	}
}