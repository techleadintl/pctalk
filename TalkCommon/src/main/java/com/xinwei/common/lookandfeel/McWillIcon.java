package com.xinwei.common.lookandfeel;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class McWillIcon implements Icon {
	private Icon icon = null;

	public McWillIcon(String filename) {
		this(new ImageIcon(filename));
	}

	public McWillIcon(URL location, String description) {
		this(new ImageIcon(location, description));
	}

	public McWillIcon(URL location) {
		this(new ImageIcon(location));
	}

	public McWillIcon(Image image, String description) {
		this(new ImageIcon(image, description));
	}

	public McWillIcon(Image image) {
		this(new ImageIcon(image));
	}

	public McWillIcon(byte[] imageData, String description) {
		this(new ImageIcon(imageData, description));
	}

	public McWillIcon(byte[] imageData) {
		this(new ImageIcon(imageData));
	}


	public McWillIcon(Icon icon) {
		this.icon = icon;
	}

	@Override
	public int getIconHeight() {
		return icon.getIconHeight();
	}

	@Override
	public int getIconWidth() {
		return icon.getIconWidth();
	}

	public void paintIcon(Component c, Graphics g, int x, int y) {
		float wid = c.getWidth();
		float hei = c.getHeight();
		int iconWid = icon.getIconWidth();
		int iconHei = icon.getIconHeight();

		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2d.scale(wid / iconWid, hei / iconHei);
		icon.paintIcon(c, g2d, 0, 0);
	}
}