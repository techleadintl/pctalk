package com.xinwei.common.lookandfeel.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.color.ColorSpace;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Element;
import javax.swing.text.ElementIterator;
import javax.swing.text.Highlighter.HighlightPainter;
import javax.swing.text.StyleConstants;

public class TalkHighlighter extends DefaultHighlighter {
	private static final String ELEM = AbstractDocument.ElementNameAttribute;
	private static final String ICON = StyleConstants.IconElementName;
	private static final String COMP = StyleConstants.ComponentElementName;

	private static final String BACK_COLOR = "&background&";

	private static final String BACK_IMG = "&backgroundimg&";

	private static final Color SELECTED_COLOR = Color.BLUE;
	private JTextPane textPane;

	public TalkHighlighter(JTextPane textPane) {
		this.textPane = textPane;
	}

	@Override
	public Object addHighlight(int p0, int p1, HighlightPainter p) throws BadLocationException {
		Object tag = super.addHighlight(p0, p1, p);
		/* notify embedded components ... */
		changeHighlight(p0, p1);
		return tag;
	}

	@Override
	public void removeHighlight(Object tag) {
		super.removeHighlight(tag);
		/* notify embedded components ... */
		changeHighlight(-1, -1);
	}

	@Override
	public void removeAllHighlights() {
		super.removeAllHighlights();
		/* notify embedded components ... */
		changeHighlight(-1, -1);
	}

	@Override
	public void changeHighlight(Object tag, int p0, int p1) throws BadLocationException {
		super.changeHighlight(tag, p0, p1);
		/* notify embedded components ... */
		changeHighlight(p0, p1);
	}

	public void changeHighlight(int p1, int p2) {
		ElementIterator iterator = new ElementIterator(textPane.getDocument());
		Element element;
		while ((element = iterator.next()) != null) {
			AttributeSet as = element.getAttributes();
			//				if (as.containsAttribute(ELEM, ICON)) {
			//				}

			if (as.containsAttribute(ELEM, COMP)) {
				Component component = StyleConstants.getComponent(as);

				if (!(component instanceof JLabel)) {
					continue;
				}

				ImageIcon background = (ImageIcon) ((JComponent) component).getClientProperty(BACK_IMG);

				int startOffset = element.getStartOffset();
				int endOffset = element.getEndOffset();
				if (endOffset < p1 || p2 < startOffset) {//选择区域之外
					if (background != null) {
						((JComponent) component).putClientProperty(BACK_IMG, null);
						JLabel label = (JLabel) component;
						label.setIcon(background);

					}
				} else if (background == null) {
					JLabel label = (JLabel) component;
					if (label.getIcon() instanceof ImageIcon) {
						ImageIcon icon = (ImageIcon) label.getIcon();
						if (icon != null) {
							ImageIcon grayImageIcon = new ImageIcon(ImageUtil.getGrayImage1(component, icon.getImage()));
							label.setIcon(grayImageIcon);

							((JComponent) component).putClientProperty(BACK_IMG, icon);
							label.setOpaque(true);
						}
					}
				}
			}
		}
	}
}