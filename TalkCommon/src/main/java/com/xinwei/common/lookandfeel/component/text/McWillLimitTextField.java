/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月10日 下午1:12:17
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component.text;

import java.awt.Toolkit;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class McWillLimitTextField extends JTextField {
	public McWillLimitTextField() {
		setDocument(new LimitedDocument());
	}

	private int maxLength = -1;

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public int getMaxLength() {
		return this.maxLength;
	}

	private class LimitedDocument extends PlainDocument {

		public LimitedDocument() {
			super();
		}

		public void insertString(int offset, String str, AttributeSet attrSet) throws BadLocationException {
			if (attrSet == null) {
				int allowCount = maxLength - getLength();
				if (allowCount > 0) {
					if (allowCount < str.length()) {
						str = str.substring(0, allowCount);
					}
				} else {
					Toolkit.getDefaultToolkit().beep();
					return;
				}
			}
			super.insertString(offset, str, attrSet);

		}
	}

}
