/*
 * Copyright 2005 MH-Software-Entwicklung. All rights reserved.
 * Use is subject to license terms.
 */
package com.xinwei.common.lookandfeel;

import com.jtattoo.plaf.AbstractIconFactory;
import javax.swing.Icon;

/**
 * @author Michael Hagen
 */
public class McWillIconFactory implements AbstractIconFactory {

    private static McWillIconFactory instance = null;

    private McWillIconFactory() {
    }

    public static synchronized McWillIconFactory getInstance() {
        if (instance == null) {
            instance = new McWillIconFactory();
        }
        return instance;
    }

    public Icon getOptionPaneErrorIcon() {
        return McWillIcons.getOptionPaneErrorIcon();
    }

    public Icon getOptionPaneWarningIcon() {
        return McWillIcons.getOptionPaneWarningIcon();
    }

    public Icon getOptionPaneInformationIcon() {
        return McWillIcons.getOptionPaneInformationIcon();
    }

    public Icon getOptionPaneQuestionIcon() {
        return McWillIcons.getOptionPaneQuestionIcon();
    }

    public Icon getFileChooserDetailViewIcon() {
        return McWillIcons.getFileChooserDetailViewIcon();
    }

    public Icon getFileChooserHomeFolderIcon() {
        return McWillIcons.getFileChooserHomeFolderIcon();
    }

    public Icon getFileChooserListViewIcon() {
        return McWillIcons.getFileChooserListViewIcon();
    }

    public Icon getFileChooserNewFolderIcon() {
        return McWillIcons.getFileChooserNewFolderIcon();
    }

    public Icon getFileChooserUpFolderIcon() {
        return McWillIcons.getFileChooserUpFolderIcon();
    }

    public Icon getMenuIcon() {
        return McWillIcons.getMenuIcon();
    }

    public Icon getIconIcon() {
        return McWillIcons.getIconIcon();
    }

    public Icon getMaxIcon() {
        return McWillIcons.getMaxIcon();
    }

    public Icon getMinIcon() {
        return McWillIcons.getMinIcon();
    }

    public Icon getCloseIcon() {
        return McWillIcons.getCloseIcon();
    }

    public Icon getPaletteCloseIcon() {
        return McWillIcons.getPaletteCloseIcon();
    }

    public Icon getRadioButtonIcon() {
        return McWillIcons.getRadioButtonIcon();
    }

    public Icon getCheckBoxIcon() {
        return McWillIcons.getCheckBoxIcon();
    }

    public Icon getComboBoxIcon() {
        return McWillIcons.getComboBoxIcon();
    }

    public Icon getTreeComputerIcon() {
        return McWillIcons.getTreeComputerIcon();
    }

    public Icon getTreeFloppyDriveIcon() {
        return McWillIcons.getTreeFloppyDriveIcon();
    }

    public Icon getTreeHardDriveIcon() {
        return McWillIcons.getTreeHardDriveIcon();
    }

    public Icon getTreeFolderIcon() {
        return McWillIcons.getTreeFolderIcon();
    }

    public Icon getTreeLeafIcon() {
        return McWillIcons.getTreeLeafIcon();
    }

    public Icon getTreeCollapsedIcon() {
        return McWillIcons.getTreeControlIcon(true);
    }

    public Icon getTreeExpandedIcon() {
        return McWillIcons.getTreeControlIcon(false);
    }

    public Icon getMenuArrowIcon() {
        return McWillIcons.getMenuArrowIcon();
    }

    public Icon getMenuCheckBoxIcon() {
        return McWillIcons.getMenuCheckBoxIcon();
    }

    public Icon getMenuRadioButtonIcon() {
        return McWillIcons.getMenuRadioButtonIcon();
    }

    public Icon getUpArrowIcon() {
        return McWillIcons.getUpArrowIcon();
    }

    public Icon getDownArrowIcon() {
        return McWillIcons.getDownArrowIcon();
    }

    public Icon getLeftArrowIcon() {
        return McWillIcons.getLeftArrowIcon();
    }

    public Icon getRightArrowIcon() {
        return McWillIcons.getRightArrowIcon();
    }

    public Icon getSplitterDownArrowIcon() {
        return McWillIcons.getSplitterDownArrowIcon();
    }

    public Icon getSplitterHorBumpIcon() {
        return McWillIcons.getSplitterHorBumpIcon();
    }

    public Icon getSplitterLeftArrowIcon() {
        return McWillIcons.getSplitterLeftArrowIcon();
    }

    public Icon getSplitterRightArrowIcon() {
        return McWillIcons.getSplitterRightArrowIcon();
    }

    public Icon getSplitterUpArrowIcon() {
        return McWillIcons.getSplitterUpArrowIcon();
    }

    public Icon getSplitterVerBumpIcon() {
        return McWillIcons.getSplitterVerBumpIcon();
    }

    public Icon getThumbHorIcon() {
        return McWillIcons.getThumbHorIcon();
    }

    public Icon getThumbVerIcon() {
        return McWillIcons.getThumbVerIcon();
    }

    public Icon getThumbHorIconRollover() {
        return McWillIcons.getThumbHorIconRollover();
    }

    public Icon getThumbVerIconRollover() {
        return McWillIcons.getThumbVerIconRollover();
    }
}
