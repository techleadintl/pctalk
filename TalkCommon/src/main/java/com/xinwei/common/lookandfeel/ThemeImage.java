package com.xinwei.common.lookandfeel;

import java.awt.Image;
import java.io.Serializable;

import javax.swing.ImageIcon;

public class ThemeImage implements Serializable {
	private transient Image image;
	private byte[] imageIcon;

	public ThemeImage() {
	}

	public ThemeImage(byte[] imageIcon) {
		if (imageIcon == null) {
			throw new IllegalArgumentException();
		}
		this.image = new ImageIcon(imageIcon).getImage();
		this.imageIcon = imageIcon;
	}

	public Image getImage() {
		if (image == null) {
			if (imageIcon != null) {
				image = new ImageIcon(imageIcon).getImage();
			}
		}
		return image;
	}

	public Image getScaledImage(int width, int height) {
		return getScaledImage(width, height, Image.SCALE_SMOOTH);
	}

	public Image getScaledImage(int width, int height, int hints) {
		Image image2 = getImage();
		if (image2 != null) {
			return image2.getScaledInstance(width, height, hints);
		}
		return null;
	}
}
