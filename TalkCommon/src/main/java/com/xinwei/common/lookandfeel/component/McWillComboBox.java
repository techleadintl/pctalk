package com.xinwei.common.lookandfeel.component;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ui.McWillComboBoxUI;

@SuppressWarnings("serial")
public class McWillComboBox<T> extends JComboBox {

	public McWillComboBox(ComboBoxModel aModel) {
		super(aModel);
	}

	public McWillComboBox(final Object items[]) {
		super(items);
	}

	public McWillComboBox(Vector<?> items) {
		super(items);
	}

	public McWillComboBox() {
		super();
	}

	@Override
	public void updateUI() {
		setUI(new MyComboBoxUI());
		ListCellRenderer renderer = getRenderer();
		if (renderer instanceof Component) {
			SwingUtilities.updateComponentTreeUI((Component) renderer);
		}
	}

	protected JButton createXButton() {
		JButton button = new McWillIconButton(getPopIcons());// 创建对象
		button.setOpaque(false);
		button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		return button;
	}

	protected JTextField createXTextField() {
		JTextField editor = new JTextField("", 9);
		editor.setBorder(null);
		return editor;
	}

	protected ImageIcon[] getPopIcons() {
		return null;
	}

	class MyComboBoxUI extends McWillComboBoxUI {
		protected McWillComboBox myComboBox;

		@Override
		public void installUI(JComponent c) {
			myComboBox = (McWillComboBox) c;
			super.installUI(c);
		}

		@Override
		public JButton createArrowButton() {
			return createXButton();
		}

		@Override
		protected ComboPopup createPopup() {
			return new MyComboPopup(myComboBox);
		}

		/**
		 * 弹出面板类
		 * 
		 */
		class MyComboPopup extends BasicComboPopup {
			public MyComboPopup(JComboBox combo) {
				super(combo);
			}

			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				McWillTheme.setOpaque(this);
			}
		}

	}

}