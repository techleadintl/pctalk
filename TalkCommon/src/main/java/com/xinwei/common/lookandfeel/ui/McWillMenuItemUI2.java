package com.xinwei.common.lookandfeel.ui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.ComponentUI;

import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.BaseMenuItemUI;
import com.xinwei.common.lookandfeel.ImageColorInfo;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.util.Constants;
import com.xinwei.common.lookandfeel.util.MenuUtil;

public class McWillMenuItemUI2 extends BaseMenuItemUI {

	public static ComponentUI createUI(JComponent c) {
		return new McWillMenuItemUI2();
	}

	protected void paintBackground(Graphics g, JComponent c, int x, int y, int w, int h) {
		int MENU_LEFT = MenuUtil.getMenuLeftInfo(c);
		//by liyong 2016-08-02 去掉默认
		//		if (MENU_LEFT == MenuUtil.DEFAULT) {
		//			super.paintBackground(g, c, x, y, w, h);
		//			return;
		//		}

		JMenuItem b = (JMenuItem) c;
		ButtonModel model = b.getModel();
		if (model.isArmed() || (c instanceof JMenu && model.isSelected())) {
			g.setColor(getMenuSelectColor());
			g.fillRect(x, y, w, h);
		} else {
			ColorUIResource menuBackgroundColor = AbstractLookAndFeel.getMenuBackgroundColor();
			if (!AbstractLookAndFeel.getTheme().isMenuOpaque()) {
				Graphics2D g2D = (Graphics2D) g;
				Composite composite = g2D.getComposite();
				AlphaComposite alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, AbstractLookAndFeel.getTheme().getMenuAlpha());
				g2D.setComposite(alpha);
				g.setColor(menuBackgroundColor);
				g.fillRect(x, y, w, h);
				g2D.setComposite(composite);
			} else {
				//				int WIDTH = Constants.MENU_LEFT_WIDTH;

				int WIDTH = w; //by liyong 2016-08-02
				if (MENU_LEFT == MenuUtil.CHECKANDICON) {
					//					WIDTH = WIDTH + 20;
				}
				g.setColor(menuBackgroundColor);

				g.fillRect(x + WIDTH, y, w, h);

				{//add by liyong
					ThemeColor themeColor = McWillTheme.getThemeColor();
					Color srcColor = getMenuSrcColor();

					Color destColor = getMenuDestColor();
					GradientPaint paint;
					if (McWillTheme.isThemeImage()) {
						ImageColorInfo imageColorInfo = McWillTheme.getImageColorInfo();
						srcColor = imageColorInfo.getMostColor();
						destColor = themeColor.getMiddleColor();
						paint = new GradientPaint(0, 0, srcColor, 30, 0, destColor, true);
					} else {
						paint = new GradientPaint(0, 0, srcColor, 200, 0, destColor, true);
					}
					((Graphics2D) g).setPaint(paint);
//					g.fillRect(x, y, WIDTH, h);
				}

			}
		}
		if (menuItem.isSelected() && menuItem.isArmed()) {
			g.setColor(getMenuSelectColor());
		} else {
			g.setColor(AbstractLookAndFeel.getMenuForegroundColor());
		}
	}

	protected Color getMenuSelectColor() {
		return  McWillTheme.getThemeColor().getMiddleColor();
	}

	protected Color getMenuSrcColor() {
		return McWillTheme.getThemeColor().getSrcColor();
	}

	protected Color getMenuDestColor() {
		return McWillTheme.getThemeColor().getSubLightColor();
	}

	protected void paintText(Graphics g, JMenuItem menuItem, Rectangle textRect, String text) {
		textRect.x += 15;
		super.paintText(g, menuItem, textRect, text);
	}

	protected Dimension getPreferredMenuItemSize(JComponent c, Icon checkIcon, Icon arrowIcon, int defaultTextIconGap) {
		Dimension size = super.getPreferredMenuItemSize(c, checkIcon, arrowIcon, defaultTextIconGap);
		if (size.width < 90) {
			return new Dimension(size.width + 50, size.height);
		}
		if (size.width > 190)
			return new Dimension(size.width + 10, size.height);

		return new Dimension(size.width + 30, size.height);
	}
}
