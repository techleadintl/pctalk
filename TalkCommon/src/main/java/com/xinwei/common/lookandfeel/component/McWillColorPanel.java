/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月12日 下午7:30:15
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;

import javax.swing.JPanel;

import com.xinwei.common.lookandfeel.util.PainterUtil;

public class McWillColorPanel extends JPanel {
	public McWillColorPanel(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
	}

	public McWillColorPanel(LayoutManager layout) {
		this(layout, true);
	}

	public McWillColorPanel(boolean isDoubleBuffered) {
		this(new FlowLayout(), isDoubleBuffered);
	}

	public McWillColorPanel() {
		this(true);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Insets insets = this.getInsets();
		PainterUtil.paintBackgroudColor(g, getColor(), insets.left, insets.top, getWidth() - insets.left - insets.right, getHeight() - insets.top - insets.bottom);
	}

	public Color getColor() {
		return Color.WHITE;
	}

}
