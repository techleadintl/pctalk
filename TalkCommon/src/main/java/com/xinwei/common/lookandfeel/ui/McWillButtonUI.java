package com.xinwei.common.lookandfeel.ui;

import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.BaseButtonUI;
import com.jtattoo.plaf.ColorHelper;
import com.jtattoo.plaf.JTattooUtilities;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicGraphicsUtils;

public class McWillButtonUI extends BaseButtonUI {

	public static ComponentUI createUI(JComponent c) {
		return new McWillButtonUI();
	}

	protected void paintBackground(Graphics g, AbstractButton b) {
//		int w = b.getWidth();
//		int h = b.getHeight();
//		Graphics2D g2D = (Graphics2D) g;
//		Shape savedClip = g.getClip();
//		if ((b.getBorder() != null) && b.isBorderPainted() && (b.getBorder() instanceof UIResource)) {
//			Area clipArea = new Area(new RoundRectangle2D.Double(0, 0, w - 1, h - 1, 6, 6));
//			clipArea.intersect(new Area(savedClip));
//			g2D.setClip(clipArea);
//		}
		
		paintBackground2(g, b);
//		
//		if (b.isContentAreaFilled() && b.isRolloverEnabled() && b.getModel().isRollover() && (b.getBorder() != null) && b.isBorderPainted()) {
//			g.setColor(AbstractLookAndFeel.getTheme().getFocusColor());
//			Insets ins = b.getBorder().getBorderInsets(b);
//			if ((ins.top == 0) && (ins.left == 1)) {
//				g.drawRect(1, 0, w - 2, h - 1);
//				g.drawRect(2, 1, w - 4, h - 3);
//			} else {
//				g.drawRect(1, 1, w - 4, h - 4);
//				g.drawRect(2, 2, w - 6, h - 6);
//			}
//		}
//		g2D.setClip(savedClip);
	}

	protected void paintBackground2(Graphics g, AbstractButton b) {
		if (!b.isContentAreaFilled() || (b.getParent() instanceof JMenuBar)) {
			return;
		}

		int width = b.getWidth();
		int height = b.getHeight();
		Color colors[] = AbstractLookAndFeel.getTheme().getRolloverColors();
		ButtonModel model = b.getModel();
		if (b.isEnabled()) {
			if (b.getBackground() instanceof ColorUIResource) {
				if (model.isPressed() && model.isArmed()) {
					colors = AbstractLookAndFeel.getTheme().getPressedColors();
				} else {
					if (b.isRolloverEnabled() && model.isRollover()) {
						colors = AbstractLookAndFeel.getTheme().getSelectedColors();
					} else {
						if (AbstractLookAndFeel.getTheme().doShowFocusFrame() && b.hasFocus()) {
							colors = AbstractLookAndFeel.getTheme().getFocusColors();
						}
					}
				}
			} else {
				if (model.isPressed() && model.isArmed()) {
					colors = ColorHelper.createColorArr(b.getBackground(), ColorHelper.darker(b.getBackground(), 50), 20);
				} else {
					if (b.isRolloverEnabled() && model.isRollover()) {
						colors = ColorHelper.createColorArr(ColorHelper.brighter(b.getBackground(), 80), ColorHelper.brighter(b.getBackground(), 20), 20);
					} else {
						colors = ColorHelper.createColorArr(ColorHelper.brighter(b.getBackground(), 40), ColorHelper.darker(b.getBackground(), 20), 20);
					}
				}
			}
		} else { // disabled
			colors = AbstractLookAndFeel.getTheme().getActiveColors();
		}
		if (b.isBorderPainted() && (b.getBorder() != null)) {
			Insets insets = b.getBorder().getBorderInsets(b);
			int x = insets.left > 0 ? 1 : 0;
			int y = insets.top > 0 ? 1 : 0;
			int w = insets.right > 0 ? width - 1 : width;
			int h = insets.bottom > 0 ? height - 1 : height;
			JTattooUtilities.fillHorGradient(g, colors, x, y, w - x, h - y);
		} else {
			JTattooUtilities.fillHorGradient(g, colors, 0, 0, width, height);
		}
	}

	protected void paintFocus(Graphics g, AbstractButton b, Rectangle viewRect, Rectangle textRect, Rectangle iconRect) {
		g.setColor(Color.black);
		BasicGraphicsUtils.drawDashedRect(g, 3, 3, b.getWidth() - 6, b.getHeight() - 6);
	}
}
