/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月28日 下午2:38:41
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.Dimension;

import javax.swing.JTextPane;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BoxView;
import javax.swing.text.ComponentView;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Element;
import javax.swing.text.IconView;
import javax.swing.text.LabelView;
import javax.swing.text.ParagraphView;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;

/**
 * 该类是真正实现超长单词都能自动换行的 JTextPane 的子类 Java 7 以下版本的 JTextPane 本身都能实现自动换行，对 超长单词都能有效，但从 Java 7 开始读超长单词就不能自动 换行，导致 JTextPane 的实际宽度变大，使得滚动条出现。 下面的方法是对这个 bug 的较好修复。
 * 
 */
public class McWillTextPane extends JTextPane {

	// 内部类  
	// 以下内部类全都用于实现自动强制折行  

	class WrapEditorKit extends StyledEditorKit {
		ViewFactory defaultFactory = new WrapColumnFactory();

		public ViewFactory getViewFactory() {
			return defaultFactory;
		}
	}

	class WrapColumnFactory implements ViewFactory {
		public View create(Element elem) {
			String kind = elem.getName();
			if (kind != null) {
				if (kind.equals(AbstractDocument.ContentElementName)) {
					return new WrapLabelView(elem);
				} else if (kind.equals(AbstractDocument.ParagraphElementName)) {
					return getParagraphView(elem);
				} else if (kind.equals(AbstractDocument.SectionElementName)) {
					return new BoxView(elem, View.Y_AXIS);
				} else if (kind.equals(StyleConstants.ComponentElementName)) {
					return new ComponentView(elem);
				} else if (kind.equals(StyleConstants.IconElementName)) {
					return new IconView(elem);
				}
			}

			// default to text display
			return new LabelView(elem);
		}

	}

	class WrapLabelView extends LabelView {
		public WrapLabelView(Element elem) {
			super(elem);
		}

		public float getMinimumSpan(int axis) {
			switch (axis) {
			case View.X_AXIS:
				return 0;
			case View.Y_AXIS:
				return super.getMinimumSpan(axis);
			default:
				throw new IllegalArgumentException("Invalid axis: " + axis);
			}
		}

		public int getBreakWeight(int axis, float pos, float len) {
			return GoodBreakWeight;
		}

		public View breakView(int axis, int p0, float pos, float len) {
			if (axis == View.X_AXIS) {
				checkPainter();
				int p1 = getGlyphPainter().getBoundedPosition(this, p0, pos, len);
				if (p0 == getStartOffset() && p1 == getEndOffset()) {
					return this;
				}
				return createFragment(p0, p1);
			}
			return this;
		}
	}

	public static class ChatParagraphView extends ParagraphView {
		public ChatParagraphView(Element elem) {
			super(elem);
		}

	}

	// 本类  

	// 构造函数  
	public McWillTextPane() {
		super();
		this.setEditorKit(new WrapEditorKit());
	}

	public McWillTextPane(DefaultStyledDocument doc) {

	}

	protected ParagraphView getParagraphView(Element elem) {
		return new ParagraphView(elem);
	}

	public boolean getScrollableTracksViewportWidth() {
		return true;
	}

	public void setSize(Dimension d) {
		if (d.width < getParent().getSize().width) {
			d.width = getParent().getSize().width;
		}
		super.setSize(d);
	}
}
