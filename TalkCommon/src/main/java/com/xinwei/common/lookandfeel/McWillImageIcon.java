/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月14日 下午7:51:03
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel;

import java.net.URL;

import javax.swing.ImageIcon;

public class McWillImageIcon extends ImageIcon {
	private static final long serialVersionUID = 1L;

	private String name;
	private String format;

	private URL url;

	public McWillImageIcon(URL url, String name, String format, String description) {
		super(url);
		this.url = url;
		setDescription(description);
		setName(name);
		setFormat(format);
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

}
