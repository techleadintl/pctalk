package com.xinwei.common.lookandfeel.component;

import java.awt.Cursor;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.Icon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class McWillIconButton extends JButton {
	private String str;
	private boolean hasBorder = false;

	private Icon icon;

	private Icon rolloverIcon;

	private Icon pressedIcon;

	public McWillIconButton() {
		this(null, null, null);
	}

	public McWillIconButton(Icon icon) {
		this(icon, null, null);
	}

	public McWillIconButton(Icon icon, boolean hasBorder) {
		this(icon, null, null);
		this.hasBorder = hasBorder;
	}

	public McWillIconButton(Icon icon, Icon overIcon, Icon pressIcon) {
		this(new Icon[] { icon, overIcon, pressIcon });
	}

	public McWillIconButton(Icon[] icons) {
		updateIcons(icons);

		setContentAreaFilled(false);
		setBorderPainted(false);
		setText(null);
		setCursor(new Cursor(Cursor.HAND_CURSOR));
		setFocusPainted(false);
		//		setBorder(BorderFactory.createEmptyBorder());
	}

	public McWillIconButton(Icon[] icons, boolean hasBorder) {
		this(icons);
		this.hasBorder = hasBorder;
	}

	public void updateIcons(Icon[] icons) {
		if (icons != null) {
			if (icons.length > 0 && icons[0] != null) {
				icon = icons[0];
				setIcon(icon);
			}
			if (icons.length > 1 && icons[1] != null) {
				rolloverIcon = icons[1];
				setRolloverIcon(rolloverIcon);
				setPressedIcon(rolloverIcon);
			}
			if (icons.length > 2 && icons[2] != null) {
				pressedIcon = icons[2];
				setPressedIcon(pressedIcon);
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (hasBorder) {
			if (getModel().isRollover()) {
				setBorderPainted(true);
			} else {
				setBorderPainted(false);
			}
		}
		if (str == null)
			return;

		FontMetrics fm = g.getFontMetrics();

		int stringWidth = fm.stringWidth(str);
		int stringAscent = fm.getAscent();

		int xCoordinate = getWidth() / 2 - stringWidth / 2;
		int yCoordinate = getHeight() / 2 + fm.getHeight() / 3;

		g.drawString(str, xCoordinate, yCoordinate);

	}

	public String getString() {
		return str;
	}

	public void setString(String text) {
		this.str = text;
	}

	public void setHasBorder(boolean hasBorder) {
		this.hasBorder = hasBorder;
	}

	public Icon getButtonIcon() {
		return icon;
	}

	public Icon getButtonRolloverIcon() {
		return rolloverIcon;
	}

	public Icon getButtonPressedIcon() {
		return pressedIcon;
	}
}