package com.xinwei.common.lookandfeel;

import java.awt.Color;

import javax.swing.border.Border;

import com.jtattoo.plaf.AbstractBorderFactory;

public class McWillBorderFactory implements AbstractBorderFactory {

	private static McWillBorderFactory instance = null;

	private McWillBorderFactory() {
	}

	public static synchronized McWillBorderFactory getInstance() {
		if (instance == null) {
			instance = new McWillBorderFactory();
		}
		return instance;
	}

	public Border getFocusFrameBorder() {
		return McWillBorders.getFocusFrameBorder();
	}

	public Border getButtonBorder() {
		return McWillBorders.getButtonBorder();
	}

	public Border getLabelBorder() {
		return McWillBorders.getLabelBorder();
	}
	public Border getLabelLightBorder() {
		return McWillBorders.getLabelLightBorder();
	}

	public Border getLabelBorder(Color color, int arc) {
		return McWillBorders.getLabelBorder(color, arc);
	}

	public Border getToggleButtonBorder() {
		return McWillBorders.getToggleButtonBorder();
	}

	public Border getTextBorder() {
		return McWillBorders.getTextBorder();
	}

	public Border getSpinnerBorder() {
		return McWillBorders.getSpinnerBorder();
	}

	public Border getTextFieldBorder() {
		return McWillBorders.getTextFieldBorder();
	}

	public Border getComboBoxBorder() {
		return McWillBorders.getComboBoxBorder();
	}

	public Border getTableHeaderBorder() {
		return McWillBorders.getTableHeaderBorder();
	}

	public Border getTableScrollPaneBorder() {
		return McWillBorders.getTableScrollPaneBorder();
	}

	public Border getScrollPaneBorder() {
		return McWillBorders.getScrollPaneBorder();
	}

	public Border getTabbedPaneBorder() {
		return McWillBorders.getTabbedPaneBorder();
	}

	public Border getMenuBarBorder() {
		return McWillBorders.getMenuBarBorder();
	}

	public Border getMenuItemBorder() {
		return McWillBorders.getMenuItemBorder();
	}

	public Border getPopupMenuBorder() {
		return McWillBorders.getPopupMenuBorder();
	}

	public Border getInternalFrameBorder() {
		return McWillBorders.getInternalFrameBorder();
	}

	public Border getPaletteBorder() {
		return McWillBorders.getPaletteBorder();
	}

	public Border getToolBarBorder() {
		return McWillBorders.getToolBarBorder();
	}

	public Border getProgressBarBorder() {
		return McWillBorders.getProgressBarBorder();
	}

	public Border getDesktopIconBorder() {
		return McWillBorders.getDesktopIconBorder();
	}
}
