package com.xinwei.common.lookandfeel.ui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JButton;
import javax.swing.JRootPane;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lookandfeel.component.McWillTitlePane;

@SuppressWarnings("serial")
public class ChatTitlePane extends McWillTitlePane {

	public ChatTitlePane(JRootPane root, BaseRootPaneUI ui) {
		super(root, ui);
	}

	@Override
	protected void createMenuBar() {
	}

	protected Image getFrameIconImage() {
		return null;
	}

	protected void addCloseButton() {
	}

	protected void addMaxButton() {
	}

	protected void addIconifyButton() {
	}

	public void paintText(Graphics g, int x, int y, String title) {
	}
	public Dimension getPreferredSize() {
		Dimension size = super.getPreferredSize();
		return new Dimension(size.width, 1);
	}
}
