package com.xinwei.common.lookandfeel.layout;

import java.awt.LayoutManager;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Container;
import java.awt.Rectangle;

public class TileLayout implements LayoutManager {
	protected int hgap;
	protected int vgap;
	protected int column;
	protected int layModel;
	protected Dimension d;
	public static final int INDENTIC_SIZE = 0;
	public static final int PREFER_SIZE = 1;

	public TileLayout() {
		this(20, 20);
	}

	public TileLayout(int column) {
		this(20, 20, column, INDENTIC_SIZE);
	}

	public TileLayout(int hgap, int vgap) {
		this(hgap, vgap, 4, PREFER_SIZE);
	}

	public TileLayout(int hgap, int vgap, int column, int model) {
		this.hgap = hgap;
		this.vgap = vgap;
		setColumn(column);
		setModel(model);
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public void setModel(int model) {
		layModel = model;
	}

	public int getColumn() {
		return column;
	}

	/**
	 * @param name
	 * @param comp
	 */
	public void addLayoutComponent(String name, Component comp) {
	}

	/**
	 * 
	 * @param comp
	 */
	public void removeLayoutComponent(Component comp) {
	}

	/**
	 */
	private Dimension maxCompSize(Container target) {
		Dimension d = new Dimension(0, 0);
		synchronized (target.getTreeLock()) {
			int numbers = target.getComponentCount();
			for (int i = 0; i < numbers; i++) {
				Dimension di = target.getComponent(i).getPreferredSize();
				d = union(d, di);
			}
			return d;
		}
	}

	/**
	 * 
	 * @param parent
	 * 
	 */
	public Dimension preferredLayoutSize(Container parent) {
		synchronized (parent.getTreeLock()) {
			d = maxCompSize(parent);
			int num = parent.getComponentCount();
			int column2 = getColumn();
			int r = (num - 1) / column2 + 1;
			int c = column2;
			int w = c * d.width + (c + 1) * hgap;
			int h = r * d.height + (r + 1) * vgap;
			return new Dimension(w, h);
		}
	}

	/**
	 * 
	 * @param parent
	 * @return
	 */
	public Dimension minimumLayoutSize(Container parent) {
		return new Dimension(0, 0);
	}

	/**
	 * 
	 * @param parent
	 */
	public void layoutContainer(Container parent) {
		synchronized (parent.getTreeLock()) {
			d = maxCompSize(parent);
			int numbers = parent.getComponentCount();
			Dimension cd = parent.getSize();
			int c = cd.width / (d.width + hgap);
			if (c == 0)
				c = 1;
			for (int i = 0; i < numbers; i++) {
				Component comp = parent.getComponent(i);
				if (comp.isVisible()) {
					int x = i % c;
					int y = i / c;
					comp.setLocation(x * (hgap + d.width) + hgap, y * (vgap + d.height) + vgap);
					if (layModel == PREFER_SIZE)
						comp.setSize(comp.getPreferredSize());
					else if (layModel == INDENTIC_SIZE)
						comp.setSize(d);
				}
			}
		}
	}

	/**
	 */
	private Dimension union(Dimension d1, Dimension d2) {
		return new Dimension(d1.width > d2.width ? d1.width : d2.width, d1.height > d2.height ? d1.height : d2.height);
	}
}