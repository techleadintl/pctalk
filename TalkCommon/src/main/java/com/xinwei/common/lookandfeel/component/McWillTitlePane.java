package com.xinwei.common.lookandfeel.component;

import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JRootPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.plaf.UIResource;

import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.BaseRootPaneUI;
import com.jtattoo.plaf.BaseTitlePane;
import com.jtattoo.plaf.DecorationHelper;
import com.jtattoo.plaf.JTattooUtilities;
import com.xinwei.common.lookandfeel.McWillLookAndFeel;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.lookandfeel.listener.McWillWindowListener;
import com.xinwei.common.lookandfeel.util.Constants;
import com.xinwei.common.lookandfeel.util.ImageUtil;

@SuppressWarnings("serial")
public class McWillTitlePane extends BaseTitlePane {

	protected Rectangle dialogBounds;

	protected Icon iconifyIcon = getIconifyIcons()[0];
	protected Icon maximizeIcon = getMaxIcons()[0];
	protected Icon minimizeIcon = getMaxIcons()[0];
	protected Icon closeIcon = getCloseIcons()[0];

	protected boolean addCustomButton;

	public McWillTitlePane(JRootPane root, BaseRootPaneUI ui) {
		super(root, ui);
	}

	public LayoutManager createLayout() {
		return new TitlePaneLayout();
	}

	@Override
	protected void createMenuBar() {
		menuBar = new SystemMenuBar() {
		};
	}

	public void paintBorder(Graphics g) {
	}

	@Override
	public void paintBackground(Graphics g) {
	}

	protected Image getFrameIconImage() {
		McWillContentPanel contentPanel = getAncestor(this);
		if (contentPanel != null && !contentPanel.isIconVisible()) {
			return null;
		}
		Frame frame = getFrame();
		if (frame != null) {
			Image image = frame.getIconImage();
			if (contentPanel != null && contentPanel.isIconFilter()) {
				image = ImageUtil.getFilterImage(image);
			}
			return image;
		}
		return null;
	}

	protected Icon[] getIconifyIcons() {
		return new Icon[] { new ImageIcon(McWillLookAndFeel.class.getResource("images/iconify.png")), new ImageIcon(McWillLookAndFeel.class.getResource("images/iconify_press.png")),
				new ImageIcon(McWillLookAndFeel.class.getResource("images/iconify_over.png")) };
	}

	protected Icon[] getMaxIcons() {
		return new Icon[] { new ImageIcon(McWillLookAndFeel.class.getResource("images/max.png")), new ImageIcon(McWillLookAndFeel.class.getResource("images/max_press.png")),
				new ImageIcon(McWillLookAndFeel.class.getResource("images/max_over.png")) };
	}

	protected Icon[] getCloseIcons() {
		return new Icon[] { new ImageIcon(McWillLookAndFeel.class.getResource("images/close.png")), new ImageIcon(McWillLookAndFeel.class.getResource("images/close_press.png")),
				new ImageIcon(McWillLookAndFeel.class.getResource("images/close_over.png")) };
	}

	protected Icon[] getRestoreIcons() {
		return new Icon[] { new ImageIcon(McWillLookAndFeel.class.getResource("images/restore.png")), new ImageIcon(McWillLookAndFeel.class.getResource("images/restore_press.png")),
				new ImageIcon(McWillLookAndFeel.class.getResource("images/restore_over.png")) };
	}

	public void createButtons() {
		iconifyButton = new McWillTitleButton(iconifyAction, ICONIFY, getIconifyIcons());
		maxButton = new McWillTitleButton(restoreAction, MAXIMIZE, getMaxIcons());
		closeButton = new McWillTitleButton(closeAction, CLOSE, getCloseIcons());
	}

	protected void installSubcomponents() {
		boolean isIconify = true;
		boolean isMax = false;
		if (window instanceof McWillWindowButtonUI) {
			McWillWindowButtonUI mwbu = (McWillWindowButtonUI) window;
			isIconify = mwbu.isIconify();
			isMax = mwbu.isMax();
		}

		createActions();
		createMenuBar();
		createButtons();
		if (menuBar != null)
			add(menuBar);
		if (iconifyButton != null && isIconify) {
			addIconifyButton();
		}
		if (maxButton != null && isMax) {
			addMaxButton();
		}
		if (closeButton != null) {
			addCloseButton();
		}
	}

	@Override
	public synchronized void addNotify() {
		super.addNotify();
	}

	@Override
	protected void updateMaxButton(Action action, Icon icon) {
		maxButton.setAction(action);
		if (action == restoreAction) {
			((McWillTitleButton) maxButton).setIcons(getRestoreIcons());
		} else if (action == maximizeAction) {
			((McWillTitleButton) maxButton).setIcons(getMaxIcons());
		}
	}

	public void paintText(Graphics g, int x, int y, String title) {
		McWillContentPanel contentPanel = getAncestor(this);

		if (contentPanel != null && !contentPanel.isTextVisible()) {
			return;
		}
		Object paintTilte = rootPane.getClientProperty(Constants.TITLE_TEXT);
		if (paintTilte != null && !Boolean.valueOf(paintTilte.toString())) {
			return;
		}
		g.setColor(McWillTheme.getWindowTitlFontColor());
		JTattooUtilities.drawString(rootPane, g, title, x, y);
	}

	public McWillContentPanel getAncestor(Component component) {
		Container parent = component.getParent();
		while (parent != null) {
			if (parent instanceof McWillContentPanel) {
				return (McWillContentPanel) parent;
			}
			return getAncestor(parent);
		}
		return null;
	}

	protected int getHorSpacing() {
		return 2;
	}

	protected int getVerSpacing() {
		return 5;
	}

	public void doMax() {
		maxButton.doClick();
	}

	public void doClose() {
		closeButton.doClick();
	}

	public void doIconify() {
		iconifyButton.doClick();
	}

	public int getState() {
		return this.state;
	}

	@Override
	protected void close() {
		if (window instanceof McWillWindowTitleActions) {
			((McWillWindowTitleActions) window).close(this);
		} else {
			xClose();
		}
	}

	public void xClose() {
		WindowListener[] windowListeners = window.getWindowListeners();
		for (WindowListener w : windowListeners) {
			if (w instanceof McWillWindowListener) {
				if (!((McWillWindowListener) w).preWindowClosing()) {
					return;
				}
			}
		}
		super.close();
	}

	protected void iconify() {
		if (window instanceof McWillWindowTitleActions) {
			((McWillWindowTitleActions) window).iconify(this);
		} else {
			xIconify();
		}
	}

	public void xIconify() {
		if (window instanceof Frame) {
			DecorationHelper.setExtendedState((Frame) window, state | Frame.ICONIFIED);
		} else if (window instanceof Dialog) {
			close();
		}
	}

	@Override
	protected void maximize() {
		if (window instanceof McWillWindowTitleActions) {
			((McWillWindowTitleActions) window).maximize(this);
		} else {
			xMaximize();
		}
	}

	public void xMaximize() {
		super.maximize();
	}

	protected void restore() {
		if (window instanceof McWillWindowTitleActions) {
			((McWillWindowTitleActions) window).restore(this);
		} else {
			xRestore();
		}
	}

	public void xRestore() {
		super.restore();
	}

	protected PropertyChangeListener createWindowPropertyChangeListener() {
		return new PropertyChangeHandler() {
			public void propertyChange(PropertyChangeEvent pce) {
				String name = pce.getPropertyName();
				// Frame.state isn't currently bound.
				if ("resizable".equals(name) || "state".equals(name)) {
					Frame frame = getFrame();
					if (frame != null) {
						setState(DecorationHelper.getExtendedState(frame), true);
					}
					if ("resizable".equals(name)) {
						getRootPane().repaint();
					}
				} else if ("tabTitle".equals(name)) {
					repaint();
				} else if ("componentOrientation".equals(name)) {
					revalidate();
					repaint();
				} else if (!JTattooUtilities.isMac() && useMaximizedBounds && "windowMaximize".equals(name)) {
					Frame frame = getFrame();
					if (frame != null) {
						GraphicsConfiguration gc = frame.getGraphicsConfiguration();
						Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(gc);
						Rectangle screenBounds = gc.getBounds();
						int x = Math.max(0, screenInsets.left);
						int y = Math.max(0, screenInsets.top);
						int w = screenBounds.width - (screenInsets.left + screenInsets.right);
						int h = screenBounds.height - (screenInsets.top + screenInsets.bottom);
						// Keep taskbar visible
						frame.setMaximizedBounds(new Rectangle(x, y, w, h));
					}
				} else if (!JTattooUtilities.isMac() && useMaximizedBounds && "windowMaximized".equals(name)) {
					Frame frame = getFrame();
					if (frame != null) {
						GraphicsConfiguration gc = frame.getGraphicsConfiguration();
						Rectangle screenBounds = gc.getBounds();
						if (frame.getSize().width > screenBounds.width || frame.getSize().height > screenBounds.height) {
							useMaximizedBounds = false;
							frame.setMaximizedBounds(null);
							restore();
							maximize();
						}
					}
				} else if (!JTattooUtilities.isMac() && "windowMoved".equals(name)) {
					useMaximizedBounds = true;
				}
			}
		};
	}

	//	protected class DialogIconifyAction extends AbstractAction {
	//
	//		public DialogIconifyAction() {
	//			super();
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			close();
	//		}
	//	}
	//
	//	protected class DialogMaxAction extends AbstractAction {
	//
	//		public DialogMaxAction() {
	//			super();
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			GraphicsConfiguration translucencyCapableGC = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
	//			Rectangle rec = translucencyCapableGC.getBounds();
	//			Insets d = Toolkit.getDefaultToolkit().getScreenInsets(translucencyCapableGC);
	//
	//			dialogBounds = window.getBounds();
	//
	//			window.setSize(rec.width, rec.height - d.bottom);
	//
	//			window.setLocation(0, 0);
	//
	//			state = Frame.MAXIMIZED_BOTH;
	//
	//			updateMaxButton(dialogRestoreAction, null);
	//			if (window instanceof Dialog) {
	//				((Dialog) window).setResizable(false);
	//			} else if (window instanceof Frame) {
	//				((Frame) window).setResizable(false);
	//			}
	//		}
	//	}
	//
	//	protected class DialogRestoreAction extends AbstractAction {
	//
	//		public DialogRestoreAction() {
	//			super();
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			if (dialogBounds != null) {
	//				window.setBounds(dialogBounds);
	//			}
	//			state = ~Frame.MAXIMIZED_BOTH;
	//
	//			updateMaxButton(dialogMaxAction, null);
	//			if (window instanceof Dialog) {
	//				((Dialog) window).setResizable(true);
	//			} else if (window instanceof Frame) {
	//				((Frame) window).setResizable(true);
	//			}
	//		}
	//	}

	//	private class McWillTitleListener extends MouseInputAdapter {
	//		protected Point origin = new Point();
	//		int i = 0;
	//
	//		public void mousePressed(MouseEvent e) {
	//			origin.x = e.getX();
	//			origin.y = e.getY();
	//		}
	//
	//		public void mouseClicked(MouseEvent e) {
	//			if (e.getClickCount() != 2)
	//				return;
	//
	//			if (state == Frame.MAXIMIZED_BOTH) {
	//				dialogRestoreAction.actionPerformed(null);
	//			} else {
	//				dialogMaxAction.actionPerformed(null);
	//			}
	//		}
	//
	//		public void mouseDragged(MouseEvent e) {
	//			if (state == Frame.MAXIMIZED_BOTH)
	//				return;
	//			{// 通过降低重绘频率，解决移动出现闪动
	//				if (i++ % 5 != 0) {
	//					return;
	//				}
	//				if (i > 100000) {
	//					i = 0;
	//				}
	//			}
	//
	//			Point p = window.getLocation();
	//			window.setLocation(p.x + e.getX() - origin.x, p.y + e.getY() - origin.y);
	//		}
	//	}

	public static class McWillTitleButton extends JButton implements UIResource, SwingConstants {
		boolean flag = false;

		public McWillTitleButton(Action action, String accessibleName, Icon icon, Icon pressIcon, Icon overIcon) {
			setAtt(action, accessibleName, new Icon[] { icon, pressIcon, overIcon });
		}

		public McWillTitleButton(Action action, String accessibleName, Icon[] icons) {
			setAtt(action, accessibleName, icons);
		}

		public void setAtt(Action action, String accessibleName, Icon[] icons) {
			setContentAreaFilled(false);
			setBorderPainted(false);
			setAction(action);
			setText(null);
			getAccessibleContext().setAccessibleName(accessibleName);
			setCursor(new Cursor(Cursor.HAND_CURSOR));
			setFocusPainted(false);
			setBorder(BorderFactory.createEmptyBorder());

			if (icons != null) {
				if (icons.length > 0 && icons[0] != null) {
					setIcon(icons[0]);
				}
				if (icons.length > 1 && icons[1] != null) {
					setRolloverIcon(icons[1]);
					setPressedIcon(icons[1]);
				}
				if (icons.length > 2 && icons[2] != null) {
					setPressedIcon(icons[2]);
				}
			}
		}

		public void setIcons(Icon[] icons) {
			if (icons != null) {
				if (icons.length > 0 && icons[0] != null) {
					setIcon(icons[0]);
				}
				if (icons.length > 1 && icons[1] != null) {
					setRolloverIcon(icons[1]);
					setPressedIcon(icons[1]);
				}
				if (icons.length > 2 && icons[2] != null) {
					setPressedIcon(icons[2]);
				}
			}
		}

		@Override
		public void setAction(Action a) {
			super.setAction(a);
		}
	}

	protected class TitlePaneLayout implements LayoutManager {

		public void addLayoutComponent(String name, Component c) {
		}

		public void removeLayoutComponent(Component c) {
		}

		public Dimension preferredLayoutSize(Container c) {
			int height = computeHeight();
			return new Dimension(height, height);
		}

		public Dimension minimumLayoutSize(Container c) {
			return preferredLayoutSize(c);
		}

		protected int computeHeight() {
			FontMetrics fm = Toolkit.getDefaultToolkit().getFontMetrics(getFont());
			return fm.getHeight() + 6;
		}

		public void layoutContainer(Container c) {
			if (AbstractLookAndFeel.getTheme().isMacStyleWindowDecorationOn()) {
				layoutMacStyle(c);
			} else {
				layoutDefault(c);
			}
		}

		public void layoutDefault(Container c) {
			boolean leftToRight = isLeftToRight();

			int spacing = getHorSpacing();
			int w = getWidth();
			int h = getHeight();

			// assumes all buttons have the same dimensions these dimensions
			// include the borders
			int buttonHeight = h - getVerSpacing();
			int buttonWidth = buttonHeight;

			int x = leftToRight ? spacing : w - buttonWidth - spacing;
			int y = Math.max(0, ((h - buttonHeight) / 2) - 1);

			int cpx = 0;
			int cpy = 0;
			int cpw = w;
			int cph = h;

			if (menuBar != null) {
				int mw = menuBar.getPreferredSize().width;
				int mh = menuBar.getPreferredSize().height;
				if (leftToRight) {
					cpx = 4 + mw;
					menuBar.setBounds(2, (h - mh) / 2, mw, mh);
				} else {
					menuBar.setBounds(getWidth() - mw, (h - mh) / 2, mw, mh);
				}
				cpw -= 4 + mw;
			}
			x = leftToRight ? w - spacing : 0;
			if (closeButton != null) {
				x += leftToRight ? -closeButton.getPreferredSize().width : spacing;
				closeButton.setBounds(x, y, closeButton.getPreferredSize().width, buttonHeight);
				if (!leftToRight) {
					x += closeButton.getPreferredSize().width;
				}
			}

			if ((maxButton != null) && (maxButton.getParent() != null)) {
				if (DecorationHelper.isFrameStateSupported(Toolkit.getDefaultToolkit(), BaseRootPaneUI.MAXIMIZED_BOTH)) {
					x += leftToRight ? -maxButton.getPreferredSize().width : spacing;
					maxButton.setBounds(x, y, maxButton.getPreferredSize().width, buttonHeight);
					if (!leftToRight) {
						x += maxButton.getPreferredSize().width;
					}
				}
			}

			if ((iconifyButton != null) && (iconifyButton.getParent() != null)) {
				x += leftToRight ? -iconifyButton.getPreferredSize().width : spacing;
				iconifyButton.setBounds(x, y, iconifyButton.getPreferredSize().width, buttonHeight);
				if (!leftToRight) {
					x += iconifyButton.getPreferredSize().width;
				}
			}
			buttonsWidth = leftToRight ? w - x : x;

			if (customTitlePanel != null) {
				if (!leftToRight) {
					cpx += buttonsWidth;
				}
				cpw -= buttonsWidth;
				Graphics g = getGraphics();
				if (g != null) {
					FontMetrics fm = g.getFontMetrics();
					int tw = SwingUtilities.computeStringWidth(fm, JTattooUtilities.getClippedText(getTitle(), fm, cpw));
					if (leftToRight) {
						cpx += tw;
					}
					cpw -= tw;
				}
				customTitlePanel.setBounds(cpx, cpy, cpw, cph);
			}

		}

		public void layoutMacStyle(Container c) {
			int spacing = getHorSpacing();
			int w = getWidth();
			int h = getHeight();

			// assumes all buttons have the same dimensions these dimensions
			// include the borders
			int buttonHeight = h - getVerSpacing();
			int buttonWidth = buttonHeight;

			int x = 0;
			int y = Math.max(0, ((h - buttonHeight) / 2) - 1);

			int cpx = 0;
			int cpy = 0;
			int cpw = w;
			int cph = h;

			if (closeButton != null) {
				closeButton.setBounds(x, y, buttonWidth, buttonHeight);
				x += buttonWidth + spacing;
			}
			if ((maxButton != null) && (maxButton.getParent() != null)) {
				if (DecorationHelper.isFrameStateSupported(Toolkit.getDefaultToolkit(), BaseRootPaneUI.MAXIMIZED_BOTH)) {
					maxButton.setBounds(x, y, buttonWidth, buttonHeight);
					x += buttonWidth + spacing;
				}
			}
			if ((iconifyButton != null) && (iconifyButton.getParent() != null)) {
				iconifyButton.setBounds(x, y, buttonWidth, buttonHeight);
				x += buttonWidth + spacing;
			}

			buttonsWidth = x;

			if (menuBar != null) {
				int mw = menuBar.getPreferredSize().width;
				int mh = menuBar.getPreferredSize().height;
				menuBar.setBounds(w - mw, (h - mh) / 2, mw, mh);
				cpw -= 4 + mw;
			}

			if (customTitlePanel != null) {
				cpx += buttonsWidth + 5;
				cpw -= buttonsWidth + 5;
				Graphics g = getGraphics();
				if (g != null) {
					FontMetrics fm = g.getFontMetrics();
					int tw = SwingUtilities.computeStringWidth(fm, JTattooUtilities.getClippedText(getTitle(), fm, cpw));
					cpx += tw;
					cpw -= tw;
				}
				customTitlePanel.setBounds(cpx, cpy, cpw, cph);
			}
		}
	}

}
