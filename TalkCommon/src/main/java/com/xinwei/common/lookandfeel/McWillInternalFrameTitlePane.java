package com.xinwei.common.lookandfeel;

import com.jtattoo.plaf.*;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JInternalFrame;
@SuppressWarnings("serial")
public class McWillInternalFrameTitlePane extends BaseInternalFrameTitlePane {

    private static final Color frameColor = new Color(0, 25, 207);

    public McWillInternalFrameTitlePane(JInternalFrame f) {
        super(f);
    }

    protected int getHorSpacing() {
        return 2;
    }

    protected int getVerSpacing() {
        return 5;
    }

    public void paintText(Graphics g, int x, int y, String title) {
        if (isActive()) {
            g.setColor(ColorHelper.brighter(McWillLookAndFeel.getTheme().getWindowBorderColor(), 10));
            JTattooUtilities.drawString(frame, g, title, x - 1, y - 1);
            g.setColor(ColorHelper.darker(McWillLookAndFeel.getTheme().getWindowBorderColor(), 25));
            JTattooUtilities.drawString(frame, g, title, x + 1, y + 1);
            g.setColor(AbstractLookAndFeel.getWindowTitleForegroundColor());
        } else {
            g.setColor(AbstractLookAndFeel.getWindowInactiveTitleForegroundColor());
        }
        JTattooUtilities.drawString(frame, g, title, x, y);
    }

    public void paintBorder(Graphics g) {
        if (!JTattooUtilities.isActive(this)) {
            g.setColor(ColorHelper.brighter(frameColor, 20));
        } else {
            g.setColor(frameColor);
        }
        g.drawLine(0, getHeight() - 1, getWidth(), getHeight() - 1);
    }
}
