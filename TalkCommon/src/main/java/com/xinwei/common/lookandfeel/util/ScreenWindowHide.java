package com.xinwei.common.lookandfeel.util;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.Timer;

public class ScreenWindowHide implements ActionListener {
	public static final int STEP = 8;

	public static final int BARE_WIDTH = 2;

	public static final int JUDGE_MAX = 5;

	public static final int JUDGE_MIN = -5;

	public static final int HIDE_NOT = -1;
	public static final int HIDE_TOP = 1;
	public static final int HIDE_LEFT = 2;
	public static final int HIDE_RIGHT = 3;

	private Timer timer;

	private Window window;

	private int HIDE_SCREEN_LOCATION = -1;

	private boolean WINDOW_VISIBLE = true;

	private boolean FLAG = false;

	private boolean animation;

	private boolean isTray = false;

	public ScreenWindowHide(Window window) {
		this.window = window;
		timer = new Timer(200, this);
	}

	public void start() {
		timer.start();
	}

	public void stop() {
		timer.stop();
	}

	@Override
	public void actionPerformed(ActionEvent e1) {
		if (!window.isVisible()) {
			WINDOW_VISIBLE = false;
			return;
		}

		if (animation)
			return;

		if (isTray) {//托盘操作
			isTray = false;
			if (HIDE_SCREEN_LOCATION > 0) {//隐藏在屏幕中
				Point location = window.getLocation();
				Point srcLocation = location;
				int X = location.x;
				int Y = location.y;
				int windowWidth = window.getWidth();
				int screenWidth = java.awt.Toolkit.getDefaultToolkit().getScreenSize().width;
				// 窗体离屏幕右边的距离；
				int right = screenWidth - X - windowWidth;
				if ((Y < 0)) {
					location = new Point(X, 1);
				} else if (X < 0) {
					location = new Point(1, Y);
				} else if (right < 0) {
					location = new Point(screenWidth - windowWidth, Y);
				}

				animationLocation(srcLocation, location);

				HIDE_SCREEN_LOCATION = -1;
				FLAG = false;
			}
			if (!WINDOW_VISIBLE) {
				WINDOW_VISIBLE = true;
				HIDE_SCREEN_LOCATION = -1;
				FLAG = false;
			}
		}

		if (FLAG) {

			//			if (window.getState() == Frame.MAXIMIZED_BOTH || window.getState() == Frame.MAXIMIZED_HORIZ
			//					|| window.getState() == Frame.MAXIMIZED_VERT)
			//				return;

			// 窗体的宽
			int width = window.getWidth();
			// 窗体的高
			int height = window.getHeight();
			// 获取窗体的轮廓
			Rectangle rect = new Rectangle(0, 0, width, height);
			// 获取鼠标在窗体的位置
			Point point = window.getMousePosition();
			boolean flag = isMouseInWindow(rect, point);
			setLocation(flag);
		} else {
			if (window.isVisible() && !window.isActive()) {
				FLAG = true;
			}
		}
	}

	public void setLocation(boolean flag) {
		if (!window.isVisible())
			return;
		// 窗体的宽
		int windowWidth = window.getWidth();
		// 窗体的高
		int windowHeight = window.getHeight();
		// 屏幕的宽度；
		int screenWidth = java.awt.Toolkit.getDefaultToolkit().getScreenSize().width;
		// 窗体离屏幕左边的距离
		int X = window.getLocationOnScreen().x;
		// 窗体离屏幕顶部的距离
		int Y = window.getLocationOnScreen().y;
		// 窗体离屏幕右边的距离；
		int right = screenWidth - X - windowWidth;

		if ((Y < 0) && flag && HIDE_SCREEN_LOCATION > 0) {//top:hide--->show
			HIDE_SCREEN_LOCATION = HIDE_NOT;
			animationLocation(window.getLocation(), new Point(X, 0));
		} else if (Y < JUDGE_MAX && !flag && HIDE_SCREEN_LOCATION < 0) {//top:show--->hide
			HIDE_SCREEN_LOCATION = HIDE_TOP;
			animationLocation(window.getLocation(), new Point(X, BARE_WIDTH - windowHeight));
		} else if (X < 0 && flag && HIDE_SCREEN_LOCATION > 0) {// left:hide-->show
			HIDE_SCREEN_LOCATION = HIDE_NOT;
			animationLocation(window.getLocation(), new Point(0, Y));
		} else if (X < JUDGE_MAX && !flag && HIDE_SCREEN_LOCATION < 0) {// left:show-->hide
			HIDE_SCREEN_LOCATION = HIDE_LEFT;
			animationLocation(window.getLocation(), new Point(-windowWidth + BARE_WIDTH, Y));
		} else if (right < 0 && flag) {//right:hide--->show
			HIDE_SCREEN_LOCATION = HIDE_NOT;
			animationLocation(window.getLocation(), new Point(screenWidth - windowWidth + 1, Y));
		} else if (right < JUDGE_MAX && !flag && HIDE_SCREEN_LOCATION < 0) {// right:show--->hide
			HIDE_SCREEN_LOCATION = HIDE_RIGHT;
			animationLocation(window.getLocation(), new Point(screenWidth - BARE_WIDTH, Y));
		}

	}

	private boolean isMouseInWindow(Rectangle rect, Point point) {
		if (rect != null && point != null) {
			int x0 = rect.x;
			int y0 = rect.y;
			int x1 = rect.width;
			int y1 = rect.height;
			int x = point.x;
			int y = point.y;
			return x >= x0 && x < x1 && y >= y0 && y < y1;
		}
		return false;
	}

	private void animationLocation(final Point srcLocation, final Point location) {

		final int x = (location.x - srcLocation.x) / STEP;
		final int y = (location.y - srcLocation.y) / STEP;
		new Thread() {
			public void run() {
				animation = true;
				for (int i = 0; i < STEP; i++) {
					window.setLocation(srcLocation.x + i * x, srcLocation.y + y * i);
					sleep();
				}
				window.setLocation(location);
				animation = false;
			}

			private void sleep() {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
				}
			}
		}.start();
	}

	public void setTray() {
		isTray = !isTray;
	}

}
