/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月28日 上午10:00:38
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import javax.swing.JSeparator;

import com.xinwei.common.lookandfeel.ui.McWillLineUI;

public class McWillLine extends JSeparator {
	private McWillLineUI lineUI;

	public McWillLine() {
		this(HORIZONTAL);
	}

	public McWillLine(int orientation) {
		super(orientation);
	}

	@Override
	public void updateUI() {
		if (lineUI == null) {
			lineUI = new McWillLineUI();
		}
		setUI(lineUI);
	}
}
