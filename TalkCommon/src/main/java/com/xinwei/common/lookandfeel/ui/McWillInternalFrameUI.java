package com.xinwei.common.lookandfeel.ui;

import com.jtattoo.plaf.BaseInternalFrameUI;
import com.xinwei.common.lookandfeel.McWillInternalFrameTitlePane;

import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.plaf.ComponentUI;
public class McWillInternalFrameUI extends BaseInternalFrameUI {

    public McWillInternalFrameUI(JInternalFrame b) {
        super(b);
    }

    public static ComponentUI createUI(JComponent c) {
        return new McWillInternalFrameUI((JInternalFrame) c);
    }

    protected JComponent createNorthPane(JInternalFrame w) {
        titlePane = new McWillInternalFrameTitlePane(w);
        return titlePane;
    }
}

