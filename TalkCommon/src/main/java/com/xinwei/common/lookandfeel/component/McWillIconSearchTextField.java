package com.xinwei.common.lookandfeel.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.xinwei.common.lang.StringUtil;

public class McWillIconSearchTextField extends JPanel {
	private static final long serialVersionUID = -7000758637988415370L;

	private JTextField textField;

	private JButton iconButton;

	private SearchListener searchListener;

	/**
	 * Creates a new McWillIconSearchTextField with Icon.
	 *
	 * the icon.
	 */
	public McWillIconSearchTextField(final Icon searchIcon) {
		this(searchIcon, null, null, null);
	}

	public McWillIconSearchTextField(final Icon searchIcon, final Icon deleteIcon) {
		this(searchIcon, null, deleteIcon, null);
	}

	public McWillIconSearchTextField(final Icon searchIcon, final Icon searchHoverIcon, final Icon deleteIcon, final Icon deleteHoverIcon) {
		textField = new JTextField();

		iconButton = new McWillIconButton(searchIcon);
		iconButton.setEnabled(false);

		setLayout(new BorderLayout());
		add(textField, BorderLayout.CENTER);
		add(iconButton, BorderLayout.EAST);

		xx(textField, Color.WHITE);

		xx(iconButton, Color.WHITE);

		setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));

		final ActionListener l = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onButtonAction();
			}
		};

		iconButton.addActionListener(l);

		textField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				documentChanged();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				documentChanged();

			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				documentChanged();
			}

			public void documentChanged() {
				onDocumentChanged(searchIcon, deleteIcon);
				doSearch(textField.getText());
			}

		});

		//		textField.addKeyListener(new KeyAdapter() {
		//			@Override
		//			public void keyReleased(KeyEvent e) {
		//				if (StringUtil.isNotEmpty(textField.getText())) {
		//					iconButton.setIcon(deleteIcon);
		//				} else {
		//					iconButton.setIcon(searchIcon);
		//				}
		//				doSearch(textField.getText());
		//			}
		//		});

	}

	protected void onDocumentChanged(final Icon searchIcon, final Icon deleteIcon) {
		if (StringUtil.isNotEmpty(textField.getText())) {
			if (deleteIcon != null) {
				iconButton.setIcon(deleteIcon);
				iconButton.setEnabled(true);
			}
		} else {
			if (searchIcon != null) {
				iconButton.setIcon(searchIcon);
				iconButton.setEnabled(false);
			}
		}
	}

	protected void onButtonAction() {
		if (StringUtil.isNotEmpty(textField.getText())) {
			textField.setText("");
			textField.requestFocus();
		}
	}

	public void doSearch(String text) {
		if (searchListener != null) {
			searchListener.doSearch(text);
		}

	}

	public void xx(JComponent com, Color background) {
		xx(com, background, Color.BLACK);
	}

	public void xx(JComponent com, Color background, Color foreground) {
		com.setOpaque(true);
		com.setForeground(foreground);
		com.setBorder(null);
		com.setBackground(background);
	}

	/**
	 * Sets the text of the textfield.
	 *
	 * @param text
	 *            the text.
	 */
	public void setText(String text) {
		textField.setText(text);
	}

	/**
	 * Returns the text inside of the textfield.
	 *
	 * @return the text inside of the textfield.
	 */
	public String getText() {
		return textField.getText();
	}

	/**
	 * Sets the icon to use inside of the textfield.
	 *
	 * @param icon
	 *            the icon.
	 */
	public void setIcon(Icon icon) {
		iconButton.setIcon(icon);
	}

	/**
	 * Returns the current icon used in the textfield.
	 *
	 * @return the icon used in the textfield.
	 */
	public Icon getIcon() {
		return iconButton.getIcon();
	}

	/**
	 * Returns the component that holds the icon.
	 *
	 * @return the component that is the container for the icon.
	 */
	public JComponent getImageComponent() {
		return iconButton;
	}

	/**
	 * Returns the text component used.
	 *
	 * @return the text component used.
	 */
	public JTextField getTextComponent() {
		return textField;
	}

	public SearchListener getSearchListener() {
		return searchListener;
	}

	public void setSearchListener(SearchListener searchListener) {
		this.searchListener = searchListener;
	}

	public static interface SearchListener {
		public void doSearch(String text);
	}

}
