package com.xinwei.common.lookandfeel.file.filter;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class McWillFileFilter extends FileFilter {
	private String extension;

	public McWillFileFilter(String extension) {
		this.extension = extension;
	}

	@Override
	public boolean accept(File f) {
		String nameString = f.getName();
		return nameString.toLowerCase().endsWith("." + extension);
	}

	@Override
	public String getDescription() {
		return extension.toUpperCase() + " (*." + extension + ")";
	}

	@Override
	public String toString() {
		return "extension:" + extension + "," + super.toString();
	}

	public String getExtension() {
		return extension;
	}
}