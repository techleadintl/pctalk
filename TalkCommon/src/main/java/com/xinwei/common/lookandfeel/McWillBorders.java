package com.xinwei.common.lookandfeel;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.UIResource;

import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.BaseBorders;
import com.jtattoo.plaf.ColorHelper;
import com.jtattoo.plaf.JTattooUtilities;
import com.xinwei.common.lookandfeel.component.McWillRolloverButton;
import com.xinwei.common.lookandfeel.util.ColorUtil;

@SuppressWarnings("serial")
public class McWillBorders extends BaseBorders {
	protected static Border labelBorder = null;
	protected static Border labelLightBorder = null;
	protected static Border colorBorder = null;

	public static Border getColorBorder() {
		if (colorBorder == null) {
			colorBorder = new ColorBorder();
		}
		return colorBorder;
	}

	// ------------------------------------------------------------------------------------
	// Lazy access methods
	// ------------------------------------------------------------------------------------
	public static Border getTextBorder() {
		if (textFieldBorder == null) {
			textFieldBorder = new TextFieldBorder();
		}
		return textFieldBorder;
	}

	public static Border getTextFieldBorder() {
		return getTextBorder();
	}

	public static Border getComboBoxBorder() {
		if (comboBoxBorder == null) {
			comboBoxBorder = new ComboBoxBorder();
		}
		return comboBoxBorder;
	}

	public static Border getScrollPaneBorder() {
		if (scrollPaneBorder == null) {
			scrollPaneBorder = new ScrollPaneBorder(false);
		}
		return scrollPaneBorder;
	}

	public static Border getTableScrollPaneBorder() {
		if (tableScrollPaneBorder == null) {
			tableScrollPaneBorder = new ScrollPaneBorder(true);
		}
		return tableScrollPaneBorder;
	}

	public static Border getLabelBorder() {
		if (labelBorder == null) {
			labelBorder = new LabelBorder();
		}
		return labelBorder;
	}

	public static Border getLabelLightBorder() {
		if (labelLightBorder == null) {
			labelLightBorder = new LabelLightBorder();
		}
		return labelLightBorder;
	}

	public static Border getLabelBorder(Color color, int arc) {
		return new LabelBorder(color, arc);
	}

	public static Border getButtonBorder() {
		if (buttonBorder == null) {
			buttonBorder = new ButtonBorder();
		}
		return buttonBorder;
	}

	public static Border getToggleButtonBorder() {
		return getButtonBorder();
	}

	public static Border getSeparatorBorder(final Color color, int dirc) {
		return new SeparatorBorder(color, dirc) {
			@Override
			public Color getLineColor() {
				return color;
			}
		};
	}

	public static Border getRolloverToolButtonBorder() {
		if (rolloverToolButtonBorder == null) {
			rolloverToolButtonBorder = new RolloverToolButtonBorder();
		}
		return rolloverToolButtonBorder;
	}

	public static Border getRoundRectBorder(Color color) {
		return new RoundRectBorder(color);
	}

	public static Border getInternalFrameBorder() {
		if (internalFrameBorder == null) {
			internalFrameBorder = new InternalFrameBorder();
			//			internalFrameBorder = ShadowBorder.getInstance();
		}
		return internalFrameBorder;
	}

	public static Border getTableHeaderBorder() {
		if (tableHeaderBorder == null) {
			tableHeaderBorder = new TableHeaderBorder();
		}
		return tableHeaderBorder;
	}

	// ------------------------------------------------------------------------------------
	// Implementation of border classes
	// ------------------------------------------------------------------------------------
	public static class ButtonBorder implements Border, UIResource {

		private static final Color defaultColorHi = new Color(220, 230, 245);
		private static final Color defaultColorMed = new Color(212, 224, 243);
		private static final Color defaultColorLo = new Color(200, 215, 240);
		private static final Insets insets = new Insets(3, 6, 3, 6);

		public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
			AbstractButton button = (AbstractButton) c;
			Graphics2D g2D = (Graphics2D) g;
			boolean isOverButton = false;
			if (button instanceof McWillRolloverButton) {
				isOverButton = true;
			}
			Color frameColor = AbstractLookAndFeel.getTheme().getFrameColor();
			if (isOverButton && button.getModel().isRollover()) {
				frameColor = McWillTheme.DEFAULT_COLOR;
			} else {
				frameColor = ColorHelper.brighter(frameColor, 40);
			}
			Object savedRederingHint = g2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
			g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			if (button.getRootPane() != null && button.equals(button.getRootPane().getDefaultButton())) {
				if (!button.getModel().isRollover()) {
					g2D.setColor(defaultColorHi);
					g2D.drawRoundRect(x + 1, y + 1, w - 4, h - 5, 6, 6);
					g2D.setColor(defaultColorMed);
					g2D.drawRoundRect(x + 2, y + 2, w - 6, h - 6, 6, 6);
					g2D.setColor(defaultColorLo);
					g2D.drawLine(x + 3, h - 3, w - 3, h - 3);
					g2D.drawLine(w - 3, y + 4, w - 3, h - 4);
				}
			}

			g2D.setColor(Color.white);
			g2D.drawRoundRect(x, y, w - 1, h - 1, 6, 6);

			g2D.setColor(frameColor);
			g2D.drawRoundRect(x, y, w - 2, h - 2, 6, 6);

			g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, savedRederingHint);
		}

		public Insets getBorderInsets(Component c) {
			return new Insets(insets.top, insets.left, insets.bottom, insets.right);
		}

		public Insets getBorderInsets(Component c, Insets borderInsets) {
			borderInsets.left = insets.left;
			borderInsets.top = insets.top;
			borderInsets.right = insets.right;
			borderInsets.bottom = insets.bottom;
			return borderInsets;
		}

		public boolean isBorderOpaque() {
			return true;
		}
	} // class ButtonBorder

	public static class RolloverToolButtonBorder implements Border, UIResource {

		private static final Insets insets = new Insets(2, 2, 2, 2);

		public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
			AbstractButton button = (AbstractButton) c;
			ButtonModel model = button.getModel();
			if (model.isEnabled()) {
				if ((model.isPressed() && model.isArmed()) || model.isSelected()) {
					Color frameColor = ColorHelper.darker(AbstractLookAndFeel.getToolbarBackgroundColor(), 20);
					g.setColor(frameColor);
					g.drawRect(x, y, w - 1, h - 1);

					Graphics2D g2D = (Graphics2D) g;
					Composite composite = g2D.getComposite();
					AlphaComposite alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1f);
					g2D.setComposite(alpha);
					g.setColor(Color.black);
					g.fillRect(x + 1, y + 1, w - 2, h - 2);
					g2D.setComposite(composite);
				} else if (model.isRollover()) {
					Color frameColor = AbstractLookAndFeel.getToolbarBackgroundColor();
					Color frameHiColor = ColorHelper.darker(frameColor, 5);
					Color frameLoColor = ColorHelper.darker(frameColor, 30);
					JTattooUtilities.draw3DBorder(g, frameHiColor, frameLoColor, x, y, w, h);
					frameHiColor = Color.white;
					frameLoColor = ColorHelper.brighter(frameLoColor, 60);
					JTattooUtilities.draw3DBorder(g, frameHiColor, frameLoColor, x + 1, y + 1, w - 2, h - 2);

					Graphics2D g2D = (Graphics2D) g;
					Composite composite = g2D.getComposite();
					AlphaComposite alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.2f);
					g2D.setComposite(alpha);
					g.setColor(Color.white);
					g.fillRect(x + 2, y + 2, w - 4, h - 4);
					g2D.setComposite(composite);
				} else if (model.isSelected()) {
					Color frameColor = AbstractLookAndFeel.getToolbarBackgroundColor();
					Color frameHiColor = Color.white;
					Color frameLoColor = ColorHelper.darker(frameColor, 30);
					JTattooUtilities.draw3DBorder(g, frameLoColor, frameHiColor, x, y, w, h);
				}
			}
		}

		public Insets getBorderInsets(Component c) {
			return new Insets(insets.top, insets.left, insets.bottom, insets.right);
		}

		public Insets getBorderInsets(Component c, Insets borderInsets) {
			borderInsets.left = insets.left;
			borderInsets.top = insets.top;
			borderInsets.right = insets.right;
			borderInsets.bottom = insets.bottom;
			return borderInsets;
		}

		public boolean isBorderOpaque() {
			return true;
		}
	} // class RolloverToolButtonBorder

	public static class SeparatorBorder extends LineBorder {

		private static final long serialVersionUID = 1L;
		public static int TOP = 1;
		public static int LEFT = 2;
		public static int BOTTOM = 3;
		public static int RIGTH = 4;
		private int direction = TOP;

		public SeparatorBorder() {
			super(null, 1, true);
		}

		public SeparatorBorder(Color color, int direction) {
			super(color, 1, true);
			this.direction = direction;
		}

		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
			Color color = getLineColor();
			if (color == null) {
				color = AbstractLookAndFeel.getTheme().getFrameColor();
				color = ColorHelper.brighter(color, 40);
			}
			Graphics2D g2d = (Graphics2D) g.create();
			g2d.setColor(color);
			switch (direction) {
			case 1:
				g2d.drawLine(0, 0, width, 0);
				break;
			case 2:
				g2d.drawLine(0, 0, 0, height);
				break;
			case 3:
				g2d.drawLine(0, height, width, height);
				break;
			case 4:
				g2d.drawLine(width, 0, width, height);
				break;
			default:
				break;
			}

			g2d.dispose();
		}

		@Override
		public Color getLineColor() {
			return super.getLineColor();
		}

		public void setDirection(int direction) {
			this.direction = direction;
		}

	}

	public static class LabelBorder extends LineBorder {

		private static final long serialVersionUID = 1L;

		private int arc = 10;

		public LabelBorder() {
			super(null, 1, true);
		}

		public LabelBorder(Color color, int arc) {
			super(color, 1, true);
			this.arc = arc;
		}

		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
			Color color = lineColor;
			if (color == null) {
				color = AbstractLookAndFeel.getTheme().getFrameColor();
				color = ColorHelper.brighter(color, 40);
			}
			Graphics2D g2d = (Graphics2D) g.create();
			Shape shape = g2d.getClip();
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setClip(shape);
			g2d.setColor(color);
			g2d.drawRoundRect(0, 0, width - 1, height - 1, arc, arc);
			g2d.dispose();
		}

	}

	public static class LabelLightBorder extends LineBorder {

		private static final long serialVersionUID = 1L;

		private int arc = 10;

		public LabelLightBorder() {
			super(null, 1, true);
		}

		public LabelLightBorder(Color color, int arc) {
			super(color, 1, true);
			this.arc = arc;
		}

		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
			Color color = lineColor;
			if (color == null) {
				color = McWillTheme.getThemeColor().getLightColor();
				color = ColorHelper.brighter(color, 40);
			}
			Graphics2D g2d = (Graphics2D) g.create();
			Shape shape = g2d.getClip();
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setClip(shape);
			g2d.setColor(color);
			g2d.drawRoundRect(0, 0, width - 1, height - 1, arc, arc);
			g2d.dispose();
		}

	}

	public static class RoundRectBorder extends LineBorder {

		private static final long serialVersionUID = 1L;

		public RoundRectBorder(Color color) {
			super(color, 1, true);
		}

		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

			RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			Color oldColor = g.getColor();
			Graphics2D g2 = (Graphics2D) g;
			int i;
			g2.setRenderingHints(rh);
			g2.setColor(lineColor);
			for (i = 0; i < thickness; i++) {
				if (!roundedCorners)
					g2.drawRect(x + i, y + i, width - i - i - 1, height - i - i - 1);
				else
					g2.drawRoundRect(x + i, y + i, width - i - i - 1, height - i - i - 1, 7, 7); //就是这一句
			}
			g2.setColor(oldColor);
		}
	}

	public static class RoundRectBorder1 extends LineBorder {

		private static final long serialVersionUID = 1L;

		public RoundRectBorder1(Color color) {
			super(color, 1, true);
		}

		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
			RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			Color oldColor = g.getColor();
			Graphics2D g2d = (Graphics2D) g;
			g2d.setRenderingHints(rh);
			RoundRectangle2D.Double rect = new RoundRectangle2D.Double(1, 1, width - 2, height - 2, 10, 10);
			g2d.setColor(lineColor);
			g2d.draw(rect);
			g2d.setColor(oldColor);
			super.paintBorder(c, g2d, x, y, width, height);
		}
	}

	public static class ComboBoxBorder extends AbstractBorder implements UIResource {

		private static final Color fieldBorderColor = new Color(127, 157, 185);
		private static final Insets insets = new Insets(1, 1, 1, 1);

		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
			width--;
			height--;
			g.setColor(fieldBorderColor);
			g.drawRect(x, y, width, height);
		}

		public Insets getBorderInsets(Component c) {
			return new Insets(insets.top, insets.left, insets.bottom, insets.right);
		}

		public Insets getBorderInsets(Component c, Insets borderInsets) {
			borderInsets.left = insets.left;
			borderInsets.top = insets.top;
			borderInsets.right = insets.right;
			borderInsets.bottom = insets.bottom;
			return borderInsets;
		}
	} // class ComboBoxBorder

	public static class TextFieldBorder extends AbstractBorder implements UIResource {

		private static final Color fieldBorderColor = new Color(127, 157, 185);
		private static final Insets insets = new Insets(2, 2, 2, 2);

		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
			width--;
			height--;
			g.setColor(fieldBorderColor);
			g.drawRect(x, y, width, height);
		}

		public Insets getBorderInsets(Component c) {
			return new Insets(insets.top, insets.left, insets.bottom, insets.right);
		}

		public Insets getBorderInsets(Component c, Insets borderInsets) {
			borderInsets.left = insets.left;
			borderInsets.top = insets.top;
			borderInsets.right = insets.right;
			borderInsets.bottom = insets.bottom;
			return borderInsets;
		}
	} // class TextFieldBorder

	public static class ScrollPaneBorder extends AbstractBorder implements UIResource {

		private static final Color fieldBorderColor = new Color(127, 157, 185);
		private static final Insets insets = new Insets(2, 2, 2, 2);
		private static final Insets tableInsets = new Insets(1, 1, 1, 1);
		private boolean tableBorder = false;

		public ScrollPaneBorder(boolean tableBorder) {
			this.tableBorder = tableBorder;
		}

		public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
			g.setColor(fieldBorderColor);
			g.drawRect(x, y, w - 1, h - 1);
			g.setColor(ColorHelper.brighter(AbstractLookAndFeel.getTheme().getBackgroundColor(), 50));
			g.drawRect(x + 1, y + 1, w - 3, h - 3);
		}

		public Insets getBorderInsets(Component c) {
			if (tableBorder) {
				return new Insets(tableInsets.top, tableInsets.left, tableInsets.bottom, tableInsets.right);
			} else {
				return new Insets(insets.top, insets.left, insets.bottom, insets.right);
			}
		}

		public Insets getBorderInsets(Component c, Insets borderInsets) {
			Insets ins = getBorderInsets(c);
			borderInsets.left = ins.left;
			borderInsets.top = ins.top;
			borderInsets.right = ins.right;
			borderInsets.bottom = ins.bottom;
			return borderInsets;
		}
	} // class ScrollPaneBorder

	public static class InternalFrameBorder extends BaseInternalFrameBorder {

		public InternalFrameBorder() {
			insets.top = 1;
		}

		public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
			boolean active = isActive(c);
			int th = getTitleHeight(c);
			Color titleColor = McWillLookAndFeel.getWindowTitleColorLight();
			titleColor = McWillTheme.getThemeColor().getSrcColor();
			Color borderColor = McWillLookAndFeel.getWindowBorderColor();
			borderColor = McWillTheme.getThemeColor().getMiddleColor();
			if (!active) {
				titleColor = ColorHelper.brighter(titleColor, 20);
				borderColor = ColorHelper.brighter(borderColor, 20);
			}
			//			g.setColor(titleColor);
			//			g.fillRect(x, y + 1, w, insets.top - 1);
			//			g.setColor(borderColor);
			//			g.fillRect(x + 1, y + h - dw, w - 2, dw - 1);
			//			g.fillRect(1, insets.top + th + 1, dw - 1, h - th - dw);
			//			g.fillRect(w - dw, insets.top + th + 1, dw - 1, h - th - dw);
			//
			//			if (active) {
			//				JTattooUtilities.fillHorGradient(g, McWillLookAndFeel.getTheme().getWindowTitleColors(), 1, insets.top, dw, th + 1);
			//				JTattooUtilities.fillHorGradient(g, McWillLookAndFeel.getTheme().getWindowTitleColors(), w - dw, insets.top, dw, th + 1);
			//			} else {
			//				JTattooUtilities.fillHorGradient(g, McWillLookAndFeel.getTheme().getWindowInactiveTitleColors(), 1, insets.top, dw, th + 1);
			//				JTattooUtilities.fillHorGradient(g, McWillLookAndFeel.getTheme().getWindowInactiveTitleColors(), w - dw, insets.top, dw, th + 1);
			//			}

			g.setColor(ColorHelper.darker(borderColor, 20));
			g.drawRect(x, y, w - 1, h - 1);
			//			g.drawLine(x + dw - 1, y + insets.top + th, x + dw - 1, y + h - dw);
			//			g.drawLine(x + w - dw, y + insets.top + th, x + w - dw, y + h - dw);
			//			g.drawLine(x + dw - 1, y + h - dw, x + w - dw, y + h - dw);
		}
	}

	public static class TableHeaderBorder extends AbstractBorder implements UIResource {

		private static final Insets insets = new Insets(0, 1, 1, 1);

		public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
			g.setColor(ColorHelper.brighter(AbstractLookAndFeel.getControlBackgroundColor(), 40));
			g.drawLine(0, 0, 0, h - 1);
			g.setColor(ColorHelper.darker(AbstractLookAndFeel.getControlBackgroundColor(), 20));
			g.drawLine(w - 1, 0, w - 1, h - 1);
			g.setColor(ColorHelper.darker(AbstractLookAndFeel.getControlBackgroundColor(), 10));
			g.drawLine(0, h - 1, w - 1, h - 1);
		}

		public Insets getBorderInsets(Component c) {
			return new Insets(insets.top, insets.left, insets.bottom, insets.right);
		}

		public Insets getBorderInsets(Component c, Insets borderInsets) {
			borderInsets.left = insets.left;
			borderInsets.top = insets.top;
			borderInsets.right = insets.right;
			borderInsets.bottom = insets.bottom;
			return borderInsets;
		}
	} // class TableHeaderBorder

	public static class RoundRectColorBorder extends LineBorder {

		private static final long serialVersionUID = 1L;

		private Color color = ColorUtil.getAlphaColor(Color.WHITE, 0);

		public RoundRectColorBorder() {
			super(null, 1, true);
			this.color = null;
		}

		public RoundRectColorBorder(Color color) {
			super(null, 1, true);
			this.color = color;
		}

		public void setRoundedCorners(boolean roundedCorners) {
			this.roundedCorners = roundedCorners;
		}

		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
			if (color == null)
				return;

			RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			Color oldColor = g.getColor();
			Graphics2D g2 = (Graphics2D) g;
			int i;
			g2.setRenderingHints(rh);

			g2.setColor(color);

			for (i = 0; i < thickness; i++) {
				if (roundedCorners)
					g2.drawRoundRect(x + i, y + i, width - i - i - 1, height - i - i - 1, 7, 7);
				else
					g2.drawRect(x + i, y + i, width - i - i - 1, height - i - i - 1);
			}
			g2.setColor(oldColor);
		}

		public void setColor(Color color) {
			this.color = color;
		}
	}

	public static class ColorBorder extends AbstractBorder implements UIResource {

		private static final Insets insets = new Insets(2, 2, 2, 2);

		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
			Color hiColor = getColor();
			Color loColor = AbstractLookAndFeel.getTheme().getFocusFrameColor();
			g.setColor(loColor);
			g.drawRect(x, y, width - 1, height - 1);
			g.setColor(hiColor);
			g.drawRect(x + 1, y + 1, width - 3, height - 3);
		}

		protected Color getColor() {
			return ColorHelper.brighter(AbstractLookAndFeel.getTheme().getFocusFrameColor(), 60);
		}

		public Insets getBorderInsets(Component c) {
			return new Insets(insets.top, insets.left, insets.bottom, insets.right);
		}

		public Insets getBorderInsets(Component c, Insets borderInsets) {
			borderInsets.left = insets.left;
			borderInsets.top = insets.top;
			borderInsets.right = insets.right;
			borderInsets.bottom = insets.bottom;
			return borderInsets;
		}

	} // class ColorBorder

	public static class ButtonRoundRectColorBorder extends LineBorder {

		private static final long serialVersionUID = 1L;

		public ButtonRoundRectColorBorder() {
			super(null, 1, true);
		}

		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

			RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			Color oldColor = g.getColor();
			Graphics2D g2 = (Graphics2D) g;
			int i;
			g2.setRenderingHints(rh);

			JButton b = (JButton) c;

			boolean isActive = JTattooUtilities.isActive(b);
			boolean isPressed = b.getModel().isPressed();
			boolean isArmed = b.getModel().isArmed();
			boolean isRollover = b.getModel().isRollover();

			if (!isActive) {
				g2.setColor(McWillTheme.getThemeColor().getLightColor());
			}
			if (isPressed && isArmed) {
				g2.setColor(McWillTheme.DEFAULT_COLOR);
			} else if (isRollover) {
				g2.setColor(McWillTheme.getThemeColor().getMiddleColor());
			} else {
				g2.setColor(McWillTheme.getThemeColor().getSrcColor());
			}

			for (i = 0; i < thickness; i++) {
				if (roundedCorners)
					g2.drawRoundRect(x + i, y + i, width - i - i - 1, height - i - i - 1, 10, 10);
				else
					g2.drawRect(x + i, y + i, width - i - i - 1, height - i - i - 1);
			}
			g2.setColor(oldColor);
		}

		public void setRoundedCorners(boolean roundedCorners) {
			this.roundedCorners = roundedCorners;
		}
	}

} // class 
