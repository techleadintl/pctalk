package com.xinwei.common.lookandfeel.listener;

import java.awt.event.WindowListener;

public interface McWillWindowListener extends WindowListener {
	public boolean preWindowClosing();
}
