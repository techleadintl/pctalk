package com.xinwei.common.lookandfeel.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;

@SuppressWarnings("serial")
public class McWillCheckButton extends McWillIconButton {
	private boolean selected;
	private ImageIcon uncheckImage;
	private ImageIcon checkImage;
	private boolean enable = true;

	public McWillCheckButton() {
		this(null, null, false);
	}

	public McWillCheckButton(boolean check) {
		this(null, null, check);
	}

	public McWillCheckButton(ImageIcon uncheckImage, ImageIcon checkImage) {
		this(uncheckImage, checkImage, false);
	}

	public McWillCheckButton(ImageIcon uncheckImage, ImageIcon checkImage, boolean check) {
		super(uncheckImage, checkImage, checkImage);
		this.uncheckImage = uncheckImage;
		this.checkImage = checkImage;
		setSelected(check);
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				McWillCheckButton.this.setSelected(!McWillCheckButton.this.selected);
			}
		});
	}

	public boolean isSelected() {
		return this.selected;
	}

	public void setSelected(boolean selection) {
		this.selected = selection;
		setImage(selection);
	}

	private void setImage(boolean check) {
		if (check)
			setIcon(this.checkImage);
		else
			setIcon(this.uncheckImage);
	}

	public ImageIcon getUncheckImage() {
		return this.uncheckImage;
	}

	public void setUncheckImage(ImageIcon uncheckImage) {
		this.uncheckImage = uncheckImage;
	}

	public ImageIcon getCheckImage() {
		return this.checkImage;
	}

	public void setCheckImage(ImageIcon checkImage) {
		this.checkImage = checkImage;
	}

	public boolean getEnabled() {
		return enable;
	}

	public void setEnabled(boolean enable) {
		this.enable = enable;
	}
}