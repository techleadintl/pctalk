package com.xinwei.common.lookandfeel;

import java.awt.Color;
import java.io.Serializable;

public class ThemeColor implements Serializable {
	private static int steps = 30;
	private Color srcColor;

	private Color darkColor;

	private Color subDarkColor;

	private Color middleColor;

	private Color middleColor1;

	private Color subLightColor;

	private Color lightColor;

	private Color destColor;

	private Color[] transitionalColor;

	public ThemeColor(Color srcColor) {
		this(srcColor, Color.WHITE);
	}

	public ThemeColor(Color srcColor, Color destColor) {
		this.srcColor = srcColor;
		this.destColor = destColor;

		transitionalColor = generateTransitionalColor(srcColor, destColor);
		int len = transitionalColor.length;
		darkColor = transitionalColor[1];
		subDarkColor = transitionalColor[len / 4];
		middleColor = transitionalColor[len / 2];
		middleColor1 = transitionalColor[len / 3];
		subLightColor = transitionalColor[3 * len / 4];
		lightColor = transitionalColor[len - 2];

	}

	public Color getColor(float alph) {
		int index = (int) (alph * steps);
		if (index >= steps)
			index = steps - 1;
		if (index < 0)
			index = 0;
		return transitionalColor[index];
	}

	public Color getAlphColor(int alph) {
		return new Color(srcColor.getRed(), srcColor.getGreen(), srcColor.getBlue(), alph);
	}

	public Color getSrcColor() {
		return srcColor;
	}

	public Color getDestColor() {
		return destColor;
	}

	public Color getDarkColor() {
		return darkColor;
	}

	public Color getSubDarkColor() {
		return subDarkColor;
	}

	public Color getMiddleColor() {
		return middleColor;
	}

	public Color getSubLightColor() {
		return subLightColor;
	}

	public Color getLightColor() {
		return lightColor;
	}

	public static Color[] generateTransitionalColor(Color color1, Color color2) {

		Color[] colors = new Color[steps];
		colors[0] = color1;
		colors[steps - 1] = color2;

		int redDiff = color2.getRed() - color1.getRed();
		int greenDiff = color2.getGreen() - color1.getGreen();
		int blueDiff = color2.getBlue() - color1.getBlue();
		for (int i = 1; i < steps - 1; i++) {
			int red = color1.getRed() + redDiff * i / steps;
			int green = color1.getGreen() + greenDiff * i / steps;
			int blue = color1.getBlue() + blueDiff * i / steps;
			colors[i] = new Color(red, green, blue);
		}

		return colors;
	}

	public Color getMiddleColor1() {
		return middleColor1;
	}

	public void setMiddleColor1(Color middleColor1) {
		this.middleColor1 = middleColor1;
	}
}
