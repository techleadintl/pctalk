/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月3日 下午3:08:58
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.listener;

import java.awt.event.WindowAdapter;

public class McWillWindowAdapter extends WindowAdapter implements McWillWindowListener {
	@Override
	public boolean preWindowClosing() {
		return false;
	}

}
