package com.xinwei.common.lookandfeel.ui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Container;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.ComponentUI;

import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.BaseMenuUI;
import com.jtattoo.plaf.ColorHelper;
import com.xinwei.common.lookandfeel.ImageColorInfo;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.util.Constants;
import com.xinwei.common.lookandfeel.util.MenuUtil;

public class McWillMenuUI extends BaseMenuUI {

	public static ComponentUI createUI(JComponent c) {
		return new McWillMenuUI();
	}

	protected void paintBackground(Graphics g, JComponent c, int x, int y, int w, int h) {
		int MENU_LEFT = MenuUtil.getMenuLeftInfo(c);
		if (MENU_LEFT==MenuUtil.DEFAULT) {
			super.paintBackground(g, c, x, y, w, h);
			return;
		}

		JMenuItem mi = (JMenuItem) c;
		ButtonModel model = mi.getModel();
		if (c.getParent() instanceof JMenuBar) {
			if (model.isRollover() || model.isArmed() || (c instanceof JMenu && model.isSelected())) {
				Color backColor = AbstractLookAndFeel.getMenuSelectionBackgroundColor();
				if (model.isRollover()) {
					backColor = ColorHelper.brighter(backColor, 10);
				}
				g.setColor(backColor);
				g.fillRect(x, y, w, h);
				if (paintRolloverBorder && model.isRollover() && !model.isSelected()) {
					backColor = ColorHelper.darker(backColor, 20);
					g.setColor(backColor);
					g.drawRect(x, y, w - 1, h - 1);
				}
			}
		} else {
			if (model.isArmed() || (c instanceof JMenu && model.isSelected())) {
				g.setColor(AbstractLookAndFeel.getMenuSelectionBackgroundColor());
				g.fillRect(x, y, w, h);
			} else {
				ColorUIResource menuBackgroundColor = AbstractLookAndFeel.getMenuBackgroundColor();
				if (!AbstractLookAndFeel.getTheme().isMenuOpaque()) {
					Graphics2D g2D = (Graphics2D) g;
					Composite composite = g2D.getComposite();
					AlphaComposite alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, AbstractLookAndFeel.getTheme()
							.getMenuAlpha());
					g2D.setComposite(alpha);
					g.setColor(menuBackgroundColor);
					g.fillRect(x, y, w, h);
					g2D.setComposite(composite);
				} else {
					g.setColor(menuBackgroundColor);
					g.fillRect(x + Constants.MENU_LEFT_WIDTH, y, w, h);
					{//add by liyong
						ThemeColor themeColor = McWillTheme.getThemeColor();
						Color srcColor = themeColor.getSrcColor();

						Color destColor = themeColor.getSubLightColor();
						GradientPaint paint;
						if (McWillTheme.isThemeImage()) {
							ImageColorInfo imageColorInfo = McWillTheme.getImageColorInfo();
							srcColor = imageColorInfo.getMostColor();
							destColor = themeColor.getMiddleColor();
							paint = new GradientPaint(0, 0, destColor, 30, 0, srcColor, true);
						} else {
							paint = new GradientPaint(0, 0, destColor, 200, 0, srcColor, true);
						}
						((Graphics2D) g).setPaint(paint);
						g.fillRect(x, y, Constants.MENU_LEFT_WIDTH, h);
					}

				}
			}
		}
		if (menuItem.isSelected() && menuItem.isArmed()) {
			g.setColor(AbstractLookAndFeel.getMenuSelectionForegroundColor());
		} else {
			g.setColor(AbstractLookAndFeel.getMenuForegroundColor());
		}
	}

	protected void paintText(Graphics g, JMenuItem menuItem, Rectangle textRect, String text) {
		Container parent = menuItem.getParent();
		if (!(parent instanceof JMenuBar)) {
			textRect.x += 15;
		}
		super.paintText(g, menuItem, textRect, text);
	}
}
