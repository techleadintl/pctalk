//package com.xinwei.common.lookandfeel.ui;
//
//import java.awt.BasicStroke;
//import java.awt.Color;
//import java.awt.Component;
//import java.awt.Font;
//import java.awt.FontMetrics;
//import java.awt.Graphics;
//import java.awt.Graphics2D;
//import java.awt.Insets;
//import java.awt.Point;
//import java.awt.Rectangle;
//import java.awt.RenderingHints;
//import java.awt.Shape;
//import java.awt.geom.Area;
//import java.awt.geom.Rectangle2D;
//import java.awt.geom.RoundRectangle2D;
//
//import javax.swing.Icon;
//import javax.swing.JComponent;
//import javax.swing.SwingUtilities;
//import javax.swing.plaf.ComponentUI;
//import javax.swing.plaf.UIResource;
//import javax.swing.text.View;
//
//import com.jtattoo.plaf.AbstractLookAndFeel;
//import com.jtattoo.plaf.JTattooUtilities;
//import com.xinwei.common.lookandfeel.McWillTheme;
//import com.xinwei.common.lookandfeel.ThemeColor;
//import com.xinwei.common.lookandfeel.util.ColorUtil;
//import com.xinwei.common.lookandfeel.util.Constants;
//
//public class McWillTabbedPaneUI extends McWillChatTabbedPaneUI {
//	private static Color sepColors[] = { AbstractLookAndFeel.getControlDarkShadow() };
//
//	public static ComponentUI createUI(WorkspacePane c) {
//		return new McWillTabbedPaneUI();
//	}
//
//	public void installDefaults() {
//		super.installDefaults();
//		Object tabareainsets = tabPane.getClientProperty(Constants.TABBEDPANE_TABAREAINSETS);
//		if (tabareainsets != null && tabareainsets instanceof Insets) {
//			tabAreaInsets = (Insets) tabareainsets;
//		} else {
//			tabAreaInsets = new Insets(2, 1, 0, 0);
//		}
//		contentBorderInsets = new Insets(0, 0, 0, 0);
//	}
//
//	protected void installComponents() {
//		simpleButtonBorder = true;
//		super.installComponents();
//	}
//
//	protected Font getTabFont(boolean isSelected) {
//		if (isSelected) {
//			return super.getTabFont(isSelected).deriveFont(Font.BOLD);
//		} else {
//			return super.getTabFont(isSelected);
//		}
//	}
//
//	protected Color getGapColor(int tabIndex) {
//		if (tabIndex == tabPane.getSelectedIndex() && (tabPane.getBackgroundAt(tabIndex) instanceof UIResource)) {
//			return AbstractLookAndFeel.getTheme().getBackgroundColor();
//		}
//		return super.getGapColor(tabIndex);
//	}
//
//	protected Color[] getTabColors(int tabIndex, boolean isSelected) {
//		ThemeColor themeColor = McWillTheme.getThemeColor();
//		boolean isChatTabbedPane = isChatTabbedPane();
//		if (isSelected) {
//			if (isChatTabbedPane) {
//				return new Color[] { themeColor.getDestColor(), themeColor.getSubLightColor() };
//			} else if (McWillTheme.isThemeImage()) {
//				return new Color[] { themeColor.getMiddleColor() };
//			} else {
//				return new Color[] { themeColor.getSrcColor() };
//			}
//		} else if (tabIndex == rolloverIndex) {
//			if (isChatTabbedPane) {
//				return new Color[] { themeColor.getSubLightColor() };
//			} else if (McWillTheme.isThemeImage()) {
//				return new Color[] { themeColor.getSubLightColor() };
//			} else {
//				return new Color[] { themeColor.getMiddleColor() };
//			}
//		} else {
//			if (isChatTabbedPane) {
//				return new Color[] { themeColor.getSrcColor() };
//			} else if (McWillTheme.isThemeImage()) {
//				//				return new Color[] { ColorUtil.getAlphaColor(Color.WHITE, 0) };
//				return new Color[] { themeColor.getSrcColor() };
//			} else {
//				return new Color[] { themeColor.getSubDarkColor() };
//			}
//		}
//
//	}
//
//	@Override
//	protected Color getLoBorderColor(int tabIndex) {
//		ThemeColor themeColor = McWillTheme.getThemeColor();
//		if (themeColor == null)
//			return AbstractLookAndFeel.getControlDarkShadow();
//		return themeColor.getSrcColor();
//	}
//
//	@Override
//	protected Color getSelectedBorderColor(int tabIndex) {
//		ThemeColor themeColor = McWillTheme.getThemeColor();
//		if (themeColor == null)
//			return AbstractLookAndFeel.getControlDarkShadow();
//		return themeColor.getDestColor();
//	}
//
//	protected Color[] getContentBorderColors(int tabPlacement) {
//		return sepColors;
//	}
//
//	protected boolean hasInnerBorder() {
//		return true;
//	}
//
//	/**
//	 * tab
//	 */
//	protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics) {
//		Object clientProperty = tabPane.getClientProperty("TabWidth");
//		if (clientProperty != null) {
//			try {
//				return Integer.valueOf(clientProperty.toString());
//			} catch (Exception e) {
//			}
//		}
//		Insets insets = getTabInsets(tabPlacement, tabIndex);
//		int width = insets.left + insets.right;
//		Component tabComponent = null;
//		if (JTattooUtilities.getJavaVersion() >= 1.6) {
//			tabComponent = tabPane.getTabComponentAt(tabIndex);
//		}
//		if (tabComponent != null) {
//			width += tabComponent.getPreferredSize().width;
//		} else {
//			Icon icon = getIconForTab(tabIndex);
//			if (icon != null) {
//				width += icon.getIconWidth() + textIconGap;
//			}
//			View v = getTextViewForTab(tabIndex);
//			if (v != null) {
//				// html
//				width += (int) v.getPreferredSpan(View.X_AXIS);
//			} else {
//				// plain text
//				String tabTitle = tabPane.getTitleAt(tabIndex);
//				width += SwingUtilities.computeStringWidth(metrics, tabTitle);
//			}
//		}
//
//		return width;
//	}
//
//	/**
//	 * tab
//	 */
//	protected int calculateTabHeight(int tabPlacement, int tabIndex, int fontHeight) {
//		Object clientProperty = tabPane.getClientProperty("TabHeight");
//		if (clientProperty != null) {
//			try {
//				return Integer.valueOf(clientProperty.toString());
//			} catch (Exception e) {
//			}
//		}
//		int height = 0;
//		Component c = null;
//		if (JTattooUtilities.getJavaVersion() >= 1.6) {
//			c = tabPane.getTabComponentAt(tabIndex);
//		}
//		if (c != null) {
//			height = c.getPreferredSize().height;
//		} else {
//			View v = getTextViewForTab(tabIndex);
//			if (v != null) {
//				// html
//				height += (int) v.getPreferredSpan(View.Y_AXIS);
//			} else {
//				// plain text
//				height += fontHeight;
//			}
//			Icon icon = getIconForTab(tabIndex);
//			if (icon != null) {
//				height = Math.max(height, icon.getIconHeight());
//			}
//		}
//		Insets ti = getTabInsets(tabPlacement, tabIndex);
//		height += ti.top + ti.bottom + 7;
//		return height;
//	}
//
//	// @Override
//	protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h) {
//		if (selectedIndex<0||tabPane.getTabCount() <= selectedIndex)
//			return;
//		Insets bi = new Insets(0, 0, 0, 0);
//		if (tabPane.getBorder() != null) {
//			bi = tabPane.getBorder().getBorderInsets(tabPane);
//		}
//		Component selectedComponent = tabPane.getTabComponentAt(selectedIndex);
//
//		if (tabPlacement != TOP)
//			return;
//
//		int tabAreaHeight = calculateTabAreaHeight(tabPlacement, runCount, maxTabHeight);
//		Insets selectedTabPadInsets = getSelectedTabPadInsets(tabPlacement);
//
//		int x1 = x + bi.left - 1;
//		int y1 = y + tabAreaHeight + bi.top - 1;
//		int x2 = x1 + w - bi.left - bi.right + 1;
//
//		g.setColor(ColorUtil.getAlphaColor(McWillTheme.getThemeColor().getSubDarkColor(), 200));
//		((Graphics2D) g).setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.0f, null, 0.0f));
//
//		if (tabPane.getBorder() == null) {
//			// g.drawLine(x1, y1, x2, y1);
//			// g.setColor(hiColor);
//			g.drawLine(x1, y1 + 1, x2, y1 + 1);
//		} else {
//			// g.drawLine(x1, y1, x2-1, y1+1);
//			// g.setColor(McWillTheme.getThemeColor().getSubDarkColor());
//			if (McWillTheme.isThemeImage() && selectedComponent != null) {
//				Rectangle bounds = selectedComponent.getBounds();
//				Point tabbedPaneOnScreen = tabPane.getLocationOnScreen();
//				Point tabOnScreen = selectedComponent.getLocationOnScreen();
//				bounds.x = tabOnScreen.x - tabbedPaneOnScreen.x - 1;
//				bounds.y = tabOnScreen.y - tabbedPaneOnScreen.y - 1;
//				g.setColor(ColorUtil.getAlphaColor(Color.white, 128));
//				g.drawLine(x1 + 1, y1 + 1, bounds.x - 2 * selectedTabPadInsets.left - 1, y1 + 1);
//
//				g.drawLine(bounds.x + bounds.width + 2 * selectedTabPadInsets.left + 2 * selectedTabPadInsets.right + 1, y1 + 1, x2 - 1, y1 + 1);
//			} else {
//				g.drawLine(x1 + 1, y1 + 1, x2 - 1, y1 + 1);
//			}
//		}
//	}
//
//	protected void paintRoundedTopTabBorder(int tabIndex, Graphics g, int x1, int y1, int x2, int y2, boolean isSelected) {
//		Graphics2D g2D = (Graphics2D) g;
//		Object savedRederingHint = g2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
//		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//		Color borderColor = getLoBorderColor(tabIndex);
//		ThemeColor themeColor = McWillTheme.getThemeColor();
//		if (isSelected) {
//			// borderColor = getSelectedBorderColor(tabIndex);
//			// borderColor = McWillTheme.getTabbedTopLineHiColor();
//			borderColor = themeColor.getSrcColor();
//		}
//		if (McWillTheme.isThemeImage()) {
//			borderColor = ColorUtil.getSaturatedColor(themeColor.getSrcColor(), 0.1);
//		}
//
//		g.setColor(borderColor);
//		g.setColor(ColorUtil.getAlphaColor(themeColor.getLightColor(), 32));
//		int d = 2 * GAP;
//		if (isSelected) {
//			g.drawLine(x1 + GAP, y1, x2 - GAP, y1);
//			g.drawArc(x1, y1, d, d, 90, 90);
//			g.drawArc(x2 - d, y1, d, d, 0, 90);
//			g.drawLine(x1, y1 + GAP + 1, x1, y2);
//			g.drawLine(x2, y1 + GAP + 1, x2, y2);
//		} else if (!isChatTabbedPane()) {
//			g.drawLine(x1 + GAP, y1, x2 - GAP, y1);
//			g.drawArc(x1, y1, d, d, 90, 90);
//			g.drawArc(x2 - d, y1, d, d, 0, 90);
//			g.drawLine(x1, y1 + GAP + 1, x1, y2);
//			g.drawLine(x2, y1 + GAP + 1, x2, y2);
//		}
//		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, savedRederingHint);
//	}
//
//	@Override
//	protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
//		boolean isChatTabbedPane = isChatTabbedPane();
//		String chatTabbedPaneType = getChatTabbedPaneType();
//		if (isChatTabbedPane) {
//			if (isTabOpaque() || isSelected) {
//				Graphics2D g2D = (Graphics2D) g;
//				Shape savedClip = g.getClip();
//				Area orgClipArea = new Area(savedClip);
//				ThemeColor themeColor = McWillTheme.getThemeColor();
//				Color colorArr[] = new Color[] { themeColor.getSrcColor(), themeColor.getSubDarkColor() };
//				int d = 2 * GAP;
//				if (isSelected) {
//					if ("1".equals(chatTabbedPaneType)) {
//						super.paintTabBackground(g, tabPlacement, tabIndex, x, y, w, h, isSelected);
//						return;
//					}
//					Area clipArea = new Area(new RoundRectangle2D.Double(x, y, w, h + 4, d, d));
//					Area rectArea = new Area(new Rectangle2D.Double(x, y, w, h + 2));
//					clipArea.intersect(rectArea);
//					clipArea.intersect(orgClipArea);
//					g2D.setClip(clipArea);
//					if (McWillTheme.isThemeColor()) {
//						JTattooUtilities.fillHorGradient(g, colorArr, x, y, w, h + 4);
//					}
//					g2D.setClip(savedClip);
//				} else if (rolloverIndex == tabIndex) {
//					if ("1".equals(chatTabbedPaneType)) {//main
//						Area clipArea = new Area(new RoundRectangle2D.Double(x, y, w, h + 4, d, d));
//						Area rectArea = new Area(new RoundRectangle2D.Double(x + 2, y + 1, w - 5, h + 2 - 0, 4, 4));
//						clipArea.intersect(rectArea);
//						clipArea.intersect(orgClipArea);
//						g2D.setClip(clipArea);
//
//						if (McWillTheme.isThemeImage()) {
//							colorArr = new Color[] { themeColor.getSrcColor() };
//						} else {
//							colorArr = new Color[] { ColorUtil.getAlphaColor(themeColor.getMiddleColor(), 128) };
//						}
//						JTattooUtilities.fillHorGradient(g, colorArr, x, y, w, h + 4);
//						g2D.setClip(savedClip);
//					} else {//chat
//						Area clipArea = new Area(new RoundRectangle2D.Double(x, y, w, h + 4, d, d));
//						Area rectArea = new Area(new RoundRectangle2D.Double(x + 5, y + 1, w - 8, h + 2 - 6, 4, 4));
//						clipArea.intersect(rectArea);
//						clipArea.intersect(orgClipArea);
//						g2D.setClip(clipArea);
//
//						colorArr = new Color[] { ColorUtil.getAlphaColor(themeColor.getLightColor(), 32) };
//
//						JTattooUtilities.fillHorGradient(g, colorArr, x, y, w, h + 4);
//						g2D.setClip(savedClip);
//					}
//				}
//			}
//		} else {
//			super.paintTabBackground(g, tabPlacement, tabIndex, x, y, w, h, isSelected);
//		}
//	}
//
//	private boolean isChatTabbedPane() {
//		//		boolean isChatTabbedPane = false;
//		//		String chatTabbedPaneType = getChatTabbedPaneType();
//		//		if ("1".equals(chatTabbedPaneType) || "2".equals(chatTabbedPaneType)) {
//		//			isChatTabbedPane = true;
//		//		}
//		return true;
//	}
//
//	private String getChatTabbedPaneType() {
//		Object clientProperty = tabPane.getClientProperty("McWillTabbedPane");
//		if (clientProperty != null) {
//			return clientProperty.toString();
//		}
//		return null;
//	}
//}