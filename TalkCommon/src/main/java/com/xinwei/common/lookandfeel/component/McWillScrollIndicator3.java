package com.xinwei.common.lookandfeel.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.plaf.ScrollBarUI;
import javax.swing.plaf.basic.BasicScrollBarUI;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;

public class McWillScrollIndicator3 extends JLayeredPane {
	private int thumbThickness = 8;
	private int thumbMargin = 3;
	private int thumbMinSize = 150;

	private final JScrollPane scrollPane;
	private final MyControlPanel controlPanel;

	public McWillScrollIndicator3(final JComponent view) {
		this(view, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED,150);
	}

	public McWillScrollIndicator3(final JComponent view, int thumb_min_size) {
		this(view);
	}

	public McWillScrollIndicator3(final JComponent view, int vsbPolicy, int hsbPolicy, int thumb_min_size) {
		this.thumbMinSize = thumb_min_size;
		scrollPane = new JScrollPane(view, vsbPolicy, hsbPolicy);
		scrollPane.setBorder(BorderFactory.createEmptyBorder());
		add(scrollPane, JLayeredPane.DEFAULT_LAYER);

		controlPanel = new MyControlPanel(scrollPane);
		add(controlPanel, JLayeredPane.PALETTE_LAYER);

		addComponentListener(new ComponentAdapter() {

			@Override
			public void componentResized(ComponentEvent e) {
				// listen to changes of JLayeredPane size
				scrollPane.setSize(getSize());
				scrollPane.getViewport().revalidate();
				controlPanel.setSize(getSize());
				controlPanel.revalidate();
			}
		});
	}

	public void setHorizontalScrollBarPolicy(int policy) {
		scrollPane.setHorizontalScrollBarPolicy(policy);
	}

	public void setVerticalScrollBarPolicy(int policy) {
		scrollPane.setVerticalScrollBarPolicy(policy);
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	private class MyControlPanel extends JPanel {

		private final MyScrollBar vScrollBar;
		private final MyScrollBar hScrollBar;

		private MyControlPanel(JScrollPane scrollPane) {

			setLayout(new BorderLayout());
			setOpaque(false);

			vScrollBar = new MyScrollBar(JScrollBar.VERTICAL);
			scrollPane.setVerticalScrollBar(vScrollBar);
			scrollPane.remove(vScrollBar);
			if (scrollPane.getVerticalScrollBarPolicy() != JScrollPane.VERTICAL_SCROLLBAR_NEVER) {
				add(vScrollBar, BorderLayout.EAST);
			}

			hScrollBar = new MyScrollBar(JScrollBar.HORIZONTAL);
			scrollPane.setHorizontalScrollBar(hScrollBar);
			scrollPane.remove(hScrollBar);
			if (scrollPane.getHorizontalScrollBarPolicy() != JScrollPane.HORIZONTAL_SCROLLBAR_NEVER) {
				add(hScrollBar, BorderLayout.SOUTH);
			}
		}
	}

	private class MyScrollBar extends JScrollBar {

		protected final MyScrollBarUI scrollUI;

		public MyScrollBar(int direction) {
			super(direction);

			scrollUI = new MyScrollBarUI(this);
			super.setUI(scrollUI);

			int size = getThumbThickness() + getThumbMargin();
			setPreferredSize(new Dimension(size, 1));
			addMouseListener(new MouseAdapter() {
				@Override
				public void mouseEntered(MouseEvent e) {
					scrollUI.setVisible(true, true);
				}

				@Override
				public void mouseExited(MouseEvent e) {
					scrollUI.setVisible(false, false);
				}
			});

			addAdjustmentListener(new AdjustmentListener() {
				@Override
				public void adjustmentValueChanged(AdjustmentEvent e) {
					scrollUI.setVisible(true, false);
				}
			});
		}

		@Override
		public void setUI(ScrollBarUI ui) {
		}

		@Override
		public void updateUI() {
		}

		@Override
		public void paint(Graphics g) {
			scrollUI.paintThumb(g, this); // just the thumb
		}

		@Override
		public void repaint(Rectangle r) {
			McWillScrollIndicator3 scrollIndicator = McWillScrollIndicator3.this;
			Rectangle rect = SwingUtilities.convertRectangle(this, r, scrollIndicator);
			rect.grow(1, 1);
			// ensure for a translucent thumb, that the view is first painted
			scrollIndicator.repaint(rect);
		}
	}

	public class MyScrollBarUI extends BasicScrollBarUI {

		private MyScrollBar scrollBar;
		private float alpha = 0.0f;
		private FadeAnimation fadeAnimation;

		private MyScrollBarUI(MyScrollBar scrollBar) {
			this.scrollBar = scrollBar;
			fadeAnimation = new FadeAnimation(this);
		}

		@Override
		protected void installComponents() {
			incrButton = new JButton();
			decrButton = new JButton();
			if (scrollBar.getOrientation() == JScrollBar.HORIZONTAL) {
				int size = getThumbThickness() + getThumbMargin(); // let lower right corner empty
				incrButton.setPreferredSize(new Dimension(1, size));
			} else {
				incrButton.setPreferredSize(new Dimension(getThumbMargin(), 1));
			}
			decrButton.setPreferredSize(new Dimension(getThumbMargin(), 1));
		}

		@Override
		protected void installDefaults() {
			super.installDefaults();

			// ensure the minimum size of the thumb
			int w = minimumThumbSize.width;
			int h = minimumThumbSize.height;
			if (scrollBar.getOrientation() == JScrollBar.VERTICAL) {
				h = Math.max(h, Math.min(maximumThumbSize.height, getThumbMinSize()));
			} else {
				w = Math.max(w, Math.min(maximumThumbSize.width, getThumbMinSize()));
			}
			minimumThumbSize = new Dimension(w, h);
		}

		private void paintThumb(Graphics g, JComponent c) {
			if (alpha == 0.0f) {
				return; // don't paint anything
			}

			if (!c.isEnabled()) {
				return;
			}
			Rectangle thumbBounds = getThumbBounds();
			Graphics2D g2D = (Graphics2D) g;

			int x = thumbBounds.x;
			int y = thumbBounds.y;
			int width = thumbBounds.width;
			int height = thumbBounds.height;

			ThemeColor themeColor;
			if (McWillTheme.isThemeColor()) {
				themeColor = McWillTheme.getThemeColor();
			} else {
				themeColor = McWillTheme.getImageColorInfo().getThemeColor();
			}
			Color fillColor = getAlphaColor(themeColor.getColor(0.35f));
			Color dragFillColor = getAlphaColor(themeColor.getColor(0.3f));
			Color dragBorderColor = getAlphaColor(themeColor.getColor(0.2f));
			int i = 3;
			if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
				if (isDragging) {
					g2D.setColor(dragBorderColor);
				} else {
					g2D.setColor(fillColor);
				}
				width -= i + 1;
				g2D.drawArc(x + i - 1, y - 1, width, 10, 0, 180);
				g2D.drawRect(x + i - 1, y + 5, width, height - 10);
				g2D.drawArc(x + i - 1, height + y - 10, width, 10, 0, -180);

				if (isDragging) {
					g2D.setColor(dragFillColor);
				} else {
					g2D.setColor(fillColor);
				}
				width -= 1;
				g2D.setColor(fillColor);
				g2D.fillArc(x + i, y, width, 10, 0, 180);
				g2D.fillRect(x + i, y + 5, width, height - 10);
				g2D.fillArc(x + i, height + y - 10, width, 10, 0, -180);

			} else {
				if (isDragging) {
					g2D.setColor(dragBorderColor);
				} else {
					g2D.setColor(fillColor);
				}
				height -= i + 1;
				g.drawArc(x, y + i - 1, 10, height, 90, 180);
				g.drawRect(x + 5, y + i - 1, width - 10, height);
				g.drawArc(width + x - 10, y + i - 1, 10, height, -90, 180);

				if (isDragging) {
					g2D.setColor(dragFillColor);
				} else {
					g2D.setColor(fillColor);
				}
				height -= 1;
				g2D.setColor(fillColor);
				g.fillArc(x, y + i, 10, height, 90, 180);
				g.fillRect(x + 5, y + i, width - 10, height);
				g.fillArc(width + x - 10, y + i, 10, height, -90, 180);
			}

		}

		private Color getAlphaColor(Color color) {
			if (alpha == 1.0f) {
				return color;
			}
			int rgb = color.getRGB() & 0xFFFFFF; // color without alpha values
			rgb |= ((int) (alpha * 255)) << 24; // add alpha object
			return new Color(rgb, true);
		}

		public void setAlpha(float alpha) {
			this.alpha = alpha;
			scrollBar.repaint(getThumbBounds());
		}

		public void setVisible(boolean visible, boolean mouseOver) {
			if (visible) {
				fadeAnimation.fadeIn(mouseOver);
			} else {
				fadeAnimation.fadeOut();
			}
			scrollBar.repaint(getThumbBounds());
		}
	}

	private class FadeAnimation {

		private final MyScrollBarUI scrollUI;
		private boolean isFadeIn;

		private FadeAnimation(MyScrollBarUI scrollUI) {
			this.scrollUI = scrollUI;
		}

		public synchronized void fadeIn(boolean mouseOver) {
			if (!isFadeIn) {
				isFadeIn = true;
				scrollUI.setAlpha(1);
				if (!mouseOver) {
					fadeOut();
				}
			}
		}

		public synchronized void fadeOut() {
			if (isFadeIn) {
				isFadeIn = false;
				scrollUI.setAlpha(0);
			}
		}
	}

	public int getThumbMinSize() {
		return thumbMinSize;
	}

	public int getThumbThickness() {
		return thumbThickness;
	}

	public int getThumbMargin() {
		return thumbMargin;
	}


	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new JFrame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				String text = "";
				for (int i = 0; i < 100; i++) {
					for (int j = 0; j < 100; j++) {
						text += i + " - " + j;
					}
					text += "\n";
				}
				JTextArea area = new JTextArea(text);
				McWillScrollIndicator3 scrollIndicator = new McWillScrollIndicator3(area);
				frame.getContentPane().add(scrollIndicator);
				frame.setBounds(100, 100, 200, 300);
				frame.setVisible(true);
			}
		});
	}

}