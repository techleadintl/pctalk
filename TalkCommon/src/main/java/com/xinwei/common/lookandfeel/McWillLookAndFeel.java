package com.xinwei.common.lookandfeel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javax.swing.UIDefaults;

import com.jtattoo.plaf.AbstractBorderFactory;
import com.jtattoo.plaf.AbstractIconFactory;
import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.BaseCheckBoxUI;
import com.jtattoo.plaf.BaseDesktopPaneUI;
import com.jtattoo.plaf.BaseEditorPaneUI;
import com.jtattoo.plaf.BaseFileChooserUI;
import com.jtattoo.plaf.BaseFormattedTextFieldUI;
import com.jtattoo.plaf.BaseLabelUI;
import com.jtattoo.plaf.BasePasswordFieldUI;
import com.jtattoo.plaf.BaseProgressBarUI;
import com.jtattoo.plaf.BaseRadioButtonMenuItemUI;
import com.jtattoo.plaf.BaseRadioButtonUI;
import com.jtattoo.plaf.BaseSeparatorUI;
import com.jtattoo.plaf.BaseSliderUI;
import com.jtattoo.plaf.BaseSpinnerUI;
import com.jtattoo.plaf.BaseSplitPaneUI;
import com.jtattoo.plaf.BaseTabbedPaneUI;
import com.jtattoo.plaf.BaseTableUI;
import com.jtattoo.plaf.BaseTextAreaUI;
import com.jtattoo.plaf.BaseTextFieldUI;
import com.jtattoo.plaf.BaseToggleButtonUI;
import com.jtattoo.plaf.BaseToolTipUI;
import com.jtattoo.plaf.BaseTreeUI;
import com.jtattoo.plaf.JTattooUtilities;
import com.xinwei.common.lookandfeel.ui.McWillButtonUI;
import com.xinwei.common.lookandfeel.ui.McWillCheckBoxMenuItemUI;
import com.xinwei.common.lookandfeel.ui.McWillComboBoxUI;
import com.xinwei.common.lookandfeel.ui.McWillInternalFrameUI;
import com.xinwei.common.lookandfeel.ui.McWillMenuBarUI;
import com.xinwei.common.lookandfeel.ui.McWillMenuItemUI;
import com.xinwei.common.lookandfeel.ui.McWillMenuUI;
import com.xinwei.common.lookandfeel.ui.McWillPanelUI;
import com.xinwei.common.lookandfeel.ui.McWillPopupMenuUI;
import com.xinwei.common.lookandfeel.ui.McWillRootPaneUI;
import com.xinwei.common.lookandfeel.ui.McWillScrollBarHideUI;
import com.xinwei.common.lookandfeel.ui.McWillScrollPaneUI;
import com.xinwei.common.lookandfeel.ui.McWillSeparatorUI;
import com.xinwei.common.lookandfeel.ui.McWillTableHeaderUI;
import com.xinwei.common.lookandfeel.ui.McWillToolBarUI;
import com.xinwei.talk.ui.skin.skin.SkinColorChooserUI;

@SuppressWarnings({"serial","rawtypes", "unchecked"})
public class McWillLookAndFeel extends AbstractLookAndFeel {

	private static McWillDefaultTheme myTheme = null;

	private static final ArrayList themesList = new ArrayList();
	private static final HashMap themesMap = new HashMap();
	private static final Properties defaultProps = new Properties();
	private static final Properties smallFontProps = new Properties();
	private static final Properties largeFontProps = new Properties();
	private static final Properties giantFontProps = new Properties();

	static {
		smallFontProps.setProperty("controlTextFont", "Dialog 10");
		smallFontProps.setProperty("systemTextFont", "Dialog 10");
		smallFontProps.setProperty("userTextFont", "Dialog 10");
		smallFontProps.setProperty("menuTextFont", "Dialog 10");
		smallFontProps.setProperty("windowTitleFont", "Dialog bold 10");
		smallFontProps.setProperty("subTextFont", "Dialog 8");

		largeFontProps.setProperty("controlTextFont", "Dialog 14");
		largeFontProps.setProperty("systemTextFont", "Dialog 14");
		largeFontProps.setProperty("userTextFont", "Dialog 14");
		largeFontProps.setProperty("menuTextFont", "Dialog 14");
		largeFontProps.setProperty("windowTitleFont", "Dialog bold 14");
		largeFontProps.setProperty("subTextFont", "Dialog 12");

		giantFontProps.setProperty("controlTextFont", "Dialog 18");
		giantFontProps.setProperty("systemTextFont", "Dialog 18");
		giantFontProps.setProperty("userTextFont", "Dialog 18");
		giantFontProps.setProperty("menuTextFont", "Dialog 18");
		giantFontProps.setProperty("windowTitleFont", "Dialog 18");
		giantFontProps.setProperty("subTextFont", "Dialog 16");

		themesList.add("Default");
		themesList.add("Small-Font");
		themesList.add("Large-Font");
		themesList.add("Giant-Font");

		themesMap.put("Default", defaultProps);
		themesMap.put("Small-Font", smallFontProps);
		themesMap.put("Large-Font", largeFontProps);
		themesMap.put("Giant-Font", giantFontProps);
	}

	public static java.util.List getThemes() {
		return themesList;
	}

	public static Properties getThemeProperties(String name) {
		return ((Properties) themesMap.get(name));
	}

	public static void setTheme(String name) {
		if (myTheme != null) {
			McWillDefaultTheme.setInternalName(name);
		}
		setTheme((Properties) themesMap.get(name));
	}

	public static void setTheme(String name, String licenseKey, String logoString) {
		Properties props = (Properties) themesMap.get(name);
		props.put("licenseKey", licenseKey);
		props.put("logoString", logoString);
		if (myTheme != null) {
			McWillDefaultTheme.setInternalName(name);
		}
		setTheme(props);
	}

	public static void setTheme(Properties themesProps) {
		if (myTheme == null) {
			myTheme = new McWillDefaultTheme();
		}
		if ((myTheme != null) && (themesProps != null)) {
			myTheme.setUpColor();
			myTheme.setProperties(themesProps);
			myTheme.setUpColorArrs();
			AbstractLookAndFeel.setTheme(myTheme);
		}
	}

	public static void setCurrentTheme(Properties themesProps) {
		setTheme(themesProps);
	}

	public String getName() {
		return "XP";
	}

	public String getID() {
		return "XP";
	}

	public String getDescription() {
		return "The XP Look and Feel";
	}

	public boolean isNativeLookAndFeel() {
		return false;
	}

	public boolean isSupportedLookAndFeel() {
		return true;
	}

	public AbstractBorderFactory getBorderFactory() {
		return McWillBorderFactory.getInstance();
	}

	public AbstractIconFactory getIconFactory() {
		return McWillIconFactory.getInstance();
	}

	protected void createDefaultTheme() {
		if (myTheme == null) {
			myTheme = new McWillDefaultTheme();
		}
		setTheme(myTheme);
	}

	protected void initClassDefaults(UIDefaults table) {
		super.initClassDefaults(table);
		Object[] uiDefaults = {
				// BaseLookAndFeel classes
				"LabelUI", BaseLabelUI.class.getName(), 
				"ToggleButtonUI", BaseToggleButtonUI.class.getName(), 
				"SeparatorUI", BaseSeparatorUI.class.getName(), 
				"TextFieldUI",BaseTextFieldUI.class.getName(), 
				"TextAreaUI", BaseTextAreaUI.class.getName(), 
				"EditorPaneUI", BaseEditorPaneUI.class.getName(), 
				"PasswordFieldUI",BasePasswordFieldUI.class.getName(), 
				"CheckBoxUI", BaseCheckBoxUI.class.getName(), 
				"RadioButtonUI", BaseRadioButtonUI.class.getName(), 
				"ToolTipUI", BaseToolTipUI.class.getName(),
				"TreeUI", BaseTreeUI.class.getName(), 
				"TableUI", BaseTableUI.class.getName(), 
				"SliderUI", BaseSliderUI.class.getName(), 
				"ProgressBarUI", BaseProgressBarUI.class.getName(),
				"SplitPaneUI", BaseSplitPaneUI.class.getName(),
				"FileChooserUI",BaseFileChooserUI.class.getName(),  
				"RadioButtonMenuItemUI", BaseRadioButtonMenuItemUI.class.getName(),
				"DesktopPaneUI",BaseDesktopPaneUI.class.getName(),

				// LunaLookAndFeel classes
				"ButtonUI", McWillButtonUI.class.getName(), 
				"ComboBoxUI", McWillComboBoxUI.class.getName(), 
				"ScrollBarUI", McWillScrollBarHideUI.class.getName(), 
				"TabbedPaneUI",	BaseTabbedPaneUI.class.getName(), 
				"TableHeaderUI", McWillTableHeaderUI.class.getName(), 
				"ToolBarUI", McWillToolBarUI.class.getName(), 
				"InternalFrameUI",McWillInternalFrameUI.class.getName(), 
				"RootPaneUI", McWillRootPaneUI.class.getName(), 
				"MenuBarUI", McWillMenuBarUI.class.getName(),
				"ScrollPaneUI", McWillScrollPaneUI.class.getName(),
				"PanelUI", McWillPanelUI.class.getName(),
				"MenuUI", McWillMenuUI.class.getName(),   
				"PopupMenuUI", McWillPopupMenuUI.class.getName(),
				"MenuItemUI",McWillMenuItemUI.class.getName(),
				"CheckBoxMenuItemUI", McWillCheckBoxMenuItemUI.class.getName(),
				"PopupMenuSeparatorUI", McWillSeparatorUI.class.getName(),
				
		};
		table.putDefaults(uiDefaults);
		if (JTattooUtilities.getJavaVersion() >= 1.5) {
			table.put("FormattedTextFieldUI", BaseFormattedTextFieldUI.class.getName());
			table.put("SpinnerUI", BaseSpinnerUI.class.getName());
		}
	}
}