package com.xinwei.common.lookandfeel;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;

import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.ColorHelper;
import com.jtattoo.plaf.LazyImageIcon;
import com.jtattoo.plaf.XPScrollButton;
import javax.swing.Icon;

@SuppressWarnings("serial")
public class McWillScrollButton extends XPScrollButton {

	protected static Icon upArrowIcon = null;
	protected static Icon downArrowIcon = null;
	protected static Icon leftArrowIcon = null;
	protected static Icon rightArrowIcon = null;

	public McWillScrollButton(int direction, int width) {
		super(direction, width);
	}

	public Icon getUpArrowIcon() {
		if (upArrowIcon == null) {
			upArrowIcon = new LazyImageIcon("luna/icons/UpArrow.gif");
		}
		return upArrowIcon;
	}

	public Icon getDownArrowIcon() {
		if (downArrowIcon == null) {
			downArrowIcon = new LazyImageIcon("luna/icons/DownArrow.gif");
		}
		return downArrowIcon;
	}

	public Icon getLeftArrowIcon() {
		if (leftArrowIcon == null) {
			leftArrowIcon = new LazyImageIcon("luna/icons/LeftArrow.gif");
		}
		return leftArrowIcon;
	}

	public Icon getRightArrowIcon() {
		if (rightArrowIcon == null) {
			rightArrowIcon = new LazyImageIcon("luna/icons/RightArrow.gif");
		}
		return rightArrowIcon;
	}

	protected Color[] getThumbColors() {
		ThemeColor themeColor = McWillTheme.getThemeColor();
		if (McWillTheme.isThemeColor()) {
			return new Color[] { themeColor.getLightColor(), themeColor.getSubLightColor() };
		} else {
			return new Color[] { themeColor.getMiddleColor(), themeColor.getSrcColor() };
		}
	}

	public void paint(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		Composite savedComposite = g2D.getComposite();
		Paint savedPaint = g2D.getPaint();

		boolean isPressed = getModel().isPressed();

		int width = getWidth();
		int height = getHeight();

		if (isPressed) {
			ThemeColor themeColor = McWillTheme.getThemeColor();
			Color c1 = themeColor.getSubDarkColor();
			Color c2 = themeColor.getMiddleColor();

			g2D.setPaint(new GradientPaint(0, 0, c1, width, height, c2));
			g.fillRect(0, 0, width, height);
		}

		g2D.setPaint(savedPaint);

		g2D.setComposite(savedComposite);

		// paint the icon
		if (getDirection() == NORTH) {
			int x = (width / 2) - (getUpArrowIcon().getIconWidth() / 2);
			int y = (height / 2) - (getUpArrowIcon().getIconHeight() / 2);
			getUpArrowIcon().paintIcon(this, g, x, y);
		} else if (getDirection() == SOUTH) {
			int x = (width / 2) - (getDownArrowIcon().getIconWidth() / 2);
			int y = (height / 2) - (getDownArrowIcon().getIconHeight() / 2) + 1;
			getDownArrowIcon().paintIcon(this, g, x, y);
		} else if (getDirection() == WEST) {
			int x = (width / 2) - (getLeftArrowIcon().getIconWidth() / 2);
			int y = (height / 2) - (getLeftArrowIcon().getIconHeight() / 2);
			getLeftArrowIcon().paintIcon(this, g, x, y);
		} else {
			int x = (width / 2) - (getRightArrowIcon().getIconWidth() / 2) + 1;
			int y = (height / 2) - (getRightArrowIcon().getIconHeight() / 2);
			getRightArrowIcon().paintIcon(this, g, x, y);
		}
	}

}
