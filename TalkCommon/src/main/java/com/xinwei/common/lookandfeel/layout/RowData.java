package com.xinwei.common.lookandfeel.layout;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class RowData {
	private int width = 0;
	private int height = 0;
	private List<Component> components = new ArrayList<Component>();

	public RowData() {
		super();
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(final int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(final int height) {
		this.height = height;
	}

	public List<Component> getComponents() {
		return components;
	}

	public void setComponents(final List<Component> components) {
		this.components = components;
	}

	public void addComponent(final Component component) {
		this.components.add(component);
	}
}