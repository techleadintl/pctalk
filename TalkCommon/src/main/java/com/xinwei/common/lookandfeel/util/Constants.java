package com.xinwei.common.lookandfeel.util;

public interface Constants {

	String SKIN_BUTTON = "skin";

	String TITLE_TEXT = "tabTitle text";

	String TITLE_IMAGE = "tabTitle image";

	String TITLE_UI = "tabTitle ui";

	String TABBEDPANE_TABAREAINSETS = "TabbedPane.tabAreaInsets";

	String TABBEDPANE_BOUNDS = "TabbedPane.bounds";

	String COLOR_TYPE = "color.type";
	
	String COLOR_RGB = "color.rgb";

	String OPAQUE = "opaque.value";

	String CLICK = "opaque.click";

	int COLOR_SRC = 0;
	int COLOR_LIGHT = 1;
	int COLOR_SUBLIGHT = 2;
	int COLOR_MIDDLE = 3;
	int COLOR_MIDDLE1 = 4;
	int COLOR_DARK = 5;
	int COLOR_SUBDARK = 6;
	int COLOR_DEST = 7;

	int MENU_LEFT_WIDTH = 30;

}
