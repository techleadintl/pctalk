package com.xinwei.common.lookandfeel.util;

import java.awt.Component;
import java.awt.Container;

import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

public class MenuUtil {
	public static int DEFAULT = 0;
	public static int CHECKORICON = 1;
	public static int CHECKANDICON = 2;

	public static int getMenuLeftInfo(JComponent c) {
		int _default = 0;
		Container parent = c.getParent();
		if (parent == null)
			return _default;

		if (!(parent instanceof JComponent)) {
			return _default;
		}
		Object clientProperty = ((JComponent) parent).getClientProperty("Menu.left");

		if (clientProperty != null && !Boolean.valueOf(clientProperty.toString())) {
			return _default;
		}

		Component[] components = parent.getComponents();
		int hasCheck = 0;
		int hasIcon = 0;
		for (Component component : components) {
			if (component instanceof JCheckBoxMenuItem || component instanceof JRadioButtonMenuItem) {
				hasCheck = 1;
			}
			if (component instanceof JMenuItem) {
				Icon icon = ((JMenuItem) component).getIcon();
				if (icon != null) {
					hasIcon = 1;
				}
			}
			if (hasCheck == 1 && hasIcon == 1) {
				break;
			}
		}

		return _default + hasCheck + hasIcon;
	}
}
