package com.xinwei.common.lookandfeel.component;

import java.awt.Cursor;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.plaf.UIResource;

@SuppressWarnings("serial")
public class McWillButton extends JButton implements UIResource, SwingConstants {
	boolean flag = false;

	private String str;

	public McWillButton() {
		this(null, null, null, null, null);
	}

	public McWillButton(Action action) {
		this(action, null, null, null, null);
	}

	public McWillButton(Icon icon) {
		this(null, null, icon, null, null);
	}

	public McWillButton(Icon icon, Icon overIcon, Icon pressIcon) {
		this(null, icon, overIcon, pressIcon);
	}

	public McWillButton(String accessibleName, Icon icon, Icon overIcon, Icon pressIcon) {
		this(null, accessibleName, icon, overIcon, pressIcon);
	}

	public McWillButton(Action action, String accessibleName, Icon icon, Icon overIcon, Icon pressIcon) {
		setAtt(action, accessibleName, icon, overIcon, pressIcon);
	}

	public void setAtt(Action action, String accessibleName, Icon icon, Icon overIcon, Icon pressIcon) {
		if (icon == null) {
			icon = (Icon) action.getValue(Action.SMALL_ICON);
		}
		if (icon != null) {
			setIcon(icon);
		}

		if (overIcon != null) {
			setRolloverIcon(overIcon);
		}

		if (pressIcon != null) {
			setPressedIcon(pressIcon);
		}

		if (action != null) {
			setAction(action);
		}

		setContentAreaFilled(false);
		setBorderPainted(false);
		setText(null);
		getAccessibleContext().setAccessibleName(accessibleName);
		setCursor(new Cursor(Cursor.HAND_CURSOR));
		setFocusPainted(false);
		setBorder(BorderFactory.createEmptyBorder());
	}

	@Override
	public void setAction(Action a) {
		super.setAction(a);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		if (str == null)
			return;

		FontMetrics fm = g.getFontMetrics();

		int stringWidth = fm.stringWidth(str);
		int stringAscent = fm.getAscent();

		int xCoordinate = getWidth() / 2 - stringWidth / 2;
		int yCoordinate = getHeight() / 2 + stringAscent / 2;

		g.drawString(str, xCoordinate, yCoordinate);

	}

	public String getString() {
		return str;
	}

	public void setString(String text) {
		this.str = text;
	}
}
