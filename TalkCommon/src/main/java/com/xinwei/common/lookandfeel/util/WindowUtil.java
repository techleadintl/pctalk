package com.xinwei.common.lookandfeel.util;

import java.awt.Shape;
import java.awt.Window;
import java.awt.geom.GeneralPath;

import javax.swing.JDialog;
import javax.swing.JFrame;

import com.sun.awt.AWTUtilities;

public class WindowUtil {
	public static int ARC = 9;

	public static void setWindowShape(final Window window) {
		if ((((window instanceof JFrame)) && (!((JFrame) window).isUndecorated()))
				|| (((window instanceof JDialog)) && (!((JDialog) window).isUndecorated()))) {
			return;
		}
		//		SwingUtilities.invokeLater(new Runnable() {
		//
		//			@Override
		//			public void run() {
		//				Shape shape = getShape(window.getWidth(), window.getHeight(), ARC);
		//				AWTUtilities.setWindowShape(window, shape);
		//			}
		//		});

		//		AWTUtilities.setWindowShape(window, new RoundRectangle2D.Double(0, 0, window.getWidth(), window.getHeight()+5, ARC-.5,ARC-.5));
//		AWTUtilities.setWindowShape(window, getShape(window.getWidth(), window.getHeight() + 5, 4));
	}
	
	public static void setWindowLocation(Window window,boolean flag) {
		if (!(window instanceof JFrame))
			return;
		JFrame frame = (JFrame) window;
		Object clientProperty = frame.getRootPane().getClientProperty("xxxxxx");
		if (clientProperty != null && clientProperty instanceof ScreenWindowHide) {
			ScreenWindowHide frameHideScreen = ((ScreenWindowHide) clientProperty);
			frameHideScreen.setLocation(flag);
		}
	}


	public static void updateAllWindowShapes() {
		for (Window localWindow : Window.getWindows()) {
			setWindowShape(localWindow);
		}
	}

	private static Shape getShape(int width, int height, int arc) {
		GeneralPath path = new GeneralPath();

		int x1 = arc, y1 = 0;

		int x2 = width - arc, y2 = 0;

		int x3 = width, y3 = arc;

		int x4 = width, y4 = height - arc;

		int x5 = width - arc, y5 = height;

		int x6 = arc, y6 = height;

		int x7 = 0, y7 = height - arc;

		int x8 = 0, y8 = arc;

		path.moveTo(x1, y1);
		path.lineTo(x2, y2);
		path.lineTo(x3+1, y3);
		path.lineTo(x4, y4-1);
		path.lineTo(x5, y5);
		path.lineTo(x6, y6);
		path.lineTo(x7, y7);
		path.lineTo(x8, y8);
		path.lineTo(x1, y1);
		path.closePath();
		return path;
	}
}
