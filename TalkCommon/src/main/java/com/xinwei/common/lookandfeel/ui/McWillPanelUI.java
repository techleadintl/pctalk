package com.xinwei.common.lookandfeel.ui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;

import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.BasePanelUI;
import com.xinwei.common.lookandfeel.util.Constants;

public class McWillPanelUI extends BasePanelUI {

	private static McWillPanelUI panelUI = null;

	public static ComponentUI createUI(JComponent c) {
		if (panelUI == null) {
			panelUI = new McWillPanelUI();
		}
		return panelUI;
	}

	protected void installDefaults(JPanel p) {
		super.installDefaults(p);
		p.setFont(AbstractLookAndFeel.getTheme().getControlTextFont());
		p.setOpaque(false);
	}

	public void paint(Graphics g, JComponent c) {
		Graphics2D g2D = (Graphics2D) g;
		Object savedRenderingHint = null;
		if (AbstractLookAndFeel.getTheme().isTextAntiAliasingOn()) {
			savedRenderingHint = g2D.getRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING);
			g2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, AbstractLookAndFeel.getTheme().getTextAntiAliasingHint());
		}
		super.paint(g, c);
		if (AbstractLookAndFeel.getTheme().isTextAntiAliasingOn()) {
			g2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, savedRenderingHint);
		}
	}
}
