/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月25日 下午3:01:12
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.component.McWillList;
import com.xinwei.common.lookandfeel.contentpanel.McWillWindowContentPanel;

@SuppressWarnings("serial")
public class McWillXPopupMenu extends JDialog {
	JList<JPanel> list;
	transient Frame frame;
	DefaultListModel<JPanel> listModel;

	public McWillXPopupMenu() {
		setUndecorated(true);//去除窗体
		setAlwaysOnTop(true); //设置界面悬浮
		//		setSize(70, 80);

		listModel = new DefaultListModel<JPanel>();
		list = new McWillList<JPanel>(listModel);
		list.setCellRenderer(new ListCellRenderer() {
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				JComponent component = (JComponent) value;
				component.setOpaque(true);
				ThemeColor themeColor = McWillTheme.getThemeColor();
				if (((McWillList) list).getRolloverIndex() == index) {
					component.setBackground(themeColor.getMiddleColor1());
				} else {
					component.setBackground(themeColor.getLightColor());
				}
				return component;
			}
		});
		list.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				JPanel panel = list.getSelectedValue();
				if (panel == null) {
					return;
				}

				JMenuItem menuItem = (JMenuItem) panel.getClientProperty("&&@#");
				ActionListener[] actionListeners = menuItem.getActionListeners();
				for (ActionListener l : actionListeners) {
					l.actionPerformed(new ActionEvent(menuItem, 0, null));
				}
				dispose();
			}
		});

		McWillWindowContentPanel frameContentPanel = new McWillWindowContentPanel() {
			@Override
			public void addTitlePanle(Component comp) {
			}
		};

		frameContentPanel.setTitleTextIconVisible(false);
		frameContentPanel.setLayout(new BorderLayout(0, -3));
		frameContentPanel.add(list, BorderLayout.CENTER);

		setContentPane(frameContentPanel);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowDeactivated(WindowEvent e) {
				e.getWindow().dispose();
			}
		});
	}

	public void add(JMenuItem menu) {
		JPanel panel = createMenuPanel(menu);

		listModel.addElement(panel);
	}

	public void add(int index, JMenuItem menu) {
		JPanel panel = createMenuPanel(menu);

		listModel.insertElementAt(panel, index);
	}

	protected JPanel createMenuPanel(JMenuItem menu) {
		JPanel panel = new JPanel(new BorderLayout());
		JLabel txtLabel = new JLabel(menu.getText());
		JLabel imgLabel = new JLabel(menu.getIcon());
		JLabel xLabel = new JLabel();
		txtLabel.setHorizontalAlignment(JLabel.CENTER);
		imgLabel.setPreferredSize(new Dimension(15, 25));
		xLabel.setPreferredSize(new Dimension(15, 25));
		panel.add(txtLabel, BorderLayout.CENTER);
		panel.add(imgLabel, BorderLayout.WEST);
		panel.add(xLabel, BorderLayout.EAST);
		panel.putClientProperty("&&@#", menu);
		return panel;
	}

	public void remove(JMenuItem menu) {
		JPanel[] panels = new JPanel[listModel.size()];
		listModel.copyInto(panels);
		for (JPanel jPanel : panels) {
			if (menu == jPanel.getClientProperty("&&@#"))
				return;
			listModel.removeElement(jPanel);
		}
	}

	public void clear() {
		listModel.clear();
	}

	private static Frame getFrame(Component c) {
		Component w = c;

		while (!(w instanceof Frame) && (w != null)) {
			w = w.getParent();
		}
		return (Frame) w;
	}

	public void showMouseXY() {
		setVisible(true);
		Point location = MouseInfo.getPointerInfo().getLocation();

		int X = location.x;
		int Y = location.y;
		Toolkit defaultToolkit = java.awt.Toolkit.getDefaultToolkit();
		Dimension screenSize = defaultToolkit.getScreenSize();
		int screenWidth = screenSize.width;
		int screenHeight = screenSize.height;

		int dialogWidth = getWidth();
		int dialogHeight = getHeight();
		/** 获取屏幕的边界 */
		Insets screenInsets = defaultToolkit.getScreenInsets(getGraphicsConfiguration());
		int x = X;
		int y = Y-80;
		int z = 8;
		if (screenInsets.bottom > 0) {
			y = Y - dialogHeight - z;
			if (X + dialogWidth > screenWidth) {
				x = X - dialogWidth;
			} else {
				x = X;
			}
		} else if (screenInsets.right > 0) {
			x = X - dialogWidth - z;
			if (Y + dialogHeight > screenHeight) {
				y = Y - dialogHeight;
			} else {
				y = Y;
			}

		} else if (screenInsets.left > 0) {
			x = X + z;
			if (Y + dialogHeight > screenHeight) {
				y = Y - dialogHeight;
			} else {
				y = Y;
			}
		} else if (screenInsets.top > 0) {
			y = Y + z;
			if (X + dialogWidth > screenWidth) {
				x = X - dialogWidth;
			} else {
				x = X;
			}
		}
		show(x, y);
	}

	public void show(int x, int y) {
		setVisible(true);
		setLocation(x, y);
	}

	public void show(Component invoker, int x, int y) {
		Frame newFrame = getFrame(invoker);
		if (newFrame != frame) {
			// Use the invoker's frame so that events
			// are propagated properly
			if (newFrame != null) {
				this.frame = newFrame;
				setVisible(false);
			}
		}
		Point invokerOrigin;
		if (invoker != null) {
			invokerOrigin = invoker.getLocationOnScreen();

			// To avoid integer overflow
			long lx, ly;
			lx = ((long) invokerOrigin.x) + ((long) x);
			ly = ((long) invokerOrigin.y) + ((long) y);
			if (lx > Integer.MAX_VALUE)
				lx = Integer.MAX_VALUE;
			if (lx < Integer.MIN_VALUE)
				lx = Integer.MIN_VALUE;
			if (ly > Integer.MAX_VALUE)
				ly = Integer.MAX_VALUE;
			if (ly < Integer.MIN_VALUE)
				ly = Integer.MIN_VALUE;

			setLocation((int) lx, (int) ly);
		} else {
			setLocation(x, y);
		}
		setVisible(true);
	}

	@Override
	public void setVisible(boolean b) {
		super.setVisible(b);
		Dimension preferredSize = list.getPreferredSize();
		setSize(preferredSize.width + 10 + 0, preferredSize.height);
	}

	//	popupMenu.add(openMenu);
	//
	//	popupMenu.add(minimizeMenu);
}
