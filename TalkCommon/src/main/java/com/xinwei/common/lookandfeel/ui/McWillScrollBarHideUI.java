package com.xinwei.common.lookandfeel.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.plaf.ComponentUI;

import com.jtattoo.plaf.JTattooUtilities;
import com.jtattoo.plaf.XPScrollBarUI;
import com.xinwei.common.lookandfeel.ImageColorInfo;
import com.xinwei.common.lookandfeel.McWillScrollButton;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;

public class McWillScrollBarHideUI extends XPScrollBarUI {
	boolean scrollBarMouseEntered = true;
	MouseAdapter ma = new MouseAdapter() {
		@Override
		public void mouseEntered(MouseEvent e) {
			Rectangle thumbBounds = getThumbBounds();
			if (thumbBounds == null || thumbBounds.width == 0 || thumbBounds.height == 0) {
				return;
			}
			scrollBarMouseEntered = true;
			incrButton.setVisible(true);
			decrButton.setVisible(true);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			Rectangle thumbBounds = getThumbBounds();
			if (thumbBounds == null || thumbBounds.width == 0 || thumbBounds.height == 0) {
				return;
			}
			if (scrollbar.contains(e.getPoint())) {
				return;
			}
			scrollBarMouseEntered = false;
			incrButton.setVisible(false);
			decrButton.setVisible(false);
		}
	};

	public void installUI(JComponent c) {
		super.installUI(c);
		Object buttonVisibleProperty = scrollbar.getClientProperty("isButtonVisible");
		if (buttonVisibleProperty != null && !Boolean.valueOf(String.valueOf(buttonVisibleProperty))) {
			c.remove(incrButton);
			c.remove(decrButton);
		} else {
			incrButton.setVisible(false);
			decrButton.setVisible(false);
		}
		scrollBarMouseEntered = false;
		c.addMouseListener(ma);
	};

	public static ComponentUI createUI(JComponent c) {
		return new McWillScrollBarHideUI();
	}

	protected void installDefaults() {
		super.installDefaults();

		Object scrollBarWidthProperty = scrollbar.getClientProperty("scrollBarWidth");
		if (scrollBarWidthProperty != null) {
			scrollBarWidth = Integer.parseInt(String.valueOf(scrollBarWidthProperty));
		} else {
			scrollBarWidth = 9;
		}
		//		Object buttonVisibleProperty = scrollbar.getClientProperty("ButtonVisible");
		//		if (buttonVisibleProperty != null) {
		//			scrollbar.remove(incrButton);
		//			scrollbar.remove(decrButton);
		//		}
	}
	//	protected Dimension getMaximumThumbSize() {
	//		//		return maximumThumbSize;
	//		return new Dimension(30,50);
	//	}

	protected JButton createDecreaseButton(int orientation) {
		return new McWillScrollButton(orientation, scrollBarWidth);
	}

	protected JButton createIncreaseButton(int orientation) {
		return new McWillScrollButton(orientation, scrollBarWidth);
	}

	@Override
	protected Color[] getThumbColors() {
		if (McWillTheme.isThemeColor()) {
			ThemeColor themeColor = McWillTheme.getThemeColor();
			return new Color[] { themeColor.getSubDarkColor(), themeColor.getSubDarkColor() };
		} else {
			ImageColorInfo imageColorInfo = McWillTheme.getImageColorInfo();
			return new Color[] { imageColorInfo.getThemeColor().getMiddleColor(), imageColorInfo.getAppropriateColor() };
		}
	}

	protected Color[] getTrackColors() {
		ThemeColor themeColor;
		if (McWillTheme.isThemeColor()) {
			themeColor = McWillTheme.getThemeColor();
		} else {
			themeColor = McWillTheme.getImageColorInfo().getThemeColor();
		}
		return new Color[] { themeColor.getColor(0.8f), themeColor.getColor(0.85f) };
	}

	protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
		int w = c.getWidth();
		int h = c.getHeight();
		//		g.setColor(Color.WHITE);
		//		g.fillRect( 0, 0, w, h);

		Object clientProperty = scrollbar.getClientProperty("isPaintTrack");
		if (clientProperty != null && !Boolean.valueOf(String.valueOf(clientProperty))) {
			return;
		}
		if (!scrollBarMouseEntered)
			return;
		if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
			JTattooUtilities.fillVerGradient(g, getTrackColors(), 0, 0, w, h);
		} else {
			JTattooUtilities.fillHorGradient(g, getTrackColors(), 0, 0, w, h);
		}
	}

	public void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
		if (!c.isEnabled()) {
			return;
		}
		//		if (!scrollBarMouseEntered) {
		//			return;
		//		}

		if (thumbBounds == null) {
			thumbBounds = getThumbBounds();
		}
		Graphics2D g2D = (Graphics2D) g;

		int x = thumbBounds.x;
		int y = thumbBounds.y;
		int width = thumbBounds.width;
		int height = thumbBounds.height;

		ThemeColor themeColor;
		if (McWillTheme.isThemeColor()) {
			themeColor = McWillTheme.getThemeColor();
		} else {
			themeColor = McWillTheme.getImageColorInfo().getThemeColor();
		}
		Color fillColor = themeColor.getColor(0.35f);
		Color dragFillColor = themeColor.getColor(0.3f);
		Color dragBorderColor = themeColor.getColor(0.2f);
		int i = 3;
		if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
			if (isDragging) {
				g2D.setColor(dragBorderColor);
			} else {
				g2D.setColor(fillColor);
			}
			width -= i + 1;
			g2D.drawArc(x + i - 1, y - 1, width, 10, 0, 180);
			g2D.drawRect(x + i - 1, y + 5, width, height - 10);
			g2D.drawArc(x + i - 1, height + y - 10, width, 10, 0, -180);

			if (isDragging) {
				g2D.setColor(dragFillColor);
			} else {
				g2D.setColor(fillColor);
			}
			width -= 1;
			g2D.setColor(fillColor);
			g2D.fillArc(x + i, y, width, 10, 0, 180);
			g2D.fillRect(x + i, y + 5, width, height - 10);
			g2D.fillArc(x + i, height + y - 10, width, 10, 0, -180);

		} else {
			if (isDragging) {
				g2D.setColor(dragBorderColor);
			} else {
				g2D.setColor(fillColor);
			}
			height -= i + 1;
			g.drawArc(x, y + i - 1, 10, height, 90, 180);
			g.drawRect(x + 5, y + i - 1, width - 10, height);
			g.drawArc(width + x - 10, y + i - 1, 10, height, -90, 180);

			if (isDragging) {
				g2D.setColor(dragFillColor);
			} else {
				g2D.setColor(fillColor);
			}
			height -= 1;
			g2D.setColor(fillColor);
			g.fillArc(x, y + i, 10, height, 90, 180);
			g.fillRect(x + 5, y + i, width - 10, height);
			g.fillArc(width + x - 10, y + i, 10, height, -90, 180);
		}
	}

}
