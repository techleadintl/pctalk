package com.xinwei.common.lookandfeel.component;

import javax.swing.JButton;

import com.xinwei.common.lookandfeel.McWillBorders.ButtonRoundRectColorBorder;

public class McWillRolloverButton extends JButton {
	public McWillRolloverButton() {
		this("");
	}

	public McWillRolloverButton(String text) {
		super(text);
		ButtonRoundRectColorBorder border = new ButtonRoundRectColorBorder();
		setBorder(border);
	}
}