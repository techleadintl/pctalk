package com.xinwei.common.lookandfeel.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ui.McWillScrollBarHideUI;
import com.xinwei.common.lookandfeel.util.PainterUtil;

public class McWillScrollPane extends JLayeredPane {
	private final JScrollPane scrollPane;
	private final MyControlPanel controlPanel;

	public McWillScrollPane(final JComponent view) {
		this(view, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER, 150);
	}

	public McWillScrollPane(final JComponent view, int thumb_min_size) {
		this(view);
	}

	public McWillScrollPane(final JComponent view, int vsbPolicy, int hsbPolicy, int thumb_min_size) {
		scrollPane = new JScrollPane(view, vsbPolicy, hsbPolicy);
		scrollPane.setBorder(BorderFactory.createEmptyBorder());
		add(scrollPane, JLayeredPane.DEFAULT_LAYER);

		controlPanel = new MyControlPanel(scrollPane);
		add(controlPanel, JLayeredPane.PALETTE_LAYER);

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				// listen to changes of JLayeredPane size
				scrollPane.setSize(getSize());
				scrollPane.getViewport().revalidate();
				controlPanel.setSize(getSize());
				controlPanel.revalidate();
			}
		});
	}

	public void setHorizontalScrollBarPolicy(int policy) {
		scrollPane.setHorizontalScrollBarPolicy(policy);
	}

	public void setVerticalScrollBarPolicy(int policy) {
		scrollPane.setVerticalScrollBarPolicy(policy);
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		PainterUtil.paintBackgroudColor((Graphics2D) g, McWillTheme.getThemeColor().getLightColor(), 0, 0, getWidth(), getHeight());
	}

	private class MyControlPanel extends JPanel {

		private final JScrollBar vScrollBar;
		private final JScrollBar hScrollBar;

		private MyControlPanel(JScrollPane scrollPane) {

			setLayout(new BorderLayout());
			setOpaque(false);

			vScrollBar = new JScrollBar(JScrollBar.VERTICAL);
			vScrollBar.setOpaque(false);
			vScrollBar.setUI(new McWillScrollBarHideUI());
			scrollPane.setVerticalScrollBar(vScrollBar);
			scrollPane.remove(vScrollBar);
			if (scrollPane.getVerticalScrollBarPolicy() != JScrollPane.VERTICAL_SCROLLBAR_NEVER) {
				add(vScrollBar, BorderLayout.EAST);
			}

			hScrollBar = new JScrollBar(JScrollBar.HORIZONTAL);
			hScrollBar.setOpaque(false);
			hScrollBar.setUI(new McWillScrollBarHideUI());
			scrollPane.setHorizontalScrollBar(hScrollBar);
			scrollPane.remove(hScrollBar);
			if (scrollPane.getHorizontalScrollBarPolicy() != JScrollPane.HORIZONTAL_SCROLLBAR_NEVER) {
				add(hScrollBar, BorderLayout.SOUTH);
			}
		}
	}
}