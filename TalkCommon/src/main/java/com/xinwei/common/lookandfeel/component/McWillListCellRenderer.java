/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月4日 下午5:55:08
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;

public class McWillListCellRenderer extends JPanel implements ListCellRenderer {
	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		JComponent component = (JComponent) value;
		component.setOpaque(true);
		ThemeColor themeColor = McWillTheme.getThemeColor();
		if (isSelected || cellHasFocus) {
			component.setBackground(themeColor.getMiddleColor1());
		} else {
			Color backgroundColor = ((McWillList) list).getRolloverIndex() == index ? themeColor.getLightColor() : list.getBackground();
			component.setBackground(backgroundColor);
		}
		return component;
	}
}
