/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月28日 上午10:00:38
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class McWillLineImage extends JLabel {

	public McWillLineImage(ImageIcon icon) {
		setIcon(icon);
	}

	@Override
	protected void paintComponent(Graphics g) {
		ImageIcon icon = (ImageIcon) getIcon();
		Insets insets = getInsets();
		g.drawImage(icon.getImage(), insets.left, (getHeight()-insets.top-insets.bottom - icon.getIconHeight()) / 2, getWidth() - insets.left - insets.right, icon.getIconHeight(), this);
	}
}
