package com.xinwei.common.lookandfeel.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.LayoutManager2;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.ComponentUI;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.jtattoo.plaf.BaseTitlePane;
import com.jtattoo.plaf.DecorationHelper;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillTitlePane;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.common.lookandfeel.contentpanel.McWillWindowContentPanel;
import com.xinwei.common.lookandfeel.util.Constants;
import com.xinwei.common.lookandfeel.util.WindowUtil;

public class McWillRootPaneUI extends BaseRootPaneUI {

	public static ComponentUI createUI(JComponent c) {
		return new McWillRootPaneUI();
	}

	public BaseTitlePane createTitlePane(JRootPane root) {
		Object clientProperty = root.getClientProperty(Constants.TITLE_UI);
		if (clientProperty != null && clientProperty instanceof BaseTitlePane)
			return (BaseTitlePane) clientProperty;
		else
			return new McWillTitlePane(root, this);
	}

	public LayoutManager createLayoutManager() {
		return new BaseRootLayout();
	}

	public MouseInputListener createWindowMouseInputListener(JRootPane root) {
		return new MouseInputHandler() {
			int i = 0;

			public void mouseClicked(MouseEvent ev) {
				if (ev.getSource() instanceof Window) {
					Window window = (Window) ev.getSource();
					if (!(window instanceof Frame)) {
						return;
					}
					Frame frame = (Frame) window;
					BaseTitlePane titlePane = getTitlePane();
					if (titlePane == null)
						return;
					Point convertedPoint = SwingUtilities.convertPoint(window, ev.getPoint(), titlePane);
					int state = DecorationHelper.getExtendedState(frame);
					if (titlePane != null && titlePane.contains(convertedPoint)) {
						if ((ev.getClickCount() % 2) == 0 && ((ev.getModifiers() & InputEvent.BUTTON1_MASK) != 0)) {
							if (frame.isResizable()) {
								PropertyChangeListener[] pcl = frame.getPropertyChangeListeners();
								if ((state & BaseRootPaneUI.MAXIMIZED_BOTH) != 0) {
									for (int i = 0; i < pcl.length; i++) {
										pcl[i].propertyChange(new PropertyChangeEvent(window, "windowRestore", Boolean.FALSE, Boolean.FALSE));
									}
									DecorationHelper.setExtendedState(frame, state & ~BaseRootPaneUI.MAXIMIZED_BOTH);
									for (int i = 0; i < pcl.length; i++) {
										pcl[i].propertyChange(new PropertyChangeEvent(window, "windowRestored", Boolean.FALSE, Boolean.FALSE));
									}
								} else {
									for (int i = 0; i < pcl.length; i++) {
										pcl[i].propertyChange(new PropertyChangeEvent(window, "windowMaximize", Boolean.FALSE, Boolean.FALSE));
									}
									DecorationHelper.setExtendedState(frame, state | BaseRootPaneUI.MAXIMIZED_BOTH);
									for (int i = 0; i < pcl.length; i++) {
										pcl[i].propertyChange(new PropertyChangeEvent(window, "windowMaximized", Boolean.FALSE, Boolean.FALSE));
									}
									WindowUtil.setWindowShape(frame);
								}
								return;
							}
						}
					}
				}
			}

			@Override
			public void mouseDragged(MouseEvent ev) {
				if (ev.getSource() instanceof Window) {
					Window w = (Window) ev.getSource();
					Point pt = ev.getPoint();

					if (isMovingWindow) {
						{// 通过降低重绘频率，解决移动出现幻�?
							if (i++ % 2 != 0) {
								return;
							}
							if (i > 100000) {
								i = 0;
							}
						}
						Point windowPt = w.getLocationOnScreen();

						windowPt.x += pt.x - dragOffsetX;
						windowPt.y += pt.y - dragOffsetY;
						w.setLocation(windowPt);
						WindowUtil.setWindowShape(w);
					} else if (dragCursor != 0) {
						Rectangle r = w.getBounds();
						Rectangle startBounds = new Rectangle(r);
						Dimension min = MINIMUM_SIZE;
						switch (dragCursor) {
						case Cursor.E_RESIZE_CURSOR:
							adjust(r, min, 0, 0, pt.x + (dragWidth - dragOffsetX) - r.width, 0);
							break;
						case Cursor.S_RESIZE_CURSOR:
							adjust(r, min, 0, 0, 0, pt.y + (dragHeight - dragOffsetY) - r.height);
							break;
						case Cursor.N_RESIZE_CURSOR:
							adjust(r, min, 0, pt.y - dragOffsetY, 0, -(pt.y - dragOffsetY));
							break;
						case Cursor.W_RESIZE_CURSOR:
							adjust(r, min, pt.x - dragOffsetX, 0, -(pt.x - dragOffsetX), 0);
							break;
						case Cursor.NE_RESIZE_CURSOR:
							adjust(r, min, 0, pt.y - dragOffsetY, pt.x + (dragWidth - dragOffsetX) - r.width, -(pt.y - dragOffsetY));
							break;
						case Cursor.SE_RESIZE_CURSOR:
							adjust(r, min, 0, 0, pt.x + (dragWidth - dragOffsetX) - r.width, pt.y + (dragHeight - dragOffsetY) - r.height);
							break;
						case Cursor.NW_RESIZE_CURSOR:
							adjust(r, min, pt.x - dragOffsetX, pt.y - dragOffsetY, -(pt.x - dragOffsetX), -(pt.y - dragOffsetY));
							break;
						case Cursor.SW_RESIZE_CURSOR:
							adjust(r, min, pt.x - dragOffsetX, 0, -(pt.x - dragOffsetX), pt.y + (dragHeight - dragOffsetY) - r.height);
							break;
						default:
							break;
						}
						if (!r.equals(startBounds)) {
							w.setLocation(r.x, r.y);
							w.setSize(r.width, r.height);
							w.validate();
							getRootPane().repaint();
							WindowUtil.setWindowShape(w);
						}
					}
				}
			}

			public void mouseMoved(MouseEvent ev) {
				if (ev.getSource() instanceof Window) {
					JRootPane root = getRootPane();
					if (DecorationHelper.getWindowDecorationStyle(root) == NONE) {
						return;
					}

					Window w = (Window) ev.getSource();
					Frame f = null;
					Dialog d = null;

					if (w instanceof Frame) {
						f = (Frame) w;
					} else if (w instanceof Dialog) {
						d = (Dialog) w;
					}

					// Update the cursor
					int cursor = getCursor(w, ev.getPoint());// getCursor(calculateCorner(w,
																// ev.getX(),
																// ev.getY()));
					if (cursor != 0 && ((f != null && (f.isResizable() && (DecorationHelper.getExtendedState(f) & BaseRootPaneUI.MAXIMIZED_BOTH) == 0)) || (d != null && d.isResizable()))) {
						w.setCursor(Cursor.getPredefinedCursor(cursor));
					} else {
						w.setCursor(savedCursor);
					}
				}
			}
		};
	}

	private class BaseRootLayout implements LayoutManager2 {
		private McWillContentPanel windowContentPanel;

		/**
		 * Returns the amount of space the layout would like to have.
		 * 
		 * @param the
		 *            Container for which this layout manager is being used
		 * @return a Dimension object containing the layout's preferred size
		 */
		public Dimension preferredLayoutSize(Container parent) {
			Dimension contentPaneSize, menuBarSize, titlePaneSize;
			int contentPaneWidth = 0;
			int contentPaneHeight = 0;
			int menuBarWidth = 0;
			int menuBarHeight = 0;
			int titlePaneWidth = 0;
			Insets i = parent.getInsets();
			JRootPane root = (JRootPane) parent;

			if (root.getContentPane() != null) {
				contentPaneSize = root.getContentPane().getPreferredSize();
			} else {
				contentPaneSize = root.getSize();
			}
			if (contentPaneSize != null) {
				contentPaneWidth = contentPaneSize.width;
				contentPaneHeight = contentPaneSize.height;
			}

			if (root.getJMenuBar() != null) {
				menuBarSize = root.getJMenuBar().getPreferredSize();
				if (menuBarSize != null) {
					menuBarWidth = menuBarSize.width;
					menuBarHeight = menuBarSize.height;
				}
			}

			if (DecorationHelper.getWindowDecorationStyle(root) != NONE && (root.getUI() instanceof BaseRootPaneUI)) {
				BaseTitlePane titlePane = ((BaseRootPaneUI) root.getUI()).getTitlePane();
				if (titlePane != null) {
					titlePaneSize = titlePane.getPreferredSize();
					if (titlePaneSize != null) {
						titlePaneWidth = titlePaneSize.width;
					}
				}
			}

			return new Dimension(Math.max(Math.max(contentPaneWidth, menuBarWidth), titlePaneWidth) + i.left + i.right, contentPaneHeight + menuBarHeight + titlePaneWidth + i.top + i.bottom);
		}

		/**
		 * Returns the minimum amount of space the layout needs.
		 * 
		 * @param the
		 *            Container for which this layout manager is being used
		 * @return a Dimension object containing the layout's minimum size
		 */
		public Dimension minimumLayoutSize(Container parent) {
			return MINIMUM_SIZE;
		}

		/**
		 * Returns the maximum amount of space the layout can use.
		 * 
		 * @param the
		 *            Container for which this layout manager is being used
		 * @return a Dimension object containing the layout's maximum size
		 */
		public Dimension maximumLayoutSize(Container target) {
			return MAXIMUM_SIZE;
		}

		/**
		 * Instructs the layout manager to perform the layout for the specified container.
		 * 
		 * @param the
		 *            Container for which this layout manager is being used
		 */
		public void layoutContainer(Container parent) {
			JRootPane root = (JRootPane) parent;
			//			root.setBorder(new LineBorder(Color.BLACK));
			Rectangle rootPaneBounds = root.getBounds();
			Insets rootPaneInsets = root.getInsets();
			int nextY = 0;
			int w = rootPaneBounds.width - rootPaneInsets.right - rootPaneInsets.left;
			int h = rootPaneBounds.height - rootPaneInsets.top - rootPaneInsets.bottom;

			if (root.getLayeredPane() != null) {
				root.getLayeredPane().setBounds(rootPaneInsets.left, rootPaneInsets.top, w, h);
			}
			if (root.getGlassPane() != null) {
				if (DecorationHelper.getWindowDecorationStyle(root) != NONE && (root.getUI() instanceof BaseRootPaneUI)) {
					BaseTitlePane titlePane = ((BaseRootPaneUI) root.getUI()).getTitlePane();
					int titleHeight = 0;
					if (titlePane != null) {
						titleHeight = titlePane.getSize().height;
					}
					root.getGlassPane().setBounds(rootPaneInsets.left, rootPaneInsets.top + titleHeight, w, h - titleHeight);
				} else {
					root.getGlassPane().setBounds(rootPaneInsets.left, rootPaneInsets.top, w, h);
				}
			}

			Container contentPane = root.getContentPane();
			// Note: This is laying out the children in the layeredPane,
			// technically, these are not our children.
			if (DecorationHelper.getWindowDecorationStyle(root) != NONE && (root.getUI() instanceof BaseRootPaneUI)) {
				BaseTitlePane titlePane = ((BaseRootPaneUI) root.getUI()).getTitlePane();
				if (titlePane != null) {
					Dimension tpd = titlePane.getPreferredSize();
					if (tpd != null) {
						//						if (windowContentPanel == null) {
						if (contentPane instanceof JComponent) {
							McWillTheme.setOpaque((JComponent) contentPane);
						}
						if (contentPane instanceof McWillContentPanel) {
							windowContentPanel = (McWillContentPanel) contentPane;
						} else {
							windowContentPanel = new McWillWindowContentPanel();
							windowContentPanel.add(contentPane, BorderLayout.CENTER);
							root.setContentPane(windowContentPanel);
						}
						titlePane.setOpaque(false);
						JMenuBar jMenuBar = root.getJMenuBar();
						if (jMenuBar != null) {
							JPanel panel = new JPanel(new BorderLayout());
							panel.setOpaque(false);
							jMenuBar.setOpaque(false);
							panel.add(titlePane, BorderLayout.NORTH);
							panel.add(jMenuBar, BorderLayout.CENTER);
							windowContentPanel.addTitlePanle(panel);
						} else {
							windowContentPanel.addTitlePanle(titlePane);
						}
					}
					//					}
				}
			}
			contentPane = root.getContentPane();
			if (contentPane != null) {
				contentPane.setBounds(0, nextY, w, h < nextY ? 0 : h - nextY);
				WindowUtil.setWindowShape(window);
			}
		}

		public void addLayoutComponent(String name, Component comp) {
		}

		public void removeLayoutComponent(Component comp) {
		}

		public void addLayoutComponent(Component comp, Object constraints) {
		}

		public float getLayoutAlignmentX(Container target) {
			return 0.0f;
		}

		public float getLayoutAlignmentY(Container target) {
			return 0.0f;
		}

		public void invalidateLayout(Container target) {
		}

	}

}
