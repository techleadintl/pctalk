package com.xinwei.common.lookandfeel.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;
import javax.swing.JSeparator;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.ComponentUI;

import com.jtattoo.plaf.AbstractLookAndFeel;
import com.jtattoo.plaf.BaseSeparatorUI;
import com.jtattoo.plaf.ColorHelper;
import com.xinwei.common.lookandfeel.ImageColorInfo;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.util.Constants;
import com.xinwei.common.lookandfeel.util.MenuUtil;

public class McWillSeparatorUI extends BaseSeparatorUI {

	private static final Dimension size = new Dimension(2, 1);

	public static ComponentUI createUI(JComponent c) {
		return new McWillSeparatorUI();
	}

	public void paint(Graphics g, JComponent c) {
		int MENU_LEFT = MenuUtil.getMenuLeftInfo(c);
		if (MENU_LEFT == MenuUtil.DEFAULT) {
			super.paint(g, c);
			return;
		}

		boolean horizontal = true;
		if (c instanceof JSeparator) {
			horizontal = (((JSeparator) c).getOrientation() == JSeparator.HORIZONTAL);
		}
		int WIDTH = Constants.MENU_LEFT_WIDTH;
		if (MENU_LEFT == MenuUtil.CHECKANDICON) {
			//			WIDTH = WIDTH + 20;
		}
		if (horizontal) {
			ThemeColor themeColor = McWillTheme.getThemeColor();
			Color srcColor = themeColor.getSrcColor();

			Color destColor = themeColor.getSubLightColor();
			GradientPaint paint;
			if (McWillTheme.isThemeImage()) {
				ImageColorInfo imageColorInfo = McWillTheme.getImageColorInfo();
				srcColor = imageColorInfo.getMostColor();
				destColor = themeColor.getMiddleColor();
				paint = new GradientPaint(0, 0, destColor, 30, 0, srcColor, true);
			} else {
				paint = new GradientPaint(0, 0, destColor, 200, 0, srcColor, true);
			}
			int w = c.getWidth();
			g.setColor(srcColor);
			g.fillRect(WIDTH, 0, w, size.height);
			((Graphics2D) g).setPaint(paint);
			g.fillRect(0, 0, WIDTH, size.height);
		} else {
			ColorUIResource backgroundColor = AbstractLookAndFeel.getBackgroundColor();
			int h = c.getHeight();
			g.setColor(ColorHelper.darker(backgroundColor, 30));
			g.drawLine(40, 0, 0, h);
			g.setColor(ColorHelper.brighter(backgroundColor, 50));
			g.drawLine(41, 0, 1, h);
		}
	}

	public Dimension getPreferredSize(JComponent c) {
		return size;
	}
}
