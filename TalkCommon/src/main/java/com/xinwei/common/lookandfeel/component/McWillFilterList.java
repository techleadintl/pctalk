package com.xinwei.common.lookandfeel.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

import javax.swing.DefaultListModel;

public class McWillFilterList<T> extends McWillList<T> {
	private static final long serialVersionUID = 1L;
	private List<T> items; // 原始数据

	private String text;

	private FilterModel model = new FilterModel();

	private MatchComparator<T> comparator;

	public McWillFilterList() {
		super();
		items = new ArrayList<T>();
		setModel(model);
	}

	public FilterModel getFilterModel() {
		return model;
	}

	public void setMatchComparator(MatchComparator<T> comparator) {
		this.comparator = comparator;
	}

	public List<T> getAllElements() {
		return items;
	}

	public void doFilter(String text) {
		this.text = text;
		//清空用于显示的数据容器

		for (int i = 0; i < items.size(); i++) {
			//如果满足条件被放入到过滤后的数据容器中用于显示
			T item = items.get(i);
			boolean match = comparator.isMatch(item, text);
			if (match) {
				if (!model.contains(item)) {
					Object[] srcItems = new Object[model.size()];
					model.copyInto(srcItems);
					List asList = new ArrayList(Arrays.asList(srcItems));

					asList.add(item);

					Collections.sort(asList, comparator);

					int index = asList.indexOf(item);

					if (index <= model.getSize()) {
						model.addElement2(index, item);
					}
				}
			} else {
				if (!match && model.contains(item)) {
					model.removeElement2(item);
				}
				updateUI();
			}
		}
		updateUI();
	}

	public interface MatchComparator<T> extends Comparator<T> {
		boolean isMatch(T item, String text);
	}

	public class FilterModel extends DefaultListModel<T> {
		private static final long serialVersionUID = 1L;

		public void addElement2(int index, T element) {
			super.add(index, element);
		}

		public void addElement2(T element) {
			super.addElement(element);
		}

		public void removeElement2(T element) {
			super.removeElement(element);
		}

		@Override
		public void add(int index, T element) {
			addElement(element);
		}

		@Override
		public void addElement(T element) {
			items.add(element);
			doFilter(text);
		}

		@Override
		public T remove(int index) {
			T obj = super.remove(index);
			items.remove(obj);
			return obj;
		}

		@Override
		public void removeAllElements() {
			super.removeAllElements();
		}

		@Override
		public boolean removeElement(Object obj) {
			boolean removeElement = super.removeElement(obj);
			items.remove(obj);
			return removeElement;
		}

		@Override
		public void removeElementAt(int index) {
			Object element = getElementAt(index);
			super.removeElementAt(index);
			items.remove(element);
		}

		@Override
		public void removeRange(int fromIndex, int toIndex) {
			if (fromIndex > toIndex) {
				throw new IllegalArgumentException("fromIndex must be <= toIndex");
			}
			for (int i = toIndex; i >= fromIndex; i--) {
				Object element = getElementAt(i);
				items.remove(element);
			}
			super.removeRange(fromIndex, toIndex);
		}

		@Override
		public void clear() {
			Enumeration elements = elements();
			while (elements.hasMoreElements()) {
				items.remove(elements.nextElement());
			}
			super.clear();
		}

		//通知JList的视图更新显示数据源自filterItems
		//它会产生ListDataEvent 事件给　JList　JList重新从模型中获取数据并重绘（repaint)自己．
		public void fireContentsChanged() {
			fireContentsChanged(this, 0, getSize());
		}
	}

}
