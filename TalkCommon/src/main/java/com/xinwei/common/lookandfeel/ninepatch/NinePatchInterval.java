package com.xinwei.common.lookandfeel.ninepatch;

import java.awt.Rectangle;
import java.io.Serializable;

@SuppressWarnings("serial")
public final class NinePatchInterval implements Serializable, Cloneable {
	public enum NinePatchIntervalType {
		horizontalStretch, verticalStretch, horizontalContent, verticalContent;
	}

	public static final String ID_PREFIX = "NPI";

	private transient String id;

	private boolean pixel;

	private int start;

	private int end;

	public NinePatchInterval() {
		this(0);
	}

	public NinePatchInterval(int paramInt) {
		this(paramInt, paramInt);
	}

	public NinePatchInterval(int paramInt, boolean paramBoolean) {
		this(paramInt, paramInt, paramBoolean);
	}

	public NinePatchInterval(int paramInt1, int paramInt2) {
		this(paramInt1, paramInt2, true);
	}

	public NinePatchInterval(int paramInt1, int paramInt2, boolean paramBoolean) {
		setId();
		setPixel(paramBoolean);
		setStart(paramInt1);
		setEnd(paramInt2);
	}

	public String getId() {
		return this.id;
	}

	public void setId(String paramString) {
		this.id = paramString;
	}

	public void setId() {
		this.id = String.valueOf(System.currentTimeMillis());
	}

	public boolean isPixel() {
		return this.pixel;
	}

	public void setPixel(boolean paramBoolean) {
		this.pixel = paramBoolean;
	}

	public int getStart() {
		return this.start;
	}

	public void setStart(int paramInt) {
		this.start = paramInt;
	}

	public int getEnd() {
		return this.end;
	}

	public void setEnd(int paramInt) {
		this.end = paramInt;
	}

	public boolean intersects(NinePatchInterval paramNinePatchInterval) {
		return new Rectangle(getStart(), 0, getEnd() - getStart(), 1).intersects(new Rectangle(paramNinePatchInterval.getStart(), 0,
				paramNinePatchInterval.getEnd() - paramNinePatchInterval.getStart(), 1));
	}

	public NinePatchInterval clone() {
		NinePatchInterval localNinePatchInterval = new NinePatchInterval();
		localNinePatchInterval.setId(getId());
		localNinePatchInterval.setPixel(isPixel());
		localNinePatchInterval.setStart(getStart());
		localNinePatchInterval.setEnd(getEnd());
		return localNinePatchInterval;
	}

	public boolean isSame(NinePatchInterval paramNinePatchInterval) {
		return (paramNinePatchInterval != null) && (getId().equals(paramNinePatchInterval.getId()));
	}

	public boolean equals(Object paramObject) {
		if ((paramObject != null) && ((paramObject instanceof NinePatchInterval))) {
			NinePatchInterval localNinePatchInterval = (NinePatchInterval) paramObject;
			return (isPixel() == localNinePatchInterval.isPixel()) && (getStart() == localNinePatchInterval.getStart())
					&& (getEnd() == localNinePatchInterval.getEnd());
		}
		return false;
	}
}