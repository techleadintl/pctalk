package com.xinwei.common.lookandfeel.util;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;

import com.xinwei.common.lookandfeel.McWillTheme;

public class PainterUtil {

	public static void tileBackgroudImage(Component container, Image image, Graphics g) {
		if (image != null) {
			int containerWidth = container.getWidth();
			int containerHeight = container.getHeight();
			int imgWidth = image.getWidth(null);
			int imgHeight = image.getHeight(null);

			int imgBkCol = containerWidth / imgWidth + 1;
			int imgBkRow = containerHeight / imgHeight + 1;
			for (int i = 0; i < imgBkCol; i++) {
				for (int j = 0; j < imgBkRow; j++) {
					g.drawImage(image, i * imgWidth, j * imgHeight, container);
				}
			}
		}
	}

	public static void tileBackgroudImage(Component container, Image image, Graphics2D g, float alpha) {
		if (image != null) {
			int imgWidth = image.getWidth(null);
			int imgHeight = image.getHeight(null);

			Rectangle bounds = container.getBounds();
			int containerWidth = bounds.width;
			int containerHeight = bounds.height;

			int imgBkCol = containerWidth / imgWidth + 1;
			int imgBkRow = containerHeight / imgHeight + 1;
			Composite composite = g.getComposite();
			Composite alpha1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
			g.setComposite(alpha1);
			for (int i = 0; i <= imgBkCol; i++) {
				for (int j = 0; j <= imgBkRow; j++) {
					g.drawImage(image, bounds.x + i * imgWidth, bounds.y + j * imgHeight, container);
				}
			}
			g.setComposite(composite);
		}
	}

	public static void paintBackgroudImage(Component container, Image image, Graphics2D g, float alpha) {
		if (image != null) {
			Rectangle bounds = container.getBounds();
			Composite composite = g.getComposite();
			Composite alpha1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
			g.setComposite(alpha1);
			g.drawImage(image, bounds.x, bounds.y, container);
			g.setComposite(composite);
		}
	}

	public static void tileChatWindowBackgroudImage(Component container, Image image, Graphics2D g, Rectangle bounds, int height, int top) {
		if (image != null) {
			int imgWidth = image.getWidth(null);
			int imgHeight = image.getHeight(null);

			int WIDTH = 0;
			//			if (imgWidth > 550) {
			//				WIDTH = imgWidth / 4;
			//			}
			int HEIGHT = 20;
			if (imgHeight > 200) {
				HEIGHT = imgHeight / 4;
			}

			int containerWidth = container.getWidth() + WIDTH;
			int containerHeight = container.getHeight() + HEIGHT;
			height += HEIGHT;

			int imgBkCol = containerWidth / imgWidth + 1;
			int imgBkRow = containerHeight / imgHeight + 1;
			BufferedImage bufferedImage = ImageUtil.getSubimage(image, 0, 0, imgWidth, height);
			BufferedImage bufferedImage1 = ImageUtil.getSubimage(image, 0, height, imgWidth, imgHeight - height);
			for (int i = 0; i < imgBkCol; i++) {
				for (int j = 0; j < imgBkRow; j++) {
					if (j == 0) {
						g.drawImage(bufferedImage, i * imgWidth - WIDTH, 0 - HEIGHT, container);
						Paint paint = g.getPaint();
						Color color1 = ColorUtil.getAlphaColor(McWillTheme.getThemeColor().getSubLightColor(), 32);
						GradientPaint gp = new GradientPaint(0, 0, color1, 0, height, color1);
						g.setPaint(gp);
						g.fillRect(i * imgWidth - WIDTH, 0 - HEIGHT, imgWidth, height);
						g.setPaint(paint);
						g.drawImage(bufferedImage1, i * imgWidth - WIDTH, height - HEIGHT, container);

					} else {
						g.drawImage(image, i * imgWidth - WIDTH, j * imgHeight - HEIGHT, container);
					}
				}
			}
			bufferedImage = null;
			bufferedImage1 = null;

			for (int i = 0; i < imgBkCol; i++) {
				int x1 = (i + 1) * imgWidth - WIDTH;
				int x0 = x1 - imgWidth;
				if (x1 >= bounds.x + bounds.width + 8 - WIDTH) {
					if (bounds.x - 4 - WIDTH >= x0) {//在图片内
						BufferedImage subimage = ImageUtil.getSubimage(image, bounds.x - 4 - x0, top + HEIGHT, bounds.width + 12, height - top - HEIGHT);
						g.drawImage(subimage, bounds.x - 4, top, container);
						subimage = null;
					} else {//横跨2张图片
						BufferedImage rightSubimage = ImageUtil.getSubimage(image, 0, top + HEIGHT, bounds.x + bounds.width + 8 - x0, height - top - HEIGHT);
						g.drawImage(rightSubimage, x0, top, container);
						BufferedImage leftSubimage = ImageUtil.getSubimage(image, bounds.x - 4 - (x0 - imgWidth), top + HEIGHT, x0 - (bounds.x - 4), height - top - HEIGHT);
						g.drawImage(leftSubimage, bounds.x - 4, top, container);
						rightSubimage = null;
						leftSubimage = null;
					}
					break;
				}
			}

		}
	}

	public static void tileBackgroudImage(Component container, Image image, Color bkgColor, Graphics2D g, float alpha) {
		if (image != null) {
			int containerWidth = container.getWidth();
			int containerHeight = container.getHeight();
			int imgWidth = image.getWidth(null);
			int imgHeight = image.getHeight(null);

			Color color = g.getColor();
			g.setColor(bkgColor);
			if (imgWidth < containerWidth) {
				g.fillRect(imgWidth, 0, containerWidth - imgWidth, containerHeight);
			}

			if (imgHeight < containerHeight) {
				g.fillRect(0, imgHeight, containerWidth, containerHeight - imgHeight);
			}

			g.setColor(color);
			Composite composite = g.getComposite();
			Composite alpha1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
			g.setComposite(alpha1);
			g.drawImage(image, 0, 0, container);

			g.setComposite(composite);
		}
	}

	//	public static void tileBackgroudImage(Component container, Color color, Graphics2D g) {
	//		int containerWidth = container.getWidth();
	//		int containerHeight = container.getHeight();
	//		g.fillRect(0, 0, containerWidth, containerHeight);
	//	}

	//	public static void tileBackgroudImage(Component container, Image image, Graphics2D g, float alpha) {
	//		if (image != null) {
	//			int containerWidth = container.getWidth();
	//			int containerHeight = container.getHeight();
	//			int imgWidth = image.getWidth(null);
	//			int imgHeight = image.getHeight(null);
	//
	//			int imgBkCol = containerWidth / imgWidth + 1;
	//			int imgBkRow = containerHeight / imgHeight + 1;
	//			Composite composite = g.getComposite();
	//			Composite alpha1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
	//			g.setComposite(alpha1);
	//			BufferedImage bufferedImage = ImageUtil.toBufferedImage(image);
	//			bufferedImage = getScaledInstance(bufferedImage, containerWidth, containerHeight);
	//			g.drawImage(bufferedImage, 0, 0, container);
	//			g.setComposite(composite);
	//		}
	//	}

	static BufferedImage getScaledInstance(BufferedImage img, int targetWidth, int targetHeight) {
		int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		BufferedImage ret = (BufferedImage) img;
		int w, h;
		w = img.getWidth();
		h = img.getHeight();

		do {
			if (w > targetWidth) {
				w /= 2;
				if (w < targetWidth) {
					w = targetWidth;
				}
			}

			if (h > targetHeight) {
				h /= 2;
				if (h < targetHeight) {
					h = targetHeight;
				}
			}

			BufferedImage tmp = new BufferedImage(w, h, type);
			Graphics2D g2 = tmp.createGraphics();
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
			g2.drawImage(ret, 0, 0, w, h, null);
			g2.dispose();

			ret = tmp;
		} while (w != targetWidth || h != targetHeight);

		return ret;
	}

	public static void paintRoundBorder(Graphics g, Color color, int x, int y, int width, int height) {
		paintRoundBorder(g, color, x, y, width, height, 7, 7);
	}

	public static void paintRoundBorder(Graphics g, Color color, int x, int y, int width, int height, int arcWidth, int arcHeight) {
		paintRoundBorder(g, color, x, y, width, height, arcWidth, arcHeight, 1);
	}

	public static void paintRoundBorder(Graphics g, Color color, int x, int y, int width, int height, int arcWidth, int arcHeight, int thickness) {
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Color oldColor = g.getColor();
		Graphics2D g2 = (Graphics2D) g;
		int i;
		g2.setRenderingHints(rh);
		g2.setColor(color);
		for (i = 0; i < thickness; i++) {
			g2.drawRoundRect(x + i, y + i, width - i - i - 1, height - i - i - 1, arcWidth, arcHeight); //就是这一句
		}
		g2.setColor(oldColor);
	}

	public static void paintBackgroudColor(Graphics g, Color color, int width, int height) {
		paintBackgroudColor(g, color, width, height, 1);
	}

	public static void paintBackgroudColor(Graphics graphics, Color color, int width, int height, float alpha) {
		Graphics2D g = (Graphics2D) graphics;
		Composite composite = g.getComposite();
		Composite alpha1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
		g.setComposite(alpha1);
		g.setPaint(new GradientPaint(0, 0, color, 0, 0, color));
		g.fillRect(0, 0, width, height);
		g.setComposite(composite);
	}

	public static void paintBackgroudColor(Graphics g, Color color, int x, int y, int width, int height) {
		paintBackgroudColor(g, color, x, y, width, height, 1);
	}

	public static void paintBackgroudColor(Graphics graphics, Color color, int x, int y, int width, int height, float alpha) {
		Graphics2D g = (Graphics2D) graphics;

		Composite composite = g.getComposite();
		Composite alpha1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
		g.setComposite(alpha1);
		Paint paint = g.getPaint();
		g.setPaint(new GradientPaint(0, 0, color, 0, 0, color));
		g.fillRect(x, y, width, height);
		g.setComposite(composite);
		g.setPaint(paint);
	}

	public static void paintRoundBackgroudColor(Graphics g, Color color, int x, int y, int width, int height) {
		paintRoundBackgroudColor(g, color, x, y, width, height, 7, 7, 1);
	}

	public static void paintRoundBackgroudColor(Graphics g, Color color, int x, int y, int width, int height, int arcWidth, int arcHeight) {
		paintRoundBackgroudColor(g, color, x, y, width, height, arcWidth, arcHeight, 1);
	}

	public static void paintRoundBackgroudColor(Graphics graphics, Color color, int x, int y, int width, int height, int arcWidth, int arcHeight, float alpha) {
		Graphics2D g = (Graphics2D) graphics;
		Composite composite = g.getComposite();
		Composite alpha1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
		g.setComposite(alpha1);
		Paint paint = g.getPaint();
		g.setPaint(new GradientPaint(0, 0, color, 0, 0, color));
		g.fillRoundRect(x, y, width, height, arcWidth, arcHeight);
		g.setComposite(composite);
		g.setPaint(paint);
	}

	public static void paintBackgroudColor(Graphics2D g, Color color1, Color color2, int x, int y, int width, int height) {
		Paint paint = g.getPaint();
		g.setPaint(new GradientPaint(0, 0, color1, 0, height, color2));
		g.fillRect(x, y, width, height);
		g.setPaint(paint);
	}

	public static void paintBackgroudColor1(Graphics2D g, Color color1, Color color2, int x, int y, int width, int height, float alph) {
		Composite composite = g.getComposite();
		Composite alpha1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alph);
		g.setComposite(alpha1);
		g.setPaint(new GradientPaint(0, 0, color1, 0, height, color2));
		g.fillRect(x, y, width, height);
		g.setComposite(composite);
	}

	public static void fillArrowLine(Graphics2D g2, int x1, int y1, int x2, int y2) {
		double phi = Math.toRadians(20);
		int barb = 20;

		double theta = Math.atan2(y2 - y1, x2 - x1);

		g2.draw(new Line2D.Double(x1, y1, x2, y2));

		double rho = theta + phi;

		int x[] = new int[3];
		int y[] = new int[3];
		x[0] = x2;
		y[0] = y2;

		for (int j = 1; j < 3; j++) {
			x[j] = (int) (x2 - barb * Math.cos(rho));
			y[j] = (int) (y2 - barb * Math.sin(rho));
			rho = theta - phi;
		}
		g2.fillPolygon(x, y, 3);

	}

	public static void drawArrowLine(Graphics2D g2, int x1, int y1, int x2, int y2) {
		double phi = Math.toRadians(20);
		int barb = 20;

		double theta = Math.atan2(y2 - y1, x2 - x1);
		double x, y, rho = theta + phi;


		for (int j = 0; j < 2; j++) {
			x = x2 - barb * Math.cos(rho);
			y = y2 - barb * Math.sin(rho);
			g2.draw(new Line2D.Double(x2, y2, x, y));
			rho = theta - phi;
		}
		g2.draw(new Line2D.Double(x1, y1, x2, y2));
	}
}
