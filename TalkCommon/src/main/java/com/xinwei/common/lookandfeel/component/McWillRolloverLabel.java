package com.xinwei.common.lookandfeel.component;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.border.Border;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.McWillBorders.RoundRectColorBorder;

public class McWillRolloverLabel extends JLabel {
	public McWillRolloverLabel() {
		decorate(false, true);
	}

	public McWillRolloverLabel(String text) {
		this(text, false);
	}

	public McWillRolloverLabel(String text, boolean isRound) {
		super(text);
		decorate(false, isRound);
	}

	public McWillRolloverLabel(Icon icon) {
		this(icon, false, true);
	}

	public McWillRolloverLabel(Icon icon, boolean isRound) {
		this(icon, false, isRound);
	}

	public McWillRolloverLabel(Icon icon, boolean colorEmpty, boolean isRound) {
		super(icon);
		decorate(colorEmpty, isRound);
	}

	protected void decorate(boolean colorEmpty, boolean isRound) {
		setOpaque(true);
		addMouseListener(new McWillBorderListener(this, colorEmpty, isRound));
	}

	public class McWillBorderListener extends MouseAdapter {
		private JComponent component;

		public McWillBorderListener(JComponent component, boolean colorEmpty, boolean isRound) {
			this.component = component;
			RoundRectColorBorder border;
			if (colorEmpty)
				border = new RoundRectColorBorder();
			else
				border = new RoundRectColorBorder(McWillTheme.getThemeColor().getSrcColor());

			border.setRoundedCorners(isRound);
			this.component.setBorder(border);
		}

		public void mouseEntered(MouseEvent e) {
			setBorderColor(getSelectColor());
		}

		public void mouseExited(MouseEvent e) {
			setBorderColor(getDefaultColor());
		}

		@Override
		public void mousePressed(MouseEvent e) {
			setBorderColor(getSelectColor());
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			setBorderColor(getDefaultColor());
		}

		private void setBorderColor(Color color) {
			Border border = component.getBorder();
			if (border instanceof RoundRectColorBorder) {
				((RoundRectColorBorder) border).setColor(color);
				component.updateUI();
			}
		}

		protected Color getDefaultColor() {
			ThemeColor themeColor = McWillTheme.getThemeColor();
			if (McWillTheme.isThemeColor())
				return themeColor.getMiddleColor();
			else
				return themeColor.getSrcColor();
		}

		protected Color getSelectColor() {
			return SELECT_COLOR;
		}

		private Color SELECT_COLOR = McWillTheme.DEFAULT_COLOR;
	}


}