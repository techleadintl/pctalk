package com.xinwei.common.lookandfeel.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.plaf.ComponentUI;

import com.jtattoo.plaf.XPScrollBarUI;
import com.xinwei.common.lookandfeel.ImageColorInfo;
import com.xinwei.common.lookandfeel.McWillScrollButton;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;

public class McWillScrollBarUI4 extends XPScrollBarUI {

	private JScrollBar scrollBar;
	private float alpha = 0.0f;
	private FadeAnimation fadeAnimation;

	private McWillScrollBarUI4(JScrollBar scrollBar) {
		this.scrollBar = scrollBar;

		fadeAnimation = new FadeAnimation(this);
		scrollBar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				setVisible(true, true);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				setVisible(false, false);
			}
		});

		scrollBar.addAdjustmentListener(new AdjustmentListener() {
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				setVisible(true, false);
			}
		});
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
		Rectangle thumbBounds = getThumbBounds();
		if (thumbBounds != null) {
			scrollBar.repaint(thumbBounds);
		}
	}

	public void setVisible(boolean visible, boolean mouseOver) {
		if (visible) {
			fadeAnimation.fadeIn(mouseOver);
		} else {
			fadeAnimation.fadeOut();
		}
		Rectangle thumbBounds = getThumbBounds();
		if (thumbBounds != null) {
			scrollBar.repaint(thumbBounds);
		}
	}

	private Color getAlphaColor(Color color) {
		if (alpha == 1.0f) {
			return color;
		}
		int rgb = color.getRGB() & 0xFFFFFF; // color without alpha values
		rgb |= ((int) (alpha * 255)) << 24; // add alpha object
		return new Color(rgb, true);
	}

	public static ComponentUI createUI(JComponent c) {
		return new McWillScrollBarUI4((JScrollBar) c);
	}

	protected void installDefaults() {
		super.installDefaults();
		scrollBarWidth = 12;
		// ensure the minimum size of the thumb
		int w = minimumThumbSize.width;
		int h = minimumThumbSize.height;
		if (scrollBar.getOrientation() == JScrollBar.VERTICAL) {
			h = Math.max(h, Math.min(maximumThumbSize.height, 48));
		} else {
			w = Math.max(w, Math.min(maximumThumbSize.width, 48));
		}
		minimumThumbSize = new Dimension(w, h);
	}

	//	protected Dimension getMaximumThumbSize() {
	//		//		return maximumThumbSize;
	//		return new Dimension(30,50);
	//	}

	protected JButton createDecreaseButton(int orientation) {
		return new McWillScrollButton(orientation, 0);
	}

	protected JButton createIncreaseButton(int orientation) {
		return new McWillScrollButton(orientation, 0);
	}

	@Override
	protected Color[] getThumbColors() {
		if (McWillTheme.isThemeColor()) {
			ThemeColor themeColor = McWillTheme.getThemeColor();
			return new Color[] { getAlphaColor(themeColor.getSubDarkColor()), getAlphaColor(themeColor.getSubDarkColor()) };
		} else {
			ImageColorInfo imageColorInfo = McWillTheme.getImageColorInfo();
			return new Color[] { getAlphaColor(imageColorInfo.getThemeColor().getMiddleColor()), getAlphaColor(imageColorInfo.getAppropriateColor()) };
		}
	}

	protected Color[] getTrackColors() {
		ThemeColor themeColor;
		if (McWillTheme.isThemeColor()) {
			themeColor = McWillTheme.getThemeColor();
		} else {
			themeColor = McWillTheme.getImageColorInfo().getThemeColor();
		}
		return new Color[] { getAlphaColor(themeColor.getColor(0.8f)), getAlphaColor(themeColor.getColor(0.85f)) };
	}

	protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
	}

	protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
		if (alpha == 0.0f) {
			return; // don't paint anything
		}

		if (!c.isEnabled()) {
			return;
		}

		Graphics2D g2D = (Graphics2D) g;

		int x = thumbBounds.x;
		int y = thumbBounds.y;
		int width = thumbBounds.width;
		int height = thumbBounds.height;

		ThemeColor themeColor;
		if (McWillTheme.isThemeColor()) {
			themeColor = McWillTheme.getThemeColor();
		} else {
			themeColor = McWillTheme.getImageColorInfo().getThemeColor();
		}
		Color fillColor = getAlphaColor(themeColor.getColor(0.35f));
		Color dragFillColor = getAlphaColor(themeColor.getColor(0.3f));
		Color dragBorderColor = getAlphaColor(themeColor.getColor(0.2f));
		int i = 3;
		if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
			if (isDragging) {
				g2D.setColor(dragBorderColor);
			} else {
				g2D.setColor(fillColor);
			}
			width -= i + 1;
			g2D.drawArc(x + i - 1, y - 1, width, 10, 0, 180);
			g2D.drawRect(x + i - 1, y + 5, width, height - 10);
			g2D.drawArc(x + i - 1, height + y - 10, width, 10, 0, -180);

			if (isDragging) {
				g2D.setColor(dragFillColor);
			} else {
				g2D.setColor(fillColor);
			}
			width -= 1;
			g2D.setColor(fillColor);
			g2D.fillArc(x + i, y, width, 10, 0, 180);
			g2D.fillRect(x + i, y + 5, width, height - 10);
			g2D.fillArc(x + i, height + y - 10, width, 10, 0, -180);

		} else {
			if (isDragging) {
				g2D.setColor(dragBorderColor);
			} else {
				g2D.setColor(fillColor);
			}
			height -= i + 1;
			g.drawArc(x, y + i - 1, 10, height, 90, 180);
			g.drawRect(x + 5, y + i - 1, width - 10, height);
			g.drawArc(width + x - 10, y + i - 1, 10, height, -90, 180);

			if (isDragging) {
				g2D.setColor(dragFillColor);
			} else {
				g2D.setColor(fillColor);
			}
			height -= 1;
			g2D.setColor(fillColor);
			g.fillArc(x, y + i, 10, height, 90, 180);
			g.fillRect(x + 5, y + i, width - 10, height);
			g.fillArc(width + x - 10, y + i, 10, height, -90, 180);
		}

	}

	private class FadeAnimation {
		private final McWillScrollBarUI4 scrollUI;
		private boolean isFadeIn;

		private FadeAnimation(McWillScrollBarUI4 scrollUI) {
			this.scrollUI = scrollUI;
		}

		public synchronized void fadeIn(boolean mouseOver) {

			if (!isFadeIn) {
				isFadeIn = true;
				scrollUI.setAlpha(1);
				if (!mouseOver) {
					fadeOut();
				}
			}
		}

		public synchronized void fadeOut() {
			if (isFadeIn) {
				isFadeIn = false;
				scrollUI.setAlpha(0);
			}
		}

	}

}
