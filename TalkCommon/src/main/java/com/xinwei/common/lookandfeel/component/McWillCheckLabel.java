package com.xinwei.common.lookandfeel.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class McWillCheckLabel extends JLabel {
	private boolean selected;
	private ImageIcon uncheckImage;
	private ImageIcon checkImage;
	private List<ActionListener> actionListeners = new ArrayList<ActionListener>();
	private boolean enable = true;

	public McWillCheckLabel() {
		this(null, null, false);
	}

	public McWillCheckLabel(ImageIcon uncheckImage, ImageIcon checkImage) {
		this(uncheckImage, checkImage, false);
	}

	public McWillCheckLabel(ImageIcon uncheckImage, ImageIcon checkImage, boolean check) {
		this.uncheckImage = uncheckImage;
		this.checkImage = checkImage;
		setSelected(check);
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!enable) {
					return;
				}
				McWillCheckLabel.this.setSelected(!McWillCheckLabel.this.selected);
				for (ActionListener actionListener : actionListeners) {
					actionListener.actionPerformed(new ActionEvent(McWillCheckLabel.this, 0, getActionCommand()));
				}
			}
		});
	}

	public void setActionCommand(String action) {
		putClientProperty("$$ActionCommand", action);
	}

	public String getActionCommand() {
		return (String) getClientProperty("$$ActionCommand");
	}

	public void addActionListener(ActionListener l) {
		actionListeners.add(l);
	}

	public boolean isSelected() {
		return this.selected;
	}

	public void setSelected(boolean selection) {
		this.selected = selection;
		setImage(selection);
	}

	private void setImage(boolean check) {
		if (check)
			setIcon(this.checkImage);
		else
			setIcon(this.uncheckImage);
	}

	public void setUncheckImage(ImageIcon uncheckImage) {
		this.uncheckImage = uncheckImage;
	}

	public void setCheckImage(ImageIcon checkImage) {
		this.checkImage = checkImage;
	}

	public boolean getEnabled() {
		return enable;
	}

	public void setEnabled(boolean enable) {
		this.enable = enable;
	}
}