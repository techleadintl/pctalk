package com.xinwei.common.lookandfeel.listener;

import java.awt.Component;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import com.xinwei.common.lang.SystemUtil;
import com.xinwei.spark.TaskEngine;

public class ComponentMouseMotionListener extends MouseMotionAdapter {
	private boolean isIn = false;

	private Component component;

	private boolean stop = false;

	public ComponentMouseMotionListener(Component component) {
		this.component = component;
		final Rectangle rec = new Rectangle();

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				while (!stop) {
					if (isIn) {
						doIn(rec);
					}
					SystemUtil.sleep(250);
				}
			}
		});
	}

	private void doIn(final Rectangle rec) {
		Point locationOnScreen = component.getLocationOnScreen();
		Rectangle bounds = component.getBounds();
		rec.x = locationOnScreen.x;
		rec.y = locationOnScreen.y;
		rec.width = bounds.width;
		rec.height = bounds.height;

		Point location = MouseInfo.getPointerInfo().getLocation();
		if (!rec.contains(location)) {
			isIn = false;
			mouseExited();
		}
	}

	public void mouseMoved(MouseEvent e) {
		int x = e.getXOnScreen();
		int y = e.getYOnScreen();
		if (!isIn) {
			isIn = true;
			mouseEntered(x, y);
		}
	}

	public void mouseEntered(int x, int y) {

	}

	public void mouseExited() {
	}
}