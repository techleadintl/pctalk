package com.xinwei.common.lookandfeel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.Window;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import com.xinwei.common.lookandfeel.util.ColorUtil;
import com.xinwei.common.lookandfeel.util.Constants;
import com.xinwei.common.lookandfeel.util.ImageUtil;

public class McWillTheme {
	//	public static Color DEFAULT_COLOR = new Color(0x8a8a8a);
	public static Color DEFAULT_COLOR = new Color(73, 165, 255);
	//	public static Color DEFAULT_COLOR=new Color(243, 148, 0);
	public static Color EMPTY_COLOR = ColorUtil.getAlphaColor(Color.WHITE, 0);
	private static ThemeColor THEME_COLOR = new ThemeColor(DEFAULT_COLOR);
	private static ThemeImage THEME_IMAGE;
	private static boolean COLOR_THEME_FLAG = true;

	private static ImageColorInfo imageColorInfo;

	private static PropertyChangeListener listener;

	public static ThemeColor getThemeColor() {
		return THEME_COLOR;
	}

	public static ThemeImage getThemeImage() {
		return THEME_IMAGE;
	}

	public static void setThemeColor(Color color) {
		THEME_COLOR = new ThemeColor(color, Color.WHITE);
		THEME_IMAGE = null;
		updateTheme(true);
	}

	//	public static void setThemeImage(ImageIcon imageIcon) {
	//		THEME_IMAGE = new ThemeImage(imageIcon);
	//		updateTheme(false);
	//	}
	public static void setThemeImage(byte[] imageIcon) {
		THEME_IMAGE = new ThemeImage(imageIcon);
		updateTheme(false);
	}

//	public static void setThemeImage(Image image) {
//		THEME_IMAGE = new ThemeImage(new ImageIcon(image));
//		updateTheme(false);
//	}

	public static void setThemeColor(ThemeColor themeColor) {
		THEME_COLOR = themeColor;
		THEME_IMAGE = null;
		updateTheme(true);
	}

	public static void setThemeImage(ThemeImage themeImage) {
		THEME_IMAGE = themeImage;
		updateTheme(false);
	}

	public static void setListener(PropertyChangeListener listener) {
		McWillTheme.listener = listener;
	}

	public static boolean isThemeColor() {
		return COLOR_THEME_FLAG;
	}

	public static boolean isThemeImage() {
		return !COLOR_THEME_FLAG;
	}

	/** 窗口字体颜色 **/
	public static Color getWindowTitlFontColor() {
		if (windowTitlFontColor != null)
			return windowTitlFontColor;

		return Color.BLACK;
	}

	public static ImageColorInfo getImageColorInfo() {
		return imageColorInfo;
	}

	public static void setOpaque(Component p) {
		Component[] components = null;
		if (p instanceof JComponent) {
			JComponent childComponent = (JComponent) p;
			Object clientProperty = childComponent.getClientProperty(Constants.OPAQUE);
			if (clientProperty == null) {
				childComponent.setOpaque(false);
				components = childComponent.getComponents();
			} else {
				childComponent.setOpaque(false);
				components = null;
			}
		} else if (p instanceof Container) {
			components = ((Container) p).getComponents();
		}
		if (components == null)
			return;

		for (Component c : components) {
			setOpaque(c);
		}
	}

	public static void setOpaque1(Component p) {
		Component[] components = null;
		if (p instanceof JComponent) {
			JComponent childComponent = (JComponent) p;
			Object clientProperty = childComponent.getClientProperty(Constants.OPAQUE);
			if (clientProperty == null) {
				childComponent.setOpaque(true);
				components = childComponent.getComponents();
			} else {
				childComponent.setOpaque(true);
				components = null;
			}
		} else if (p instanceof Container) {
			components = ((Container) p).getComponents();
		}
		if (components == null)
			return;

		for (Component c : components) {
			setOpaque1(c);
		}
	}

	private static void updateTheme(boolean colorTheme) {
		COLOR_THEME_FLAG = colorTheme;

		Object changeValue;

		if (isThemeColor()) {
			THEME_IMAGE = null;

			imageColorInfo = null;

			windowTitlFontColor = ColorUtil.getFontColor(THEME_COLOR.getSrcColor());

			changeValue = THEME_COLOR;

		} else {
			Image image = THEME_IMAGE.getImage();

			imageColorInfo = ImageUtil.getImageColorInfo(image);

			THEME_COLOR = new ThemeColor(imageColorInfo.getAppropriateColor(), Color.WHITE);

			windowTitlFontColor = ColorUtil.getFontColor(ImageUtil.getImageColor(image, 3, 3));

			changeValue = THEME_IMAGE;
		}

		if (listener != null) {
			listener.propertyChange(new PropertyChangeEvent(listener, "mcwilltheme", null, changeValue));
		}
		props.clear();
		ThemeColor themeColor = getThemeColor();
		if (isThemeImage()) {
			props.put("menuBackgroundColor", getColorString(themeColor.getSrcColor()));
			Color invertColor = ColorUtil.getSaturatedColor(themeColor.getSrcColor(), 1.0);
			props.put("menuSelectionBackgroundColor", getColorString(invertColor));
			Color mostColor = imageColorInfo.getMostColor();
			if (mostColor.getRed() < 95 && mostColor.getBlue() < 95 && mostColor.getGreen() < 95)
				props.put("frameColor", DEFAULT_COLOR);
			else
				props.put("frameColor", getColorString(mostColor));
			props.put("controlColorLight", getColorString(themeColor.getMiddleColor()));
			props.put("controlColorDark", getColorString(themeColor.getSrcColor()));
		} else {
			props.put("menuBackgroundColor", getColorString(themeColor.getSubLightColor()));
			props.put("menuSelectionBackgroundColor", getColorString(themeColor.getSrcColor()));
			props.put("frameColor", getColorString(themeColor.getSrcColor()));
			props.put("controlColorLight", getColorString(themeColor.getLightColor()));
			props.put("controlColorDark", getColorString(themeColor.getMiddleColor()));
		}
		McWillLookAndFeel.setCurrentTheme(props);

		for (Window window : Window.getWindows()) {
			SwingUtilities.updateComponentTreeUI(window);
		}
	}

	private static String getColorString(Color color) {
		return color.getRed() + " " + color.getGreen() + " " + color.getBlue();
	}

	private static Color windowTitlFontColor;
	private static Properties props = new Properties();
}
