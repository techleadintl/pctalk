package com.xinwei.common.lookandfeel.util;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.color.ColorSpace;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.ConvolveOp;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageObserver;
import java.awt.image.Kernel;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

import com.mortennobel.imagescaling.AdvancedResizeOp;
import com.mortennobel.imagescaling.DimensionConstrain;
import com.mortennobel.imagescaling.ResampleOp;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lookandfeel.ImageColorInfo;
import com.xinwei.common.lookandfeel.McWillFilter;
import com.xinwei.common.scalr.AnimatedGifEncoder;
import com.xinwei.common.scalr.GifDecoder;
import com.xinwei.talk.common.LAF;

import sun.awt.image.ToolkitImage;

public class ImageUtil {
	static Color color = new Color(235, 234, 219);

	public static int[][] getImageGRB(BufferedImage bfImage) {
		int width = bfImage.getWidth();
		int height = bfImage.getHeight();
		int[][] result = new int[height][width];
		for (int h = 0; h < height; h++) {
			for (int w = 0; w < width; w++) {
				//使用getRGB(w, h)获取该点的颜色值是ARGB，而在实际应用中使用的是RGB，所以需要将ARGB转化成RGB，即bufImg.getRGB(w, h) & 0xFFFFFF。
				result[h][w] = bfImage.getRGB(w, h) & 0xFFFFFF;
			}
		}
		return result;
	}

	public static boolean isSameImage(ImageIcon imageIcon1, ImageIcon imageIcon2) {
		if (imageIcon1.getIconWidth() != imageIcon2.getIconWidth() || imageIcon1.getIconHeight() != imageIcon2.getIconHeight()) {
			return false;
		}

		BufferedImage bufferedImage1 = getBufferedImage(imageIcon1.getImage());
		BufferedImage bufferedImage2 = getBufferedImage(imageIcon2.getImage());
		int[][] imageGRB1 = getImageGRB(bufferedImage1);
		int[][] imageGRB2 = getImageGRB(bufferedImage2);
		int h = bufferedImage1.getHeight();
		int w = bufferedImage1.getWidth();
		for (int i = 0; i < h; h++) {
			for (int j = 0; j < w; w++) {
				//使用getRGB(w, h)获取该点的颜色值是ARGB，而在实际应用中使用的是RGB，所以需要将ARGB转化成RGB，即bufImg.getRGB(w, h) & 0xFFFFFF。
				if (imageGRB1[i][j] != imageGRB2[i][j]) {
					return false;
				}
			}
		}
		return true;
	}

	static char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static String getImageMD5(ImageIcon imageIcon) {
		try {
			BufferedImage bufferedImage = getBufferedImage(imageIcon.getImage());
			byte[] gif = getGif(bufferedImage);
			// 获得MD5摘要算法的 MessageDigest 对象
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			// 使用指定的字节更新摘要
			mdInst.update(gif);
			// 获得密文
			byte[] md = mdInst.digest();
			// 把密文转换成十六进制的字符串形式
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ImageIcon resizeImageIcon2(ImageIcon icon, int limitW, int limitH) {
		int iconWidth = icon.getIconWidth();
		int iconHeight = icon.getIconHeight();

		if (iconWidth <= limitW && iconHeight <= limitH) {
			return icon;
		}
		double i = (double) iconWidth / iconHeight;
		double l = (double) limitW / limitH;

		if (i < l) {
			iconWidth = Double.valueOf(limitH * i).intValue();
			iconHeight = limitH;
		} else {
			iconHeight = Double.valueOf(limitW / i).intValue();
			iconWidth = limitW;
		}

		return new ImageIcon(icon.getImage().getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH));
	}

	public static Image resizeImage(Image image, int w, int h) {
		return image.getScaledInstance(w, h, Image.SCALE_SMOOTH);
	}

	public static ImageIcon resizeImageIcon(ImageIcon image, int w, int h) {
		return new ImageIcon(image.getImage().getScaledInstance(w, h, Image.SCALE_SMOOTH));
	}

	public static BufferedImage getClipboard() {
		Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
		try {
			if (t != null && t.isDataFlavorSupported(DataFlavor.imageFlavor)) {
				return (BufferedImage) t.getTransferData(DataFlavor.imageFlavor);
			}
		} catch (UnsupportedFlavorException e) {
			// Nothing to do
		} catch (IOException e) {
			// Nothing to do
		}
		return null;
	}

	public static BufferedImage resizeHigh(BufferedImage bi, int newWidth, int newHeight) {
		try {
			DimensionConstrain dc = DimensionConstrain.createMaxDimensionNoOrientation(newWidth, newHeight);
			ResampleOp resampleOp = new ResampleOp(newWidth,newHeight);
			resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Normal);
			return resampleOp.filter(bi, null);
		} catch (Exception e) {

		}
		return null;
	}

	public static byte[] resize(BufferedImage bi, int newWidth, int newHeight, String format) {
		try {
			if (isGif(format)) {
				return resizeGif(bi, newWidth, newHeight);
			} else {
				return resizeJpg(bi, newWidth, newHeight);
			}
		} catch (Exception e) {

		}
		return null;
	}

	public static byte[] resize(byte[] bytes, int newWidth, int newHeight) {
		if (isGif(bytes)) {
			return resizeGif(bytes, newWidth, newHeight);
		} else {
			return resizeJpg(new ImageIcon(bytes).getImage(), newWidth, newHeight);
		}

	}

	public static byte[] resizeJpg(Image image, int newWidth, int newHeight) {
		Image resizedImage = image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);

		// This code ensures that all the pixels in the image are loaded.  
		Image temp = new ImageIcon(resizedImage).getImage();

		// Create the buffered image.  
		BufferedImage bufferedImage = new BufferedImage(temp.getWidth(null), temp.getHeight(null), BufferedImage.TYPE_INT_RGB);

		// Copy image to buffered image.  
		Graphics g = bufferedImage.createGraphics();

		// Clear background and paint the image.  
		g.setColor(Color.white);
		g.fillRect(0, 0, temp.getWidth(null), temp.getHeight(null));
		g.drawImage(temp, 0, 0, null);
		g.dispose();

		// Soften.  
		float softenFactor = 0.05f;
		float[] softenArray = { 0, softenFactor, 0, softenFactor, 1 - (softenFactor * 4), softenFactor, 0, softenFactor, 0 };
		Kernel kernel = new Kernel(3, 3, softenArray);
		ConvolveOp cOp = new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP, null);
		bufferedImage = cOp.filter(bufferedImage, null);

		// Write the jpeg to a file.  
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		// Encodes image as a JPEG data stream  
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);

		JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(bufferedImage);

		param.setQuality(1.0f, true);

		encoder.setJPEGEncodeParam(param);
		try {
			encoder.encode(bufferedImage);
			return out.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return toBytes(bufferedImage, "jpg");
	}

	public static byte[] resizeGif(BufferedImage bi, int newWidth, int newHeight) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImageIO.write(bi, "gif", os);
		return resizeGif(os.toByteArray(), newWidth, newHeight);
	}

	public static byte[] resizeGif(byte[] bytes, int newWidth, int newHeight) {
		InputStream is = new ByteArrayInputStream(bytes);

		GifDecoder decoder = new GifDecoder();
		int status = decoder.read(is);
		if (status != GifDecoder.STATUS_OK) {
			return null;
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		AnimatedGifEncoder encoder = new AnimatedGifEncoder();
		encoder.start(baos);
		encoder.setRepeat(decoder.getLoopCount());
		for (int i = 0; i < decoder.getFrameCount(); i++) {
			encoder.setDelay(decoder.getDelay(i));
			BufferedImage image = javaResize(decoder.getFrame(i), newWidth, newHeight);
			encoder.addFrame(image);
		}
		encoder.finish();

		return baos.toByteArray();
	}

	public static BufferedImage toBufferedImage(byte[] bytes) {
		if (bytes == null)
			return null;
		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		try {
			return ImageIO.read(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] toBytes(BufferedImage bi, String format) {
		if (bi == null)
			return null;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ImageIO.write(bi, format, os);
			return os.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 压缩图片
	 * 
	 * @param sourceImage
	 *            待压缩图片
	 * @param width
	 *            压缩图片高度
	 * @param heigt
	 *            压缩图片宽度
	 */
	private static BufferedImage javaResize(BufferedImage sourceImage, int width, int height) {
		BufferedImage zoomImage = new BufferedImage(width, height, sourceImage.getType());
		Image image = sourceImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		Graphics gc = zoomImage.getGraphics();
		gc.setColor(Color.WHITE);
		gc.drawImage(image, 0, 0, null);
		return zoomImage;
	}
	//	public static Image getScaledImage(byte[] bytes, int w, int h) {
	//		return getScaledImage(new ImageIcon(bytes).getImage(), w, h, getImageFormat(bytes));
	//	}
	//
	//	public static ImageIcon getScaledImageIcon(byte[] bytes, int w, int h) {
	//		return getScaledImageIcon(new ImageIcon(bytes), w, h, getImageFormat(bytes));
	//	}

	public static String getImageFormat(byte[] bytes) {
		if (isGif(bytes)) {
			return "gif";
		}
		if (isJpg(bytes)) {
			return "jpg";
		}
		if (isPng(bytes)) {
			return "png";
		}
		if (isTiff(bytes)) {
			return "tiff";
		}
		if (isBmp(bytes)) {
			return "bmp";
		}
		if (isIcon(bytes)) {
			return "icon";
		}
		return null;

	}

	public static String getImageFormat(File file) {
		ImageInputStream iis = null;
		try {
			// create an image input stream from the specified file
			iis = ImageIO.createImageInputStream(file);
			// get all currently registered readers that recognize the image format
			Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);

			if (!iter.hasNext()) {
				throw new RuntimeException("No readers found!");
			}

			// get the first reader
			ImageReader reader = iter.next();

			return reader.getFormatName().toLowerCase();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close stream
			IOUtil.closeQuietly(iis);
		}
		return null;
	}

	// get image format in a file

	/** '47 49 46 38 39'(GIF89a,或GIF87a)开始,'00 3B'作为文件结束标志 **/
	public static boolean isGif(byte[] bytes) {
		return bytes.length > 8 && bytes[0] == (byte) 0x47 && bytes[1] == (byte) 0x49 && bytes[2] == (byte) 0x46 && bytes[3] == (byte) 0x38 && (bytes[4] == (byte) 0x39 || bytes[4] == (byte) 0x37) && bytes[5] == (byte) 0x61;
	}

	public static boolean isGif(String format) {
		return "gif".equalsIgnoreCase(format);
	}

	/** 'FFD8'作为文件开始，'FFD9'作为文件结束标志 **/
	public static boolean isJpg(byte[] bytes) {
		return bytes.length > 4 && bytes[0] == (byte) 0xFF && bytes[1] == (byte) 0xD8 && bytes[bytes.length - 2] == (byte) 0xFF && bytes[bytes.length - 1] == (byte) 0xD9;
	}

	public static boolean isJpg(String format) {
		return "jpg".equalsIgnoreCase(format) || "jpeg".equalsIgnoreCase(format);
	}

	/** '89 50 4E 47 0D 0A 1A 0A'作为文件开始，'49 45 4E 44 AE 42 60 82'作为文件结束标志 **/
	public static boolean isPng(byte[] bytes) {
		return bytes.length > 16 && bytes[0] == (byte) 0x89 && bytes[1] == (byte) 0x50 && bytes[2] == (byte) 0x4E && bytes[3] == (byte) 0x47 && bytes[4] == (byte) 0x0D && bytes[5] == (byte) 0x0A && bytes[6] == (byte) 0x1A && bytes[7] == (byte) 0x0A
				&& bytes[bytes.length - 8] == (byte) 0x49 && bytes[bytes.length - 7] == (byte) 0x45 && bytes[bytes.length - 6] == (byte) 0x4E && bytes[bytes.length - 5] == (byte) 0x44 && bytes[bytes.length - 4] == (byte) 0xAE
				&& bytes[bytes.length - 3] == (byte) 0x42 && bytes[bytes.length - 2] == (byte) 0x60 && bytes[bytes.length - 1] == (byte) 0x82;
	}

	public static boolean isPng(String format) {
		return "png".equalsIgnoreCase(format);
	}

	/** '42,4D' BM作为文件开始， **/
	public static boolean isBmp(byte[] bytes) {
		return bytes.length > 2 && bytes[0] == (byte) 0x42 && bytes[1] == (byte) 0x4D;
	}

	public static boolean isBmp(String format) {
		return "bmp".equalsIgnoreCase(format);
	}

	/** '49 49 2A 00'或'4D 4D 00 2A'作为文件开始， **/
	public static boolean isTiff(byte[] bytes) {
		return bytes.length > 4
				&& ((bytes[0] == (byte) 0x49 && bytes[1] == (byte) 0x49 && bytes[2] == (byte) 0x2A && bytes[3] == (byte) 0x00) || (bytes[0] == (byte) 0x4D && bytes[1] == (byte) 0x4D && bytes[2] == (byte) 0x00 && bytes[3] == (byte) 0x2A));
	}

	/** '00 00 01 00'或'00 00 02 00'作为文件开始， **/
	public static boolean isIcon(byte[] bytes) {
		return bytes.length > 4
				&& ((bytes[0] == (byte) 0x00 && bytes[1] == (byte) 0x00 && bytes[2] == (byte) 0x01 && bytes[3] == (byte) 0x00) || (bytes[0] == (byte) 0x00 && bytes[1] == (byte) 0x00 && bytes[2] == (byte) 0x02 && bytes[3] == (byte) 0x00));
	}

	public static byte[] getGif(BufferedImage image) throws IOException {
		return getImageBytes(image, "gif");
	}

	public static byte[] getPng(BufferedImage image) {
		return getImageBytes(image, "png");
	}

	public static byte[] getJpg(BufferedImage image) {
		return getImageBytes(image, "jpg");
	}

	public static byte[] getJpeg(BufferedImage image) {
		return getImageBytes(image, "jpeg");
	}

	public static byte[] getBmp(BufferedImage image) {
		return getImageBytes(image, "bmp");
	}

	public static byte[] getImageBytes(BufferedImage image, String format) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, format, out);
			return out.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Combines two images into one
	 * 
	 * @param image1
	 *            left image
	 * @param image2
	 *            right image
	 * @return combined Image
	 */
	public static Image combine(Image image1, Image image2) {
		return combine(new ImageIcon(image1), new ImageIcon(image2));
	}

	/**
	 * Combines two images into one
	 * 
	 * @param image1
	 *            left image
	 * @param image2
	 *            right image
	 * @return combined Image
	 */
	public static Image combine(ImageIcon image1, ImageIcon image2) {

		ImageObserver comp = new JComponent() {
			private static final long serialVersionUID = 1L;
		};

		int w = image1.getIconWidth() + image2.getIconWidth();
		int h = Math.max(image1.getIconHeight(), image2.getIconHeight());

		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2 = image.createGraphics();

		g2.drawImage(image1.getImage(), 0, 0, comp);
		g2.drawImage(image2.getImage(), image1.getIconWidth(), 0, comp);
		g2.dispose();

		return image;
	}

	public static Image returnTransparentImage(int w, int h) {

		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

		return image;

	}

	/**
	 * Creates an Image from the specified Icon
	 * 
	 * @param icon
	 *            that should be converted to an image
	 * @return the new image
	 */
	public static Image iconToImage(Icon icon) {
		if (icon instanceof ImageIcon) {
			return ((ImageIcon) icon).getImage();
		} else {
			int w = icon.getIconWidth();
			int h = icon.getIconHeight();
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice gd = ge.getDefaultScreenDevice();
			GraphicsConfiguration gc = gd.getDefaultConfiguration();
			BufferedImage image = gc.createCompatibleImage(w, h);
			Graphics2D g = image.createGraphics();
			icon.paintIcon(null, g, 0, 0);
			g.dispose();
			return image;
		}
	}

	public static byte[] compress(byte[] imageData, String imageType, int width, int height, float quality, boolean percentage) {
		ByteArrayOutputStream baos = null;
		if (imageData != null && width > 0 && height > 0) {
			Image srcImg = null;
			try {
				/* 读取图片信息 */
				srcImg = ImageIO.read(new ByteArrayInputStream(imageData));
				int type = BufferedImage.TYPE_INT_RGB;

				if (imageType == null)
					return null;

				if ("png".equalsIgnoreCase(imageType))
					type = BufferedImage.TYPE_4BYTE_ABGR;
				int new_w = width;
				int new_h = height;
				if (percentage) {
					// 为等比缩放计算输出的图片宽度及高度
					double rate1 = ((double) srcImg.getWidth(null)) / (double) width + 0.1;
					double rate2 = ((double) srcImg.getHeight(null)) / (double) height + 0.1;
					double rate = rate1 > rate2 ? rate1 : rate2;
					new_w = (int) (((double) srcImg.getWidth(null)) / rate);
					new_h = (int) (((double) srcImg.getHeight(null)) / rate);
				}
				BufferedImage tempImage = new BufferedImage(new_w, new_h, type);
				tempImage.getGraphics().drawImage(srcImg, 0, 0, new_w, new_h, null);
				/* 压缩后的文件 */
				baos = new ByteArrayOutputStream(imageData.length);
				ImageIO.write(tempImage, imageType, baos);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (srcImg != null) {
					srcImg.flush();
				}
			}
		}
		if (baos != null) {
			return baos.toByteArray();
		} else {
			return null;
		}
	}

	public final static String getImageType(Object input) {
		try {
			ImageInputStream iis = ImageIO.createImageInputStream(input);
			Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);
			if (!iter.hasNext()) {
				return null;
			}
			ImageReader reader = iter.next();
			iis.close();
			return reader.getFormatName();
		} catch (Exception e) {
			return null;
		}
	}

	public static final boolean isImage(File file) {
		boolean flag = false;
		try {
			BufferedImage bufreader = ImageIO.read(file);
			int width = bufreader.getWidth();
			int height = bufreader.getHeight();
			if (width == 0 || height == 0) {
				flag = false;
			} else {
				flag = true;
			}
		} catch (IOException e) {
			flag = false;
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	public static BufferedImage getMergeImage(int W, int H, byte[]... images) {
		return getMergeImage(W, H, Arrays.asList(images));
	}

	public static BufferedImage getMergeImage(int W, int H, List<byte[]> images) {
		return getMergeImage(W, H, Color.WHITE, images);
	}

	public static BufferedImage getMergeImage(int W, int H, Color color, List<byte[]> images) {
		int GAP = 2;

		if (W < 20 || H < 20) {
			GAP = 0;
		}

		BufferedImage combined = new BufferedImage(W, H, BufferedImage.TYPE_INT_RGB);

		// paint both images, preserving the alpha channels  
		Graphics g = combined.getGraphics();

		g.setColor(color);

		g.fillRect(0, 0, W, H);

		drawMergeImage(g, 0, 0, W, H, GAP, images);

		return combined;
	}

	public static void drawMergeImage(Graphics g, int x, int y, int darwW, int darwH, int GAP, List<byte[]> images) {
		int num = 0;
		if (images != null) {
			num = images.size();
		}

		darwW = darwH = Math.min(darwW, darwH);
		if (num > 8) {
			int h = (darwH - 2 * GAP) / 3;
			int w = (darwW - 2 * GAP) / 3;

			g.drawImage(LAF.getImage(images.get(0), w, h), x, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(1), w, h), x + w + GAP, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(2), w, h), x + 2 * w + 2 * GAP, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(3), w, h), x, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(4), w, h), x + w + GAP, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(5), w, h), x + 2 * w + 2 * GAP, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(6), w, h), x, y + 2 * h + 2 * GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(7), w, h), x + w + GAP, y + 2 * h + 2 * GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(8), w, h), x + 2 * w + 2 * GAP, y + 2 * h + 2 * GAP, w, h, null);
		} else if (num == 8) {
			int h = (darwH - 2 * GAP) / 3;
			int w = (darwW - 2 * GAP) / 3;
			g.fillRect(x + w + GAP, y + h + GAP, w, h);

			g.drawImage(LAF.getImage(images.get(0), w, h), x, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(1), w, h), x + w + GAP, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(2), w, h), x + 2 * w + 2 * GAP, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(3), w, h), x, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(4), w, h), x + 2 * w + 2 * GAP, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(5), w, h), x, y + 2 * h + 2 * GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(6), w, h), x + w + GAP, y + 2 * h + 2 * GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(7), w, h), x + 2 * w + 2 * GAP, y + 2 * h + 2 * GAP, w, h, null);
		} else if (num == 5) {
			int h = (darwH - 2 * GAP) / 3;
			int w = (darwW - 2 * GAP) / 3;
			g.fillRect(x + w + GAP, y, w, h);
			g.fillRect(x, y + h + GAP, w, h);
			g.fillRect(x + 2 * w + 2 * GAP, y + h + GAP, w, h);
			g.fillRect(x + w + GAP, y + 2 * h + 2 * GAP, w, h);

			g.drawImage(LAF.getImage(images.get(0), w, h), x, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(1), w, h), x + 2 * w + 2 * GAP, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(2), w, h), x + w + GAP, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(3), w, h), x, y + 2 * h + 2 * GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(4), w, h), x + 2 * w + 2 * GAP, y + 2 * h + 2 * GAP, w, h, null);
		} else if (num == 6) {
			int h = (darwH - 2 * GAP) / 3;
			int w = (darwW - 2 * GAP) / 3;
			g.fillRect(x, y, w, h);
			g.fillRect(x + 2 * w + 2 * GAP, y, w, h);
			g.fillRect(x + w + GAP, y + 2 * h + 2 * GAP, w, h);

			g.drawImage(LAF.getImage(images.get(0), w, h), x + w + GAP, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(1), w, h), x, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(2), w, h), x + w + GAP, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(3), w, h), x + 2 * w + 2 * GAP, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(4), w, h), x, y + 2 * h + 2 * GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(5), w, h), x + 2 * w + 2 * GAP, y + 2 * h + 2 * GAP, w, h, null);
		} else if (num == 7) {
			int h = (darwH - 2 * GAP) / 3;
			int w = (darwW - 2 * GAP) / 3;

			g.fillRect(x, y, w, h);
			g.fillRect(x + w + GAP, y, w, h);
			g.fillRect(x + 2 * w + 2 * GAP, y, w, h);

			g.drawImage(LAF.getImage(images.get(0), w, h), (darwW - w) / 2, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(1), w, h), x, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(2), w, h), x + w + GAP, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(3), w, h), x + 2 * w + 2 * GAP, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(4), w, h), x, y + 2 * h + 2 * GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(5), w, h), x + w + GAP, y + 2 * h + 2 * GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(6), w, h), x + 2 * w + 2 * GAP, y + 2 * h + 2 * GAP, w, h, null);
		} else if (num == 4) {
			int h = (darwH - GAP) / 2;
			int w = (darwW - GAP) / 2;

			g.drawImage(LAF.getImage(images.get(0), w, h), x, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(1), w, h), x + w + GAP, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(2), w, h), x, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(3), w, h), x + w + GAP, y + h + GAP, w, h, null);
		} else if (num == 3) {
			int h = (darwH - GAP) / 2;
			int w = (darwW - GAP) / 2;

			g.fillRect(x, y, w, h);
			g.fillRect(x + w + GAP, y, w, h);

			g.drawImage(LAF.getImage(images.get(0), w, h), (darwW - w) / 2, y, w, h, null);

			g.drawImage(LAF.getImage(images.get(1), w, h), x, y + h + GAP, w, h, null);

			g.drawImage(LAF.getImage(images.get(2), w, h), x + w + GAP, y + h + GAP, w, h, null);
		} else if (num == 2) {
			int h = (darwH - GAP) / 2;
			int w = (darwW - GAP) / 2;
			g.fillRect(x + w + GAP, y, w, h);
			g.fillRect(x, y + h + GAP, w, h);

			g.drawImage(LAF.getImage(images.get(0), w, h), x, y, w, h, null);
			g.drawImage(LAF.getImage(images.get(1), w, h), x + w + GAP, y + h + GAP, w, h, null);

		} else if (num == 1) {
			int h = darwH;
			int w = darwW;
			g.drawImage(LAF.getImage(images.get(0), w, h), x, y, w, h, null);
		} else {
			int h = darwH;
			int w = darwW;
			g.fillRect(x, y, w, h);
		}
	}

	/** 获取角度图片 **/
	public static BufferedImage getRoundedCornerImg(Image image, int cornerRadius) {
		return getRoundedCornerImg(getBufferedImage(image), cornerRadius);
	}

	public static BufferedImage getRoundedCornerImg(BufferedImage image, int cornerRadius) {
		int w = image.getWidth();
		int h = image.getHeight();
		BufferedImage output = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = output.createGraphics();

		g2.setComposite(AlphaComposite.Src);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(Color.WHITE);
		g2.fill(new RoundRectangle2D.Float(0, 0, w, h, cornerRadius, cornerRadius));

		g2.setComposite(AlphaComposite.SrcAtop);
		g2.drawImage(image, 0, 0, null);

		g2.dispose();

		return output;
	}

	public static BufferedImage getSubimage(Image image, int x, int y, int width, int height) {
		try {
			BufferedImage bufferedImage = getBufferedImage(image);
			bufferedImage = bufferedImage.getSubimage(x, y, width, height);
			return bufferedImage;
		} catch (Exception e) {
			return null;
		}
	}

	public static BufferedImage getSubimage(ImageIcon imageIcon, int x, int y, int width, int height) {
		return getSubimage(imageIcon.getImage(), x, y, width, height);
	}

	public static Image getFilterImage(Image image) {
		try {
			BufferedImage bufferedImage = getBufferedImage(image);
			if (bufferedImage == null) {
				return image;
			}
			ImageFilter imgf = new McWillFilter(255);
			FilteredImageSource fis = new FilteredImageSource(bufferedImage.getSource(), imgf);
			Image im = Toolkit.getDefaultToolkit().createImage(fis);
			return im;
		} catch (Exception ex) {
		}
		return null;
	}

	public static BufferedImage getGrayImage(BufferedImage originalPic) {
		return getColorImage(originalPic, ColorSpace.CS_GRAY);
	}

	public static BufferedImage getColorImage(BufferedImage originalPic, int colorSpace) {
		int imageWidth = originalPic.getWidth();
		int imageHeight = originalPic.getHeight();

		BufferedImage newPic = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_3BYTE_BGR);

		ColorConvertOp cco = new ColorConvertOp(ColorSpace.getInstance(colorSpace), null);
		cco.filter(originalPic, newPic);
		return newPic;
	}

	public static ImageIcon getGrayImageIcon(ImageIcon originalImageIcon) {
		return getColorImageIcon(originalImageIcon, ColorSpace.CS_GRAY);
	}

	public static ImageIcon getColorImageIcon(ImageIcon originalImageIcon, int colorSpace) {
		BufferedImage originalPic = getBufferedImage(originalImageIcon);
		int imageWidth = originalPic.getWidth();
		int imageHeight = originalPic.getHeight();

		BufferedImage newPic = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_3BYTE_BGR);

		ColorConvertOp cco = new ColorConvertOp(ColorSpace.getInstance(colorSpace), null);
		cco.filter(originalPic, newPic);

		return new ImageIcon(newPic);
	}

	public static Image getGrayImage(Component component, Image colorImage) {
		if (component == null || colorImage == null)
			return null;
		int w = colorImage.getWidth(component); //取得图片宽；  
		int h = colorImage.getHeight(component);//取得图片高；  

		if (w == -1 || h == -1) {
			return colorImage;
		}
		int[] pixels = new int[w * h];

		PixelGrabber pg = new PixelGrabber(colorImage, 0, 0, w, h, pixels, 0, w);//定义一块内存空间；  
		try {
			pg.grabPixels();
		} catch (InterruptedException ex) {
			return null;
		}

		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				int color = pixels[w * i + j];// 获得像素的颜色  
				int alpha = (color & 0xFF000000) >> 24;
				int red = (int) (((color & 0x00FF0000) >> 16) * 0.3);
				int green = (int) (((color & 0x0000FF00) >> 8) * 0.59);
				int blue = (int) ((color & 0x000000FF) * 0.11);
				color = red + green + blue;
				color = (alpha << 24) | (color << 16) | (color << 8) | color;
				pixels[w * i + j] = color;//由红，绿，蓝值得到灰度值； 
			}
		}
		Image grayImage = component.createImage(w, h);
		if (grayImage == null)
			return null;
		Image pic = component.createImage(new MemoryImageSource(w, h, pixels, 0, w));
		grayImage.getGraphics().drawImage(pic, 0, 0, null); //显示黑白图片；  
		return grayImage;
	}

	public static Image getGrayImage1(Component component, Image colorImage) {
		if (component == null || colorImage == null)
			return null;
		int w = colorImage.getWidth(component); //取得图片宽；  
		int h = colorImage.getHeight(component);//取得图片高；  

		if (w == -1 || h == -1) {
			return colorImage;
		}
		int[] pixels = new int[w * h];

		PixelGrabber pg = new PixelGrabber(colorImage, 0, 0, w, h, pixels, 0, w);//定义一块内存空间；  
		try {
			pg.grabPixels();
		} catch (InterruptedException ex) {
			return null;
		}

		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				int color = pixels[w * i + j];// 获得像素的颜色  
				color = ColorUtil.darker(new Color(color), 30).getRGB();
				pixels[w * i + j] = color;//由红，绿，蓝值得到灰度值； 
			}
		}
		Image grayImage = component.createImage(w, h);
		if (grayImage == null)
			return null;
		Image pic = component.createImage(new MemoryImageSource(w, h, pixels, 0, w));
		grayImage.getGraphics().drawImage(pic, 0, 0, null); //显示黑白图片；  
		return grayImage;
	}

	public static ImageColorInfo getImageColorInfo(Image image) {

		BufferedImage bufferedImage = getBufferedImage(image);
		if (bufferedImage == null) {
			return new ImageColorInfo(color);
		}
		ImageColorInfo imageInfo = new ImageColorInfo();
		int[] rgb = new int[3];
		int width = bufferedImage.getWidth();
		int height = bufferedImage.getHeight();
		int minx = bufferedImage.getMinX();
		int miny = bufferedImage.getMinY();

		ColorInfo colorInfo$0 = new ColorInfo();
		ColorInfo colorInfo$32 = new ColorInfo();
		ColorInfo colorInfo$64 = new ColorInfo();
		ColorInfo colorInfo$128 = new ColorInfo();
		ColorInfo colorInfo$192 = new ColorInfo();
		ColorInfo colorInfo$210 = new ColorInfo();
		ColorInfo colorInfo$220 = new ColorInfo();
		ColorInfo colorInfo$230 = new ColorInfo();
		ColorInfo colorInfo$240 = new ColorInfo();

		for (int i = minx; i < width; i++) {
			for (int j = miny; j < height; j++) {
				int pixel = bufferedImage.getRGB(i, j);
				rgb[0] = (pixel & 0xff0000) >> 16;
				rgb[1] = (pixel & 0xff00) >> 8;
				rgb[2] = (pixel & 0xff);

				double $value = rgb[0] * 0.299 + rgb[1] * 0.587 + rgb[2] * 0.114;
				Color c = new Color(rgb[0], rgb[1], rgb[2]);
				if ($value > 240) {
					if (colorInfo$240.color == null || colorInfo$240.value > $value) {
						colorInfo$240.setInfo(c, $value);
					}
					colorInfo$240.add();
				} else if ($value > 230) {
					if (colorInfo$230.color == null || colorInfo$230.value > $value) {
						colorInfo$230.setInfo(c, $value);
					}
					colorInfo$230.add();
				} else if ($value > 220) {
					if (colorInfo$220.color == null || colorInfo$220.value > $value) {
						colorInfo$220.setInfo(c, $value);
					}
					colorInfo$220.add();
				} else if ($value > 210) {
					if (colorInfo$210.color == null || colorInfo$210.value > $value) {
						colorInfo$210.setInfo(c, $value);
					}
					colorInfo$210.add();
				} else if ($value > 192) {
					if (colorInfo$192.color == null || colorInfo$192.value > $value) {
						colorInfo$192.setInfo(c, $value);
					}
					colorInfo$192.add();
				} else if ($value > 128) {
					if (colorInfo$128.color == null || colorInfo$128.value > $value) {
						colorInfo$128.setInfo(c, $value);
					}
					colorInfo$128.add();
				} else if ($value > 64) {
					if (colorInfo$64.color == null || colorInfo$64.value < $value) {
						colorInfo$64.setInfo(c, $value);
					}
					colorInfo$64.add();
				} else if ($value > 32) {
					if (colorInfo$32.color == null || colorInfo$32.value < $value) {
						colorInfo$32.setInfo(c, $value);
					}
					colorInfo$32.add();
				} else {
					if (colorInfo$0.color == null || colorInfo$0.value < $value) {
						colorInfo$0.setInfo(c, $value);
					}
					colorInfo$0.add();
				}
			}
		}
		ColorInfo[] values = new ColorInfo[] { colorInfo$0, colorInfo$32, colorInfo$64, colorInfo$128, colorInfo$192, colorInfo$210, colorInfo$220, colorInfo$230, colorInfo$240 };

		java.util.Arrays.sort(values);

		int colorIndex = 0;
		for (int i = 0; i < values.length; i++) {
			if (values[i].total != 0) {
				colorIndex = i;
				break;
			}
		}

		ColorInfo lightcolorInfo = null;
		ColorInfo darkcolorInfo = null;
		Color appropriateColor = null;
		Color bkgColor = null;
		for (int i = values.length - 1; i >= colorIndex; i--) {
			ColorInfo colorInfo = values[i];

			if (lightcolorInfo == null) {
				lightcolorInfo = colorInfo;
			}

			if (darkcolorInfo == null) {
				darkcolorInfo = colorInfo;
			}

			if (colorInfo.value < darkcolorInfo.value) {
				darkcolorInfo = colorInfo;
			}
			if (colorInfo.value > lightcolorInfo.value) {
				lightcolorInfo = colorInfo;
			}
		}

		if (colorInfo$210.color != null) {
			appropriateColor = colorInfo$210.color;
		} else if (colorInfo$220.color != null) {
			appropriateColor = colorInfo$220.color;
		} else if (colorInfo$230.color != null) {
			appropriateColor = colorInfo$230.color;
		} else if (colorInfo$192.color != null) {
			appropriateColor = colorInfo$192.color;
		} else if (colorInfo$128.color != null) {
			appropriateColor = colorInfo$128.color;
		} else if (colorInfo$64.color != null) {
			appropriateColor = ColorUtil.getAlphaColor(colorInfo$64.color, 128);
		} else {
			appropriateColor = color;
		}

		//bkgColor
		if (colorInfo$192.color != null) {
			bkgColor = colorInfo$192.color;
		} else if (colorInfo$210.color != null) {
			bkgColor = colorInfo$210.color;
		} else if (colorInfo$128.color != null) {
			bkgColor = colorInfo$128.color;
		} else if (colorInfo$220.color != null) {
			bkgColor = colorInfo$220.color;
		} else if (colorInfo$230.color != null) {
			bkgColor = colorInfo$230.color;
		} else if (colorInfo$64.color != null) {
			bkgColor = ColorUtil.getAlphaColor(colorInfo$64.color, 128);
		} else {
			bkgColor = color;
		}

		imageInfo.setMostColor(values[values.length - 1].color);
		imageInfo.setLessColor(values[colorIndex].color);
		imageInfo.setDarkColor(darkcolorInfo.color);
		imageInfo.setLightColor(lightcolorInfo.color);
		imageInfo.setAppropriateColor(appropriateColor);
		imageInfo.setBkgColor(bkgColor);

		return imageInfo;

	}

	//	public static Color getImageLightColor(Image image) {
	//		BufferedImage bufferedImage = toBufferedImage(image);
	//		if (bufferedImage == null)
	//			return color;
	//		int[] rgb = new int[3];
	//		int width = bufferedImage.getWidth();
	//		int height = bufferedImage.getHeight();
	//		int minx = bufferedImage.getMinX();
	//		int miny = bufferedImage.getMinY();
	//
	//		ColorInfo imageInfo$0 = new ColorInfo();
	//		ColorInfo imageInfo$32 = new ColorInfo();
	//		ColorInfo imageInfo$64 = new ColorInfo();
	//		ColorInfo imageInfo$128 = new ColorInfo();
	//		ColorInfo imageInfo$192 = new ColorInfo();
	//		ColorInfo imageInfo$200 = new ColorInfo();
	//		ColorInfo imageInfo$240 = new ColorInfo();
	//		ColorInfo imageInfo$220 = new ColorInfo();
	//
	//		for (int i = minx; i < width; i++) {
	//			for (int j = miny; j < height; j++) {
	//				int pixel = bufferedImage.getRGB(i, j);
	//				rgb[0] = (pixel & 0xff0000) >> 16;
	//				rgb[1] = (pixel & 0xff00) >> 8;
	//				rgb[2] = (pixel & 0xff);
	//
	//				double $object = rgb[0] * 0.299 + rgb[1] * 0.587 + rgb[2] * 0.114;
	//				Color c = new Color(rgb[0], rgb[1], rgb[2]);
	//				if ($object > 240) {
	//					if (imageInfo$240.color == null || imageInfo$240.value > $object) {
	//						imageInfo$240.setInfo(c, $object);
	//					}
	//					imageInfo$240.add();
	//				} else if ($object > 220) {
	//					if (imageInfo$220.color == null || imageInfo$220.value > $object) {
	//						imageInfo$220.setInfo(c, $object);
	//					}
	//					imageInfo$220.add();
	//				} else if ($object > 210) {
	//					if (imageInfo$200.color == null || imageInfo$200.value > $object) {
	//						imageInfo$200.setInfo(c, $object);
	//					}
	//					imageInfo$200.add();
	//				} else if ($object > 192) {
	//					if (imageInfo$192.color == null || imageInfo$192.value > $object) {
	//						imageInfo$192.setInfo(c, $object);
	//					}
	//					imageInfo$192.add();
	//				} else if ($object > 128) {
	//					if (imageInfo$128.color == null || imageInfo$128.value > $object) {
	//						imageInfo$128.setInfo(c, $object);
	//					}
	//					imageInfo$128.add();
	//				} else if ($object > 64) {
	//					if (imageInfo$64.color == null || imageInfo$64.value < $object) {
	//						imageInfo$64.setInfo(c, $object);
	//					}
	//					imageInfo$64.add();
	//				} else if ($object > 32) {
	//					if (imageInfo$32.color == null || imageInfo$32.value < $object) {
	//						imageInfo$32.setInfo(c, $object);
	//					}
	//					imageInfo$32.add();
	//				} else {
	//					if (imageInfo$0.color == null || imageInfo$0.value < $object) {
	//						imageInfo$0.setInfo(c, $object);
	//					}
	//					imageInfo$0.add();
	//				}
	//			}
	//		}
	//		ColorInfo[] values = new ColorInfo[] { imageInfo$0, imageInfo$32, imageInfo$64, imageInfo$128, imageInfo$192, imageInfo$200,
	//				imageInfo$240, imageInfo$220 };
	//
	//		java.util.Arrays.sort(values);
	//
	//		int colorIndex = 0;
	//		for (int i = 0; i < values.length; i++) {
	//			if (values[i].total != 0) {
	//				colorIndex = i;
	//				break;
	//			}
	//		}
	//
	//		ColorInfo minImageInfo = null;
	//		for (int i = values.length - 1; i >= colorIndex; i--) {
	//			ColorInfo imageInfo = values[i];
	//			if (imageInfo.value > 128) {
	//				return imageInfo.color;
	//			}
	//			if (minImageInfo == null) {
	//				minImageInfo = imageInfo;
	//			} else if (imageInfo.value > minImageInfo.value) {
	//				minImageInfo = imageInfo;
	//			}
	//		}
	//		if (minImageInfo.value < 65) {
	//
	//			return color;
	//		} else {
	//			return ColorUtil.getAlphaColor(minImageInfo.color, 128);
	//		}
	//	}

	public static Color getImageColor(Image image, int x, int y) {
		BufferedImage bufferedImage = getBufferedImage(image);
		if (bufferedImage == null)
			return color;
		int width = bufferedImage.getWidth();
		int height = bufferedImage.getHeight();
		int minx = bufferedImage.getMinX();
		int miny = bufferedImage.getMinY();
		if (x < minx || x > minx + width || y < miny || y > miny + height) {
			return null;
		}
		int pixel = bufferedImage.getRGB(x, y);

		return new Color((pixel & 0xff0000) >> 16, (pixel & 0xff00) >> 8, (pixel & 0xff));
	}

	public static Color getColor(BufferedImage bufferedImage, int x, int y) {
		int width = bufferedImage.getWidth();
		int height = bufferedImage.getHeight();
		int minx = bufferedImage.getMinX();
		int miny = bufferedImage.getMinY();
		if (x < minx || x > minx + width || y < miny || y > miny + height) {
			return null;
		}
		int pixel = bufferedImage.getRGB(x, y);

		return new Color((pixel & 0xff0000) >> 16, (pixel & 0xff00) >> 8, (pixel & 0xff));
	}

	public static BufferedImage getBufferedImage(URL paramURL) {
		return getBufferedImage(new ImageIcon(paramURL));
	}

	public static BufferedImage getBufferedImage(String paramString) {
		return getBufferedImage(new ImageIcon(paramString));
	}

	public static BufferedImage getBufferedImage(ImageIcon paramImageIcon) {
		return getBufferedImage(paramImageIcon.getImage());
	}

	public static BufferedImage getBufferedImage(Image paramImage) {
		if ((paramImage == null))
			return null;

		boolean f = (paramImage.getWidth(null) <= 0) || (paramImage.getHeight(null) <= 0);
		int i = 0;
		while (f) {//比较傻逼
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			f = (paramImage.getWidth(null) <= 0) || (paramImage.getHeight(null) <= 0);
			i++;
			if (i > 4) {
				break;
			}
		}
		if (f) {
			return null;
		}

		if ((paramImage instanceof BufferedImage))
			return (BufferedImage) paramImage;
		if (((paramImage instanceof ToolkitImage)) && (((ToolkitImage) paramImage).getBufferedImage() != null))
			return ((ToolkitImage) paramImage).getBufferedImage();
		BufferedImage localBufferedImage = createCompatibleImage(paramImage.getWidth(null), paramImage.getHeight(null), 3);
		Graphics2D localGraphics2D = localBufferedImage.createGraphics();
		localGraphics2D.drawImage(paramImage, 0, 0, null);
		localGraphics2D.dispose();
		return localBufferedImage;
	}

	public static Image getBufferedImage(Icon paramIcon) {
		if (paramIcon == null)
			return null;
		BufferedImage localBufferedImage = createCompatibleImage(paramIcon.getIconWidth(), paramIcon.getIconHeight(), 3);
		Graphics2D localGraphics2D = localBufferedImage.createGraphics();
		paramIcon.paintIcon(null, localGraphics2D, 0, 0);
		localGraphics2D.dispose();
		return localBufferedImage;
	}

	public static BufferedImage createCompatibleImage(int paramInt1, int paramInt2, int paramInt3) {
		return getGraphicsConfiguration().createCompatibleImage(paramInt1, paramInt2, paramInt3);
	}

	public static BufferedImage createCompatibleImage(BufferedImage paramBufferedImage, int paramInt1, int paramInt2) {
		return createCompatibleImage(paramInt1, paramInt2, paramBufferedImage.getTransparency());
	}

	public static GraphicsConfiguration getGraphicsConfiguration() {
		return getGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
	}

	private static GraphicsEnvironment getGraphicsEnvironment() {
		return GraphicsEnvironment.getLocalGraphicsEnvironment();
	}

	static class ColorInfo implements Comparable<ColorInfo> {
		public Color color;
		public double value = 0;
		public int total = 0;

		public static int allTotal = 0;

		public void setInfo(Color color, double value) {
			this.color = color;
			this.value = value;
			total++;
			allTotal++;
		}

		public void add() {
			total++;
			allTotal++;
		}

		public double percent() {
			return total / allTotal;
		}

		public int compareTo(ColorInfo another) {
			int thisVal = this.total;
			int anotherVal = another.total;
			return (thisVal < anotherVal ? -1 : (thisVal == anotherVal ? 0 : 1));
		}
		//		public int compareTo(ImageInfo another) {
		//			double thisVal = this.value;
		//			double anotherVal = another.value;
		//			return (thisVal < anotherVal ? -1 : (thisVal == anotherVal ? 0 : 1));
		//		}
	}
}
