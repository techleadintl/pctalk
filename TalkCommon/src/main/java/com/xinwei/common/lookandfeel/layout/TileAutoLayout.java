package com.xinwei.common.lookandfeel.layout;

import java.awt.Component;

public class TileAutoLayout extends TileLayout {
	protected Component component;
	protected int excludeWidth;

	public TileAutoLayout(Component component, int hgap, int vgap) {
		this(component, 0, hgap, vgap);
	}

	public TileAutoLayout(Component component, int excludeWidth, int hgap, int vgap) {
		super(hgap, vgap);
		this.component = component;
		this.excludeWidth = excludeWidth;
	}

	public int getColumn() {
		int width = component.getWidth();
		int c = (width - excludeWidth) / (d.width + hgap);
		if (c == 0)
			c = 1;
		return c;
	}
}