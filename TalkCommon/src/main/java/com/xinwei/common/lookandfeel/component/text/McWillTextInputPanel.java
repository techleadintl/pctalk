/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月5日 下午4:32:55
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component.text;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.xinwei.common.lookandfeel.McWillBorders;
import com.xinwei.common.lookandfeel.component.McWillHintTextField;
import com.xinwei.common.lookandfeel.component.McWillIconButton;
import com.xinwei.common.lookandfeel.util.PainterUtil;

public class McWillTextInputPanel extends JPanel {
	protected String defaultText;
	protected McWillHintTextField textField;
	protected McWillIconButton button;

	public McWillTextInputPanel(String defaultText) {
		this.defaultText = defaultText;

		setLayout(new BorderLayout());

		textField = new McWillHintTextField(defaultText);
		textField.setBorder(BorderFactory.createEmptyBorder());

		button = new McWillIconButton();

		add(textField, BorderLayout.CENTER);

		add(button, BorderLayout.EAST);

		textField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				documentChanged();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				documentChanged();

			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				documentChanged();
			}

			public void documentChanged() {
				onDocumentChanged();
				doSearch(textField.getText());
			}
		});

		final ActionListener l = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onButtonClick();
			}
		};

		button.addActionListener(l);
	}

	protected void setTextFieldFont(Font font) {
		textField.setFont(font);
	}

	protected void onDocumentChanged() {
	}

	protected void onButtonClick() {

	}

	protected void doSearch(String text) {

	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);

		PainterUtil.paintRoundBackgroudColor(g2d, Color.WHITE, 0, 0, getWidth(), getHeight());
		setBorder(McWillBorders.getRoundRectBorder(Color.GRAY));
		g2d.dispose();
	}

	protected Color getBorderColor() {
		// TODO Auto-generated method stub
		return null;
	}

	public McWillHintTextField getTextField() {
		return textField;
	}

	public McWillIconButton getButton() {
		return button;
	}

	public void setButton(McWillIconButton button) {
		this.button = button;
	}

	public String getText() {
		String text = textField.getText();
		if (text.equals(defaultText)) {
			return "";
		} else {
			return text;
		}
	}

}
