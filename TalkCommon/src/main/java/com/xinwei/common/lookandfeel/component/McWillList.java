/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月2日 下午2:57:51
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.lang.reflect.Method;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.ListUI;
import javax.swing.plaf.basic.BasicListUI;

@SuppressWarnings("serial")
public class McWillList<E> extends JList<E> {
	private int rolloverIndex = -1;

	public McWillList(ListModel<E> dataModel) {
		super(dataModel);
		getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	public McWillList(final E[] listData) {
		super(listData);
		getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	public McWillList(final Vector<? extends E> listData) {
		super(listData);
		getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

	}

	public McWillList() {
		super();
		getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	public int getRolloverIndex() {
		return rolloverIndex;
	}

	public void setRolloverIndex(int rolloverIndex) {
		this.rolloverIndex = rolloverIndex;
	}

	public boolean containsPoint(Point p) {
		int size = getModel().getSize();
		for (int i = 0; i < size; i++) {
			Rectangle cellBounds = getCellBounds(i, i + 1);
			if (cellBounds == null) {
				continue;
			} else {
				if (cellBounds.contains(p)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean containsPoint(int i, Point p) {
		Rectangle cellBounds = getCellBounds(i, i + 1);
		if (cellBounds == null) {
			return false;
		}
		return cellBounds.contains(p);
	}

	@Override
	public void updateUI() {
		setUI(new MyListUI());
		invalidate();
	}

	public int getValidIndex(Point p) {
		try {
			int index = locationToIndex(p);
			return index >= 0 && getCellBounds(index, index).contains(p) ? index : -1;
		} catch (Exception ex) {
			return -1;
		}
	}

	class MyListUI extends BasicListUI {
		protected MouseInputListener createMouseInputListener() {
			return new ListMouseInputHandler();
		}

		private class ListMouseInputHandler extends MouseInputHandler {
			//			private int selIndex = -1;

			public void mousePressed(MouseEvent e) {
				int index = getValidIndex(e.getPoint());
				if (index >= 0) {
					//					selIndex = index;
					super.mousePressed(e);
					doMouseEvent(e, index, "mousePressed");
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int index = getValidIndex(e.getPoint());
				if (index >= 0) {
					super.mouseClicked(e);

					doMouseEvent(e, index, "mouseClicked");
				}
			}

			public void mouseDragged(MouseEvent e) {
				int index = getValidIndex(e.getPoint());
				if (index >= 0) {
					super.mouseDragged(e);
				}
			}

			public void mouseMoved(MouseEvent e) {
				mouseEntered(e);

				int index = getValidIndex(e.getPoint());
				if (index >= 0) {
					JComponent listCellComponent = getIndexCell(index);

					Rectangle r = getCellBounds(list, index, index);

					Point p = new Point(e.getX() - r.x, e.getY() - r.y);

					MouseMotionListener[] mouseMotionListeners = listCellComponent.getMouseMotionListeners();
					for (MouseMotionListener ml : mouseMotionListeners) {
						MouseEvent e2 = new MouseEvent(listCellComponent, e.getID(), e.getWhen(), e.getModifiers(), p.x, p.y, e.getClickCount(), e.isPopupTrigger(), e.getButton());
						ml.mouseMoved(e2);
					}

					mouseChildComponentMoved(e, listCellComponent, p);

					list.repaint(r.x, r.y, r.width, r.height);
				}
			}

			public void mouseEntered(MouseEvent e) {
				if (list.isEnabled()) {
					setRolloverIndex(e, getValidIndex(e.getPoint()));
				}
			}

			public void mouseExited(MouseEvent e) {
				if (list.isEnabled()) {
					setRolloverIndex(e, -1);
				}
			}

			public void mouseReleased(MouseEvent e) {
				int index = getValidIndex(e.getPoint());
				if (index >= 0) {
					super.mouseReleased(e);
					doMouseEvent(e, index, "mouseReleased");
					//					if (selIndex > -1 && list.getSelectedIndex() == selIndex)
					//						list.removeSelectionInterval(selIndex, selIndex);
				}
			}

			private void setRolloverIndex(MouseEvent e, int index) {
				int oldIndex = rolloverIndex;
				if (index != oldIndex) {
					rolloverIndex = index;
					if (oldIndex > -1) {//退出
						Rectangle r = getCellBounds(list, oldIndex, oldIndex);
						if (r != null) {
							list.repaint(r.x, r.y, r.width, r.height);
							JComponent listCellComponent = getIndexCell(oldIndex);
							MouseListener[] mouseListeners = listCellComponent.getMouseListeners();
							for (MouseListener ml : mouseListeners) {
								ml.mouseExited(e);
							}

						}
					}
					if (index > -1) {//进入
						Rectangle r = getCellBounds(list, index, index);
						if (r != null) {
							list.repaint(r.x, r.y, r.width, r.height);
							JComponent listCellComponent = getIndexCell(index);
							MouseListener[] mouseListeners = listCellComponent.getMouseListeners();
							for (MouseListener ml : mouseListeners) {
								ml.mouseEntered(e);
							}
						}
					}
				}
			}
		}

		private int getValidIndex(Point p) {
			try {
				int index = locationToIndex(list, p);
				return index >= 0 && getCellBounds(list, index, index).contains(p) ? index : -1;
			} catch (Exception ex) {
				return -1;
			}
		}

		private JComponent getIndexCell(int index) {
			Object elementAt = list.getModel().getElementAt(index);

			JComponent listCellComponent = (JComponent) list.getCellRenderer().getListCellRendererComponent(list, elementAt, index, list.getSelectedIndex() == index, false);
			return listCellComponent;
		}

		private void mouseChildComponentMoved(MouseEvent e, JComponent parentComp, Point p) {
			for (int i = 0; i < parentComp.getComponentCount(); i++) {
				Component component = parentComp.getComponent(i);
				if (!(component instanceof JComponent))
					continue;

				JComponent childComp = (JComponent) component;
				Object clientProperty = childComp.getClientProperty(MOUSE_KEY);
				Rectangle childBounds = childComp.getBounds();
				if (childBounds.contains(p)) {//包含
					Point childPoint = new Point(p.x - childBounds.x, p.y - childBounds.y);
					if (clientProperty == null) {
						childComp.putClientProperty(MOUSE_KEY, MOUSE_KEY);
						MouseListener[] mouseListeners = childComp.getMouseListeners();
						for (MouseListener ml : mouseListeners) {
							MouseEvent e2 = new MouseEvent(childComp, e.getID(), e.getWhen(), e.getModifiers(), childPoint.x, childPoint.y, e.getClickCount(), e.isPopupTrigger(), e.getButton());
							ml.mouseEntered(e2);
						}
					} else {
						MouseMotionListener[] mouseMotionListeners = childComp.getMouseMotionListeners();
						for (MouseMotionListener ml : mouseMotionListeners) {
							MouseEvent e2 = new MouseEvent(childComp, e.getID(), e.getWhen(), e.getModifiers(), childPoint.x, childPoint.y, e.getClickCount(), e.isPopupTrigger(), e.getButton());
							ml.mouseMoved(e2);
						}
					}
					mouseChildComponentMoved(e, childComp, childPoint);
				} else {//不包含
					if (clientProperty != null) {
						childComp.putClientProperty(MOUSE_KEY, null);
						MouseListener[] mouseListeners = childComp.getMouseListeners();
						for (MouseListener ml : mouseListeners) {
							MouseEvent e2 = new MouseEvent(childComp, e.getID(), e.getWhen(), e.getModifiers(), p.x, p.y, e.getClickCount(), e.isPopupTrigger(), e.getButton());
							ml.mouseExited(e2);
						}
					}
				}

			}
		}

		private void doMouseEvent(MouseEvent e, int index, String methodName) {
			Rectangle r = getCellBounds(list, index, index);
			Point p = new Point(e.getX() - r.x, e.getY() - r.y);

			Object elementAt = list.getModel().getElementAt(index);

			JComponent listCellComponent = (JComponent) list.getCellRenderer().getListCellRendererComponent(list, elementAt, index, list.getSelectedIndex() == index, false);

			doChildMouseEvent(e, methodName, listCellComponent, p);
		}

		private void doChildMouseEvent(MouseEvent e, String methodName, JComponent listCellComponent, Point p) {
			MouseListener[] mls = listCellComponent.getMouseListeners();
			for (MouseListener ml : mls) {
				MouseEvent e2 = new MouseEvent(listCellComponent, e.getID(), e.getWhen(), e.getModifiers(), p.x, p.y, e.getClickCount(), e.isPopupTrigger(), e.getButton());
				invoke(methodName, ml, new Class[] { MouseEvent.class }, new Object[] { e2 });
			}

			for (int i = 0; i < listCellComponent.getComponentCount(); i++) {
				Component component = listCellComponent.getComponent(i);
				if (component instanceof JComponent) {
					JComponent childComp = (JComponent) component;
					Rectangle childBounds = childComp.getBounds();
					if (childBounds.contains(p)) {
						Point childPoint = new Point(p.x - childBounds.x, p.y - childBounds.y);

						doChildMouseEvent(e, methodName, childComp, childPoint);
					}
				}
			}
		}

		public Object invoke(String methodName, Object object, Class<?>[] argsTypes, Object[] args) {
			Object result = null;
			try {
				Method m = object.getClass().getMethod(methodName, argsTypes);
				m.setAccessible(true);
				result = m.invoke(object, args);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}
	}

	String MOUSE_KEY = "^&^%^@^";
}
