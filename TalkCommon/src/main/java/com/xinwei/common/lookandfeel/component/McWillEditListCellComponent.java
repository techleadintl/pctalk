package com.xinwei.common.lookandfeel.component;

import java.awt.Component;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillEditList.XTable;
import com.xinwei.talk.ui.chat.room.history.HistoryFileWindow.FileMsgPanel;

@SuppressWarnings("serial")
public class McWillEditListCellComponent<T extends Component> extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {
	public Object getCellEditorValue() {
		return "";
	}

	protected void updateData(T component, JTable table, Object value, boolean isSelected, int row, int column) {
	}

	protected void onSelected(T component, JTable table, Object value, boolean isSelected, int row, int column) {

	}

	protected void onHovered(T component, JTable table, Object value, boolean isSelected, int row, int column) {

	}

	protected void onHoverLosted(JTable table, int row) {
//		Object value = table.getValueAt(row, 0);
//		T tableCellComponent = getTableCellComponent(table, value, table.getSelectedRow() == row, row, 0);
//		if (tableCellComponent != null) {
//			onDefault(tableCellComponent, table, value, table.getSelectedRow() == row, row, 0);
//		}
	}

	protected void onDefault(T component, JTable table, Object value, boolean isSelected, int row, int column) {

	}

	protected T getTableCellComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		return null;
	}

	@Override
	public boolean shouldSelectCell(EventObject anEvent) {
		return true;
	}

	protected Component setBackground(T component, JTable table, Object value, boolean isSelected, int row, int column) {
		if (isSelected) {
			component.setBackground(McWillTheme.getThemeColor().getLightColor());
			//			component.setBackground(Color.GREEN);
			onSelected(component, table, value, isSelected, row, column);
			fireEditingStopped();
		} else if (istHoverdRow(table, row)) {
			component.setBackground(McWillTheme.getThemeColor().getMiddleColor());
			//			component.setBackground(Color.RED);
			onHovered(component, table, value, isSelected, row, column);
		} else {
			component.setBackground(McWillTheme.getThemeColor().getSubDarkColor());
			
			//			component.setBackground(Color.YELLOW);
			onDefault(component, table, value, isSelected, row, column);
		}
		return component;
	}

	private boolean istHoverdRow(JTable table, int row) {
		return ((XTable) table).rolloverIndex == row;
	}

	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		T cellComponent = getTableCellComponent(table, value, true, row, column);
		setBackground(cellComponent, table, value, true, row, column);
		return cellComponent;
	}

	public T getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		T cellComponent = getTableCellComponent(table, value, isSelected, row, column);

		setBackground(cellComponent, table, value, isSelected, row, column);
		return cellComponent;
	}
}