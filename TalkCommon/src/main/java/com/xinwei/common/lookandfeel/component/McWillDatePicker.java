package com.xinwei.common.lookandfeel.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.EventListener;
import java.util.GregorianCalendar;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.EventListenerList;

import com.jtattoo.plaf.LazyImageIcon;
import com.xinwei.common.lang.DateUtil;

//import com.xinwei.talk.common.Res;

public class McWillDatePicker extends JPanel {

	protected Color weekBackgroundColor = new Color(189, 235, 238);
	protected Color weekendBtnFontColor = new Color(240, 64, 64); // color
	protected Color selectedColor = weekBackgroundColor;
	protected Font labelFont = new Font("Dialog", Font.PLAIN, 12);
	protected Font headFont = new Font("Dialog", Font.PLAIN, 14);
	protected Color defaultBtnFontColor = Color.BLACK;
	protected Color defaultBtnBackgroundColor = Color.WHITE;
	private Calendar cal = null;
	private Calendar todayCal = null;
	private int year;
	private int month;
	private int day;
	private JPanel navigatePanel;
	private JPanel dateHeadPanel = null;
	private JPanel dateContainerPanel = null;
	protected DateButton[][] buttonDays = null;

	protected int[] months = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
	private EventListenerList actionListenerList = new EventListenerList();

	private Date maxCalendar;

	private Date minCalendar;

	public McWillDatePicker() {
		setLayout(new BorderLayout(0, 0));
		cal = Calendar.getInstance();

		buttonDays = new DateButton[6][7];
		todayCal = Calendar.getInstance();
		this.year = cal.get(Calendar.YEAR);
		this.month = cal.get(Calendar.MONTH);
		this.day = cal.get(Calendar.DATE);

		navigatePanel = new NavigatePanel();

		dateHeadPanel = new JPanel(new GridLayout(1, 7, 0, 0));

		dateContainerPanel = new JPanel(new GridLayout(6, 7, 0, 0));
		//		dateContainerPanel.setBorder(BorderFactory.createEtchedBorder());

		JPanel p = new JPanel(new GridLayout(2, 1, 0, 0));
		p.setBackground(weekBackgroundColor);
		p.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		p.add(navigatePanel);
		p.add(dateHeadPanel);

		add(p, BorderLayout.NORTH);
		add(dateContainerPanel, BorderLayout.CENTER);

		createDayHeadPanel();
		createDayPanel(cal);
		setActiveDay(day);

	}

	protected String[] getWeekTitle() {
		return new String[] { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
	}

	public JPanel createDayHeadPanel() {
		String[] weekTitle = getWeekTitle();
		if (weekTitle == null) {
			return dateHeadPanel;
		}
		for (int i = 0; i < 7; i++) {
			JLabel weekLabel = new JLabel(weekTitle[i], SwingConstants.CENTER);
			weekLabel.setFont(headFont);
			weekLabel.setOpaque(false);
			dateHeadPanel.add(weekLabel);
		}
		return dateHeadPanel;
	}

	/**
	 * 创建日期组件
	 * 
	 * @param cal
	 *            void
	 */
	public void createDayPanel(Calendar cal) {
		dateContainerPanel.removeAll();
		dateContainerPanel.revalidate();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.DAY_OF_MONTH, -1);

		cal.set(Calendar.DAY_OF_MONTH, 1);
		int weekday = cal.get(Calendar.DAY_OF_WEEK);

		cal.add(Calendar.DAY_OF_MONTH, 1 - weekday);

		DayButtonActionListener dayButtonActionListener = new DayButtonActionListener();

		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				int curMonth = cal.get(Calendar.MONTH);
				DateButton button = null;
				if (curMonth != month) {
					button = new DateButton(" ");
					button.setEnabled(false);
					button.setBackground(defaultBtnBackgroundColor);
				} else {
					int currentDay = cal.get(Calendar.DAY_OF_MONTH);
					button = new DateButton(currentDay + "");
					button.setHorizontalTextPosition(SwingConstants.RIGHT);
					button.setFont(labelFont);
					button.setBackground(defaultBtnBackgroundColor);
					if (currentDay == todayCal.get(Calendar.DAY_OF_MONTH) && month == todayCal.get(Calendar.MONTH) && year == todayCal.get(Calendar.YEAR)) {
						button.setBorder(BorderFactory.createLineBorder(weekendBtnFontColor));
					}
					if (cal.get(Calendar.MONTH) != month) {
						button.setForeground(Color.BLUE);
					}
					if (j == 0 || j == 6) {
						button.setForeground(weekendBtnFontColor);
					} else {
						button.setForeground(defaultBtnFontColor);
					}

				}
				button.addActionListener(dayButtonActionListener);
				buttonDays[i][j] = button;
				dateContainerPanel.add(buttonDays[i][j]);
				cal.add(Calendar.DAY_OF_MONTH, 1);
			}
		}

	}

	/**
	 * 选中一天
	 * 
	 * @param selectedDay
	 *            void
	 */
	public void setActiveDay(int selectedDay) {
		clearAllActiveDay();
		if (selectedDay <= 0) {
			day = new GregorianCalendar().get(Calendar.DAY_OF_MONTH);
		} else {
			day = selectedDay;
		}
		int leadGap = new GregorianCalendar(year, month, 1).get(Calendar.DAY_OF_WEEK) - 1;
		JButton selectedButton = buttonDays[(leadGap + selectedDay - 1) / 7][(leadGap + selectedDay - 1) % 7];
		selectedButton.setBackground(weekBackgroundColor);
	}

	/**
	 * 清除所有选择的日期
	 * 
	 */
	public void clearAllActiveDay() {
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				JButton button = buttonDays[i][j];
				if (button.getText() != null && button.getText().trim().length() > 0) {
					button.setBackground(defaultBtnBackgroundColor);
					button.revalidate();
				}
			}

		}
	}

	/**
	 * 获取选中的日期
	 * 
	 * @return String
	 */
	public Calendar getSelectedDate() {
		return new GregorianCalendar(year, month, day);
	}

	public void setMaxCalendar(Date maxCalendar) {
		this.maxCalendar = maxCalendar;
	}

	public void setMinCalendar(Date minCalendar) {
		this.minCalendar = minCalendar;
	}

	protected ImageIcon getNextYearImageIcon() {
		return new ImageIcon(McWillDatePicker.class.getResource("images/nextyear.gif"), ">>");
	}

	protected ImageIcon getNextMonthImageIcon() {
		return new ImageIcon(McWillDatePicker.class.getResource("images/nextmon.gif"), ">");
	}

	protected ImageIcon getPreMonthImageIcon() {
		return new ImageIcon(McWillDatePicker.class.getResource("images/premon.gif"), "<");
	}

	protected ImageIcon getPreYearImageIcon() {
		return new ImageIcon(McWillDatePicker.class.getResource("images/preyear.gif"), "<<");
	}

	class DayButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JButton button = (JButton) e.getSource();
			if (button.getText() != null && button.getText().trim().length() > 0) {
				int d = Integer.parseInt(button.getText());

				if (minCalendar != null || maxCalendar != null) {

					GregorianCalendar calendar = new GregorianCalendar(year, month, d);
					if (minCalendar != null && calendar.getTimeInMillis() < minCalendar.getTime()) {
						return;
					}
					if (maxCalendar != null && calendar.getTimeInMillis() > maxCalendar.getTime()) {
						return;
					}
				}

				day = d;
				setActiveDay(day);
				fireActionPerformed(e);// fire button click event
			}
		}
	}

	public void addActionListener(ActionListener actionListener) {
		actionListenerList.add(ActionListener.class, actionListener);
	}

	public void removeActionListener(ActionListener actionListener) {
		actionListenerList.remove(ActionListener.class, actionListener);
	}

	/**
	 * 事件管理,触发事件
	 * 
	 * @param actionEvent
	 *            void
	 */
	protected void fireActionPerformed(ActionEvent actionEvent) {
		EventListener listenerList[] = actionListenerList.getListeners(ActionListener.class);
		for (int i = 0, n = listenerList.length; i < n; i++) {
			((ActionListener) listenerList[i]).actionPerformed(actionEvent);
		}
	}

	protected String getMonthText(int year, int month) {
		return String.valueOf(year + "-" + month);
	}

	public class NavigatePanel extends JPanel implements ActionListener {
		private JButton premon;

		private JButton preyear;

		private JButton nextmon;

		private JButton nextyear;

		private JLabel lbl;

		public NavigatePanel() {
			setBackground(weekBackgroundColor);
			setLayout(new BorderLayout(0, 0));
			Dimension d = new Dimension(20, 20);
			JPanel left = new JPanel(new FlowLayout(FlowLayout.LEFT));
			preyear = new McWillIconButton(getPreYearImageIcon());
			preyear.addActionListener(this);
			preyear.setPreferredSize(d);
			left.add(preyear);

			premon = new McWillIconButton(getPreMonthImageIcon());
			premon.addActionListener(this);
			premon.setPreferredSize(d);
			left.add(premon);

			add(left, BorderLayout.WEST);

			JPanel right = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			nextmon = new McWillIconButton(getNextMonthImageIcon());
			nextmon.setPreferredSize(d);
			nextmon.addActionListener(this);
			right.add(nextmon);

			nextyear = new McWillIconButton(getNextYearImageIcon());
			nextyear.setPreferredSize(d);
			nextyear.addActionListener(this);
			right.add(nextyear);

			add(right, BorderLayout.EAST);

			lbl = new JLabel();
			lbl.setFont(headFont);
			lbl.setHorizontalAlignment(SwingConstants.CENTER);

			add(lbl, BorderLayout.CENTER);
			lbl.setText(getMonthText(year, month + 1));
		}

		public void actionPerformed(ActionEvent e) {
			Calendar cal = new GregorianCalendar(year, month, 1);
			if (e.getSource() == premon) {
				cal.add(Calendar.MONTH, -1);
			} else if (e.getSource() == nextmon) {
				cal.add(Calendar.MONTH, 1);
			} else if (e.getSource() == nextyear) {
				cal.add(Calendar.YEAR, 1);
			} else if (e.getSource() == preyear) {
				cal.add(Calendar.YEAR, -1);
			}
			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH);
			createDayPanel(cal);
			lbl.setText(getMonthText(year, month + 1));
		}
	}

	public class DateButton extends JButton {
		private static final long serialVersionUID = 1L;
		protected Color normalBackground;

		public DateButton() {
			initAttributes();
		}

		public DateButton(Icon icon) {
			super(icon);
			initAttributes();
		}

		public DateButton(String text, Icon icon) {
			super(text, icon);
			initAttributes();
		}

		public DateButton(String text) {
			super(text);
			initAttributes();
		}

		public DateButton(Action a) {
			super(a);
			initAttributes();
		}

		public void initAttributes() {
			setRolloverEnabled(true);
			setBorder(BorderFactory.createEmptyBorder());
			setContentAreaFilled(false);
			setFocusPainted(false);
			setNormalBackground(new Color(216, 216, 216));
			setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		}

		@Override
		public void paint(Graphics g) {

			Graphics2D g2d = (Graphics2D) g;
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			Paint oldPaint = g2d.getPaint();

			if ((getModel().isSelected() || getModel().isPressed()) && weekBackgroundColor != null) {
				g2d.setPaint(weekBackgroundColor);
				g2d.fillRect(0, 0, getWidth(), getHeight());
			} else if (getNormalBackground() != null) {
				g2d.setPaint(getNormalBackground());
				g2d.fillRect(0, 0, getWidth(), getHeight());
			}
			g2d.setPaint(oldPaint);
			super.paint(g2d);

		}

		public void clearDefaultAttribute() {
			setNormalBackground(null);
		}

		@Override
		public void setBackground(Color bg) {
			super.setBackground(bg);
			normalBackground = bg;
		}

		public Color getNormalBackground() {
			return normalBackground;
		}

		public void setNormalBackground(Color normalBackground) {
			super.setBackground(normalBackground);
			this.normalBackground = normalBackground;
		}

	}

	public static void main(String[] args) {

		JFrame frame = new JFrame("DP");
		frame.setSize(400, 300);

		final McWillDatePicker datePicker = new McWillDatePicker();
		datePicker.addActionListener(new ActionListener() {// 事件捕获
			public void actionPerformed(ActionEvent e) {
			}
		});
		frame.getContentPane().add(datePicker);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

}

//-------自定义按钮--------------
