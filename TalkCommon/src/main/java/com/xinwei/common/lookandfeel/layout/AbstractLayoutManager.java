package com.xinwei.common.lookandfeel.layout;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager2;

public abstract class AbstractLayoutManager implements LayoutManager2 {
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addLayoutComponent(final Component comp, final Object constraints) {
		addComponent(comp, constraints);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addLayoutComponent(final String name, final Component comp) {
		addComponent(comp, name);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeLayoutComponent(final Component comp) {
		removeComponent(comp);
	}

	/**
	 * Called when component added into container with this layout.
	 *
	 * @param component
	 *            added component
	 * @param constraints
	 *            component constraints
	 */
	public void addComponent(final Component component, final Object constraints) {
		// Do nothing
	}

	/**
	 * Called when component removed from container with this layout.
	 *
	 * @param component
	 *            removed component
	 */
	public void removeComponent(final Component component) {
		// Do nothing
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension minimumLayoutSize(final Container parent) {
		return preferredLayoutSize(parent);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension maximumLayoutSize(final Container target) {
		return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getLayoutAlignmentX(final Container target) {
		return 0.5f;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getLayoutAlignmentY(final Container target) {
		return 0.5f;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void invalidateLayout(final Container target) {
		//
	}
}