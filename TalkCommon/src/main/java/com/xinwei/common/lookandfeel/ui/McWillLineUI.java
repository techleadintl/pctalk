package com.xinwei.common.lookandfeel.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;
import javax.swing.JSeparator;
import javax.swing.LookAndFeel;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.SeparatorUI;

public class McWillLineUI extends SeparatorUI {

	public static ComponentUI createUI(JComponent c) {
		return new McWillLineUI();
	}

	public void installUI(JComponent c) {
		installDefaults((JSeparator) c);
		installListeners((JSeparator) c);
	}

	public void uninstallUI(JComponent c) {
		uninstallDefaults((JSeparator) c);
		uninstallListeners((JSeparator) c);
	}

	protected void installDefaults(JSeparator s) {
		LookAndFeel.installProperty(s, "opaque", Boolean.FALSE);
	}

	protected void uninstallDefaults(JSeparator s) {
	}

	protected void installListeners(JSeparator s) {
	}

	protected void uninstallListeners(JSeparator s) {
	}

	public void paint(Graphics g, JComponent c) {
		Dimension s = c.getSize();
		g.setColor(c.getBackground());
		if (((JSeparator) c).getOrientation() == JSeparator.VERTICAL) {
			g.drawLine(0, 0, 1, s.height);
		} else // HORIZONTAL
		{
			g.drawLine(0, 0, s.width * 2, 1);
		}
	}

	public Dimension getPreferredSize(JComponent c) {
		if (((JSeparator) c).getOrientation() == JSeparator.VERTICAL)
			return new Dimension(1, 0);
		else
			return new Dimension(0, 1);
	}

	public Dimension getMinimumSize(JComponent c) {
		return null;
	}

	public Dimension getMaximumSize(JComponent c) {
		return null;
	}
}
