package com.xinwei.common.lookandfeel;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRootPane;
import javax.swing.UIManager;

import com.xinwei.common.lookandfeel.contentpanel.McWillWindowContentPanel;
import com.xinwei.common.lookandfeel.ui.McWillRootPaneUI;
import com.xinwei.common.lookandfeel.util.Constants;

public class McWillTest {
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(McWillLookAndFeel.class.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		JFrame f = new JFrame("Test");
		f.setSize(400, 300);
		JButton btn = new JButton("button");
		Container contentPane = f.getContentPane();
		f.setContentPane(new McWillWindowContentPanel());
		f.add(btn, BorderLayout.EAST);
		f.add(new JButton("lll"), BorderLayout.WEST);
		JRootPane root = f.getRootPane();
		root.putClientProperty(Constants.SKIN_BUTTON, true);
		Container parent = btn.getParent();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
