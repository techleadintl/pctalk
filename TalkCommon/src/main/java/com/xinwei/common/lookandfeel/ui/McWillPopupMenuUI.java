package com.xinwei.common.lookandfeel.ui;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;

import com.jtattoo.plaf.BasePopupMenuUI;

public class McWillPopupMenuUI extends BasePopupMenuUI {

	public static ComponentUI createUI(JComponent c) {
		c.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		return new McWillPopupMenuUI();
	}

}
