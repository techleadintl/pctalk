/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月25日 下午5:32:01
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.listener;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class HandCursorListener extends MouseAdapter {
	public void mouseEntered(MouseEvent e) {
		Component component = e.getComponent();
		component.setCursor(new Cursor(Cursor.HAND_CURSOR));

	}
	public void mouseExited(MouseEvent e) {
		Component component = e.getComponent();
		component.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}
}
