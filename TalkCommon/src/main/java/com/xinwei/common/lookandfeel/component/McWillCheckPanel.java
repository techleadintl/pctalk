package com.xinwei.common.lookandfeel.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class McWillCheckPanel extends JPanel {
	private boolean check;
	private Icon uncheckImage;
	private Icon checkImage;

	private JLabel label;

	public McWillCheckPanel() {
	}

	public McWillCheckPanel(Icon uncheckImage, Icon checkImage) {
		this.uncheckImage = uncheckImage;
		this.checkImage = checkImage;
	}

	public McWillCheckPanel(Icon uncheckImage, Icon checkImage, boolean check) {
		this.uncheckImage = uncheckImage;
		this.checkImage = checkImage;
		this.check = check;
	}

	public JLabel initUI() {
		this.setLayout(new BorderLayout());

		createUI();

		this.label = new JLabel();

		add(this.label, BorderLayout.EAST);

		setCheck(check);

		addCheckedListener(this);

		return this.label;
	}

	protected void createUI() {

	}

	MouseAdapter selectedListener = new MouseAdapter() {
		public void mousePressed(MouseEvent e) {
			setCheck(!McWillCheckPanel.this.check);
		};
	};

	protected void addCheckedListener(Component p) {
		Component[] components = null;
		if (p instanceof JComponent) {
			JComponent childComponent = (JComponent) p;
			childComponent.addMouseListener(selectedListener);
			components = childComponent.getComponents();
		} else if (p instanceof Container) {
			components = ((Container) p).getComponents();
		}
		if (components == null)
			return;

		for (Component c : components) {
			addCheckedListener(c);
		}
	}

	public String getText() {
		return this.label.getText();
	}

	public void setText(String text) {
		this.label.setText(text);
	}

	public String getToolTipText() {
		return this.label.getToolTipText();
	}

	public void setToolTipText(String toolTipText) {
		this.label.setToolTipText(toolTipText);
	}

	public boolean getCheck() {
		return this.check;
	}

	public void setCheck(boolean selection) {
		setImage(selection);

		firePropertyChange("check", this.check, selection);

		this.check = selection;
	}

	private void setImage(boolean check) {
		if (check)
			this.label.setIcon(this.checkImage);
		else
			this.label.setIcon(this.uncheckImage);
	}

	public Icon getUncheckImage() {
		return this.uncheckImage;
	}

	public void setUncheckIcon(Icon uncheckImage) {
		this.uncheckImage = uncheckImage;
	}

	public Icon getCheckImage() {
		return this.checkImage;
	}

	public void setCheckIcon(Icon checkImage) {
		this.checkImage = checkImage;
	}

	public void addCheckListener(PropertyChangeListener listener) {
		this.addPropertyChangeListener(listener);
	}
}