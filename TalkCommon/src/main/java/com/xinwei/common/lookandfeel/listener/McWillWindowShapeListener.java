package com.xinwei.common.lookandfeel.listener;

import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.RoundRectangle2D;

import com.sun.awt.AWTUtilities;

public class McWillWindowShapeListener extends ComponentAdapter {
	private double arc = 10;

	public McWillWindowShapeListener() {
	}

	public McWillWindowShapeListener(double arc) {
		this.arc = arc;
	}

	@Override
	public void componentResized(ComponentEvent e) {
		Window win = (Window) e.getSource();
		Frame frame = (win instanceof Frame) ? (Frame) win : null;

		if ((frame != null) && ((frame.getExtendedState() & Frame.MAXIMIZED_BOTH) != 0)) {
			AWTUtilities.setWindowShape(win, null);
		} else {
			/** 设置圆角 */
			AWTUtilities.setWindowShape(win, new RoundRectangle2D.Double(0.0D, 0.0D, win.getWidth(), win.getHeight(), arc, arc));
		}
	}
}