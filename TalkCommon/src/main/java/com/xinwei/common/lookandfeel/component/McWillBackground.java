package com.xinwei.common.lookandfeel.component;

import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class McWillBackground extends JLabel {
	private static final long serialVersionUID = 1L;

	public McWillBackground() {
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Rectangle bounds = getBounds();
		ImageIcon icon = (ImageIcon) getIcon();
		g.drawImage(icon.getImage(), bounds.x, bounds.y, bounds.width, bounds.height, this);
	}
}
