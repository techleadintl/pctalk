package com.xinwei.common.lookandfeel.util;

import java.awt.Color;

public class ColorUtil {

	private static final int _DEFAULT_STEP = 20;

	public static Color[] generateTransitionalColor(Color color1, Color color2) {
		return generateTransitionalColor(color1, color2, _DEFAULT_STEP);
	}

	public static Color[] generateTransitionalColor(Color color1, Color color2, int steps) {
		if (steps < 3)
			return new Color[] {};
		Color[] colors = new Color[steps];
		colors[0] = color1;
		colors[steps - 1] = color2;

		int redDiff = color2.getRed() - color1.getRed();
		int greenDiff = color2.getGreen() - color1.getGreen();
		int blueDiff = color2.getBlue() - color1.getBlue();
		for (int i = 1; i < steps - 1; i++) {
			int red = color1.getRed() + redDiff * i / steps;
			int green = color1.getGreen() + greenDiff * i / steps;
			int blue = color1.getBlue() + blueDiff * i / steps;
			colors[i] = new Color(red, green, blue);
		}

		return colors;
	}

	/**
	 * Returns the brightness of the specified color.
	 * 
	 * @param rgb
	 *            RGB object of a color.
	 * @return The brightness of the specified color.
	 */
	public static int getColorBrightness(int rgb) {
		int oldR = (rgb >>> 16) & 0xFF;
		int oldG = (rgb >>> 8) & 0xFF;
		int oldB = (rgb >>> 0) & 0xFF;

		return (222 * oldR + 707 * oldG + 71 * oldB) / 1000;
	}

	public static Color getColorBrightness(Color clor) {
		int rgb = clor.getRGB();
		int oldR = (rgb >>> 16) & 0xFF;
		int oldG = (rgb >>> 8) & 0xFF;
		int oldB = (rgb >>> 0) & 0xFF;

		return new Color((222 * oldR + 707 * oldG + 71 * oldB) / 1000);
	}

	public static float getColorStrength(Color color) {
		return Math.max(getColorBrightness(color.getRGB()), getColorBrightness(getNegativeColor(color.getRGB()))) / 255.0f;
	}

	public static Color invertColor(Color color) {
		return new Color(255 - color.getRed(), 255 - color.getGreen(), 255 - color.getBlue(), color.getAlpha());
	}

	/**
	 * Returns a negative of the specified color.
	 * 
	 * @param color
	 *            Color.
	 * @return Negative of the specified color.
	 */
	public static Color getNegativeColor(Color color) {
		return new Color(255 - color.getRed(), 255 - color.getGreen(), 255 - color.getBlue(), color.getAlpha());
	}

	/**
	 * Returns a negative of the specified color.
	 * 
	 * @param rgb
	 *            Color RGB.
	 * @return Negative of the specified color.
	 */
	public static int getNegativeColor(int rgb) {
		int transp = (rgb >>> 24) & 0xFF;
		int r = (rgb >>> 16) & 0xFF;
		int g = (rgb >>> 8) & 0xFF;
		int b = (rgb >>> 0) & 0xFF;

		return (transp << 24) | ((255 - r) << 16) | ((255 - g) << 8) | (255 - b);
	}

	/**
	 * Returns a translucent of the specified color.
	 * 
	 * @param color
	 *            Color.
	 * @param alpha
	 *            Alpha channel object.
	 * @return Translucent of the specified color that matches the requested alpha channel object.
	 */
	public static Color getAlphaColor(Color color, int alpha) {
		return new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha);
	}

	/**
	 * Returns saturated version of the specified color.
	 * 
	 * @param color
	 *            Color.
	 * @param factor
	 *            Saturation factor.
	 * @return Saturated color.
	 */
	public static Color getSaturatedColor(Color color, double factor) {
		float[] hsbvals = new float[3];
		Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), hsbvals);
		float sat = hsbvals[1];
		if (factor > 0.0) {
			sat = sat + (float) factor * (1.0f - sat);
		} else {
			sat = sat + (float) factor * sat;
		}
		return new Color(Color.HSBtoRGB(hsbvals[0], sat, hsbvals[2]));
	}

	/**
	 * Returns hue-shifted (in HSV space) version of the specified color.
	 * 
	 * @param color
	 *            Color.
	 * @param hueShift
	 *            hue shift factor.
	 * @return Hue-shifted (in HSV space) color.
	 */
	public static Color getHueShiftedColor(Color color, double hueShift) {
		float[] hsbvals = new float[3];
		Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), hsbvals);
		float hue = hsbvals[0];
		hue += hueShift;
		if (hue < 0.0)
			hue += 1.0;
		if (hue > 1.0)
			hue -= 1.0;
		return new Color(Color.HSBtoRGB(hue, hsbvals[1], hsbvals[2]));
	}

	public static boolean isLighterColor(Color color) {
		if (color == null)
			return true;
		int $R = color.getRed();
		int $G = color.getGreen();
		int $B = color.getBlue();
		double $value = $R * 0.299 + $G * 0.587 + $B * 0.114;
		if ($value > 128) {
			return true;
		} else {
			return false;
		}
	}

	public static int getGrayValue(Color c) {
		if (c == null) {
			return 0;
		}

		double r = c.getRed();
		double g = c.getGreen();
		double b = c.getBlue();
		return Math.min(255, (int) (r * 0.28 + g * 0.59 + b * 0.13));
	}

	public static Color getFontColor(Color color) {
		boolean lighterColor = isLighterColor(color);
		if (lighterColor)
			return new Color(38, 38, 38);
		else
			return new Color(222, 222, 222);
	}

	public static Color lighter(Color color, double fraction) {
		int red = (int) Math.round(color.getRed() * (1.0 + fraction));
		int green = (int) Math.round(color.getGreen() * (1.0 + fraction));
		int blue = (int) Math.round(color.getBlue() * (1.0 + fraction));

		if (red < 0)
			red = 0;
		else if (red > 255)
			red = 255;
		if (green < 0)
			green = 0;
		else if (green > 255)
			green = 255;
		if (blue < 0)
			blue = 0;
		else if (blue > 255)
			blue = 255;

		int alpha = color.getAlpha();

		return new Color(red, green, blue, alpha);
	}

	public static Color createColor(int r, int g, int b) {
		return new Color(((r & 0xFF) << 16) | ((g & 0xFF) << 8) | ((b & 0xFF)));
	}

	public static Color[] createColorArr(Color c1, Color c2, int steps) {
		if (c1 == null || c2 == null) {
			return null;
		}

		Color colors[] = new Color[steps];
		double r = c1.getRed();
		double g = c1.getGreen();
		double b = c1.getBlue();
		double dr = ((double) c2.getRed() - r) / steps;
		double dg = ((double) c2.getGreen() - g) / steps;
		double db = ((double) c2.getBlue() - b) / steps;
		colors[0] = c1;
		for (int i = 1; i < steps - 1; i++) {
			r += dr;
			g += dg;
			b += db;
			colors[i] = createColor((int) r, (int) g, (int) b);
		}
		colors[steps - 1] = c2;
		return colors;
	}

	public static Color brighter(Color c, double p) {
		if (c == null) {
			return null;
		}

		double r = c.getRed();
		double g = c.getGreen();
		double b = c.getBlue();

		double rd = 255.0 - r;
		double gd = 255.0 - g;
		double bd = 255.0 - b;

		r += (rd * p) / 100.0;
		g += (gd * p) / 100.0;
		b += (bd * p) / 100.0;
		return createColor((int) r, (int) g, (int) b);
	}

	public static Color darker(Color c, double p) {
		if (c == null) {
			return null;
		}

		double r = c.getRed();
		double g = c.getGreen();
		double b = c.getBlue();

		r -= (r * p) / 100.0;
		g -= (g * p) / 100.0;
		b -= (b * p) / 100.0;

		return createColor((int) r, (int) g, (int) b);
	}

	public static Color median(Color c1, Color c2) {
		if ((c1 == null || c2 == null)) {
			return null;
		}

		int r = (c1.getRed() + c2.getRed()) / 2;
		int g = (c1.getGreen() + c2.getGreen()) / 2;
		int b = (c1.getBlue() + c2.getBlue()) / 2;
		return createColor(r, g, b);
	}


	public static Color toGray(Color c) {
		if (c == null) {
			return null;
		}

		int gray = getGrayValue(c);
		return new Color(gray, gray, gray, c.getAlpha());
	}
}
