/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月16日 下午6:40:56
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;

public class McWillScrollPaneLabel extends JLabel {
	private String text;

	public McWillScrollPaneLabel() {
	}

	@Override
	public void setText(String text) {
		this.text = text;
		revalidate();
		repaint();
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (getText() != null && getText().length() > 0) {
			Insets insets = getInsets();
			String maxLengthString = getMaxLengthString(this, getText(), getWidth());
			g.drawString(maxLengthString, insets.left, (getHeight() - insets.top - insets.bottom) / 2 + g.getFontMetrics().getHeight() / 3);
		}
//		super.paintComponent(g);
	}

	public String getMaxLengthString(JComponent comp, String longString, int length) {
		char[] chars = longString.toCharArray();
		FontMetrics fontMetrics = comp.getFontMetrics(comp.getFont());
		if (fontMetrics.charsWidth(chars, 0, chars.length) < length) {
			return longString;
		} else {
			int i = 1;
			int l = 3 * fontMetrics.charWidth('.');
			length -= l + 3;
			while (true) {
				if (fontMetrics.charsWidth(chars, 0, i) > length || i >= chars.length) {
					break;
				}
				i++;
			}
			return longString.substring(0, i) + "...";
		}
	}

}
