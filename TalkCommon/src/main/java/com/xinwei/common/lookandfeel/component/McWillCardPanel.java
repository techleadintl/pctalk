/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月19日 下午3:34:07
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;

import com.xinwei.spark.TaskEngine;

@SuppressWarnings("serial")
public class McWillCardPanel extends JPanel {
	final CardLayout cardLayout = new CardLayout();

	private List<PropertyChangeListener> changeListeners = new ArrayList<PropertyChangeListener>();

	public McWillCardPanel() {
		setLayout(cardLayout);
	}

	public void previous() {
		previous(true);
	}

	private void firePropertyChanged(boolean c, int oldValue, int newValue) {
		if (!c) {
			return;
		}
		for (PropertyChangeListener changeListener : changeListeners) {
			changeListener.propertyChange(new PropertyChangeEvent(this, null, oldValue, newValue));

		}
	}

	public void previous(boolean c) {
		int oldIndex = getSelectedIndex();
		cardLayout.previous(this);
		int newIndex = getSelectedIndex();
		firePropertyChanged(c, oldIndex, newIndex);
	}

	public void next() {
		next(true);
	}

	public void next(boolean c) {
		int oldIndex = getSelectedIndex();
		cardLayout.next(this);
		int newIndex = getSelectedIndex();
		firePropertyChanged(c, oldIndex, newIndex);
	}

	public void first() {
		first(true);
	}

	public void first(boolean c) {
		int oldIndex = getSelectedIndex();
		cardLayout.first(this);
		int newIndex = getSelectedIndex();
		firePropertyChanged(c, oldIndex, newIndex);
	}

	public void last() {
		last(true);
	}

	public void last(boolean c) {
		int oldIndex = getSelectedIndex();
		cardLayout.last(this);
		int newIndex = getSelectedIndex();
		firePropertyChanged(c, oldIndex, newIndex);
	}

	public void show(String name) {
		show(name, true);
	}

	public void show(String name, boolean c) {
		if (name == null)
			return;
		int oldIndex = getSelectedIndex();
		cardLayout.show(this, name);
		int newIndex = getSelectedIndex();
		firePropertyChanged(c, oldIndex, newIndex);
	}

	public void addCard(String name, JComponent comp) {
		add(name, comp);
	}

	@Override
	public Component add(String name, Component comp) {
		Component add = super.add(name, comp);
		comp.setName(name);
		return add;
	}

	public String getSelectedName() {
		Component selectedComponent = getSelectedComponent();
		if (selectedComponent != null) {
			return selectedComponent.getName();
		}
		return null;
	}

	public int getSelectedIndex() {
		for (int i = 0; i < getComponentCount(); i++) {
			java.awt.Component c = getComponent(i);
			if (c.isVisible()) {
				return i;
			}
		}
		return -1;
	}

	public Component getSelectedComponent() {
		for (int i = 0; i < getComponentCount(); i++) {
			java.awt.Component c = getComponent(i);
			if (c.isVisible()) {
				return c;
			}
		}
		return null;
	}

	public void addCardChangeListener(PropertyChangeListener changeListener) {
		this.changeListeners.add(changeListener);
	}

	public static class CardChangeListener implements PropertyChangeListener {
		private static long SLEPP = 10;
		private int lastIndex = -1;

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			final int oldValue = (int) evt.getOldValue();
			final int newValue = (int) evt.getNewValue();

			final McWillCardPanel cardPanel = (McWillCardPanel) evt.getSource();

			if (oldValue == newValue)
				return;
			final int currentIndex = newValue;

			final JComponent component = (JComponent) cardPanel.getSelectedComponent();

			TaskEngine.getInstance().submit(new Runnable() {
				@Override
				public void run() {
					boolean vertical = false;
					long timeMillis = 200;
					Object clientProperty = component.getClientProperty("card-vertical");
					if (clientProperty != null) {
						vertical = Boolean.valueOf(clientProperty.toString());
					}
					clientProperty = component.getClientProperty("card-timeMillis");
					if (clientProperty instanceof Number) {
						timeMillis = ((Number) clientProperty).longValue();
					}

					Insets insets = cardPanel.getInsets();
					LayoutManager layout = component.getLayout();
					component.setLayout(null);
					int width = component.getWidth();
					int height = component.getHeight();

					long step = timeMillis / SLEPP;

					if (vertical) {
						long H = height / step == 0 ? (height / step + 1) : (height / step);

						int y = height + insets.bottom;
						if (currentIndex < lastIndex) {
							for (int i = 0; i < step && y > insets.top; i++) {
								component.setBounds(insets.left, y, width, height);
								y -= H;
								sleep();
							}
						} else {
							y = -height;
							for (int i = 0; i < step && y < insets.top; i++) {
								component.setBounds(insets.left, y, width, height);
								y += H;
								sleep();
							}
						}
					} else {
						long W = width / step == 0 ? (width / step + 1) : (width / step);

						int x = width + insets.right;
						if (currentIndex < lastIndex) {
							for (int i = 0; i < step && x > insets.left; i++) {
								component.setBounds(x, insets.top, width, height);
								x -= W;
								sleep();
							}
						} else {
							x = -width;
							for (int i = 0; i < step && x < insets.left; i++) {
								component.setBounds(x, insets.top, width, height);
								x += W;
								sleep();
							}
						}
					}
					component.setBounds(insets.left, insets.top, width, height);
					lastIndex = currentIndex;
					component.setLayout(layout);
				}
			});

		}

		private void sleep() {
			try {
				Thread.sleep(SLEPP);
			} catch (InterruptedException e) {
			}
		}

	}
}
