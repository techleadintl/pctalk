/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年2月10日 下午3:16:36
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.HeadlessException;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import sun.swing.FilePane;

public class McWillFileDialog extends JFileChooser {
	public void updateUI() {
		LookAndFeel old = UIManager.getLookAndFeel();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable ex) {
			old = null;
		}

		super.updateUI();

		if (old != null) {
			try {
				UIManager.setLookAndFeel(old);
			} catch (UnsupportedLookAndFeelException ignored) {
			} // shouldn't get here
		}
	}

	protected JDialog createDialog(Component parent) throws HeadlessException {
		LookAndFeel old = UIManager.getLookAndFeel();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable ex) {
			old = null;
		}
		JDialog dialog = super.createDialog(parent);
		if (old != null) {
			try {
				UIManager.setLookAndFeel(old);
			} catch (UnsupportedLookAndFeelException ignored) {
			}
		}
		return dialog;
	}

	@Override
	protected void paintComponent(Graphics g) {
		setChildOpaque(this);
		super.paintComponent(g);
	}

	private void setChildOpaque(Component p) {
		Component[] components = null;
		if (p instanceof JComponent) {
			components = ((JComponent) p).getComponents();
			((JComponent) p).setOpaque(true);
		} else if (p instanceof Container) {
			components = ((Container) p).getComponents();
		}
		if (components == null)
			return;

		for (Component c : components) {
			setChildOpaque(c);
		}
	}

	private FilePane findFilePane(Container parent) {
		for (Component comp : parent.getComponents()) {
			if (FilePane.class.isInstance(comp)) {
				return (FilePane) comp;
			}
			if (comp instanceof Container) {
				Container cont = (Container) comp;
				if (cont.getComponentCount() > 0) {
					FilePane found = findFilePane(cont);
					if (found != null) {
						return found;
					}
				}
			}
		}

		return null;
	}
}
