package com.xinwei.common.lookandfeel.ui;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;

public class McWillCheckBoxMenuItemUI extends McWillMenuItemUI {

    public static ComponentUI createUI(JComponent c) {
        return new McWillCheckBoxMenuItemUI();
    }
    protected void installDefaults() {
        super.installDefaults();
        checkIcon = UIManager.getIcon("CheckBoxMenuItem.checkIcon");
    }
}