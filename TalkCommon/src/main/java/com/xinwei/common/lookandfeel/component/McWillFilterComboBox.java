package com.xinwei.common.lookandfeel.component;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ListDataEvent;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;

import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.component.McWillFilterComboBox.McWillFilterComboBoxUI.FilterComboPopup;
import com.xinwei.common.lookandfeel.ui.McWillComboBoxUI;

/**
 * 自动过滤下拉框
 *
 */
@SuppressWarnings("serial")
public class McWillFilterComboBox<T> extends JComboBox {

	/**
	 * 显示用模型
	 */
	protected DefaultComboBoxModel showModel;

	private boolean selectingItem;

	/**
	 * 创建一个 <code>JFilterComboBox</code>， 其项取自现有的 <code>ComboBoxModel</code>。
	 * 由于提供了 <code>ComboBoxModel</code>， 使用此构造方法创建的组合框不创建默认组合框模型，
	 * 这可能影响插入、移除和添加方法的行为方式。
	 */
	public McWillFilterComboBox(ComboBoxModel aModel) {
		super(aModel);
		initialize();
	}

	/**
	 * 创建包含指定数组中的元素的 <code>JFilterComboBox</code>。
	 * 默认情况下，选择数组中的第一项（因而也选择了该项的数据模型）。
	 * 
	 * @param items
	 *            - 要插入到组合框的对象数组
	 */
	public McWillFilterComboBox(final Object items[]) {
		super(items);
		initialize();
	}

	/**
	 * 创建包含指定 <code>Vector</code> 中的元素的 <code>JFilterComboBox</code>。
	 * 默认情况下，选择数组中的第一项（因而也选择了该项的数据模型）。
	 * 
	 * @param items
	 *            - 要插入到组合框的向量数组
	 */
	public McWillFilterComboBox(Vector<?> items) {
		super(items);
		initialize();
	}

	/**
	 * 创建具有默认数据模型的 <code>JFilterComboBox</code>。 默认的数据模型为空对象列表。使用
	 * <code>addItem</code> 添加项。 默认情况下，选择数据模型中的第一项。
	 */
	public McWillFilterComboBox() {
		super();
		initialize();
	}

	private void initialize() {
		showModel.addListDataListener(this);

		setBorder(BorderFactory.createEmptyBorder());
	}

	@Override
	public void updateUI() {
		setUI(new McWillFilterComboBoxUI());
		ListCellRenderer renderer = getRenderer();
		if (renderer instanceof Component) {
			SwingUtilities.updateComponentTreeUI((Component) renderer);
		}
	}

	@Override
	public Object getSelectedItem() {
		return showModel.getSelectedItem();
	}

	@Override
	public void setSelectedItem(Object anObject) {
		Object oldSelection = selectedItemReminder;
		Object objectToSelect = anObject;
		if (oldSelection == null || !oldSelection.equals(anObject)) {

			if (anObject != null && !isEditable()) {
				boolean found = false;
				for (int i = 0; i < showModel.getSize(); i++) {
					Object element = showModel.getElementAt(i);
					if (anObject.equals(element)) {
						found = true;
						objectToSelect = element;
						break;
					}
				}
				if (!found) {
					return;
				}
			}

			selectingItem = true;
			showModel.setSelectedItem(objectToSelect);
			selectingItem = false;

			if (selectedItemReminder != showModel.getSelectedItem()) {
				selectedItemChanged();
			}
		}
		fireActionEvent();
	}

	@Override
	public void setSelectedIndex(int anIndex) {
		int size = showModel.getSize();
		if (anIndex == -1 || size == 0) {
			setSelectedItem(null);
		} else if (anIndex < -1) {
			throw new IllegalArgumentException("setSelectedIndex: " + anIndex + " out of bounds");
		} else if (anIndex >= size) {
			setSelectedItem(showModel.getElementAt(size - 1));
		} else {
			setSelectedItem(showModel.getElementAt(anIndex));
		}
	}

	@Override
	public int getSelectedIndex() {
		Object sObject = showModel.getSelectedItem();
		int i, c;
		Object obj;

		for (i = 0, c = showModel.getSize(); i < c; i++) {
			obj = showModel.getElementAt(i);
			if (obj != null && obj.equals(sObject))
				return i;
		}
		return -1;
	}

	@Override
	public void contentsChanged(ListDataEvent e) {
		Object oldSelection = selectedItemReminder;
		Object newSelection = showModel.getSelectedItem();
		if (oldSelection == null || !oldSelection.equals(newSelection)) {
			selectedItemChanged();
			if (!selectingItem) {
				fireActionEvent();
			}
		}
	}

	@Override
	protected void selectedItemChanged() {
		if (selectedItemReminder != null) {
			fireItemStateChanged(
					new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, selectedItemReminder, ItemEvent.DESELECTED));
		}

		selectedItemReminder = showModel.getSelectedItem();

		if (selectedItemReminder != null) {
			fireItemStateChanged(
					new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, selectedItemReminder, ItemEvent.SELECTED));
		}
	}

	@Override
	public void intervalAdded(ListDataEvent e) {
		if (selectedItemReminder != showModel.getSelectedItem()) {
			selectedItemChanged();
		}
	}

	@Override
	public void setEditable(boolean aFlag) {
		super.setEditable(true);
	}

	/**
	 * 返回显示用模型
	 * 
	 * @return
	 */
	public DefaultComboBoxModel getShowModel() {
		if (showModel == null) {
			showModel = new DefaultComboBoxModel();
		}
		return showModel;
	}

	protected JButton createXButton() {
		JButton button = new McWillIconButton(getPopIcons());// 创建对象
		button.setOpaque(false);
		button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		return button;
	}

	protected JTextField createXTextField() {
		JTextField editor = new JTextField("", 9);
		editor.setBorder(null);
		return editor;
	}

	protected JList createXList() {
		JList list = new McWillList(getModel()) {
			public void processMouseEvent(MouseEvent e) {
				if ((e.getModifiers() & Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()) != 0) {
					Toolkit toolkit = Toolkit.getDefaultToolkit();
					e = new MouseEvent((Component) e.getSource(), e.getID(), e.getWhen(),
							e.getModifiers() ^ toolkit.getMenuShortcutKeyMask(), e.getX(), e.getY(), e.getXOnScreen(),
							e.getYOnScreen(), e.getClickCount(), e.isPopupTrigger(), MouseEvent.NOBUTTON);
				}
				super.processMouseEvent(e);
			}
		};
		list.setModel(getShowModel());
		return list;
	}

	protected ImageIcon[] getPopIcons() {
		return null;
	}

	/**
	 * 按关键字进行查询，该方法中，可以自行加入各种查询算法
	 */
	public void findMatchs() {
		ComboBoxModel model = getModel();
		DefaultComboBoxModel showModel = getShowModel();
		showModel.removeAllElements();

		for (int i = 0; i < model.getSize(); i++) {
			if (isMatch(textField.getText(), model.getElementAt(i))) {
				showModel.addElement(model.getElementAt(i));
			}
		}
		if (showModel.getSize() == 0) {
			doNoMatchs(showModel);
		}
		comboPopup.repaint();

		firePopupMenuWillBecomeVisible();
		int selectedIndex = getSelectedIndex();
		if (selectedIndex == -1) {
			xlist.clearSelection();
		} else {
			xlist.setSelectedIndex(selectedIndex);
			xlist.ensureIndexIsVisible(selectedIndex);
		}

		Point location = comboPopup.getXPopupLocation();
		comboPopup.pack();
		comboPopup.show(this, location.x, location.y - 3);
	}

	public void selectedItem(Object obj) {
		ComboBoxModel model = getModel();
		DefaultComboBoxModel showModel = getShowModel();
		showModel.removeAllElements();

		showModel.addElement(obj);
		textField.setText(obj.toString());

		comboPopup.repaint();
	}

	protected void doNoMatchs(DefaultComboBoxModel showModel) {
	}

	public boolean isMatch(String text, Object element) {
		return element.toString().indexOf(text) >= 0;
	}

	FilterComboPopup comboPopup;

	JTextField textField;

	JList xlist;

	class McWillFilterComboBoxUI extends McWillComboBoxUI {

		/**
		 * 编辑区事件监听器
		 */
		protected EditorListener editorListener;
		/**
		 * 该 UI 类负责绘制的控件
		 */
		protected McWillFilterComboBox filterComboBox;

		@Override
		public void installUI(JComponent c) {
			filterComboBox = (McWillFilterComboBox) c;
			filterComboBox.setEditable(true);
			super.installUI(c);
		}

		@Override
		public JButton createArrowButton() {
			return createXButton();
		}

		@Override
		protected ComboBoxEditor createEditor() {
			return new BasicComboBoxEditor.UIResource() {
				protected JTextField createEditorComponent() {
					textField = createXTextField();
					return textField;
				}
			};
		}

		@Override
		public void configureEditor() {
			super.configureEditor();
			editor.addKeyListener(getEditorListener());
			editor.addMouseListener(getEditorListener());
			editor.addFocusListener(getEditorListener());
		}

		@Override
		public void unconfigureEditor() {
			super.unconfigureEditor();
			if (editorListener != null) {
				editor.removeKeyListener(editorListener);
				editor.removeMouseListener(editorListener);
				editor.removeFocusListener(editorListener);
				editorListener = null;
			}
		}

		@Override
		protected ComboPopup createPopup() {
			comboPopup = new FilterComboPopup(filterComboBox);
			return comboPopup;
		}

		/**
		 * 初始化并返回编辑区事件监听器
		 * 
		 * @return
		 */
		protected EditorListener getEditorListener() {
			if (editorListener == null) {
				editorListener = new EditorListener();
			}
			return editorListener;
		}

		/**
		 * 返回编辑区文本
		 * 
		 * @return
		 */
		private String getEditorText() {
			return filterComboBox.getEditor().getItem().toString();
		}

		/**
		 * 弹出面板类
		 * 
		 * 
		 *
		 */
		class FilterComboPopup extends BasicComboPopup {

			public FilterComboPopup(JComboBox combo) {
				super(combo);
			}

			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				McWillTheme.setOpaque(this);
			}

			@Override
			protected JList createList() {
				xlist = createXList();
				return xlist;
			}

			@Override
			public void setVisible(boolean b) {
				super.setVisible(b);
				if (!b) {
					comboBox.getEditor().setItem(comboBox.getSelectedItem());
				}
			}

			public Point getXPopupLocation() {
				Dimension popupSize = comboBox.getSize();
				Insets insets = getInsets();

				// reduce the width of the scrollpane by the insets so that the
				// popup
				// is the same width as the combo box.
				popupSize.setSize(popupSize.width - (insets.right + insets.left),
						getPopupHeightForRowCount(comboBox.getMaximumRowCount()));
				Rectangle popupBounds = computePopupBounds(0, comboBox.getBounds().height, popupSize.width,
						popupSize.height);

				Dimension scrollSize = popupBounds.getSize();
				Point popupLocation = popupBounds.getLocation();

				scroller.setMaximumSize(scrollSize);
				scroller.setPreferredSize(scrollSize);
				scroller.setMinimumSize(scrollSize);

				list.revalidate();

				return popupLocation;
			}

			@Override
			public void show() {
				findMatchs();
				// comboBox.firePopupMenuWillBecomeVisible();
				// int selectedIndex = comboBox.getSelectedIndex();
				// if (selectedIndex == -1) {
				// list.clearSelection();
				// } else {
				// list.setSelectedIndex(selectedIndex);
				// list.ensureIndexIsVisible(selectedIndex);
				// }
				//
				// Point location = getPopupLocation();
				// show(comboBox, location.x, location.y);
			}

			protected int getPopupHeightForRowCount(int maxRowCount) {
				// Set the cached value of the minimum row count
				int minRowCount = Math.min(maxRowCount, showModel.getSize());
				int height = 0;
				ListCellRenderer renderer = list.getCellRenderer();
				Object value = null;
				for (int i = 0; i < minRowCount; ++i) {
					value = showModel.getElementAt(i);
					Component c = renderer.getListCellRendererComponent(list, value, i, false, false);
					height += c.getPreferredSize().height;
				}

				if (height == 0) {
					height = comboBox.getHeight();
				}

				Border border = scroller.getViewportBorder();
				if (border != null) {
					Insets insets = border.getBorderInsets(null);
					height += insets.top + insets.bottom;
				}

				border = scroller.getBorder();
				if (border != null) {
					Insets insets = border.getBorderInsets(null);
					height += insets.top + insets.bottom;
				}

				return height;
			}

		}

		/**
		 * 编辑区事件监听器类
		 * 
		 */
		class EditorListener implements KeyListener, MouseListener, FocusListener {

			/**
			 * 旧文本，用于键盘输入时的比对
			 */
			private String oldText = "";

			@Override
			public void keyReleased(KeyEvent e) {
				String newText = getEditorText();
				if (!newText.equals(oldText)) {
					findMatchs();
				}
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					if (isPopupVisible(filterComboBox)) {
						setPopupVisible(filterComboBox, false);
					}
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
				oldText = getEditorText();
				if (!isPopupVisible(filterComboBox)) {
					setPopupVisible(filterComboBox, true);
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
				findMatchs();
			}

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
				if (!isPopupVisible(filterComboBox)) {
					setPopupVisible(filterComboBox, true);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void focusGained(FocusEvent e) {
				if (!isPopupVisible(filterComboBox)) {
					setPopupVisible(filterComboBox, true);
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
			}

		}

	}

}