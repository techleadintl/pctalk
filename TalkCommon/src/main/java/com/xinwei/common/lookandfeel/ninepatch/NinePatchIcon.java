package com.xinwei.common.lookandfeel.ninepatch;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.xinwei.common.lookandfeel.ninepatch.NinePatchInterval.NinePatchIntervalType;
import com.xinwei.common.lookandfeel.util.ImageUtil;

public class NinePatchIcon implements Icon {
	private Component component;
	private BufferedImage image;
	private List<NinePatchInterval> horizontalStretch;
	private List<NinePatchInterval> verticalStretch;
	private Insets margin;
	private Integer cachedWidth0;
	private Integer cachedWidth1;
	private Integer cachedHeight0;
	private Integer cachedHeight1;

	public NinePatchIcon(URL paramURL) {
		this(paramURL, null);
	}

	public NinePatchIcon(URL paramURL, Component paramComponent) {
		this(ImageUtil.getBufferedImage(paramURL), paramComponent);
	}

	public NinePatchIcon(String paramString) {
		this(paramString, null);
	}

	public NinePatchIcon(String paramString, Component paramComponent) {
		this(ImageUtil.getBufferedImage(paramString), paramComponent);
	}

	public NinePatchIcon(ImageIcon paramImageIcon) {
		this(paramImageIcon, null);
	}

	public NinePatchIcon(ImageIcon paramImageIcon, Component paramComponent) {
		this(ImageUtil.getBufferedImage(paramImageIcon), paramComponent);
	}

	public NinePatchIcon(Image paramImage) {
		this(paramImage, null);
	}

	public NinePatchIcon(Image paramImage, Component paramComponent) {
		this(ImageUtil.getBufferedImage(paramImage), paramComponent);
	}

	public NinePatchIcon(BufferedImage paramBufferedImage) {
		this(paramBufferedImage, null);
	}

	public NinePatchIcon(BufferedImage paramBufferedImage, Component paramComponent) {
		this(paramBufferedImage, paramComponent, false);
	}

	private NinePatchIcon(BufferedImage paramBufferedImage, Component paramComponent, boolean paramBoolean) {
		if (paramBoolean) {
			this.component = paramComponent;
			this.image = paramBufferedImage;
			this.horizontalStretch = new ArrayList<NinePatchInterval>();
			this.verticalStretch = new ArrayList<NinePatchInterval>();
		} else {
			if ((paramBufferedImage.getWidth() < 3) || (paramBufferedImage.getHeight() < 3))
				throw new IllegalArgumentException("Buffered image must be atleast 3x3 pixels size");
			this.component = paramComponent;
			int i = paramBufferedImage.getWidth() - 2;
			int j = paramBufferedImage.getHeight() - 2;
			this.image = ImageUtil.createCompatibleImage(paramBufferedImage, i, j);
			Graphics2D localGraphics2D = this.image.createGraphics();
			localGraphics2D.drawImage(paramBufferedImage, 0, 0, i, j, 1, 1, paramBufferedImage.getWidth() - 1,
					paramBufferedImage.getHeight() - 1, null);
			localGraphics2D.dispose();
			this.horizontalStretch = parseIntervals(paramBufferedImage, NinePatchIntervalType.horizontalStretch);
			this.verticalStretch = parseIntervals(paramBufferedImage, NinePatchIntervalType.verticalStretch);
			if (((this.horizontalStretch.size() <= 1) && ((this.horizontalStretch.size() != 1) || (((NinePatchInterval) this.horizontalStretch
					.get(0)).isPixel())))
					|| ((this.verticalStretch.size() <= 1) && ((this.verticalStretch.size() != 1) || (((NinePatchInterval) this.verticalStretch
							.get(0)).isPixel()))))
				throw new IllegalArgumentException("There must be stretch constraints specified on image");
			List<NinePatchInterval> localList1 = parseIntervals(paramBufferedImage, NinePatchIntervalType.verticalContent);
			List<NinePatchInterval> localList2 = parseIntervals(paramBufferedImage, NinePatchIntervalType.horizontalContent);
			int k = localList1.size() == 0 ? 0 : ((NinePatchInterval) localList1.get(0)).getStart();
			int m = localList1.size() == 0 ? 0 : this.image.getHeight() - ((NinePatchInterval) localList1.get(0)).getEnd() - 1;
			int n = localList2.size() == 0 ? 0 : ((NinePatchInterval) localList2.get(0)).getStart();
			int i1 = localList2.size() == 0 ? 0 : this.image.getWidth() - ((NinePatchInterval) localList2.get(0)).getEnd() - 1;
			this.margin = new Insets(k, n, m, i1);
			calculateFixedPixelsWidth(true);
			calculateFixedPixelsWidth(false);
			calculateFixedPixelsHeight(true);
			calculateFixedPixelsHeight(false);
		}
	}

	public static NinePatchIcon create(BufferedImage paramBufferedImage) {
		return new NinePatchIcon(paramBufferedImage, null, true);
	}

	public BufferedImage getImage() {
		return this.image;
	}

	public Component getComponent() {
		return this.component;
	}

	public void setComponent(Component paramComponent) {
		this.component = paramComponent;
	}

	public List<NinePatchInterval> getHorizontalStretch() {
		return this.horizontalStretch;
	}

	public void setHorizontalStretch(List<NinePatchInterval> paramList) {
		this.horizontalStretch = paramList;
		clearCachedWidthData();
	}

	public void addHorizontalStretch(NinePatchInterval paramNinePatchInterval) {
		this.horizontalStretch.add(paramNinePatchInterval);
		clearCachedWidthData();
	}

	public List<NinePatchInterval> getVerticalStretch() {
		return this.verticalStretch;
	}

	public void setVerticalStretch(List<NinePatchInterval> paramList) {
		this.verticalStretch = paramList;
		clearCachedHeightData();
	}

	public void addVerticalStretch(NinePatchInterval paramNinePatchInterval) {
		this.verticalStretch.add(paramNinePatchInterval);
		clearCachedHeightData();
	}

	public Insets getMargin() {
		return this.margin;
	}

	public void setMargin(Insets paramInsets) {
		this.margin = paramInsets;
	}

	public void paintIcon(Component paramComponent, Graphics paramGraphics, int paramInt1, int paramInt2) {
		paintIcon((Graphics2D) paramGraphics, 0, 0, paramComponent.getWidth(), paramComponent.getHeight());
	}

	public void paintIcon(Graphics2D paramGraphics2D, Rectangle paramRectangle) {
		paintIcon(paramGraphics2D, paramRectangle.x, paramRectangle.y, paramRectangle.width, paramRectangle.height);
	}

	public void paintIcon(Graphics2D paramGraphics2D, int x, int y, int width, int height) {
		int i = Math.max(width, getIconWidth());
		int j = Math.max(height, getIconHeight());
		int k = getFixedPixelsWidth(false);
		int m = i - k;
		int n = getFixedPixelsHeight(false);
		int i1 = j - n;
		int i2 = y;
		for (int i3 = 0; i3 < this.verticalStretch.size(); i3++) {
			NinePatchInterval localNinePatchInterval1 = (NinePatchInterval) this.verticalStretch.get(i3);
			int i4 = localNinePatchInterval1.getEnd() - localNinePatchInterval1.getStart() + 1;
			int i5;
			if (localNinePatchInterval1.isPixel()) {
				i5 = i4;
			} else {
				float f1 = i4 / (this.image.getHeight() - n);
				i5 = Math.round(f1 * i1);
			}
			int i6 = x;
			for (int i7 = 0; i7 < this.horizontalStretch.size(); i7++) {
				NinePatchInterval localNinePatchInterval2 = (NinePatchInterval) this.horizontalStretch.get(i7);
				int i8 = localNinePatchInterval2.getEnd() - localNinePatchInterval2.getStart() + 1;
				int i9;
				if (localNinePatchInterval2.isPixel()) {
					i9 = i8;
				} else {
					float f2 = i8 / (this.image.getWidth() - k);
					i9 = Math.round(f2 * m);
				}
				paramGraphics2D.drawImage(this.image, i6, i2, i6 + i9, i2 + i5, localNinePatchInterval2.getStart(),
						localNinePatchInterval1.getStart(), localNinePatchInterval2.getStart() + i8, localNinePatchInterval1.getStart()
								+ i4, null);
				i6 += i9;
			}
			i2 += i5;
		}
	}

	public int getFixedPixelsWidth(boolean paramBoolean) {
		if (paramBoolean) {
			if (this.cachedWidth0 == null)
				this.cachedWidth0 = Integer.valueOf(calculateFixedPixelsWidth(paramBoolean));
			return this.cachedWidth0.intValue();
		}
		if (this.cachedWidth1 == null)
			this.cachedWidth1 = Integer.valueOf(calculateFixedPixelsWidth(paramBoolean));
		return this.cachedWidth1.intValue();
	}

	private int calculateFixedPixelsWidth(boolean paramBoolean) {
		int i = this.image.getWidth();
		Iterator<NinePatchInterval> localIterator = this.horizontalStretch.iterator();
		while (localIterator.hasNext()) {
			NinePatchInterval localNinePatchInterval = (NinePatchInterval) localIterator.next();
			if (!localNinePatchInterval.isPixel()) {
				i -= localNinePatchInterval.getEnd() - localNinePatchInterval.getStart() + 1;
				if (paramBoolean)
					i++;
			}
		}
		return i;
	}

	public int getFixedPixelsHeight(boolean paramBoolean) {
		if (paramBoolean) {
			if (this.cachedHeight0 == null)
				this.cachedHeight0 = Integer.valueOf(calculateFixedPixelsHeight(paramBoolean));
			return this.cachedHeight0.intValue();
		}
		if (this.cachedHeight1 == null)
			this.cachedHeight1 = Integer.valueOf(calculateFixedPixelsHeight(paramBoolean));
		return this.cachedHeight1.intValue();
	}

	private int calculateFixedPixelsHeight(boolean paramBoolean) {
		int i = this.image.getHeight();
		Iterator<NinePatchInterval> localIterator = this.verticalStretch.iterator();
		while (localIterator.hasNext()) {
			NinePatchInterval localNinePatchInterval = (NinePatchInterval) localIterator.next();
			if (!localNinePatchInterval.isPixel()) {
				i -= localNinePatchInterval.getEnd() - localNinePatchInterval.getStart() + 1;
				if (paramBoolean)
					i++;
			}
		}
		return i;
	}

	private void clearCachedWidthData() {
		this.cachedWidth0 = null;
		this.cachedWidth1 = null;
	}

	private void clearCachedHeightData() {
		this.cachedHeight0 = null;
		this.cachedHeight1 = null;
	}

	public int getIconWidth() {
		return Math.max(this.component != null ? this.component.getWidth() : 0, getFixedPixelsWidth(true));
	}

	public int getIconHeight() {
		return Math.max(this.component != null ? this.component.getHeight() : 0, getFixedPixelsHeight(true));
	}

	public Dimension getPreferredSize() {
		return new Dimension(getFixedPixelsWidth(true), getFixedPixelsHeight(true));
	}

	public Dimension getRealImageSize() {
		return new Dimension(getImage().getWidth(), getImage().getHeight());
	}

	public static List<NinePatchInterval> parseIntervals(BufferedImage paramBufferedImage, NinePatchIntervalType paramNinePatchIntervalType) {
		ArrayList<NinePatchInterval> localArrayList = new ArrayList<NinePatchInterval>();
		NinePatchInterval localNinePatchInterval = null;
		for (int i = 1; i < ((paramNinePatchIntervalType.equals(NinePatchIntervalType.horizontalStretch))
				|| (paramNinePatchIntervalType.equals(NinePatchIntervalType.horizontalContent)) ? paramBufferedImage.getWidth()
					: paramBufferedImage.getHeight()) - 1; i++) {
			int j;
			switch (paramNinePatchIntervalType.ordinal() + 1) {
			case 1:
				j = paramBufferedImage.getRGB(i, 0);
				break;
			case 2:
				j = paramBufferedImage.getRGB(0, i);
				break;
			case 3:
				j = paramBufferedImage.getRGB(i, paramBufferedImage.getHeight() - 1);
				break;
			case 4:
				j = paramBufferedImage.getRGB(paramBufferedImage.getWidth() - 1, i);
				break;
			default:
				j = 0;
			}
			boolean bool = j != Color.BLACK.getRGB();
			if (localNinePatchInterval == null) {
				localNinePatchInterval = new NinePatchInterval(i - 1, i - 1, bool);
			} else if (bool == localNinePatchInterval.isPixel()) {
				localNinePatchInterval.setEnd(i - 1);
			} else {
				if (bool == localNinePatchInterval.isPixel())
					continue;
				if ((paramNinePatchIntervalType.equals(NinePatchIntervalType.horizontalStretch))
						|| (paramNinePatchIntervalType.equals(NinePatchIntervalType.verticalStretch))
						|| (!localNinePatchInterval.isPixel()))
					localArrayList.add(localNinePatchInterval);
				localNinePatchInterval = new NinePatchInterval(i - 1, i - 1, bool);
			}
		}
		if ((localNinePatchInterval != null)
				&& ((paramNinePatchIntervalType.equals(NinePatchIntervalType.horizontalStretch))
						|| (paramNinePatchIntervalType.equals(NinePatchIntervalType.verticalStretch)) || (!localNinePatchInterval.isPixel())))
			localArrayList.add(localNinePatchInterval);
		return localArrayList;
	}
}