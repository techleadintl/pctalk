package com.xinwei.common.lookandfeel.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;

import com.jtattoo.plaf.BaseMenuItemUI;
import com.xinwei.common.lookandfeel.McWillTheme;

public class McWillMenuItemColorUI extends BaseMenuItemUI {
	Color selectColor;

	public McWillMenuItemColorUI() {
	}

	protected void paintBackground(Graphics g, JComponent c, int x, int y, int w, int h) {
		if (menuItem.isSelected() && menuItem.isArmed() && selectColor != null) {
			g.setColor(selectColor);
		} else {
			g.setColor(McWillTheme.getThemeColor().getSubLightColor());
		}
		g.fillRect(x, y, w, h);
	}

	protected void paintText(Graphics g, JMenuItem menuItem, Rectangle textRect, String text) {
		textRect.x += 15;
		super.paintText(g, menuItem, textRect, text);
	}

	protected Dimension getPreferredMenuItemSize(JComponent c, Icon checkIcon, Icon arrowIcon, int defaultTextIconGap) {
		Dimension size = super.getPreferredMenuItemSize(c, checkIcon, arrowIcon, defaultTextIconGap);
		if (size.width < 90) {
			return new Dimension(size.width + 50, size.height);
		}
		if (size.width > 190)
			return new Dimension(size.width + 10, size.height);

		return new Dimension(size.width + 30, size.height);
	}
}
