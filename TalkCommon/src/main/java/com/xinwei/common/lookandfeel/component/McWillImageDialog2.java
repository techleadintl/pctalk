/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年11月21日 下午1:56:59
 * 
 ***************************************************************/
package com.xinwei.common.lookandfeel.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Window;

import javax.accessibility.AccessibleContext;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JRootPane;
import javax.swing.UIManager;

import com.xinwei.talk.ui.skin.McWillSkinContentPanel;
import com.xinwei.talk.ui.skin.skin.filechooser.GenericFileFilter;
import com.xinwei.talk.ui.skin.skin.filechooser.GenericFileView;
import com.xinwei.talk.ui.skin.skin.filechooser.ImagePreviewPanel;

public class McWillImageDialog2 extends JFileChooser {
	private Window parentDialog;

	public McWillImageDialog2() {
		this(null);
	}

	public McWillImageDialog2(Window parentDialog) {
		this(parentDialog, true);
	}

	public McWillImageDialog2(Window parentDialog, boolean flag) {
		this.parentDialog = parentDialog;

		setAcceptAllFileFilterUsed(false);

		ImagePreviewPanel preview = new ImagePreviewPanel();
		preview.setOpaque(false);
		setAccessory(preview);
		addPropertyChangeListener(preview);
		setFileSelectionMode(JFileChooser.FILES_ONLY);

		// add Filters
		if (flag) {
			String imageExts[] = { "bmp", "png", "gif", "jpg", "jpeg" };

			GenericFileFilter filter = new GenericFileFilter(imageExts, getDescriptionPrefix() + "(*.jpg;*.jpeg;*.png;*.gif;*.bmp;)");
			addChoosableFileFilter(filter);
			setFileView(new GenericFileView(imageExts, GenericFileView.IMAGE_FILEVIEW));
		}
	}

	protected String getDescriptionPrefix() {
		return "Images Files";
	}

	protected JDialog createDialog(Component parent) throws HeadlessException {
		String title = getUI().getDialogTitle(this);
		putClientProperty(AccessibleContext.ACCESSIBLE_DESCRIPTION_PROPERTY, title);
		JDialog dialog = null;
		if (parentDialog instanceof JFrame)
			dialog = new JDialog((JFrame) parentDialog, title, true);
		else if (parentDialog instanceof JDialog)
			dialog = new JDialog((JDialog) parentDialog, title, true);
		else {
			dialog = new JDialog();
		}
		dialog.setComponentOrientation(this.getComponentOrientation());
		McWillSkinContentPanel mcWillSkinContentPanel = new McWillSkinContentPanel();
		dialog.setContentPane(mcWillSkinContentPanel);
		mcWillSkinContentPanel.add(this, BorderLayout.CENTER);
		if (JDialog.isDefaultLookAndFeelDecorated()) {
			boolean supportsWindowDecorations = UIManager.getLookAndFeel().getSupportsWindowDecorations();
			if (supportsWindowDecorations) {
				dialog.getRootPane().setWindowDecorationStyle(JRootPane.FILE_CHOOSER_DIALOG);
			}
		}
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		return dialog;
	}

	@Override
	protected void paintComponent(Graphics g) {
		setChildOpaque(this);
		super.paintComponent(g);
	}

	private void setChildOpaque(Component p) {
		Component[] components = null;
		if (p instanceof JComponent) {
			components = ((JComponent) p).getComponents();
			((JComponent) p).setOpaque(false);
		} else if (p instanceof Container) {
			components = ((Container) p).getComponents();
		}
		if (components == null)
			return;

		for (Component c : components) {
			setChildOpaque(c);
		}
	}
}
