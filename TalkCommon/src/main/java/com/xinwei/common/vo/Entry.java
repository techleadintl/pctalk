/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年10月25日 上午10:28:27
 * 
 ***************************************************************/
package com.xinwei.common.vo;

import java.io.Serializable;

public class Entry<M, N> implements Serializable {
	private M key;
	private N value;

	public Entry(M key, N value) {
		this.key = key;
		this.value = value;
	}

	public M getKey() {
		return key;
	}

	public void setKey(M key) {
		this.key = key;
	}

	public N getValue() {
		return value;
	}

	public void setValue(N value) {
		this.value = value;
	}

	@Override
	public String toString() {
		if (value != null) {
			return String.valueOf(value);
		} else {
			return null;
		}
	}

}
