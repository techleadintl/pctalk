package com.xinwei.common.vo;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({ "rawtypes", "serial" })
public class McWillMap<K, V> extends LinkedHashMap<K, V> {
	public void set(K key, V obj) {
		put(key, obj);
	}

	public String getString(K key) {
		return getMapString(this, key);
	}

	public Boolean getBoolean(K key) {
		return getMapBoolean(this, key);
	}

	public Byte getByte(K key) {
		return getMapByte(this, key);
	}

	public Short getShort(K key) {
		return getMapShort(this, key);
	}

	public Integer getInteger(K key) {
		return getMapInteger(this, key);
	}

	public Long getLong(K key) {
		return getMapLong(this, key);
	}

	public Float getFloat(K key) {
		return getMapFloat(this, key);
	}

	public Double getDouble(K key) {
		return getMapDouble(this, key);
	}

	public Map getMap(Object key) {
		return getMapMap(this, key);
	}

	public List getList(Object key) {
		return getMapList(this, key);
	}

	public Set getSet(Object key) {
		return getMapSet(this, key);
	}

	public String getString(K key, String defaultValue) {
		return getMapString(this, key, defaultValue);
	}

	public boolean getBoolean(K key, boolean defaultValue) {
		return getMapBoolean(this, key, defaultValue);
	}

	public byte getByte(K key, byte defaultValue) {
		return getMapByte(this, key, defaultValue);
	}

	public short getShort(K key, short defaultValue) {
		return getMapShort(this, key, defaultValue);
	}

	public int getInteger(K key, int defaultValue) {
		return getMapInteger(this, key, defaultValue);
	}

	public long getLong(K key, long defaultValue) {
		return getMapLong(this, key, defaultValue);
	}

	public float getFloat(K key, float defaultValue) {
		return getMapFloat(this, key, defaultValue);
	}

	public double getDouble(K key, double defaultValue) {
		return getMapDouble(this, key, defaultValue);
	}

	public Map getMap(Object key, Map defaultValue) {
		return getMapMap(this, key, defaultValue);
	}

	public List getList(Object key, List defaultValue) {
		return getMapList(this, key, defaultValue);
	}

	public Set getSet(Object key, Set defaultValue) {
		return getMapSet(this, key, defaultValue);
	}

	////////
	public static boolean isEmpty(Map map) {
		return (map == null) || (map.isEmpty());
	}

	public static boolean isNotEmpty(Map map) {
		return !isEmpty(map);
	}

	public static Object getMapObject(Map map, Object key) {
		if (map != null) {
			return map.get(key);
		}
		return null;
	}

	public static String getMapString(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				String string = String.valueOf(answer);
				if (answer instanceof Number) {
					BigDecimal bd = new BigDecimal(string);
					string = bd.toPlainString();
				}
				if (string.equals("null")) {
					string = null;
				}
				return string;
			}
		}
		return null;
	}

	public static Boolean getMapBoolean(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				if ((answer instanceof Boolean)) {
					return (Boolean) answer;
				}
				if ((answer instanceof String)) {
					return new Boolean((String) answer);
				}
				if ((answer instanceof Number)) {
					Number n = (Number) answer;
					return n.intValue() != 0 ? Boolean.TRUE : Boolean.FALSE;
				}
			}
		}
		return null;
	}

	public static Number getMapNumber(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				if ((answer instanceof Number)) {
					return (Number) answer;
				}
				if ((answer instanceof String))
					try {
						String text = (String) answer;
						return NumberFormat.getInstance().parse(text);
					} catch (ParseException localParseException) {
					}
			}
		}
		return null;
	}

	public static Byte getMapByte(Map map, Object key) {
		Number answer = getMapNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Byte)) {
			return (Byte) answer;
		}
		return new Byte(answer.byteValue());
	}

	public static Short getMapShort(Map map, Object key) {
		Number answer = getMapNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Short)) {
			return (Short) answer;
		}
		return new Short(answer.shortValue());
	}

	public static Integer getMapInteger(Map map, Object key) {
		Number answer = getMapNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Integer)) {
			return (Integer) answer;
		}
		return new Integer(answer.intValue());
	}

	public static Long getMapLong(Map map, Object key) {
		Number answer = getMapNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Long)) {
			return (Long) answer;
		}
		return new Long(answer.longValue());
	}

	public static Float getMapFloat(Map map, Object key) {
		Number answer = getMapNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Float)) {
			return (Float) answer;
		}
		return new Float(answer.floatValue());
	}

	public static Double getMapDouble(Map map, Object key) {
		Number answer = getMapNumber(map, key);
		if (answer == null)
			return null;
		if ((answer instanceof Double)) {
			return (Double) answer;
		}
		return new Double(answer.doubleValue());
	}

	public static Map getMapMap(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if ((answer != null) && ((answer instanceof Map))) {
				return (Map) answer;
			}
		}
		return null;
	}

	public static List getMapList(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if ((answer != null) && ((answer instanceof List))) {
				return (List) answer;
			}
		}
		return null;
	}

	public static Object[] getMapArray(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if ((answer != null) && ((answer.getClass().isArray()))) {
				return (Object[]) answer;
			}
		}
		return null;
	}

	public static Set getMapSet(Map map, Object key) {
		if (map != null) {
			Object answer = map.get(key);
			if ((answer != null) && ((answer instanceof Set))) {
				return (Set) answer;
			}
		}
		return null;
	}

	public static Object getMapObject(Map map, Object key, Object defaultValue) {
		if (map != null) {
			Object answer = map.get(key);
			if (answer != null) {
				return answer;
			}
		}
		return defaultValue;
	}

	public static String getMapString(Map map, Object key, String defaultValue) {
		String answer = getMapString(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Boolean getMapBoolean(Map map, Object key, Boolean defaultValue) {
		Boolean answer = getMapBoolean(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Number getMapNumber(Map map, Object key, Number defaultValue) {
		Number answer = getMapNumber(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Byte getMapByte(Map map, Object key, Byte defaultValue) {
		Byte answer = getMapByte(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Short getMapShort(Map map, Object key, Short defaultValue) {
		Short answer = getMapShort(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Integer getMapInteger(Map map, Object key, Integer defaultValue) {
		Integer answer = getMapInteger(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Long getMapLong(Map map, Object key, Long defaultValue) {
		Long answer = getMapLong(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Float getMapFloat(Map map, Object key, Float defaultValue) {
		Float answer = getMapFloat(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Double getMapDouble(Map map, Object key, Double defaultValue) {
		Double answer = getMapDouble(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static Map getMapMap(Map map, Object key, Map defaultValue) {
		Map answer = getMapMap(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return answer;
	}

	public static List getMapList(Map map, Object key, List defaultValue) {
		List answer = getMapList(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return null;
	}

	public static Set getMapSet(Map map, Object key, Set defaultValue) {
		Set answer = getMapSet(map, key);
		if (answer == null) {
			answer = defaultValue;
		}
		return null;
	}

}
