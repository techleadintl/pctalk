/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 上午10:04:39
 * 
 ***************************************************************/
package com.xinwei.spark.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.geom.AffineTransform;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.talk.common.LAF;

/**
 * <code>TitlePanel</code> class is the top panel displayed in this application. This should be used to identify the application to users using a tabTitle, brief description, and the company's logo.
 */
public final class TitlePanel extends JPanel {
	private static final long serialVersionUID = -967166058268240672L;
	private final JLabel titleLabel = new JLabel();
	private final JLabel descriptionLabel = new JLabel();
	private final JLabel iconLabel = new JLabel();
	private final GridBagLayout gridBagLayout = new GridBagLayout();

	/**
	 * Create a new TitlePanel.
	 *
	 * @param tabTitle
	 *            the tabTitle to use with the panel.
	 * @param description
	 *            the panel description.
	 * @param icon
	 *            the icon to use with the panel.
	 * @param showDescription
	 *            true if the descrption should be shown.
	 */
	public TitlePanel(String title, String description, Icon icon, boolean showDescription) {

		// Set the icon
		iconLabel.setIcon(icon);

		// Set the tabTitle
		setTitle(title);

		// Set the description
		setDescription(description);

		setLayout(gridBagLayout);

		descriptionLabel.setBackground(Color.white);

		if (showDescription) {
			add(iconLabel, new GridBagConstraints(2, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
			add(descriptionLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 9, 5, 5), 0, 0));
			add(titleLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
			setBackground(Color.white);

			titleLabel.setFont(new Font("dialog", Font.BOLD, 11));
			descriptionLabel.setFont(new Font("dialog", 0, 10));
		} else {
			final JPanel panel = new ImagePanel();
			panel.setBorder(BorderFactory.createEtchedBorder());

			panel.setLayout(new GridBagLayout());
			panel.add(titleLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
			panel.add(iconLabel, new GridBagConstraints(2, 0, 1, 2, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0));

			titleLabel.setVerticalTextPosition(JLabel.CENTER);
			titleLabel.setFont(new Font("dialog", Font.BOLD, 14));
			titleLabel.setForeground(Color.black);
			descriptionLabel.setFont(new Font("dialog", 0, 10));
			add(panel, new GridBagConstraints(0, 0, 1, 0, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 2, 2, 2), 0, 0));
		}

	}

	/**
	 * Set the icon for the panel.
	 *
	 * @param icon
	 *            - the relative icon based on classpath. ex. /com/jivesoftware/images/Foo.gif.
	 */
	public final void setIcon(Icon icon) {
		titleLabel.setIcon(icon);
	}

	/**
	 * Set the main tabTitle for this panel.
	 *
	 * @param tabTitle
	 *            - main tabTitle.
	 */
	public final void setTitle(String title) {
		titleLabel.setText(title);
	}

	/**
	 * Set a brief description which will be displayed below the main tabTitle.
	 *
	 * @param desc
	 *            - brief description
	 */
	public final void setDescription(String desc) {
		desc = "<HTML><BODY>" + desc + "</HTML></BODY>";
		descriptionLabel.setText(desc);
	}

	public class ImagePanel extends JPanel {
		private static final long serialVersionUID = 5155908601530113727L;
		final ImageIcon icons = LAF.getTitlePanelImageIcon();

		public ImagePanel() {

		}

		public ImagePanel(LayoutManager layout) {
			super(layout);
		}

		public void paintComponent(Graphics g) {
			Image backgroundImage = icons.getImage();
			double scaleX = getWidth() / (double) backgroundImage.getWidth(null);
			double scaleY = getHeight() / (double) backgroundImage.getHeight(null);
			AffineTransform xform = AffineTransform.getScaleInstance(scaleX, scaleY);
			((Graphics2D) g).drawImage(backgroundImage, xform, this);
		}
	}

}
