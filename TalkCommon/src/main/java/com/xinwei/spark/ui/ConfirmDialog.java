package com.xinwei.spark.ui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.spark.SwingWorker;

/**
 * Implementation of a Confirm Dialog to replace the modal JOptionPane.confirm. This is intended for use as a yes - no dialog.
 */
public class ConfirmDialog extends JPanel {
	private static final long serialVersionUID = -441250586899776207L;
	private JLabel message;
	private JLabel iconLabel;
	private JButton yesButton;
	private JButton noButton;

	private JDialog dialog;

	/**
	 * Creates the base confirm Dialog.
	 */
	public ConfirmDialog() {
		setLayout(new GridBagLayout());

		message = new JLabel();
		iconLabel = new JLabel();
		yesButton = new JButton();
		noButton = new JButton();

		add(iconLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(message, new GridBagConstraints(1, 0, 4, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
		add(yesButton, new GridBagConstraints(3, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
		add(noButton, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

		yesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				SwingWorker worker = new SwingWorker() {
					public Object construct() {
						SystemUtil.sleep(50);
						return "ok";
					}

					public void finished() {
						doWork();
					}
				};
				worker.start();
				doYes();
				dialog.dispose();
			}
		});

		noButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				doNo();
				dialog.dispose();
			}
		});

	}

	/**
	 * Creates and displays the new confirm dialog.
	 *
	 * @param parent
	 *            the parent dialog.
	 * @param title
	 *            the title of this dialog.
	 * @param text
	 *            the main text to display.
	 * @param yesText
	 *            the text to use on the OK or Yes button.
	 * @param noText
	 *            the text to use on the No button.
	 * @param icon
	 *            the icon to use for graphical represenation.
	 */
	public JDialog showConfirmDialog(Window parent, String title, String text, String yesText, String noText, Icon icon) {
		message.setText("<html><body>" + text + "</body></html>");
		iconLabel.setIcon(icon);

		SwingUtil.resButton(yesButton, yesText);
		SwingUtil.resButton(noButton, noText);

		dialog = new JDialog(parent, title);
		dialog.getContentPane().setLayout(new BorderLayout());
		dialog.getContentPane().add(this);
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);

		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				doNo();
			}
		});

		return dialog;
	}

	public void setDialogSize(int width, int height) {
		dialog.setSize(width, height);
		dialog.pack();
		dialog.validate();
	}

	public void centerWindowOnScreen() {
		SwingUtil.centerWindowOnScreen(dialog);
	}

	/**
	 * Fired when the Yes button has been clicked.
	 */
	public void doYes() {
	};

	public void doWork() {
	};

	/**
	 * Fired when the No button has been clicked.
	 */
	public void doNo() {
	};
}
