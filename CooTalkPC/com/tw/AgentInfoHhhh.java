package com.tw;

import java.io.Serializable;

/**
 * Model class of 代理信息.
 * 
 * @author generated by ERMaster
 * @version $Id$
 */
public class AgentInfoHhhh implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 电话号码. */
	private String telNo;

	/** 代理信息. */
	private String agentinfo;

	/** uid. */
	private String uid;

	/** 代理等级. */
	private Integer level;

	/** 累计欠款. */
	private String accumulativeDebt;

	/** 最大欠款额度. */
	private String maxDebtQuota;

	/** 折扣比例，如果折扣>100，付款价格提交服务器后计算。预留接口. */
	private Integer discountRatio;

	/** 付款比例，真正的比例需要除以100. */
	private Integer payMoneyRatio;

	/** 币种. */
	private String currency;

	/** 库存数量. */
	private Integer storeAmount;

	/** 描述. */
	private String description;

	/**
	 * Constructor.
	 */
	public AgentInfoHhhh() {
	}

	/**
	 * Set the 电话号码.
	 * 
	 * @param telNo
	 *            电话号码
	 */
	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}

	/**
	 * Get the 电话号码.
	 * 
	 * @return 电话号码
	 */
	public String getTelNo() {
		return this.telNo;
	}

	/**
	 * Set the 代理信息.
	 * 
	 * @param agentinfo
	 *            代理信息
	 */
	public void setAgentinfo(String agentinfo) {
		this.agentinfo = agentinfo;
	}

	/**
	 * Get the 代理信息.
	 * 
	 * @return 代理信息
	 */
	public String getAgentinfo() {
		return this.agentinfo;
	}

	/**
	 * Set the uid.
	 * 
	 * @param uid
	 *            uid
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**
	 * Get the uid.
	 * 
	 * @return uid
	 */
	public String getUid() {
		return this.uid;
	}

	/**
	 * Set the 代理等级.
	 * 
	 * @param level
	 *            代理等级
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**
	 * Get the 代理等级.
	 * 
	 * @return 代理等级
	 */
	public Integer getLevel() {
		return this.level;
	}

	/**
	 * Set the 累计欠款.
	 * 
	 * @param accumulativeDebt
	 *            累计欠款
	 */
	public void setAccumulativeDebt(String accumulativeDebt) {
		this.accumulativeDebt = accumulativeDebt;
	}

	/**
	 * Get the 累计欠款.
	 * 
	 * @return 累计欠款
	 */
	public String getAccumulativeDebt() {
		return this.accumulativeDebt;
	}

	/**
	 * Set the 最大欠款额度.
	 * 
	 * @param maxDebtQuota
	 *            最大欠款额度
	 */
	public void setMaxDebtQuota(String maxDebtQuota) {
		this.maxDebtQuota = maxDebtQuota;
	}

	/**
	 * Get the 最大欠款额度.
	 * 
	 * @return 最大欠款额度
	 */
	public String getMaxDebtQuota() {
		return this.maxDebtQuota;
	}

	/**
	 * Set the 折扣比例，如果折扣>100，付款价格提交服务器后计算。预留接口.
	 * 
	 * @param discountRatio
	 *            折扣比例，如果折扣>100，付款价格提交服务器后计算。预留接口
	 */
	public void setDiscountRatio(Integer discountRatio) {
		this.discountRatio = discountRatio;
	}

	/**
	 * Get the 折扣比例，如果折扣>100，付款价格提交服务器后计算。预留接口.
	 * 
	 * @return 折扣比例，如果折扣>100，付款价格提交服务器后计算。预留接口
	 */
	public Integer getDiscountRatio() {
		return this.discountRatio;
	}

	/**
	 * Set the 付款比例，真正的比例需要除以100.
	 * 
	 * @param payMoneyRatio
	 *            付款比例，真正的比例需要除以100
	 */
	public void setPayMoneyRatio(Integer payMoneyRatio) {
		this.payMoneyRatio = payMoneyRatio;
	}

	/**
	 * Get the 付款比例，真正的比例需要除以100.
	 * 
	 * @return 付款比例，真正的比例需要除以100
	 */
	public Integer getPayMoneyRatio() {
		return this.payMoneyRatio;
	}

	/**
	 * Set the 币种.
	 * 
	 * @param currency
	 *            币种
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * Get the 币种.
	 * 
	 * @return 币种
	 */
	public String getCurrency() {
		return this.currency;
	}

	/**
	 * Set the 库存数量.
	 * 
	 * @param storeAmount
	 *            库存数量
	 */
	public void setStoreAmount(Integer storeAmount) {
		this.storeAmount = storeAmount;
	}

	/**
	 * Get the 库存数量.
	 * 
	 * @return 库存数量
	 */
	public Integer getStoreAmount() {
		return this.storeAmount;
	}

	/**
	 * Set the 描述.
	 * 
	 * @param description
	 *            描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Get the 描述.
	 * 
	 * @return 描述
	 */
	public String getDescription() {
		return this.description;
	}


}
