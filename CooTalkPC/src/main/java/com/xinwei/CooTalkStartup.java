/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月12日 上午11:02:35
 * 
 ***************************************************************/
package com.xinwei;

import org.jivesoftware.smack.filter.OrFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smack.packet.Presence;

import com.xinwei.talk.action.ActivateChatRoomAction;
import com.xinwei.talk.action.group.GroupInviteAction;
import com.xinwei.talk.action.group.GroupOpenRoomAction;
import com.xinwei.talk.action.group.GroupQuitAction;
import com.xinwei.talk.action.member.MemberDeleteAction;
import com.xinwei.talk.action.member.MemberInviteAction;
import com.xinwei.talk.action.member.MemberOpenAction;
import com.xinwei.talk.action.member.MemberUpdateAction;
import com.xinwei.talk.action.talk.TalkInfoAction;
import com.xinwei.talk.action.talk.TalkOpenAction;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.dao.TalkDaoImpl;
import com.xinwei.talk.http.service.HttpFileServiceImpl;
import com.xinwei.talk.http.service.HttpFindFriendsServiceImpl;
import com.xinwei.talk.http.service.HttpInviteFriendsServiceImpl;
import com.xinwei.talk.http.service.HttpLoginServiceImpl;
import com.xinwei.talk.http.service.HttpPasswordServiceImpl;
import com.xinwei.talk.http.service.HttpTalkServiceImpl;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.manager.VCardManagerImpl;
import com.xinwei.talk.ui.CooTalkGroupDialog;
import com.xinwei.talk.ui.CooTalkInfoDialog;
import com.xinwei.talk.ui.action.TalkDeleteAction;
import com.xinwei.talk.ui.main.dialog.user.TalkDialog;
import com.xinwei.talk.xmpp.MessageConverterImpl;
import com.xinwei.talk.xmpp.listener.ChatXmppListener;
import com.xinwei.talk.xmpp.listener.ErrorXmppListener;
import com.xinwei.talk.xmpp.listener.GroupXmppListener;
import com.xinwei.talk.xmpp.listener.HeadlineXmppListener;
import com.xinwei.talk.xmpp.listener.NormalXmppListener;
import com.xinwei.talk.xmpp.listener.UserPresenceListener;
import com.xinwei.talk.xmpp.listener.XmppListener;
import com.xinwei.talk.xmpp.listener.filter.RepeatXmppMessageFilter;
import com.xinwei.talk.xmpp.listener.filter.TestDateXmppMessageFilter;

public class CooTalkStartup extends Startup {
	public static void main(String[] args) {
		new CooTalkStartup().startup();
	}

	protected void addPlugins() {
		super.addPlugins();
		// final PluginManager pluginManager = PluginManager.getInstance();
	}

	protected void addResources() {
		super.addResources();

		Res.addClassLoader(CooTalkStartup.class.getClassLoader());
		Res.addResource("talk/properties/cootalk");

		Res.put("MULTI-MESSAGE", Boolean.FALSE);
	}

	@Override
	protected void addI18ns() {
		super.addI18ns();
		Res.addI18n("talk/i18n/cootalk");
	}

	protected void addServices() {
		TalkManager.setTalkDao(new TalkDaoImpl());

		TalkManager.setLoginService(new HttpLoginServiceImpl());
		TalkManager.setTalkService(new HttpTalkServiceImpl());
		TalkManager.setFileService(new HttpFileServiceImpl());
		TalkManager.setVCardManager(new VCardManagerImpl());
//		Added by Charith
		TalkManager.setHttpPasswordService(new HttpPasswordServiceImpl());
		TalkManager.setHttpInviteFriendService(new HttpInviteFriendsServiceImpl());
		TalkManager.setHttpfindFriendsService(new HttpFindFriendsServiceImpl());
		
		TalkManager.setMessageConverter(new MessageConverterImpl());
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void addActionClass() {
		TalkManager.addActionClasses("talk", TalkOpenAction.class, TalkInfoAction.class, TalkDeleteAction.class);
		TalkManager.addActionClasses("member", MemberOpenAction.class, MemberUpdateAction.class,
				MemberInviteAction.class, MemberDeleteAction.class);
		TalkManager.addActionClasses("group", GroupOpenRoomAction.class, GroupQuitAction.class,
				GroupInviteAction.class);
		TalkManager.addActionClasses("activateChatRoom", ActivateChatRoomAction.class);
	}

	protected void addComponentClass() {
		TalkManager.addComponentClass("addgroup", CooTalkGroupDialog.class);
		TalkManager.addComponentClass("talkInfo", CooTalkInfoDialog.class);
		TalkManager.addComponentClass("addtalk", TalkDialog.class);
	}

	@Override
	protected void addMessageListenersAndMessageFilters() {
		TalkManager.addPacketFilter(
				new OrFilter(new PacketTypeFilter(Presence.class), new PacketTypeFilter(Message.class)));

		XmppListener packetListener = new XmppListener();
		TalkManager.setPacketListener(packetListener);
		// Single chat and group chat messages
		packetListener.addMessageListener(Type.chat, new ChatXmppListener());// 单聊和群聊消息
		// Group member change message
		packetListener.addMessageListener(Type.groupchat, new GroupXmppListener());// 组的成员变化消息
		// Receipt message
		packetListener.addMessageListener(Type.normal, new NormalXmppListener());// 回执消息
		packetListener.addMessageListener(Type.error, new ErrorXmppListener());
		packetListener.addMessageListener(Type.headline, new HeadlineXmppListener());
		packetListener.addXmppPresenceListener(Presence.Type.presence, new UserPresenceListener() );

		packetListener.addMessageFilter(new RepeatXmppMessageFilter());
		packetListener.addMessageFilter(new TestDateXmppMessageFilter());
		// packetListener.addMessageFilter(new AvatarXmppMessageFilter());//头像消息
		// packetListener.addMessageFilter(new MomentsXmppMessageFilter());//朋友圈消息
		// packetListener.addMessageFilter(new RedPacketXmppMessageFilter());//红包消息

		// This comment is to check gitlab from Srinath
	}

}
