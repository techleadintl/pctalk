package com.xinwei.talk.http.service;

import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.http.service.HttpFindFriendsService;
import com.xinwei.talk.dto.FindFriendsDTO;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;

public class HttpFindFriendsServiceImpl extends TalkServiceHelper implements HttpFindFriendsService{
	
	 VCard vcard = TalkManager.getVCard();
	 String countryCode = vcard.getTel();
	 String URL_QUERRYFINDFRIEND = "/search/" +"0094"+ "/searchFriend";
	 String URL = "https://talkapp.cootel.com";
	/**
	 * @author Charith
	 * @param FindFriendsDTO
	 * @throws Exception 
	 */
	@Override
	public HttpResponseInfo querryFindFriendParams(FindFriendsDTO ffDTO) throws Exception {
		
		HttpResponseInfo requestInfo = postRequestInfo(URL+URL_QUERRYFINDFRIEND, new String[] {"telNo",ffDTO.getTelNo()},new String[] {"nickName",ffDTO.getNickName()},
														new String[] {"age",ffDTO.getAge()},new String[] {"gender",ffDTO.getGender()},new String[] {"country",ffDTO.getCountry()},
														new String[] {"Email",ffDTO.getEmail()});
		
		handleResponse(requestInfo, URL, null);	
		return requestInfo;
	}

}
