package com.xinwei.talk.http.service;

import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.XMap;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.http.service.HttpTalkService;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.*;

import java.util.*;
import java.util.Map.Entry;

public class HttpTalkServiceImpl extends TalkServiceHelper implements HttpTalkService {
	// 查询好友列表
	static String URL_QUERY_FRIEND_LIST = "/Ucenter/queryFriendList";
	// 通过电话号码查询单个好友
	static String URL_QUERY_USER_INFO = "/Ucenter/queryUserInfo";
	// 通过UID查询单个好友
	static String URL_SELECT_USER_INFO_BY_CID = "/Ucenter/selectUserInfoByCid";
	// 添加单个好友
	static String URL_ADD_FRIEND = "/Ucenter/addFriend";
	// 修改好友昵称
	static String URL_MODIFY_FRIEND = "/Ucenter/modifyFriend";
	// 删除好友
	static String URL_DELETE_FRIEND = "/Ucenter/Ucenter/removeFriend";

	/**************** 群组 ********************/
	// 创建群组信息
	static String URL_CREATE_GROUP = "/CooTalk-GateWay/groupManager!createGroup.do";
	// 解散群组
	static String URL_DESTORY_GROUP = "/CooTalk-GateWay/groupManager!destoryGroup.do";
	// 更新群组信息
	static String URL_UPDATE_GROUP = "/CooTalk-GateWay/groupManager!updateGroupInfo.do";
	// 根据UID查询所在群组列表
	static String URL_QUERY_GROUP_LIST = "/CooTalk-GateWay/groupManager!queryGroupListByCid.do";
	// 根据groupId查询群组信息
	static String URL_QUERY_GROUP = "/CooTalk-GateWay/groupManager!queryGroupInfoById.do";
	// 添加组成员信息
	static String URL_ADD_MEMBER = "/CooTalk-GateWay/groupManager!addMember.do";
	// 修改组成员信息
	static String URL_UPDATE_MEMBER = "/CooTalk-GateWay/groupManager!updateMember.do";
	// 删除组成员
	static String URL_REMOVE_MEMBER = "/CooTalk-GateWay/groupManager!removeMember.do";

	/*
	 * Sachini EditProfile - will be moved to a better place when other server
	 * features come in
	 * 
	 */
	static String URL_EDIT_NAME = "/Ucenter/updateCootalkName";

	public Object[] getTalkList() throws Exception {
		List<Talk> result = new ArrayList<Talk>();

		HttpResponseInfo responseInfo = postRequestInfo(getServer() + URL_QUERY_FRIEND_LIST,
				new Object[] { "cid", TalkManager.getVCard().getUid() });

		List<Map> friendList = (List<Map>) getResponseParam(responseInfo, "friendList");

		for (Map fMap : friendList) {
			CooTalk talk = toTalk(fMap);
			if (!getVCardTel().equals(talk.getTel())) {
				result.add(talk);
			}
		}

		return new Object[] { null, result };

	}

	/**
	 * 通过电话号码查询好友
	 */
	@Override
	public Talk getTalk(String phone) throws Exception {
		HttpResponseInfo responseInfo = postRequestInfo(getServer() + URL_QUERY_USER_INFO,
				new Object[] { "cid", TalkManager.getVCard().getUid() }, new Object[] { "fields", phone });

		Map map = (Map) getResponseParam(responseInfo, "friend");

		return toTalk(map);
	}

	@Override
	public Talk getTalkByUid(String uid) throws Exception {
		HttpResponseInfo responseInfo = postRequestInfo(getServer() + URL_SELECT_USER_INFO_BY_CID,
				new Object[] { "cid", uid });

		Map map = (Map) getResponseParam(responseInfo, "friend");

		return toTalk(map);
	}

	@Override
	public void addTalk(Talk talk, String display) throws Exception {
		Map<Talk, String> talkMap = new HashMap<Talk, String>();
		talkMap.put(talk, display);
		addTalk(talkMap);

	}

	@Override
	public void addTalk(Map<Talk, String> talkMap) throws Exception {
		Set<Entry<Talk, String>> talks = talkMap.entrySet();

		for (Entry<Talk, String> entry : talks) {
			Talk talk = entry.getKey();
			postRequestInfo(getServer() + URL_ADD_FRIEND, new Object[] { "cid", TalkManager.getVCard().getUid() },
					new Object[] { "friendCid", talk.getUid() });
		}
	}

	@Override
	public int deleteTalk(Talk talk, LinkedHashMap<String, Object> attMap) throws Exception {
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("cid", TalkManager.getVCard().getUid());
		map.put("friendCid", talk.getUid());
		if (attMap != null) {
			Set<Entry<String, Object>> attSet = attMap.entrySet();
			for (Entry<String, Object> entry : attSet) {
				map.put(entry.getKey(), entry.getValue());
			}
		}
		HttpResponseInfo responseInfo = postRequestInfo(getServer() + URL_DELETE_FRIEND, map);
		return getResult(responseInfo);

	}

	@Override
	public int updateTalk(Talk talk, LinkedHashMap<String, Object> attMap) throws Exception {
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("cid", TalkManager.getVCard().getUid());
		map.put("friendCid", talk.getUid());
		if (attMap != null) {
			Set<Entry<String, Object>> attSet = attMap.entrySet();
			for (Entry<String, Object> entry : attSet) {
				map.put(entry.getKey(), entry.getValue());
			}
		}
		HttpResponseInfo responseInfo = postRequestInfo(getServer() + URL_MODIFY_FRIEND, map);
		return getResult(responseInfo);
	}

	public List<Talk> getTalkList(Collection<String> phones) throws Exception {
		List<Talk> result = new ArrayList<Talk>();
		for (String phone : phones) {
			try {
				Talk talk = getTalk(phone);
				if (talk != null) {
					result.add(talk);
				}
			} catch (Exception ex) {
			}
		}
		return result;
	}
	//

	/**
	 * 查询群组
	 * 
	 * Query group
	 */
	public List<Group> getGroupList() throws Exception {
		List<Group> result = new ArrayList<Group>();

		HttpResponseInfo responseInfo = postRequestInfo(TalkManager.getVCard().getServer() + URL_QUERY_GROUP_LIST,
				new String[] { "cid", String.valueOf(TalkManager.getVCard().getUid()) });

		List<Map> groupList = (List<Map>) getResponseParam(responseInfo, "userGroupList");

		for (Map map : groupList) {
			Group group = toGroup(map);
			result.add(group);
		}
		return result;
	}

	/**
	 * 查询单个组信息
	 * 
	 * @param groupId
	 * @return
	 * @throws Exception
	 */
	public Group getGroup(String groupId) throws Exception {
		if (StringUtil.isEmpty(groupId)) {
			return null;
		}

		HttpResponseInfo responseInfo = postRequestInfo(TalkManager.getVCard().getServer() + URL_QUERY_GROUP,
				new String[] { "cid", TalkManager.getVCard().getUid() }, new String[] { "groupId", groupId });

		Map map = (Map) getResponseParam(responseInfo, "groupInfo");

		return toGroup(map);
	}

	public Map<Group, List<Member>> getGroupMemberList(Collection<Group> xwGroupList) throws Exception {
		if (CollectionUtil.isEmpty(xwGroupList))
			return null;

		return null;
	}

	/**
	 * 查询组内指定成员信息
	 */
	public List<Member> getGroupMemberList(String groupId, List<String> memJidList) throws Exception {
		return null;
	}

	/**
	 * 创建群组信息
	 * 
	 * Create group information
	 * 
	 * @param group
	 * @return 组ID
	 * @throws Exception
	 */

	public String createGroup(Group group, List<Member> groupMemberList) throws Exception {
		LinkedHashMap<String, Object> requestParam = new LinkedHashMap<String, Object>();
		List<LinkedHashMap<String, Object>> memberList = new ArrayList<LinkedHashMap<String, Object>>();
		requestParam.put("creator", group.getCreatorUid());// Group creator UID

		requestParam.put("memberList", memberList);

		if (groupMemberList != null) {
			for (Member member : groupMemberList) {
				LinkedHashMap<String, Object> memberMap = new LinkedHashMap<String, Object>();
				memberMap.put("cid", member.getUid());// Group member UID
				memberMap.put("role", member.getMemRole());
				memberList.add(memberMap);
			}
		}
		HttpResponseInfo responseInfo = postRequestInfo(getServer() + URL_CREATE_GROUP, requestParam);
		Object groupIdObject = getResponseParam(responseInfo, "groupId");
		String groupId = StringUtil.toString(groupIdObject);
		group.setId(groupId);
		return groupId;
	}

	/**
	 * 解散群组
	 * 
	 * @param subsId  群创建者UID
	 * @param groupId 群组ID
	 */
	public int deleteGroup(String subsId, String groupId) throws Exception {
		// LinkedHashMap<String, Object> requestParam = new LinkedHashMap<String,
		// Object>();
		// requestParam.put("cid", subsId);
		// requestParam.put("groupId", groupId);
		// HttpResponseInfo responseInfo = postRequestInfo(getServer() +
		// URL_DESTORY_GROUP, requestParam);
		//
		// return getResult(responseInfo);
		List<Member> memberList = TalkManager.getLocalManager().getMemberList(groupId);
		List<String> uidList = new ArrayList<String>();
		if (memberList != null) {
			for (Member m : memberList) {
				String uid = m.getUid();
				if (StringUtil.isNotBlank(uid)) {
					uidList.add(uid);
				}
			}
		}

		if (uidList.size() == 0) {
			return -1;
		}
		uidList.remove(subsId);
		deleteMembers(subsId, groupId, uidList);
		return deleteMember(subsId, groupId, subsId);
	}

	/**
	 * 退出群组
	 * 
	 * @param subsId  群创建者UID
	 * @param groupId 群组ID
	 */
	public int quitMember(String subsId, String groupId) throws Exception {
		return deleteMembers(subsId, groupId, null);
	}

	/**
	 * 删除组成员
	 * 
	 * @param subsId  群创建者UID
	 * @param groupId 群组ID
	 */
	public int deleteMembers(String subsId, String groupId, List<String> uids) throws Exception {
		LinkedHashMap<String, Object> requestParam = new LinkedHashMap<String, Object>();
		requestParam.put("cid", subsId);
		requestParam.put("groupId", groupId);

		if (uids != null) {
			List<LinkedHashMap<String, Object>> memberList = new ArrayList<LinkedHashMap<String, Object>>();
			requestParam.put("memberList", memberList);
			for (String uid : uids) {
				LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
				map.put("cid", uid);
				memberList.add(map);
			}
		}
		HttpResponseInfo responseInfo = postRequestInfo(getServer() + URL_REMOVE_MEMBER, requestParam);

		return getResult(responseInfo);
	}

	public int deleteMember(String subsId, String groupId, String uid) throws Exception {
		List<String> uids = new ArrayList<String>();
		uids.add(uid);
		return deleteMembers(subsId, groupId, uids);
	}

	public int addGroupMembers(Group group, List<Member> groupMemberList) throws Exception {
		LinkedHashMap<String, Object> requestParam = new LinkedHashMap<String, Object>();

		List<LinkedHashMap<String, Object>> memberList = new ArrayList<LinkedHashMap<String, Object>>();

		requestParam.put("cid", TalkManager.getVCard().getUid());// 添加成员的用户UID
		requestParam.put("groupId", group.getId());// 组ID
		requestParam.put("memberList", memberList);

		for (Member member : groupMemberList) {
			LinkedHashMap<String, Object> memberMap = new LinkedHashMap<String, Object>();
			memberMap.put("cid", member.getUid());// 群成员UID
			memberList.add(memberMap);
		}
		HttpResponseInfo responseInfo = postRequestInfo(getServer() + URL_ADD_MEMBER, requestParam);
		return getResult(responseInfo);
	}

	/** 更新群组信息 **/
	public int updateGroup(Group group, String groupName, String describe) throws Exception {
		LinkedHashMap<String, Object> requestParam = new LinkedHashMap<String, Object>();

		requestParam.put("cid", group.getCreatorUid());
		requestParam.put("groupId", group.getId());// 组ID

		LinkedHashMap<String, Object> updateMap = new LinkedHashMap<String, Object>();
		requestParam.put("update", updateMap);
		{
			updateMap.put("groupName", groupName);//
			updateMap.put("describe", describe);//
		}
		HttpResponseInfo responseInfo = postRequestInfo(getServer() + URL_UPDATE_GROUP, requestParam);
		return getResult(responseInfo);
	}

	/** 更新群组成员信息 **/
	public int updateMember(String groupId, String memberUid, String memberNickName) throws Exception {
		LinkedHashMap<String, Object> requestParam = new LinkedHashMap<String, Object>();

		requestParam.put("cid", memberUid);
		requestParam.put("groupId", groupId);// 组ID

		LinkedHashMap<String, Object> updateMap = new LinkedHashMap<String, Object>();
		requestParam.put("update", updateMap);
		{
			updateMap.put("memberNickName", memberNickName);//
		}
		HttpResponseInfo responseInfo = postRequestInfo(getServer() + URL_UPDATE_MEMBER, requestParam);
		return getResult(responseInfo);
	}

	public CooTalk toTalk(Map m) {
		if (m == null || m.isEmpty()) {
			return null;
		}

		String tel = XMap.getString(m, "telNo", "");

		CooTalk talk = new CooTalk();
		talk.setTel(tel);
		talk.setJid(getJid(tel));

		talk.setUid(XMap.getString(m, "cid", ""));
		talk.setName(XMap.getString(m, "cootalkName", ""));
		talk.setNick(XMap.getString(m, "remarkNick", ""));
		talk.setAvatarUrl(XMap.getString(m, "picture", ""));
		talk.setSubTelNos(StringUtil.join(XMap.getList(m, "subTelNos"), ","));
		talk.setRemarkNick(XMap.getString(m, "nick", ""));
		talk.setTopFlag(XMap.getInteger(m, "topFlag"));
		talk.setLockFlag(XMap.getInteger(m, "lockFlag"));
		talk.setBackgroundUrl(XMap.getString(m, "backGround", ""));
		talk.setStatus(XMap.getString(m, "status", ""));
		talk.setSex(XMap.getInteger(m, "sex"));
		talk.setArea(XMap.getString(m, "area", ""));
		talk.setRemarkTelno(XMap.getString(m, "remarkTelno", ""));
		talk.setSignature(XMap.getString(m, "signature", ""));
		talk.setSubscribeUID(XMap.getString(m, "subscribeUID", ""));
		return talk;
	}

	public CooGroup toGroup(Map m) {
		String groupId = XMap.getString(m, "groupId", "");
		CooGroup group = new CooGroup();
		group.setId(groupId);
		group.setName(XMap.getString(m, "groupName", ""));
		group.setGroupNickName(XMap.getString(m, "groupNickName", ""));
		group.setDescribe(XMap.getString(m, "describe", ""));
		group.setTopFlag(XMap.getInteger(m, "topFlag", 1));
		group.setLockFlag(XMap.getInteger(m, "lockFlag", 1));
		group.setBackgroundUrl(XMap.getString(m, "backGround", ""));

		List<Map> memberList = XMap.getList(m, "memberList");
		for (Map mMap : memberList) {
			CooMember member = toMember(groupId, mMap);
			if (member.getMemRole() == CooMember.ROLE_ADMIN) {
				group.setCreatorUid(member.getUid());
			}
			group.addMember(member);
		}
		return group;
	}

	public CooMember toMember(String groupId, Map mMap) {
		CooMember member = new CooMember();

		member.setGid(groupId);
		member.setUid(XMap.getString(mMap, "cid", ""));
		member.setMemNick(XMap.getString(mMap, "memberNickName", ""));
		int role = XMap.getIntValue(mMap, "role", CooMember.ROLE_USER);
		member.setMemRole(role);

		CooTalk talk = toTalk(XMap.getMap(mMap, "userInfo"));
		member.setAvatarUrl(talk.getAvatarUrl());

		member.setMemJid(getJid(talk.getTel()));
		member.setUid(talk.getUid());

		return member;
	}

	@Override
	public int editName(String name) throws Exception {
		HttpResponseInfo responseInfo = postRequestInfo(getServer() + URL_EDIT_NAME,
				new Object[] { "cid", TalkManager.getVCard().getUid() }, new Object[] { "cootalkName", name });
		return getResult(responseInfo);
	}

}
