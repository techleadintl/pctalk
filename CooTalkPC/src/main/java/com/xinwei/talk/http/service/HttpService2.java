//package com.xinwei.talk.http.service;
//
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.Set;
//
//import com.google.gson.Gson;
//import com.xinwei.common.lang.GsonUtil;
//import com.xinwei.common.lang.HttpUtils;
//import com.xinwei.common.lang.StringUtil;
//import com.xinwei.talk.common.HttpException;
//import com.xinwei.talk.common.Log;
//import com.xinwei.talk.common.TalkUtil;
//import com.xinwei.talk.http.model.HttpFileRequestInfo;
//import com.xinwei.talk.http.model.HttpFileResponseInfo;
//import com.xinwei.talk.http.model.HttpResponseInfo;
//import com.xinwei.talk.manager.TalkManager;
//import com.xinwei.talk.model.Group;
//import com.xinwei.talk.model.Talk;
//import com.xinwei.talk.model.http.HttpRequestInfo;
//import com.xinwei.talk.service.file.DownloadListener;
//
//public class HttpService2 {
//	/** 获得文件输入流 **/
//	public byte[] downloadFile(String url, DownloadListener listener) throws Exception {
//		return HttpUtils.download(url, listener);
//	}
//
//	/** 獲取文件字节數組 **/
//	public byte[] downloadImage(String url) throws Exception {
//		return HttpUtils.download(url);
//	}
//
//	protected HttpFileResponseInfo uploadFile(Object obj, String url, Object file, String fileFullName, String[]... otherParams) throws Exception {
//		Gson gson = new Gson();
//
//		String filePartName = "file";
//
//		Map<String, String> params = new HashMap<String, String>();
//
//		String fileType = "";
//
//		String fileName = fileFullName;
//		int index = fileFullName.lastIndexOf(".");
//		if (index != -1) {
//			fileName = fileFullName.substring(0, index);
//			fileType = fileFullName.substring(index + 1);
//		}
//
//		HttpFileRequestInfo fileRequestInfo = getHttpFileRequestInfo(fileName, fileType, obj);
//
//		for (String[] entry : otherParams) {
//			fileRequestInfo.addRequestParam(entry[0], entry[1]);
//		}
//
//		params.put("requestInfo", gson.toJson(fileRequestInfo));
//
//		String responseStr = HttpUtils.upload(url, file, filePartName, params);
//
//		HttpFileResponseInfo fileResponseInfo = gson.fromJson(responseStr, HttpFileResponseInfo.class);
//
//		handleResponse(fileResponseInfo, url, params);
//
//		return fileResponseInfo;
//	}
//
//	protected <T> T postWithValue(String url, Class<T> clazz, Object valueParam) throws Exception {
//		Gson gson = new Gson();
//
//		String param = null;
//
//		if (valueParam instanceof String) {
//			param = (String) valueParam;
//		} else {
//			param = gson.toJson(valueParam);
//		}
//
//		String responseStr = HttpUtils.post(url, param);
//
//		System.out.println(responseStr);
//
//		T response = gson.fromJson(responseStr, clazz);
//
//		return response;
//	}
//
//	protected <T> T postWithNameValue(String url, Class<T> clazz, Object[]... nameValueParams) throws Exception {
//		Map<String, Object> paramMap = new HashMap<String, Object>();
//
//		for (Object[] entry : nameValueParams) {
//			paramMap.put(String.valueOf(entry[0]), entry[1]);
//		}
//
//		return postWithNameValue(url, clazz, paramMap);
//	}
//
//	protected <T> T postWithNameValue(String url, Class<T> clazz, Map<String, Object> nameValueParamMap) throws Exception {
//		Map<String, String> params = null;
//
//		if (nameValueParamMap.size() > 0) {
//			params = new LinkedHashMap<String, String>();
//
//			Set<Entry<String, Object>> entrySet = nameValueParamMap.entrySet();
//			for (Entry<String, Object> entry : entrySet) {
//				String param = null;
//				String key = entry.getKey();
//				Object value = entry.getValue();
//				if (value instanceof String) {
//					param = (String) value;
//				} else {
//					param = GsonUtil.toJson(value);
//				}
//				params.put(key, param);
//			}
//		}
//
//		String responseStr = HttpUtils.post(url, params);
//
//		T responseInfo = GsonUtil.fromJson(responseStr, clazz);
//
//		if (responseInfo instanceof HttpResponseInfo) {
//			handleResponse((HttpResponseInfo) responseInfo, url, params);
//		}
//
//		return responseInfo;
//	}
//
//	/**
//	 * 
//	 * @param url
//	 * @param otherParams
//	 *            [[key1,value1],[key2,value2],[key3,value3],...]]
//	 * @return
//	 * @throws Exception
//	 */
//	protected HttpResponseInfo postRequestInfo(String url, Object[]... otherParams) throws Exception {
//		return postRequestInfo(url, HttpResponseInfo.class, otherParams);
//	}
//
//	protected <T> T postRequestInfo(String url, Class<T> clazz, Object[]... otherParams) throws Exception {
//		LinkedHashMap<String, Object> requestParam = new LinkedHashMap<String, Object>();
//
//		if (otherParams.length > 0) {
//			for (Object[] entry : otherParams) {
//				requestParam.put(String.valueOf(entry[0]), entry[1]);
//			}
//		}
//		return postRequestInfo(url, clazz, requestParam);
//	}
//
//	protected HttpResponseInfo postRequestInfo(String url, LinkedHashMap<String, Object> otherParams) throws Exception {
//		return postRequestInfo(url, HttpResponseInfo.class, otherParams);
//	}
//
//	protected <T> T postRequestInfo(String url, Class<T> clazz, LinkedHashMap<String, Object> otherParams) throws Exception {
//		Map<String, String> params = null;
//
//		if (otherParams != null && !otherParams.isEmpty()) {
//			params = new HashMap<String, String>();
//			HttpRequestInfo requestInfo = new HttpRequestInfo();
//
//			Set<Entry<String, Object>> entrySet = otherParams.entrySet();
//			for (Entry<String, Object> entry : entrySet) {
//				String key = entry.getKey();
//				Object value = entry.getValue();
//				requestInfo.addRequestParam(key, value);
//			}
//
//			params.put("requestInfo", GsonUtil.toJson(requestInfo));
//		}
//
//		String responseStr = HttpUtils.post(url, params);
//
//		Log.info("request:" + url + "---params:" + params + "---response:" + responseStr);
//
//		T responseInfo = GsonUtil.fromJson(responseStr, clazz);
//
//		if (responseInfo instanceof HttpResponseInfo) {
//			handleResponse((HttpResponseInfo) responseInfo, url, params);
//		}
//
//		return responseInfo;
//	}
//
//	public String get(String url) throws Exception {
//		return HttpUtils.get(url);
//	}
//
//	protected HttpFileResponseInfo deleteFile(String url, String[]... otherParams) throws Exception {
//		Gson gson = new Gson();
//
//		Map<String, String> params = new HashMap<String, String>();
//
//		HttpFileRequestInfo fileRequestInfo = new HttpFileRequestInfo();
//
//		for (String[] entry : otherParams) {
//			fileRequestInfo.addRequestParam(entry[0], entry[1]);
//		}
//
//		params.put("requestInfo", gson.toJson(fileRequestInfo));
//
//		String responseStr = HttpUtils.post(url, params);
//
//		HttpFileResponseInfo fileResponseInfo = gson.fromJson(responseStr, HttpFileResponseInfo.class);
//
//		handleResponse(fileResponseInfo, url, params);
//
//		return fileResponseInfo;
//	}
//
//	protected void handleResponse(HttpResponseInfo responseInfo, String url, Map<String, String> params) throws Exception {
//		int result = responseInfo.getResultCode();
//		if (result != 0) {
//			throw new HttpException(responseInfo.getResultMsg(), result, url, params);
//		}
//	}
//
//	protected HttpFileRequestInfo getHttpFileRequestInfo(String fileName, String fileType, Object obj) {
//		if (obj instanceof Talk) {
//			Talk talk = (Talk) obj;
//			String uid = talk.getUid();
//			if (StringUtil.isNotBlank(uid)) {
//				return new HttpFileRequestInfo(fileName, fileType, getVCardUid(), uid, null);
//			}
//		} else if (obj instanceof Group) {
//			Group group = (Group) obj;
//			String gid = group.getId();
//			if (StringUtil.isNotBlank(gid)) {
//				return new HttpFileRequestInfo(fileName, fileType, getVCardUid(), null, gid);
//			}
//		}
//
//		return null;
//
//	}
//
//	protected String getVCardUid() {
//		return TalkManager.getVCard().getUid();
//	}
//
//	protected String getVCardTel() {
//		return TalkManager.getVCard().getTel();
//	}
//
//	protected String getVCardJid() {
//		return TalkManager.getVCard().getJid();
//	}
//
//	protected String getJid(String tel) {
//		return TalkUtil.getUserFullJid(tel);
//	}
//
//	protected String getServer() {
//		return TalkManager.getVCard().getAppServer();
//	}
//
//	protected String getFileServer() {
//		return TalkManager.getVCard().getFileServer();
//	}
//}
