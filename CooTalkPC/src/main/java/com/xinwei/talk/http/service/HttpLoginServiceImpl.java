package com.xinwei.talk.http.service;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import com.xinwei.common.lang.DESCoderUtil;
import com.xinwei.common.lang.HttpUtils;
import com.xinwei.common.lang.XMap;
import com.xinwei.http.model.HttpRequestInfo;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.http.service.HttpLoginService;
import com.xinwei.talk.common.Constants;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;

public class HttpLoginServiceImpl extends TalkServiceHelper implements HttpLoginService {
	static String URL_GETSERVERPATH = "/Ucenter/getServerPath";

	static String URL_LOGIN = "/Ucenter/userInfoLogin";

	//	18888888888   123456
	//
	//	12345678901  123456
	//
	//	18001190517  123456
	/** 登录 **/
	public void login(String tel, String password) throws Exception {
		HttpRequestInfo.DEVICE_ID = getLocalMac();

		VCard vCard = TalkManager.getVCard();
		
		String url = vCard.getLoginServer();
		// 获取服务器地址
		HttpResponseInfo requestInfo = postRequestInfo(url + URL_GETSERVERPATH, new String[] { "telNo", tel });
		System.out.println(requestInfo);
		handleResponse(requestInfo, url, null);

		// "reServers","coobillServers"
		
		vCard.setImServers((List<String>) (getResponseParam(requestInfo, "imServers")));
		vCard.setAppServers((List<String>) (getResponseParam(requestInfo, "appServers")));
		vCard.setFileServers((List<String>) (getResponseParam(requestInfo, "fileServers")));
		vCard.setSipServers((List<String>) (getResponseParam(requestInfo, "sipServers")));

		vCard.setLoginServer(vCard.getAppServer());
		
		//向http服务器登录
		url = vCard.getAppServer();
		/** userName:电话号码 **/
		/** password:密码 **/
		/** flag:登录类型 0 用户名 1 id 2 号码 **/
		requestInfo = postRequestInfo(url + URL_LOGIN, new String[] { "userName", tel }, new String[] { "password", DESCoderUtil.encryptToStr(password) }, new String[] { "flag", "2" });

		//		    "cid": 210398374,  //用户ID
		//          "telNo": "008618210398374",  //电话号码
		//          "netId": "855",  //网络号
		//          "countryCode": "86",  //国家码
		//          "subscribeUID": "1644563467",  //uid,暂不用
		//          "nick": "gy", //昵称
		//          "sex": 0,//性别
		//          "cootalkName": “f5d1f5ee”, //CooTalk号
		//          "picture": “asdfwfwqweefe”,//头像地址
		//          "signature": "1988",//签名
		//          "area": "北京 海淀",  //地区
		//          "address": "北京 昌平区 回龙观",  //地址信息
		//          "subTelNos": "[0085518243087650]",  //子号码
		//          "ext": "unchangeable"  //CooTalk号不可改变的
		//          "netBookSeq":1000057,//网络通讯录版本号，为0表示还没有网络通讯录

		Map userInfo = (Map) getResponseParam(requestInfo, "userInfo");

		Number deviceIdFlag = (Number) getResponseParam(requestInfo, "deviceIdFlag");
		String token = (String) getResponseParam(requestInfo, "token");
		

		vCard.setBookVersion(XMap.getIntValue(userInfo, "netBookSeq", 0));
		vCard.setCountryCode(XMap.getIntValue(userInfo, "countryCode"));
		vCard.setNetworkId(XMap.getIntValue(userInfo, "netId"));
		//

		vCard.setUid(XMap.getString(userInfo, "cid", ""));
		vCard.setName(XMap.getString(userInfo, "cootalkName", ""));
		vCard.setNick(XMap.getString(userInfo, "nick", ""));
		vCard.setTel(XMap.getString(userInfo, "telNo", ""));
		vCard.setSignature(XMap.getString(userInfo, "signature", ""));
		vCard.putV("token", token);
		// 加载图片
		String picture = XMap.getString(userInfo, "picture", null);
		if (picture != null) {
			vCard.setPicture(picture);
			try {
				byte[] bytes = TalkManager.getFileService().downloadImage(picture);
				vCard.setAvatar(bytes);
				TalkManager.updateVCard(vCard.getUid(), bytes);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// weiquan#camtalk_008613910162613@camito.cn
		String jidPattern = "weiquan#camtalk_{0}@" + vCard.getImServer();
		vCard.setJid(MessageFormat.format(jidPattern, vCard.getTel()));

		String jid = vCard.getJid();
		int index = jid.indexOf("_");
		if (index != -1) {
			Constants.GROUP_PREFIX = Constants.USER_PREFIX = jid.substring(0, index + 1);
		}

		index = jid.indexOf("@");
		if (index != -1) {
			String domain = Constants.DOMAIN = jid.substring(index + 1);
			Constants.USER_SUBFIX = "@" + domain;
			Constants.GROUP_SUBFIX = "@conference." + domain;
		}

		HttpUtils.addHead("token", token);
		HttpUtils.addHead("cid", vCard.getUid());
		HttpUtils.addHead("permanent", "1");
	}

	private static String getLocalMac() {
		try {
			InetAddress adress = InetAddress.getLocalHost();
			NetworkInterface net = NetworkInterface.getByInetAddress(adress);
			byte[] macBytes = net.getHardwareAddress();
			return transBytesToStr(macBytes);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (SocketException e) {
			e.printStackTrace();
		}
		return String.valueOf(System.currentTimeMillis());
	}

	public static String transBytesToStr(byte[] bytes) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			if (i != 0)
				buffer.append("-");
			//bytes[i]&0xff将有符号byte数值转换为32位有符号整数，其中高24位为0，低8位为byte[i]  
			int intMac = bytes[i] & 0xff;
			//toHexString函数将整数类型转换为无符号16进制数字  
			String str = Integer.toHexString(intMac);
			if (str.length() == 0) {
				buffer.append("0");
			}
			buffer.append(str);
		}
		return buffer.toString().toUpperCase();
	}
}
