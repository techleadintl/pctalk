package com.xinwei.talk.http.service;

import com.xinwei.http.model.HttpRequestInfo;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.http.service.AbstractHttpTalkService;
import com.xinwei.talk.common.HttpException;

import java.util.LinkedHashMap;
import java.util.Map;

public class TalkServiceHelper extends AbstractHttpTalkService {
	public void handleResponse(HttpResponseInfo responseInfo, String url, Map<String, String> params) throws Exception {
		int result = getResult(responseInfo);
		if (result != 0) {
			throw new HttpException(getResultMsg(responseInfo), result, url, params);  // 
			//throw new HttpException("error");											// added May 11
		}
	}

	public Object getResponseParam(HttpResponseInfo responseInfo, String key) {
		Object obj = responseInfo.get("data");
		if (obj != null) {
			Map<String, Object> map = (Map<String, Object>) obj;
			return map.get(key);
		} else {
			return null;
		}
	}

	@Override
	public int getResult(HttpResponseInfo responseInfo) {
		return responseInfo.getInteger("resultCode", 0);
	}

	@Override
	public String getResultMsg(HttpResponseInfo responseInfo) {
		return responseInfo.getString("resultMsg");
	}

	@Override
	public void addRequestParam(HttpRequestInfo requestInfo, String key, Object value) {
		LinkedHashMap<String, Object> requestParam = (LinkedHashMap<String, Object>) requestInfo.get("parameter");
		if (requestParam == null) {
			requestParam = new LinkedHashMap<String, Object>();
			requestInfo.put("parameter", requestParam);
		}
		((Map<String, Object>) requestParam).put(key, value);
	}

}
