package com.xinwei.talk.http.service;

import java.io.File;
import java.util.Map;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.http.model.HttpRequestInfo;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.http.service.AbstractHttpFileService;
import com.xinwei.http.service.ITalkServiceHelper;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;

public class HttpFileServiceImpl extends AbstractHttpFileService {
	// //通讯录文件上传  Address book file upload
	// static String URL_FILE_XBOSS =
	// "/xboss_file_system/file/fileManager!uploadFile.do";
	// 头像上传
	static String URL_FILE_AVATAR = "/Ucenter/updateAvatar";
	// 聊天文件上传
	static String URL_FILE_CHAT = "/file_server/file/fileManager!uploadChatFile.do";
	static String URL_FILE_TRANSFER = "/file_server/file/fileManager!uploadFile.do";

	/** 删除文件 **/
	public HttpResponseInfo deleteFile(String groupName, String remoteFileName) throws Exception {
		// HttpFileResponseInfo httpFileResponseInfo =
		// deleteFile(TalkManager.getVCard().getServer() + URL_FILE_XBOSS, new
		// String[] { "groupName", groupName }, new String[] { "remoteFileName",
		// remoteFileName });
		// return httpFileResponseInfo.getResultCode() == 0;
		return null;
	}

	/** 通讯录文件上传 **/
	public HttpResponseInfo uploadContactFile(File file) throws Exception {
		// HttpFileResponseInfo fileResponseInfo =
		// uploadFile(TalkManager.getVCard().getServer() + URL_FILE_XBOSS, file,
		// file.getName());
		// return fileResponseInfo.getFileAbsolutePath();
		return null;
	}

	@Override
	public void setAttachment(HttpResponseInfo httpResponseInfo, Map<Object, Object> attachment) {
		Map map = httpResponseInfo.getMap("data");
		if (map == null)
			return;
		attachment.put("fileAbsolutePath", map.get("fileAbsolutePath"));// 原图文件下载地址
		attachment.put("fileThumbnailPath", map.get("fileThumbnailPath")); // 缩略图文件下载地址
		attachment.put("fileSize", map.get("fileSize"));// 原图大小，单位字节
		attachment.put("width", map.get("width")); // 缩略图宽度
		attachment.put("height", map.get("height")); // 缩略图高度
	}

	@Override
	public HttpRequestInfo getHttpFileChatRequestInfo(String fileName, String fileType, Object obj) {
		HttpRequestInfo fileRequestInfo = new HttpRequestInfo();
		getTalkServiceHelper().addRequestParam(fileRequestInfo, "fileName", fileName);
		getTalkServiceHelper().addRequestParam(fileRequestInfo, "fileType", fileType);
		getTalkServiceHelper().addRequestParam(fileRequestInfo, "from_cid", getVCardUid());

		if (obj instanceof Talk) {
			Talk talk = (Talk) obj;
			String uid = talk.getUid();
			if (StringUtil.isNotBlank(uid)) {
				getTalkServiceHelper().addRequestParam(fileRequestInfo, "to_cid", uid);
			}
		} else if (obj instanceof Group) {
			Group group = (Group) obj;
			String gid = group.getId();
			if (StringUtil.isNotBlank(gid)) {
				getTalkServiceHelper().addRequestParam(fileRequestInfo, "to_groupId", gid);
			}
		}

		return fileRequestInfo;
	}

	@Override
	public String getChatFileServerUrl() {
		return getFileServer() + URL_FILE_CHAT;
	}

	@Override
	public String getTransferFileServerUrl() {
		return getFileServer() + URL_FILE_TRANSFER;
	}

	@Override
	public String getAvatarFileServerUrl() {
		return TalkManager.getVCard().getAppServer() + URL_FILE_AVATAR;
	}

	@Override
	public HttpRequestInfo getHttpAvatarRequestInfo(String fileName, String fileType, VCard vCard) {
		HttpRequestInfo fileRequestInfo = new HttpRequestInfo();
		getTalkServiceHelper().addRequestParam(fileRequestInfo, "cid", vCard.getUid());
		getTalkServiceHelper().addRequestParam(fileRequestInfo, "fileName", fileName);
		getTalkServiceHelper().addRequestParam(fileRequestInfo, "fileType", fileType);
		return fileRequestInfo;
	}

	@Override
	public String getFileUrl(HttpResponseInfo responseInfo) {
		return (String) getTalkServiceHelper().getResponseParam(responseInfo, "fileAbsolutePath");

	}

	@Override
	public ITalkServiceHelper getTalkServiceHelper() {
		return (ITalkServiceHelper) TalkManager.getTalkService();
	}

}
