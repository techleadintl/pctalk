package com.xinwei.talk.http.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.http.service.HttpInviteFriendService;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;

public class HttpInviteFriendsServiceImpl extends TalkServiceHelper implements HttpInviteFriendService{

	static String URL_SENDFINDFRIEND = "/r/invite/inviteManager!inviteFriend.do";
	static String URL = "https://talkapp.cootel.com";
	VCard vcard = TalkManager.getVCard();
	
	/**
	 * @author Charith
	 * @param inviterTel - String
	 * @param friendList - List<Map<String,String>> list
	 */
	@Override
	public HttpResponseInfo inviteFriends(String inviterTel, List<Map<String,String>> friendList) throws Exception {
/*		req param 
		inviterTel : String
		list : map<String,String>
		list structure : [{inviteeTel : 123142131}, ..]*/
		String name = vcard.getName(); 
		HttpResponseInfo requestInfo = postRequestInfo(URL+URL_SENDFINDFRIEND, new String[] {"telno",inviterTel},new String[] {"username",name},new String[] {"msgLanguage","en"},
														new String[] {"tempId","2005000101"},new Object[] {"friends",friendList});
		
/*		res param
		“resultCode”:
			  0 success
              1 failure
              5001 Interface internal error
              5002 Request parameter validation error
              5003 sessionId - invalid
              5004 Token verification failed
*/	
		if(getResult(requestInfo) == 5001) {
			Log.error("Invite friend api request - 5001 - Interface internal error");	
			return null;
		}else if (getResult(requestInfo) == 5002) {
			Log.error("Invite friend api request - 5002 - Request parameter validation error");
			return null;
		}else if (getResult(requestInfo) == 5003) {
			Log.error("Invite friend api request - 5003 - sessionId - invalid");
			return null;
		}else if (getResult(requestInfo) == 5004) {
			Log.error("Invite friend api request - 5004 - Token verification failed");
			return null;
		}
		
//		handleResponse(requestInfo, URL, null);	
		return requestInfo;
	}

}
