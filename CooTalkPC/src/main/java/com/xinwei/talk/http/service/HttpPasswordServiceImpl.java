package com.xinwei.talk.http.service;

import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.http.service.HttpPasswordService;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;

public class HttpPasswordServiceImpl extends TalkServiceHelper implements HttpPasswordService{
	
	static String URL_SENDAUTHPATH = "/r/resetPwd/sendAuthCode.do";
	String URL_CHECKAUTHPATH = "/r/resetPwd/checkAuthCode.do";
	String URL_MODIFYPWPATH = "/r/resetPwd/modifyPwd.do";
	VCard vcard = TalkManager.getVCard();
	String url = null;
	
	
	@Override
	public HttpResponseInfo sendAuthCode(String telNo, String tag) throws Exception {
		//fixme : relocate setPwServer to resetPassword method 
		vcard.setPasswordServer(Res.getString("talk.server"));
		url = vcard.getPasswordServer();
//		request param
//		"telNo":"008618210398374", Full number
//		"tag":"" message ，voice phone
		HttpResponseInfo requestInfo = postRequestInfo(url+URL_SENDAUTHPATH, new String[] {"telNo",telNo},new String[] { "tag", tag });
		
//		response param
//	    "resultCode": 0,  success；   1  The number is not talk user.
//	    "resultMsg": "success",
//	    "data": {
//	       "seqCode": "2515"   serial number ,
//    	   "tag":"message" Representative SMS verification code，"voice" Representing voice verification code}
		handleResponse(requestInfo, url, null);	
		return requestInfo;
	}
	
	@Override
	public HttpResponseInfo checkAuthCode(String telNo, String authCode, String seqCode) throws Exception {
//		request param
//		"telNo":"008618210398374",
//		"authCode":"721683",
//		"seqCode":"7509"
		HttpResponseInfo requestInfo = postRequestInfo(url+URL_CHECKAUTHPATH, new String[] {"telNo",telNo},new String[] { "authCode", authCode },new String[] { "seqCode", seqCode });
		
//		response param
//		 "resultCode": 0,  success；   1  fail
//		 "resultMsg": "success",
//		 "data": {
//		       "sessionId": "1495436394073"  Mark a reply and pass the password when changing it}. 
		handleResponse(requestInfo, url, null);	
		return requestInfo;
	
	}
	
	@Override
	public HttpResponseInfo modifyPassword(String telNo,String password, String sessionId) throws Exception {
		
//		request param
//		"telNo":"008618210398374", Full number
//		" sessionId ":"812614",    Reply to the tag and verify the verification code when it returns. 
//		"password":"jjRGKZcM9x8="  Encrypted cipher
		HttpResponseInfo requestInfo = postRequestInfo(url+URL_MODIFYPWPATH, new String[] {"telNo",telNo}, new String[] {"sessionId",sessionId},new String[] { "password", password });
		
//		response param
//	    "resultCode": 0,
//	    "resultMsg": "updateUserPassword success"
		handleResponse(requestInfo, url, null);	
		return requestInfo;
		
	}

}
