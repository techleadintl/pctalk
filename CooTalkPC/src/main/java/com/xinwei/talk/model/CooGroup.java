package com.xinwei.talk.model;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.model.Group;

public class CooGroup extends Group {
	String groupNickName;

	int topFlag;//消息置顶开关 0 开， 1 关
	int lockFlag;//消息免打扰开关 0 开， 1 关
	String backgroundUrl;//聊天背景
	byte[] background;

	public int getTopFlag() {
		return topFlag;
	}

	public void setTopFlag(int topFlag) {
		this.topFlag = topFlag;
	}

	public int getLockFlag() {
		return lockFlag;
	}

	public void setLockFlag(int lockFlag) {
		this.lockFlag = lockFlag;
	}

	public String getBackgroundUrl() {
		return backgroundUrl;
	}

	public void setBackgroundUrl(String backgroundUrl) {
		this.backgroundUrl = backgroundUrl;
	}

	public byte[] getBackground() {
		return background;
	}

	public void setBackground(byte[] background) {
		this.background = background;
	}

	public String getGroupNickName() {
		return groupNickName;
	}

	public void setGroupNickName(String groupNickName) {
		this.groupNickName = groupNickName;
	}

	@Override
	public boolean equals2(Object obj) {
		boolean equals = super.equals2(obj);
		if (!equals) {
			return false;
		}
		CooGroup other = (CooGroup) obj;
		equals = this.getLockFlag() == other.getLockFlag();
		if (!equals) {
			return false;
		}
		equals = this.getTopFlag() == other.getTopFlag();
		if (!equals) {
			return false;
		}
		equals = StringUtil.equals(this.getBackgroundUrl(), other.getBackgroundUrl());
		if (!equals) {
			return false;
		}
		return equals;

	}
}
