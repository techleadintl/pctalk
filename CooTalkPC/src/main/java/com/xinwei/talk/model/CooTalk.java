package com.xinwei.talk.model;

import com.xinwei.talk.model.Talk;

public class CooTalk extends Talk {
	String subscribeUID; //UID ,暂时不用
	String name; //CooTalk
	Integer sex; //性别
	String area; //地区
	String subTelNos; //子号码列表
	String remarkNick; //备注昵称
	String remarkTelno; //备注号码
	Integer topFlag;//消息置顶开关 0 开， 1 关
	Integer lockFlag;//消息免打扰开关 0 开， 1 关
	String backgroundUrl;//聊天背景
	byte[] background;

	String status; //200正常    201已拉黑

	public String getSubscribeUID() {
		return subscribeUID;
	}

	public void setSubscribeUID(String subscribeUID) {
		this.subscribeUID = subscribeUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getSubTelNos() {
		return subTelNos;
	}

	public void setSubTelNos(String subTelNos) {
		this.subTelNos = subTelNos;
	}

	public String getRemarkNick() {
		return remarkNick;
	}

	public void setRemarkNick(String remarkNick) {
		this.remarkNick = remarkNick;
	}

	public String getRemarkTelno() {
		return remarkTelno;
	}

	public void setRemarkTelno(String remarkTelno) {
		this.remarkTelno = remarkTelno;
	}

	public Integer getTopFlag() {
		return topFlag;
	}

	public void setTopFlag(Integer topFlag) {
		this.topFlag = topFlag;
	}

	public Integer getLockFlag() {
		return lockFlag;
	}

	public void setLockFlag(Integer lockFlag) {
		this.lockFlag = lockFlag;
	}

	public String getBackgroundUrl() {
		return backgroundUrl;
	}

	public void setBackgroundUrl(String backgroundUrl) {
		this.backgroundUrl = backgroundUrl;
	}

	public byte[] getBackground() {
		return background;
	}

	public void setBackground(byte[] background) {
		this.background = background;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
