package com.xinwei.talk.dao;

import com.xinwei.common.lang.Sqlite;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.model.*;
import com.xinwei.talk.model.message.TalkMessage;

import java.io.File;
import java.sql.SQLException;
import java.util.*;

public class TalkDaoImpl implements TalkDao {
	// 文件
	static Sqlite fileSqlite;
	// 用户配置
	static Sqlite configSqlite;

	// 用户
	static Sqlite userSqlite;

	// 群
	static Sqlite groupSqlite;

	// 头像
	static Sqlite infoSqlite;

	// 用户消息
	static Sqlite msgSqlite;

	// 联系人
	private static final String USER_SQL = "CREATE TABLE IF NOT EXISTS ORG_USER(UID VARCHAR(32) PRIMARY KEY, JID VARCHAR(32), NICK VARCHAR(32), TEL VARCHAR(32), AVATARURL TEXT, AVATAR BLOB, SIGNATURE TEXT, SUBSCRIBEUID VARCHAR(32),NAME VARCHAR(64), SEX SMALLINT, AREA TEXT, SUBTELNOS TEXT, REMARKNICK TEXT, REMARKTELNO VARCHAR(32), TOPFLAG SMALLINT, LOCKFLAG SMALLINT, BACKGROUNDURL TEXT, BACKGROUND BLOB, STATUS VARCHAR(8))";

	// 群组
	private static final String GROUP_SQL = "CREATE TABLE IF NOT EXISTS ORG_GROUP(ID VARCHAR(32) PRIMARY KEY, AVATARURL TEXT, CREATORUID VARCHAR(32), NAME VARCHAR(32), TYPE SMALLINT,MAXCOUNT INTEGER, DESCRIBE TEXT, ISALLOWINVITED SMALLINT, MSGDISTURBFLAG SMALLINT, TOPFLAG SMALLINT, LOCKFLAG SMALLINT, BACKGROUNDURL TEXT, BACKGROUND BLOB)";
	private static final String GROUP_MEMBER_SQL = "CREATE TABLE IF NOT EXISTS ORG_GROUP_MEMBER(GID VARCHAR(32), UID VARCHAR(32), AVATARURL TEXT, AVATAR BLOB, MEMJID  VARCHAR(32), USERKEY VARCHAR(32), MEMROLE SMALLINT,MEMNICK VARCHAR(32))";

	private static final String MSG_CHAT_SQL = "CREATE TABLE IF NOT EXISTS MSG_CHAT(ID BIGINT PRIMARY KEY, JID VARCHAR(64), USERTEL VARCHAR(32), PACKETID VARCHAR(32),TXT BLOB, MSGTYPE INTEGER,READED SMALLINT,SENDORRECEIVE SMALLINT, ISGROUP BOOLEAN,TIME TIMESTAMP)";
	private static final String MSG_CHAT_INDEX_SQL = "CREATE INDEX IF NOT EXISTS MSG_CHAT_INDEX ON MSG_CHAT(ID, JID)";

	private static final String USER_CONFIG_SQL = "CREATE TABLE IF NOT EXISTS CONFIG_USER(KEY VARCHAR(64) PRIMARY KEY, VALUE TEXT)";

	private static final String FILES_SQL = "CREATE TABLE IF NOT EXISTS FILES(MSGID BIGINT,URL TEXT,TYPE SMALLINT,DATA BLOB,PATH TEXT,DOWNLOAD BOOLEAN,FINISH BOOLEAN,PRIMARY KEY(MSGID,URL))";

	public void init() {
		// 信息
		File infoFile = TalkUtil.getUserFile("Info.db");
		infoSqlite = new Sqlite(infoFile.getAbsolutePath());
		createTable(infoSqlite, USER_CONFIG_SQL);

		// 文件
		File filesFile = TalkUtil.getUserFile("File1.db");
		fileSqlite = new Sqlite(filesFile.getAbsolutePath());
		createTable(fileSqlite, FILES_SQL);

		// 用户
		File userFile = TalkUtil.getUserFile("Friend1.0.db");
		userSqlite = new Sqlite(userFile.getAbsolutePath());
		createTable(userSqlite, USER_SQL);

		// 组
		File groupFile = TalkUtil.getUserFile("Friend2.0.db");
		groupSqlite = new Sqlite(groupFile.getAbsolutePath());
		createTable(groupSqlite, GROUP_SQL);
		createTable(groupSqlite, GROUP_MEMBER_SQL);

		// 用户消息
		File userMsgFile = TalkUtil.getUserFile("Msg1.0.db");
		msgSqlite = new Sqlite(userMsgFile.getAbsolutePath());

		createTable(msgSqlite, MSG_CHAT_SQL);
		createTable(msgSqlite, MSG_CHAT_INDEX_SQL);

	}

	/*************** 用户 *****************************/
	String SQL_INSERT_TALK = "INSERT INTO ORG_USER (UID,JID,NICK,TEL,AVATARURL,SIGNATURE,SUBSCRIBEUID,NAME,SEX,AREA,SUBTELNOS,REMARKNICK,REMARKTELNO,TOPFLAG,LOCKFLAG,BACKGROUNDURL,STATUS) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String SQL_UPDATE_TALK = "UPDATE ORG_USER SET JID=?,NICK=?,TEL=?,AVATARURL=?,SIGNATURE=?,SUBSCRIBEUID=?,NAME=?,SEX=?,AREA=?,SUBTELNOS=?,REMARKNICK=?,REMARKTELNO=?,TOPFLAG=?,LOCKFLAG=?,BACKGROUNDURL=?,STATUS=? WHERE UID = ?";

	public int addTalkList(List<? extends Talk> us) {
		try {
			List<Object[]> pl = new ArrayList<Object[]>();
			for (Talk c : us) {
				CooTalk u = (CooTalk) c;
				pl.add(new Object[] { u.getUid(), u.getJid(), u.getNick(), u.getTel(), u.getAvatarUrl(),
						u.getSignature(), u.getSubscribeUID(), u.getName(), u.getSex(), u.getArea(), u.getSubTelNos(),
						u.getRemarkNick(), u.getRemarkTelno(), u.getTopFlag(), u.getLockFlag(), u.getBackgroundUrl(),
						u.getStatus() });
			}

			return userSqlite.insertList(SQL_INSERT_TALK, pl);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int updateTalkList(List<? extends Talk> us) {
		try {
			List<Object[]> pl = new ArrayList<Object[]>();
			for (Talk c : us) {
				CooTalk u = (CooTalk) c;
				pl.add(new Object[] { u.getJid(), u.getNick(), u.getTel(), u.getAvatarUrl(), u.getSignature(),
						u.getSubscribeUID(), u.getName(), u.getSex(), u.getArea(), u.getSubTelNos(), u.getRemarkNick(),
						u.getRemarkTelno(), u.getTopFlag(), u.getLockFlag(), u.getBackgroundUrl(), u.getStatus(),
						u.getUid() });
			}
			return userSqlite.updateList(SQL_UPDATE_TALK, pl);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int updateTalkAvatar(String userTel, byte[] avatar) {
		try {
			String sql = "UPDATE ORG_USER SET AVATAR = ? WHERE TEL = ?";
			return userSqlite.update(sql, avatar, userTel);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int updateTalkBackground(String userTel, byte[] background) {
		try {
			String sql = "UPDATE ORG_USER SET BACKGROUND = ? WHERE TEL = ?";
			return userSqlite.update(sql, background, userTel);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int deleteTalkList(List<? extends Talk> camTalks) {
		if (camTalks == null || camTalks.isEmpty())
			return 0;

		String[] q = new String[camTalks.size()];
		Object[] v = new Object[camTalks.size()];
		for (int i = 0; i < camTalks.size(); i++) {
			q[i] = "?";
			v[i] = camTalks.get(i).getUid();
		}

		try {
			String sql = "DELETE FROM ORG_USER WHERE UID IN (" + StringUtil.join(q, ",") + ")";
			return userSqlite.update(sql, v);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int clearTalkList() {
		try {
			String sql = "DELETE FROM ORG_USER";
			return userSqlite.update(sql);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public List<? extends Talk> getTalkList() {
		try {
			String sql = "SELECT * FROM ORG_USER";
			return userSqlite.queryList(sql, CooTalk.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<? extends Talk> getTalkByJid(String jId) {
		try {
			String sql = "SELECT * FROM ORG_USER WHERE JID = ?";
			return userSqlite.queryList(sql, CooTalk.class, jId);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<? extends Talk> getTalkByTel(String tel) {
		try {
			String sql = "SELECT * FROM ORG_USER WHERE TEL = ?";
			return userSqlite.queryList(sql, CooTalk.class, tel);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*************** 组 *****************************/
	String SQL_INSERT_GROUP = "INSERT INTO ORG_GROUP (ID,AVATARURL,CREATORUID,NAME,TYPE,MAXCOUNT,DESCRIBE,ISALLOWINVITED,MSGDISTURBFLAG,TOPFLAG,LOCKFLAG, BACKGROUNDURL,BACKGROUND) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String SQL_INSERT_MEMBER = "INSERT INTO ORG_GROUP_MEMBER (GID,UID,AVATARURL,MEMJID,USERKEY,MEMROLE,MEMNICK) VALUES (?,?,?,?,?,?,?)";

	public int addGroupList(List<Group> gs, List<Member> ms) {

		List<Object[]> pl = new ArrayList<Object[]>();
		List<String> sqls = new ArrayList<String>();

		for (Group cg : gs) {
			CooGroup g = (CooGroup) cg;
			sqls.add(SQL_INSERT_GROUP);
			pl.add(new Object[] { g.getId(), g.getAvatarUrl(), g.getCreatorUid(), g.getName(), g.getGroupType(),
					g.getMaxCount(), g.getDescribe(), g.getIsAllowInvited(), g.getMsgDisturbFlag(), g.getTopFlag(),
					g.getLockFlag(), g.getBackgroundUrl(), g.getBackground() });
		}

		for (Member cm : ms) {
			CooMember m = (CooMember) cm;
			sqls.add(SQL_INSERT_MEMBER);
			pl.add(new Object[] { m.getGid(), m.getUid(), m.getAvatarUrl(), m.getMemJid(), m.getUserKey(),
					m.getMemRole(), m.getMemNick() });
		}

		try {
			return groupSqlite.insertTransaction(sqls, pl);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int deleteGroupList(List<Group> gs) {
		if (gs == null || gs.isEmpty())
			return -1;

		String[] q = new String[gs.size()];
		Object[] v = new Object[gs.size()];

		for (int i = 0; i < gs.size(); i++) {
			q[i] = "?";
			v[i] = gs.get(i).getId();
		}
		List<Object[]> pl = new ArrayList<Object[]>();
		List<String> sqls = new ArrayList<String>();

		String in = StringUtil.join(q, ",");

		sqls.add("DELETE FROM ORG_GROUP WHERE ID IN (" + in + ")");
		pl.add(v);

		sqls.add("DELETE FROM ORG_GROUP_MEMBER WHERE GID IN (" + in + ")");
		pl.add(v);

		try {
			return groupSqlite.updateTransaction(sqls, pl);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public List<Group> getGroupList() {
		try {
			String sql = "SELECT * FROM ORG_GROUP";
			return groupSqlite.queryList(sql, Group.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * get group from DB by given Group ID
	 * 
	 * @author nisal
	 * @param ID request ID of the group
	 **/
	public List<? extends Group> getGroupByID(String ID) {
		try {
			String sql = "SELECT * FROM ORG_GROUP WHERE ID = ?";
			return groupSqlite.queryList(sql, Group.class, ID);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int updateGroupList(List<Group> gs) {
		String sql = "UPDATE ORG_GROUP SET AVATARURL=?,CREATORUID=?,NAME=?,TYPE=?,MAXCOUNT=?,DESCRIBE=?,ISALLOWINVITED=?,MSGDISTURBFLAG=? WHERE ID = ?";
		List<Object[]> pl = new ArrayList<Object[]>();
		for (Group g : gs) {
			pl.add(new Object[] { g.getAvatarUrl(), g.getCreatorUid(), g.getName(), g.getGroupType(), g.getMaxCount(),
					g.getDescribe(), g.getIsAllowInvited(), g.getMsgDisturbFlag(), g.getId() });
		}
		try {
			return groupSqlite.updateList(sql, pl);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public int updateGroupBackground(String gid, byte[] background) {
		try {
			String sql = "UPDATE ORG_GROUP SET BACKGROUND = ? WHERE ID = ?";
			return groupSqlite.update(sql, background, gid);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	/*************** 组成员 *****************************/

	public List<Member> getMemberList() {
		try {
			String sql = "SELECT * FROM ORG_GROUP_MEMBER";
			return groupSqlite.queryList(sql, Member.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Member> getMemberList(String gid) {
		try {
			String sql = "SELECT * FROM ORG_GROUP_MEMBER WHERE GID = ?";
			return groupSqlite.queryList(sql, Member.class, gid);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Member> getMemberList(List<String> gids) {
		if (gids == null || gids.isEmpty())
			return getMemberList();

		String[] q = new String[gids.size()];
		Object[] v = new Object[gids.size()];
		for (int i = 0; i < gids.size(); i++) {
			q[i] = "?";
			v[i] = gids.get(i);
		}

		try {
			String sql = "SELECT * FROM ORG_GROUP_MEMBER WHERE GID IN (" + StringUtil.join(q, ",") + ")";
			return groupSqlite.queryList(sql, Member.class, v);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Member getMember(String gid, String uid) {
		try {
			String sql = "SELECT * FROM ORG_GROUP_MEMBER WHERE GID = ? AND UID = ?";
			return groupSqlite.query(sql, Member.class, gid, uid);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Member getMemberByJid(String gid, String jId) {
		try {
			String sql = "SELECT * FROM ORG_GROUP_MEMBER WHERE GID = ? AND MEMJID = ?";
			return groupSqlite.query(sql, Member.class, gid, jId);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int addMember(Member g) {
		try {
			String sql = "INSERT INTO ORG_GROUP_MEMBER (GID,UID,AVATARURL,MEMJID,USERKEY,MEMROLE,MEMNICK) VALUES (?,?,?,?,?,?,?)";
			return groupSqlite.insert(sql, g.getGid(), g.getUid(), g.getAvatarUrl(), g.getMemJid(), g.getUserKey(),
					g.getMemRole(), g.getMemNick());
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int addMemberList(Collection<Member> ms) {
		List<Object[]> pl = new ArrayList<Object[]>();

		String memberSql = "INSERT INTO ORG_GROUP_MEMBER (GID,UID,AVATARURL,MEMJID,USERKEY,MEMROLE,MEMNICK) VALUES (?,?,?,?,?,?,?)";
		for (Member m : ms) {
			pl.add(new Object[] { m.getGid(), m.getUid(), m.getAvatarUrl(), m.getMemJid(), m.getUserKey(),
					m.getMemRole(), m.getMemNick() });
		}

		try {
			return groupSqlite.insertList(memberSql, pl);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int updateMember(Member g) {
		try {
			String sql = "UPDATE ORG_GROUP_MEMBER SET AVATARURL = ?,MEMJID = ?,USERKEY = ?,MEMROLE = ?,MEMNICK= ?  WHERE GID = ? AND UID = ?";
			return groupSqlite.update(sql, g.getAvatarUrl(), g.getMemJid(), g.getUserKey(), g.getMemRole(),
					g.getMemNick(), g.getGid(), g.getUid());
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int updateMemberList(List<Member> ms) {
		String sql = "UPDATE ORG_GROUP_MEMBER SET AVATARURL = ?,MEMJID = ?,USERKEY = ?,MEMROLE = ?,MEMNICK= ?  WHERE GID = ? AND UID = ?";

		List<Object[]> pl = new ArrayList<Object[]>();
		for (Member m : ms) {
			pl.add(new Object[] { m.getAvatarUrl(), m.getMemJid(), m.getUserKey(), m.getMemRole(), m.getMemNick(),
					m.getGid(), m.getUid() });
		}
		try {
			return groupSqlite.updateList(sql, pl);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int deleteMemberList(String gid) {
		try {
			String sql = "DELETE FROM ORG_GROUP_MEMBER WHERE GID = ?";
			return groupSqlite.update(sql, gid);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int deleteMemberList(String gid, List<Member> memberList) {
		if (memberList == null || memberList.isEmpty())
			return -1;

		String[] q = new String[memberList.size()];
		Object[] v = new Object[memberList.size() + 1];
		v[0] = gid;
		for (int i = 0; i < memberList.size(); i++) {
			q[i] = "?";
			v[i + 1] = memberList.get(i).getUid();
		}

		try {
			String sql = "DELETE FROM ORG_GROUP_MEMBER WHERE GID = ? AND UID IN (" + StringUtil.join(q, ",") + ")";
			return groupSqlite.update(sql, v);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int updateMemberAvatar(String gid, String uid, byte[] avatar) {
		try {
			String sql = "UPDATE ORG_GROUP_MEMBER SET AVATAR = ? WHERE GID = ? AND UID = ?";
			return groupSqlite.update(sql, avatar, gid, uid);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	/*************** 用户消息 *****************************/
	// public int addCamTalkMessageList(List<CamTalkTranscriptMessage> cs) {
	// try {
	// String sql = "INSERT INTO MSG_USER (ID,JID,PACKETID,TXT,STATUS,TYPE,TIME)
	// VALUES (?,?,?,?,?,?,?)";
	//
	// List<Object[]> pl = new ArrayList<Object[]>();
	// for (CamTalkTranscriptMessage msg : cs) {
	// pl.add(new Object[] { msg.getId(), msg.getJid(), msg.getPacketId(),
	// msg.getTxt(), msg.getReaded(), msg.getSendOrReceive(), msg.getTime() });
	// }
	// return userMsgSqlite.insertList(sql, pl);
	// } catch (Exception e) {
	// e.printStackTrace();
	// return -1;
	// }
	// }
	//
	// public Map<String, Object> getCamTalkMessageTimeInfo(String userjid, String
	// searchStr, Date searchStartTime, Date searchEndTime) {
	// try {
	// String sql = null;
	// if (searchStr != null) {
	// sql = "SELECT COUNT(1) COUNT,MAX(TIME) MAXTIME,MIN(TIME) MINTIME,MAX(ID)
	// MAXTID,MIN(ID) MINID FROM MSG_USER WHERE USERJID = ? AND TXT LIKE ? AND TIME
	// BETWEEN ? AND ?";
	// } else {
	// sql = "SELECT COUNT(1) COUNT,MAX(TIME) MAXTIME,MIN(TIME) MINTIME,MAX(ID)
	// MAXTID,MIN(ID) MINID FROM MSG_USER WHERE USERJID = ? AND TIME BETWEEN ? AND
	// ?";
	// }
	// if (searchStr == null) {
	// return userMsgSqlite.queryMap(sql, userjid, searchStartTime, searchEndTime);
	// } else {
	// return userMsgSqlite.queryMap(sql, userjid, "%" + searchStr + "%",
	// searchStartTime, searchEndTime);
	// }
	// } catch (Exception e) {
	// return null;
	// }
	// }

	// public List<CamTalkTranscriptMessage> getCamTalkMessageList(String userjid,
	// String searchStr, long id, int num, boolean next) {
	// String sql = null;
	// if (next) {
	// if (searchStr == null) {
	// sql = "SELECT * FROM MSG_USER WHERE USERJID = ? AND ID > ? ORDER BY TIME ASC
	// LIMIT ?";
	// } else {
	// sql = "SELECT * FROM MSG_USER WHERE USERJID = ? AND TXT LIKE ? AND ID > ?
	// ORDER BY TIME ASC LIMIT ?";
	// }
	// } else {
	// if (searchStr == null) {
	// sql = "SELECT * FROM(SELECT * FROM MSG_USER WHERE USERJID = ? AND ID < ?
	// ORDER BY TIME DESC LIMIT ? ) ORDER BY TIME ASC";
	// } else {
	// sql = "SELECT * FROM(SELECT * FROM MSG_USER WHERE USERJID = ? AND TXT LIKE ?
	// AND ID < ? ORDER BY TIME DESC LIMIT ? ) ORDER BY TIME ASC";
	// }
	// }
	// try {
	// if (searchStr == null) {
	// return userMsgSqlite.queryList(sql, CamTalkTranscriptMessage.class, userjid,
	// id, num);
	// } else {
	// return userMsgSqlite.queryList(sql, CamTalkTranscriptMessage.class, userjid,
	// "%" + searchStr + "%", id, num);
	// }
	// } catch (Exception e) {
	// return null;
	// }
	// }
	//
	// public List<CamTalkTranscriptMessage> getCamTalkMessageList(String userjid,
	// Date date, int num) {
	// String sql = "SELECT * FROM MSG_USER WHERE USERJID = ? AND TIME >= ? ORDER BY
	// TIME ASC LIMIT ?";
	// try {
	// return userMsgSqlite.queryList(sql, CamTalkTranscriptMessage.class, userjid,
	// date, num);
	// } catch (Exception e) {
	// return null;
	// }
	// }

	// //查询最近联系人及消息
	// public List<CamTalkTranscriptMessage> getCamTalkMessageRecentList(int limit)
	// {
	// String sql = "SELECT T1.* FROM MSG_USER T1 INNER JOIN (SELECT * FROM (SELECT
	// USERJID,MAX(ID) AS ID FROM MSG_USER GROUP BY USERJID) ORDER BY ID DESC LIMIT
	// ? )T2 ON T1.USERJID=T2.USERJID AND T1.ID=T2.ID";
	//
	// try {
	// return userMsgSqlite.queryList(sql, CamTalkTranscriptMessage.class, limit);
	// } catch (Exception e) {
	// e.printStackTrace();
	// return null;
	// }
	// }

	// //查询未阅读消息最新一条
	// public List<CamTalkTranscriptMessage> getCamTalkLastMessageList(int status) {
	// String sql = "SELECT T1.*,T2.COUNT FROM MSG_USER T1 INNER JOIN (SELECT
	// USERJID,MAX(ID) AS ID, COUNT(1) AS COUNT FROM MSG_USER WHERE STATUS = ? GROUP
	// BY USERJID)T2 ON T1.USERJID=T2.USERJID AND T1.ID=T2.ID";
	//
	// try {
	// return userMsgSqlite.queryList(sql, CamTalkTranscriptMessage.class, status);
	// } catch (Exception e) {
	// e.printStackTrace();
	// return null;
	// }
	// }

	// public List<CamTalkTranscriptMessage> getCamTalkMessageList(int status) {
	// String sql = "SELECT * FROM MSG_USER WHERE STATUS = ?";
	//
	// try {
	// return userMsgSqlite.queryList(sql, CamTalkTranscriptMessage.class, status);
	// } catch (Exception e) {
	// e.printStackTrace();
	// return null;
	// }
	// }

	// /** 查询最大标识数 **/
	// public long getCamTalkMessageMaxId() {
	// try {
	// String sql = "SELECT MAX(ID) VALUE FROM MSG_USER";
	// return userMsgSqlite.query(sql, Integer.class);
	// } catch (Exception e) {
	// return 0;
	// }
	// }

	// public McWillList2<CamTalkTranscriptMessage>
	// getTodayCamTalkHistoryMessageList(String userId, long time) {
	// try {
	// String sql = "SELECT * FROM(SELECT * FROM MSG_USER WHERE USERJID = ? AND TIME
	// > ? ORDER BY TIME DESC LIMIT 100) ORDER BY TIME ASC";
	// return userMsgSqlite.queryList(sql, CamTalkTranscriptMessage.class, userId,
	// time);
	// } catch (Exception e) {
	// return null;
	// }
	// }

	// public int updateCamTalkMessageStatus(String userId, int status) {
	// try {
	// String sql = "UPDATE MSG_USER SET STATUS = ? WHERE USERJID = ?";
	// return userMsgSqlite.update(sql, status, userId);
	// } catch (Exception e) {
	// return -1;
	// }
	// }

	/*************** 消息 *****************************/
	public int removeMessageList(String jid) {
		String deleteSql = "DELETE FROM MSG_CHAT WHERE JID= ? ";
		try {
			List<Long> msgIdList = msgSqlite.queryList("SELECT ID FROM MSG_CHAT WHERE JID= ? ", Long.class, jid);
			msgSqlite.update(deleteSql, jid);

			return deleteFileList(msgIdList);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int deleteMessage(String jid, long msgId) {
		String sql = "DELETE FROM MSG_CHAT WHERE JID= ? AND ID = ?";
		try {
			msgSqlite.update(sql, jid, msgId);
			return deleteFile(msgId);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int addMessageList(List<TalkMessage> gs) {
		String sql = "INSERT INTO MSG_CHAT (ID,JID,USERTEL,PACKETID,TXT,MSGTYPE,READED,SENDORRECEIVE,ISGROUP,TIME) VALUES (?,?,?,?,?,?,?,?,?,?)";

		try {
			List<Object[]> pl = new ArrayList<Object[]>();
			for (TalkMessage msg : gs) {
				pl.add(new Object[] { msg.getId(), msg.getJid(), msg.getUserTel(), msg.getTalkBody().getPacketId(),
						msg.getTxt(), msg.getMsgType(), msg.getReaded(), msg.getSendOrReceive(), msg.getIsGroup(),
						msg.getTalkBody().getTime() });
			}
			return msgSqlite.insertList(sql, pl);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public Map<String, Object> getMessageTimeInfo(String jid, String searchStr, Date searchStartTime,
			Date searchEndTime) {
		try {
			String sql = null;
			if (searchStr != null) {
				sql = "SELECT COUNT(1) COUNT,MAX(TIME) MAXTIME,MIN(TIME) MINTIME,MAX(ID) MAXTID,MIN(ID) MINID FROM MSG_CHAT WHERE JID = ? AND TXT LIKE ? AND TIME BETWEEN ? AND ?";
			} else {
				sql = "SELECT COUNT(1) COUNT,MAX(TIME) MAXTIME,MIN(TIME) MINTIME,MAX(ID) MAXTID,MIN(ID) MINID FROM MSG_CHAT WHERE JID = ? AND TIME BETWEEN ? AND ?";
			}
			if (searchStr == null) {
				return msgSqlite.queryMap(sql, jid, searchStartTime, searchEndTime);
			} else {
				return msgSqlite.queryMap(sql, jid, "%" + searchStr + "%", searchStartTime, searchEndTime);
			}
		} catch (Exception e) {
			return null;
		}
	}

	public List<TalkMessage> getMessageList(String jid, String searchStr, long id, int num, boolean next) {
		String sql = null;
		if (next) {
			if (searchStr == null) {
				sql = "SELECT * FROM MSG_CHAT WHERE JID = ? AND ID > ? ORDER BY ID ASC LIMIT ?";
			} else {
				sql = "SELECT * FROM MSG_CHAT WHERE JID = ? AND TXT LIKE ? AND ID > ? ORDER BY ID ASC LIMIT ?";
			}
		} else {
			if (searchStr == null) {
				sql = "SELECT * FROM(SELECT * FROM MSG_CHAT WHERE JID = ? AND ID < ? ORDER BY ID DESC LIMIT ? ) ORDER BY ID ASC";
			} else {
				sql = "SELECT * FROM(SELECT * FROM MSG_CHAT WHERE JID = ? AND TXT LIKE ? AND ID < ? ORDER BY ID DESC LIMIT ? ) ORDER BY ID ASC";
			}
		}
		try {
			if (searchStr == null) {
				return msgSqlite.queryList(sql, TalkMessage.class, jid, id, num);
			} else {
				return msgSqlite.queryList(sql, TalkMessage.class, jid, "%" + searchStr + "%", id, num);
			}
		} catch (Exception e) {
			return null;
		}
	}

	public List<TalkMessage> getMessageList(String jid, Date date, int num) {
		String sql = "SELECT * FROM MSG_CHAT WHERE JID = ? AND TIME >= ? ORDER BY TIME ASC LIMIT ?";
		try {
			return msgSqlite.queryList(sql, TalkMessage.class, jid, date, num);
		} catch (Exception e) {
			return null;
		}
	}

	public TalkMessage getLastMessage(String jid) {
		String sql = "SELECT * FROM MSG_CHAT WHERE JID = ? ORDER BY TIME DESC LIMIT 1";
		try {
			return msgSqlite.query(sql, TalkMessage.class, jid);
		} catch (Exception e) {
			return null;
		}
	}

	/** 查询某种类型的消息 **/
	public List<TalkMessage> getMessageList(String jid, int type) {
		String sql = "SELECT * FROM MSG_CHAT WHERE JID = ? AND (?&MSGTYPE)>>? = 1 ORDER BY ID ASC LIMIT 50";
		try {
			return msgSqlite.queryList(sql, TalkMessage.class, jid, (int) Math.pow(2, (type - 1)), (type - 1));
		} catch (Exception e) {
			return null;
		}
	}

	/** 查询最大标识数 **/
	public long getMessageMaxId() {
		try {
			String sql = "SELECT MAX(ID) VALUE FROM MSG_CHAT";
			return msgSqlite.query(sql, int.class);
		} catch (Exception e) {
			return 0;
		}
	}

	// 查询最近联系人及消息
	public List<TalkMessage> getMessageRecentList(int limit) {
		String sql = "SELECT T1.* FROM MSG_CHAT T1 INNER JOIN (SELECT * FROM (SELECT JID,MAX(ID) AS ID FROM MSG_CHAT GROUP BY JID) ORDER BY ID DESC LIMIT ? )T2 ON T1.JID=T2.JID AND T1.ID=T2.ID";

		try {
			return msgSqlite.queryList(sql, TalkMessage.class, limit);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// 查询未阅读消息最新一条
	public List<TalkMessage> getLastMessageList(int read) {
		String sql = "SELECT T1.* ,T2.COUNT FROM MSG_CHAT T1 INNER JOIN (SELECT JID,MAX(ID) AS ID, COUNT(1) AS COUNT FROM MSG_CHAT WHERE READED = ? GROUP BY JID)T2 ON T1.JID=T2.JID AND T1.ID=T2.ID";

		try {
			return msgSqlite.queryList(sql, TalkMessage.class, read);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// 查询未阅读消息最新一条
	public List<TalkMessage> getMessageList(int readed) {
		String sql = "SELECT *  FROM MSG_CHAT  WHERE READED = ?";

		try {
			return msgSqlite.queryList(sql, TalkMessage.class, readed);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int updateMessageStatus(String jid, int readed) {
		try {
			String sql = "UPDATE MSG_CHAT SET READED = ? WHERE JID = ?";
			return msgSqlite.update(sql, readed, jid);
		} catch (Exception e) {
			return -1;
		}
	}

	@Override
	public List<TalkMessage> getUnReadMessageList(String jid, int readed) {
		String sql = "SELECT * FROM MSG_CHAT WHERE JID = ? AND READED = ? ORDER BY TIME ASC";
		try {
			return msgSqlite.queryList(sql, TalkMessage.class, jid, readed);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public int updateMsgReadStatus(long msgId, int readStatus) {

		try {
			String sql = "UPDATE MSG_CHAT SET READED = ? WHERE ID = ?";
			return msgSqlite.update(sql, readStatus, msgId);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}

	}

	/*************** 配置信息 *****************************/
	public int saveUserConfig(String key, String json) {
		try {
			int r = updateUserConfig(key, json);
			if (r < 1) {
				r = insertUserConfig(key, json);
			}
			return r;
		} catch (Exception e) {
			return -1;
		}
	}

	public int insertUserConfig(String key, String json) {
		try {
			String sql = "INSERT INTO CONFIG_USER (KEY,VALUE) VALUES (?,?)";
			return infoSqlite.insert(sql, key, json);
		} catch (Exception e) {
			return -1;
		}
	}

	public int updateUserConfig(String key, String json) {
		try {
			String sql = "UPDATE CONFIG_USER SET VALUE=? WHERE KEY=?";
			return infoSqlite.update(sql, json, key);
		} catch (Exception e) {
			return -1;
		}
	}

	public String getUserConfig(String key) {
		try {
			String sql = "SELECT VALUE FROM CONFIG_USER WHERE KEY = ?";
			return infoSqlite.query(sql, String.class, key);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<HashMap> getUserConfigList() {
		try {
			String sql = "SELECT * FROM CONFIG_USER";
			return infoSqlite.queryList(sql, HashMap.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*************** 文件 *****************************/
	public int addFile(long msgId, String url, int type, byte[] data, String path, boolean download, boolean finish) {
		try {
			String sql = "INSERT INTO FILES (MSGID,URL,TYPE,DATA,PATH,DOWNLOAD,FINISH) VALUES (?,?,?,?,?,?,?)";
			return fileSqlite.insert(sql, msgId, url, type, data, path, download, finish);
		} catch (Exception e) {
			return -1;
		}
	}

	public int updateFile(long msgId, String url, boolean finish) {
		try {
			String sql = "UPDATE FILES SET FINISH = ? WHERE MSGID = ? AND URL = ?";
			return fileSqlite.insert(sql, finish, msgId, url);
		} catch (Exception e) {
			return -1;
		}
	}

	public int deleteFile(long msgId) {
		try {
			String sql = "DELETE FROM FILES WHERE MSGID = ?";
			return fileSqlite.update(sql, msgId);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int deleteFileList(List<Long> msgIdList) {
		if (msgIdList == null || msgIdList.isEmpty())
			return 0;

		String[] q = new String[msgIdList.size()];
		Object[] v = new Object[msgIdList.size()];
		for (int i = 0; i < msgIdList.size(); i++) {
			q[i] = "?";
			v[i] = msgIdList.get(i);
		}

		try {
			String sql = "DELETE FROM FILES WHERE MSGID  IN (" + StringUtil.join(q, ",") + ")";
			return fileSqlite.update(sql, v);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}

	}

	public int deleteFile(long msgId, String url, int type) {
		try {
			String sql = "DELETE FROM FILES WHERE MSGID = ? AND URL = ? AND TYPE = ?";
			return fileSqlite.update(sql, msgId, url, type);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public TalkFile getFile(long msgId, String url, int type) {
		try {
			String sql = "SELECT * FROM FILES WHERE MSGID = ? AND URL = ? AND TYPE = ?";
			return fileSqlite.query(sql, TalkFile.class, msgId, url, type);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<TalkFile> getFileList(int type) {
		try {
			String sql = "SELECT * FROM FILES  WHERE TYPE = ?";
			return fileSqlite.queryList(sql, TalkFile.class, type);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<TalkFile> getFileList(int type, String... urls) {
		return getFileList(type, Arrays.asList(urls));
	}

	public List<TalkFile> getFileList(int type, List<String> urls) {
		if (urls == null || urls.isEmpty()) {
			return getFileList(type);
		}

		String[] q = new String[urls.size()];
		Object[] v = new Object[urls.size() + 1];
		v[0] = type;
		for (int i = 0; i < urls.size(); i++) {
			q[i] = "?";
			v[i + 1] = urls.get(i);
		}

		try {
			String sql = "SELECT * FROM FILES WHERE TYPE = ? AND URL IN (" + StringUtil.join(q, ",") + ")";
			return fileSqlite.queryList(sql, TalkFile.class, v);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*************** 通讯录 *****************************/
	public int replaceContactList(List<Contact> cs) {
		return -1;
	}

	public List<Contact> getContactList() {
		return null;
	}

	private int createTable(Sqlite sqlite, String sql) {
		try {
			return sqlite.createTable(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

}
