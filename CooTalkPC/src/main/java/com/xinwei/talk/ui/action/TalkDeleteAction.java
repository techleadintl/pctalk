/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月16日 下午3:52:57
 * 
 ***************************************************************/
package com.xinwei.talk.ui.action;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.talk.action.talk.TalkAction;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Talk;

public class TalkDeleteAction extends TalkAction {
	public TalkDeleteAction() {
		setLabel(Res.getMessage("action.delete"));
		setIcon(LAF.getDeleteIcon()[0]);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			int deleteTalk = TalkManager.getTalkService().deleteTalk(obj, null);
			if (deleteTalk == 0) {
				List<Talk> talkList = new ArrayList<Talk>();
				talkList.add(obj);
				TalkManager.getVCardManager().deleteTalk(talkList);
			}
		} catch (Exception e1) {
			Log.error(e1);
		}
	}

	public boolean isVisible() {
		String loginUid = TalkManager.getVCard().getUid();
		String memUid = obj.getUid();
		if (StringUtil.equals(memUid, loginUid)) {//自己不跟自己聊天
			return false;
		}

		return true;
	}
}
