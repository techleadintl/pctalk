package com.xinwei.talk.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JRootPane;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.contentpanel.McWillContentPanel;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.ui.main.dialog.group.ListPanel;
import com.xinwei.talk.ui.util.component.XWDialogTitle;
import com.xinwei.talk.ui.util.component.dialog.XWDialog;

@SuppressWarnings("serial")
public class CooTalkGroupDialog extends XWDialog {
	private ListPanel memberPanel;
	
	/**
	 * Create a new instance of RosterDialog.
	 */
	public CooTalkGroupDialog() {
		super(TalkManager.getMainWindow(), "", false);
		createDialog();
		JRootPane rootPane = getRootPane();
		((BaseRootPaneUI) rootPane.getUI()).setTitlePane(rootPane, null);

		setSize(new Dimension(575, 450));
	}

	@Override
	protected JPanel createDialogContainer() {
		return new McWillContentPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				LAF.paintMainAddGroupDialog(this, (Graphics2D) g);
			}
		};
	}

	@Override
	protected JPanel createDialogContent() {
		JPanel dialogPanel = new JPanel(new BorderLayout());
		XWDialogTitle titlePanel = new XWDialogTitle(this, Res.getMessage("tabTitle.group.add"));
		titlePanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 0));
		dialogPanel.add(titlePanel, BorderLayout.NORTH);

		dialogPanel.add(createInputPanel(), BorderLayout.CENTER);

		return dialogPanel;
	}

	private JPanel createInputPanel() {
		JPanel inputPanel = new JPanel();

		memberPanel = new ListPanel();

		inputPanel.setLayout(new GridBagLayout());

		inputPanel.add(memberPanel, new GridBagConstraints(0, 4, 2, 1, 1.0D, 1.0D, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

		return inputPanel;
	}

	@Override
	protected boolean onValidate() {
		return true;
	}

	@Override
	protected boolean onOkPressed() {
		VCard vCard = TalkManager.getVCard();
		String creator = String.valueOf(vCard.getUid());

		Group xwGroup = new Group();
		xwGroup.setCreatorUid(creator);

		List<Member> memberList = new ArrayList<>();

		Member self = new Member();
		self.setUid(creator);
		self.setMemNick(vCard.getNick());
		self.setMemJid(vCard.getJid());
		self.setMemRole(Member.ROLE_ADMIN);

		memberList.add(self);

		Enumeration<Talk> selectedContacts = memberPanel.getSelectedContacts();
		while (selectedContacts.hasMoreElements()) {
			Talk camTalk = selectedContacts.nextElement();
			if (StringUtil.notEquals(camTalk.getUid(), creator)) {
				Member m2 = new Member();
				m2.setUid(camTalk.getUid());
				m2.setMemNick(TalkUtil.getDisplayName(camTalk.getJid()));
				m2.setMemJid(camTalk.getJid());
				m2.setMemRole(Member.ROLE_USER);

				memberList.add(m2);
			}
		}
		try {
			String groupId = TalkManager.getTalkService().createGroup(xwGroup, memberList);
			xwGroup.setId(groupId);
			return super.onOkPressed();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	protected LayoutManager getButtonBarLayout() {
		return new FlowLayout(FlowLayout.RIGHT, 30, 5);
	}

	protected JPanel createButtonBar() {
		JPanel bottomButtonPanel = new JPanel(getButtonBarLayout());
		bottomButtonPanel.setOpaque(false);

		closeButton = SwingUtil.newButton(closeTxt);
		okButton = SwingUtil.newButton(Res.getMessage("button.group.add.ok"));

		okButton.addActionListener(this);
		closeButton.addActionListener(this);

		bottomButtonPanel.add(closeButton);
		bottomButtonPanel.add(okButton);

		return bottomButtonPanel;
	}
}