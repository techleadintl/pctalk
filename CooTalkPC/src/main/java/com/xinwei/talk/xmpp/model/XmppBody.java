package com.xinwei.talk.xmpp.model;

public class XmppBody {
	public static int MESSAGE_TYPE_TEXT = 0; //文本类型
	public static int MESSAGE_TYPE_IMAGE = 1; //图片类型
	public static int MESSAGE_TYPE_CARD = 2; //名片类型
	public static int MESSAGE_TYPE_VOICE = 3; //语音类型
	public static int MESSAGE_TYPE_LOCATION = 4; //位置类型
	public static int MESSAGE_TYPE_GROUP = 5; //组的推送消息
	public static int MESSAGE_TYPE_FILE = 19; //文件
	
	public static int MESSAGE_MODEL_CHAT = 0; //单聊
	public static int MESSAGE_MODEL_GROUPCHAT = 1; //群聊
	//消息时间
	private String time;
	//消息id:PC-uid-time
	private String messageId;
	////消息模型   0:单聊,1:群聊,2：公众号
	private int messageModel;
	//消息类型
	private int messageType;
	//聊天对象:单聊：自己Tel，群聊：群UID
	private String chatId;
	//消息体
	private Object detail;

	//发送者
	private String sender;
	//拓展
	private String ext;

	private XmppParam param;

	public XmppBody() {
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public int getMessageModel() {
		return messageModel;
	}

	public void setMessageModel(int messageModel) {
		this.messageModel = messageModel;
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

	public Object getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public XmppParam getParam() {
		return param;
	}

	public void setParam(XmppParam param) {
		this.param = param;
	}

}
