/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月22日 下午4:52:12
 * 
 ***************************************************************/
package com.xinwei.talk.xmpp.listener;

import org.jivesoftware.smack.packet.Message;

import com.xinwei.talk.xmpp.model.XmppBodies;

public interface XmppMessageListener {
	public void processXmppMessage(Message message, XmppBodies xmppBodies);
}
