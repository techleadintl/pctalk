package com.xinwei.talk.xmpp.listener;

import com.xinwei.common.lang.GsonUtil;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.xmpp.model.XmppBodies;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class XmppListener implements PacketListener {
	protected final Map<Type, XmppMessageListener> recvListeners = new ConcurrentHashMap<Type, XmppMessageListener>();
	protected final Map<Presence.Type, XmppPresenceListener> recvPresenceListeners = new ConcurrentHashMap<Presence.Type, XmppPresenceListener>();
	private List<MessageFilter> messageFilters = new ArrayList<MessageFilter>();

	public XmppListener() {

	}

	public void addMessageListener(Type type, XmppMessageListener xmppMessageListener) {
		if (xmppMessageListener == null) {
			throw new NullPointerException("XmppMessage listener is null.");
		}
		recvListeners.put(type, xmppMessageListener);
	}

	public void addXmppPresenceListener(Presence.Type type, XmppPresenceListener xmppPresenceListener) {
		if (xmppPresenceListener == null) {
			throw new NullPointerException("XmppPresence listener is null.");
		}
		recvPresenceListeners.put(type, xmppPresenceListener);
	}

	public void addMessageFilter(MessageFilter filter) {
		messageFilters.add(filter);
	}

	@Override
	public void processPacket(final Packet packet) {
		if (packet instanceof Presence) {
			handlePresencePacket((Presence) packet);

		}
		if (packet instanceof Message) {
			handleMessagePacket((Message) packet);
		}
	}

	/**
	 * manage all packets received from Smack library
	 **/
	protected void handleMessagePacket(Message message) {
		System.out.println("handleMessagePacket:" + message.toXML());
		XmppBodies xmppBodies = GsonUtil.fromJson(message.getBody(), XmppBodies.class);

		// 过滤消息
		for (MessageFilter filter : messageFilters) {
			boolean f = filter.filter(message, xmppBodies);
			if (f) {
				return;
			}
		}
		Type type = message.getType();
		XmppMessageListener xmppMessageListener = recvListeners.get(type);
		if (xmppMessageListener != null) {
			xmppMessageListener.processXmppMessage(message, xmppBodies);
		}
	}

	protected void handlePresencePacket(Presence presence) {
		System.out.println("handlePresencePacket:" + presence.toXML());
		ListenerManager.firePresenceReceived(presence.getFrom(),presence);
		Presence.Type type = presence.getType();
		XmppPresenceListener xmppPresenceListener = recvPresenceListeners.get(type);
		if (xmppPresenceListener != null) {
			xmppPresenceListener.processXmppPresence(presence);
		}
	}

}
