package com.xinwei.talk.xmpp.model;

import java.util.List;

public class XmppBodies {
	private List<XmppBody> bodies;
	private int isOffline;

	public List<XmppBody> getBodies() {
		return bodies;
	}

	public int getIsOffline() {
		return isOffline;
	}

	public void setIsOffline(int isOffline) {
		this.isOffline = isOffline;
	}

	public void setBodies(List<XmppBody> bodies) {
		this.bodies = bodies;
	}

	public XmppBodies() {
	}
}
