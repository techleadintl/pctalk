package com.xinwei.talk.xmpp.extension;

import java.text.MessageFormat;

import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;

public class ReceiptMessage extends Packet {
	//	<message to="weiquan#camtalk_008615652486096@camito.cn/mobile" from="weiquan#camtalk_008615652486096@camito.cn/mobile" type="headline"><body>M008615652486096_1489988860688</body><x xmlns="jabber:x:delay" stamp="20170320T05:50:25"></x></message>
	String pattern = "<message id=\"{0}\" from=\"{1}\" to=\"{2}\" type=\"headline\"><body>xmpp:receipts</body><x xmlns=\"jabber:x:delay\"></x></message>";

	@Override
	public String toXML() {
		String format = MessageFormat.format(pattern, getPacketID(), getTo(), StringUtils.escapeForXML(getFrom()));
		return format;
	}

}
