package com.xinwei.talk.xmpp.model.specific;

import com.xinwei.util.Constants;
import com.xinwei.xmpp.model.XmppSpecific;

//图片消息
public class XmppImageSpecific extends XmppSpecific {

	public XmppImageSpecific(String thumbnailPath, String absolutePath, int size, int height, int width,
			String userName, String userAvatar, String uid) {
		set("fileThumbnailPath", thumbnailPath);
		set("fileAbsolutePath", absolutePath);
		set("size", size);
		set("height", height);
		set("width", width);

		// added by Nisal to send, sender's details with message
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_NAME, userName);
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_AVATAR, userAvatar);
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_ID, uid);
	}
}
