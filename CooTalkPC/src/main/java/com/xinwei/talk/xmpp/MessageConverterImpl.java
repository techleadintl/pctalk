package com.xinwei.talk.xmpp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jivesoftware.smack.packet.Message;

import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.DateUtil;
import com.xinwei.common.lang.FileUtil;
import com.xinwei.common.lang.GsonUtil;
import com.xinwei.common.vo.McWillMap;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.message.MessageConverter;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.model.message.specific.TalkGroupSpecific;
import com.xinwei.talk.model.message.specific.TalkImageSpecific;
import com.xinwei.talk.model.message.specific.TalkLocationSpecific;
import com.xinwei.talk.model.message.specific.TalkNameCardSpecific;
import com.xinwei.talk.model.message.specific.TalkParam;
import com.xinwei.talk.model.message.specific.TalkTextSpecific;
import com.xinwei.talk.model.message.specific.TalkVoiceSpecific;
import com.xinwei.talk.xmpp.model.XmppBodies;
import com.xinwei.talk.xmpp.model.XmppBody;
import com.xinwei.talk.xmpp.model.XmppParam;
import com.xinwei.talk.xmpp.model.specific.XmppImageSpecific;
import com.xinwei.talk.xmpp.model.specific.XmppLocationSpecific;
import com.xinwei.talk.xmpp.model.specific.XmppNameCardSpecific;
import com.xinwei.talk.xmpp.model.specific.XmppOfflineFileSpecific;
import com.xinwei.talk.xmpp.model.specific.XmppTextSpecific;
import com.xinwei.talk.xmpp.model.specific.XmppVoiceSpecific;
import com.xinwei.util.Constants;
import com.xinwei.xmpp.model.XmppSpecific;

public class MessageConverterImpl implements MessageConverter {
	/**
	 * convert server response to class objects according to message type
	 **/
	public TalkBody[] conver2TalkBody(Object obj) {
		if (!(obj instanceof XmppBodies))
			return null;
		XmppBodies xmppBodies = (XmppBodies) obj;
		List<XmppBody> bodies = xmppBodies.getBodies();
		if (CollectionUtil.isEmpty(bodies)) {
			return null;
		}
		List<TalkBody> result = new ArrayList<TalkBody>();
		for (XmppBody xmppBody : bodies) {
			TalkBody talkBody = getTalkBody(xmppBody);
			if (talkBody != null) {
				result.add(talkBody);
			}
		}
		return result.toArray(new TalkBody[0]);
	}

	/**
	 * create common TalkBody class object with message specifications (text, image,
	 * voice, file)
	 * 
	 * @param xmppBody server response
	 **/
	private TalkBody getTalkBody(XmppBody xmppBody) {
		Object detail = xmppBody.getDetail();
		if (detail == null)
			return null;

		TalkBody talkBody = new TalkBody();
		int messageModel = xmppBody.getMessageModel();
		int messageType = xmppBody.getMessageType();

		talkBody.setFrom(xmppBody.getSender());
		talkBody.setTo(xmppBody.getChatId());
		talkBody.setType(messageModel);

		// 如果不是XMPP push message
		if (messageType != XmppBody.MESSAGE_TYPE_GROUP) {
			XmppParam xmppParam = xmppBody.getParam();
			if (xmppParam != null) {
				talkBody.setParam(new TalkParam(xmppParam.getF(), xmppParam.getS(), xmppParam.getU(), xmppParam.getI(),
						xmppParam.getB(), xmppParam.getFg(), xmppParam.getBg(), xmppParam.getBalloon(),
						xmppParam.getBalloonType()));
			} else {
				talkBody.setParam(new TalkParam());
			}
		}

		String ext = xmppBody.getExt();
		if (ext != null) {
		}

		// separate server response to specific messages (text, image, voice, file)
		TalkSpecific talkSpecific = getTalkSpecific(detail, messageType);

		if (talkSpecific != null) {
			talkBody.addSpecific(talkSpecific);
		}
		talkBody.setTime(DateUtil.getDate(xmppBody.getTime()));
		talkBody.setPacketId(xmppBody.getMessageId());

		return talkBody;
	}

	/**
	 * convert specific class object to server acceptable object
	 * 
	 * @param talkBody custom class object
	 * @param type     type of current message, that trying to send to the server
	 **/
	public XmppBody conver2XmppBody(TalkBody talkBody, Message.Type type) {
		if (talkBody == null)
			return null;
		XmppBody xmppBody = new XmppBody();

		xmppBody.setMessageId(talkBody.getPacketId());
		xmppBody.setSender(talkBody.getFrom());
		xmppBody.setChatId(talkBody.getTo());
		// xmppBody.setChatId(talkBody.getFrom());
		if (type == Message.Type.chat) {
			xmppBody.setMessageModel(XmppBody.MESSAGE_MODEL_CHAT);
			xmppBody.setChatId(talkBody.getFrom());
		} else {
			xmppBody.setMessageModel(XmppBody.MESSAGE_MODEL_GROUPCHAT);
			xmppBody.setChatId(talkBody.getTo());
		}

		TalkParam talkParam = talkBody.getParam();
		if (talkParam != null) {
			xmppBody.setParam(new XmppParam(talkParam.getF(), talkParam.getS(), talkParam.getU(), talkParam.getI(),
					talkParam.getB(), talkParam.getFg(), talkParam.getBg(), talkParam.getBalloon(),
					talkParam.getBalloonType()));
		}

		List<TalkSpecific> msgList = talkBody.getSpecificList();
		if (msgList != null && !msgList.isEmpty()) {
			TalkSpecific talkSpecific = msgList.get(0);
			// convert all specific messages to server acceptable objects
			Object[] detail = getXmppSpecific(talkSpecific, type);
			if (detail == null) {
				return null;
			}
			Object v = detail[1];
			if (v instanceof String) {
				xmppBody.setDetail((String) v);
			} else if (v instanceof XmppSpecific) {
				xmppBody.setDetail(GsonUtil.toJson(v));
			}
			xmppBody.setMessageType((int) detail[0]);
		}
		return xmppBody;
	}

	/**
	 * create specific message(text, image, file, name card, etc...), from given
	 * received message details
	 * 
	 * @param detail      received message object
	 * @param messageType current message type(text, image, file, name card, etc...)
	 **/
	private TalkSpecific getTalkSpecific(Object detail, int messageType) {

		XmppSpecific xmppSpecific = null;
		if (detail instanceof String) {
			xmppSpecific = GsonUtil.fromJson((String) detail, XmppSpecific.class);
		} else if (detail instanceof Map) {
			xmppSpecific = new XmppSpecific((Map) detail);
		}

		// added by Nisal to process the text messages without sender's details
		if (xmppSpecific == null && messageType == XmppBody.MESSAGE_TYPE_TEXT) {
			return new TalkTextSpecific((String) detail);
		}

		if (xmppSpecific == null) {
			return null;
		}

		if (messageType == XmppBody.MESSAGE_TYPE_TEXT) {

			// added by Nisal to retrieve, stranger sender's details from message
			TalkTextSpecific talkTextSpecific = new TalkTextSpecific();
			talkTextSpecific.setMsg(xmppSpecific.getString(XmppTextSpecific.TEXT_SPECIFIC_MESSAGE_USER_MESSAGE));
			talkTextSpecific.setSenderAvatar(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_AVATAR));
			talkTextSpecific.setSenderName(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_NAME));
			talkTextSpecific.setSenderUID(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_ID));

			return talkTextSpecific;

		} else if (messageType == XmppBody.MESSAGE_TYPE_IMAGE) {

			String url = xmppSpecific.getString("fileAbsolutePath");// Original image file download address

			TalkImageSpecific talkImageSpecific = new TalkImageSpecific(url, FileUtil.getFileName(url));
			talkImageSpecific.addAttachment("fileAbsolutePath", url);// Original image file download address
			// Thumbnail file download address
			talkImageSpecific.addAttachment("fileThumbnailPath", xmppSpecific.getString("fileThumbnailPath"));
			talkImageSpecific.addAttachment("fileSize", xmppSpecific.getInteger("fileSize"));// Original image size,
																								// unit byte
			talkImageSpecific.addAttachment("width", xmppSpecific.getInteger("width")); // Thumbnail width
			talkImageSpecific.addAttachment("height", xmppSpecific.getInteger("height")); // Thumbnail height

			// added by Nisal to set sender's details
			talkImageSpecific.setSenderAvatar(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_AVATAR));
			talkImageSpecific.setSenderName(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_NAME));
			talkImageSpecific.setSenderUID(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_ID));

			return talkImageSpecific;

		} else if (messageType == XmppBody.MESSAGE_TYPE_FILE) {

			TalkFileSpecific talkFileSpecific = new TalkFileSpecific(
					xmppSpecific.getString("url") != null ? xmppSpecific.getString("url")
							: xmppSpecific.getString("fileAbsolutePath"),
					null != xmppSpecific.getString("filename") ? xmppSpecific.getString("filename")
							: xmppSpecific.getString("fileName"),
					0 != xmppSpecific.getInteger("length", 0) ? xmppSpecific.getInteger("length", 0)
							: xmppSpecific.getInteger("size", 0));

			// added by Nisal to set sender's details
			talkFileSpecific.setSenderAvatar(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_AVATAR));
			talkFileSpecific.setSenderName(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_NAME));
			talkFileSpecific.setSenderUID(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_ID));

			return talkFileSpecific;

		} else if (messageType == XmppBody.MESSAGE_TYPE_VOICE) {

			int duration = xmppSpecific.getInteger("voiceDuration", 0);// 时长
			String url = xmppSpecific.getString("voiceUrl");// File address

			TalkVoiceSpecific talkVoiceSpecific = new TalkVoiceSpecific(url, FileUtil.getFileName(url), duration);

			// added by Nisal to set sender's details
			talkVoiceSpecific.setSenderAvatar(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_AVATAR));
			talkVoiceSpecific.setSenderName(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_NAME));
			talkVoiceSpecific.setSenderUID(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_ID));

			return talkVoiceSpecific;

		} else if (messageType == XmppBody.MESSAGE_TYPE_LOCATION) {

			String lng = xmppSpecific.getString("lon", "0");// 经度
			String lat = xmppSpecific.getString("lat", "0");// 纬度
			String locationName = xmppSpecific.getString("locationName", "");
			String locationDescribe = xmppSpecific.getString("lat", "locationDescribe");

			return new TalkLocationSpecific(lat, lng, locationName, locationDescribe);

		} else if (messageType == XmppBody.MESSAGE_TYPE_CARD) {

			TalkNameCardSpecific talkNameCardSpecific = new TalkNameCardSpecific();
			talkNameCardSpecific.setUid(xmppSpecific.getString("cid"));
			talkNameCardSpecific.setMobile(xmppSpecific.getString("telNo"));
			talkNameCardSpecific.setNick(xmppSpecific.getString("nick"));
			talkNameCardSpecific.setSex(xmppSpecific.getInteger("sex", 1));
			talkNameCardSpecific.setSignature(xmppSpecific.getString("signature"));
			talkNameCardSpecific.setArea(xmppSpecific.getString("area"));
			talkNameCardSpecific.setPicture(xmppSpecific.getString("picture"));

			// added by Nisal to set sender's details
			talkNameCardSpecific.setSenderAvatar(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_AVATAR));
			talkNameCardSpecific.setSenderName(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_NAME));
			talkNameCardSpecific.setSenderUID(xmppSpecific.getString(Constants.TEXT_SPECIFIC_MESSAGE_USER_ID));

			return talkNameCardSpecific;

		} else if (messageType == XmppBody.MESSAGE_TYPE_GROUP) {

			return new TalkGroupSpecific(xmppSpecific);

		}

		return null;

	}

	/**
	 * convert all specific messages to server acceptable objects
	 * 
	 * @param msg  custom specific class object with message
	 * @param type custom specific class message type
	 * @return server acceptable object
	 */
	private Object[] getXmppSpecific(TalkSpecific msg, Message.Type type) {
		VCard card = TalkManager.getVCard();

		if (msg instanceof TalkTextSpecific) {// text
			TalkTextSpecific m = (TalkTextSpecific) msg;

			// added by Nisal - to send the sender's full details to the receiver, for group
			// chat purposes
//			XmppTextSpecific xmppTextSpecific = new XmppTextSpecific(card.getNick(), card.getPicture(), card.getUid(),
//					m.getMsg());

			return new Object[] { XmppBody.MESSAGE_TYPE_TEXT, m.getMsg() };
		} else if (msg instanceof TalkImageSpecific) {// image

			TalkImageSpecific m = (TalkImageSpecific) msg;
			Map attachment = m.getAttachment();
			String absolutePath = McWillMap.getMapString(attachment, "fileAbsolutePath");
			String thumbnailPath = McWillMap.getMapString(attachment, "fileThumbnailPath");
			int size = McWillMap.getMapInteger(attachment, "fileSize", 0);
			int width = McWillMap.getMapInteger(attachment, "width", 0);
			int height = McWillMap.getMapInteger(attachment, "height", 0);

			// added by Nisal - to send the sender's full details to the receiver, for group
			// chat purposes
			XmppImageSpecific xmppImageSpecific = new XmppImageSpecific(thumbnailPath, absolutePath, size, height,
					width, card.getNick(), card.getPicture(), card.getUid());

			return new Object[] { XmppBody.MESSAGE_TYPE_IMAGE, xmppImageSpecific };

		} else if (msg instanceof TalkVoiceSpecific) {// sound

			TalkVoiceSpecific m = (TalkVoiceSpecific) msg;

			// added by Nisal - to send the sender's full details to the receiver, for group
			// chat purposes
			XmppVoiceSpecific xmppVoiceSpecific = new XmppVoiceSpecific(m.getUrl(), m.getLength(), card.getNick(),
					card.getPicture(), card.getUid());

			return new Object[] { XmppBody.MESSAGE_TYPE_VOICE, xmppVoiceSpecific };

		} else if (msg instanceof TalkFileSpecific) {// file

			TalkFileSpecific m = (TalkFileSpecific) msg;

			// added by Nisal - to send the sender's full details to the receiver, for group
			// chat purposes
			XmppOfflineFileSpecific xmppOfflineFileSpecific = new XmppOfflineFileSpecific(m.getUrl(), m.getFilename(),
					m.getLength(), "", "", card.getNick(), card.getPicture(), card.getUid());

			return new Object[] { XmppBody.MESSAGE_TYPE_FILE, xmppOfflineFileSpecific };

		} else if (msg instanceof TalkLocationSpecific) {// gps

			TalkLocationSpecific m = (TalkLocationSpecific) msg;
			XmppLocationSpecific xmppLocationSpecific = new XmppLocationSpecific(m.getLng(), m.getLat(), m.getTitle(),
					m.getAddr());

			return new Object[] { XmppBody.MESSAGE_TYPE_LOCATION, xmppLocationSpecific };

		} else if (msg instanceof TalkNameCardSpecific) {// name card

			TalkNameCardSpecific m = (TalkNameCardSpecific) msg;
			XmppNameCardSpecific xmppNameCardSpecific = new XmppNameCardSpecific(m.getUid(), m.getMobile(), m.getNick(),
					m.getSex(), m.getSignature(), m.getArea(), m.getPicture(), card.getNick(), card.getPicture(),
					card.getUid());

			return new Object[] { XmppBody.MESSAGE_TYPE_CARD, xmppNameCardSpecific };

		} else {
		}
		return null;
	}

}
