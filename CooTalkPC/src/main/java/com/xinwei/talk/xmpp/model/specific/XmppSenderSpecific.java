package com.xinwei.talk.xmpp.model.specific;

import com.xinwei.xmpp.model.XmppSpecific;

/**
 * added to send sender's correct details to the receiver for all messages
 * 
 * @author nisal
 **/
public class XmppSenderSpecific extends XmppSpecific {
	public final static String TEXT_SPECIFIC_MESSAGE_USER_NAME = "userName";
	public final static String TEXT_SPECIFIC_MESSAGE_USER_AVATAR = "userAvatar";
	public final static String TEXT_SPECIFIC_MESSAGE_USER_ID = "userID";

	public XmppSenderSpecific(String userName, String userAvatar, String uid) {
		set(XmppSenderSpecific.TEXT_SPECIFIC_MESSAGE_USER_NAME, userName);
		set(XmppSenderSpecific.TEXT_SPECIFIC_MESSAGE_USER_AVATAR, userAvatar);
		set(XmppSenderSpecific.TEXT_SPECIFIC_MESSAGE_USER_ID, uid);
	}

}
