package com.xinwei.talk.xmpp.extension;

import java.util.Date;

import org.jivesoftware.smack.packet.PacketExtension;

public class ClientInformation implements PacketExtension {

	private Date date;

	public ClientInformation() {
		this.date = new Date();
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getElementName() {
		return "info";
	}

	public String getNamespace() {
		return "jabber:client";
	}

	public String toXML() {
		StringBuilder buf = new StringBuilder();
		buf.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\"");

		
		buf.append("</").append(getElementName()).append(">");
		return buf.toString();
	}
}
