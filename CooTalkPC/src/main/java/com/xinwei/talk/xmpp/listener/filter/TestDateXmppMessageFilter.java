/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2017年1月19日 上午11:03:26
 * 
 ***************************************************************/
package com.xinwei.talk.xmpp.listener.filter;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smackx.packet.DelayInformation;

import com.xinwei.common.lang.SyncLock;
import com.xinwei.talk.xmpp.listener.MessageFilter;
import com.xinwei.talk.xmpp.model.XmppBodies;

public class TestDateXmppMessageFilter implements MessageFilter {
	@Override
	public boolean filter(Message message, XmppBodies xmppBodies) {
		if (xmppBodies != null) {
			return false;
		}
		if (message.getType() != Type.chat) {
			return false;
		}
		String body = message.getBody();
		if (!"test-date".equals(body)) {
			return false;
		}

		DelayInformation delayInformation = (DelayInformation) message.getExtension("x", "jabber:x:delay");
		if (delayInformation == null) {
			return false;
		}

		SyncLock.setContent(message.getPacketID(), delayInformation.getStamp());
		SyncLock.unlock(message.getPacketID());

		return true;
	}

}