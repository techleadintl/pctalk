package com.xinwei.talk.xmpp.model.specific;

import com.xinwei.util.Constants;
import com.xinwei.xmpp.model.XmppSpecific;

//文本消息
/**
 * added to send sender's correct details to the receiver for text messages
 * 
 * @author nisal
 **/
public class XmppTextSpecific extends XmppSpecific {
	public final static String TEXT_SPECIFIC_MESSAGE_USER_MESSAGE = "message";

	public XmppTextSpecific(String userName, String userAvatar, String uid, String message) {
		set(XmppTextSpecific.TEXT_SPECIFIC_MESSAGE_USER_MESSAGE, message);

		// added by Nisal to send, sender's details with message
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_NAME, userName);
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_AVATAR, userAvatar);
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_ID, uid);
	}
}
