package com.xinwei.talk.xmpp.model.specific;

import com.xinwei.util.Constants;
import com.xinwei.xmpp.model.XmppSpecific;

//语音消息
public class XmppVoiceSpecific extends XmppSpecific {

	public XmppVoiceSpecific(String url, long voiceDuration, String userName, String userAvatar, String uid) {
		set("voiceUrl", url);
		set("voiceDuration", voiceDuration);

		// added by Nisal to send, sender's details with message
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_NAME, userName);
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_AVATAR, userAvatar);
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_ID, uid);
	}
}
