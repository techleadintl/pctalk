package com.xinwei.talk.xmpp.listener;

import org.jivesoftware.smack.packet.Presence;

public interface XmppPresenceListener {

    public void processXmppPresence(Presence presence);
}
