package com.xinwei.talk.xmpp.listener;

import com.xinwei.talk.xmpp.model.XmppBodies;

public interface MessageFilter {
	public boolean filter(org.jivesoftware.smack.packet.Message message, XmppBodies xmppBodies);
}
