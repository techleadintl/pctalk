/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月22日 下午5:06:41
 * 
 ***************************************************************/
package com.xinwei.talk.xmpp.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jivesoftware.smack.packet.Message;

import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.DateUtil;
import com.xinwei.common.lang.NumberUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.vo.McWillMap;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.Constants;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.http.service.HttpTalkServiceImpl;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.manager.VCardManager;
import com.xinwei.talk.model.CooMember;
import com.xinwei.talk.model.CooTalk;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkGroupSpecific;
import com.xinwei.talk.model.message.specific.TalkParam;
import com.xinwei.talk.model.message.specific.TalkTextSpecific;
import com.xinwei.talk.ui.chat.ChatContainer;
import com.xinwei.talk.ui.chat.room.ChatRoom;
import com.xinwei.talk.ui.utils.XMPPMessageResponses;
import com.xinwei.talk.xmpp.extension.ReceiptMessage;
import com.xinwei.talk.xmpp.model.XmppBodies;
import com.xinwei.talk.xmpp.model.XmppParam;
import com.xinwei.xmpp.XmppListenerHelper;

public class ChatXmppListener extends XmppListenerHelper implements XmppMessageListener {

	/**
	 * this will invoke directly form Smack library when new message received
	 **/
	@Override
	public void processXmppMessage(Message message, XmppBodies xmppBodies) {
		// PacketFilter fromFilter = new FromMatchesFilter(participantJID);
		if (message.getError() != null) {
			if (message.getError().getCode() == 404) {
				// Check to see if the user is online to recieve this message.
			}
			return;
		}
		String packetID = message.getPacketID();
		{// Send and receive receipt messages
			ReceiptMessage receiptMessage = new ReceiptMessage();
			receiptMessage.setFrom(TalkManager.getSessionManager().getJID());
			receiptMessage.setTo(message.getFrom());
			receiptMessage.setPacketID(packetID);

			TalkManager.getConnection().sendPacket(receiptMessage);
		}
		if (xmppBodies == null) {
			return;
		}

		boolean broadcast = message.getProperty("broadcast") != null;
		// If this is a group chat message, discard
		if (broadcast) {
			return;
		}

		// convert server response to class objects according to message type
		TalkBody[] talkBodys = MessageUtil.conver2TalkBody(xmppBodies);
		if (talkBodys == null || talkBodys.length == 0) {
			return;
		}
		for (TalkBody talkBody : talkBodys) {
			List<TalkSpecific> msgList = talkBody.getSpecificList();
			if (CollectionUtil.isEmpty(msgList)) {
				continue;
			}
			TalkSpecific talkSpecific = msgList.get(0);
			if (talkSpecific instanceof TalkGroupSpecific) {
				// manage all group messages (sent and received)
				handleGroupMessage(talkBody, (TalkGroupSpecific) talkSpecific);
			} else {
				// manage all messages (sent and received)
				handleMessage(talkBody);
			}
		}
	}

	/**
	 * manage all group received and sent messages
	 * 
	 * @param talkBody      current message body with multiple types
	 * @param groupSpecific contains group specific map functions
	 **/
	private void handleGroupMessage(TalkBody talkBody, TalkGroupSpecific groupSpecific) {
		int chatType = talkBody.getType();
		String from = talkBody.getFrom();
		String to = talkBody.getTo();
		Date msgTime = talkBody.getTime();

		int type = groupSpecific.getOperType();
		String fromCid = groupSpecific.getFromCid();
		String fromNick = groupSpecific.getFromNick();

		if (StringUtil.isEmpty(fromCid)) {
			return;
		}

		String groupId = to;

		LocalManager localManager = TalkManager.getLocalManager();

		VCardManager vCardManager = TalkManager.getVCardManager();
		String loginUid = vCardManager.getVCard().getUid();

		// Whether the operator
		boolean isFromMe = StringUtil.equals(fromCid, loginUid);
		String packetId = talkBody.getPacketId();
		String timeStr = DateUtil.getString(msgTime);

		if (2001 == type) {
			Map userInfo = groupSpecific.getMap("userInfo");
			CooTalk newTalk = ((HttpTalkServiceImpl) TalkManager.getTalkService()).toTalk(userInfo);
			List<Talk> talkList = new ArrayList<>();
			talkList.add(newTalk);
			vCardManager.updateTalk(talkList);
			return;
		}

		// The notification interface displays a prompt message.
		boolean deleteGroup = false;
		String msg = null;
		// fired when user created or someone invited to a group
		if (XMPPMessageResponses.XMPP_MESSAGE_RES_CODE_GROUP_ADDED.getValue() == type) {
			// detail":{"type":"1004","fromCid":"","fromNick":"","groupId":""}
			// Operator：You invite """""" to join the group chat
			// other people：Query the group information according to groupId and prompt who
			// and who
			// invited you to join the group chat

			try {
				Group group = TalkManager.getTalkService().getGroup(groupId);
				List<Group> groupList = new ArrayList<Group>();
				groupList.add(group);
				vCardManager.addGroup(groupList);

				// The notification interface displays a prompt message.
				if (isFromMe) {// creator
					List<String> nickList = new ArrayList<String>();
					Collection<Member> memberList = group.getMemberList();
					for (Member member : memberList) {
						nickList.add(member.getMemNick());
					}
					msg = Res.getMessage("message.group.invited.2", StringUtil.join(nickList, ","));
				} else {
					msg = Res.getMessage("message.group.invited.1", fromNick);
				}

			} catch (Exception e) {
				Log.printPushMsg(System.currentTimeMillis(), type + "error--gid:{0},error:{1}", groupId,
						e.getMessage());
			}
		} else if (1005 == type) {
			// "detail":{"type":"1005","fromCid":"","fromNick":"",
			// "groupId":"","memberList":[ {“memberNickName”:””, Group member nickname
			// “cid”:””,
			// 群成员id“role”:””, Group member permissions “userInfo”:{User Info}}]},
			// 推送所有人，
			// 操作人：Tip You invite """""" to join the group chat
			// 被操作者：Query the group information based on groupId and prompt who and who
			// invited you to join the group chat
			// 其他人：Save group member information and prompt who and who invited who and who
			// joined the group chat
			List<Map> memberList = groupSpecific.getList("memberList");
			if (memberList == null) {
				Log.printPushMsg(System.currentTimeMillis(), type + "memberList is null--gid:{0}", groupId);
				return;
			}
			List<Member> groupMemberList = new ArrayList<>();
			List<String> cidList = new ArrayList<String>();
			List<String> nickList = new ArrayList<String>();
			for (Map mMap : memberList) {
				CooMember member = ((HttpTalkServiceImpl) TalkManager.getTalkService()).toMember(groupId, mMap);
				if (StringUtil.isNotBlank(member.getUid())) {
					groupMemberList.add(member);
					cidList.add(member.getUid());
					nickList.add(member.getMemNick());
				}
			}
			boolean containsMe = cidList.contains(loginUid);
			try {
				if (containsMe && !isFromMe) {
					Group group = TalkManager.getTalkService().getGroup(groupId);
					List<Group> groupList = new ArrayList<Group>();
					groupList.add(group);
					vCardManager.addGroup(groupList);
					msg = Res.getMessage("message.group.invited.1", fromNick);
				} else {
					Group group = localManager.getGroup(groupId);
					if (group == null) {
						Log.printPushMsg(System.currentTimeMillis(), type + "group is null--gid:{0}", groupId);
						return;
					}
					String nickNames = StringUtil.join(nickList, ",");
					vCardManager.addMember(groupId, groupMemberList);
					if (!containsMe && isFromMe) {
						msg = Res.getMessage("message.group.invited.2", nickNames);
					} else {
						msg = Res.getMessage("message.group.invited.3", fromNick, nickNames);
					}
				}
			} catch (Exception e) {
				Log.printPushMsg(System.currentTimeMillis(), type + "error--gid:{0},error:{1}", groupId,
						e.getMessage());
			}
		} else {
			Group group = localManager.getGroup(groupId);
			if (group == null) {
				Log.printPushMsg(System.currentTimeMillis(), type + "group is null--gid:{0}", groupId);
				return;
			}
			String groupDisplayName = TalkUtil.getDisplayName(group.getJid());
			Member fromMember = localManager.getMemberByUid(groupId, fromCid);
			if (fromMember != null) {
				from = TalkUtil.getUserTel(fromMember.getMemJid());
			}
			if (1001 == type) {
				// detail":{"type":"1001","fromCid":"","fromNick":"","groupName":""}
				// 操作人：Tip You modify the group name to ""
				// 其他人：Modify local data and prompt who and who modified the group name as ""
				String newGroupName = groupSpecific.getString("groupName");
				if (StringUtil.isEmpty(newGroupName)) {
					Log.printPushMsg(System.currentTimeMillis(), type + "group name is null--gid:{0}", groupId);
					return;
				}
				group.setName(newGroupName);
				try {
					List<Group> groupList = new ArrayList<Group>();
					groupList.add(group);
					vCardManager.updateGroup(groupList);
				} catch (Exception e) {
					e.printStackTrace();
					Log.printPushMsg(System.currentTimeMillis(), type + "update group name error--gid:{0},error:{1}",
							groupId, e.getMessage());
				}
				// 通知界面显示提示消息
				if (isFromMe) {
					msg = Res.getMessage("message.group.rename.2", newGroupName); // Res.getMessage("message.update.groupname.group",
																					// groupName, newGroupName);
				} else {
					msg = Res.getMessage("message.group.rename.3", newGroupName, fromNick);
				}
			} else if (1002 == type) {
				// detail":{"type":"1002","fromCid":"","fromNick":"","describe":""}
				// 操作人：Tip You modify the group announcement to ""
				// 其他人：Modify local data and prompt who and who modified the group announcement
				// as ""
				String newGroupDescribe = groupSpecific.getString("describe");
				if (StringUtil.isEmpty(newGroupDescribe)) {
					Log.printPushMsg(System.currentTimeMillis(), type + "group describe is null--gid:{0}", groupId);
					return;
				}
				group.setDescribe(newGroupDescribe);
				try {
					List<Group> groupList = new ArrayList<Group>();
					groupList.add(group);
					vCardManager.updateGroup(groupList);
				} catch (Exception e) {
					e.printStackTrace();
					Log.printPushMsg(System.currentTimeMillis(),
							type + "update group describe error--gid:{0},error:{1}", groupId, e.getMessage());
				}
				// 通知界面显示提示消息
				if (isFromMe) {
					msg = Res.getMessage("message.group.update.describe.2", newGroupDescribe); // Res.getMessage("message.update.groupname.group",
																								// groupName,
																								// newGroupName);
				} else {
					msg = Res.getMessage("message.group.update.describe.3", newGroupDescribe, fromNick);
				}
			} else if (1003 == type) {
				// detail":{"type":"1003","fromCid":"","groupNickName":""}
				// 操作人：Tip You have modified the nickname "" in the group.
				String newNickName = groupSpecific.getString("groupNickName");
				if (StringUtil.isEmpty(newNickName)) {
					Log.printPushMsg(System.currentTimeMillis(), type + "groupNickName is null--gid:{0}", groupId);
					return;
				}
				fromMember.setMemNick(newNickName);
				try {
					List<Member> updateMemberList = new ArrayList<Member>();
					updateMemberList.add(fromMember);
					vCardManager.updateMember(groupId, updateMemberList);
				} catch (Exception e) {
					e.printStackTrace();
					Log.printPushMsg(System.currentTimeMillis(), type + "error--gid:{0},error:{1}", groupId,
							e.getMessage());
				}
				// 通知界面显示提示消息
				if (isFromMe) {
					msg = Res.getMessage("message.group.update.nick.2", newNickName); // Res.getMessage("message.update.groupname.group",
																						// groupName, newGroupName);
				}
			} else if (1006 == type) {
				List<Map> list = groupSpecific.getList("toCid");
				if (list == null) {
					return;
				}
				String newCreatorUid = groupSpecific.getString("creator");
				if (StringUtil.isEmpty(newCreatorUid)) {
					return;
				}
				String oldCreatorUid = group.getCreatorUid();
				if (StringUtil.isEmpty(oldCreatorUid)) {
					return;
				}

				List<String> toCidList = new ArrayList<String>();
				List<Member> memList = new ArrayList<Member>();
				List<String> nickList = new ArrayList<String>();
				for (Map map : list) {
					String memUid = McWillMap.getMapString(map, "member");
					if (StringUtil.isNotEmpty(memUid)) {
						toCidList.add(memUid);
						Member member = localManager.getMemberByUid(groupId, memUid);
						if (member != null) {
							memList.add(member);
							nickList.add(member.getMemNick());
						}
					}
				}
				// 我自己操作的退群:1、Group leader；2、Non-group leader，3、Group master kicks members out
				// of the group
				boolean amIAdmin = StringUtil.equals(oldCreatorUid, loginUid);
				boolean fromIsAdmin = StringUtil.equals(fromCid, oldCreatorUid);
				boolean containsAdmin = toCidList.contains(oldCreatorUid);
				boolean containsMe = toCidList.contains(loginUid);
				// I am the owner, I quit the group.
				if (isFromMe && amIAdmin && fromIsAdmin && containsAdmin && containsMe) {
					// 我(group owner) exited the group
					deleteGroup = true;
					msg = Res.getMessage("message.group.delete.1", groupDisplayName);
				} // I am not a group owner, I quit the group.
				else if (isFromMe && !amIAdmin && !fromIsAdmin && !containsAdmin && containsMe) {
					// 我退出了群
					deleteGroup = true;
					msg = Res.getMessage("message.group.delete.1", fromNick, groupDisplayName);
				} // I am not a group owner, I was kicked out by the group owner.
				else if (!isFromMe && !amIAdmin && fromIsAdmin && !containsAdmin && containsMe) {
					// XX(the group owner) kicked me out of the group
					deleteGroup = true;
					msg = Res.getMessage("message.group.delete.4", fromNick, groupDisplayName);
				} // I am not a group owner, the group owner has withdrawn from the group.
				else if (!isFromMe && !amIAdmin && fromIsAdmin && containsAdmin && !containsMe) {
					// XX(group owner) exited the group
					msg = Res.getMessage("message.group.delete.2", fromNick);
				} // I am the owner, I kicked Li Si out of the group.
				else if (isFromMe && amIAdmin && fromIsAdmin && !containsAdmin && !containsMe) {
					// 我(group owner) kicked XX out of the group
					msg = Res.getMessage("message.group.delete.3", StringUtil.join(nickList, ","));
				} // I am not a group owner, Li Si was kicked out of the group by the group owner.
				else if (!isFromMe && !amIAdmin && fromIsAdmin && !containsAdmin && !containsMe) {
					// XX(group owner) kicked YY out of the group
					msg = Res.getMessage("message.group.delete.5", fromNick, StringUtil.join(nickList, ","));
				} // I am the owner, Li Si has withdrawn from the group.
				else if (!isFromMe && amIAdmin && !fromIsAdmin && !containsAdmin && !containsMe) {
					// XXExited the group
					msg = Res.getMessage("message.group.delete.2", fromNick);
				} // I am not a group owner, Li Si has withdrawn from the group.
				else if (!isFromMe && !amIAdmin && !fromIsAdmin && !containsAdmin && !containsMe) {
					// XXExited the group
					msg = Res.getMessage("message.group.delete.2", fromNick);
				} else {
					for (int i = 0; i < 100; i++) {
						System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
					}
				}
				group.setCreatorUid(newCreatorUid);
				try {
					if (deleteGroup) {
						List<Group> groupList = new ArrayList<Group>();
						groupList.add(group);
						vCardManager.deleteGroup(groupList);
					} else {
						vCardManager.deleteMember(groupId, memList);
					}
				} catch (Exception ex) {
					Log.printPushMsg(System.currentTimeMillis(), type + "error--gid:{0},error:{1}", groupId,
							ex.getMessage());
					msg = null;
				}
			}
		}
		if (msg != null) {
			fireNotifyMessage(packetId, groupId, from, to, timeStr, msg, deleteGroup, type);
		}
	}

//	private void fireNotifyMessage(String packetId, String gid, String from, String to, String msgTime, String tex) {
//		fireNotifyMessage(packetId, gid, from, to, msgTime, tex, false);
//	}

	private void fireNotifyMessage(String packetId, String groupId, String from, String to, String msgTime, String tex,
			boolean groupDelete, int msg_type) {
		final TalkMessage talkMessage = new TalkMessage();
		String talkJid;
		if (groupDelete) {
			Talk talk = TalkManager.getLocalManager().getTalk(Constants.CUSTOM_TALK_TEL_GROUP_MESSAGE);
			if (talk == null) {
				return;
			}
			talkJid = talk.getJid();
			talkMessage.setIsGroup(false);
		} else {
			talkJid = TalkUtil.getGroupFullJid(groupId);
			talkMessage.setIsGroup(true);
		}
		//
		TalkParam param = new TalkParam();
		param.setBalloon(String.valueOf(LAF.getBalloonLeftColor1().getRGB()));
		param.setBalloonType(XmppParam.BALLOON_TYPE_HINT);

		TalkBody talkBody = new TalkBody();
		talkBody.setFrom(from);
		talkBody.setTo(to);
		talkBody.setPacketId(packetId);
		talkBody.setParam(param);
		talkBody.addSpecific(new TalkTextSpecific(tex));
		talkBody.setTime(DateUtil.getDate(NumberUtil.getLong(msgTime, System.currentTimeMillis())));

		talkMessage.setSendOrReceive(TalkMessage.TYPE_RECEIVE);
		talkMessage.setId(TalkManager.getMessageManager().nextMessageId());
		talkMessage.setJid(talkJid);
		talkMessage.setTalkBody(talkBody);
		talkMessage.setMsgType(msg_type);

		int readFlag = TalkMessage.STATUS_UNREAD;

		// if user already selected a chat room and this message is belongs to that
		// room, set message status as read
		// currently this is not working, because can't contact pctalkui project from
		// here
		ChatContainer chatContainer = TalkManager.getChatManager().getChatContainer();
		ChatRoom chatRoom = chatContainer.getChatRoom(talkJid);
		if (chatRoom != null) {
			boolean isActive = chatContainer.isActiveChatRoom(chatRoom);
			if (isActive) {// Active room
				readFlag = TalkMessage.STATUS_READED;
			}
			chatRoom.receiveTalkMessage(talkMessage);
		}

		System.out.println("----------------------- Read status" + readFlag);

		talkMessage.setReaded(readFlag);

		ListenerManager.fireMessageReceived(talkJid, talkMessage);
	}
}
