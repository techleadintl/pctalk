package com.xinwei.talk.xmpp.model.specific;

import com.xinwei.xmpp.model.XmppSpecific;

//位置消息
public class XmppLocationSpecific extends XmppSpecific {

	public XmppLocationSpecific(String lng, String lat, String locationName, String describe) {
		set("lon", lng);
		set("lat", lat);
		set("locationName", locationName);
		set("locationDescribe", describe);
	}
}
