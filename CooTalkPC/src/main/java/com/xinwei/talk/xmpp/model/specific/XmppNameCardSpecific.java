package com.xinwei.talk.xmpp.model.specific;

import com.xinwei.util.Constants;
import com.xinwei.xmpp.model.XmppSpecific;

//语音消息
public class XmppNameCardSpecific extends XmppSpecific {
	public XmppNameCardSpecific(String cid, String telNo, String nick, int sex, String signature, String area,
			String picture, String userName, String userAvatar, String uid) {
		set("cid", cid);
		set("telNo", telNo);
		set("nick", nick);
		set("sex", sex);
		set("signature", signature);
		set("area", area);
		set("picture", picture);

		// added by Nisal to send, sender's details with message
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_NAME, userName);
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_AVATAR, userAvatar);
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_ID, uid);
	}
}
