package com.xinwei.talk.xmpp.model.specific;

import com.xinwei.util.Constants;
import com.xinwei.xmpp.model.XmppSpecific;

//离线文件消息
public class XmppOfflineFileSpecific extends XmppSpecific {

	public XmppOfflineFileSpecific(String url, String filename, long length, String secret, String hash,
			String userName, String userAvatar, String uid) {
		setType("offlinefile");
		set("url", url);
		set("filename", filename);
		set("length", length);
		set("hash", hash);
		set("secret", secret);
		set("fileName", filename);
		set("size", length);
		set("fileAbsolutePath", url);

		// added by Nisal to send, sender's details with message
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_NAME, userName);
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_AVATAR, userAvatar);
		set(Constants.TEXT_SPECIFIC_MESSAGE_USER_ID, uid);
	}

//	@Override
//	public String toString() {
//		return "OfflineFileSpecific [url=" + url + ", filename=" + filename + ", length=" + length + ", secret=" + secret + "]";
//	}
}
