/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年12月30日 下午2:03:09
 * 
 ***************************************************************/
package com.xinwei.talk.xmpp.listener.filter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.jivesoftware.smack.packet.Message;

import com.xinwei.common.lang.SystemUtil;
import com.xinwei.talk.xmpp.listener.MessageFilter;
import com.xinwei.talk.xmpp.model.XmppBodies;
import com.xinwei.talk.xmpp.model.XmppBody;

public class RepeatXmppMessageFilter implements MessageFilter {
	Object lock = new Object();
	Map<String, Long> map = new HashMap<String, Long>();

	public RepeatXmppMessageFilter() {
		SystemUtil.execute(new Runnable() {
			@Override
			public void run() {
				while (true) {
					synchronized (lock) {
						Iterator<Entry<String, Long>> iterator = map.entrySet().iterator();
						long time = System.currentTimeMillis();
						while (iterator.hasNext()) {
							Entry<String, Long> next = iterator.next();
							Long value = next.getValue();
							long duration = time - value;
							if (duration > 1000 * 60 || duration < 0) {//1分钟
								iterator.remove();
							}
						}
					}
					SystemUtil.sleep(10 * 1000);
				}
			}
		});
	}

	@Override
	public boolean filter(Message message, XmppBodies xmppBodies) {
		if (xmppBodies == null) {
			return false;
		}
		String packetID = message.getPacketID();
		synchronized (lock) {
			if (map.containsKey(packetID)) {
				return true;
			}
			map.put(packetID, System.currentTimeMillis());
		}
		return false;
	}

}
