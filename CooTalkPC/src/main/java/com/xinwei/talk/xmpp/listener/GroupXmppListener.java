/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月22日 下午5:06:41
 * 
 ***************************************************************/
package com.xinwei.talk.xmpp.listener;

import org.jivesoftware.smack.packet.Message;

import com.xinwei.talk.xmpp.model.XmppBodies;

public class GroupXmppListener implements XmppMessageListener {
	@Override
	public void processXmppMessage(Message message, XmppBodies xmppBodies) {
		if (xmppBodies == null) {
			return;
		}
		if (message.getType() != Message.Type.groupchat) {
			return;
		}
		System.out.println();
	}

}
