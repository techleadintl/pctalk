/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月22日 下午5:06:41
 * 
 ***************************************************************/
package com.xinwei.talk.xmpp.listener;

import java.util.Date;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.packet.DelayInformation;

import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.xmpp.extension.ClientInformation;
import com.xinwei.talk.xmpp.extension.ClientInformationProvider;
import com.xinwei.talk.xmpp.model.XmppBodies;

public class HeadlineXmppListener implements XmppMessageListener {

	public HeadlineXmppListener() {
		ProviderManager.getInstance().addExtensionProvider("info", "jabber:client", new ClientInformationProvider());
	}
	
	@Override
	public void processXmppMessage(Message message, XmppBodies xmppBodies) {
		ClientInformation receiptMessage = (ClientInformation) message.getExtension("info", "jabber:client");
		if (receiptMessage != null) {
			handleReceipt(message.getBody() == null ? message.getPacketID() : message.getBody(), receiptMessage.getDate());
		}
		
		DelayInformation delayInformation = (DelayInformation) message.getExtension("x", "jabber:x:delay");
		if (delayInformation != null) {
			handleReceipt(message.getBody() == null ? message.getPacketID() : message.getBody(), delayInformation.getStamp());
		}
		
		return;
	}

	private void handleReceipt(String packetID, Date date) {
		TalkManager.getMessageManager().receiptSendedMessage(packetID, date);
	}
}
