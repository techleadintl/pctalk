/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月13日 下午5:32:02
 * 
 ***************************************************************/
package com.xinwei.talk.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.common.lang.HttpUtils;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.dao.TalkDao;
import com.xinwei.talk.model.CooTalk;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.avatar.Avatar;
import com.xinwei.talk.model.avatar.BackgroundAvatar;
import com.xinwei.talk.model.avatar.MemberAvatar;
import com.xinwei.talk.model.avatar.TalkAvatar;

public class VCardManagerImpl implements VCardManager {
	protected LocalManager localManager = TalkManager.getLocalManager();

	protected Set<String> delayedContacts = Collections.synchronizedSet(new HashSet<String>());

	protected boolean vcardLoaded;

	protected LinkedBlockingQueue<List<Avatar>> queue = new LinkedBlockingQueue<List<Avatar>>();

	protected File contactsDir;

	/**
	 * Initialize VCardManager.
	 */
	public VCardManagerImpl() {
		// Start Listener
		startQueueListener();
	}

	// 缓存用户
	public void initTalk() {
		TalkDao dao = TalkManager.getTalkDao();

		List<? extends Talk> talkList = dao.getTalkList();

		System.out.println("c:" + talkList);

		localManager.putTalkList(talkList);

		ListenerManager.fireTalkAdd(talkList);
	}

	// 缓存头像
	public void initAvatar() {
		// 自己头像
		VCard vCard = TalkManager.getVCard();
		ImageManager.setAvatar(vCard.getJid(), vCard.getAvatar());
		// 好友头像
		Collection<Talk> talkList = localManager.getTalkList();
		for (Talk talk : talkList) {
			ImageManager.setAvatar(talk.getJid(), talk.getAvatar());
		}
		// 群成员头像
		List<Member> memberList = localManager.getMemberList();
		for (Member member : memberList) {
			ImageManager.setAvatar(member.getMemJid(), member.getAvatar());
		}

		ListenerManager.fireAvatarChanged(null);
	}

	// 缓存群组
	public void initGroup() {
		TalkDao dao = TalkManager.getTalkDao();
		List<Group> groupList = dao.getGroupList();
		if (CollectionUtil.isEmpty(groupList))
			return;

		localManager.putGroupList(groupList);

		ListenerManager.fireGroupAdd(groupList);

		List<Member> memberList = dao.getMemberList();

		localManager.putMemberList(memberList);

		for (Group group : groupList) {
			List<Member> memberList2 = localManager.getMemberList(group.getId());
			if (CollectionUtil.isEmpty(memberList2))
				continue;

			ListenerManager.fireMemberAdd(group.getId(), memberList2);
		}
	}

	private void loadAndSaveAvatar(List<Avatar> avatars) {
		List<Avatar> result = new ArrayList<Avatar>();
		for (Avatar avatar : avatars) {
			String url = avatar.getUrl();
			if (avatar instanceof TalkAvatar) {
				TalkAvatar talkAvatar = (TalkAvatar) avatar;
				String userTel = talkAvatar.getUserTel();
				Talk talk = localManager.getTalk(userTel);
				if (talk == null)
					continue;
				if (url == null || url.trim().length() == 0) {
					talk.setAvatar(null);
				} else {
					talk.setAvatar(downloadAvatar(url));
				}
				byte[] avatarBytes = talk.getAvatar();
				if (avatarBytes == null) {
					ImageManager.deleteAvatar(talk.getJid());
					TalkManager.getTalkDao().updateTalkAvatar(userTel, avatarBytes);
				} else {
					ImageManager.setAvatar(talk.getJid(), avatarBytes);
					TalkManager.getTalkDao().updateTalkAvatar(userTel, avatarBytes);
				}
				// TODO:Notification update avatar
			} else if (avatar instanceof BackgroundAvatar) {
				BackgroundAvatar backgroundAvatar = (BackgroundAvatar) avatar;
				String userTel = backgroundAvatar.getUserTel();
				CooTalk talk = (CooTalk) localManager.getTalk(userTel);
				if (talk == null) {
					continue;
				}
				if (url == null || url.trim().length() == 0) {
					talk.setBackground(null);
				} else {
					byte[] avatarBytes = downloadAvatar(url);
					talk.setBackground(avatarBytes);
				}
				byte[] background = talk.getBackground();
				if (background == null) {
					ImageManager.deleteBackground(talk.getJid());
					TalkManager.getTalkDao().updateTalkBackground(userTel, background);
				} else {
					ImageManager.setBackground(talk.getJid(), background);
					TalkManager.getTalkDao().updateTalkBackground(userTel, background);
				}
				// TODO:Notification update avatar
			} else if (avatar instanceof MemberAvatar) {
				MemberAvatar memberAvatar = (MemberAvatar) avatar;
				String uid = memberAvatar.getUid();
				String gid = memberAvatar.getGid();
				Member member = localManager.getMemberByUid(gid, uid);
				if (member == null)
					continue;

				if (url == null || url.trim().length() == 0)
					continue;

				byte[] avatarBytes = downloadAvatar(url);
				if (avatarBytes == null)
					continue;

				member.setAvatar(avatarBytes);
				TalkManager.getTalkDao().updateMemberAvatar(gid, uid, avatarBytes);

				ImageManager.setAvatar(member.getMemJid(), member.getAvatar());
				// TODO:Notification update avatar
			}
			result.add(avatar);
		}
		if (result.isEmpty())
			return;
		ListenerManager.fireAvatarChanged(result);
	};

	/**
	 */
	public void addToQueue(List<Avatar> avatars) {
		if (avatars != null && !avatars.isEmpty()) {
			queue.add(avatars);
		}
	}

	private void addToQueueTalk(List<? extends Talk> talks) {
		List<Avatar> avatars = new ArrayList<Avatar>();
		List<Avatar> backgrounds = new ArrayList<Avatar>();
		for (Talk talk : talks) {
			avatars.add(new TalkAvatar(talk.getTel(), talk.getAvatarUrl()));
			backgrounds.add(new BackgroundAvatar(talk.getTel(), ((CooTalk) talk).getBackgroundUrl()));
		}
		addToQueue(avatars);
		addToQueue(backgrounds);
	}

	public void addToQueueTalk(Talk talk) {
		List<Avatar> avatars = new ArrayList<Avatar>();
		avatars.add(new TalkAvatar(TalkUtil.getUserTel(talk), talk.getAvatarUrl()));
		addToQueue(avatars);
	}

	public void addToQueueMember(List<Member> members) {
		List<Avatar> avatars = new ArrayList<Avatar>();
		for (Member member : members) {
			avatars.add(new MemberAvatar(member.getGid(), member.getUid(), member.getAvatarUrl()));
		}
		addToQueue(avatars);
	}

	/**
	 * Listens for new VCards to lookup in a queue.
	 */
	protected void startQueueListener() {
		final Runnable queueListener = new Runnable() {
			public void run() {
				while (true) {
					try {
						List<Avatar> avatars = queue.take();
						loadAndSaveAvatar(avatars);
					} catch (InterruptedException e) {
						e.printStackTrace();
						break;
					}
				}
			}
		};
		TaskEngine.getInstance().submit(queueListener);
	}

	/**
	 * Returns the VCard for this Spark user. This information will be cached after
	 * loading.
	 *
	 * @return this users VCard.
	 */
	public VCard getVCard() {
		if (!vcardLoaded) {
			vcardLoaded = true;
		}
		return TalkManager.getVCard();
	}

	/**
	 * Parses out the numbers only from a phone number.
	 *
	 * @param number the full phone number.
	 * @return the phone number only (5551212)
	 */
	public static String getNumbersFromPhone(String number) {
		if (number == null) {
			return null;
		}

		number = number.replace("-", "");
		number = number.replace("(", "");
		number = number.replace(")", "");
		number = number.replace(" ", "");

		return number;
	}

	protected byte[] downloadAvatar(String url) {
		if (StringUtil.isEmpty(url)) {
			return null;
		}
		byte[] bytes = null;
		try {
			bytes = HttpUtils.download(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (bytes != null && bytes.length > 0) {
			return bytes;
		} else {
			return null;
		}
	}

	/**
	 * get contact list from server and update local DB
	 **/
	@SuppressWarnings({ "unchecked" })
	public void sysTalk() throws Exception {
		Object[] objects = TalkManager.getTalkService().getTalkList();

		List<Talk> newTalks = new ArrayList<Talk>((Collection<Talk>) objects[1]);

		List<Talk> oldTalks = new ArrayList<Talk>(localManager.getTalkList());

		System.out.println("n:" + newTalks);

		// 新增、Delete, same
		List<Talk>[] analyse = CollectionUtil.analyse(oldTalks, newTalks);
		// 新增
		addTalk(analyse[0]);
		// 删除
		deleteTalk(analyse[1]);
		// 更新
		updateTalk(analyse[2]);
	}

	// 新增
	public void addTalk(List<Talk> talkList) {
		List<Talk> updateTalkList = new ArrayList<>();
		Iterator<Talk> iterator = talkList.iterator();
		while (iterator.hasNext()) {
			Talk talk = iterator.next();
			if (localManager.containsTalk(talk.getTel())) {
				updateTalkList.add(talk);
				iterator.remove();
			}
		}
		TalkManager.getTalkDao().addTalkList(talkList);
		localManager.putTalkList(talkList);
		ListenerManager.fireTalkAdd(talkList);
		// 加载头像
		addToQueueTalk(talkList);

		// 更新
		updateTalk(updateTalkList);
	}

	// 删除
	public void deleteTalk(List<Talk> list) {
		TalkManager.getTalkDao().deleteTalkList(list);
		localManager.removeTalkList(list);
		ListenerManager.fireTalkDelete(list);
	}

	// public void updateTalk(Map<Talk, Talk> talkMap) {
	// Set<Entry<Talk, Talk>> entrySet = talkMap.entrySet();
	// //更新
	// List<Talk> updateTalkList = new ArrayList<Talk>();
	// List<Talk> updateAvatarList = new ArrayList<Talk>();
	// List<Talk> updateBackgroundList = new ArrayList<Talk>();
	// for (Entry<Talk, Talk> entry : entrySet) {
	// CooTalk oldTalk = (CooTalk) entry.getKey();
	// CooTalk newTalk = (CooTalk) entry.getValue();
	// updateTalkList.add(newTalk);
	// if (!StringUtil.equals(newTalk.getAvatarUrl(), oldTalk.getAvatarUrl()) ||
	// (newTalk.getAvatarUrl() != null && oldTalk.getAvatar() == null)) {
	// updateAvatarList.add(newTalk);
	// }
	// if (!StringUtil.equals(newTalk.getBackgroundUrl(),
	// oldTalk.getBackgroundUrl()) || (newTalk.getBackgroundUrl() != null &&
	// oldTalk.getBackground() == null)) {
	// updateAvatarList.add(newTalk);
	// }
	// }
	// TalkManager.getTalkDao().updateTalkList(updateTalkList);
	//
	// localManager.putTalkList(updateTalkList);
	//
	// ListenerManager.fireTalkUpdate(updateTalkList);
	// //加载头像
	// if (!updateAvatarList.isEmpty()) {
	// addToQueueTalk(updateAvatarList);
	// }
	// //加载背景
	// if (!updateBackgroundList.isEmpty()) {
	// addToQueueTalk(updateBackgroundList);
	// }
	// }
	public void updateTalk(List<Talk> talkList) {
		TalkManager.getTalkDao().updateTalkList(talkList);

		localManager.putTalkList(talkList);

		ListenerManager.fireTalkUpdate(talkList);
		// 加载头像
		addToQueueTalk(talkList);
	}

	/**
	 * get group and group member list from server and update local DB
	 **/
	public void sysGroup() {
		try {
			Collection<Group> oldGroupList = localManager.getGroupList();

			List<Group> newGroupList = TalkManager.getTalkService().getGroupList();

			// Add delete, same
			List<Group>[] analyse = CollectionUtil.analyse(oldGroupList, newGroupList);

			// Add
			addGroup(analyse[0]);
			// delete
			deleteGroup(analyse[1]);
			// Update
			updateGroup(analyse[2]);

			// Synchronization group member
			sysMemberList(analyse[2]);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * add groups and group members, when start the app
	 **/
	public void addGroup(List<Group> groupList) throws Exception {
		if (CollectionUtil.isEmpty(groupList))
			return;

		List<Group> deleteList = new ArrayList<Group>();
		Iterator<Group> iterator = groupList.iterator();
		while (iterator.hasNext()) {
			Group group = iterator.next();
			if (localManager.containsGroup(group)) {
				deleteList.add(group);
			}
		}
		// Delete existing groups first
		deleteGroup(deleteList);

		List<Member> memberList = new ArrayList<Member>();
		for (Group group : groupList) {
			Collection<Member> ml = group.getMemberList();
			if (!ml.isEmpty()) {
				memberList.addAll(ml);
			}
		}

		TalkDao dao = TalkManager.getTalkDao();
		dao.addGroupList(groupList, memberList);

		// Save to memory
		localManager.putGroupList(groupList);
		localManager.putMemberList(memberList);

		// Notification interface update
		ListenerManager.fireGroupAdd(groupList);

		addToQueueMember(memberList);
	}

	public void deleteGroup(List<Group> groupList) {
		if (CollectionUtil.isEmpty(groupList)) {
			return;
		}

		// 从本地数据库删除
		TalkManager.getTalkDao().deleteGroupList(groupList);

		// 从内存删除
		localManager.removeGroupList(groupList);
		localManager.removeMemberList(groupList);

		// 通知界面更新
		ListenerManager.fireGroupDelete(groupList);
	}

	public void updateGroup(List<Group> groupList) throws Exception {
		if (CollectionUtil.isEmpty(groupList)) {
			return;
		}
		// 更新数据库
		TalkManager.getTalkDao().updateGroupList(groupList);
		// 更新内存删除
		localManager.putGroupList(groupList);

		ListenerManager.fireGroupUpdate(groupList);

	}

	private void sysMemberList(Collection<Group> groupList) throws Exception {
		if (CollectionUtil.isEmpty(groupList))
			return;
		for (Group group : groupList) {
			Collection<Member> newList = group.getMemberList();

			String gid = group.getId();
			List<Member> oldList = localManager.getMemberList(gid);

			// 新增、delete
			List<Member>[] analyse = CollectionUtil.analyse(oldList, newList);

			// 新增
			addMember(gid, analyse[0]);

			// 删除
			deleteMember(gid, analyse[1]);

			// 更新
			updateMember(gid, analyse[2]);
		}
	}

	public void addMember(String gid, List<Member> memberList) {
		if (CollectionUtil.isEmpty(memberList))
			return;

		Group group = localManager.getGroup(gid);
		if (group == null)
			return;

		TalkManager.getTalkDao().addMemberList(memberList);

		localManager.putMemberList(memberList);

		ListenerManager.fireMemberAdd(gid, memberList);
		// 加载头像
		addToQueueMember(memberList);
	};

	public void deleteMember(String gid, List<Member> memberList) {
		if (CollectionUtil.isEmpty(memberList))
			return;

		Group group = localManager.getGroup(gid);
		if (group == null)
			return;

		TalkManager.getTalkDao().deleteMemberList(gid, memberList);

		localManager.removeMemberList(gid, memberList);

		ListenerManager.fireMemberDelete(gid, memberList);
	}

	public void updateMember(String gid, List<Member> memberList) {
		Group group = localManager.getGroup(gid);
		if (group == null) {
			return;
		}
		if (CollectionUtil.isEmpty(memberList)) {
			return;
		}

		// 更新数据库
		TalkManager.getTalkDao().updateMemberList(memberList);
		// 更新内存删除
		localManager.putMemberList(memberList);

		ListenerManager.fireMemberUpdate(gid, memberList);

		addToQueueMember(memberList);
	}
}
