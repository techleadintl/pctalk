package com.xinwei.util;

public class Constants {

	public final static String TEXT_SPECIFIC_MESSAGE_USER_NAME = "userName";
	public final static String TEXT_SPECIFIC_MESSAGE_USER_AVATAR = "userAvatar";
	public final static String TEXT_SPECIFIC_MESSAGE_USER_ID = "userID";

}
