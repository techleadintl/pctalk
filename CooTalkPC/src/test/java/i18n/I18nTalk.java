package i18n;

import com.xinwei.common.resource.I18nConfigurator;

public class I18nTalk extends I18nConfigurator {
	private static void addI18n() throws Exception {
		addI18n("message.group.invited.1", "I have been invited to group chat by {0} ", "{0}邀请您加入了群聊");
		addI18n("message.group.invited.2", "You invite {0} to group chat", "您邀请{0}加入了群聊");
		addI18n("message.group.invited.3", "{0} invite {1} to group chat", "{0}邀请{1}加入了群聊");
		addI18n("message.group.rename.2", "You rename group to {0}", "您将群名称修改为{0}");
		addI18n("message.group.rename.3", "{1} rename group to {0}", "{1}将群名称修改为{0}");
		addI18n("message.group.update.describe.2", "You update group describe to {0}", "您修改群公告为{0}");
		addI18n("message.group.update.describe.3", "{1} update group describe to {0}", "{1}修改群公告为{0}");
		addI18n("message.group.update.nick.2", "You update your nickname in group {0} to {1}", "您修改了在群中的昵称为{0}");
		addI18n("message.group.delete.1", "You quit the group {0}", "您退出了群[{0}]");
		addI18n("message.group.delete.2", "{0} quit the group", "{0}退出了群");
		addI18n("message.group.delete.3", "You kicked {0} out of the group", "您将{0}请出了群");
		addI18n("message.group.delete.4", "{0} kicked me out of the group {1}", "{0}将您请出了群[{1}]");
		addI18n("message.group.delete.5", "{0} kicked {1} out of the group", "{0}将{1}请出了群");
	}

	public static void main(String[] args) {
		try {
			init("src/main/resources/talk/i18n/", "cootalk");
			clearI18n();
			addI18n();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
