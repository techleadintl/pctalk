import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileLock;

public class FileLocking {
	public static void main(String[] args) throws Exception {
		new Thread() {
			public void run() {
				try {
					xxx();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			};
		}.start();
	}

	protected static void xxx() throws FileNotFoundException, IOException, InterruptedException {
		System.out.println(Thread.currentThread().getName() + "_1");

		FileOutputStream fos = new FileOutputStream("file.txt");
		//获取文件锁 FileLock 对象  
		FileLock fl = fos.getChannel().tryLock();
		System.out.println(Thread.currentThread().getName() + "_2");
		//tryLock是尝试获取锁，有可能为空，所以要判断  
		if (fl != null) {
			System.out.println("Locked File");
			Thread.sleep(100000);
			fl.release();//释放锁  
			System.out.println(Thread.currentThread().getName() + "_3");
			System.out.println("Released Lock");
		} else {
			System.out.println();
		}
		System.out.println(Thread.currentThread().getName() + "_4");
		fos.close();
		System.out.println(Thread.currentThread().getName() + "_5");
	}
}