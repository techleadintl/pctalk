package com.xinwei.cusview;

import javafx.scene.layout.HBox;

public abstract class CustomChatView extends HBox {

	public CustomChatView() {
	}

	public abstract void initializeViews();

	public abstract void updateViews();

}
