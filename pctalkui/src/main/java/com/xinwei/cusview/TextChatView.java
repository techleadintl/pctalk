package com.xinwei.cusview;

import java.util.List;

import javax.swing.ImageIcon;

import com.xinwei.manager.EmojiManager;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.plugin.emoticon.EmoticonManager;
import com.xinwei.util.ScreenUtil;
import com.xinwei.util.TranslateUtil;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 * This class will handle Text and Emoji chat messages.
 * 
 * @author nisal
 **/
public class TextChatView extends CustomChatView {
	public ViewHolder viewHolder;

	private String message;
	private TalkMessage talkMessage;
	private String currentJID;
	private boolean isUser;

	private List<Object> textAndEmoticon;

	/**
	 * @param message     received chat message as string
	 * @param talkMessage current chat message's meta data object
	 * @param currentJID  received message's JID to identify the message saved in
	 *                    local DB
	 * @param isUser      if true -> message sent by current user, if false ->
	 *                    received message
	 **/
	public TextChatView(String message, TalkMessage talkMessage, String currentJID, boolean isUser) {
		this.talkMessage = talkMessage;
		this.currentJID = currentJID;
		this.isUser = isUser;
		this.message = message;

		initializeViews();
		updateViews();
	}

	@Override
	public void initializeViews() {
		try {
			viewHolder = new ViewHolder();

			textAndEmoticon = EmoticonManager.getInstance().getTextAndEmoticon(message);

			viewHolder.viewContainer = new TextFlow();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateViews() {
		if (currentJID.equals(talkMessage.getJid())) {

			if (textAndEmoticon != null) {

				for (Object obj : textAndEmoticon) {

					if (obj instanceof String) {

						Text lbl = new Text();
						if (isUser) {
							lbl.setStyle("-fx-fill: #797E84;");
							viewHolder.viewContainer.setPadding(new Insets(0, 0, 15, 0));
						} else {
							lbl.setStyle("-fx-fill: #FFFFFF;");
							viewHolder.viewContainer.setPadding(new Insets(0, 15, 0, 0));
						}

						lbl.setFont(new Font(14));
						lbl.setText(obj.toString());

						lbl.setOnMouseClicked(new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								try {
									lbl.setText(TranslateUtil.translate(lbl.getText()));
								} catch (Exception ex) {
									Log.error(ex.getMessage());
								}
							}
						});

						lbl.setOnMouseEntered(new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								if (viewHolder.toolTip != null) {
									viewHolder.toolTip.hide();
									viewHolder.toolTip = null;
								}

								viewHolder.toolTip = new Tooltip("Click to translate");

								viewHolder.toolTip.setAutoHide(true);
								viewHolder.toolTip.setHideOnEscape(true);
								viewHolder.toolTip.show(lbl, ScreenUtil.getScreenXPostionOfNode(lbl),
										ScreenUtil.getScreenYPostionOfNode(lbl) + 20);
							}
						});

						lbl.setOnMouseExited(new EventHandler<Event>() {
							@Override
							public void handle(Event event) {
								if (viewHolder.toolTip != null) {
									viewHolder.toolTip.hide();
									viewHolder.toolTip = null;
								}
							}
						});
						viewHolder.viewContainer.getChildren().add(lbl);

					} else if (obj instanceof ImageIcon) {

						Image img = EmojiManager.getInstance().getEmoticon(obj.toString());
						ImageView p = new ImageView(img);
						p.setFitWidth(30);
						p.setFitHeight(30);
						viewHolder.viewContainer.getChildren().add(p);
					}
				}
			}
		}
	}

	public class ViewHolder {
		public TextFlow viewContainer;
		public Tooltip toolTip;
	}

}
