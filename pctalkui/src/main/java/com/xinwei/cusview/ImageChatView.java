package com.xinwei.cusview;

import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;

import javax.imageio.ImageIO;

import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.listener.FileDownloadListener;
import com.xinwei.talk.manager.FileManager;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.specific.TalkImageSpecific;
import com.xinwei.talk.model.message.specific.TalkResourceSpecific;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * This class will handle Image chat messages.
 * 
 * @author nisal
 **/
public class ImageChatView extends CustomChatView {
	public ViewHolder viewHolder;

	private TalkImageSpecific imageMsg;
	private boolean isMember;
	private int isRead;
	private long msgId;

	private TalkResourceSpecific talkResourceSpecific;

	/**
	 * @param imageMsg    received chat message with image URL
	 * @param isMember    if true -> message sent by current user, if false ->
	 *                    received message
	 * @param isRead      is this message already read or not
	 * @param msgId       received chat message's ID from the local DB
	 * @param name        message sender's name
	 * @param time        message sent date and time
	 * @param talkMessage current chat message's meta data object
	 **/
	public ImageChatView(TalkImageSpecific imageMsg, boolean isMember, int isRead, long msgId) {
		this.imageMsg = imageMsg;
		this.isMember = isMember;
		this.isRead = isRead;
		this.msgId = msgId;

		initializeViews();
		updateViews();
	}

	@Override
	public void initializeViews() {
		try {
			viewHolder = new ViewHolder();

			Image image = new Image(getClass().getClassLoader().getResource("images/custom/default_img.jpg").toString(),
					120, 80, true, true);
			viewHolder.imageView = new ImageView(image);

			setAlignment(Pos.CENTER);
			setPadding(new Insets(5, 5, 5, 5));

			viewHolder.imageView.setOnMouseClicked(event -> openImage(imageMsg, isMember, null));

			if (isMember)
				setStyle("-fx-border-radius: 10;-fx-background-color: #ECEBF3;");
			else
				setStyle("-fx-border-radius: 10;-fx-background-color: #3cdeb5;");

			talkResourceSpecific = (TalkResourceSpecific) imageMsg;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateViews() {
		try {

			handleResourceMsg(isRead, msgId, talkResourceSpecific, com.xinwei.talk.common.Constants.MSG_TYPE_IMG,
					viewHolder);
			getChildren().add(viewHolder.imageView);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class ViewHolder {
		public ImageView imageView;
		public Image chatImage;
	}

	/**
	 * download image from server
	 * 
	 * @param isRead      current downloading image's read status
	 * @param msgId       current chat message's id
	 * @param resourceMsg current chat message's meta data
	 * @param msgType     current message's type
	 **/
	private void handleResourceMsg(int isRead, long msgId, TalkResourceSpecific resourceMsg, int msgType,
			ViewHolder holder) {

		FileManager fileManager = new FileManager();
		final int updownType = isRead == TalkMessage.STATUS_UNREAD
				? com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DOWNLOAD
				: com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DB;

		fileManager.asynDownloadFile(msgId, msgType, resourceMsg.getUrl(), new FileDownloadListener() {
			@Override
			public void call(long msgId, int type, String url, byte[] data) {

				if (data != null) {
					resourceMsg.setData(data);

					Image image = new Image(new ByteArrayInputStream(data), 120, 80, true, true);
					holder.imageView.setImage(image);

				}

			}
		}, updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DOWNLOAD);
	}

	private void openImage(TalkImageSpecific image, boolean isMember, File file) {

		Desktop dt = Desktop.getDesktop();
		try {
			// if file is not being uploaded
			if (file == null) {
				String path = TalkUtil.getImagesDirectory() + "/" + image.getFilename();
				// if file is being downloaded for the first time
				if (!new File(path).exists()) {
					BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(image.getData()));
					ImageIO.write(bufferedImage, "png", new File(path));
				}
				file = new File(path);
			}
			dt.open(file);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" > FX-Engine : File not found");
		}
	}

}
