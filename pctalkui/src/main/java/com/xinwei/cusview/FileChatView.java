package com.xinwei.cusview;

import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.Icon;
import javax.swing.filechooser.FileSystemView;

import com.xinwei.http.service.file.InFileTransfer;
import com.xinwei.nortify.CopyTask;
import com.xinwei.talk.common.Downloads;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.util.FileSizeUtil;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 * This class will handle File chat messages.
 * 
 * @author nisal
 **/
public class FileChatView extends CustomChatView {
	public ViewHolder viewHolder;

	private TalkFileSpecific talkFileSpecific;
	private boolean isMember;

	private boolean isFileDownloading = false;
	private DoubleProperty progress = new SimpleDoubleProperty(0);
	private CopyTask copyTask;// file downloading task

	/**
	 * @param receivedFile received chat message with file url
	 * @param isMember     if true -> message sent by current user, if false ->
	 *                     received message
	 **/
	public FileChatView(TalkFileSpecific talkFileSpecific, boolean isMember) {
		this.talkFileSpecific = talkFileSpecific;
		this.isMember = isMember;

		isFileDownloading = false;

		if (talkFileSpecific == null)
			return;

		initializeViews();
		updateViews();
	}

	@Override
	public void initializeViews() {
		try {
			viewHolder = new ViewHolder();

			viewHolder.baseContainer = new HBox(4);
			viewHolder.fileDataContainer = new VBox(4);
			viewHolder.fileContainer = new HBox(4);
			viewHolder.fileDownloadContainer = new VBox(3);

			viewHolder.fileSize = new Text("0 B");
			viewHolder.fileSize.setFont(new Font(10));
			viewHolder.fileName = new Text("");
			viewHolder.fileName.setFont(new Font(15));
			viewHolder.fileName.setStyle("-fx-fill: #8e8e8e;");
			viewHolder.fileSize.setStyle("-fx-fill: #8e8e8e;");
			viewHolder.fileNameContainer = new TextFlow();
			viewHolder.fileNameContainer.setMinWidth(150);
			viewHolder.fileNameContainer.setPrefWidth(150);
			viewHolder.fileNameContainer.setMaxWidth(150);
			viewHolder.fileSeparator = new Separator(Orientation.HORIZONTAL);
			viewHolder.fileSeparator.setStyle("-fx-background-color: white; -fx-margin: 5px 0 10px 0;");

			Image fileIcon = new Image(getClass().getClassLoader().getResource("images/chatfile.png").toString(), 30,
					30, true, true);
			viewHolder.fileIcon = new ImageView(fileIcon);

			viewHolder.progressBar = new ProgressBar();
			viewHolder.progressBar.setProgress(0.5);
			viewHolder.progressBar.setPrefSize(150, 12);
			viewHolder.progressBar.setMinSize(150, 12);
			viewHolder.downloadBtn = new Button("Download File");
			viewHolder.downloadBtn.setFont(new Font(13));
			viewHolder.downloadBtn.setStyle("-fx-text-fill: #606060; -fx-background-color: transparent;");

			viewHolder.progressBar.progressProperty().bind(progress);// set progress property

			viewHolder.fileNameContainer.getChildren().add(viewHolder.fileName);

			viewHolder.fileDataContainer.getChildren().addAll(viewHolder.fileNameContainer, viewHolder.fileSize);
			viewHolder.fileContainer.getChildren().addAll(viewHolder.fileIcon, viewHolder.fileDataContainer);
			viewHolder.fileDownloadContainer.getChildren().addAll(viewHolder.fileContainer, viewHolder.fileSeparator,
					viewHolder.progressBar, viewHolder.downloadBtn);

			viewHolder.fileContainer.setAlignment(Pos.CENTER_LEFT);
			viewHolder.fileDataContainer.setAlignment(Pos.TOP_LEFT);
			viewHolder.fileDownloadContainer.setAlignment(Pos.CENTER);

			viewHolder.baseContainer.getChildren().addAll(viewHolder.fileDownloadContainer);
			// set file download action event
			viewHolder.downloadBtn.setOnAction(e -> {
				handleFileDownload();// download file from server
			});

			manageDownloadState();// manage download state

			setAlignment(Pos.CENTER_LEFT);
			setPadding(new Insets(10));
			setStyle("-fx-background-color: #e8e9ea; -fx-border-radius: 8; -fx-background-radius: 8;");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateViews() {
		try {
			showFileDetails();// show file name, size and icon

			getChildren().addAll(viewHolder.baseContainer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class ViewHolder {
		public HBox baseContainer;
		public VBox fileDataContainer;
		public HBox fileContainer;
		public VBox fileDownloadContainer;

		public Text fileName;
		public TextFlow fileNameContainer;
		public Text fileSize;
		public ImageView fileIcon;
		public Separator fileSeparator;

		public ProgressBar progressBar;
		public Button downloadBtn;
	}

	/**
	 * show file name, size and icon
	 **/
	private void showFileDetails() {
		File file = getDownloadedFile();
		// get system icon for given file mime type
		Image fxImage = new Image(getClass().getClassLoader().getResource("images/chatfile.png").toString(), 30, 30,
				true, true);
		if (file != null && file.exists()) {
			Icon icon = FileSystemView.getFileSystemView().getSystemIcon(file);

			BufferedImage bufferedImage = new BufferedImage(30, 30, BufferedImage.TYPE_INT_ARGB);
			icon.paintIcon(null, bufferedImage.getGraphics(), 0, 0);
			fxImage = SwingFXUtils.toFXImage(bufferedImage, null);
		}

		if (viewHolder != null) {
			viewHolder.fileIcon.setImage(fxImage);
			viewHolder.fileName.setText(talkFileSpecific.getFilename());
			viewHolder.fileSize.setText(FileSizeUtil.getFormattedFileSize(talkFileSpecific.getSize()));
		}
	}

	/**
	 * handle file downloading from the server or retrieve from local storage, if
	 * previously downloaded
	 **/
	private void handleFileDownload() {
		try {

			if (viewHolder == null)
				return;

			// change button download text
			viewHolder.downloadBtn.setText(isFileDownloading ? "Cancel Download" : "Download File");

			File file = getDownloadedFile();

			if (isMember && !file.exists()) {
				viewHolder.progressBar.setVisible(true);

				final long fileSize = talkFileSpecific.getLength();
				InFileTransfer transfer = new InFileTransfer(talkFileSpecific.getUrl(), talkFileSpecific.getFilename(),
						fileSize);
				copyTask = new CopyTask(transfer, file, talkFileSpecific);
				progress.set(copyTask.getProgress());

				// When completed tasks
				copyTask.setOnRunning(e -> {
					manageDownloadState();// manage download state
				});
				copyTask.setOnSucceeded(e -> {
					manageDownloadState();// manage download state
				});
				copyTask.setOnCancelled(e -> {
					manageDownloadState();// manage download state
				});
				copyTask.setOnFailed(e -> {
					manageDownloadState();// manage download state
				});

				// Start the Task.
				new Thread(copyTask).start();

			} else {
				manageDownloadState();// manage download state
				Desktop.getDesktop().open(file);// if downloaded, open the file
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * get downloaded file as a File object
	 **/
	private File getDownloadedFile() {
		String path = isMember ? Downloads.getDownloadDirectory() + "/" + talkFileSpecific.getFilename()
				: talkFileSpecific.getFileAbsolutePath();
		return new File(path);
	}

	/**
	 * set file downloading status and change download button text
	 **/
	private void manageDownloadState() {
		try {
			viewHolder.progressBar.setVisible(false);
			File file = getDownloadedFile();
			if (isMember && !file.exists()) {
				viewHolder.downloadBtn.setText(isFileDownloading ? "Cancel Download" : "Download File");
			} else {
				viewHolder.downloadBtn.setText("Open File");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
