package com.xinwei.cusview;

import java.util.Optional;

import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;

import com.xinwei.BackgroundStuff;
import com.xinwei.talk.model.message.specific.TalkResourceSpecific;
import com.xinwei.talk.model.message.specific.TalkVoiceSpecific;
import com.xinwei.util.AudioPlayerUtil;
import com.xinwei.util.FxAudioUtil;
import com.xinwei.util.MsgUtil;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 * This class will handle Audio chat messages.
 * 
 * @author nisal
 **/
public class AudioChatView extends CustomChatView implements LineListener {
	public ViewHolder viewHolder;

	private TalkVoiceSpecific audioMessage;
	private int isRead;
	private long messageID;

	private boolean isPlaying = false;
	private AudioPlayerUtil audioPlayer;

	private DoubleProperty progress = new SimpleDoubleProperty(0);

	/**
	 * @param audioMessage received chat message with audio file
	 * @param isRead       is this message already read or not
	 * @param messageID    received chat message's ID from the local DB
	 **/
	public AudioChatView(TalkVoiceSpecific audioMessage, int isRead, long messageID) {
		this.audioMessage = audioMessage;
		this.isRead = isRead;
		this.messageID = messageID;

		initializeViews();
		updateViews();
	}

	@Override
	public void initializeViews() {
		try {
			viewHolder = new ViewHolder();

			viewHolder.audioController = new Button();
			viewHolder.audioController.setStyle("-fx-background-color: transparent;");
			changePlayImage(false);

			viewHolder.audioProgress = new ProgressBar();
			viewHolder.audioPlayTime = new Label("12.00");
			viewHolder.audioPlayTime.setFont(new Font(10));

			viewHolder.audioProgress.setPrefSize(120, 8);
			viewHolder.audioProgress.setMinSize(120, 8);
			viewHolder.audioProgress.setProgress(0);

			// Fixme : Impl pause button accordingly
			viewHolder.audioController.setOnAction((event) -> {
				manageAudioControlls();// handle audio play/pause/stop
			});

			setPadding(new Insets(8));
			setStyle("-fx-background-color: #e8e9ea; -fx-border-radius: 8; -fx-background-radius: 8;");
			setSpacing(4);
			setAlignment(Pos.CENTER_LEFT);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateViews() {
		try {
			viewHolder.audioProgress.progressProperty().bind(progress);

			getChildren().addAll(viewHolder.audioController, viewHolder.audioProgress/* , viewHolder.audioPlayTime */);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(LineEvent event) {
		try {

			boolean isPlaying = (event.getType() == LineEvent.Type.OPEN || event.getType() == LineEvent.Type.START);

			progress.set(0);
			while (isPlaying) {
				progress.set(
						(double) audioPlayer.getAudioClipCurrentPosition() / (double) audioPlayer.getAudioClipLength());
			}

			if (!isPlaying)
				progress.set(0);

			Platform.runLater(() -> {
				changePlayImage(isPlaying); // change play image
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * change player, play/pause image according to music player status
	 * 
	 * @param isPlaying is audio playing or not
	 **/
	public void changePlayImage(boolean isPlaying) {
		try {
			if (viewHolder == null)
				return;
			viewHolder.audioController.setGraphic(null);

			String img_name = (isPlaying) ? "audio_stop.png" : "audio_play.png";
			Image image = new Image(getClass().getClassLoader()
					.getResource(String.format("images/custom/audio/%s", img_name)).toString(), 30, 30, true, true);
			ImageView imageView = new ImageView(image);

			viewHolder.audioController.setGraphic(imageView);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class ViewHolder {
		public Button audioController;
		public VBox audioTimeContainer;
		public ProgressBar audioProgress;
		public Label audioPlayTime;
	}

	/**
	 * handle all audio play, pause and other audio controls
	 **/
	private void manageAudioControlls() {

		try {

			if (isPlaying) {

				changePlayImage(false);// set pause image

				audioPlayer.gotoChoice(4);
				isPlaying = false;
				progress.set(0);

			} else {
				changePlayImage(true);// set play image

				final TalkResourceSpecific resourceMsg = (TalkResourceSpecific) audioMessage;
				byte[] data = MsgUtil.handleResourceMsg(isRead, messageID, resourceMsg,
						com.xinwei.talk.common.Constants.MSG_TYPE_VOICE);
				audioPlayer = AudioPlayerUtil.getInstance(data);
				audioPlayer.setAudioLineListener(this);

				// VoiceComponent component = new VoiceComponent(audioMsg, left, null, data);
				if (BackgroundStuff.getMuteStatus() == true) {
					if (handleMute())
						audioPlayer.play();
				} else {
					audioPlayer.play();
				}
				isPlaying = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * if user disable all audio outputs, handle it
	 **/
	private boolean handleMute() {

		Alert confirmAlert = new Alert(AlertType.CONFIRMATION, "Application is muted, would you like to disable it ?",
				ButtonType.YES);
		Optional<ButtonType> result = confirmAlert.showAndWait();
		ButtonType button = result.orElse(ButtonType.CANCEL);

		if (button == ButtonType.YES) {
			FxAudioUtil.muteSound(false);
			BackgroundStuff.setMuteStatus(false);

			return true;
		}

		return false;
	}

}
