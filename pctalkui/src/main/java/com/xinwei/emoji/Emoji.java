package com.xinwei.emoji;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import com.xinwei.mainwindow.ChatRoom_Controller;
import com.xinwei.talk.common.Log;

import javafx.animation.ScaleTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Window;
import javafx.util.Duration;

public class Emoji implements Initializable {

	@FXML
	private VBox box;
	
	ChatRoom_Controller control;
	

	@SuppressWarnings("deprecation")
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		FlowPane p = new FlowPane();
		p.setPadding(new Insets(15));
		
		Map<String, String> map = new LinkedHashMap<>();
		map.put("/emoticon/ee_1.png", "[):]");
		map.put("/emoticon/ee_2.png", "[:D]");
		map.put("/emoticon/ee_3.png", "[),)]");
		map.put("/emoticon/ee_4.png", "[:-o]");
		map.put("/emoticon/ee_5.png", "[:p]");
		map.put("/emoticon/ee_6.png", "[(H)]");
		map.put("/emoticon/ee_7.png", "[:@]");
		map.put("/emoticon/ee_8.png", "[:s]");
		map.put("/emoticon/ee_9.png", "[:$]");
		map.put("/emoticon/ee_10.png", "[:(]");
		map.put("/emoticon/ee_11.png", "[:'(]");
		map.put("/emoticon/ee_12.png", "[:|]");
		map.put("/emoticon/ee_13.png", "[(a)]");
		map.put("/emoticon/ee_14.png", "[8o|]");
		map.put("/emoticon/ee_15.png", "[8-|]");
		map.put("/emoticon/ee_16.png", "[+o(]");
		map.put("/emoticon/ee_17.png", "[<o)]");
		map.put("/emoticon/ee_18.png", "[|-)]");
		map.put("/emoticon/ee_19.png", "[*-)]");
		map.put("/emoticon/ee_20.png", "[:-#]");
		map.put("/emoticon/ee_21.png", "[:-*]");
		map.put("/emoticon/ee_22.png", "[^o)]");
		map.put("/emoticon/ee_23.png", "[8-)]");
		map.put("/emoticon/ee_24.png", "[(|)]");
		map.put("/emoticon/ee_25.png", "[(u)]");
		map.put("/emoticon/ee_26.png", "[(S)]");
		map.put("/emoticon/ee_27.png", "[(*)]");
		map.put("/emoticon/ee_28.png", "[(#)]");
		map.put("/emoticon/ee_29.png", "[(R)]");
		map.put("/emoticon/ee_30.png", "[({)]");
		map.put("/emoticon/ee_31.png", "[(})]");
		map.put("/emoticon/ee_32.png", "[(k)]");
		map.put("/emoticon/ee_33.png", "[(F)]");
		map.put("/emoticon/ee_34.png", "[(W)]");

		
		for (String path : map.keySet()) {

			StackPane stackPane = new StackPane();
			int size = 40;
			stackPane.setMaxSize(size, size);
			stackPane.setPrefSize(size,size);
			stackPane.setMinSize(size, size);
			stackPane.setPadding(new Insets(3));
			ImageView imgView = new ImageView();
			imgView.setFitWidth(32);
			imgView.setFitHeight(32);
			Image image = new Image(path);
			imgView.setImage(image);
			
			stackPane.getChildren().add(imgView);

			stackPane.setCursor(Cursor.HAND);
			ScaleTransition animation = new ScaleTransition(Duration.millis(90), imgView);

			stackPane.setOnMouseEntered(e -> {

				imgView.setEffect(new DropShadow());
				animation.setToX(1.2);
				animation.setToY(1.2);
				animation.playFromStart();

			});
			stackPane.setOnMouseExited(e -> {
				imgView.setEffect(null);
				animation.setToX(1.);
				animation.setToY(1.);
				animation.playFromStart();
			});
			stackPane.setOnMouseClicked(e -> {
				Log.info(imgView.getImage().impl_getUrl());
				Log.info(map.get((imgView.getImage().impl_getUrl()).split("(?=/emoticon)")[1]));
				control.area.appendText(map.get((imgView.getImage().impl_getUrl()).split("(?=/emoticon)")[1]));
				Window currentPopup = box.getScene().getWindow();
				currentPopup.hide();
			});
			
			p.getChildren().add(stackPane);
		}
		box.getChildren().add(p);
	}

	public void emoji(ChatRoom_Controller chatRoom_Controller) {
		this.control = chatRoom_Controller;
	}

}
