package com.xinwei.mainwindow;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.jivesoftware.smack.packet.Presence;

import com.xinwei.BackgroundStuff;
import com.xinwei.common.lang.CollectionUtil;
import com.xinwei.comparators.FriendListComparator;
import com.xinwei.comparators.GroupListComparator;
import com.xinwei.screenshot.ScreenCapture;
import com.xinwei.spark.TaskEngine;
import com.xinwei.tabwindow.Chat_Controller;
import com.xinwei.tabwindow.GroupController;
import com.xinwei.tabwindow.ManageGroupMembersController;
import com.xinwei.tabwindow.ShortcutKeysController;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.dao.TalkDao;
import com.xinwei.talk.listener.MessageListener;
import com.xinwei.talk.listener.ObjectListener;
import com.xinwei.talk.listener.PresenceListener;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.XWObject;
import com.xinwei.talk.model.conf.UserConfig;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.utils.MessageType;
import com.xinwei.talk.ui.utils.XMPPMessageResponses;
import com.xinwei.util.GroupUtil;
import com.xinwei.util.ListUtil;
import com.xinwei.util.MsgUtil;
import com.xinwei.util.NotificationUtil;
import com.xinwei.util.RestartUtil;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Main_Controller implements Initializable, MessageListener, PresenceListener, ObjectListener {
	public ManageGroupMembersController manageGroupMembersController;

	public Chat_Controller chat_Controller;// single user list view controller
	public GroupController groupController;// group list view controller

	static Parent rootPO = null;

	private static final String character2 = null;
	public BitSet keyboardBitSet = new BitSet();
	boolean isMaximize = false;
	double x, y;

	BufferedImage img = null;

	@FXML
	private Button close, maximize, minimize, chat, member, group, setting, send;
	@FXML
	private BorderPane bdr;
	@FXML
	private AnchorPane card;
	@FXML
	private Pane card2;
	@FXML
	private FlowPane p1;
	@FXML
	private Pane mainPane;
	@FXML
	public Pane commonPane;
	@FXML
	private TextField input;
	@FXML
	Circle userAtr;
	@FXML
	StackPane MainHbox;
	@FXML
	public HBox tiltlePanel;
	@FXML
	public VBox menuTray;
	String currentUI = null;
	FXMLLoader loader = new FXMLLoader();

	private List<? extends Talk> talkList;
	protected LocalManager localManager = TalkManager.getLocalManager();
	private static Map<String, Talk> memberMap = new HashMap<String, Talk>();
	private TalkDao dao = TalkManager.getTalkDao();

	// added by Nisal to keep base UI container
	@FXML
	public StackPane base_main_ui_container;
	// added by Nisal for keep recent chat user index
	private int recentChatUserIndex = 0;
	private int recentGroupUserIndex = 0;

	/**
	 * This method give user presence author vaasu
	 * 
	 * @param fromJid
	 * @param presence
	 */
	public void presenceReceived(String fromJid, Presence presence) {
		String first = fromJid.substring(fromJid.indexOf("/") + 1);
		List<? extends Talk> talk = dao.getTalkByTel(first);
		Talk talk1 = talk.get(0);
		ObservableList<Talk> recentFriends = FXCollections.observableArrayList();

		ObservableList<Talk> friendList = FXCollections.observableArrayList();
		recentFriends = BackgroundStuff.getInstance().getRcntFriendList();
		friendList = BackgroundStuff.getInstance().getFriendList();
		int position = friendList.indexOf(talk1);
		if (presence.getType().name().equals(Presence.Type.available.toString())) {
			talk1.setPresence(1);
		} else {
			talk1.setPresence(2);
		}
		friendList.set(position, talk1);
		recentFriends.set(recentFriends.indexOf(talk1), talk1);
	}

	@FXML
	protected void handleButtonAction(ActionEvent event) {

		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();

			// re-set selected JID to stop auto selection for lists
			BackgroundStuff.setCurrentJid(null);
			// re-set selections of all lists before start
			BackgroundStuff.resetLists();

			if (button.equals(chat)) {
				BackgroundStuff.setActiveNode("C");
				loadRelatedUI("Chat");
				setSelectStyle(chat);
			} else if (button.equals(member)) {
				BackgroundStuff.setActiveNode("M");
				loadRelatedUI("Chat");
				setSelectStyle(member);
			} else if (button.equals(group)) {
				BackgroundStuff.setActiveNode("G");
				loadRelatedUI("Group");
				setSelectStyle(group);
			} else if (button.equals(setting)) {
				BackgroundStuff.setActiveNode("S");
				loadRelatedUI("Setting");
				setSelectStyle(setting);
			}
		}
	}

	@FXML
	void dragged(MouseEvent event) {
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.setX(event.getSceneX() - x);
		stage.setY(event.getSceneY() - y);
	}

	@FXML
	void pressed(MouseEvent event) {

		x = event.getSceneX();
		y = event.getSceneY();

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		ListenerManager.addPresenceListener(this);

		updateListsFromLocalDB();// update group and chat lists from local DB data

		loadVCard();

		try {

			loadRelatedUI("Chat");

			userAtr.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					chat_Controller.setMyProfile(userAtr);
				}
			});

			menuTray.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
			menuTray.addEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);
			RestartUtil.shutdown();

			// added by Nisal to register this class for incoming messages
			if (!BackgroundStuff.chatListening) {
				ListenerManager.addMessageListener(this);
				BackgroundStuff.chatListening = true;
			}

			// added by Nisal to register this class for ongoing group or contact changes
			ListenerManager.addGroupListener(this);
			ListenerManager.addTalkListener(this);

		} catch (Exception e) {
			Log.error(e);
			e.printStackTrace();
		}

	}

	/**
	 * new message receive/send call back methods
	 **/
	@Override
	public void messageReceived(String fromJid, TalkMessage message) {
		BackgroundStuff bs = BackgroundStuff.getInstance();

		// by Nisal - handle new group adding functions
//		if (message.getMsgType() == XMPPMessageResponses.XMPP_MESSAGE_RES_CODE_GROUP_ADDED.getValue() && message != null
//				&& message.getJid() != null) {
//
//			GroupController.addNewGroupToGroupList(message);// add new group object to group list
//			return;
//		}

		if (message.getUserTel().isEmpty())
			return;

		try {

			NotificationUtil.displayTray(message.getUserTel(), MessageUtil.getMessagePrompt(message));
			if (null != message.getMessageType() && message.getMessageType().equals(MessageType.NOTIFY))
				return;

			if (!message.getIsGroup()) {

				// if this contact is not present in DB, add it as a new one
				if (!ListUtil.isJidInTalkList(bs.getRcntFriendList(), fromJid)
						&& ListUtil.isJidInTalkList(bs.getFriendList(), fromJid)) {

					List<? extends Talk> talkP = dao.getTalkByJid(fromJid);
					Talk exFriend = talkP.get(0);
					BackgroundStuff.addToRecentFriendList(exFriend);

				} else if (!ListUtil.isJidInTalkList(bs.getRcntFriendList(), fromJid)) {

					Talk talk = new Talk();
					talk.setJid(message.getJid());
					talk.setTel(message.getUserTel());
					talk.setNick("");
					BackgroundStuff.addToRecentFriendList(talk);
				}

				recentChatUserIndex = 0;
				bs.getRcntFriendList().forEach((friend) -> {

					if (fromJid.equals(friend.getJid())) {

						friend.setHasMsg(true);
						bs.getRcntFriendList().get(recentChatUserIndex).setHasMsg(true);
						if (BackgroundStuff.getCurrentJid() != null
								&& BackgroundStuff.getCurrentJid().equals(fromJid)) {

							bs.getRcntFriendList().get(recentChatUserIndex).setActiveRoom(true);
							dao.updateMsgReadStatus(message.getId(), TalkMessage.STATUS_READED);

						} else {

							bs.getRcntFriendList().get(recentChatUserIndex).setActiveRoom(false);
							dao.updateMsgReadStatus(message.getId(), TalkMessage.STATUS_UNREAD);

							// added by Vasu for update new message count in recent chat user list
							int count = (bs.getRcntFriendList() != null
									&& bs.getRcntFriendList().size() > recentChatUserIndex && recentChatUserIndex >= 0)
											? bs.getRcntFriendList().get(recentChatUserIndex).getNewMessageCount()
											: 0;
							friend.setNewMessageCount(count + 1);
						}

						bs.getRcntFriendList().set(recentChatUserIndex, friend);

					}

					recentChatUserIndex++;

				});

				if (BackgroundStuff.getCurrentJid() != null && BackgroundStuff.getCurrentJid().equals(fromJid)
						&& chat_Controller != null)
					MsgUtil.addTalkMessage(message, true, chat_Controller.chatRoom_Controller,
							BackgroundStuff.getCurrentJid(), "");

			} else if (BackgroundStuff.getGroupList() != null && BackgroundStuff.getGroupList().size() > 0) {

				// if this group is not present in DB, add it as a new one
//				if (!ListUtil.isJidInGroupList(BackgroundStuff.getGroupList(), fromJid)) {
//					Group group = new Group();
//					group.setJid(message.getJid());
//					group.setName(message.getUserTel());
//					group.setHasMsg(true);
//
//					BackgroundStuff.addToGroupList(group);
//				}

				recentGroupUserIndex = 0;
				BackgroundStuff.getGroupList().forEach((grp) -> {

					if (fromJid.equals(grp.getJid())) {
						grp.setHasMsg(true);
						BackgroundStuff.getGroupList().get(recentGroupUserIndex).setHasMsg(true);

						if (BackgroundStuff.getCurrentJid() != null
								&& BackgroundStuff.getCurrentJid().equals(fromJid)) {

							BackgroundStuff.getGroupList().get(recentGroupUserIndex).setActiveRoom(true);
							dao.updateMsgReadStatus(message.getId(), TalkMessage.STATUS_READED);

						} else {

							BackgroundStuff.getGroupList().get(recentGroupUserIndex).setActiveRoom(false);
							dao.updateMsgReadStatus(message.getId(), TalkMessage.STATUS_UNREAD);

							// added by Vasu for update new message count in recent chat user list
							int count = (BackgroundStuff.getGroupList() != null
									&& BackgroundStuff.getGroupList().size() > recentGroupUserIndex
									&& recentGroupUserIndex >= 0)
											? BackgroundStuff.getGroupList().get(recentGroupUserIndex)
													.getNewMessageCount()
											: 0;
							grp.setNewMessageCount(count + 1);
						}

						BackgroundStuff.getGroupList().set(recentGroupUserIndex, grp);

						dao.updateMsgReadStatus(message.getId(), TalkMessage.STATUS_READED);

					}

					recentGroupUserIndex++;

				});

				if (BackgroundStuff.getCurrentJid() != null && BackgroundStuff.getCurrentJid().equals(fromJid)
						&& groupController != null) {
					groupController.chatRoom_Controller.setIsGroup(true);

					String name = MsgUtil.getNameByIds(BackgroundStuff.getCurrentGroupId(), fromJid);
					Platform.runLater(() -> MsgUtil.addTalkMessage(message, true, groupController.chatRoom_Controller,
							BackgroundStuff.getCurrentJid(), name));
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// After user sent any message, this event is firing. This one was already
	// implemented. All chat history panel updates are now limited to main
	// controller.
	@Override
	public void messageSent(String toJid, TalkMessage message) {
		BackgroundStuff bs = BackgroundStuff.getInstance();
		if (null != message.getMessageType() && message.getMessageType().equals(MessageType.NOTIFY))
			return;
		try {

			if (!message.getIsGroup()) {

				// if this contact is not present in DB, add it as a new one
				if (!ListUtil.isJidInTalkList(bs.getRcntFriendList(), toJid)
						&& ListUtil.isJidInTalkList(bs.getFriendList(), toJid)) {

					List<? extends Talk> talkP = dao.getTalkByJid(toJid);
					Talk exFriend = talkP.get(0);
					BackgroundStuff.addToRecentFriendList(exFriend);

				} else if (!ListUtil.isJidInTalkList(bs.getRcntFriendList(), toJid)) {

					Talk talk = new Talk();
					talk.setJid(message.getJid());
					talk.setTel(message.getUserTel());
					talk.setNick("");
					BackgroundStuff.addToRecentFriendList(talk);
				}

				recentChatUserIndex = 0;
				bs.getRcntFriendList().forEach((friend) -> {

					if (toJid.equals(friend.getJid())) {
						friend.setHasMsg(true);
						bs.getRcntFriendList().set(recentChatUserIndex, friend);
					}

					recentChatUserIndex++;

				});

				if (BackgroundStuff.getCurrentJid() != null && BackgroundStuff.getCurrentJid().equals(toJid)
						&& chat_Controller != null)
					MsgUtil.addTalkMessage(message, false, chat_Controller.chatRoom_Controller,
							BackgroundStuff.getCurrentJid(), "");

			} else if (BackgroundStuff.getGroupList() != null && BackgroundStuff.getGroupList().size() > 0) {

				// if this group is not present in DB, add it as a new one
//				if (!ListUtil.isJidInGroupList(BackgroundStuff.getGroupList(), toJid)) {
//					Group group = new Group();
//					group.setJid(message.getJid());
//					group.setName(message.getUserTel());
//					group.setHasMsg(true);
//
//					BackgroundStuff.addToGroupList(group);
//				}

				recentGroupUserIndex = 0;
				BackgroundStuff.getGroupList().forEach((grp) -> {

					if (toJid.equals(grp.getJid())) {
						grp.setHasMsg(true);
						BackgroundStuff.getGroupList().set(recentGroupUserIndex, grp);
					}
					recentGroupUserIndex++;
				});

				if (BackgroundStuff.getCurrentJid() != null && BackgroundStuff.getCurrentJid().equals(toJid)
						&& groupController != null) {
					groupController.chatRoom_Controller.setIsGroup(true);

					String name = MsgUtil.getNameByIds(BackgroundStuff.getCurrentGroupId(), toJid);
					Platform.runLater(() -> MsgUtil.addTalkMessage(message, false, groupController.chatRoom_Controller,
							BackgroundStuff.getCurrentJid(), name));
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// If user add any group or contact to local DB, this will fire
	@Override
	public void add(Collection<? extends XWObject> objects) {
		updateListsFromLocalDB();// update group and chat lists from local DB data
	}

	// If user update any group or contact in local DB, this will fire
	@Override
	public void update(Collection<? extends XWObject> objects) {
		updateListsFromLocalDB();// update group and chat lists from local DB data
	}

	// If user remove any group or contact from local DB, this will fire
	@Override
	public void delete(Collection<? extends XWObject> objects) {
		updateListsFromLocalDB();// update group and chat lists from local DB data
	}

	public void setSelectStyle(Button button) {
		Button[] buttons = { chat, member, group, setting };
		button.getStyleClass().remove("buttonsvary");
		button.getStyleClass().add("buttonsclicked");

		button.getStyleClass().remove(button.getId());
		button.getStyleClass().add(button.getId() + "clicked");

		for (Button b : buttons) {
			if (!button.equals(b)) {
				b.getStyleClass().remove("buttonsclicked");
				b.getStyleClass().remove(b.getId() + "clicked");

				b.getStyleClass().add("buttonsvary");
				b.getStyleClass().add(b.getId());
			}
		}
	}

	public void setCurrentUI(String ui) {
		this.currentUI = ui;
	}

	public void loadVCard() {
		final Runnable loadVCard = new Runnable() {
			public void run() {
				VCard vCard = BackgroundStuff.getvCard();
				Image img = null;
				try {
					img = new Image(new ByteArrayInputStream(vCard.getAvatar()));
				} catch (Exception e) {
					System.out.println("> FX-Engine : Image byte[] not found, Setting default");
					img = new Image("/images/default_dp.png");
				}
				;
				userAtr.setFill(new ImagePattern(img));
			}
		};

		TaskEngine.getInstance().submit(loadVCard);
	}

	@FXML
	public void logout() {
		// final XMPPConnection con = TalkManager.getConnection();
		// LoginFrameController.getStage().hide();

		String status = ""; // null;

		if (status != null || !false) {
			try {
				// Set auto-login to false;
				TalkManager.getVCard().setAutoLogin(false);
				TalkManager.saveVCard();

			} finally {
				RestartUtil.closeConnectionAndInvoke(status);
				System.exit(0);
			}
		}

	}

	String charact = getCharactor(character2);
	public EventHandler<KeyEvent> keyPressedEventHandler = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {

			System.out.println("Shortcut key pressed   >>>>    " + event.toString());

			keyboardBitSet.set(event.getCode().ordinal(), true);

			event.consume();

			updateKeyboardStatus();

			String code = "CONTROL " + event.getCode().toString();

			if (code.equals(character)) {

				try {

					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(getClass().getResource("/fxml/ShortcutKeys.fxml"));
					Parent root = loader.load();
					ShortcutKeysController shortcutKeysController = loader.getController();

					shortcutKeysController.keyType(getCharactor(character));

				} catch (IOException e) {

					e.printStackTrace();
				}

			}
		}
	};

	public EventHandler<KeyEvent> keyReleasedEventHandler = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {

			System.out.println("Shortcut key Released   >>>>    " + event.toString());

			keyboardBitSet.set(event.getCode().ordinal(), false);

			event.consume();

			updateKeyboardStatus();

		}

	};

	public String character = "";

	public void updateKeyboardStatus() {
		character = "";

		StringBuilder sb = new StringBuilder();
		int count = 0;
		for (KeyCode keyCode : KeyCode.values()) {

			if (keyboardBitSet.get(keyCode.ordinal())) {
				if (count > 0) {
					sb.append(" ");
				}
				count++;
				sb.append(keyCode.toString());

				character = sb.toString();
				UserConfig userConfig = TalkManager.getUserConfig();
				String scr_hotkey = userConfig.getShortKeyValue("Screenshot");
				String main_win_hotkey = userConfig.getShortKeyValue("MainWindow");

				if (scr_hotkey.equals(character)) {

					Platform.runLater(new Runnable() {

						@Override
						public void run() {

							new ScreenCapture().start(new Stage());
							keyboardBitSet.clear();
						}
					});
				}
				if (main_win_hotkey.equals(character)) {
					Platform.runLater(new Runnable() {

						@Override
						public void run() {

							Stage stage = (Stage) minimize.getScene().getWindow();
							stage.setIconified(true);
							keyboardBitSet.clear();
						}
					});
				}

				if (count == 2) {

					getCharactor(character);

				}

			}

		}

	}

	public String getCharactor(String character2) {
		return character2;

	}

	/**
	 * load friend list and group list from local DB and set to singleton
	 * 
	 * @author nisal
	 * 
	 **/
	private void updateListsFromLocalDB() {
//		TaskEngine.getInstance().submit(() -> {
		setFriendList();// load current user's friends and recent chat details
		setGroupList();// load current user joined group details
//		});
	}

	/**
	 * load and keep all available friend list data in a singleton. Then it's easy
	 * to access and modify them without much time delay.
	 **/
	public void setFriendList() {

		ObservableList<Talk> friendList = FXCollections.observableArrayList(); // contacts for ui
		ObservableList<Talk> rcntFriendList = FXCollections.observableArrayList(); // recent messages for ui

		BackgroundStuff bs = BackgroundStuff.getInstance();
		BackgroundStuff.setController(this);

		talkList = dao.getTalkList(); // contact list

		List<TalkMessage> talkMessages = dao.getMessageRecentList(200); // recent message list

		talkList.forEach((talk) -> {
			talk.setImage();
			memberMap.put(talk.getJid(), talk);
			friendList.add(talk);
		});

		talkMessages.forEach((talkMessage) -> {

			if (talkMessage.getSendOrReceive() == TalkMessage.TYPE_RECEIVE && talkMessage.getUserTel() == null)
				return;

			if (!talkMessage.getIsGroup()) {

				if (ListUtil.isJidInTalkList(friendList, talkMessage.getJid())) {

					List<? extends Talk> talkP = dao.getTalkByJid(talkMessage.getJid());
					Talk exFriend = talkP.get(0);
					List<TalkMessage> unread_msgs = dao.getUnReadMessageList(talkMessage.getJid(),
							TalkMessage.STATUS_UNREAD);
					exFriend.setNewMessageCount((unread_msgs == null) ? 0 : unread_msgs.size());
					exFriend.setHasMsg(unread_msgs != null && unread_msgs.size() > 0);
					rcntFriendList.add(exFriend);

				} else if (!ListUtil.isJidInTalkList(friendList, talkMessage.getJid())
						&& !ListUtil.isJidInTalkList(rcntFriendList, talkMessage.getJid())) {

					Talk talk = new Talk();
					talk.setJid(talkMessage.getJid());

					// added by Nisal to fix --> user TP no: not present occasionally issue
					String tel = talkMessage.getUserTel();
					if ((tel == null || tel.isEmpty()) && talkMessage.getTalkBody() != null) {
						if (talkMessage.getSendOrReceive() == TalkMessage.TYPE_RECEIVE)
							tel = talkMessage.getTalkBody().getFrom();
						if (talkMessage.getSendOrReceive() == TalkMessage.TYPE_SEND)
							tel = talkMessage.getTalkBody().getTo();
					}
					talk.setTel(tel);

					List<TalkMessage> unread_msgs = dao.getUnReadMessageList(talkMessage.getJid(),
							TalkMessage.STATUS_UNREAD);
					talk.setNewMessageCount((unread_msgs == null) ? 0 : unread_msgs.size());
					talk.setHasMsg(unread_msgs != null && unread_msgs.size() > 0);
					talk.setNick("");
					rcntFriendList.add(talk);

				}

			}

		});

		// added by Nisal to, make the friend list sort available in any case. after we
		// create an sorted list(this is also a observable list), if we change the list,
		// it will automatically sorted
		SortedList<Talk> sortedContactList = new SortedList<Talk>(friendList, FriendListComparator.getComparator());
		SortedList<Talk> sortedRecentChatList = new SortedList<Talk>(rcntFriendList,
				FriendListComparator.getComparator());
		bs.setSortedFriendList(sortedContactList);
		bs.setSortedRcntFriendList(sortedRecentChatList);

		bs.setFriendList(friendList);
		bs.setRcntFriendList(rcntFriendList);
		localManager.putTalkList(talkList);

		// removed by Nisal (looks like this function used by old app to update their
		// user list)
		// ListenerManager.fireTalkAdd(talkList);

	}

	/**
	 * load and keep all available group list data in a singleton. Then it's easy to
	 * access and modify them without much time delay.
	 **/
	public void setGroupList() {

		ObservableList<Group> groupList = FXCollections.observableArrayList(); // groups for ui

		List<Group> groups = dao.getGroupList();
		if (CollectionUtil.isEmpty(groups))
			return;

		localManager.putGroupList(groups);

		// removed by Nisal - this was already added in Group initialization process
		// ListenerManager.fireGroupAdd(groups);// set presence(availability) of current
		// user in the group

		// get all members who are in all available groups
		// TODO: Nisal find out why this code is here for
		List<Member> memberList = dao.getMemberList();
		localManager.putMemberList(memberList);

		for (Group group : groups) {

			List<Member> memberList2 = localManager.getMemberList(group.getId());
			if (CollectionUtil.isEmpty(memberList2))
				continue;

//			group.setGroupMembers(memberList2);
			GroupUtil.updateGroupWithData(group);// update this group with necessary data
			groupList.add(group);

		}

		// added by Nisal to, make the friend list sort available in any case. after we
		// create an sorted list(this is also a observable list), if we change the list,
		// it will automatically sorted
		SortedList<Group> sortedGroupList = new SortedList<>(groupList, new GroupListComparator());
		BackgroundStuff.setSortedGroupList(sortedGroupList);

		BackgroundStuff.setGroupList(groupList);

	}

	/**
	 * when user clicks on a left menu item, load related UI to center container
	 *
	 * @param ui current fxml file's name to load in center
	 **/
	private void loadRelatedUI(String ui) {

		try {
			BackgroundStuff.resetLists();

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/" + ui + ".fxml"));
			Parent root1 = loader.load();

			if (BackgroundStuff.getActiveNode().equals("C") || BackgroundStuff.getActiveNode().equals("M")) {
				chat_Controller = loader.getController();
				chat_Controller.setBaseViewContainer(base_main_ui_container);
			} else if (BackgroundStuff.getActiveNode().equals("G")) {
				groupController = loader.getController();
				groupController.setBaseViewContainer(base_main_ui_container);
			}

			MainHbox.getChildren().setAll(root1);

		} catch (Exception e) {
			e.printStackTrace();
			Log.error(e);
		}

	}

}