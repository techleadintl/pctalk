package com.xinwei.mainwindow;

import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.events.JFXDrawerEvent;
import com.xinwei.BackgroundStuff;
import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.cusview.AudioChatView;
import com.xinwei.cusview.FileChatView;
import com.xinwei.cusview.ImageChatView;
import com.xinwei.emoji.Emoji;
import com.xinwei.loginwindow.LoginFrameController;
import com.xinwei.manager.FxTransferManager;
import com.xinwei.modelwindow.AddGroup_Controller;
import com.xinwei.modelwindow.SuggestFriendController;
import com.xinwei.nortify.CopyTask;
import com.xinwei.screenshot.ScreenCapture;
import com.xinwei.spark.TaskEngine;
import com.xinwei.tabwindow.ManageGroupMembersController;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.dao.TalkDao;
import com.xinwei.talk.dao.TalkDaoImpl;
import com.xinwei.talk.manager.FileManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.CooTalk;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.model.message.specific.TalkImageSpecific;
import com.xinwei.talk.model.message.specific.TalkNameCardSpecific;
import com.xinwei.talk.model.message.specific.TalkTextSpecific;
import com.xinwei.talk.model.message.specific.TalkVoiceSpecific;
import com.xinwei.talk.ui.chat.component.VoiceComponent;
import com.xinwei.talk.ui.chat.room.TalkChatRoom;
import com.xinwei.talk.ui.utils.MessageType;
import com.xinwei.uiutils.ChatBubble;
import com.xinwei.util.FxAudioUtil;
import com.xinwei.util.ImageUtils;
import com.xinwei.util.ListUtil;
import com.xinwei.util.MsgUtil;
import com.xinwei.util.ScreenUtil;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCharacterCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Popup;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.Window;

public class ChatRoom_Controller implements Initializable {

	@FXML
	public Button send;
	@FXML
	public Button attachment, emoji, screenShot, voiceRecButton, contactDetails;
	@FXML
	public JFXButton hamburger;
	@FXML
	public JFXDrawer drawer;
	@FXML
	public ListView<Object> chatPane;
	@FXML
	public TextField area;
	@FXML
	public Text chatMember;
	@FXML
	private AnchorPane chatList;
	@FXML
	private BorderPane rootBorderPane;
	/*
	 * @FXML private HBox bottomPane;
	 */
	@FXML
	public StackPane vbox, title, bottomPane;
	@FXML
	public ScrollPane chatScroll;

	private CopyTask copyTask;

	final FileChooser fileChooser = new FileChooser();

	public FxAudioUtil recorder = new FxAudioUtil();

	public static ObservableList<Object> chatItems = FXCollections.observableArrayList();

	ProgressIndicator p1 = new ProgressIndicator();
	VCard vCard = TalkManager.getVCard();

	Talk talk;
	boolean ismember;
	boolean isGroup = false;
	TalkDao dao = TalkManager.getTalkDao();
	FileManager fileManager = new FileManager();
	static Parent rootGM = null;

	Screen screen = Screen.getPrimary();
	public Rectangle2D bounds = screen.getVisualBounds();

	@FXML
	public Button manipulate;

	@FXML
	public ImageView optionsImg;

	public ManageGroupMembersController manageGroupMembersController;

	public String groupId;

	ContactDetailsController contactDetailsController;
	public AddGroup_Controller addGroupController;

	// added by Nisal
	public StackPane rootContainerView;

	public String getTel(String tel) {
		String[] countryCodeAndTel = TalkUtil.getCountryCodeAndTel(tel);
		if (countryCodeAndTel == null)
			return getString(tel);
		return countryCodeAndTel[0] + " " + countryCodeAndTel[1];
	}

	public String getString(String str) {
		if (str == null)
			return "";
		return str;
	}

	public String getUid(String uid) {
		if (StringUtil.isBlank(uid))
			return "";
		try {
			return Integer.toHexString(Integer.valueOf(uid)).toUpperCase();
		} catch (Exception e) {
			e.printStackTrace();

			return "";
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		resizeChatPaneHideDrawer();
		resizeChatPane();
		resizeChatPaneDrag();
		Image image = null;
		if (BackgroundStuff.getActiveNode().equals("G")) {
			image = new Image(getClass().getResourceAsStream("/images/setting_2.png"));
			manipulate.setTooltip(new Tooltip("Group Options"));
		} else if (BackgroundStuff.getActiveNode().equals("C") || BackgroundStuff.getActiveNode().equals("M")) {
			image = new Image(getClass().getResourceAsStream("/images/navi-group-grey.png"));
			manipulate.setTooltip(new Tooltip("Add to group"));
		}
		if (image != null && !image.equals(""))
			manipulate.setBackground(new Background(new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
					BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, null)));

		chatPane.setItems(chatItems);

		// chatPane.prefWidthProperty().bind(this.rootGM.);
		// https://stackoverflow.com/questions/6174299/javafx-2-0-set-component-to-full-width-and-height-of-immediate-parent
		chatPane.setOnDragOver(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent event) {
				Dragboard db = event.getDragboard();
				if (db.hasFiles()) {
					event.acceptTransferModes(TransferMode.COPY);
				} else {
					event.consume();
				}
			}
		});
		if (BackgroundStuff.getActiveNode().equals("G")) {
			hamburger.setVisible(false);
		} else {
			hamburger.setVisible(true);
			try {
				hamburger.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
					drawerAction();
					if (drawer.isOpened()) {
						drawer.close();
						vbox.setMaxWidth(bounds.getWidth());
						title.setMaxWidth(bounds.getWidth());
						bottomPane.setMaxWidth(bounds.getWidth());
					} else {
						hamburger.setDisable(true);
						Timer timer = new Timer();
						timer.schedule(new TimerTask() {
							@Override
							public void run() {
								hamburger.setDisable(false);
							}
						}, 450);
						drawer.open();

						// drawer.disabledProperty().removeListener(listener);
						vbox.setMaxWidth(chatPane.getWidth() - 150);
						title.setMaxWidth(chatPane.getWidth() - 150);
						bottomPane.setMaxWidth(chatPane.getWidth() - 150);
					}
				});
			} catch (Exception ex) {
				Logger.getLogger(ChatRoom_Controller.class.getName()).log(Level.SEVERE, null, ex);
				ex.printStackTrace();

			}
		}
		chatPane.setOnDragDropped(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent event) {
				Dragboard db = event.getDragboard();
				boolean success = false;
				if (db.hasFiles()) {
					success = true;
					String filePath = null;
					for (File file : db.getFiles()) {
						filePath = file.getAbsolutePath();
						System.out.println(filePath);
						System.out.println("send drag n drop");
						if (file != null) {
							System.out.println(file.getAbsolutePath());
							// Fixme - repetitive with block above
							TalkChatRoom talkroom = new TalkChatRoom(BackgroundStuff.getCurrentJid(), "name",
									"destination");
							TalkBody bodyMessage = new TalkBody(talkroom.getMessageFrom(), talkroom.getMessageTo());

							String imageFormat = null;
							try {
								imageFormat = ImageUtil.getImageFormat(file);
							} catch (Exception e) {
								System.out.println("> FX-Engine : " + e);
								e.printStackTrace();

							}

							System.out.println("> FX-Engine : Image format - " + imageFormat);
							System.out.println(file.getAbsolutePath());

							if (imageFormat != null) {
//								setImage(file, false, vCard.getNick());
								byte[] data = IOUtil.readBytes(file);
								TalkSpecific msg = new TalkImageSpecific(data, file.getName());
								try {
									bodyMessage.addSpecific(msg);
								} catch (Exception e) {
									System.out.println("> FX-Engine : " + e);
									e.printStackTrace();

								}
								final TalkMessage transcriptMessage = getTalkMessage(null, bodyMessage,
										TalkMessage.TYPE_SEND, null);

								transcriptMessage.setJid(BackgroundStuff.getCurrentJid());
								transcriptMessage.setIsGroup(isGroup);
								doSendMediaMessage(transcriptMessage);
							} else {
//								setFile(BackgroundStuff.getCurrentJid(), false, file, vCard.getNick());
								new FxTransferManager(isGroup).sendFileFromFx(file, BackgroundStuff.getCurrentJid());
							}
						}
					}
				}
				event.setDropCompleted(success);
				event.consume();
			}
		});

		manipulate.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (BackgroundStuff.getActiveNode().equals("G")) {
					System.out.println("Group!!!!");
					popUp("fxml/ManageGroupMembers.fxml");
				} else if (BackgroundStuff.getActiveNode().equals("C")) {
					popUp("fxml/NewGroup.fxml");
				}
			}
		});

		area.setOnKeyPressed((event) -> {
			if (event.getCode() == KeyCode.ENTER) {
				sendMsg();
			}

			if (BackgroundStuff.getActiveNode().equals("G") && (charCombo.match(event) || codeCombo.match(event))) {
				createTagList();
			}
		});

	}

	KeyCombination charCombo = new KeyCharacterCombination("2", KeyCombination.SHIFT_DOWN);
	KeyCombination codeCombo = new KeyCodeCombination(KeyCode.DIGIT2, KeyCombination.SHIFT_DOWN);

	List<String> taggedUsers = new ArrayList<String>();

	private void createTagList() {
		try {
			Group group = TalkManager.getTalkService().getGroup(BackgroundStuff.getCurrentGroupId());
			Collection<Member> memberList = group.getMemberList();

			Popup friendsPopup = new Popup();
			ScrollPane scroll = new ScrollPane();
			scroll.setMaxHeight(300);
			scroll.setMaxWidth(300);
			scroll.setStyle("-fx-background-color: transparent");

			VBox friendSuggest = new VBox();
			scroll.setContent(friendSuggest);
			friendSuggest.setMaxWidth(scroll.getMaxWidth());
			friendSuggest.setMaxHeight(Region.USE_COMPUTED_SIZE);
			// friendSuggest.setBackground(Background.EMPTY);
			friendSuggest.setStyle("-fx-background-color: rgba(255,255,255, 0.5);");
			friendSuggest.setPadding(new Insets(10));

			for (Member object : memberList) {
				if(object.getUid().equals(vCard.getUid()))
					continue;
				
				Label label = new Label(!object.getMemNick().equals("") ? object.getMemNick() : object.getMemJid());
				label.setMinWidth(friendSuggest.getMaxWidth());

				label.setPadding(new Insets(20));
				label.setGraphic(new Circle(10, new ImagePattern(ImageUtils.getImg(object.getMemJid()))));
				label.setStyle(
						"-fx-border-color:#87E1E1; -fx-border-width:1;-fx-background-color: rgba(255,255,255, 0.5);-fx-background-radius: 10;");
				friendSuggest.getChildren().add(label);

				label.setOnMouseClicked(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						if (e.getClickCount() == 2) {
							area.setText(area.getText() + object.getMemNick());
							area.end();
							taggedUsers.add(object.getMemJid());
							friendsPopup.hide();
						}
					}
				});
			}
			friendsPopup.getContent().add(scroll);
			friendsPopup.setAutoHide(true);

			Window parent = area.getScene().getWindow();

			friendsPopup.show(parent, ScreenUtil.getScreenXPostionOfNode(emoji) + emoji.widthProperty().get(),
					ScreenUtil.getScreenYPostionOfNode(emoji) - 230.0);
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private void popUp(String model) {
		try {
//			Stage stage = new Stage();
			FXMLLoader loader1 = new FXMLLoader();
			loader1.setLocation(getClass().getClassLoader().getResource(model));
			rootGM = loader1.load();
			if (BackgroundStuff.getActiveNode().equals("G")) {
				manageGroupMembersController = loader1.getController();
				manageGroupMembersController.rootContainerView = rootContainerView;
			} else if (BackgroundStuff.getActiveNode().equals("C")) {
				addGroupController = loader1.getController();
				addGroupController.setMemberInitialized(true); // to say this was initialized with a member in it
																// already
				addGroupController.rootContainerView = rootContainerView;
			}
			System.out.println(rootContainerView);
			VBox outer = new VBox();
			outer.getChildren().add(rootGM);
			outer.setAlignment(Pos.CENTER);
			outer.setBackground(
					new Background(new BackgroundFill(Color.rgb(0, 0, 0, 0.3), new CornerRadii(0), new Insets(0))));

			// show pop-up dialog in UI
			if (rootContainerView != null
					&& rootContainerView.getChildren().get(rootContainerView.getChildren().size() - 1) != outer) {
				rootContainerView.setAlignment(Pos.CENTER);
				rootContainerView.getChildren().add(outer);
			}

//			Scene scene = new Scene(outer);
//			scene.setFill(Color.rgb(255, 255, 255, 0));
//
//			stage.initStyle(StageStyle.TRANSPARENT);
//			stage.setScene(scene);
//			stage.showAndWait();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendMsg() {
		TalkChatRoom talkroom = new TalkChatRoom(BackgroundStuff.getCurrentJid(), "name", "destination");

		TalkBody bodyMessage = new TalkBody(talkroom.getMessageFrom(), talkroom.getMessageTo());

		String message = area.getText();
		Log.info("message = " + message);
		if (message.trim().isEmpty())
			return;

		bodyMessage.addSpecific(new TalkTextSpecific(message));

		final TalkMessage transcriptMessage = getTalkMessage(null, bodyMessage, TalkMessage.TYPE_SEND, null);
		transcriptMessage.setUserTel(bodyMessage.getTo());

		transcriptMessage.setJid(BackgroundStuff.getCurrentJid());
		transcriptMessage.setIsGroup(isGroup);

		// add message to the history panel
//		MsgUtil.addTalkMessage(transcriptMessage, false, this, BackgroundStuff.getCurrentJid(), "");

		area.clear();

		doSendTalkMessage(transcriptMessage, null);

		if (!taggedUsers.isEmpty()) {
			notifyUser();
		}
	}

	@FXML
	protected void handleButtonAction(ActionEvent event) throws IOException {

		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			TalkChatRoom talkroom = new TalkChatRoom(BackgroundStuff.getCurrentJid(), "name", "destination");
			TalkBody bodyMessage = new TalkBody(talkroom.getMessageFrom(), talkroom.getMessageTo());

			if (button.equals(send)) {
				sendMsg();
			} else if (button.equals(attachment)) {
				chatItems.add(p1);
				configureFileChooser(fileChooser);
				File file = fileChooser.showOpenDialog(((Node) event.getTarget()).getScene().getWindow());
				if (file != null) {

					// check current file is an image or not
					String imageFormat = null;
					try {
						imageFormat = ImageUtil.getImageFormat(file);
					} catch (Exception e) {
						System.out.println("> FX-Engine : " + e);
					}

					if (imageFormat != null) {

//						setImage(file, false, vCard.getNick());
						byte[] data = IOUtil.readBytes(file);
						TalkSpecific msg = new TalkImageSpecific(data, file.getName());
						try {
							bodyMessage.addSpecific(msg);
						} catch (Exception e) {
							System.out.println("> FX-Engine : " + e);
						}
						final TalkMessage transcriptMessage = getTalkMessage(null, bodyMessage, TalkMessage.TYPE_SEND,
								null);

						transcriptMessage.setJid(BackgroundStuff.getCurrentJid());
						transcriptMessage.setIsGroup(isGroup);
						doSendMediaMessage(transcriptMessage);
//						fireMessageSent(transcriptMessage);
					} else {
//						setFile(BackgroundStuff.getCurrentJid(), false, file, vCard.getNick());
						new FxTransferManager(isGroup).sendFileFromFx(file, BackgroundStuff.getCurrentJid());
					}
				}
				chatItems.remove(p1);
			} else if (button.equals(emoji)) {
				Log.info("button is emoji");

				Popup emojiPopup = new Popup();

				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getClassLoader().getResource("fxml/Emoji.fxml"));
				Parent root1 = (Parent) loader.load();
				Emoji room = loader.getController();
				Log.info("message = " + room);

				room.emoji(this);
				emojiPopup.setAutoHide(true);
				emojiPopup.getContent().add(root1);
				Window parent = emoji.getScene().getWindow();

				// added by Nisal to put emoji pop-up in correct place
				// remove pop-up height from y position to place the pop-up in correct place
				emojiPopup.show(parent, ScreenUtil.getScreenXPostionOfNode(emoji) + emoji.widthProperty().get(),
						ScreenUtil.getScreenYPostionOfNode(emoji) - 315.0);

			} else if (button.equals(voiceRecButton)) {
				if (recorder.isRecording) {

					recorder.doStop(button);
					recorder.toFinish();
					// recorder.doPlay();
					byte[] audioBytes = recorder.getAudioBytes();

					VoiceComponent voiceComponent = new VoiceComponent(UUID.randomUUID().toString() + ".amr", true,
							null, audioBytes);

					audioBytes = voiceComponent.getVoice();
					TalkSpecific msg = new TalkVoiceSpecific(audioBytes, voiceComponent.getVoiceName());
					try {
						bodyMessage.addSpecific(msg);
					} catch (Exception e) {
						System.out.println("> FX-Engine : " + e);
					}
					final TalkMessage transcriptMessage = getTalkMessage(null, bodyMessage, TalkMessage.TYPE_SEND,
							null);
					transcriptMessage.setJid(BackgroundStuff.getCurrentJid());
					transcriptMessage.setIsGroup(isGroup);
//					setVoiceMsg(BackgroundStuff.getCurrentJid(), voiceComponent, false, vCard.getNick());
					doSendMediaMessage(transcriptMessage);
					chatItems.remove(p1);

				} else {
					chatItems.add(p1);
					// Fixme: recording response - add proper animation
					Image img = new Image("/images/voice-rec.gif", 20, 20, true, true);
					button.setGraphic(new ImageView(img));
					try {
						recorder.doCapture();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else if (button.equals(screenShot)) {
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						new ScreenCapture().start(new Stage());
					}
				});
			}
			if (!ListUtil.isJidInTalkList(BackgroundStuff.getInstance().getRcntFriendList(),
					BackgroundStuff.getCurrentJid())
					&& ListUtil.isJidInTalkList(BackgroundStuff.getInstance().getFriendList(),
							BackgroundStuff.getCurrentJid())) {

				List<? extends Talk> talkP = dao.getTalkByJid(BackgroundStuff.getCurrentJid());
				Talk exFriend = talkP.get(0);
				BackgroundStuff.addToRecentFriendList(exFriend);
			} else if (!ListUtil.isJidInTalkList(BackgroundStuff.getInstance().getRcntFriendList(),
					BackgroundStuff.getCurrentJid())) {
				Talk talk = new Talk();
				talk.setJid(BackgroundStuff.getCurrentJid());
				talk.setTel(bodyMessage.getTo());
				talk.setNick("");
				BackgroundStuff.addToRecentFriendList(talk);
			}
		}

		setScrollToEnd();

	}

	public void setMultiMsg(Node content, boolean isMember, String name, Date time, TalkMessage talkMessage) {
		if (null != content)
			if (time != null) {
				createChatBubble(isMember, content, null, name, time, MessageType.TEXT, talkMessage);
			} else {
				createChatBubble(isMember, content, null, name, null, MessageType.TEXT, talkMessage);
			}

	}

	public void setFile(String jid2, boolean isMember, File file, String name) {

		Label msgLabel = new Label();
		Label filename = new Label();
		msgLabel = new Label();
		Image img = new Image("/images/chatfile.png", 50, 50, true, true);
		msgLabel.setGraphic(new ImageView(img));
		filename.setText(file.getName());

		msgLabel.setOnMousePressed(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				if (null != file) {
					try {
						Desktop.getDesktop().open(file);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		});

		createChatBubble(isMember, msgLabel, null == file ? null : filename, name, null, MessageType.FILE, null);
	}

	public void setVoiceMsg(String jid, VoiceComponent component, boolean isMember, String name) {

		Button playButton = new Button();
		Image img = new Image("/images/play1.png");
		playButton.setStyle("-fx-background-color: transparent;");

		playButton.setGraphic(new ImageView(img));
		playButton.setOnAction((event) -> {
			if (BackgroundStuff.getMuteStatus() == true) {
				Alert confirmAlert = new Alert(AlertType.CONFIRMATION,
						"Application is muted, would you like to desable it ?", ButtonType.YES);
				Optional<ButtonType> result = confirmAlert.showAndWait();
				ButtonType button = result.orElse(ButtonType.CANCEL);
				if (button == ButtonType.YES) {
					FxAudioUtil.muteSound(false);
					BackgroundStuff.setMuteStatus(false);
					component.play();
				}
			} else {
				component.play();
			}
		});

		createChatBubble(isMember, playButton, null, name, null, MessageType.VOICE, null);
	}

	public void setReceivedFileOperation(TalkFileSpecific receivedFile, boolean isMember, String name, Date time,
			TalkMessage talkMessage) {

		if (receivedFile != null) {
			FileChatView fileChatView = new FileChatView(receivedFile, isMember);
			createChatBubble(isMember, fileChatView, null, name, time, MessageType.FILE, talkMessage);
		}
	}

	/**
	 * After RECEIVED NameCard Printing UI on ChatRoom
	 *
	 * @Author VAASU
	 * @param nameCardMsg is received NameCard Object
	 * @param isUser      is group or single
	 * @param name
	 * @param time
	 *
	 */
	public static void setNameCardOnRoom(TalkNameCardSpecific nameCardMsg, boolean isUser, String name, Date time,
			String jid, TalkMessage talkMessage) {
		VBox nameCard = new VBox();
		Label nameCardTitle = new Label();
		nameCardTitle.setText("Suggest Friend Name Card");
		Label nameCardLine = new Label();
		nameCardLine.setText("--------------------------------------------");
		Label nameCardInstruct = new Label();
		nameCardInstruct.setText("Click to add Friend");
		HBox mainBaseContainer = new HBox(8);
		VBox vBox = new VBox();
		Circle userImgCircle = new Circle(18, 30, 18);
		Label userNameLabel = new Label();
		Label recentChatMsg = new Label();

		userNameLabel.setPrefHeight(29.5);
		userNameLabel.setTextAlignment(TextAlignment.LEFT);
		recentChatMsg.setPrefHeight(29.5);
		recentChatMsg.setTextAlignment(TextAlignment.LEFT);
		Image image = getImg(jid);
		userImgCircle.setFill(new ImagePattern(image));
		userNameLabel.setText(nameCardMsg.getNick());
		recentChatMsg.setText(nameCardMsg.getNote());
		vBox.getChildren().addAll(userNameLabel);
		mainBaseContainer.getChildren().addAll(userImgCircle, vBox);
		nameCard.getChildren().addAll(nameCardTitle, nameCardLine, mainBaseContainer, nameCardInstruct);

		ChatRoom_Controller.createChatBubble(isUser, nameCard, null, name, time, MessageType.NAMECARD, talkMessage);

		System.out.println("======searching telephone number====is=" + nameCardMsg.getSignature());
		mainBaseContainer.setOnMouseClicked(event -> SuggestFriendController.addContact(nameCardMsg.getMobile()));
	}

	public static Image getImg(String jid) {
		Image img = new Image(new ByteArrayInputStream(SuggestFriendController.getAvatarImages(jid)), 50, 50, true,
				true);
		return img;
	}

	public static void createChatBubble(boolean isMember, Node content, Label fileName, String name, Date time,
			MessageType type, TalkMessage message) {
		chatItems.add(new ChatBubble(isMember, content, fileName, name, time, type, message));
		/*
		 * Platform.runLater(new Runnable() {
		 * 
		 * @Override public void run() { chatItems.add(new ChatBubble(isMember, content,
		 * fileName, name, time, type));
		 * 
		 * setScrollToEnd(); } });
		 */
	}

	public void receivedAudioMsgHandler(TalkVoiceSpecific audioMsg, boolean isMember, int isRead, long msgId,
			String name, Date time, TalkMessage talkMessage)
			throws InterruptedException, UnsupportedAudioFileException, IOException, LineUnavailableException {

		AudioChatView audioChatView = new AudioChatView(audioMsg, isRead, msgId);

		createChatBubble(isMember, audioChatView, null, name, time, MessageType.VOICE, talkMessage);
	}

	public void setImage(File file, boolean isMember, String name) {
		if (file != null) {

			Image img = new Image(file.toURI().toString(), 200, 200, true, true);
			ImageView imageView = new ImageView(img);
			imageView.setOnMouseClicked(event -> openImage(null, isMember, file));
			createChatBubble(isMember, imageView, null, name, null, MessageType.IMAGE, null);
		}
	}

	/**
	 * create and show image chat message components in chat history panel
	 * 
	 * @param imageMsg    received chat message with image URL
	 * @param isMember    if true -> message sent by current user, if false ->
	 *                    received message
	 * @param isRead      is this message already read or not
	 * @param msgId       received chat message's ID from the local DB
	 * @param name        message sender's name
	 * @param time        message sent date and time
	 * @param talkMessage current chat message's meta data object
	 **/
	public void receivedImageMsgHandler(TalkImageSpecific imageMsg, boolean isMember, int isRead, long msgId,
			String name, Date time, TalkMessage talkMessage) throws InterruptedException {

		HBox container = new ImageChatView(imageMsg, isMember, isRead, msgId);

		createChatBubble(isMember, container, null, name, time, MessageType.IMAGE, talkMessage);

		dao.updateMsgReadStatus(msgId, TalkMessage.STATUS_READED);

	}

	public void openImage(TalkImageSpecific image, boolean isMember, File file) {

		Desktop dt = Desktop.getDesktop();
		try {
			// if file is not being uploaded
			if (file == null) {
				String path = TalkUtil.getImagesDirectory() + "/" + image.getFilename();
				// if file is being downloaded for the first time
				if (!new File(path).exists()) {
					BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(image.getData()));
					ImageIO.write(bufferedImage, "png", new File(path));
				}
				file = new File(path);
			}
			dt.open(file);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" > FX-Engine : File not found");
		}
	}

	public TalkMessage getTalkMessage(String packetId, TalkBody bodyMessage, int transcriptMessageType, Date time) {
		TalkMessage m = new TalkMessage();
		m.setId(TalkManager.getMessageManager().nextMessageId());
		m.setJid(BackgroundStuff.getCurrentJid());
		m.setSendOrReceive(transcriptMessageType);
		m.setReaded(TalkMessage.STATUS_READED);
		m.setIsGroup(false);
		m.setTalkBody(bodyMessage);
		bodyMessage.setTime(time);
		bodyMessage.setPacketId(packetId);
		return m;
	}

	/**
	 * send new message to given user
	 * 
	 * @param talkMessage user entered new message object
	 * @param to          receiver's reference
	 **/
	public void doSendTalkMessage(TalkMessage talkMessage, String to) {
		final String toJid = (null != to && !to.equals("")) ? to : BackgroundStuff.getCurrentJid();

		String packetId = talkMessage.getTalkBody().getPacketId();
		if (packetId == null) {
			packetId = TalkManager.getMessageManager().nextPackageId();
			talkMessage.getTalkBody().setPacketId(packetId);
		}

		fireMessageSent(talkMessage);
		final AtomicInteger sendAtomicInteger = new AtomicInteger();
		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {

				boolean result = MessageUtil.sendBalloonMessage(talkMessage, TalkManager.getSessionManager().getJID(),
						toJid);
				if (result) {

				} else {

				}
				sendAtomicInteger.incrementAndGet();
			}
		});

	}

	public void doSendMediaMessage(final TalkMessage talkMessage) {
		final AtomicInteger uploadAtomicInteger = new AtomicInteger();
		final AtomicInteger sendAtomicInteger = new AtomicInteger();

		String packetId = talkMessage.getTalkBody().getPacketId();
		if (packetId == null) {
			packetId = TalkManager.getMessageManager().nextPackageId();
			talkMessage.getTalkBody().setPacketId(packetId);
		}

		TaskEngine.getInstance().submit(() -> {
			// boolean result =
			MessageUtil.handResourceMsg(talkMessage, com.xinwei.talk.common.Constants.UPDOWN_TYPE_UPLOAD);
			uploadAtomicInteger.incrementAndGet();
		});

		// uploadSize(2nd param) is always one since new UI send only one msg at a time
		SystemUtil.waitForSize(uploadAtomicInteger, 1, 360 * 1000);

		fireMessageSent(talkMessage);
		// boolean result =
		MessageUtil.sendBalloonMessage(talkMessage, TalkManager.getSessionManager().getJID(),
				BackgroundStuff.getCurrentJid());
		sendAtomicInteger.incrementAndGet();
	}

	public void fireMessageSent(TalkMessage message) {
		ListenerManager.fireMessageSent(message.getJid(), message);
	}

	// ui controls
	private static void configureFileChooser(final FileChooser fileChooser) {
		fileChooser.setTitle("Select Attachment");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Images", "*.*"),
				new FileChooser.ExtensionFilter("JPG", "*.jpg"), new FileChooser.ExtensionFilter("PNG", "*.png"));
	}

	public void setPaneVisible(boolean isVisible) {
		bottomPane.setVisible(isVisible);
	}

	public void setScrollToEnd() {
		List<Object> items = chatPane.getItems();
		int index = items.size() - 1;
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				chatPane.scrollTo(index);
			}
		});
	}

	public void clearChatBox() {
		area.clear();
	}

	public void setIsGroup(boolean value) {
		this.isGroup = value;
	}

	public void setRoomType(boolean value) {
		this.ismember = value;
	}

	public void clearChatRoom() {
		chatPane.getItems().clear();
	}

	public void setChatMember(String text) {
		chatMember.setText(text);
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public void drawerAction() {
		try {
			FXMLLoader loaderContactDetails = new FXMLLoader();
			loaderContactDetails.setLocation(getClass().getClassLoader().getResource("fxml/ContactDetails.fxml"));
			Parent root = loaderContactDetails.load();
			contactDetailsController = loaderContactDetails.getController();

			Image image = MsgUtil.getAvatar(BackgroundStuff.getCurrentJid(), true);
			Circle circle = new Circle(40, 40, 40);
			circle.setCenterX(43);
			circle.setCenterY(43);
			contactDetailsController.getAvatar().setClip(circle);

			contactDetailsController.getAvatar().setImage(image);
			String id = BackgroundStuff.getCurrentJid();
			contactDetailsController.getName().setText(TalkUtil.getDisplayName(id));

			TalkDaoImpl talkDaoImpl = new TalkDaoImpl();
			List<? extends Talk> talkList = talkDaoImpl.getTalkByJid(id);
			CooTalk a = (CooTalk) talkList.get(0);
			contactDetailsController.getRole().setText(talkList.get(0).getSignature());
			contactDetailsController.getLocation().setText(a.getArea());
			contactDetailsController.getMobileNo().setText(getTel(talkList.get(0).getTel()));
			contactDetailsController.getSex()
					.setText((a.getSex() == null || a.getSex().intValue() == 0) ? Res.getMessage("namecard.sex.male")
							: Res.getMessage("namecard.sex.female"));

			contactDetailsController.getUid().setText(talkList.get(0).getUid());

			System.out.println("Contact object   :    " + a);

			VBox box = (VBox) root;
			drawer.setSidePane(box);
		} catch (Exception e2) {
			e2.printStackTrace();

		}
	}

	/**
	 * remove pop-up UI from container view
	 * 
	 * @author nisal
	 **/
	private void closePopUp() {
		if (rootContainerView != null) {
			rootContainerView.getChildren().remove(rootContainerView.getChildren().size() - 1);
		}
	}

	/**
	 * Chat room auto resize when clicked full screen button when opened drawer
	 *
	 * Srinath
	 **/
	private void resizeChatPane() {
		Stage stage = LoginFrameController.getStage();

		stage.maximizedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				System.out.println("Window is maximized");
				drawer.close();
				vbox.setMaxWidth(bounds.getWidth());
				title.setMaxWidth(bounds.getWidth());
				bottomPane.setMaxWidth(bounds.getWidth());

			}
		});
	}

	/**
	 * Chat room auto resize when drag resize when opened drawer
	 *
	 * Srinath
	 **/
	private void resizeChatPaneDrag() {
		Stage stage = LoginFrameController.getStage();

		stage.widthProperty().addListener((obs, oldVal, newVal) -> {
			System.out.println("Window is Resized1");
			drawer.close();
			vbox.setMaxWidth(bounds.getWidth());
			title.setMaxWidth(bounds.getWidth());
			bottomPane.setMaxWidth(bounds.getWidth());
		});

		stage.heightProperty().addListener((obs, oldVal, newVal) -> {
			System.out.println("Window is Resized2");
			drawer.close();
			vbox.setMaxWidth(bounds.getWidth());
			title.setMaxWidth(bounds.getWidth());
			bottomPane.setMaxWidth(bounds.getWidth());
		});
	}

	/**
	 * Chat room auto resize when drawer hide
	 *
	 * Srinath
	 **/
	private void resizeChatPaneHideDrawer() {

		drawer.setOnDrawerClosed(new EventHandler<JFXDrawerEvent>() {

			@Override
			public void handle(JFXDrawerEvent event) {
				System.out.println("Drawer Hidden");
				vbox.setMaxWidth(bounds.getWidth());
				title.setMaxWidth(bounds.getWidth());
				bottomPane.setMaxWidth(bounds.getWidth());
			}
		});

	}

	/*
	 * Send @Tag notification Sachini
	 */
	private void notifyUser() {
		for (String jid : taggedUsers) {
			TalkChatRoom talkroom = new TalkChatRoom(jid, "name", "destination");
			TalkBody bodyMessage = new TalkBody(talkroom.getMessageFrom(), talkroom.getMessageTo());

			bodyMessage.addSpecific(
					new TalkTextSpecific(vCard.getNick() + " tagged you in a message on  " + chatMember.getText()));

			final TalkMessage transcriptMessage = getTalkMessage(null, bodyMessage, TalkMessage.TYPE_SEND, null);
			transcriptMessage.setUserTel(bodyMessage.getTo());

			transcriptMessage.setJid(jid);
			transcriptMessage.setIsGroup(false);
			transcriptMessage.setMessageType(MessageType.NOTIFY);
			doSendTalkMessage(transcriptMessage, jid);
		}
	}
}
