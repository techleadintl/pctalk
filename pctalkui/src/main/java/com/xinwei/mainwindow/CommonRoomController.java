package com.xinwei.mainwindow;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ResourceBundle;

import com.xinwei.modelwindow.NewChatController;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;
import com.xinwei.uiutils.PopUpUtil;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class CommonRoomController implements Initializable {

	@FXML
	private ImageView avatar;

	@FXML
	private Text welcomeTxt;

	@FXML
	private Button convoBtn;

	@FXML
	Circle circle;

	// added by Nisal
	@FXML
	private Text info_label;
	public StackPane rootContainerView;

	NewChatController newChatController;
	ChatRoom_Controller chatRoom_Controller;
	Stage perStage;
	Parent root1;
	Pane chatRoomPane;

	public void setPerStage(Stage perStage) {
		this.perStage = perStage;
	}

	public void setChatRoom_Controller(ChatRoom_Controller chatRoom_Controller) {
		this.chatRoom_Controller = chatRoom_Controller;
	}

	public void setRoot1(Parent root1) {
		this.root1 = root1;
	}

	public void setChatRoomPane(Pane chatRoomPane) {
		this.chatRoomPane = chatRoomPane;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loadVCard();

		
	}

	@FXML
	void handleButtonAction(ActionEvent event) {

		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(convoBtn)) {
				try {
					String model = "NewChat";
					perStage = (Stage) chatRoomPane.getScene().getWindow();
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(getClass().getClassLoader().getResource("fxml/" + model + ".fxml"));
					Parent root = loader.load();
					if (model.equalsIgnoreCase("NewChat")) {
						newChatController = loader.getController();
						newChatController.setChatRoomController(chatRoom_Controller);
						newChatController.setChatRoomPane(chatRoomPane);
						newChatController.setRoot1(root1);
						newChatController.rootContainerView = rootContainerView;
					}

					VBox outer = new VBox();
					outer.setAlignment(Pos.CENTER);
					outer.getChildren().add(root);
					outer.setBackground(new Background(
							new BackgroundFill(Color.rgb(0, 0, 0, 0.3), new CornerRadii(0), new Insets(0))));

					// show pop-up dialog in UI
					if (rootContainerView != null && rootContainerView.getChildren()
							.get(rootContainerView.getChildren().size() - 1) != outer) {
						rootContainerView.setAlignment(Pos.CENTER);
						rootContainerView.getChildren().add(outer);
					}

//					PopUpUtil.popUp(model, perStage, root);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadVCard() {
		final Runnable loadVCard = new Runnable() {
			public void run() {
				VCard vCard = TalkManager.getVCardManager().getVCard();
				Image img = new Image(new ByteArrayInputStream(vCard.getAvatar()));
				circle.setEffect(new DropShadow(+25d, 0d, +2d, Color.DARKGREY));
				circle.setFill(new ImagePattern(img));
				welcomeTxt.setText(welcomeTxt.getText() + " " + vCard.getNick());
			}
		};

		TaskEngine.getInstance().submit(loadVCard);
	}
}
