package com.xinwei.mainwindow;

import java.net.URL;
import java.util.ResourceBundle;

import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.TalkManager;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class ContactDetailsController implements Initializable{
	@FXML
	private ImageView avatar;
	@FXML
	private TextField name;
	@FXML
	private TextField role;
	@FXML
	private TextField location;
	@FXML
	private TextField sex;
	@FXML
	private TextField mobileNo;
	@FXML
	private TextField uid;
	
	public TextField getUid() {
		return uid;
	}

	public void setUid(TextField uid) {
		this.uid = uid;
	}

	protected LocalManager localManager = TalkManager.getLocalManager();
	
	public ImageView getAvatar() {
		return avatar;
	}

	public void setAvatar(ImageView avatar) {
		this.avatar = avatar;
	}

	public TextField getName() {
		return name;
	}

	public void setName(TextField name) {
		this.name = name;
	}

	public TextField getRole() {
		return role;
	}

	public void setRole(TextField role) {
		this.role = role;
	}

	public TextField getLocation() {
		return location;
	}

	public void setLocation(TextField location) {
		this.location = location;
	}

	public TextField getSex() {
		return sex;
	}

	public void setSex(TextField email) {
		this.sex = email;
	}

	public TextField getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(TextField mobileNo) {
		this.mobileNo = mobileNo;
	}

	public LocalManager getLocalManager() {
		return localManager;
	}

	public void setLocalManager(LocalManager localManager) {
		this.localManager = localManager;
	}

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	
	}

	public void setImage(Image image) {
		avatar.setImage(image);
	}

}
