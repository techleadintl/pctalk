package com.xinwei.constants;

public enum ScreenSizes {
	INITIAL_WINDOWS_HEIGHT(560d),
	INITIAL_WINDOWS_WIDTH(700d),
	INITIAL_WINDOWS_HEIGHT_MULTIPLY_FACTOR(0.653),
	INITIAL_WINDOWS_WIDTH_MULTIPLY_FACTOR(0.752);
	
	private double value;
	
	ScreenSizes(double value) {
		this.value = value;
	}
	
	public double getValue() {
		return this.value;
	}
	
}
