package com.xinwei.nortify;

import com.xinwei.http.service.file.InFileTransfer;
import com.xinwei.http.service.file.TransferStatus;
import com.xinwei.talk.common.Downloads;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import javafx.concurrent.Task;
import javafx.scene.layout.FlowPane;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class CopyTask extends Task<File> {

	private InFileTransfer transfer;
	private File downloadedFile;
	private TalkFileSpecific fileMsg;
//    private FlowPane flowPane;//removed by Nisal

	public CopyTask(InFileTransfer transfer, File downloadedFile, TalkFileSpecific fileMsg/* , FlowPane flowPane */) {
		this.transfer = transfer;
		this.downloadedFile = downloadedFile;
		this.fileMsg = fileMsg;
//		this.flowPane = flowPane;//removed by Nisal
	}

	@Override
	protected File call() throws Exception {
		final long fileSize = fileMsg.getLength();
		transfer = new InFileTransfer(fileMsg.getUrl(), fileMsg.getFilename(), fileSize);
		downloadedFile = new File(Downloads.getDownloadDirectory(), fileMsg.getFilename());
		try {
			transfer.recieveFile(downloadedFile);
		} catch (Exception e) {
			Log.error(e);
		}
		final Timer timer = new Timer();
		TimerTask updateProgessBar = new TimerTask() {
			@Override
			public void run() {
				if (transfer.getAmountWritten() >= fileSize || transfer.getStatus() == TransferStatus.error
						|| transfer.getStatus() == TransferStatus.cancelled
						|| transfer.getStatus() == TransferStatus.complete) {
					this.cancel();
					timer.cancel();
					updateProgress(100, 100);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
//                    flowPane.setVisible(false);//removed by Nisal
				} else {
					long p = (transfer.getAmountWritten() * 100 / transfer.getFileSize());
					updateProgress(Math.round(p), 100);
				}

			}
		};

		timer.scheduleAtFixedRate(updateProgessBar, 10, 10);
		return downloadedFile;
	}

}
