package com.xinwei.profile;

import com.gluonhq.charm.glisten.control.Alert;
import com.xinwei.BackgroundStuff;
import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.mainwindow.ChatRoom_Controller;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.HttpException;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.UUID;

public class MyProfileController implements Initializable {
	@FXML
	Circle avatar;
	@FXML
	private Text nicklbl,locLbl,emaillbl,bdaylbl, phonelbl,error;
	@FXML
	private TextField nameLbl;
	@FXML
	private Button uploadBtn;
	
	@FXML
	private Button editNameBtn;
	
	Circle userAtr;
	ChatRoom_Controller chatRoom_Controller;
	Stage perStage;
	Parent root1;
	Pane chatRoomPane;
	File file = null;
	final FileChooser fileChooser = new FileChooser();
    final ImageManager imageManager = new ImageManager();
    private String oldNick;
	public void setPerStage(Stage perStage) {
		this.perStage = perStage;
	}

	public void setChatRoom_Controller(ChatRoom_Controller chatRoom_Controller) {
		this.chatRoom_Controller = chatRoom_Controller;
	}

	public void setRoot1(Parent root1) {
		this.root1 = root1;
	}

	public void setChatRoomPane(Pane chatRoomPane) {
		this.chatRoomPane = chatRoomPane;
	}
	public void setUserAtr(Circle userAtr) {
		this.userAtr = userAtr;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loadVCard();
		nameLbl.setEditable(false);
		nameLbl.setOnKeyPressed((event) -> {
			if (event.getCode() == KeyCode.ENTER) {
				editName(nameLbl.getText());
				nameLbl.setEditable(false);
				editNameBtn.requestFocus();
			}
		});
	}
	
	@FXML
	public void editNickName(ActionEvent event) {
    	if (event.getSource() instanceof Button) {
    		oldNick = nameLbl.getText();
    		nameLbl.setEditable(true);
    		nameLbl.requestFocus();
		}	
    }
	
	public void loadVCard() {
		final Runnable loadVCard = new Runnable() {
			public void run() {
				VCard vCard = TalkManager.getVCardManager().getVCard();
				try {
					Image img = new Image(new ByteArrayInputStream(vCard.getAvatar()));
					avatar.setEffect(new DropShadow(+25d, 0d, +2d, Color.DARKGREY));
					avatar.setFill(new ImagePattern(img));	
				} catch (Exception e) {
					// TODO: handle exception
				}
	
				nameLbl.setText(" " + vCard.getNick());
				nicklbl.setText(" " + vCard.getName());
				locLbl.setText(" " + "Colombo, Sri Lanka");
				emaillbl.setText(" " + "abc@sampley.com");
				bdaylbl.setText(" " + "09/09/1990");
				phonelbl.setText(" " + vCard.getTel());
			}
		};

		TaskEngine.getInstance().submit(loadVCard);
	}
	
	@FXML
	public void handleButtonAction(ActionEvent event) {
    	if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(uploadBtn)) {				
				file = fileChooser.showOpenDialog(((Node) event.getTarget()).getScene().getWindow());
				if (file != null) {
					setImage(file);
					confirmImageChange(file);
				}
				
			}
    	}
	}
	
	public void setImage(File file) {
		Image img = new Image(file.toURI().toString(), 200, 200, true, true);
		avatar.setFill(new ImagePattern(img));;
	}
	
	public void confirmImageChange(File file) {
		VCard vCard = BackgroundStuff.getvCard();
		byte[] data = IOUtil.readBytes(file);	
		
		Image img = new Image(file.toURI().toString(), 200, 200, true, true);
		userAtr.setFill(new ImagePattern(img));	
		
		String imageFormat = ImageUtil.getImageFormat(file);
		String tel = vCard.getTel();
		String fileName = tel + "_avatar_" + UUID.randomUUID() + "." + imageFormat;
		try {
			TalkManager.getFileService().uploadAvatarFile(fileName, data, vCard);
		} catch (Exception e) {
			e.printStackTrace();
		}

		vCard.setAvatar(data);
		ImageManager.setAvatar(vCard.getJid(), data);
		TalkManager.saveVCard();
		ListenerManager.fireVcardChanged();
		BackgroundStuff.setvCard(vCard);
	}
	
	private void editName(String name) {
		try {
			
			int code = TalkManager.getTalkService().editName(name);
			VCard me = TalkManager.getVCardManager().getVCard();
			me.setNick(name);
			TalkManager.getAppDao().updateLogin(me);
			
			if(code==0) {
				Alert alert = new Alert(AlertType.INFORMATION, "Sucessfully Updated");
				alert.showAndWait();
			}
		} catch (HttpException e) {
			String err1 = "CooTalk can only be modified once.!";
			String err2 = "The CooTalk number already exists.!";
			System.out.println(e.getErrorCode()==1?err1:err2);
			error.setText(e.getErrorCode()==1?err1:err2);
			nameLbl.setText(oldNick);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
}
