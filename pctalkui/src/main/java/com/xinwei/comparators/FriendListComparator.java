package com.xinwei.comparators;

import java.util.Comparator;

import com.xinwei.talk.model.CooTalk;
import com.xinwei.talk.model.Talk;

public class FriendListComparator implements Comparator<Talk> {

	@Override
	public int compare(Talk arg0, Talk arg1) {
		String name_1, name_2;
		name_1 = arg0.getNick();
		name_2 = arg1.getNick();
		if (arg0 instanceof CooTalk && (name_1 == null || name_1.isEmpty()))
			name_1 = ((CooTalk) arg0).getRemarkNick();
		if (arg1 instanceof CooTalk && (name_2 == null || name_2.isEmpty()))
			name_2 = ((CooTalk) arg1).getRemarkNick();

		int name = (name_1 == null || name_2 == null) ? 0 : name_1.compareTo(name_2);
		int tp = (arg0.getTel() == null || arg1.getTel() == null) ? 0 : arg0.getTel().compareTo(arg1.getTel());

		return (name == 0) ? tp : name;
	}

	/**
	 * get custom comparator object created specifically for Friend list
	 * 
	 * @author nisal
	 **/
	public static Comparator<Talk> getComparator() {
		return Comparator.comparing((Talk t) -> t.getNewMessageCount()).reversed()
				.thenComparing(new FriendListComparator());
	}

}
