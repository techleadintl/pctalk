package com.xinwei.comparators;

import java.util.Comparator;

import com.xinwei.talk.model.Group;

public class GroupListComparator implements Comparator<Group> {

	@Override
	public int compare(Group arg0, Group arg1) {

		int name = (arg0.getName() == null || arg1.getName() == null) ? 0 : arg0.getName().compareTo(arg1.getName());
		int jid = (arg0.getJid() == null || arg1.getJid() == null) ? 0 : arg0.getJid().compareTo(arg1.getJid());

		return (name == 0) ? jid : name;
	}

	/**
	 * get custom comparator object created specifically for Friend list
	 * 
	 * @author nisal
	 **/
	public static Comparator<Group> getComparator() {
		return Comparator.comparing((Group t) -> t.getNewMessageCount()).reversed()
				.thenComparing(new GroupListComparator());
	}

}
