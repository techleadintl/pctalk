package com.xinwei.uiutils;

import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListCell;
import com.xinwei.loginwindow.CountryCode;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

public class ComboBox {
	
	/**
	 * @author Charith
	 * @param country_cmb
	 */
	public void getCountryListComboBox(JFXComboBox<String>  country_cmb) {
		Map<String, String> countryCodes ;
		//Map<String, String> countryListMap = TalkManager.getLocalManager().get("countrycode");
		ObservableList<String> list = FXCollections.observableArrayList();
		
		//countryListMap.forEach((k,v) -> {list.add(Res.getMessage(k));});
		countryCodes = new CountryCode().getCountryCode();
		//Map<String, String> countryListMap = countryCodes.entrySet().iterator().next();
		for( String list2 : countryCodes.values()) {
			list.addAll(list2);
		}
		country_cmb.setItems(list);
		country_cmb.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {

			@Override
			public ListCell<String> call(ListView<String> param) {
				return new JFXListCell<String>() {
					private ImageView imgView;
					{
						imgView = new ImageView();
						imgView.setFitWidth(25);
						imgView.setFitHeight(18);
					}
					@Override
					protected void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							try {
								String iconPath = "/talk/images/country/";
								for (Entry<String, String> entry : countryCodes.entrySet()) {
									if (Objects.equals(item, entry.getValue())) {
										iconPath = iconPath + entry.getValue().toLowerCase().replace(" ", "_")
												.replace("-", "_").replace(".", "") + "_" + entry.getKey() + ".png";
									}
								}
								
								Image image = new Image(getClass().getResourceAsStream(iconPath));
								imgView.setImage(image);
								setGraphic(imgView);
							} catch (Exception e) {
								Log.warn(e.toString());
							}
							
						}
					}
				};
			}
		});
	}
}
