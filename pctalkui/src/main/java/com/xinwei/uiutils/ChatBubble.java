package com.xinwei.uiutils;

import com.xinwei.BackgroundStuff;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.dao.TalkDao;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.ui.utils.MessageType;
import com.xinwei.util.MsgUtil;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.SVGPath;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Sachini
 *
 */
public class ChatBubble extends VBox {
	Label fileName;
	String name;
	Date time;
	private SVGPath directionIndicator;
	private Node content;
	private boolean isMember;
	private VBox y;
	MessageType type;

	// added by Nisal as base view
	private HBox container;
	private Label senderName;
	private TalkMessage talkMessage;

	/**
	 * @param isMember      False for sent True for recieved
	 * @param content
	 * @param fileName
	 * @param name
	 * @param time
	 * @param type
	 * @param senderDetails details of the person who sends the message
	 */
	public ChatBubble(boolean isMember, Node content, Label fileName, String name, Date time, MessageType type,
			TalkMessage talkMessage) {
		this.isMember = isMember;
		this.content = content;
		this.fileName = fileName;
		this.name = name;
		this.time = time;
		this.type = type;
		this.talkMessage = talkMessage;
		setupElements();
	}

	private void setupElements() {

		directionIndicator = new SVGPath();
		setStyle("-fx-padding: 10 10 10 10;");

		// add current sender's image
		Circle avatar = new Circle(20);

		y = new VBox(5);

		// added by Nisal to show sender's details in group chat
		container = new HBox();
		senderName = new Label();

		// retrieve and show group member details from local DB
		setGroupMemberDetails(senderName, avatar);

		Label dateLbl = new Label();
		if (time == null) {
			dateLbl.setText(MsgUtil.getTimeNow());
		} else {
			SimpleDateFormat dt = new SimpleDateFormat("MM-dd hh:mm a");
			String date = dt.format(time);
			dateLbl.setText(date);
		}
		dateLbl.setStyle("-fx-font-size: 6.5pt; -fx-text-fill: #B7C4CD;");
		senderName.setStyle("-fx-font-size: 7.5pt; -fx-text-fill: #B7C4CD;");

		if (isMember) {
			configureForReceiver(avatar);
		} else {
			configureForSender(avatar);
		}

		getChildren().addAll(senderName, container);

		if (type.equals(MessageType.FILE)) {
			container.setSpacing(8);
//			fileName.setStyle("-fx-font-size: 8pt; -fx-text-fill: #454d58;");
			y.getChildren().addAll(content, /* fileName, */ dateLbl);
		} else if (type.equals(MessageType.VOICE)) {
			container.setSpacing(8);
			y.getChildren().addAll(content, dateLbl);
		} else if (type.equals(MessageType.NAMECARD)) {
			y.getChildren().addAll(content, dateLbl);
		} else {
			y.getChildren().addAll(content, dateLbl);
		}

		// added by Nisal to show/hide the sender name only in group chat
		senderName.setVisible(
				isMember && BackgroundStuff.getActiveNode() != null && BackgroundStuff.getActiveNode().equals("G"));

	}

	private void configureForSender(Circle avatar) {

		container.setAlignment(Pos.TOP_RIGHT);
		setAlignment(Pos.TOP_RIGHT);

		directionIndicator.setContent(
				"m 77.992338,56.403976 c 4.33053,0.03149 0.951746,3.511604 -1.672472,7.234842 -0.987467,1.401016 -1.090816,4.461674 -1.7451,6.202697 l -0.01227,-13.462479 z");
		directionIndicator.setFill(Color.web("#3cdeb5"));
		if (type.equals(MessageType.TEXT) || type.equals(MessageType.IMAGE) || type.equals(MessageType.NAMECARD)) {
			content.getStyleClass().add("chabbl-send");
			container.getChildren().addAll(y, directionIndicator, avatar);
		} else {
			container.getChildren().addAll(y, avatar);
		}

	}

	private void configureForReceiver(Circle avatar) {

		container.setAlignment(Pos.TOP_LEFT);
		setAlignment(Pos.TOP_LEFT);
		senderName.setPadding(new Insets(0, 0, 8, 45));

		directionIndicator.setContent(
				"m 76.769623,56.403976 c -4.602133,0.03149 -1.011438,3.511604 1.777366,7.234842 1.049399,1.401016 1.15923,4.461674 1.85455,6.202697 l 0.01304,-13.462479 z");
		directionIndicator.setFill(Color.web("#ECEBF3"));
		if (type.equals(MessageType.TEXT) || type.equals(MessageType.IMAGE) || type.equals(MessageType.NAMECARD)) {
			content.getStyleClass().add("chabbl-rcv");
			container.getChildren().addAll(avatar, directionIndicator, y);
		} else {
			container.getChildren().addAll(avatar, y);
		}

	}

	/**
	 * set group member avatar image from DB
	 * 
	 * @author nisal
	 * @param senderName current group member's name
	 * @param circle     current group memeber UI component for set the image
	 **/
	private void setGroupMemberDetails(Label senderName, Circle circle) {

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {

				try {

					Image image = MsgUtil.getAvatar(BackgroundStuff.getCurrentJid(), isMember);

					// remove node checking --> recent group chats also need to be added to the
					// recent chat list
					if (talkMessage != null
							&& talkMessage.getIsGroup()/* && BackgroundStuff.getActiveNode().equals("G") */) {

						Member member = TalkManager.getTalkDao().getMemberByJid(talkMessage.getTalkBody().getTo(),
								String.format("weiquan#camtalk_%s@talkim.cootel.com",
										talkMessage.getTalkBody().getFrom()));

						if (member != null && member.getAvatar() != null && member.getAvatar().length > 0)
							image = new Image(new ByteArrayInputStream(member.getAvatar()), 50, 50, true, true);

						Platform.runLater(() -> {

							String sender_name = talkMessage.getTalkBody().getFrom();
							if (member != null && member.getMemNick() != null && !member.getMemNick().isEmpty()) {
								sender_name = String.format("%s (%s)", member.getMemNick(),
										talkMessage.getTalkBody().getFrom());
							}
							senderName.setText(sender_name);

						});

					}

					circle.setFill(new ImagePattern(image));

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

	}

}
