package com.xinwei.uiutils;

import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class PopUpUtil {

	public static void popUp(String model, Stage perStage, Parent root ) {
		try {

			double x = perStage.getX();
			double y = perStage.getY();

			Stage stage = new Stage();

			stage.setX(x);
			stage.setY(y);

			VBox outer = new VBox();
			outer.getChildren().add(root);
			outer.setPadding(new Insets(110d, 330d, 90d, 350d));
			outer.setBackground(new Background(new BackgroundFill(Color.rgb(0, 0, 0, 0.3), new CornerRadii(0), new Insets(0))));

			Scene scene = new Scene(outer);
			scene.setFill(Color.rgb(255, 255, 255, 0));

			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			stage.showAndWait();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void showAlert(String message, AlertType alertType) {
		Alert alert = new Alert(alertType, message, ButtonType.OK);
		alert.show();
	}
}
