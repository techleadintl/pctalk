package com.xinwei.photoalbum;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import com.xinwei.BackgroundStuff;
import com.xinwei.common.lang.IOUtil;
import com.xinwei.common.lookandfeel.util.ImageUtil;
import com.xinwei.mainwindow.ChatRoom_Controller;
import com.xinwei.manager.FxTransferManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkImageSpecific;
import com.xinwei.talk.ui.chat.room.TalkChatRoom;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Slider;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.SepiaTone;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.Window;


public class PhotoAlbumController implements Initializable {
	ChatRoom_Controller myControllerHandle;
    private static Logger logger = Logger.getLogger(PhotoAlbumController.class.getName());


    private Stage primaryStage;

    private DoubleProperty brightnessProperty = new SimpleDoubleProperty(1);
    private DoubleProperty saturationProperty = new SimpleDoubleProperty(0);
    private DoubleProperty sepiaProperty = new SimpleDoubleProperty(0);
	VCard vCard = TalkManager.getVCard();

    ProgressIndicator p1 = new ProgressIndicator();
    @FXML private ListView<Photo> photoListView;
    @FXML private ImageView imageView;
    @FXML private VBox vBox;
    @FXML private Label photoNameLabel;
    @FXML private HBox brightnessBox;
    @FXML private Slider brightnessSlider;
    @FXML private HBox sepiaBox;
    @FXML private Slider sepiaSlider;
    @FXML private HBox buttonBox;

    @FXML private HBox saturationBox;
    @FXML private Slider saturationSlider;
    @FXML private Button sendBtn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    	
    	try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ChatRoom.fxml"));
			Parent root = (Parent) loader.load();

			myControllerHandle = loader.getController();
			

		}catch (Exception e) {
			e.printStackTrace();
		}
    	
    	myControllerHandle.chatPane.setItems(ChatRoom_Controller.chatItems);
    }

    public void setupUi() {

        File path1 = lastFile();
        String fileName = "file:///"+path1.toString();
       
        imageView.setImage(new Image(new Photo("Screenshot", fileName).getPhotoFilename()));
        //photoNameLabel.setText(new Photo("Screenshot", fileName).getPhotoName());

        primaryStage.getScene().widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                vBox.setMaxWidth(primaryStage.getScene().getWidth() - photoListView.getWidth());
                imageView.setFitWidth(vBox.getMaxWidth());
            }
        });

        primaryStage.getScene().heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                vBox.setMaxHeight(primaryStage.getScene().getHeight());
                imageView.setFitHeight(calculateMaxImageHeight());
            }
        });

        vBox.setMaxWidth(primaryStage.getScene().getWidth() - photoListView.getWidth());
        imageView.setFitWidth(vBox.getMaxWidth());

        vBox.setMaxHeight(primaryStage.getScene().getHeight());
        imageView.setFitHeight(calculateMaxImageHeight());

        brightnessSlider.valueProperty().bindBidirectional(brightnessProperty);
        brightnessProperty.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                setImageEffect(path1);
            }
        });

        saturationSlider.valueProperty().bindBidirectional(saturationProperty);
        saturationProperty.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                setImageEffect(path1);
            }
        });

        sepiaSlider.valueProperty().bindBidirectional(sepiaProperty);
        sepiaProperty.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                setImageEffect(path1);
            }
        });
    }

    public File lastFile() {
    	// FileFilter that accepts all files except directories
    	FileFilter noDirectories = new FileFilter() {
    	public boolean accept(File f) {
    		System.out.println(f);
    	return !f.isDirectory();
    	
    		}
    	
    	};

    	// Comparator for modification date in descending order
    	Comparator<File> descendingOnModificationDate = new Comparator<File>() {
    	public int compare(File f1, File f2) {
    	long diff = f1.lastModified() - f2.lastModified();
    	int returnValue;
    	if (diff < 0L) {
    	returnValue = -1;
    	} else if (diff > 0L) {
    	returnValue = +1;
    	} else {
    	assert diff == 0L;
    	returnValue = 0;
    	}
    	return -returnValue; // +returnValue for ascending order
    		}
    	};

    	File directory = new File(System.getProperty("user.home")+ "/AppData/Local/CamTalk/log", "screenshots"); 
    	
    	// Obtain non-directory files in the directory
    	File[] filesInDirectory = directory.listFiles(noDirectories);
    	
    	// Sort the list on modification date in descending order
    	Arrays.sort(filesInDirectory, descendingOnModificationDate);

    	System.out.print("First value    >>>>>            "+filesInDirectory[0]);

    	return filesInDirectory[0];

    	}
    
    @FXML
    public void normalAction() {
        brightnessProperty.set(1);
        saturationProperty.set(0);
        sepiaProperty.set(0);
    }

    @FXML
    public void brightAction() {
        brightnessProperty.set(1.5);
        saturationProperty.set(0);
        sepiaProperty.set(0);
    }

    @FXML
    public void dimAction() {
        brightnessProperty.set(0.5);
        saturationProperty.set(0.2);
        sepiaProperty.set(0);
    }

    @FXML
    public void oldSchoolLook() {
        brightnessProperty.set(1.3);
        saturationProperty.set(0.2);
        sepiaProperty.set(0.7);
    }

    
    String jid = null;
	boolean ismember;
	boolean isGroup = false;
	String time = "00:00 AM";
	
	private File takeSnapShot(Node node){

        WritableImage writableImage = node.snapshot(new SnapshotParameters(), null);
        File folder = new File(System.getProperty("user.home")+ "/AppData/Local/CamTalk/log", "screenshots");
	       
        folder.mkdirs(); 
        
        
        try {
        	File file = File.createTempFile("CooTalkPC_img", ".jpg", folder);
            ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", file);
            System.out.println("snapshot saved: " + file.getAbsolutePath());
            return file;
        } catch (IOException ex) {
           // Logger.getLogger(JavaFXImage.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
	
    @FXML
    public void sendScreenShot(ActionEvent event) {
    	
    		ProgressIndicator p1 = new ProgressIndicator();
    		ChatRoom_Controller.chatItems.add(p1);
    		Node source = (Node) event.getSource();
    		Window theStage = source.getScene().getWindow();
    		theStage.hide();
    	
    		takeSnapShot(imageView);
    		
        	File file = lastFile();

			String jid = BackgroundStuff.getCurrentJid();

			TalkChatRoom talkroom = new TalkChatRoom(jid, "name", "destination");
			TalkBody bodyMessage = new TalkBody(talkroom.getMessageFrom(), talkroom.getMessageTo());
			
			String imageFormat = null;
			try {
				imageFormat = ImageUtil.getImageFormat(file);
			} catch (Exception e) {
				System.out.println("> FX-Engine : " + e);
			}

			System.out.println("> FX-Engine : Image format - " + imageFormat);
			
			if(imageFormat != null) {
				try {
					FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ChatRoom.fxml"));
					Parent root = (Parent) loader.load();

					myControllerHandle = loader.getController();
					

				}catch (Exception e) {
					e.printStackTrace();
				}
				setImageEffect(file);
				myControllerHandle.setImage(file, false,vCard.getNick());
				byte[] data = IOUtil.readBytes(file);
				TalkSpecific msg = new TalkImageSpecific(data, file.getName());
				try {
					bodyMessage.addSpecific(msg);
				} catch (Exception e) {
					System.out.println("> FX-Engine : " + e);
				}
				final TalkMessage transcriptMessage = myControllerHandle.getTalkMessage(null, bodyMessage, TalkMessage.TYPE_SEND,
						null);

				transcriptMessage.setJid(jid);
				transcriptMessage.setIsGroup(isGroup);
				myControllerHandle.doSendMediaMessage(transcriptMessage);
				myControllerHandle.fireMessageSent(transcriptMessage);
				

			}else {
				myControllerHandle.setFile(jid, false, file,vCard.getNick());
				new FxTransferManager(false).sendFileFromFx(file, jid);						

			}    	
			ChatRoom_Controller.chatItems.remove(p1);
			System.out.println("Screenshot sent");
			myControllerHandle.setScrollToEnd();
    }
    
    private void setImageEffect(File file) {
        ColorAdjust colorAdjust = new ColorAdjust();
        colorAdjust.setBrightness((1 - brightnessProperty.get()) * -1);
        colorAdjust.setSaturation(saturationProperty.get());

        SepiaTone st = new SepiaTone();
        st.setLevel(sepiaProperty.get());

        colorAdjust.setInput(st);

        imageView.setEffect(colorAdjust);
    }

    private double calculateMaxImageHeight() {
        return vBox.getMaxHeight() - brightnessBox.getHeight() - saturationBox.getHeight() - buttonBox.getHeight() - sepiaSlider.getHeight();
    }

    @FXML
    public void onListChange() {
        logger.info("onListChange");
    }



    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
}
