package com.xinwei.photoalbum;

import java.io.IOException;
import java.net.URL;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;


public class MainClass extends javafx.application.Application {
	
    private static final Logger logger = Logger.getLogger(MainClass.class.getName());

    private PhotoAlbumController mainController;

    public void photoEditor() {
        launch();
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        configureLog4J();
        
        URL resource = FileUtil.getUrlForResource("fxml/PhotoAlbum.fxml");

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(resource);
            BorderPane page = (BorderPane) fxmlLoader.load();
            mainController = (PhotoAlbumController) fxmlLoader.getController();
            mainController.setPrimaryStage(stage);
            
            Scene scene = new Scene(page, 600, 500);

            stage.setScene(scene);

            stage.setTitle("CooTalkPC Screenshots");
            stage.show();
        	Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        	stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        	stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);
        	
            mainController.setupUi();
            logger.info("Showed Primary Stage");
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void configureLog4J() throws IOException {
    	
        Logger root = Logger.getRootLogger();
        if (!root.getAllAppenders().hasMoreElements()) {
           
            root.setLevel(Level.INFO);
            root.addAppender(new ConsoleAppender(new PatternLayout("%-5p [%t]: %m%n")));
            
        }
    }
    public String getScreenCapture(String path) {
		return path;
    	
    }

}
