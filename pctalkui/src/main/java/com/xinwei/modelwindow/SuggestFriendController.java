package com.xinwei.modelwindow;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.jivesoftware.smack.packet.Presence;

import com.xinwei.BackgroundStuff;
import com.xinwei.common.lang.HttpUtils;
import com.xinwei.mainwindow.ChatRoom_Controller;
import com.xinwei.spark.TaskEngine;
import com.xinwei.spark.ui.PresenceListener;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.CooTalk;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkNameCardSpecific;
import com.xinwei.talk.ui.chat.room.TalkChatRoom;
import com.xinwei.uiutils.PopUpUtil;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

/**
 * Created By Vaasu to Suggest friend
 */

public class SuggestFriendController implements Initializable, PresenceListener {

    @FXML
    private ListView<Talk> mbrList;
    @FXML
    private Button suggestBtn;
    @FXML
    private Button close;
    private ObservableList<Talk> friends = FXCollections.observableArrayList();
    private ChatRoom_Controller chatRoomController;
    private Talk selectedFriend;

    public StackPane rootContainerView;

    public void setChatRoomController(ChatRoom_Controller chatRoomController) {
        this.chatRoomController = chatRoomController;
    }

    public Talk getSelectedFriend() {
        return selectedFriend;
    }

    public void setSelectedFriend(Talk selectedFriend) {
        this.selectedFriend = selectedFriend;
    }

    /**
     * @param event for send suggest button
     */
    @FXML
    void handleButtonAction(ActionEvent event) {

        if (event.getSource() instanceof Button) {
            Button button = (Button) event.getSource();
            if (button.equals(close)) {
                closePopUp();// close pop-up
            } else if (button.equals(suggestBtn)) {
                suggestFriend();
            }
        }
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {

        friends = BackgroundStuff.getInstance().getFriendList();
        mbrList.setItems(friends);
        mbrList.setCellFactory(list -> new AttachmentListCell());
        mbrList.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2) {
                suggestFriend();
            }
        });
    }

    private void suggestFriend() {
        System.out.println("------Suggest Button Pressed--------");
        chatRoomController.area.clear();
        chatRoomController.setPaneVisible(true);

        String id = mbrList.getSelectionModel().getSelectedItem().getJid();
        System.out.println("------Suggest Member id here----" + id + "---------");
        BackgroundStuff.setCurrentJid(selectedFriend.getJid());
        Talk selectedTalk = mbrList.getSelectionModel().getSelectedItem();

        TalkChatRoom talkroom = new TalkChatRoom(selectedFriend.getJid(), "name", "destination");
        TalkBody bodyMessage = new TalkBody(talkroom.getMessageFrom(), talkroom.getMessageTo());
        String nameSuggestedFriend = "";
        if (selectedTalk instanceof CooTalk) {

            CooTalk cooTalk = (CooTalk) selectedTalk;
            nameSuggestedFriend = (!(cooTalk.getNick()).equals("")) ? cooTalk.getNick() : cooTalk.getRemarkNick();
            System.out.println("====nameSuggestedFriend=====" + nameSuggestedFriend);
        } else {
            nameSuggestedFriend = (!(selectedTalk.getNick()).equals("")) ? selectedTalk.getNick() : selectedTalk.getTel();
            System.out.println("====nameSuggestedFriend=====" + nameSuggestedFriend);
        }
        TalkSpecific msg = new TalkNameCardSpecific(nameSuggestedFriend,
                selectedTalk.getSignature(),
                selectedTalk.getUid(),
                selectedTalk.getJid(),
                selectedTalk.getTel(),
                selectedTalk.getTel(),
                selectedTalk.getAvatarUrl(), "ADDED BY VAASU");
        bodyMessage.addSpecific(msg);
        TalkMessage talkMessage = chatRoomController.getTalkMessage(null, bodyMessage, TalkMessage.TYPE_SEND,
                null);
        talkMessage.setJid(BackgroundStuff.getCurrentJid());
        talkMessage.setIsGroup(false);
        chatRoomController.doSendMediaMessage(talkMessage);
        closePopUp();// close pop-up
    }

    private static class AttachmentListCell extends ListCell<Talk> {

        @Override
        public void updateItem(Talk item, boolean empty) {

            super.updateItem(item, empty);
            if (empty) {
                setGraphic(null);
                setText(null);
            } else {

                Circle avatar = new Circle(15);
				Image image = new Image(
						new ByteArrayInputStream(getAvatarImages(item.getJid())),50, 50, true, true);
				avatar.setFill(new ImagePattern(image));

                setGraphic(avatar);
                if (item instanceof CooTalk) {
                    CooTalk cooTalk = (CooTalk) item;
                    if (!((cooTalk.getNick()).equals(""))) {
                        setText(cooTalk.getNick());
                    } else {
                        setText(cooTalk.getRemarkNick());
                    }
                }
            }
        }
    }

    public static byte[] getAvatarImages(String jid) {
        byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
        return talkAvatar;
    }

    /**
     * remove pop-up UI from container view
     *
     * @author nisal
     **/
    private void closePopUp() {
        if (rootContainerView != null && rootContainerView.getChildren().size() > 1) {
            rootContainerView.getChildren().remove(rootContainerView.getChildren().size() - 1);
        }
    }

    @Override
    public void presenceChanged(Presence presence) {
        System.out.println("======ADDED VAASU PRESENCE CHANGED====" + presence.getStatus());
    }

    public static void addContact(String telephone) {
        try {

            final Talk camTalk = TalkManager.getTalkService().getTalk(telephone);

            if (telephone.length() == 0) {
                System.out.println("Empty text Field");
            } else if (telephone.equals(BackgroundStuff.getvCard().getTel())) {
                PopUpUtil.showAlert("Cannot add your own self as a friend!", Alert.AlertType.INFORMATION);
            } else {
                Talk ct = TalkManager.getLocalManager().getTalk(telephone);
                if (ct != null) {
                    PopUpUtil.showAlert("Contact already exists", Alert.AlertType.INFORMATION);
                } else {
                    System.out.println("Searching ...");
                    TaskEngine.getInstance().submit(() -> {
                        try {
                            if (null == camTalk) {
                                Platform.runLater(() -> PopUpUtil.showAlert("Contact not found", Alert.AlertType.INFORMATION));
                                return;
                            }
                            try {
                                byte[] avatar = HttpUtils.download(camTalk.getAvatarUrl());
                                camTalk.setAvatar(avatar);
                                updatefriendlist(camTalk);
                                Platform.runLater(() -> PopUpUtil.showAlert("Contact Successfully Added ", Alert.AlertType.INFORMATION));
                            } catch (Exception ex) {
                                Log.error(ex);
                            }
                        } catch (Exception e1) {
                            if (camTalk != null) {
                                Log.error(e1);
                            }
                            PopUpUtil.showAlert("Result not found", Alert.AlertType.INFORMATION);
                        }

                    });

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean updatefriendlist(Talk camTalk) {
        if (camTalk == null)
            return false;
        String useTel = TalkUtil.getUserTel(camTalk.getJid());
        Talk camTalk2 = TalkManager.getLocalManager().getTalk(useTel);
        if (camTalk2 != null) {
            return false;
        }
        final List<Talk> list = new ArrayList<Talk>();
        list.add(camTalk);
        BackgroundStuff.addToFriendList(camTalk);
        TalkManager.getVCardManager().addTalk(list);

        TaskEngine.getInstance().submit(() -> {
            try {
                TalkManager.getTalkService().addTalk(camTalk, null);
            } catch (Exception e) {
                Log.error(e);
                TalkManager.getVCardManager().deleteTalk(list);
            }
        });
        return true;
    }
}