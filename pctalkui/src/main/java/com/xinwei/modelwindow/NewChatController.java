package com.xinwei.modelwindow;

import com.xinwei.BackgroundStuff;
import com.xinwei.mainwindow.ChatRoom_Controller;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.model.Talk;
import com.xinwei.util.MsgUtil;
import com.xinwei.util.TalkUtil;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.*;

public class NewChatController implements Initializable {

	@FXML
	private HBox upperBox;

	@FXML
	private ListView<Talk> mbrList;

	@FXML
	private TextField textfield;

	@FXML
	private Button chatBtn;

	@FXML
	private Pane title;

	@FXML
	private Button close;

	private Stage stage;

	ObservableList<Talk> friends = FXCollections.observableArrayList();
	static Map<String, Talk> mapMamber = new HashMap<String, Talk>();
	List<Talk> addedMembers = new ArrayList<Talk>();

	ChatRoom_Controller chatRoomController;
	Pane chatRoomPane;
	Parent root1;

	// added by Nisal
	public StackPane rootContainerView;

	public void setChatRoomController(ChatRoom_Controller chatRoomController) {
		this.chatRoomController = chatRoomController;
	}

	public void setChatRoomPane(Pane chatRoomPane) {
		this.chatRoomPane = chatRoomPane;
	}

	public void setRoot1(Parent root1) {
		this.root1 = root1;
	}

	@FXML
	void handleButtonAction(ActionEvent event) {

		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {
//				stage = (Stage) close.getScene().getWindow();
//				stage.close();
				closePopUp();// close pop-up
			} else if (button.equals(chatBtn)) {

				openChat();

			}
		}

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		friends = BackgroundStuff.getInstance().getFriendList();

		mbrList.setItems(friends);
		mbrList.getStyleClass().add("mylist");
		mbrList.setCellFactory(new Callback<ListView<Talk>, ListCell<Talk>>() {
			@Override
			public ListCell<Talk> call(ListView<Talk> list) {
				return new AttachmentListCell();

			}
		});

		mbrList.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.getClickCount() == 2) {
					openChat();
				}
			}
		});

	}

	void openChat() {
		chatRoomPane.getChildren().setAll(root1);
		chatRoomController.area.clear();
		chatRoomController.setPaneVisible(true);

		String id = (String) (mbrList.getSelectionModel().getSelectedItem()).getJid();
		String name = "";
		if (!((mbrList.getSelectionModel().getSelectedItem().getNick()).equals(""))) {
			name = mbrList.getSelectionModel().getSelectedItem().getNick();
		} else {
			name = mbrList.getSelectionModel().getSelectedItem().getTel();
		}
		BackgroundStuff.setCurrentJid(id);
		MsgUtil.updateChatRoom(name, id, chatRoomController);
//		stage = (Stage) close.getScene().getWindow();
//		stage.close();
		closePopUp();// close pop-up
	}

	private static class AttachmentListCell extends ListCell<Talk> {

		@Override
		public void updateItem(Talk item, boolean empty) {
			HBox mainContainer = new HBox(10);
			Circle userImage = new Circle(18, 30, 18);
			Label userName = new Label();

			{
				setPrefHeight(53);
				setStyle(String.format("%s-fx-padding: 3 8.075 3 8.075;", getStyle()));
				mainContainer.getChildren().addAll(userImage, userName);
				mainContainer.setAlignment(Pos.CENTER_LEFT);
			}

			super.updateItem(item, empty);
			if (empty) {
				setGraphic(null);
				setText(null);
			} else {

				try {

					Image image = new Image(new ByteArrayInputStream(getAvatarImages(item.getJid())), 50, 50, true,
							true);
					userImage.setFill(new ImagePattern(image));

					userName.setText(TalkUtil.getValidMemberName(item));

					setGraphic(mainContainer);

				} catch (Exception e) {
					e.printStackTrace();
					setGraphic(null);
					setText(null);
				}

			}
		}
	}

	protected static byte[] getAvatarImages(String jid) {
		byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
		return talkAvatar;
	}

	/*
	 * public void updateChatRoom(String name, String id, ChatRoom_Controller
	 * chatRoom_Controller) { long maxId = 0, minId = 0; try {
	 * 
	 * chatRoom_Controller.clearChatRoom(); chatRoom_Controller.setChatMember(name);
	 * 
	 * final MessageManager historyManager = TalkManager.getMessageManager();
	 * 
	 * Date searchEndTime = TalkManager.getXmppService().getServerDate();
	 * 
	 * Date searchStartTime = MsgUtil.getStartTime(searchEndTime);
	 * 
	 * Map<String, Object> messageTimeInfo = historyManager.getMessageTimeInfo(id,
	 * null, searchStartTime, searchEndTime);
	 * 
	 * Object maxObj = messageTimeInfo.get("MAXTID"); Object minObj =
	 * messageTimeInfo.get("MINID");
	 * 
	 * if (maxObj != null && minObj != null) { maxId = ((Number)
	 * maxObj).longValue(); minId = ((Number) minObj).longValue(); }
	 * 
	 * VCard vCard = TalkManager.getVCardManager().getVCard();
	 * 
	 * List<? extends TalkMessage> talkMessageList =
	 * historyManager.getMessageList(id, null, maxId + 1, 100, false); if
	 * (talkMessageList != null && !(talkMessageList.isEmpty())) { for (TalkMessage
	 * transcriptMessage : talkMessageList) {
	 * 
	 * Platform.runLater(() -> {
	 * 
	 * if ((transcriptMessage.getTalkBody().getFrom()).equals(vCard.getTel())) {
	 * MsgUtil.addTalkMessage(transcriptMessage, false, chatRoom_Controller,
	 * BackgroundStuff.getCurrentJid(), vCard.getNick()); } else {
	 * 
	 * MsgUtil.addTalkMessage(transcriptMessage, true, chatRoom_Controller,
	 * BackgroundStuff.getCurrentJid(), ""); } }); } }
	 * 
	 * } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * }
	 */

	/*
	 * private Date getStartTime(Date endtime) { Integer key = 1000; Calendar cal =
	 * Calendar.getInstance(); cal.setTime(endtime); cal.add(Calendar.MONTH, 0 -
	 * key); return cal.getTime(); }
	 */

	/**
	 * remove pop-up UI from container view
	 * 
	 * @author nisal
	 **/
	private void closePopUp() {
		if (rootContainerView != null && rootContainerView.getChildren().size() > 1) {
			rootContainerView.getChildren().remove(rootContainerView.getChildren().size() - 1);
		}
	}

}