package com.xinwei.modelwindow;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.xinwei.BackgroundStuff;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;
import com.xinwei.util.GroupUtil;
import com.xinwei.util.ProgressUtil;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Callback;

public class AddGroup_Controller implements Initializable {

	@FXML
	private HBox upperBox;

	@FXML
	private ListView<String> mbrList;

	@FXML
	private TextField textfield;

	@FXML
	private Button create;

	@FXML
	private Pane title;

	@FXML
	private Button close;

	private Stage stage;

	ObservableList<String> data = FXCollections.observableArrayList();
	static Map<String, Talk> mapMamber = new HashMap<String, Talk>();
	List<Talk> addedMembers = new ArrayList<Talk>();
	boolean memberInitialized;

	// added by Nisal
	public StackPane rootContainerView;

	public void setMemberInitialized(boolean memberInitialized) {
		this.memberInitialized = memberInitialized;
	}

	@FXML
	void handleButtonAction(ActionEvent event) {

		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {
//				stage = (Stage) close.getScene().getWindow();
//				stage.close();
				closePopUp();// close pop-up
			} else if (button.equals(create)) {

				TaskEngine.getInstance().submit(() -> {
					Platform.runLater(() -> {
						showProgressView();// show progress view until current process finished
					});

					try {

						VCard vCard = TalkManager.getVCard();
						String creator = String.valueOf(vCard.getUid());

						Group xwGroup = new Group();
						xwGroup.setCreatorUid(creator);

						List<Member> memberList = new ArrayList<>();

						Member self = new Member();
						self.setUid(creator);
						self.setMemNick(vCard.getNick());
						self.setMemJid(vCard.getJid());
						self.setMemRole(Member.ROLE_ADMIN);

						memberList.add(self);
						for (Talk member : addedMembers) {
							Member m2 = new Member();
							m2.setUid(member.getUid());
							m2.setMemNick(TalkUtil.getDisplayName(member.getJid()));
							m2.setMemJid(member.getJid());
							m2.setMemRole(Member.ROLE_USER);
							memberList.add(m2);
						}

						// API call to create a new group and notify it to all parties
						String groupId = TalkManager.getTalkService().createGroup(xwGroup, memberList);
						xwGroup.setId(groupId);

						Platform.runLater(() -> {
							hideProgressView();// hide progress view after process finished
							Alert alert = new Alert(AlertType.INFORMATION, "Group Successfully Created", ButtonType.OK);
							alert.show();
						});

					} catch (Exception e) {
						Platform.runLater(() -> {
							hideProgressView();// hide progress view after process finished
							Alert alert = new Alert(AlertType.ERROR, "Error Occurred While Creating Group",
									ButtonType.OK);
							alert.show();
						});
						e.printStackTrace();
					}

					closePopUp();// close pop-up

				});

			}
		}

	}

	void addInitUser() {
		Circle groupAvatar = new Circle(15);
		Image image = new Image(new ByteArrayInputStream(getAvatarImages(BackgroundStuff.getCurrentJid())), 50, 50,
				true, true);
		groupAvatar.setFill(new ImagePattern(image));

		upperBox.getChildren().add(groupAvatar);

		addedMembers.add(mapMamber.get(BackgroundStuff.getCurrentJid()));
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		List<Talk> camTalks = new ArrayList<Talk>(TalkManager.getLocalManager().getTalkList());

		for (int i = 0; i < camTalks.size(); i++) {

			Talk camTalk = camTalks.get(i);

			try {
				mapMamber.put(camTalk.getJid(), camTalk);
				data.add(camTalk.getJid());

			} catch (Exception e) {
				Log.error(e);
			}
		}

		Platform.runLater(() -> {
			if (memberInitialized)
				addInitUser();
		});

		mbrList.setItems(data);
		mbrList.getStyleClass().add("mylist");
		mbrList.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
			@Override
			public ListCell<String> call(ListView<String> list) {
				return new AttachmentListCell();

			}
		});

		mbrList.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.getClickCount() == 2) {
					if (addedMembers.contains(mapMamber.get(mbrList.getSelectionModel().getSelectedItem())))
						return;

					Circle groupAvatar = new Circle(15);
					Image image = new Image(
							new ByteArrayInputStream(getAvatarImages(mbrList.getSelectionModel().getSelectedItem())),
							50, 50, true, true);
					groupAvatar.setFill(new ImagePattern(image));

					upperBox.getChildren().add(groupAvatar);

					addedMembers.add(mapMamber.get(mbrList.getSelectionModel().getSelectedItem()));
				}
			}
		});

	}

	private static class AttachmentListCell extends ListCell<String> {

		@Override
		public void updateItem(String item, boolean empty) {

			super.updateItem(item, empty);
			if (empty) {
				setGraphic(null);
				setText(null);
			} else {
				Circle avatar = new Circle(15);
				Image image = new Image(new ByteArrayInputStream(getAvatarImages(item)), 50, 50, true, true);
				avatar.setFill(new ImagePattern(image));

				setGraphic(avatar);
				Talk user = mapMamber.get(item);
				if (!((user.getNick()).equals(""))) {
					setText(user.getNick());
				} else {
					setText(user.getTel());
				}
			}
		}
	}

	protected static byte[] getAvatarImages(String jid) {
		byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
		return talkAvatar;
	}

	/**
	 * remove pop-up UI from container view
	 * 
	 * @author nisal
	 **/
	private void closePopUp() {
		Platform.runLater(() -> {
			if (rootContainerView != null && rootContainerView.getChildren().size() > 1) {
				rootContainerView.getChildren().remove(rootContainerView.getChildren().size() - 1);
			}
		});
	}

	/**
	 * create new view with progress view to show while server call in progress
	 * 
	 * @author nisal
	 **/
	private void showProgressView() {

		VBox pop_up = ProgressUtil.getProgressView();

		// show pop-up dialog in UI
		if (rootContainerView != null && pop_up != null
				&& rootContainerView.getChildren().get(rootContainerView.getChildren().size() - 1) != pop_up) {
			rootContainerView.setAlignment(Pos.CENTER);
			rootContainerView.getChildren().add(pop_up);
		}
	}

	/**
	 * hide progress view from UI
	 * 
	 * @author nisal
	 **/
	private void hideProgressView() {
		ProgressUtil.hideProgressView(rootContainerView);
	}

}