package com.xinwei.modelwindow;

import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.xinwei.BackgroundStuff;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.talk.http.service.TalkServiceHelper;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.uiutils.PopUpUtil;
import com.xinwei.util.ProgressUtil;
import com.xinwei.util.ScreenUtil;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

public class InviteFriendsController extends TalkServiceHelper implements Initializable {

	@FXML
	private StackPane rootStackPane;
	
    @FXML
    public VBox vbParamHolder,rootVBox;

	@FXML
	private Button btnClose;

	@FXML
	private Button btnAdd;

	@FXML
	private TextField txtPhoneNumber,txtName;

	@FXML
	private ListView<FriendDataHolder> lvInviteFriendList;

	@FXML
	private Button btnInvite;

	ObservableList<FriendDataHolder> olInviteFriendList = FXCollections.observableArrayList();
	FriendDataHolder tempFriend = new FriendDataHolder();
	// added by Nisal
	public StackPane rootContainerView;

	HttpResponseInfo responseInfo;
	// result code is always 0 or 1, initialized as 2 to avoid nullpointerexception
	// in if(resultCode == 0)
	Integer resultCode = 2;

	@FXML
	void handleAddAction(ActionEvent event) {
		if (txtPhoneNumber.getText().equals("")) {
			PopUpUtil.showAlert("Phone number field can not be empty ", AlertType.INFORMATION);
		//regex - \d is digit , + one or more times
		}else if(!(txtPhoneNumber.getText().trim().matches("\\d+"))){
			PopUpUtil.showAlert("Phone number field can not containt letters ", AlertType.INFORMATION);
		}else {
			tempFriend.setTelno(txtPhoneNumber.getText());
			tempFriend.setName(txtName.getText());
			olInviteFriendList.add(tempFriend);
		}
		txtPhoneNumber.clear();
		txtName.clear();
		txtName.setDisable(true);
	}
	
	@FXML
	void ntKeyReleased(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER) {
			if (txtPhoneNumber.getText().equals("")) {
				PopUpUtil.showAlert("Phone number field can not be empty ", AlertType.INFORMATION);
			}else if(!(txtPhoneNumber.getText().trim().matches("\\d+"))){
				PopUpUtil.showAlert("Phone number field can not containt letters ", AlertType.INFORMATION);
			}else {
				tempFriend.setTelno(txtPhoneNumber.getText());
				tempFriend.setName(txtName.getText());
				olInviteFriendList.add(tempFriend);
			}
			txtPhoneNumber.clear();;
			txtName.clear();
			txtName.setDisable(true);
    	}
	}

    @FXML
    void ptKeyReleased(KeyEvent event) {
    	if(txtPhoneNumber.getText().length() > 0) {
    		txtName.setDisable(false);
    	}
    }
	
	@FXML
	void handleCloseAction(ActionEvent event) {
		closePopUp();
	}

	@FXML
	void handleInviteAction(ActionEvent event) {
		if(olInviteFriendList.size() < 1) {
			PopUpUtil.showAlert("Please add friends to the list", AlertType.INFORMATION);
		}else {
			Task<Void> serverReq = new Task<Void>() {

				@Override
				protected Void call() throws Exception {
					processInviteFriendsReq();
					return null;
				}

			};
			serverReq.setOnRunning(e -> {
				showProgressView();// show progress view until current process finished
			});
			serverReq.setOnSucceeded(e -> {
				hideProgressView();// hide progress view after process finished
				olInviteFriendList.clear();
				if(responseInfo == null) {
					PopUpUtil.showAlert("Internal Server Error", AlertType.INFORMATION);
				}else {
					resultCode = getResult(responseInfo);
					if(resultCode == 0) 
						PopUpUtil.showAlert("Request Successful", AlertType.INFORMATION); 
					else 
						PopUpUtil.showAlert("Internal Server Error", AlertType.INFORMATION) ;			
				}
			});

			Thread serverReqThread = new Thread(serverReq);
			serverReqThread.setDaemon(true);
			serverReqThread.start();
		}
	}


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		organizeUIComponents();
	}

	private class AttachmentListCell extends ListCell<FriendDataHolder> {
		FriendUIComponentsHolder holder = new FriendUIComponentsHolder();
		{
			initInviteFriendListCell(holder, this);
		}

		@Override
		public void updateItem(FriendDataHolder item, boolean empty) {
			super.updateItem(item, empty);
			if (empty) {
				setItem(null);
				setGraphic(null);
			} else {
				updateInviteFriendListCellWithData(holder, this, item, empty);
			}
		}
	}

	private void updateInviteFriendListCellWithData(FriendUIComponentsHolder holder, ListCell<FriendDataHolder> cell, FriendDataHolder item,
			boolean empty) {
		try {
			if (empty) {
				cell.setGraphic(null);
			} else {
				holder.lblPhoneNumberVal.setText(item.getTelno());
				holder.lblNameVal.setText(item.getName()+",");
				cell.setGraphic(holder.mainBaseContainer);
			}
		} catch (Exception e) {
			e.printStackTrace();
			cell.setGraphic(null);
		}
	}

	private void initInviteFriendListCell(FriendUIComponentsHolder holder, ListCell<FriendDataHolder> cell) {
		holder.mainBaseContainer = new HBox(10);
		
		holder.btnRemove = new Button();
		holder.btnRemove.setAlignment(Pos.CENTER_RIGHT);
		holder.btnRemove.setPadding(new Insets(0, 0, 2, 6));
		Image img = new Image("/images/icons/minus-button.png", 15, 15, false, false);
		holder.btnRemove.setStyle("-fx-background-color: transparent;");
		holder.btnRemove.setGraphic(new ImageView(img));
		holder.btnRemove.setOnAction(e -> {
			if (cell.getIndex() >= 0 && cell.getIndex() < olInviteFriendList.size()) {
				olInviteFriendList.remove(cell.getIndex());
				lvInviteFriendList.refresh();
			}
		});
		
		holder.lblPhoneNumber = new Label();
		holder.lblPhoneNumber.setStyle("-fx-font-weight: bold");
		holder.lblPhoneNumber.setText("Phone :");
		holder.lblPhoneNumberVal = new Label();
		holder.lblPhoneNumberVal.setAlignment(Pos.CENTER);
		

		holder.lblName= new Label();
		holder.lblName.setStyle("-fx-font-weight: bold");
		holder.lblName.setText("Name :");
		holder.lblNameVal= new Label();
		holder.lblNameVal.setAlignment(Pos.CENTER);
		
		holder.mainBaseContainer.setPadding(new Insets(0, 0, 0, 8));
		holder.mainBaseContainer.getChildren().addAll(holder.lblName, holder.lblNameVal,holder.lblPhoneNumber,holder.lblPhoneNumberVal ,holder.btnRemove);
		holder.mainBaseContainer.setAlignment(Pos.CENTER_LEFT);
	}

	/**
	 * This class will hold the references to all UI components used in cell
	 **/
	static class FriendUIComponentsHolder {
		Button btnRemove;
		Label lblPhoneNumberVal;
		Label lblNameVal;
		Label lblPhoneNumber;
		Label lblName;
		HBox mainBaseContainer;
	}
	
	/**
	 * This class will hold the reference to all data used in the cell
	 */
	class FriendDataHolder {
		String telno;
		String name;
		
		public String getTelno() {
			return telno;
		}
		public void setTelno(String telno) {
			this.telno = telno;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}

	private void processInviteFriendsReq() {
		try {
			List<Map<String, String>> listContainer = new ArrayList<>();
			LinkedHashMap<String, String> itemContainer = new LinkedHashMap<>();
			olInviteFriendList.forEach(item -> {
				itemContainer.put("telno",item.getName());
				itemContainer.put("name",item.getName());
				listContainer.add(itemContainer);
			});
			responseInfo = TalkManager.getHttpInviteFriendService().inviteFriends(BackgroundStuff.getvCard().getTel(),
					listContainer);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("-> Fx Engine : Invalid response");
		}
	}
	
	public void organizeUIComponents() {
		double window_w = ScreenUtil.getMainScreenDimentions().get(ScreenUtil.MAIN_SCREEN_DIMENTIONS_WIDTH);
		double window_h = ScreenUtil.getMainScreenDimentions().get(ScreenUtil.MAIN_SCREEN_DIMENTIONS_HEIGHT);
		
		rootVBox.setPrefSize(window_w * 0.35, window_h * 0.7);
		rootVBox.setMaxSize(window_w * 0.35, window_h * 0.7);
		
		lvInviteFriendList.setItems(olInviteFriendList);
		lvInviteFriendList.getStyleClass().add("mylist");
		lvInviteFriendList.setCellFactory(new Callback<ListView<FriendDataHolder>, ListCell<FriendDataHolder>>() {
			@Override
			public ListCell<FriendDataHolder> call(ListView<FriendDataHolder> param) {
				return new AttachmentListCell();
			}
		});
		txtName.setDisable(true);
	}

	/**
	 * remove pop-up UI from container view
	 * 
	 * @author nisal
	 **/
	private void closePopUp() {
		if (rootContainerView != null && rootContainerView.getChildren().size() > 1) {
			rootContainerView.getChildren().remove(rootContainerView.getChildren().size() - 1);
		}
	}

	private void showProgressView() {

		VBox pop_up = ProgressUtil.getProgressView();

		// show pop-up dialog in UI
		if (rootContainerView != null && pop_up != null
				&& rootContainerView.getChildren().get(rootContainerView.getChildren().size() - 1) != pop_up) {
			rootContainerView.setAlignment(Pos.CENTER);
			rootContainerView.getChildren().add(pop_up);
		}
	}

	/**
	 * hide progress view from UI
	 * 
	 * @author nisal
	 **/
	private void hideProgressView() {
		ProgressUtil.hideProgressView(rootContainerView);
	}

}
