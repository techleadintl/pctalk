package com.xinwei.modelwindow;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXComboBox;
import com.xinwei.BackgroundStuff;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.talk.dto.FindFriendsDTO;
import com.xinwei.talk.http.service.TalkServiceHelper;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.uiutils.ComboBox;
import com.xinwei.util.ProgressUtil;
import com.xinwei.util.ScreenUtil;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class FindFriendsController extends TalkServiceHelper implements Initializable {

    @FXML
    public VBox rootVbox,vbParamHolder;

    @FXML
    private StackPane rootStackPane;

    @FXML
    private Button btnClose;

    @FXML
    private TextField textfield;

    @FXML
    private TextField txtNickName;

    @FXML
    private TextField txtAge;

    @FXML
    private TextField txtEmail;

    @FXML
    private JFXComboBox<?> cbGender;

    @FXML
    private JFXComboBox<String> cbCountry;

    @FXML
    private Button btnSerach;

    @FXML
    private ListView<?> lvResultHolder;
    
    
	// added by Nisal
	public StackPane rootContainerView;
	ObservableList<String> olResultHolder = FXCollections.observableArrayList();
	HttpResponseInfo responseInfo;
	// result code is always 0 or 1, initialized as 2 to avoid nullpointerexception
	// in if(resultCode == 0)
	Integer resultCode = 2;

    @FXML
    void getCountryList(MouseEvent event) {
    	ComboBox comboBoxUtil = new ComboBox();
    	comboBoxUtil.getCountryListComboBox(cbCountry);
    }

    @FXML
    void handleCloseAction(ActionEvent event) {
    	closePopUp();
    }

    @FXML
    void handleSearchAction(ActionEvent event) {
    	Task<Void> serverReq = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				FindFriendsDTO ffdto = new FindFriendsDTO("0094773584525", null, null, null, null, null);
				processFindFriendsRequest(ffdto);
				return null;
			}
    	};
    	serverReq.setOnRunning(e -> {
    		showProgressView();
    	});
    	serverReq.setOnSucceeded(e -> {
    		hideProgressView();
    	});
    	
    	Thread serverReqThead = new Thread(serverReq);
    	serverReqThead.setDaemon(true);
    	serverReqThead.start();
    }

    @FXML
    void keyReleasedSelectCountry(KeyEvent event) {

    }

    @FXML
    void keyReleasedSelectGender(KeyEvent event) {

    }

    @FXML
    void selectCountry(ActionEvent event) {

    }

    @FXML
    void selectGender(ActionEvent event) {

    }
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		organizeUIComponents();
	}
	
	public void processFindFriendsRequest(FindFriendsDTO ffDTO) {
		try {
			responseInfo = TalkManager.getHttpfindFriendsService().querryFindFriendParams(ffDTO);
			resultCode = getResult(responseInfo);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("-> Fx Engine : Invalid response");
		}
	}
	
	public void organizeUIComponents() {
		double window_w = ScreenUtil.getMainScreenDimentions().get(ScreenUtil.MAIN_SCREEN_DIMENTIONS_WIDTH) * 0.7;
		
		vbParamHolder.setPrefWidth(window_w * 0.5);
		vbParamHolder.setMaxWidth(window_w * 0.5);
	}
	
	/**
	 * remove pop-up UI from container view
	 * 
	 * @author nisal
	 **/
	private void closePopUp() {
		if (rootContainerView != null && rootContainerView.getChildren().size() > 1) {
			rootContainerView.getChildren().remove(rootContainerView.getChildren().size() - 1);
		}
	}
	
	private void showProgressView() {

		VBox pop_up = ProgressUtil.getProgressView();

		// show pop-up dialog in UI
		if (rootContainerView != null && pop_up != null
				&& rootContainerView.getChildren().get(rootContainerView.getChildren().size() - 1) != pop_up) {
			rootContainerView.setAlignment(Pos.CENTER);
			rootContainerView.getChildren().add(pop_up);
		}
	}
	
	/**
	 * hide progress view from UI
	 * 
	 * @author nisal
	 **/
	private void hideProgressView() {
		ProgressUtil.hideProgressView(rootContainerView);
	}
}
