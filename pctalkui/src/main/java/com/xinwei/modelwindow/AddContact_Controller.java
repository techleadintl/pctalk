package com.xinwei.modelwindow;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.xinwei.BackgroundStuff;
import com.xinwei.common.lang.HttpUtils;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Talk;
import com.xinwei.uiutils.PopUpUtil;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Callback;

public class AddContact_Controller implements Initializable {

	@FXML
	private Button close;

	@FXML
	private Button add;

	@FXML
	private ListView<String> memberList;

	@FXML
	private TextField textField;

	@FXML
	private Pane title;
	private Stage stage;
	private Talk camTalk = null;

	// added by Nisal
	public StackPane rootContainerView;

	ObservableList<String> data = FXCollections.observableArrayList();

	@FXML
	void handleButtonAction(ActionEvent event) {

		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {
//				stage = (Stage) close.getScene().getWindow();
//				stage.close();
				closePopUp();// close pop-up
			} else if (button.equals(add)) {
				boolean result = updatefriendlist();
				if (result == true) {
					PopUpUtil.showAlert("Contact Added Successfully", AlertType.INFORMATION);
				} else {
					PopUpUtil.showAlert("Error Occurred While Adding Contact", AlertType.ERROR);
				}
//				stage = (Stage) close.getScene().getWindow();
//				stage.close();
				closePopUp();// close pop-up
				add.setDisable(true);
			}
		}

	}

	private static class AttachmentListCell extends ListCell<String> {

		@Override
		public void updateItem(String item, boolean empty) {

			super.updateItem(item, empty);
			if (empty) {
				setGraphic(null);
				setText(null);
			} else {
				Circle avatar = new Circle(15);
				Image image = new Image(getClass().getClassLoader().getResource("images/user-default.png").toString(),
						50, 50, true, true);
				avatar.setFill(new ImagePattern(image));
				setGraphic(avatar);
				setText(item);
			}
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		add.setDisable(true);
		memberList.setItems(data);
		memberList.getStyleClass().add("mylist");
		memberList.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
			@Override
			public ListCell<String> call(ListView<String> list) {
				return new AttachmentListCell();
			}
		});

		textField.setOnAction(e -> {
			final String tel = textField.getText().trim();
			System.out.println(tel);
			getserverRes(tel);
		});

		memberList.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {

			}
		});

		textField.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					textField.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});

	}

	private void getserverRes(String telephone) {
		if (telephone.length() == 0) {
			System.out.println("> Empty text Field");
		} else if (telephone.equals(BackgroundStuff.getvCard().getTel())) {
			PopUpUtil.showAlert("Cannot add your own self as a friend!", AlertType.INFORMATION);
		} else {
			Talk ct = TalkManager.getLocalManager().getTalk(telephone);
			if (ct != null) {
				PopUpUtil.showAlert("Contact already exists", AlertType.INFORMATION);
			} else {
				System.out.println("Searching ...");
				TaskEngine.getInstance().submit(new Runnable() {
					@Override
					public void run() {
						try {
							camTalk = TalkManager.getTalkService().getTalk(telephone);
							if (null == camTalk) {
								Platform.runLater(() -> {
									PopUpUtil.showAlert("Contact not found", AlertType.INFORMATION);
								});
								return;
							}

							Platform.runLater(() -> {
								add.setDisable(false);
							});
							try {
								byte[] avatar = HttpUtils.download(camTalk.getAvatarUrl());
								camTalk.setAvatar(avatar);
							} catch (Exception ex) {
								Log.error(ex);
							}
							Platform.runLater(() -> {
								memberList.getItems().clear();
								memberList.getItems().add(camTalk.getTel());
							});
						} catch (Exception e1) {
							if (camTalk != null) {
								Log.error(e1);
							}
							PopUpUtil.showAlert("Result not found", AlertType.INFORMATION);
						}
					}
				});

			}
		}

	}

	private boolean updatefriendlist() {
		if (camTalk == null)
			return false;
		String useTel = TalkUtil.getUserTel(camTalk.getJid());
		Talk camTalk2 = TalkManager.getLocalManager().getTalk(useTel);
		if (camTalk2 != null) {
			return false;
		}

		final List<Talk> list = new ArrayList<Talk>();
		list.add(camTalk);
		BackgroundStuff.addToFriendList(camTalk);

		TalkManager.getVCardManager().addTalk(list);

		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				try {
					TalkManager.getTalkService().addTalk(camTalk, null);
				} catch (Exception e) {
					Log.error(e);
					TalkManager.getVCardManager().deleteTalk(list);
				}
			}
		});
		return true;
	}

	/**
	 * remove pop-up UI from container view
	 * 
	 * @author nisal
	 **/
	private void closePopUp() {
		if (rootContainerView != null && rootContainerView.getChildren().size() > 1) {
			rootContainerView.getChildren().remove(rootContainerView.getChildren().size() - 1);
		}
	}

}
