package com.xinwei.tabwindow;

import com.xinwei.BackgroundStuff;
import com.xinwei.mainwindow.ChatRoom_Controller;
import com.xinwei.mainwindow.CommonRoomController;
import com.xinwei.mainwindow.Main_Controller;
import com.xinwei.modelwindow.FindFriendsController;
import com.xinwei.modelwindow.AddContact_Controller;
import com.xinwei.modelwindow.AddGroup_Controller;
import com.xinwei.modelwindow.InviteFriendsController;
import com.xinwei.modelwindow.NewChatController;
import com.xinwei.modelwindow.SuggestFriendController;
import com.xinwei.profile.MyProfileController;
import com.xinwei.screenshot.ScreenCapture;
import com.xinwei.search.SearchPanel;
import com.xinwei.talk.common.HttpException;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.dao.TalkDao;
import com.xinwei.talk.listener.AvatarListener;
import com.xinwei.talk.manager.*;
import com.xinwei.talk.model.CooTalk;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.avatar.Avatar;
import com.xinwei.talk.model.conf.UserConfig;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.uiutils.PopUpUtil;
import com.xinwei.util.MsgUtil;
import com.xinwei.util.ScreenUtil;
import com.xinwei.util.TalkUtil;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import javafx.stage.Stage;
import org.controlsfx.control.PopOver;

import java.io.IOException;
import java.lang.Thread.State;
import java.net.URL;
import java.util.*;

public class Chat_Controller implements Initializable, AvatarListener {

	private static final String character2 = null;
	public BitSet keyboardBitSet = new BitSet();

	@FXML
	private ListView<Talk> lst;

	@FXML
	public Pane chatRoomPane;

	@FXML
	private Button add;

	@FXML
	private VBox search;

	@FXML
	private ImageView search_img;

	@FXML
	public TextField searchFriend;

	List<? extends Talk> talkList;

	Main_Controller mainControllerHandle;

	List<? extends TalkMessage> talkMessageList;

	// private String currentjid = "";

	protected LocalManager localManager = TalkManager.getLocalManager();

	ObservableList<Talk> recentFriends = FXCollections.observableArrayList();
	ObservableList<Talk> friendList = FXCollections.observableArrayList();

	// added by Nisal to keep the list of users, that are currently loaded to the UI
	ObservableList<Talk> currentUserList = FXCollections.observableArrayList();

	ProgressIndicator p1 = new ProgressIndicator();

	final ObservableMap<Talk, Integer> messageCount = FXCollections.observableHashMap();

	ObservableList<String> deleteSuggestContent = FXCollections.observableArrayList("Delete Friend", "Suggest Friend");

	ObservableList<String> menuContent = FXCollections.observableArrayList("Find Friends", "New User", "New Group",
			"New Chat", "Invite Friends");

	public ChatRoom_Controller chatRoom_Controller;

	NewChatController newChatController;

	SuggestFriendController suggestFriendController;

	CommonRoomController commonRoomController;

	MyProfileController myProfileController;

	boolean hasMsg;

	TalkDao dao = TalkManager.getTalkDao();
	Parent root1;

	// added by Nisal to keep last selected item details (this will be used to clear
	// the selected text styles)
	private Talk lastSelectedUserObj;
	private Thread chatHistoryManageThread;
	private PopOver plusBtnPopOver;
	// this will keep the base UI component container from Main_Controller
	public StackPane base_main_ui_container;

	// added by Vaasu to fix Recent chat user duplicate issue
	private int userIndex = 0;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		lst.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
		lst.addEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);

		// added by Nisal to register user Avatar changed listener
		ListenerManager.addAvatarListener(this);

		// reset last selected user before start
		lastSelectedUserObj = null;

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/ChatRoom.fxml"));
			root1 = loader.load();
			chatRoom_Controller = loader.getController();
			chatRoom_Controller.setPaneVisible(false);
			chatRoom_Controller.rootContainerView = base_main_ui_container;
			System.out.println("baseview" + base_main_ui_container);

			FXMLLoader loaderCom = new FXMLLoader();
			loaderCom.setLocation(getClass().getClassLoader().getResource("fxml/CommonChatRoom.fxml"));
			Parent commmon = loaderCom.load();
			commonRoomController = loaderCom.getController();
			chatRoomPane.getChildren().setAll(commmon);
			commonRoomController.setChatRoom_Controller(chatRoom_Controller);
			commonRoomController.setRoot1(root1);
			commonRoomController.setChatRoomPane(chatRoomPane);
			// chatRoomPane.getChildren().setAll(root1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// initAvatar();
		recentFriends = BackgroundStuff.getInstance().getRcntFriendList();
		friendList = BackgroundStuff.getInstance().getFriendList();

		// modified by Nisal
		// in here I removed the observable list reference form list view and added a
		// sorted list(also a observable list) reference. because of this no matter
		// whatever change we made to the list view, list will be automatically sorted.
		// but we directly can't update a sorted list. then I kept the observable list
		// references too. here we have to change the observable list only. then
		// sort enabled list will automatically updated with sorting.
		if (BackgroundStuff.getActiveNode().equalsIgnoreCase("C")) {
			currentUserList = recentFriends;
			lst.setItems(BackgroundStuff.getInstance().getSortedRcntFriendList());
		} else {
			currentUserList = friendList;
			lst.setItems(BackgroundStuff.getInstance().getSortedFriendList());
		}

		lst.setEditable(true);

		lst.setCellFactory(lv -> new ListCell<Talk>() {
			UserListComponentsHolder holder = new UserListComponentsHolder();

			{
				initializeUserListNodes(holder, this);// create cell UI components
			}

			@Override
			public void updateItem(Talk item, boolean empty) {
				super.updateItem(item, empty);

				if (empty || item == null) {
					setStyle("-fx-background-color: white;");// remove selection color for this cell
					if (chatRoom_Controller != null)// clear chat history pane
						chatRoom_Controller.chatPane.getItems().clear();
					setGraphic(null);
					setItem(null);
				} else {
					Platform.runLater(() -> {
						// update cell items with current cell data
						updateUserListCellNodesWithData(holder, this, item, empty);
					});
				}
			}

		});

		lst.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				// stop current playing audios, if any.
				if (chatRoom_Controller.recorder.isRecording) {
					chatRoom_Controller.recorder.doStop(chatRoom_Controller.voiceRecButton);
				}

				// clear user added chat message in chat message text field
				chatRoom_Controller.area.clear();
				if (lst.getItems() == null || lst.getItems().isEmpty() || lst.getSelectionModel() == null
						|| lst.getSelectionModel().getSelectedItem() == null)
					return;

				chatRoomPane.getChildren().setAll(root1);

				String id = lst.getSelectionModel().getSelectedItem().getJid();

				// set valid name for selected user
				String name = TalkUtil.getValidMemberName(lst.getSelectionModel().getSelectedItem());

				if (BackgroundStuff.getCurrentJid() != null && id != null && BackgroundStuff.getCurrentJid().equals(id))
					return;

				// added by Nisal to stop current ongoing threads
				stopCurrentRunningThread();

				BackgroundStuff.setCurrentJid(id);// currentjid = id;
				BackgroundStuff.setName(name);
				chatRoom_Controller.setPaneVisible(true);

				userIndex = 0;

				// select correct user list before update the status of the chat list users
				currentUserList = friendList;
				if (BackgroundStuff.getActiveNode().equals("C"))
					currentUserList = recentFriends;

				// update current showing chat user list status to show selection styles and to
				// load the chat history
				currentUserList.forEach((friend) -> {
					friend.setActiveRoom(false);

					if (BackgroundStuff.getCurrentJid().equals(friend.getJid())) {
						// set last selected user details
						// TODO: try to work this out for reset styles
						lastSelectedUserObj = friend;

						// added by Vasu for reset new messages count
						friend.setNewMessageCount(0);

						friend.setHasMsg(false);
						friend.setActiveRoom(true);
					}
					if (userIndex >= 0 && lst.getItems().size() > userIndex)
						currentUserList.set(userIndex, friend);// changed by Nisal from List view to currentUserList, to
																// avoid from unsupported operation Excep: (because of
																// sorting list)

					userIndex++;
				});

				// added by Nisal to update the chat history data in a separate thread
				updateChatHistory(id, name);
			}
		});

		plusBtnPopOver = createPopOver(menuContent);
		add.setOnMousePressed(new EventHandler<MouseEvent>() {
			// ContextMenu contextMenu = createPopup();
			@Override
			public void handle(MouseEvent event) {
				System.out.println("===MOUSE PRESSED FOR POPUP====");
				plusBtnPopOver.show(add);
			}
		});

		// FOR SEARCH FEATURE ADDED BY VAASU

		String DEFAULT_TEXT = Res.getMessage("input.text.search");
		final ListView<String> console = new ListView<String>(FXCollections.<String>observableArrayList());
		// listen on the console items and remove old ones when we get over 20 items in
		// the list
		console.getItems().addListener(new ListChangeListener<String>() {
			@Override
			public void onChanged(Change<? extends String> change) {
				while (change.next()) {
					if (change.getList().size() > 20)
						change.getList().remove(0);
				}
			}
		});

		searchFriend.setOnKeyPressed(ke -> {
			console.getItems().add("Key Pressed: " + ke.getText());
			System.out.println("ENTERING SEARCH BOX");
			if (searchFriend.getText().equals(DEFAULT_TEXT)) {
				searchFriend.setText("");
			}
			if (ke.getCode() == KeyCode.ENTER && searchFriend.isFocused()) {
				System.out.println("==FOCUS SEARCH BOX===");
			}
		});

		searchFriend.setOnKeyReleased(ke -> {
			System.out.println("===KEY REALEASE HERE====");
//			if (searchFriend.getText()!=null){

			if (BackgroundStuff.getActiveNode().equalsIgnoreCase("C")) {
				new SearchPanel(searchFriend.getText().isEmpty(), searchFriend, lst, recentFriends, null, null)
						.doSearch();
			} else if (BackgroundStuff.getActiveNode().equalsIgnoreCase("M")) {
				new SearchPanel(searchFriend.getText().isEmpty(), searchFriend, lst, friendList, null, null).doSearch();
			}
		});

		/**
		 * Added By Vaasu Delete Friend return remove from Talk ListView
		 */
		ContextMenu contextMenu = new ContextMenu();
		MenuItem deleteFriend = new MenuItem("Delete Friend");
		MenuItem suggestFriend = new MenuItem("Suggest Friend");
		if (!(BackgroundStuff.getActiveNode().equals("C"))) {
			deleteFriend.setOnAction(event -> {
				Talk selectedTalk = lst.getSelectionModel().getSelectedItem();
				System.out.println("Delete " + lst.getSelectionModel().getSelectedItem().getJid());
				System.out.println("------Selected Talk For Removing-------");
				try {
					int deleteTalk = TalkManager.getTalkService().deleteTalk(selectedTalk, null);
				} catch (HttpException e) {
					e.printStackTrace();
				} catch (Exception e) {
					PopUpUtil.showAlert("Could Not Delete Friend", Alert.AlertType.ERROR);
					e.printStackTrace();
					return;
				}
				lst.getItems().remove(selectedTalk);
				List<Talk> talkList = new ArrayList<Talk>();

				talkList.add(selectedTalk);
				TalkManager.getVCardManager().deleteTalk(talkList);
				Platform.runLater(
						() -> PopUpUtil.showAlert("Contact Successfully Deleted ", Alert.AlertType.INFORMATION));
			});

//		contextMenu.getItems().addAll(deleteFriend);
//		lst.setOnContextMenuRequested(event -> contextMenu.show(lst, event.getScreenX(), event.getScreenY()));
			/**
			 * Added By Vaasu Suggest Friend return open ListView to Select
			 */
			suggestFriend.setOnAction(event -> {
				Talk selectedTalk = lst.getSelectionModel().getSelectedItem();
				System.out.println("------Selected Talk For Suggest Friend-------"
						+ lst.getSelectionModel().getSelectedItem().getJid());
				try {
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(getClass().getClassLoader().getResource("fxml/SuggestFriend.fxml"));
					Parent root = loader.load();
					suggestFriendController = loader.getController();
					suggestFriendController.setChatRoomController(chatRoom_Controller);
					suggestFriendController.rootContainerView = base_main_ui_container;
					suggestFriendController.setSelectedFriend(selectedTalk);
					VBox outer = new VBox();
					outer.setAlignment(Pos.CENTER);
					outer.getChildren().add(root);
					outer.setBackground(new Background(
							new BackgroundFill(Color.rgb(0, 0, 0, 0.3), new CornerRadii(0), new Insets(0))));

					// show pop-up dialog in UI
					if (base_main_ui_container != null && base_main_ui_container.getChildren()
							.get(base_main_ui_container.getChildren().size() - 1) != outer) {
						base_main_ui_container.setAlignment(Pos.CENTER);
						base_main_ui_container.getChildren().add(outer);
					}
//                contextMenu.hide();
				} catch (IOException e) {
					PopUpUtil.showAlert("Could Not Delete Friend", Alert.AlertType.ERROR);
					e.printStackTrace();
				}
			});
			contextMenu.getItems().addAll(deleteFriend, suggestFriend);
			/**
			 * Added by Vaasu to reuse the popup component
			 */
//        lstPopOver = createPopOver(deleteSuggestContent);
//        lst.setOnMousePressed(event -> {
//            System.out.println("===MOUSE PRESSED FOR POPUP====");
//            lstPopOver.show(lst, event.getScreenX(), event.getScreenY());
//        });
			lst.setOnContextMenuRequested(event -> contextMenu.show(lst, event.getScreenX(), event.getScreenY()));
		}

		/*
		 * chatRoom_Controller.chatPane.setOnScroll( event -> { //
		 * chatRoom_Controller.chatPane.setOnScrollFinished( event -> {
		 * System.out.println("====SCROLL EVENT IS TRIGEERING====="); Node nodeTemp =
		 * chatRoom_Controller.chatPane.lookup(".scroll-bar"); if(nodeTemp!=null){
		 * ScrollBar listViewScrollBar =
		 * getListViewScrollBar(chatRoom_Controller.chatPane);
		 * listViewScrollBar.valueProperty().addListener((observable, oldValue,
		 * newValue) -> { double position = newValue.doubleValue(); ScrollBar scrollBar
		 * = getListViewScrollBar(chatRoom_Controller.chatPane); // if (position ==
		 * scrollBar.getMax()) { if (position == scrollBar.getMin()) { int selectedItems
		 * = chatRoom_Controller.chatPane.getItems().size();
		 * System.out.println("====Selected Items :"+selectedItems); String name = "";
		 * if (!((lst.getSelectionModel().getSelectedItem().getNick()).equals(""))) {
		 * name = lst.getSelectionModel().getSelectedItem().getNick(); } else { name =
		 * lst.getSelectionModel().getSelectedItem().getTel(); } if
		 * (newValue.doubleValue() >= scrollBar.getMin()) {
		 * chatRoom_Controller.chatPane.getItems().clear(); if(selectedItems>=100){
		 * updateChatRoom(name, BackgroundStuff.getCurrentJid(), chatRoom_Controller,
		 * selectedItems+25); }
		 * 
		 * } } }); } });
		 */

	}

	/*************************** Listeners start ****************************/

	/**
	 * fired when user Avatar changed
	 **/
	@Override
	public void update(List<Avatar> avatars) {
//		if ((avatars == null || avatars.isEmpty()) && BackgroundStuff.getActiveNode() != null
//				&& (!BackgroundStuff.getActiveNode().equals("C") || !BackgroundStuff.getActiveNode().equals("M")))
//			return;
		
//		for(Avatar avatar: avatars) {
//			
//			if(BackgroundStuff.getInstance().getFriendList())
//			
//		}

//		lst.refresh();
	}

	/*************************** Listeners end ****************************/

	private ScrollBar getListViewScrollBar(ListView<Object> listView) {
		ScrollBar scrollbar = null;
		Node nodeTemp = listView.lookup(".scroll-bar");
		if (nodeTemp != null) {
			if (nodeTemp instanceof ScrollBar) {
//                    System.out.println("===Selected Node=="+ ((ScrollBar) nodeTemp).getValue());
			}
			for (Node node : listView.lookupAll(".scroll-bar")) {
				if (node instanceof ScrollBar) {
					ScrollBar bar = (ScrollBar) node;
					if (bar.getOrientation().equals(Orientation.VERTICAL)) {
						scrollbar = bar;
					}
				}
			}
		}
		return scrollbar;
	}

	String charact = getCharactor(character2);
	public EventHandler<KeyEvent> keyPressedEventHandler = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {

			System.out.println("Shortcut key pressed   >>>>    " + event.toString());

			keyboardBitSet.set(event.getCode().ordinal(), true);

			event.consume();

			updateKeyboardStatus();

			String code = "CONTROL " + event.getCode().toString();

			if (code.equals(character)) {

				try {

					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(getClass().getResource("/fxml/ShortcutKeys.fxml"));
					Parent root = loader.load();
					ShortcutKeysController shortcutKeysController = loader.getController();

					shortcutKeysController.keyType(getCharactor(character));

				} catch (IOException e) {

					e.printStackTrace();
				}

			}
		}
	};

	public EventHandler<KeyEvent> keyReleasedEventHandler = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {

			System.out.println("Shortcut key Released   >>>>    " + event.toString());

			keyboardBitSet.set(event.getCode().ordinal(), false);

			event.consume();

			updateKeyboardStatus();

		}

	};

	public String character = "";

	public void updateKeyboardStatus() {
		character = "";

		StringBuilder sb = new StringBuilder();
		int count = 0;
		for (KeyCode keyCode : KeyCode.values()) {

			if (keyboardBitSet.get(keyCode.ordinal())) {
				if (count > 0) {
					sb.append(" ");
				}
				count++;
				sb.append(keyCode.toString());

				character = sb.toString();

				UserConfig userConfig = TalkManager.getUserConfig();
				String scr_hotkey = userConfig.getShortKeyValue("Screenshot");
				String main_win_hotkey = userConfig.getShortKeyValue("MainWindow");

				if (scr_hotkey.equals(character)) {

					Platform.runLater(new Runnable() {

						@Override
						public void run() {

							new ScreenCapture().start(new Stage());
							keyboardBitSet.clear();
						}
					});
				}
				if (main_win_hotkey.equals(character)) {
					Platform.runLater(new Runnable() {

						@Override
						public void run() {

							Stage stage = (Stage) lst.getScene().getWindow();
							stage.setIconified(true);
							keyboardBitSet.clear();
							keyboardBitSet.clear();
						}
					});
				}

				if (count == 2) {

					getCharactor(character);

				}
			}

		}
	}

	public String getCharactor(String character2) {
		return character2;

	}

	// Fixme : Method name
	public void updateChatRoom(String name, String id, ChatRoom_Controller chatRoom_Controller, int num) {

		long maxId = 0, minId = 0;
		try {

			Platform.runLater(() -> {
				chatRoom_Controller.clearChatRoom();
				chatRoom_Controller.chatPane.getItems().clear();
				chatRoom_Controller.setChatMember(name);// set current selected user's name shows in header

				chatRoom_Controller.drawer.close();
			});

			final MessageManager historyManager = TalkManager.getMessageManager();

			Date searchEndTime = TalkManager.getXmppService().getServerDate();

			Date searchStartTime = MsgUtil.getStartTime(searchEndTime);

			// get available message list details (msg count, max id, min id, etc...)
			// probably this was implemented for pagination
			Map<String, Object> messageTimeInfo = historyManager.getMessageTimeInfo(id, null, searchStartTime,
					searchEndTime);

			Object maxObj = messageTimeInfo.get("MAXTID");
			Object minObj = messageTimeInfo.get("MINID");

			if (maxObj != null && minObj != null) {
				maxId = ((Number) maxObj).longValue();
				minId = ((Number) minObj).longValue();
			}

			VCard vCard = TalkManager.getVCardManager().getVCard();

			// get available latest 100 messages from DB
			List<? extends TalkMessage> talkMessageList = historyManager.getMessageList(id, null, maxId + 1, 100,
					false);
			if (talkMessageList != null && !(talkMessageList.isEmpty())) {

				Platform.runLater(() -> {
					if (chatRoom_Controller.chatPane != null) {
						chatRoom_Controller.chatPane.getItems().clear();
//						chatRoom_Controller.chatPane.refresh();
						chatRoom_Controller.rootContainerView = base_main_ui_container;
					}
				});
				for (TalkMessage transcriptMessage : talkMessageList) {

					Platform.runLater(() -> {

						if ((transcriptMessage.getTalkBody().getFrom()).equals(vCard.getTel())) {
							MsgUtil.addTalkMessage(transcriptMessage, false, chatRoom_Controller,
									BackgroundStuff.getCurrentJid(), vCard.getNick());
						} else {

							MsgUtil.addTalkMessage(transcriptMessage, true, chatRoom_Controller,
									BackgroundStuff.getCurrentJid(), "");
						}
					});
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private PopOver createPopOver(ObservableList<String> menuContent) {
		ListView<String> menuList = new ListView<String>();
		menuList.setItems(menuContent);
		menuList.setStyle("-fx-max-height: 120;-fx-max-width: 130;");
		menuList.setCellFactory(lv -> new ListCell<String>() {
			Label cellItemLabel = new Label();

			{
				cellItemLabel.setStyle(
						"-fx-text-fill:  #a9a9a9 , white , white; -fx-font-style: italic;-fx-font-family: GTWalsheimProRegular;");
			}

			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (!empty) {
					cellItemLabel.setText(item);
					setGraphic(cellItemLabel);
				} else {
					setGraphic(null);
				}
			}
		});
//		menuList.getStyleClass().add("@../styles/menulist.css");
		menuList.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {

				// close pop-over before open the pop-up dialog
				if (plusBtnPopOver != null)
					plusBtnPopOver.hide();

				popUp((menuList.getSelectionModel().getSelectedItem()).replace(" ", ""));
			}
		});

		PopOver popOver = new PopOver(menuList);
		popOver.setArrowLocation(PopOver.ArrowLocation.TOP_LEFT);
		popOver.setAutoHide(true);
		popOver.setHideOnEscape(true);

		return popOver;

	}

	public void popUp(String model) {
		try {

//			Stage perStage = (Stage) chatRoomPane.getScene().getWindow();
//
//			double x = perStage.getX();
//			double y = perStage.getY();
//
//			Stage stage = new Stage();
//
//			stage.setX(x);
//			stage.setY(y);

			// Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/"
			// + model + ".fxml"));

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/" + model + ".fxml"));
			Parent root = loader.load();
			if (model.equalsIgnoreCase("NewChat")) {

				newChatController = loader.getController();
				newChatController.setChatRoomController(chatRoom_Controller);
				newChatController.setChatRoomPane(chatRoomPane);
				newChatController.setRoot1(root1);
				newChatController.rootContainerView = base_main_ui_container;

			} else if (model.equals("NewUser")) {
				AddContact_Controller controller = loader.getController();
				controller.rootContainerView = base_main_ui_container;

			} else if (model.equalsIgnoreCase("FindFriends")) {

				FindFriendsController controller = loader.getController();
				double prefWidth = ScreenUtil.getMainScreenDimentions().get(ScreenUtil.MAIN_SCREEN_DIMENTIONS_WIDTH);
				double prefHeight = ScreenUtil.getMainScreenDimentions().get(ScreenUtil.MAIN_SCREEN_DIMENTIONS_HEIGHT);
				controller.rootVbox.setPrefSize(prefWidth * 0.6, prefHeight * 0.5);
				controller.rootContainerView = base_main_ui_container;
			} else if (model.equalsIgnoreCase("NewGroup")) {

				AddGroup_Controller controller = loader.getController();
				controller.rootContainerView = base_main_ui_container;

			} else if (model.equalsIgnoreCase("InviteFriends")) {
				InviteFriendsController controller = loader.getController();
				controller.rootContainerView = base_main_ui_container;
			}
			VBox outer = new VBox();
			outer.setAlignment(Pos.CENTER);
			outer.getChildren().add(root);
			outer.setBackground(
					new Background(new BackgroundFill(Color.rgb(0, 0, 0, 0.3), new CornerRadii(0), new Insets(0))));

			// show pop-up dialog in UI
			if (base_main_ui_container != null && base_main_ui_container.getChildren()
					.get(base_main_ui_container.getChildren().size() - 1) != outer) {
				base_main_ui_container.setAlignment(Pos.CENTER);
				base_main_ui_container.getChildren().add(outer);
			}

//			Scene scene = new Scene(outer);
//			scene.setFill(Color.rgb(255, 255, 255, 0));
//
//			stage.initStyle(StageStyle.TRANSPARENT);
//			stage.setScene(scene);
//			stage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setUserInfoPane() {
		Parent root1 = null;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/UserInfo.fxml"));
			root1 = (Parent) loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		chatRoomPane.getChildren().setAll(root1);
	}

	public void initAvatar() {

		VCard vCard = TalkManager.getVCard();
		ImageManager.setAvatar(vCard.getJid(), vCard.getAvatar());

		Collection<Talk> talkList = localManager.getTalkList();
		for (Talk talk : talkList) {
			ImageManager.setAvatar(talk.getJid(), talk.getAvatar());
		}

		List<Member> memberList = localManager.getMemberList();
		for (Member member : memberList) {
			ImageManager.setAvatar(member.getMemJid(), member.getAvatar());
		}
		ListenerManager.fireAvatarChanged(null);
	}

	protected static byte[] getAvatarImages(String jid) {
		byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
		return talkAvatar;
	}

	@FXML
	void ActionListener(ActionEvent event) {

	}

	/*
	 * private Date getStartTime(Date endtime) { Integer key = 1000; Calendar cal =
	 * Calendar.getInstance(); cal.setTime(endtime); cal.add(Calendar.MONTH, 0 -
	 * key); return cal.getTime(); }
	 */

	public void setMyProfile(Circle userAtr) {
		try {
			FXMLLoader loaderpro = new FXMLLoader();
			loaderpro.setLocation(getClass().getClassLoader().getResource("fxml/Myprofile.fxml"));
			Parent commmon = loaderpro.load();
			myProfileController = loaderpro.getController();
			chatRoomPane.getChildren().setAll(commmon);
			myProfileController.setChatRoom_Controller(chatRoom_Controller);
			myProfileController.setRoot1(root1);
			myProfileController.setChatRoomPane(chatRoomPane);
			myProfileController.setUserAtr(userAtr);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}

	public void setBaseViewContainer(StackPane base_view) {
		base_main_ui_container = base_view;
		if (commonRoomController != null)
			commonRoomController.rootContainerView = base_main_ui_container;
	}

	/**
	 * load chat history data from local DB and visualize it, using a separate
	 * thread
	 *
	 * @param id       current selected user's jID
	 * @param userName current selected user's name or TP no
	 * @author nisal
	 **/
	private void updateChatHistory(String id, String userName) {
		// TODO: create a thread pool executor and add this to it
		chatHistoryManageThread = new Thread(() -> {
			try {

//				if (!chatHistoryManageThread.isInterrupted())
//					Thread.sleep(1000);// wait until recent chat user list UI updates

				updateChatRoom(userName, id, chatRoom_Controller, 100);

				// set current selected user's all message list as READ
				dao.updateMessageStatus(BackgroundStuff.getCurrentJid(), TalkMessage.STATUS_READED);

			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		chatHistoryManageThread.setDaemon(true);
		chatHistoryManageThread.start();
	}

	/**
	 * stop current on going chat history update thread before create a new one
	 *
	 * @author nisal
	 **/
	private void stopCurrentRunningThread() {

		try {

			if (chatHistoryManageThread != null && chatHistoryManageThread.getState() != State.TIMED_WAITING)
				chatHistoryManageThread.interrupt();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * create all necessary UI nodes for recent chat user list
	 *
	 * @param holder cell UI component holder
	 * @param cell   current updating cell
	 **/
	private void initializeUserListNodes(UserListComponentsHolder holder, ListCell<Talk> cell) {

		try {

			holder.mainBaseContainer = new HBox(8);

			holder.userImgCircle = new Circle(18, 30, 18);
			holder.presenceBaseContainer = new VBox();
			holder.presenceNotificationBaseContainer = new HBox();
			holder.presenceNotificationCircle = new Circle(18, 30, 7);

			holder.userNameLabel = new Label();
			holder.recentChatMsg = new Label();

			holder.newMsgNotificationCount = new Text();
			holder.centerComponentContainer = new VBox();
			holder.leftComponentContainer = new StackPane();
			holder.rightComponentContainer = new VBox();

			holder.presenceBaseContainer.setPrefSize(35, 45);
			holder.presenceBaseContainer.setMaxSize(35, 45);
			holder.presenceNotificationBaseContainer.setPrefSize(15, 15);
			holder.presenceBaseContainer.setStyle("/*-fx-background-color: yellow;*/-fx-alignment: bottom-right;");
			holder.presenceNotificationBaseContainer
					.setStyle("/*-fx-background-color: #2a53e2;*/-fx-alignment: top-right;");

			cell.setMaxWidth(190);
			cell.setPrefWidth(190);
			cell.setPrefHeight(59);
			cell.setStyle(String.format("%s-fx-padding: 6 8.075 6 8.075;", cell.getStyle()));

			holder.rightComponentContainer.setAlignment(Pos.CENTER_LEFT);

			holder.centerComponentContainer.setPadding(new Insets(0, 0, 0, 0));
			holder.centerComponentContainer.setAlignment(Pos.CENTER_LEFT);

			// added by Nisal to keep recent chat message label width equal to parent (when
			// there is a message with more emojis, label width increase more that list
			// width)
			Rectangle rect = new Rectangle(110, 29.5);
			holder.recentChatMsg.setClip(rect);

			holder.userNameLabel.setId("idUserNameLabel");
			holder.recentChatMsg.setId("idChatNameLabel");

			holder.userNameLabel.setPrefHeight(29.5);
			holder.userNameLabel.setTextAlignment(TextAlignment.LEFT);
			holder.recentChatMsg.setPrefHeight(29.5);
			holder.recentChatMsg.setTextAlignment(TextAlignment.LEFT);

			holder.leftComponentContainer.prefHeight(Region.USE_COMPUTED_SIZE);
			holder.leftComponentContainer.setAlignment(Pos.CENTER);
			holder.leftComponentContainer.prefWidth(25);
			holder.leftComponentContainer.setMaxSize(35, Region.USE_COMPUTED_SIZE);
			holder.leftComponentContainer.setStyle("-fx-alignment: center;-fx-start-margin: 25;");

			holder.presenceNotificationBaseContainer.getChildren().add(holder.presenceNotificationCircle);
			holder.presenceBaseContainer.getChildren().add(holder.presenceNotificationBaseContainer);
			holder.leftComponentContainer.getChildren().addAll(holder.userImgCircle, holder.presenceBaseContainer);

			holder.newMsgNotificationCircle = new Circle(7);
			holder.newMsgNotificationCircle.setStyle("-fx-fill: #39CBE2;-fx-alignment: top-right;");
			holder.newMsgNotificationContainer = new StackPane(holder.newMsgNotificationCircle);
			holder.recentMsgIconContainer = new StackPane();
			holder.newMsgNotificationContainer.setAlignment(Pos.CENTER);
			holder.newMsgNotificationContainer.setPrefHeight(29.5);
			holder.recentMsgIconContainer.setPrefHeight(29.5);
			holder.recentMsgIconContainer.setAlignment(Pos.CENTER);
			holder.newMsgNotificationCount.setStyle(
					"-fx-text-fill: white;-fx-fill: white;-fx-font-size: 10px; -fx-font-family: GTWalsheimProBold;-fx-padding: 20 0 10 0;");
			holder.newMsgNotificationCount.setBoundsType(TextBoundsType.VISUAL);
			holder.newMsgNotificationContainer.getChildren().add(holder.newMsgNotificationCount);

			holder.rightComponentContainer.getChildren().addAll(holder.newMsgNotificationContainer,
					holder.recentMsgIconContainer);

			holder.mainBaseContainer.setPadding(new Insets(0, 0, 0, 8));
			holder.mainBaseContainer.getChildren().addAll(holder.leftComponentContainer,
					holder.centerComponentContainer, holder.rightComponentContainer);
			holder.mainBaseContainer.setAlignment(Pos.CENTER_LEFT);

		} catch (Exception e) {
			e.printStackTrace();

			cell.setGraphic(null);
		}
	}

	/**
	 * update all created user list cell nodes with data
	 *
	 * @param holder cell UI component holder
	 * @param cell   current updating cell
	 * @param talk   current item object
	 **/
	private void updateUserListCellNodesWithData(UserListComponentsHolder holder, ListCell<Talk> cell, Talk talk,
			boolean empty) {

		try {

			// change font colors
			if (talk.isActiveRoom()) {
				holder.recentChatMsg.getStyleClass().clear();
				holder.recentChatMsg.getStyleClass().add("selected");
				holder.userNameLabel.getStyleClass().clear();
				holder.userNameLabel.getStyleClass().add("selected");
				cell.setStyle("-fx-background-color: linear-gradient(to right, #39CDE3, #3cdeb5);");
			} else {
				holder.userNameLabel.getStyleClass().clear();
				holder.recentChatMsg.getStyleClass().clear();
				cell.setStyle("-fx-background-color: white;");
			}
			if (talk.getPresence() == 1) {
				holder.presenceNotificationCircle.setStroke(Color.WHITE);
				holder.presenceNotificationCircle
						.setStyle("-fx-fill: #87EABF;-fx-alignment: top-right;-fx-border-radius: 30;");
			} else if (talk.getPresence() == 2) {
				holder.presenceNotificationCircle.setStroke(Color.WHITE);
				holder.presenceNotificationCircle
						.setStyle("-fx-fill: #ea2f20;-fx-alignment: top-right;-fx-border-radius: 30;");
			} else {
				holder.presenceNotificationCircle.setFill(Color.TRANSPARENT);
				holder.presenceNotificationCircle.setStyle("-fx-alignment: top-right;");
			}

			holder.userNameLabel.setText(TalkUtil.getValidMemberName(talk));// create valid name for selected chat
																			// member

			holder.centerComponentContainer.getChildren().removeAll(holder.userNameLabel, holder.recentChatMsg);
			if (BackgroundStuff.getActiveNode().equals("C")) {
				holder.centerComponentContainer.setPrefWidth(110);
				holder.centerComponentContainer.setMaxWidth(110);
				holder.userNameLabel.setPadding(new Insets(4, 0, -2, 0));
				holder.recentChatMsg.setPadding(new Insets(-2, 0, 4, 0));
				// set most recent chat message to label
				holder.recentChatMsg = MsgUtil.printOnChatList(talk, holder.recentChatMsg,
						holder.recentMsgIconContainer);
				holder.centerComponentContainer.getChildren().addAll(holder.userNameLabel, holder.recentChatMsg);
			} else {
				holder.userNameLabel.setPrefHeight(Region.USE_COMPUTED_SIZE);
				holder.centerComponentContainer.setPrefWidth(Region.USE_COMPUTED_SIZE);
				holder.centerComponentContainer.setMaxWidth(Region.USE_COMPUTED_SIZE);
				holder.centerComponentContainer.setPrefHeight(Region.USE_COMPUTED_SIZE);
				holder.centerComponentContainer.getChildren().add(holder.userNameLabel);
			}

			holder.userImgCircle.setFill(new ImagePattern(talk.getImg()));

			cell.setGraphic(holder.mainBaseContainer);

			if (talk.isHasMsg() && talk.getNewMessageCount() > 0 && BackgroundStuff.getActiveNode().equals("C")) {
				holder.newMsgNotificationCount.setText(String.valueOf(talk.getNewMessageCount()));
				holder.newMsgNotificationContainer.setVisible(true);
			} else
				holder.newMsgNotificationContainer.setVisible(false);

		} catch (Exception e) {
			e.printStackTrace();

			cell.setGraphic(null);
			cell.setText(null);
		}

	}

	/**
	 * This class will hold the references to all UI components used in recent chat
	 * user list
	 **/
	static class UserListComponentsHolder {
		Text newMsgNotificationCount;
		Circle newMsgNotificationCircle;
		StackPane newMsgNotificationContainer;
		StackPane recentMsgIconContainer;
		VBox rightComponentContainer;

		Circle userImgCircle;
		Circle presenceNotificationCircle;
		StackPane leftComponentContainer;
		VBox presenceBaseContainer;
		HBox presenceNotificationBaseContainer;

		Label userNameLabel;
		Label recentChatMsg;
		VBox centerComponentContainer;

		HBox mainBaseContainer;
	}

}
