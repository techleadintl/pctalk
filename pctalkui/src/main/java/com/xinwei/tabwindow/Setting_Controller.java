package com.xinwei.tabwindow;

import com.xinwei.screenshot.ScreenCapture;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.conf.UserConfig;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.BitSet;
import java.util.ResourceBundle;

public class Setting_Controller implements Initializable {

	private static final String character2 = null;
	public BitSet keyboardBitSet = new BitSet();
	
	@FXML
	private ListView<String> Setting_lst;
	@FXML
	private Pane uppur;
	@FXML
	Pane SettingPane;

	ObservableList<String> data = FXCollections.observableArrayList("General Setting", "Shortcut Keys","Language Setting", "About");

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		Setting_lst.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
		Setting_lst.addEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);
		
		setRoom("GeneralSettings");
		Setting_lst.setItems(data);
		Setting_lst.setCellFactory(list -> new AttachmentListCell());

		Setting_lst.setOnMouseClicked(event -> {
            if (Setting_lst.getSelectionModel().getSelectedItem().equals("General Setting")) {
                setRoom("GeneralSettings");
            } else if (Setting_lst.getSelectionModel().getSelectedItem().equals("Shortcut Keys")) {
                setRoom("ShortcutKeys");
            } else if (Setting_lst.getSelectionModel().getSelectedItem().equals("Language Setting")) {
                setRoom("LanguageSettings");
            } else if(Setting_lst.getSelectionModel().getSelectedItem().equals("About")){
                setRoom("About");
            }
        });

	}

	private static class AttachmentListCell extends ListCell<String> {
		@Override
		public void updateItem(String item, boolean empty) {
			super.updateItem(item, empty);
			this.setStyle("-fx-cell-size: 60;");
			if (empty) {
				setGraphic(null);
				setText(null);
			} else {
				ImageView pictureImageView = new ImageView();
				pictureImageView.setStyle("-fx-padding: 45px");


				Image image = new Image(getClass().getClassLoader().getResource("images/" + item + ".png").toString());
				Circle circle = new Circle(10);
				circle.setFill(new ImagePattern(image));
				circle.setStyle("-fx-padding: 45;");
				pictureImageView.setImage(image);
				HBox hBox = new HBox(10);
				hBox.setStyle(" -fx-padding: 10;");
				Label label = new Label();
				label.setText(item);
//				label.setStyle("-fx-text-fill:gray;");
				hBox.getChildren().addAll(circle,label);
//				setGraphic(circle);
//				setText(item);
				setStyle("-fx-font-family: GTWalsheimProRegular; -fx-text-fill:gray;");
				setGraphic(hBox);
			}
		}
	}

	public void setRoom(String ui) {
		Parent root1 = null;
		try {
			root1 = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/" + ui + ".fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		SettingPane.getChildren().setAll(root1);
	}
	
	String charact = getCharactor(character2);
	 public EventHandler<KeyEvent> keyPressedEventHandler = new EventHandler<KeyEvent>() {
	        @Override
	        public void handle(KeyEvent event) {

	        	System.out.println("Shortcut key pressed   >>>>    " + event.toString());
	        	
	            keyboardBitSet.set(event.getCode().ordinal(), true);
	            
	            event.consume();

	            updateKeyboardStatus();
	            
	            String code = "CONTROL " + event.getCode().toString();
	            
	            if (code.equals(character)) {
	            	
					try {
					
						FXMLLoader loader = new FXMLLoader();
						loader.setLocation(getClass().getResource("/fxml/ShortcutKeys.fxml"));
						Parent root = loader.load();
						ShortcutKeysController shortcutKeysController = loader.getController();
						
						shortcutKeysController.keyType(getCharactor(character));
						
		            	System.out.println("character : "+character);
		            	
					} catch (IOException e) {
						
						e.printStackTrace();
					}
					
	            }
	        }
	    };

	    public EventHandler<KeyEvent> keyReleasedEventHandler = new EventHandler<KeyEvent>() {
	        @Override
	        public void handle(KeyEvent event) {
	        	
	        	System.out.println("Shortcut key Released   >>>>    " + event.toString());

	            keyboardBitSet.set(event.getCode().ordinal(), false);
	            
	            event.consume();

	            updateKeyboardStatus();
	            

	        }
	        
	    };
	
    public String character ="" ;
    public void updateKeyboardStatus() {
    	character ="" ;

        StringBuilder sb = new StringBuilder();
    	int count = 0;
        for( KeyCode keyCode: KeyCode.values()) {

            if( keyboardBitSet.get(keyCode.ordinal())) {
            	if( count > 0) {
                    sb.append( " ");
                }
            	count++;
            	sb.append(keyCode.toString());
            	
            	character = sb.toString();
    		    System.out.println("Shortcut key   >>>>    " + character);
    		    
    						UserConfig userConfig = TalkManager.getUserConfig();
    						String scr_hotkey = userConfig.getShortKeyValue("Screenshot");
    						String main_win_hotkey = userConfig.getShortKeyValue("MainWindow");

    						if (scr_hotkey.equals(character)) {
    							
    							Platform.runLater(new Runnable() {
    								
    								@Override
    								public void run() {
    									
    									new ScreenCapture().start(new Stage());
    									keyboardBitSet.clear();
    								}
    							});
    						} if (main_win_hotkey.equals(character)) {
    							Platform.runLater(new Runnable() {
    								
    								@Override
    								public void run() {

    									Stage stage = (Stage)Setting_lst.getScene().getWindow();
    									stage.setIconified(true);
    									keyboardBitSet.clear();
    									keyboardBitSet.clear();
    								}
    							});
    						}
    						
    						if (count == 2) {
    							System.out.println("Count is equal to : " + count);
    							System.out.println("Return Value1     >>>>>>>>>>>>>>>>>>>>>            " + character);
    							getCharactor(character);

    						}	
            }
            
        }
        System.out.println("Return Value2     >>>>>>>>>>>>>>>>>>>>>            " + character);

    }
    
    public String getCharactor(String character2) {
		return character2;
		
	}
}
