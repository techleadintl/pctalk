package com.xinwei.tabwindow;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import com.xinwei.BackgroundStuff;
import com.xinwei.spark.ui.MessageDialog;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.dao.TalkDao;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class ManageGroupMembersController implements Initializable {

	@FXML
	private HBox upperBox;

	@FXML
	ListView<Circle> horizontalList;
	
	
	@FXML
	private ListView<Talk> listView;

    @FXML
    private TextField textfield;

    @FXML
    private Button update;
    
    @FXML
    private Button getMemList;
    
    @FXML
    private Pane title;

    @FXML
    private Button close;
    
    
    @FXML
    private TextField grpName;
    
    private Stage stage;
    
	ObservableList<Talk> friendList = FXCollections.observableArrayList();
	
	ObservableList<Circle> memberAvatars = FXCollections.observableArrayList();

	
	Collection<Member> memberList = null;
	List<String> memJid = new ArrayList<String>();
	TalkDao dao = TalkManager.getTalkDao();
	Group group = null;
	
	public StackPane rootContainerView;

	@FXML
    void handleButtonAction(ActionEvent event) {
    	
    	if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {
				/*stage = (Stage) close.getScene().getWindow();
				stage.close();*/
				closePopUp();
			}else if (button.equals(update)) { 
				
				remUIDs.forEach(remUID -> {
					try {
						TalkManager.getTalkService().deleteMember(group.getCreatorUid(), BackgroundStuff.getCurrentGroupId(), remUID);
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
				
				List<Member> members =  new ArrayList<>(memberList);

				addJids.forEach(addJId -> {
					try {
						List<? extends Talk> talk = dao.getTalkByJid(addJId);
						Member m2 = new Member();
						m2.setUid(talk.get(0).getUid());
						m2.setMemNick(TalkUtil.getDisplayName(addJId));
						m2.setMemJid(addJId);
						m2.setMemRole(Member.ROLE_USER);
						members.add(m2);
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
				
				try {
					if(addJids.size()>0)
					TalkManager.getTalkService().addGroupMembers(group, members);
				} catch (Exception e) {
					e.printStackTrace();
				}
				/*stage = (Stage) close.getScene().getWindow();
				stage.close();*/
				closePopUp();
			}
		}
    }
	
	public void getMembers() {
		
		try {
			group = TalkManager.getTalkService().getGroup(BackgroundStuff.getCurrentGroupId());
			grpName.setText(!group.getName().isEmpty()?group.getName():"Name Group");

		} catch (Exception e) {
			e.printStackTrace();
		}
		memberList = group.getMemberList();
		for(Member member : memberList) {
			Circle avatar = new Circle(15);
			Image image = new Image(new ByteArrayInputStream(getAvatarImages(member.getMemJid())), 50, 50, true, true);
			avatar.setFill(new ImagePattern(image));
			avatar.setId(member.getMemJid());
			
			memberAvatars.add(avatar);
			memJid.add(member.getMemJid());
		}				
		horizontalList.setItems(memberAvatars);
	}
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		horizontalList.setOrientation(Orientation.HORIZONTAL);
		
		friendList = BackgroundStuff.getInstance().getFriendList();		
		listView.setItems(friendList);
        listView.setEditable(true);
        listView.setCellFactory(lv -> new ListCell<Talk>() {

        	@Override
    		public void updateItem(Talk item, boolean empty) {
    			if (empty) {
    				setGraphic(null);
    				setText(null);
    			} else {
    		            //do other stuff here
    		            setOnMouseClicked(mouseClickedEvent -> {
    		                if (mouseClickedEvent.getButton().equals(MouseButton.PRIMARY) && mouseClickedEvent.getClickCount() == 2) {
    		                	if(memJid.contains(item.getJid()))
    								return;
    							Circle avatar = new Circle(15);
    							Image image = new Image(new ByteArrayInputStream(getAvatarImages(item.getJid())), 50, 50, true, true);
    							avatar.setFill(new ImagePattern(image));
    							avatar.setId(item.getJid());
    							memberAvatars.add(avatar);

    							memJid.add(item.getJid());
    							addJids.add(item.getJid());
    		                }
    		            });
    		        
    		        Circle friendAvatar = new Circle(15);
    				Image image = new Image(new ByteArrayInputStream(getAvatarImages(item)), 50, 50, true, true);
    				friendAvatar.setFill(new ImagePattern(image));

    				setGraphic(friendAvatar);
    				if (!((item.getNick()).equals(""))) {
    					setText(item.getNick());
    				} else {
    					setText(item.getTel());
    				}
    			}
    		}
        });
        
        
        horizontalList.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(event.getClickCount() == 2) {
					String memberJId =(horizontalList.getSelectionModel().getSelectedItem().getId());
					if(memberList.stream().filter(o -> o.getMemJid().equals(memberJId)).findFirst().isPresent()) {
						Member m =localManager.getMemberByJid(BackgroundStuff.getCurrentGroupId(), memberJId);
						memberList.remove(m);
						memJid.remove(memberJId);
						remUIDs.add(m.getUid());
					}
					addJids.remove(memberJId);
					memberAvatars.remove(horizontalList.getSelectionModel().getSelectedItem());
				}
			}
		});
       
        getMembers();	
       
        grpName.focusedProperty().addListener(new ChangeListener<Boolean>()
		{
		    @Override
		    public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
		    {
		        if (!newPropertyValue&& (!grpName.getText().equals("Name Group")))
		        {
		        	try {
						TalkManager.getTalkService().updateGroup(group, grpName.getText(), group.getDescribe());
					} catch (Exception e1) {
						e1.printStackTrace();
						MessageDialog.showErrorDialog(e1);
					}
		        }
		       
		    }
		});
	}

	public void setName() {
		
	}
	
public void setNamea() {
		
	}
	LocalManager localManager = TalkManager.getLocalManager();
	List<String> remUIDs = new ArrayList<String>();
	List<String> addJids = new ArrayList<String>();
	
	protected static byte[] getAvatarImages(Talk item) {
		String jid = item.getJid();
		byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
		return talkAvatar;
	}
	
	protected static byte[] getAvatarImages(String item) {
		byte[] talkAvatar = ImageManager.getTalkAvatar(item, true);
		return talkAvatar;
	}
	
	private void closePopUp() {
		if (rootContainerView != null && rootContainerView.getChildren().size() > 1) {
			rootContainerView.getChildren().remove(rootContainerView.getChildren().size() - 1);
		}
	}

}