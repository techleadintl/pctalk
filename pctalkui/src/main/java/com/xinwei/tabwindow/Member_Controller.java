package com.xinwei.tabwindow;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.xinwei.BackgroundStuff;
import com.xinwei.mainwindow.ChatRoom_Controller;
import com.xinwei.mainwindow.Main_Controller;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

public class Member_Controller implements Initializable {

	@FXML
	private ImageView search_img;
	
	@FXML
	private ListView<Talk> lst;
	
	@FXML
	private Pane chatRoomPane;

	
	protected LocalManager localManager = TalkManager.getLocalManager();
	
	List<? extends Talk> talkList;

	ObservableList<Talk> friends = FXCollections.observableArrayList();


	static Map<String, Talk> mapMamber = new HashMap<String, Talk>();

	protected static List<byte[]> avatars = new ArrayList<byte[]>();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	initAvatar();
	friends = BackgroundStuff.getInstance().getFriendList();
	lst.setItems(friends);
    lst.setEditable(true);

    lst.setCellFactory(lv -> new ListCell<Talk>() {
        final StackPane messageNotification;
        //final Text numberText;
        
        private HBox x = new HBox(10);;
		Label imageLabel = new Label();
       
        //final InvalidationListener listener;

        {
        	Label label = new Label();
			label.setStyle("-fx-text-fill: #FFFFFF;");
			Image img = new Image("/images/blink.gif", 20, 20, true, true);
			label.setGraphic(new ImageView(img));
			
			
            // notification item (white number on red circle)
            //Circle background = new Circle(10, Color.RED);

            //numberText = new Text();
          //  numberText.setFill(Color.BLACK);
//            /numberText.setText(Integer.toString(5));

            messageNotification = new StackPane(label);//, numberText);
            messageNotification.setVisible(false);

            //listener = o -> updateMessageCount();
            setGraphic(messageNotification);
        }

      /*  void updateMessageCount() {
            updateMessageCount(messageCount.getOrDefault(getItem(), 0));
        }

        void updateMessageCount(int count) {
            boolean messagesPresent = count > 0;
            if (messagesPresent) {
                //numberText.setText(Integer.toString(count));
            }
            messageNotification.setVisible(messagesPresent);

        }
*/
    	@Override
		public void updateItem(Talk item, boolean empty) {

			super.updateItem(item, empty);
			
            Label not = new Label();
			not.setGraphic(messageNotification);
			
			if (empty) {
				setGraphic(null);
				setText(null);
			} else {
				
				setText((!(item.getNick()).equals(""))?item.getNick():item.getTel());

    			
    			imageLabel.setStyle("-fx-text-fill: #FFFFFF;");
    			imageLabel.setGraphic(item.getImage());

    			if(item.isHasMsg())
					messageNotification.setVisible(true);
				else {
                    messageNotification.setVisible(false);
				}
    			
    			x.getChildren().addAll(not,imageLabel);
    			

    			setGraphic(x);
    			
    			if(item.isActiveRoom()) {
                	  messageNotification.setVisible(false);
                	//x.getChildren().remove(not);
                	return;
                }
			}
		}

    });
    
    Main_Controller mainControllerHandle= new Main_Controller();
	search_img.addEventFilter(KeyEvent.KEY_PRESSED, mainControllerHandle.keyPressedEventHandler);
	search_img.addEventFilter(KeyEvent.KEY_RELEASED, mainControllerHandle.keyReleasedEventHandler);
	
}

	public void UpdateRoom(String name) {
		Parent root1 = null;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/ChatRoom.fxml"));
			root1 = (Parent) loader.load();
			ChatRoom_Controller room = loader.getController();
			room.setRoomType(true);
			room.setChatMember(name);

		} catch (IOException e) {
			e.printStackTrace();
		}
		chatRoomPane.getChildren().setAll(root1);
	}

	
	protected static byte[] getAvatarImages(String jid) {
		byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
		return talkAvatar;
	}



	public void initAvatar() {

		VCard vCard = TalkManager.getVCard();
		ImageManager.setAvatar(vCard.getJid(), vCard.getAvatar());

		Collection<Talk> talkList = localManager.getTalkList();
		for (Talk talk : talkList) {
			ImageManager.setAvatar(talk.getJid(), talk.getAvatar());
		}

		List<Member> memberList = localManager.getMemberList();
		for (Member member : memberList) {
			ImageManager.setAvatar(member.getMemJid(), member.getAvatar());
		}
		ListenerManager.fireAvatarChanged(null);
	}




}
