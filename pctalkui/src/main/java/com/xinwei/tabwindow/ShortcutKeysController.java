package com.xinwei.tabwindow;

import java.net.URL;
import java.util.BitSet;
import java.util.ResourceBundle;

import org.controlsfx.control.ToggleSwitch;

import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.conf.UserConfig;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class ShortcutKeysController implements Initializable{
	@FXML
	private ToggleSwitch openMainWindowCheckLabel;
	@FXML
	private ToggleSwitch screenShotCheckLabel;
	@FXML
	private TextField openMainWindowTextField;
	@FXML
	private TextField screenShotTextField, text;
	@FXML
	private Button btn;
	@FXML
	private AnchorPane ShortcutKey;
	
	public BitSet keyboardBitSet = new BitSet();
	
	private static final String[] keys = {"Screenshot"};
	private static final String[] keys2 = {"MainWindow"};
	
	@FXML
	public void handleShortcutKey(MouseEvent e) {
		System.out.println("Toggle key pressed");
		UserConfig userConfig = TalkManager.getUserConfig();
		
			if(e.getSource().equals(screenShotCheckLabel) && screenShotCheckLabel.isSelected()) {
			
				ObservableMap<String, String> map = FXCollections.observableHashMap();
		        for (String key : keys) map.put(key, key.replaceAll("key", "value"));
		        String key = "Screenshot";
				map.put(key, screenShotTextField.getText());
				
		        userConfig.setShortKeyValue(key, screenShotTextField.getText());
		        TalkManager.saveUserConfig();

				screenShotTextField.setEditable(false);
				screenShotTextField.setDisable(false);
			}if(e.getSource().equals(openMainWindowCheckLabel) && openMainWindowCheckLabel.isSelected()) {
			
				ObservableMap<String, String> map = FXCollections.observableHashMap();
				for (String key : keys2) map.put(key, key.replaceAll("key", "value"));
				String key = "MainWindow";
				map.put(key, openMainWindowTextField.getText());
			
				userConfig.setShortKeyValue(key, openMainWindowTextField.getText());
				TalkManager.saveUserConfig();
				
				openMainWindowTextField.setEditable(false);
				openMainWindowTextField.setDisable(false);
			}if(e.getSource().equals(screenShotCheckLabel) && screenShotCheckLabel.isSelected() == false) {

				ObservableMap<String, String> map = FXCollections.observableHashMap();
		        for (String key : keys) map.put(key, key.replaceAll("key", "value"));
		        String key = "Screenshot";
				map.put(key, "");
				
		        userConfig.setShortKeyValue(key, "");
		        TalkManager.saveUserConfig();
		        screenShotTextField.setText("");
		        screenShotTextField.setDisable(true);
			}if(e.getSource().equals(openMainWindowCheckLabel) && openMainWindowCheckLabel.isSelected() == false) {

				ObservableMap<String, String> map = FXCollections.observableHashMap();
				for (String key : keys2) map.put(key, key.replaceAll("key", "value"));
				String key = "MainWindow";
				map.put(key, "");
			
				userConfig.setShortKeyValue(key, "");
				TalkManager.saveUserConfig();
				openMainWindowTextField.setText("");
				openMainWindowTextField.setDisable(true);
				}
		}
	
		@FXML
		public void keyType(String character) {
			
			if (screenShotTextField.isFocused()) {
			System.out.println("Button pressed");
			screenShotTextField.setText(character);
			System.out.println("Text field value    >>>   " + screenShotTextField.getText());
			UserConfig userConfig = TalkManager.getUserConfig();
			ObservableMap<String, String> map = FXCollections.observableHashMap();
	        for (String key : keys) map.put(key, key.replaceAll("key", "value"));
	        String key = "Screenshot";
			map.put(key, screenShotTextField.getText());
			TalkManager.getUserConfig().setShortKeyValue(key, screenShotTextField.getText());
			userConfig.setShortKeyValue(key, screenShotTextField.getText());
			TalkManager.saveUserConfig();
			
		}else {
			
			System.out.println("screenShotTextField not focused");
			
		}
			
	}
		
		
		private EventHandler<KeyEvent> keyPressedEventHandler = new EventHandler<KeyEvent>() {
	        @Override
	        public void handle(KeyEvent event) {
	        	
	        	if (event.getCode().toString().equals("BACK_SPACE") && screenShotTextField.isFocused() ) {
	        		screenShotTextField.setText(null);
	        	
	        	}if (event.getCode().toString().equals("BACK_SPACE") && openMainWindowTextField.isFocused() ) {
	        		openMainWindowTextField.setText(null);
	        		
	        	}

	        	System.out.println("Shortcut key pressed   >>>>    " + event.toString());
	        	
	            keyboardBitSet.set(event.getCode().ordinal(), true);
	            
	            event.consume();

	            updateKeyboardStatus();

	           
	        }
	    };

	    private EventHandler<KeyEvent> keyReleasedEventHandler = new EventHandler<KeyEvent>() {
	        @Override
	        public void handle(KeyEvent event) {
	        	
	        	System.out.println("Shortcut key Released   >>>>    " + event.toString());

	            keyboardBitSet.set(event.getCode().ordinal(), false);
	            
	            event.consume();

	            updateKeyboardStatus();
	            

	        }
	        
	    };
	    
	
		@FXML
		public String character ="" ;
		@FXML
	    public void updateKeyboardStatus() {
	    	character ="" ;

	        StringBuilder sb = new StringBuilder();
	    	int count = 0;
	        for( KeyCode keyCode: KeyCode.values()) {

	            if( keyboardBitSet.get(keyCode.ordinal())) {
	            	if( count > 0) {
	                    sb.append( " ");
	                }
	            	count++;
	            	sb.append(keyCode.toString());
	            	
	            	character = sb.toString();
	    		    System.out.println("Shortcut key   >>>>    " + character);
	    						
	    						if (count == 2 && screenShotTextField.isFocused()) {
	    							System.out.println("Count is equal to : " + count);
	    							
	    							System.out.println("Return Value1     >>>>>>>>>>>>>>>>>>>>>            " + character);
	    							getCharactor(character);
	    							screenShotTextField.setText(character);
	    							UserConfig userConfig = TalkManager.getUserConfig();
	    							ObservableMap<String, String> map = FXCollections.observableHashMap();
	    					        for (String key : keys) map.put(key, key.replaceAll("key", "value"));
	    					        String key = "Screenshot";
	    							map.put(key, character);
	    							TalkManager.getUserConfig().setShortKeyValue(key, character);
	    							userConfig.setShortKeyValue(key, character);
	    							TalkManager.saveUserConfig();
	    						
	    						}if (count == 2 && openMainWindowTextField.isFocused()) {
	    							System.out.println("Count is equal to : " + count);
	    							
	    							System.out.println("Return Value1     >>>>>>>>>>>>>>>>>>>>>            " + character);
	    							getCharactor(character);
	    							openMainWindowTextField.setText(character);
	    							UserConfig userConfig = TalkManager.getUserConfig();
	    							ObservableMap<String, String> map = FXCollections.observableHashMap();
	    					        for (String key : keys2) map.put(key, key.replaceAll("key", "value"));
	    					        String key = "MainWindow";
	    							map.put(key, character);
	    							TalkManager.getUserConfig().setShortKeyValue(key, character);
	    							userConfig.setShortKeyValue(key, character);
	    							TalkManager.saveUserConfig();
	    						
	    						}
	    						
	            }
	            
	        }
	        System.out.println("Return Value2     >>>>>>>>>>>>>>>>>>>>>            " + character);
    
	    }

		public String getCharactor(String character2) {
			return character2;
			
		}
		
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		//ShortcutKey.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
		//openMainWindowTextField.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
		UserConfig userConfig = TalkManager.getUserConfig();
		String key1 = "Screenshot";
		String key2 = "MainWindow";
		//String key = userConfig.getShortKeyValue(key1);
		//String key21 = userConfig.getShortKeyValue(key2);
		boolean flag = userConfig.getShortKeyEnable(userConfig.getShortKeyValue(key1));
		boolean flag2 = userConfig.getShortKeyEnable(userConfig.getShortKeyValue(key2));
		screenShotTextField.setText(userConfig.getShortKeyValue(key1));
		openMainWindowTextField.setText(userConfig.getShortKeyValue(key2));
		screenShotCheckLabel.setSelected(flag);
		openMainWindowCheckLabel.setSelected(flag2);
		if (flag == false) {
			screenShotTextField.setEditable(false);
			screenShotTextField.setDisable(true);
		}if (flag == false) {
			openMainWindowTextField.setEditable(false);
			openMainWindowTextField.setDisable(true);
		}
		
		
		screenShotTextField.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
		screenShotTextField.addEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);
		openMainWindowTextField.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
		openMainWindowTextField.addEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);
	}

    @FXML
	public void shortCutSet(String string) {
		text.setText(string);
		System.out.println("hjkl;");
	}
	
	
}
