package com.xinwei.tabwindow;

import java.io.IOException;
import java.net.URL;
import java.util.BitSet;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.controlsfx.control.PopOver;

import com.xinwei.BackgroundStuff;
import com.xinwei.mainwindow.ChatRoom_Controller;
import com.xinwei.mainwindow.Main_Controller;
import com.xinwei.modelwindow.AddContact_Controller;
import com.xinwei.modelwindow.AddGroup_Controller;
import com.xinwei.modelwindow.NewChatController;
import com.xinwei.screenshot.ScreenCapture;
import com.xinwei.search.SearchPanel;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.dao.TalkDao;
import com.xinwei.talk.listener.AvatarListener;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.LocalManager;
import com.xinwei.talk.manager.MessageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.avatar.Avatar;
import com.xinwei.talk.model.conf.UserConfig;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.uiutils.PopUpUtil;
import com.xinwei.util.GroupUtil;
import com.xinwei.util.MsgUtil;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import javafx.stage.Stage;

public class GroupController implements Initializable, AvatarListener {
	private static final String character2 = null;
	public BitSet keyboardBitSet = new BitSet();

	Main_Controller mainControllerHandle;

	@FXML
	private ImageView add;

	@FXML
	private ListView<Group> group_lst;

	@FXML
	private Pane chatRoomPane;

	@FXML
	public TextField searchFriend;

	List<? extends Group> groupList;

	protected static LocalManager localManager = TalkManager.getLocalManager();

	ObservableList<Group> data = FXCollections.observableArrayList();
	ObservableList<String> menuContent = FXCollections.observableArrayList("New User", "New Group"/* , "New Chat" */);

	Parent root1;
	NewChatController newChatController;
	public ChatRoom_Controller chatRoom_Controller;
	ManageGroupMembersController manageGroupMembersController;
	TalkDao dao = TalkManager.getTalkDao();
	static Parent rootGM = null;
	public StackPane base_main_ui_container;

	// added by Vaasu to fix Recent chat group duplicate issue
	private int userIndex = 0;

	private PopOver popOver;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		group_lst.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
		group_lst.addEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);

		try {

			// added by Nisal to register user Avatar changed listener
			ListenerManager.addAvatarListener(this);

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/ChatRoom.fxml"));
			Parent root1 = loader.load();
			chatRoom_Controller = loader.getController();
			chatRoom_Controller.rootContainerView = base_main_ui_container;

			chatRoomPane.getChildren().setAll(root1);

			chatRoom_Controller.setIsGroup(true);
			chatRoom_Controller.clearChatRoom();
			System.out.println("baseview" + base_main_ui_container);

		} catch (Exception e) {
			e.printStackTrace();
		}

		data = BackgroundStuff.getGroupList();

		// modified by Nisal
		// in here I removed the observable list reference form list view and added a
		// sorted list(also a observable list) reference. because of this no matter
		// whatever change we made to the list view, list will be automatically sorted.
		// but we directly can't update a sorted list. then I kept the observable list
		// references too. here we have to change the observable list only. then
		// sort enabled list will automatically updated with sorting.
		group_lst.setItems(BackgroundStuff.getSortedGroupList());

		group_lst.setCellFactory(lv -> new ListCell<Group>() {
			GroupListComponentsHolder holder = new GroupListComponentsHolder();

			{
				initializeGroupListNodes(holder, this);// create cell UI components
			}

			@Override
			public void updateItem(Group item, boolean empty) {
				super.updateItem(item, empty);

				if (empty || item == null) {
					setStyle("-fx-background-color: white;");// remove selection color for this cell
					if (chatRoom_Controller != null)// clear chat history pane
						chatRoom_Controller.chatPane.getItems().clear();
					setGraphic(null);
					setItem(null);
				} else {
					Platform.runLater(() -> {
						// update cell items with current cell data
						updateGroupListCellNodesWithData(holder, this, item, empty);
					});
				}
			}

		});

		group_lst.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {

				if (group_lst.getItems() == null || group_lst.getItems().isEmpty()
						|| group_lst.getSelectionModel() == null
						|| group_lst.getSelectionModel().getSelectedItem() == null
						|| group_lst.getSelectionModel().getSelectedItem().getJid() == null
						|| group_lst.getSelectionModel().getSelectedItem().getJid().isEmpty())
					return;

				String jid = group_lst.getSelectionModel().getSelectedItem().getJid();

				if (BackgroundStuff.getCurrentJid() != null && jid != null
						&& BackgroundStuff.getCurrentJid().equals(jid))
					return;

				BackgroundStuff.setCurrentJid(jid);
				Group group = (group_lst.getSelectionModel().getSelectedItem());

				// set current selected user's all message list as READ
				dao.updateMessageStatus(BackgroundStuff.getCurrentJid(), TalkMessage.STATUS_READED);

				updateRoom((group_lst.getSelectionModel().getSelectedItem()).getName(),
						group_lst.getSelectionModel().getSelectedItem().getJid(), chatRoom_Controller);

				userIndex = 0;
				data.forEach((grp) -> {
					grp.setActiveRoom(false);

					if (BackgroundStuff.getCurrentJid().equals(grp.getJid())) {
						grp.setHasMsg(false);
						grp.setActiveRoom(true);

						// added by Nisal for reset new messages count
						grp.setNewMessageCount(0);
					}

					if (userIndex >= 0 && group_lst.getItems().size() > userIndex)
						data.set(userIndex, grp);// changed by Nisal from List view to data, to avoid from unsupported
													// operation Excep: (because of sorting list)

					userIndex++;
				});

			}
		});

		// FOR SEARCH FEATURE ADDED BY VAASU
		String DEFAULT_TEXT = Res.getMessage("input.text.search");
		final ListView<String> console = new ListView<String>(FXCollections.<String>observableArrayList());
		// listen on the console items and remove old ones when we get over 20 items in
		// the list
		console.getItems().addListener(new ListChangeListener<String>() {
			@Override
			public void onChanged(Change<? extends String> change) {
				while (change.next()) {
					if (change.getList().size() > 20)
						change.getList().remove(0);
				}
			}
		});

		searchFriend.setOnKeyPressed(ke -> {
			console.getItems().add("Key Pressed: " + ke.getText());
			System.out.println("ENTERING SEARCH BOX");
			if (searchFriend.getText().equals(DEFAULT_TEXT)) {
				searchFriend.setText("");
			}
			if (ke.getCode() == KeyCode.ENTER && searchFriend.isFocused()) {
				System.out.println("FOCUS SEARCH BOX");
			}
		});

		searchFriend.setOnKeyReleased(ke -> {
			System.out.println("===KEY REALEASE HERE====");
			new SearchPanel(searchFriend.getText().isEmpty(), searchFriend, null, null, group_lst, data).doSearch();
		});

		ContextMenu contextMenu = new ContextMenu();

		MenuItem item1 = new MenuItem("Quit Group");
		item1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.out.println("Quit " + group_lst.getSelectionModel().getSelectedItem().getJid());
				try {
					contextMenu.hide();

					int status = TalkManager.getTalkService().quitMember(BackgroundStuff.getvCard().getUid(),
							group_lst.getSelectionModel().getSelectedItem().getId());

					if (status == 0) // remove selected item from list view
						removeGivenItemFromGroupList(group_lst.getSelectionModel().getSelectedItem());
					else
						PopUpUtil.showAlert("Could Not Quit Group", AlertType.ERROR);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		MenuItem item2 = new MenuItem("Delete group");
		item2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					System.out.println("Delete " + group_lst.getSelectionModel().getSelectedItem().getJid());
					contextMenu.hide();

					int status = TalkManager.getTalkService().deleteGroup(BackgroundStuff.getvCard().getUid(),
							group_lst.getSelectionModel().getSelectedItem().getId());

					if (status == 0) // remove selected item from list view
						removeGivenItemFromGroupList(group_lst.getSelectionModel().getSelectedItem());
					else
						PopUpUtil.showAlert("Could Not Delete Group", AlertType.ERROR);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		contextMenu.getItems().addAll(item1, item2);

		group_lst.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {

			@Override
			public void handle(ContextMenuEvent event) {

				contextMenu.show(group_lst, event.getScreenX(), event.getScreenY());
			}
		});
		add.setOnMousePressed(new EventHandler<MouseEvent>() {

			ContextMenu contextMenu = createPopup();
			{
				popOver = createPopOver(menuContent);
			}

			@Override
			public void handle(MouseEvent event) {
				popOver.show(add);
				System.out.println("button clicked");
			}
		});

	}

	/*************************** Listeners start ****************************/

	/**
	 * fired when user Avatar changed
	 **/
	@Override
	public void update(List<Avatar> avatars) {
//		if ((avatars == null || avatars.isEmpty()) && BackgroundStuff.getActiveNode() != null
//				&& !BackgroundStuff.getActiveNode().equals("G"))
//			return;
//
//		group_lst.refresh();
	}

	/*************************** Listeners end ****************************/

	protected void updateRoom(String name, String id, ChatRoom_Controller chatRoom_Controller) {

		long maxId = 0;
		try {

			chatRoom_Controller.clearChatRoom();
			chatRoom_Controller.setIsGroup(true);
			chatRoom_Controller.setChatMember(name);
			BackgroundStuff.setCurrentGroupId(group_lst.getSelectionModel().getSelectedItem().getId());
			final MessageManager historyManager = TalkManager.getMessageManager();

			Date searchEndTime = TalkManager.getXmppService().getServerDate();

			Date searchStartTime = MsgUtil.getStartTime(searchEndTime);

			Map<String, Object> messageTimeInfo = historyManager.getMessageTimeInfo(id, null, searchStartTime,
					searchEndTime);

			Object maxObj = messageTimeInfo.get("MAXTID");
			Object minObj = messageTimeInfo.get("MINID");

			if (maxObj != null && minObj != null) {
				maxId = ((Number) maxObj).longValue();
			}
			VCard vCard = TalkManager.getVCardManager().getVCard();

			List<? extends TalkMessage> talkMessageList = historyManager.getMessageList(id, null, maxId + 1, 100,
					false);
			if (talkMessageList != null && !(talkMessageList.isEmpty())) {
				Platform.runLater(() -> {
					if (chatRoom_Controller.chatPane != null) {
						chatRoom_Controller.chatPane.getItems().clear();
//							chatRoom_Controller.chatPane.refresh();
						chatRoom_Controller.rootContainerView = base_main_ui_container;
					}
				});
				for (TalkMessage transcriptMessage : talkMessageList) {
					final String user = MsgUtil.getNameByIds(BackgroundStuff.getCurrentGroupId(),
							transcriptMessage.getJid());
					Platform.runLater(() -> {

						if ((transcriptMessage.getTalkBody().getFrom()).equals(vCard.getTel())) {

							MsgUtil.addTalkMessage(transcriptMessage, false, chatRoom_Controller,
									BackgroundStuff.getCurrentJid(), vCard.getNick());
						} else {
							MsgUtil.addTalkMessage(transcriptMessage, true, chatRoom_Controller,
									BackgroundStuff.getCurrentJid(), user);
						}
					});
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String createGroupName(Collection<Member> memberList) {

		String name = "";
		if (memberList != null) {
			for (Member menberName : memberList) {
				name += menberName.getMemNick() + ",";
			}
		}
		return name;
	}

//	private Date getStartTime(Date endtime) {
//		Integer key = 1000;
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(endtime);
//		cal.add(Calendar.MONTH, 0 - key);
//		return cal.getTime();
//	}

	String charact = getCharactor(character2);
	public EventHandler<KeyEvent> keyPressedEventHandler = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {

			System.out.println("Shortcut key pressed   >>>>    " + event.toString());

			keyboardBitSet.set(event.getCode().ordinal(), true);

			event.consume();

			updateKeyboardStatus();

			String code = "CONTROL " + event.getCode().toString();

			if (code.equals(character)) {

				try {

					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(getClass().getResource("/fxml/ShortcutKeys.fxml"));
					Parent root = loader.load();
					ShortcutKeysController shortcutKeysController = loader.getController();

					shortcutKeysController.keyType(getCharactor(character));

				} catch (IOException e) {

					e.printStackTrace();
				}

			}
		}
	};

	public EventHandler<KeyEvent> keyReleasedEventHandler = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {

			System.out.println("Shortcut key Released   >>>>    " + event.toString());

			keyboardBitSet.set(event.getCode().ordinal(), false);

			event.consume();

			updateKeyboardStatus();

		}

	};

	public String character = "";

	public void updateKeyboardStatus() {
		character = "";

		StringBuilder sb = new StringBuilder();
		int count = 0;
		for (KeyCode keyCode : KeyCode.values()) {

			if (keyboardBitSet.get(keyCode.ordinal())) {
				if (count > 0) {
					sb.append(" ");
				}
				count++;
				sb.append(keyCode.toString());

				character = sb.toString();

				UserConfig userConfig = TalkManager.getUserConfig();
				String scr_hotkey = userConfig.getShortKeyValue("Screenshot");
				String main_win_hotkey = userConfig.getShortKeyValue("MainWindow");

				if (scr_hotkey.equals(character)) {

					Platform.runLater(new Runnable() {

						@Override
						public void run() {

							new ScreenCapture().start(new Stage());
							keyboardBitSet.clear();
						}
					});
				}
				if (main_win_hotkey.equals(character)) {
					Platform.runLater(new Runnable() {

						@Override
						public void run() {

							Stage stage = (Stage) group_lst.getScene().getWindow();
							stage.setIconified(true);
							keyboardBitSet.clear();
						}
					});
				}

				if (count == 2) {

					getCharactor(character);

				}
			}
		}

	}

	public String getCharactor(String character2) {
		return character2;

	}

	private ContextMenu createPopup() {
		final ContextMenu contextMenu = new ContextMenu();
		contextMenu.setStyle(
				"-fx-text-fill:  #a9a9a9 , white , white; -fx-font-style: italic;-fx-font-family: GTWalsheimProRegular;");
		MenuItem member = new MenuItem("New User");
		MenuItem group = new MenuItem("New Group");

		member.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				popUp("NewUser");
			}
		});

		group.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				popUp("NewGroup");
			}
		});

		contextMenu.getItems().addAll(member, group);
		return contextMenu;

	}

	private PopOver createPopOver(ObservableList<String> menuContent) {
		ListView<String> menuList = new ListView<String>();
		menuList.setItems(menuContent);
		menuList.setStyle("-fx-max-height: 120;-fx-max-width: 130;");
		menuList.setCellFactory(lv -> new ListCell<String>() {
			Label cellItemLabel = new Label();

			{
				cellItemLabel.setStyle(
						"-fx-text-fill:  #a9a9a9 , white , white; -fx-font-style: italic;-fx-font-family: GTWalsheimProRegular;");
			}

			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (!empty) {
					cellItemLabel.setText(item);
					setGraphic(cellItemLabel);
				} else {
					setGraphic(null);
				}
			}
		});
//		menuList.getStyleClass().add("@../styles/menulist.css");
		menuList.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {

				// close pop-over before open the pop-up dialog
				if (popOver != null)
					popOver.hide();

				popUp((menuList.getSelectionModel().getSelectedItem()).replace(" ", ""));
			}
		});

		PopOver popOver = new PopOver(menuList);
		popOver.setArrowLocation(PopOver.ArrowLocation.TOP_LEFT);
		popOver.setAutoHide(true);
		popOver.setHideOnEscape(true);

		return popOver;

	}

	public void popUp(String model) {
		try {

//			Stage perStage = (Stage) chatRoomPane.getScene().getWindow();
//
//			double x = perStage.getX();
//			double y = perStage.getY();
//
//			Stage stage = new Stage();
//
//			stage.setX(x);
//			stage.setY(y);

			// Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/"
			// + model + ".fxml"));

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/" + model + ".fxml"));
			Parent root = loader.load();

			// "New User", "New Group", "New Chat"
			if (model.equalsIgnoreCase("NewChat")) {
				newChatController = loader.getController();
				newChatController.setChatRoomController(chatRoom_Controller);
				newChatController.setChatRoomPane(chatRoomPane);
				newChatController.setRoot1(root1);
				newChatController.rootContainerView = base_main_ui_container;
			} else if (model.equalsIgnoreCase("NewGroup")) {
				AddGroup_Controller addGroup_Controller = loader.getController();
				addGroup_Controller.rootContainerView = base_main_ui_container;
			} else if (model.equalsIgnoreCase("NewUser")) {
				AddContact_Controller addContact_Controller = loader.getController();
				addContact_Controller.rootContainerView = base_main_ui_container;
			}

			VBox outer = new VBox();
			outer.getChildren().add(root);
			outer.setPadding(new Insets(110d, 330d, 90d, 350d));
			outer.setBackground(
					new Background(new BackgroundFill(Color.rgb(0, 0, 0, 0.3), new CornerRadii(0), new Insets(0))));

			// show pop-up dialog in UI
			if (base_main_ui_container != null && base_main_ui_container.getChildren()
					.get(base_main_ui_container.getChildren().size() - 1) != outer) {
				base_main_ui_container.setAlignment(Pos.CENTER);
				base_main_ui_container.getChildren().add(outer);
			}

//			Scene scene = new Scene(outer);
//			scene.setFill(Color.rgb(255, 255, 255, 0));
//
//			stage.initStyle(StageStyle.TRANSPARENT);
//			stage.setScene(scene);
//			stage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * add new group to group list
	 * 
	 * @author nisal
	 * @param message current received new group confirm message object
	 **/
	public static void addNewGroupToGroupList(TalkMessage message) {

		try {

			// get group id from message JID
			String id = message.getJid().split("@")[0].split("_")[1];
			List<? extends Group> groups = TalkManager.getTalkDao().getGroupByID(id);
			// add this new group to group list
			if (groups != null && groups.size() > 0) {
				Group group = groups.get(0);
				GroupUtil.updateGroupWithData(group);// update this group with necessary data
				BackgroundStuff.addToGroupList(group);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * remove given group from group list and re-set all selections
	 * 
	 * @author nisal
	 * @param selectedGroup group object to be removed
	 **/
	private void removeGivenItemFromGroupList(Group selectedGroup) {
		// changed by Nisal from List view to data, to avoid from unsupported operation
		// Excep: (because of sorting list)
		data.remove(group_lst.getSelectionModel().getSelectedItem());
		BackgroundStuff.setCurrentJid(null);
		group_lst.refresh();
		BackgroundStuff.resetLists();
	}

	/**
	 * create all necessary UI nodes for recent chat group list
	 *
	 * @param holder cell UI component holder
	 * @param cell   current updating cell
	 **/
	private void initializeGroupListNodes(GroupListComponentsHolder holder, ListCell<Group> cell) {

		try {

			holder.mainBaseContainer = new HBox(8);

			holder.userImgCircle = new Circle(18, 30, 18);
			holder.rightComponentContainer = new VBox();

			holder.userNameLabel = new Label();
			holder.recentChatMsg = new Label();
			holder.centerComponentContainer = new VBox();

			holder.newMsgNotificationCount = new Text();
			holder.newMsgNotificationCircle = new Circle(7);
			holder.newMsgNotificationContainer = new StackPane(holder.newMsgNotificationCircle);
			holder.recentMsgIconContainer = new StackPane();
			holder.leftComponentContainer = new StackPane();

			cell.setMaxWidth(190);
			cell.setPrefWidth(190);
			cell.setPrefHeight(59);
			cell.setStyle(String.format("%s-fx-padding: 6 8.075 6 8.075;", cell.getStyle()));

			holder.rightComponentContainer.setAlignment(Pos.CENTER_LEFT);
			holder.centerComponentContainer.setPadding(new Insets(0, 0, 0, 0));
			holder.centerComponentContainer.setAlignment(Pos.CENTER_LEFT);

			// added by Nisal to keep recent chat message label width equal to parent (when
			// there is a message with more emojis, label width increase more that list
			// width)
			Rectangle rect = new Rectangle(110, 29.5);
			holder.recentChatMsg.setClip(rect);

			holder.userNameLabel.setId("idUserNameLabel");
			holder.recentChatMsg.setId("idChatNameLabel");

			holder.userNameLabel.setPrefHeight(29);
			holder.userNameLabel.setTextAlignment(TextAlignment.LEFT);
			holder.recentChatMsg.setPrefHeight(29);
			holder.recentChatMsg.setTextAlignment(TextAlignment.LEFT);

			holder.leftComponentContainer.prefHeight(Region.USE_COMPUTED_SIZE);
			holder.leftComponentContainer.setAlignment(Pos.CENTER);
//			holder.leftComponentContainer.prefWidth(25);
			holder.leftComponentContainer.setMaxSize(35, Region.USE_COMPUTED_SIZE);
			holder.leftComponentContainer.setStyle("-fx-alignment: center;-fx-start-margin: 25;");
			holder.leftComponentContainer.getChildren().add(holder.userImgCircle);

			holder.newMsgNotificationCircle.setStyle("-fx-fill: #39CBE2;-fx-alignment: top-right;");
			holder.newMsgNotificationContainer.setAlignment(Pos.CENTER);
			holder.newMsgNotificationContainer.setPrefHeight(29);
			holder.recentMsgIconContainer.setPrefHeight(29);
			holder.recentMsgIconContainer.setAlignment(Pos.CENTER);
			holder.newMsgNotificationCount.setStyle(
					"-fx-text-fill: white;-fx-fill: white;-fx-font-size: 10px; -fx-font-family: GTWalsheimProBold;-fx-padding: 20 0 10 0;");
			holder.newMsgNotificationCount.setBoundsType(TextBoundsType.VISUAL);
			holder.newMsgNotificationContainer.getChildren().add(holder.newMsgNotificationCount);

			holder.rightComponentContainer.getChildren().addAll(holder.newMsgNotificationContainer,
					holder.recentMsgIconContainer);

			holder.mainBaseContainer.setPadding(new Insets(0, 0, 0, 8));
			holder.mainBaseContainer.getChildren().addAll(holder.leftComponentContainer,
					holder.centerComponentContainer, holder.rightComponentContainer);
			holder.mainBaseContainer.setAlignment(Pos.CENTER_LEFT);

		} catch (Exception e) {
			e.printStackTrace();

			cell.setGraphic(null);
		}

	}

	/**
	 * update all created group list cell nodes with data
	 *
	 * @param holder cell UI component holder
	 * @param cell   current updating cell
	 *
	 **/
	private void updateGroupListCellNodesWithData(GroupListComponentsHolder holder, ListCell<Group> cell, Group group,
			boolean empty) {

		try {

			if (group.isActiveRoom()) {
				holder.userNameLabel.getStyleClass().clear();
				holder.userNameLabel.getStyleClass().add("selected");
				holder.recentChatMsg.getStyleClass().clear();
				holder.recentChatMsg.getStyleClass().add("selected");
				holder.newMsgNotificationContainer.setVisible(false);
				cell.setStyle("-fx-background-color: linear-gradient(to right, #39CDE3, #3cdeb5);");
			} else {
				holder.userNameLabel.getStyleClass().clear();
				holder.recentChatMsg.getStyleClass().clear();
				cell.setStyle("-fx-background-color: white;");
			}

			holder.userNameLabel.setText(
					(group.getName() != null && !group.getName().equals("")) ? group.getName() : group.getJid());
			holder.centerComponentContainer.getChildren().removeAll(holder.userNameLabel, holder.recentChatMsg);

			holder.centerComponentContainer.widthProperty().add(110);
			holder.centerComponentContainer.setMinWidth(110);
			holder.centerComponentContainer.setPrefWidth(110);
			holder.centerComponentContainer.setMaxWidth(110);
			holder.userNameLabel.setPadding(new Insets(4, 0, -2, 0));
			holder.recentChatMsg.setPadding(new Insets(-2, 0, 4, 0));

			// set most recent chat message to label
			holder.recentChatMsg = MsgUtil.printOnChatList(group, holder.recentChatMsg, holder.recentMsgIconContainer);
			holder.centerComponentContainer.getChildren().addAll(holder.userNameLabel, holder.recentChatMsg);

			holder.userImgCircle.setFill(new ImagePattern(group.getImg()));
			cell.setGraphic(holder.mainBaseContainer);

			if (group.isHasMsg() && group.getNewMessageCount() > 0 && BackgroundStuff.getActiveNode().equals("G")) {
				holder.newMsgNotificationCount.setText(String.valueOf(group.getNewMessageCount()));
				holder.newMsgNotificationContainer.setVisible(true);
			} else
				holder.newMsgNotificationContainer.setVisible(false);

		} catch (Exception e) {
			e.printStackTrace();

			cell.setGraphic(null);
		}

	}

	/**
	 * This class will hold the references to all UI components used in recent chat
	 * group list
	 **/
	static class GroupListComponentsHolder {
		Text newMsgNotificationCount;
		Circle newMsgNotificationCircle;
		StackPane newMsgNotificationContainer;
		StackPane recentMsgIconContainer;
		VBox rightComponentContainer;

		Circle userImgCircle;
		StackPane leftComponentContainer;

		Label userNameLabel;
		Label recentChatMsg;
		VBox centerComponentContainer;

		HBox mainBaseContainer;
	}

	public void setBaseViewContainer(StackPane base_view) {
		base_main_ui_container = base_view;
	}
}
