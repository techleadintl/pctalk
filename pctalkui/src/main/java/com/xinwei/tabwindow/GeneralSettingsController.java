package com.xinwei.tabwindow;

import com.jfoenix.controls.JFXTextField;
import com.xinwei.BackgroundStuff;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.conf.UserConfig;
import com.xinwei.util.FxAudioUtil;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.controlsfx.control.ToggleSwitch;

import java.net.URL;
import java.util.ResourceBundle;

public class GeneralSettingsController implements Initializable{
	@FXML
	private JFXTextField autoMsgText;
	@FXML
	private AnchorPane SettingCommonPanel;
	@FXML
	private ToggleSwitch loginCheckLabel;
	@FXML
	private ToggleSwitch updateCheckLabel;
	@FXML
	private ToggleSwitch soundCheckLabel;
	@FXML
	private ToggleSwitch checkBtnMute;
	@FXML
	private ToggleSwitch checkBtnAutoReply;
	@FXML
	private RadioButton receiveMsgRadioGroup1;
	@FXML
	private RadioButton receiveMsgRadioGroup2;
	@FXML
	private RadioButton closeMainWindowRadioGroup1;
	@FXML
	private RadioButton closeMainWindowRadioGroup2;
	@FXML
	private BooleanProperty switchedOn = new SimpleBooleanProperty(false);
	@FXML
	private BooleanProperty switchedOff = new SimpleBooleanProperty(true);
	private Object powerOnCheckLabel1;
	@FXML
	private ToggleSwitch powerOnCheckLabel;
	
	@FXML
	public void handleActionPerformed(MouseEvent e) {

		UserConfig userConfig = TalkManager.getUserConfig();

		if (e.getSource() == loginCheckLabel) {
			System.out.println("Pressed Auto Login Toggle");
			TalkManager.getVCard().setAutoLogin(loginCheckLabel.isSelected());
			TalkManager.saveVCard();
		} else {
			if (e.getSource() == soundCheckLabel) {
				userConfig.setMsgSound(soundCheckLabel.isSelected());
				System.out.println("Pressed soundCheckLabel Toggle");
			} else if (e.getSource() == updateCheckLabel) {
				userConfig.setAutoUpdater(updateCheckLabel.isSelected());
				System.out.println("Pressed updateCheckLabel Toggle");
			} else if (e.getSource() == powerOnCheckLabel1) {
				userConfig.setAutoPowerOn(powerOnCheckLabel.isSelected());
			} else if (e.getSource() == receiveMsgRadioGroup2) {
				userConfig.setAvatarFlash(receiveMsgRadioGroup2.isSelected());
				System.out.println("Pressed receiveMsgRadioGroup1 Toggle");
			} 
			else if (e.getSource() == receiveMsgRadioGroup1) {
				userConfig.setAvatarFlash(receiveMsgRadioGroup2.isSelected());
				System.out.println("Pressed receiveMsgRadioGroup2 Toggle");
			}
			else if (e.getSource() == closeMainWindowRadioGroup1) {
				userConfig.setCloseMainWindow(closeMainWindowRadioGroup1.isSelected());
				System.out.println("Pressed closeMainWindowRadioGroup1 Toggle");
			}
			else if (e.getSource() == closeMainWindowRadioGroup2) {
				userConfig.setCloseMainWindow(closeMainWindowRadioGroup1.isSelected());
				System.out.println("Pressed closeMainWindowRadioGroup2 Toggle");
			}else if (e.getSource() == checkBtnMute) {
				FxAudioUtil.muteSound(checkBtnMute.isSelected());
				BackgroundStuff.setMuteStatus(checkBtnMute.isSelected());
			}else if (e.getSource() == checkBtnAutoReply) {
				System.out.println("Pressed Auto Message Toggle");
				autoMsgText.getText();
				System.out.println("=====&&&&&&&&&&&&&&&&&&&&&&====="+autoMsgText.getText());
				TalkManager.getVCard().setAutoMsg(autoMsgText.getText());
				TalkManager.saveVCard();
			}
			TalkManager.saveUserConfig();
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		UserConfig userConfig = TalkManager.getUserConfig();
		
		loginCheckLabel.setSelected(TalkManager.getVCard().isAutoLogin());
		
		soundCheckLabel.setSelected(userConfig.getMsgSound());
		
		updateCheckLabel.setSelected(userConfig.getAutoUpdater());
		checkBtnMute.setSelected(userConfig.getMuteStatus());
		//powerOnCheckLabel.setSelected(userConfig.getAutoPowerOn());
		
		if(userConfig.getAvatarFlash()==true) {
			receiveMsgRadioGroup1.setSelected(false);
			receiveMsgRadioGroup2.setSelected(userConfig.getAvatarFlash());
		}else {
			receiveMsgRadioGroup1.setSelected(true);
		}
		
		if(userConfig.getCloseMainWindow()==false) {
			closeMainWindowRadioGroup2.setSelected(true);
		}else {
			closeMainWindowRadioGroup1.setSelected(true);
		}

		
	}
	
}
