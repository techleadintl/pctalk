package com.xinwei.tabwindow;

import java.io.ByteArrayInputStream;

import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.model.Talk;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;

public class Friend extends Talk{

	private String jId;
	//private String nick;
	private ImageView image;
	private boolean hasMsg;
	private boolean isActiveRoom;
	public Friend() {
	}
	
	public Friend(String jId,String nick) {
		this.jId = jId;
		this.nick = nick;
	}
	
	public String getjId() {
		return jId;
	}
	public void setjId(String jId) {
		this.jId = jId;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public ImageView getImage() {
		ImageView pictureImageView = new ImageView();
		Image img = new Image(new ByteArrayInputStream(getAvatarImages(this.jId)), 50, 50, true, true);
		pictureImageView.setClip(new Circle(25, 25, 25));
		pictureImageView.setImage(img);
		
		this.image = pictureImageView;
		return image;
	}
	public void setImage(ImageView image) {
		ImageView pictureImageView = new ImageView();
		Image img = new Image(new ByteArrayInputStream(getAvatarImages(this.jId)), 50, 50, true, true);
		pictureImageView.setClip(new Circle(25, 25, 25));
		pictureImageView.setImage(img);
		
		this.image = pictureImageView;
	}
	
	protected static byte[] getAvatarImages(String jid) {
		byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
		return talkAvatar;
	}

	public boolean isHasMsg() {
		return hasMsg;
	}

	public void setHasMsg(boolean hasMsg) {
		this.hasMsg = hasMsg;
	}

	public boolean isActiveRoom() {
		return isActiveRoom;
	}

	public void setActiveRoom(boolean isActiveRoom) {
		this.isActiveRoom = isActiveRoom;
	}
	
	
	
}
