package com.xinwei;

import com.xinwei.mainwindow.Main_Controller;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Talk;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.conf.UserConfig;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;

public class BackgroundStuff {

	private static final BackgroundStuff instance = new BackgroundStuff();

	private BackgroundStuff() {
		// TODO Auto-generated constructor stub
	}

	public static BackgroundStuff getInstance() {
		return instance;

	}

	private static VCard vCard = TalkManager.getVCardManager().getVCard();

	private static ObservableList<Talk> friendList = FXCollections.observableArrayList();
	private static ObservableList<Talk> rcntFriendList = FXCollections.observableArrayList();
	private static SortedList<Talk> friendListSortable = new SortedList<>(FXCollections.observableArrayList());
	private static SortedList<Talk> rcntFriendListSortable = new SortedList<>(FXCollections.observableArrayList());

	private static ObservableList<Group> groupList = FXCollections.observableArrayList();
	private static SortedList<Group> groupListSortable = new SortedList<>(FXCollections.observableArrayList());

	private static String activeNode = "C";
	private static Main_Controller controller;
	private static String currentJid = "";
	private static String currentGroupId = "";
	private static String name = "";
	private static UserConfig userConfig = TalkManager.getUserConfig();
	private static boolean muteStatus;

	public static String getName() {
		return name;
	}

	public static void setName(String name) {
		BackgroundStuff.name = name;
	}

	public static boolean chatListening = false;
	public static boolean groupListening = false;

	public static VCard getvCard() {
		return vCard;
	}

	public static void setvCard(VCard vCard) {
		BackgroundStuff.vCard = vCard;
	}

	public ObservableList<Talk> getFriendList() {
		return friendList;
	}

	public void setFriendList(ObservableList<Talk> friendList) {
		BackgroundStuff.friendList = friendList;
	}

	public ObservableList<Talk> getRcntFriendList() {
		return rcntFriendList;
	}

	public void setRcntFriendList(ObservableList<Talk> rcntFriendList) {
		BackgroundStuff.rcntFriendList = rcntFriendList;
	}

	// manage sortable lists start ------------------------------
	public SortedList<Talk> getSortedFriendList() {
		return friendListSortable;
	}

	public void setSortedFriendList(SortedList<Talk> friendList) {
		BackgroundStuff.friendListSortable = friendList;
	}

	public SortedList<Talk> getSortedRcntFriendList() {
		return rcntFriendListSortable;
	}

	public void setSortedRcntFriendList(SortedList<Talk> rcntFriendList) {
		BackgroundStuff.rcntFriendListSortable = rcntFriendList;
	}
	// manage sortable lists end ------------------------------

	public static String getActiveNode() {
		return activeNode;
	}

	public static void setActiveNode(String activeNode) {
		BackgroundStuff.activeNode = activeNode;
	}

	public static void addToFriendList(Talk friend) {
		friendList.add(friend);
	}

	public static void addToRecentFriendList(Talk friend) {
		rcntFriendList.add(friend);
	}

	public static void resetLists() {
		if (friendList != null && !friendList.isEmpty())
			friendList.forEach((friend) -> {
				friend.setActiveRoom(false);
			});

		if (rcntFriendList != null && !rcntFriendList.isEmpty())
			rcntFriendList.forEach((friend) -> {
				friend.setActiveRoom(false);
			});

		if (groupList != null && !groupList.isEmpty())
			groupList.forEach((group) -> {
				group.setActiveRoom(false);
			});
	}

	public static Main_Controller getController() {
		return controller;
	}

	public static void setController(Main_Controller controller) {
		BackgroundStuff.controller = controller;
	}

	public static String getCurrentJid() {
		return currentJid;
	}

	public static void setCurrentJid(String currentJid) {
		BackgroundStuff.currentJid = currentJid;
	}

	public static String getCurrentGroupId() {
		return currentGroupId;
	}

	public static void setCurrentGroupId(String currentGroupId) {
		BackgroundStuff.currentGroupId = currentGroupId;
	}

	public static ObservableList<Group> getGroupList() {
		return groupList;
	}

	public static void setGroupList(ObservableList<Group> groupList) {
		BackgroundStuff.groupList = groupList;
	}

	// manage sortable lists start ------------------------------
	public static SortedList<Group> getSortedGroupList() {
		return groupListSortable;
	}

	public static void setSortedGroupList(SortedList<Group> groupList) {
		BackgroundStuff.groupListSortable = groupList;
	}
	// manage sortable lists end ------------------------------

	/**
	 * add new group to existing group list
	 * 
	 * @param group new group to be added to the group list
	 **/
	public static void addToGroupList(Group group) {
		BackgroundStuff.groupList.add(group);
	}

	public static boolean getMuteStatus() {
		return userConfig.getMuteStatus();
	}

	public static void setMuteStatus(boolean isMuted) {
		BackgroundStuff.userConfig.setMuteStatus(isMuted);
		TalkManager.saveUserConfig();
	}
}
