package com.xinwei.search;

import com.xinwei.common.lang.PinyinUtil;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Talk;
import com.xinwei.util.TalkUtil;

import java.util.List;

public class SearchItemInfo {
	public Object obj;
	public String jid;
	public String displayName;
	public String displayId;
	public String avatarUrl;

	public String withoutTone;

	public List<String> searchKeys;

	public SearchItemInfo(Object obj) {
		this.obj = obj;

		if (obj instanceof Talk) {
			Talk talk = (Talk) obj;
			String tel = TalkUtil.getUserTel(talk);
			String uid = talk.getUid();
			this.jid = talk.getJid();
			this.avatarUrl = talk.getAvatarUrl();
			this.displayId = tel != null && !tel.isEmpty() ? tel : uid;
			this.displayName = TalkUtil.getDisplayName(talk.getJid());
			this.withoutTone = PinyinUtil.convertWithoutTone(displayName);

			searchKeys = PinyinUtil.getSearchKeys(displayName, displayId, uid);

		} else if (obj instanceof Group) {
			Group group = (Group) obj;
			this.jid = group.getJid();
			this.avatarUrl = group.getAvatarUrl();
			this.displayId = group.getId();
			this.displayName = TalkUtil.getDisplayName(group.getJid());

			this.withoutTone = PinyinUtil.convertWithoutTone(displayName);

			searchKeys = PinyinUtil.getSearchKeys(displayName, displayId);
		}

	}

	public boolean search(String text) {
		text = text.toLowerCase();
		for (String searchKey : searchKeys) {
			if (searchKey.contains(text)) {
				return true;
			}
		}
		return false;
	}




}