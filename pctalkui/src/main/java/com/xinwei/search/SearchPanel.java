package com.xinwei.search;

import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Talk;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.List;

public class SearchPanel {

    public TextField searchFriend;
    private boolean isInputing;
    private ListView<Talk> listView;
    private ListView<Group> grouplistView;
    private ObservableList<Talk> friendList;
    private ObservableList<Group> groupFriendList;
    private String lastText;

    public SearchPanel(boolean isInputing,
                       TextField searchFriend,
                       ListView<Talk> listView,
                       ObservableList<Talk> friendList,
                       ListView<Group> grouplistView,
                       ObservableList<Group> groupFriendList) {
        this.isInputing = isInputing;
        this.searchFriend = searchFriend;
        this.listView = listView;
        this.friendList= friendList;
        this.grouplistView = grouplistView;
        this.groupFriendList= groupFriendList;
    }

    public void doSearch() {
        if (!isInputing) {
            String text = searchFriend.getText();
            if (text.equals(lastText)) {
                return;
            }
            lastText = text;
             this.updateUI(text);
        }
        else {
            updateUI(lastText);
        }
    }

    public List<SearchItemInfo> updateUI(String text) {
        java.util.List<SearchItemInfo> camtalkItemList = new ArrayList<SearchItemInfo>();
        if(friendList!=null){
            ObservableList<Talk> camtalks = friendList;
            ObservableList<Talk> searchTalk = FXCollections.observableArrayList();
            for (Talk camtalk : camtalks) {
                SearchItemInfo searchItemInfo = new SearchItemInfo(camtalk);
                boolean flag = true;
                if (text != null) {
                    flag = searchItemInfo.search(text);
                }
                if (flag) {
                    searchTalk.add(camtalk);
                }/*else {
                    listView.getItems().remove(camtalk);
                }*/
            }
            listView.setItems(searchTalk);
        }
        if(groupFriendList!=null) {
            ObservableList<Group> camtalksGroup = groupFriendList;
            ObservableList<Group> groupList = FXCollections.observableArrayList();
            for (Group group : camtalksGroup) {
                SearchItemInfo searchItemInfo = new SearchItemInfo(group);
                boolean flag = true;
                if (text != null) {
                    flag = searchItemInfo.search(text);
                }
                if (flag) {
                    groupList.add(group);
                }
                grouplistView.setItems(groupList);
            }
        }

//        friendList = camtalks;
//        listView.refresh();
        return camtalkItemList;

    }

}
