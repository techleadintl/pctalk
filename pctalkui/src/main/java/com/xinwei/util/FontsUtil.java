package com.xinwei.util;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class FontsUtil {

	/**
	 * get font size for given component size
	 * 
	 * @param width_property     current component's width value
	 * @param height_property    current compoent's height value
	 * @param default_value      default value for current component's text size
	 * @param size_devide_factor size deciding factor
	 **/
	public static DoubleProperty getDynamicFontSize(ReadOnlyDoubleProperty width_property,
			ReadOnlyDoubleProperty height_property, double default_value, int size_devide_factor) {

		DoubleProperty font_size = new SimpleDoubleProperty(default_value);
		try {
			font_size.bind(width_property.add(height_property).divide(size_devide_factor));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return font_size;

	}

}
