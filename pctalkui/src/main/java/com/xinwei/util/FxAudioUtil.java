package com.xinwei.util;

import com.xinwei.talk.ui.chat.voice.VoiceManager;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

//Fixme - A similar abstract class exists in the package "com.xinwei.talk.ui.chat.voice.CaptureVoice" 
public class FxAudioUtil{

	public boolean isRecording = false;
	private static int MAX_CAPTURE_VOICE = 30000;
	boolean stopCapture = false;
	
	AudioFormat audioFormat;
	ByteArrayOutputStream byteArrayOutputStream;
	int totaldatasize = 0;

	TargetDataLine targetDataLine;
	AudioInputStream audioInputStream;
	SourceDataLine sourceDataLine;
	CaptureListenerThread listener;

	public void doCapture() throws Exception {
		System.out.println("Fx-Engine : Audio recording started");
		isRecording = true;
		audioFormat = VoiceManager.getAudioFormat();
		DataLine.Info dataLineInfo = new DataLine.Info(TargetDataLine.class, audioFormat);
		targetDataLine = (TargetDataLine) AudioSystem.getLine(dataLineInfo);
		targetDataLine.open(audioFormat);
		targetDataLine.start();
		new CaptureThread().start();
		listener = new CaptureListenerThread();
		listener.start();
	}

	//Fixme - Implement a method to pause/stop the playback.
	//Deprecated
	public void doPlay() {
		try {
			byte audioData[] = byteArrayOutputStream.toByteArray();
			InputStream byteArrayInputStream = new ByteArrayInputStream(audioData);
			AudioFormat audioFormat = VoiceManager.getAudioFormat();
			audioInputStream = new AudioInputStream(byteArrayInputStream, audioFormat, audioData.length / audioFormat.getFrameSize());
			DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, audioFormat);
			sourceDataLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
			sourceDataLine.open(audioFormat);
			sourceDataLine.start();
			new PlayThread().start();
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	public void doStop(Button button) {
		System.out.println("Fx-Engine : Audio recording stopped");
		stopCapture = true;
		isRecording = false;
		Image img = new Image("/images/mic.png", 20, 20, true, true);
		button.setGraphic(new ImageView(img));
	}

	public byte[] getAudioBytes() {
		return byteArrayOutputStream.toByteArray();
	}
	public void toFinish() {
		listener.interrupt();
		targetDataLine.close();
//		targetDataLine.stop(); Fixme - Throws a nullpointer exception 
		targetDataLine = null;	
	}
	
//	Fixme : find a way to exclude mic lines when muting 
	public static void muteSound(Boolean status) {
		Mixer.Info[] mixerInfos = AudioSystem.getMixerInfo();
		for (int i = 0; i < mixerInfos.length; i++) {
//			System.out.println("AudioSystem Name:" + mixerInfos.toString());
			Mixer mixer = AudioSystem.getMixer(mixerInfos[i]);	
			Line.Info[] targetLineInfos = mixer.getTargetLineInfo();
//			System.out.println("targets : " + targetLineInfos.length);
			for (int j = 0; j < targetLineInfos.length; j++) {
				setVolume(targetLineInfos[j], status);
			}
		}	
	}
	
	static void setVolume(Line.Info lineInfo, Boolean status) {
		System.out.println(lineInfo);
		Line line = null;
//		System.out.println(" open " + line.getLineInfo());
		try {
			line = AudioSystem.getLine(lineInfo);
			line.open();
			line.getControls(); /*SPECIAL LINE*/
			BooleanControl muteControl = (BooleanControl)line.getControl(BooleanControl.Type.MUTE);
			System.out.println(" mute " + muteControl.getValue());
			muteControl.setValue(status);
			line.close();
		}catch (IllegalArgumentException e) {
			line.close();
			System.out.println(" > FX engine : WARNING : " + e);
			return;
		}catch (LineUnavailableException e) {
			line.close();
			System.out.println(" > FX engine : WARNING : " + e);
			return;
		}
//		Fixme : Future enhancement - use float controller to have arbitary values 
		
/*		FloatControl control = 	(FloatControl)line.getControl(FloatControl.Type.VOLUME);
		control.setValue(control.getMinimum());
		FloatControl controlp = (FloatControl)line.getControl(FloatControl.Type.MASTER_GAIN);
		controlp.setValue(control.getMinimum());*/
		
		}

	class CaptureThread extends Thread {
		byte tempBuffer[] = new byte[10000];

		public void run() {
			byteArrayOutputStream = new ByteArrayOutputStream();
			totaldatasize = 0;
			stopCapture = false;
			try {
				while (!stopCapture) {
					int cnt = targetDataLine.read(tempBuffer, 0, tempBuffer.length);
					if (cnt > 0) {
						byteArrayOutputStream.write(tempBuffer, 0, cnt);
						totaldatasize += cnt;
					}
				}
				byteArrayOutputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			toFinish();
		}
	}

	class PlayThread extends Thread {
		byte tempBuffer[] = new byte[10000];

		public void run() {
			try {
				int cnt;
				while ((cnt = audioInputStream.read(tempBuffer, 0, tempBuffer.length)) != -1) {
					if (cnt > 0) {
						sourceDataLine.write(tempBuffer, 0, cnt);
					}
				}
				sourceDataLine.drain();
				sourceDataLine.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class CaptureListenerThread extends Thread {
		public void run() {
			try {
				sleep(MAX_CAPTURE_VOICE);
			} catch (Exception e) {
			}
			stopCapture = true;
		}
	}


}
