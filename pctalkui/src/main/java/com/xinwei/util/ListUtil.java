package com.xinwei.util;

import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Talk;
import javafx.collections.ObservableList;

/**
 * @author Sachini
 *
 */
public class ListUtil {

	/**
	 * @author Sachini
	 * @param list The search list
	 * @param jId  the JID of the person searched
	 * @return boolean - if the particular friend is in the list
	 */
	public static boolean isJidInTalkList(ObservableList<Talk> list, String jId) {
		if (list.stream().filter(o -> o.getJid().equals(jId)).findFirst().isPresent()) {
			return true;
		}
		return false;
	}

	/**
	 * check given JID is available inside the group list
	 * 
	 * @author nisal
	 * @param list current filtering group list
	 * @param jId  user given JID to find existing inside group list
	 **/
	public static boolean isJidInGroupList(ObservableList<Group> list, String jId) {
		if (list.stream().filter(o -> o.getJid().equals(jId)).findFirst().isPresent()) {
			return true;
		}
		return false;
	}
	
	public static Talk getTalkObjectOfGivenAvatar(ObservableList<Talk> list, String url) {
//		list.stream().filter(item -> (item.getAvatarUrl()!= null && item.getAvatarUrl().equals(url))).;
		
		return null;
	}

}
