package com.xinwei.util;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class ProgressUtil {

	/**
	 * create new view with progress view to show while server call in progress
	 * 
	 * @author nisal
	 **/
	public static VBox getProgressView() {
		VBox outer = new VBox();

		try {
			ProgressIndicator indicator = new ProgressIndicator();
			indicator.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
			indicator.setStyle("-fx-progress-color: white;");
			Label label = new Label("Please wait...");
			label.setStyle("-fx-text-fill: white;-fx-font-size: 16px;-fx-font-weight: bold;");
			label.setAlignment(Pos.CENTER);
			label.setWrapText(true);

			outer.setAlignment(Pos.CENTER);
			outer.getChildren().addAll(indicator, label);
			outer.setBackground(
					new Background(new BackgroundFill(Color.rgb(0, 0, 0, 0.5), new CornerRadii(0), new Insets(0))));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return outer;

	}

	/**
	 * hide progress view from UI
	 * 
	 * @author nisal
	 * @param container base container view that hold the progress view
	 **/
	public static void hideProgressView(StackPane container) {
		try {
			if (container != null && container.getChildren().size() > 1) {
				container.getChildren().remove(container.getChildren().size() - 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
