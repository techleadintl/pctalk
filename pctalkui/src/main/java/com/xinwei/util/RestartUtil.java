package com.xinwei.util;

import com.xinwei.Startup;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.loginwindow.LoginFrameController;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.conf.UserConfig;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Presence;

import java.io.File;
import java.io.IOException;

/**
 * @author Sachini
 *
 */
public class RestartUtil {

	public static void closeConnectionAndInvoke(String reason) {
		Log.info("closeConnectionAndInvoke");
		try {
			final XMPPConnection con = TalkManager.getConnection();

			if (con.isConnected()) {
				if (reason != null) {
					Presence byePresence = new Presence(Presence.Type.unavailable, reason, -1, null);
					con.disconnect(byePresence);
				} else {
					con.disconnect();
				}
			}

			if (!restartApplicationWithScript()) {
				restartApplicationWithJava();
			}
		} catch (Exception e) {
			Log.info("closeConnectionAndInvoke Error");
			Log.error(e.getMessage());

		}
		
	}
	
	
	public static boolean restartApplicationWithScript() {

		String command = null;
		try {
			if (SwingUtil.isWindows()) {
				String sparkExe = getCommandPath() + File.separator + Res.getString("short.name") + ".exe";
				if (!new File(sparkExe).exists()) {
					Log.warn("Client EXE file does not exist");
					return false;
				}
				String starterExe = getCommandPath() + File.separator + "starter.exe";
				if (new File(starterExe).exists()) {
					command = starterExe + " \"" + sparkExe + "\"";
				} else {
					command = sparkExe;
				}
			} else if (SwingUtil.isLinux()) {
				command = getCommandPath() + File.separator + Res.getString("short.name");
				if (!new File(command).exists()) {
					Log.warn("Client startup script does not exist");
					return false;
				}
			} else if (SwingUtil.isMac()) {
				command = "open -a " + Res.getString("short.name");
			}

			Runtime.getRuntime().exec(command);

			shutdown();
			return true;

		} catch (IOException e) {
			Log.error("Error trying to restart application with script", e);
			return false;
		} catch (Exception e) {
			Log.error(e);
			return false;
		}

	}

	public static boolean restartApplicationWithJava() {
		String javaBin = System.getProperty("java.home") + File.separatorChar + "bin" + File.separatorChar + "java";
		try {
			String toExec[] = new String[] { javaBin, "-cp", getClasspath(), Startup.class.getName() };
			Runtime.getRuntime().exec(toExec);
		} catch (Exception e) {
			Log.error("Error trying to restart application with java", e);
			shutdown();
			return false;
		}
		shutdown();
		return true;
	}

	private static String getCommandPath() throws IOException, Exception {
		return getLibDirectory().getParentFile().getCanonicalPath();
	}

	private static File getLibDirectory() throws IOException {
		File jarFile;

		try {

			jarFile = new File(Startup.class.getProtectionDomain().getCodeSource().getLocation().toURI()); // E:\Project\talk_pc\TalkCommon\bin

		} catch (Exception e) {
			Log.error("Cannot get jar file containing the startup class", e);
			return null;
		}
		if (jarFile.getName().endsWith(".exe")) { // for inno setup

			return new File(jarFile.getParent(), "lib");
		}
		if (!jarFile.getName().endsWith(".jar")) {

			Log.error("The startup class is not packaged in a jar file");
			return null;
		}
		File libDir = jarFile.getParentFile();
		return libDir;

	}

	public static void shutdown() {

		Stage stage = LoginFrameController.mainStage;
		
		stage.setOnHidden(new EventHandler<WindowEvent>() {
			UserConfig userConfig = TalkManager.getUserConfig();
			@Override
			public void handle(WindowEvent event) {
				if (userConfig.getCloseMainWindow()) {

					System.out.println("Decorator Pressed2");
					final XMPPConnection con = TalkManager.getConnection();

					if (con.isConnected()) {
						// Send disconnect.
						con.disconnect();
					}

					// Notify all MainWindowListeners

					// Close application.
					if (!Res.getBoolean("disable.exit"))
						System.exit(1);

				} else {
					stage.setIconified(true);
					System.out.println("Stage hide");
				}			
			}
		});
	}

	private static  String getClasspath() throws IOException {
		File libDir = getLibDirectory();
		String libPath = libDir.getCanonicalPath();
		String[] files = libDir.list();
		StringBuilder classpath = new StringBuilder();
		for (String file : files) {
			if (file.endsWith(".jar")) {
				classpath.append(libPath + File.separatorChar + file + File.pathSeparatorChar);
			}
		}
		return classpath.toString();
	}

}
