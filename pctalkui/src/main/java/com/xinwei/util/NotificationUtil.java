package com.xinwei.util;

import java.awt.*;
import java.awt.TrayIcon.MessageType;
import java.net.MalformedURLException;

public class NotificationUtil {
	public static void displayTray(String sender, String message) throws AWTException, MalformedURLException {
        SystemTray tray = SystemTray.getSystemTray();

        Image image = Toolkit.getDefaultToolkit().createImage("icon.png");
        
        //Alternative (if the icon is on the classpath):
        //Image image = Toolkit.getDefaultToolkit().createImage(getClass().getResource("icon.png"));

        TrayIcon trayIcon = new TrayIcon(image, "PCTalk");
        trayIcon.setImageAutoSize(true);
        trayIcon.setToolTip(sender +" sent you a message");
        tray.add(trayIcon);

        trayIcon.displayMessage(sender, message, MessageType.INFO);
    }
}
