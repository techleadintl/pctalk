package com.xinwei.util;

/**
 * this class will handle all size calculations of file
 * 
 * @author nisal
 **/
public class FileSizeUtil {

	/**
	 * get formatted file size in GB, MB, kB and B
	 * 
	 * @param fileSize file size in Bytes
	 **/
	public static String getFormattedFileSize(long fileSize) {

		try {

			int unit = 1000;
			if (fileSize < unit)
				return fileSize + " B";
			int exp = (int) (Math.log(fileSize) / Math.log(unit));
			String pre = ("kMGTPE").charAt(exp - 1) + "";
			return String.format("%.1f %sB", fileSize / Math.pow(unit, exp), pre);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "0 B";

	}

}
