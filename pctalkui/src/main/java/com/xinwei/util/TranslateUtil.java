package com.xinwei.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * @author Sachini PC
 *
 */
public class TranslateUtil {
	
	/**
	 * @param text
	 * @return Translated text either in Chinese or English
	 * @throws IOException
	 */
	public static String translate(String text) throws IOException{
		String from, to = "";
		
		//Check if string is an English Word
		boolean b = text.matches("^[\u0000-\u0080]+$");
		if(b) {
			from = "en";
			to = "zh-CN";
		}else {
			to = "en";
			from = "zh-CN";
		}
		
		StringBuilder urlString = new StringBuilder("https://script.google.com/macros/s/AKfycbzDXYAOuHxMHaxYJbhmkxPRG0cNcb0AyyvKbAgNsUKSGzIKHolb/exec");
		urlString.append("?q=")
				.append(URLEncoder.encode(text, "UTF-8"))
				.append("&target=")
				.append(to)
				.append("&source=")
				.append(from);
		
		URL url = new URL(urlString.toString());
		
		StringBuilder response = new StringBuilder();
		
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		
		String line;
		
		while((line = in.readLine())!=null){
			response.append(line);
		}
		
		in.close();
		
		return response.toString();
	}
}
