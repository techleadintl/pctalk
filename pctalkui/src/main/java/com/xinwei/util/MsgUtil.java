package com.xinwei.util;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.ImageIcon;

import com.xinwei.BackgroundStuff;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.cusview.TextChatView;
import com.xinwei.mainwindow.ChatRoom_Controller;
import com.xinwei.manager.EmojiManager;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.dao.TalkDao;
import com.xinwei.talk.listener.FileDownloadListener;
import com.xinwei.talk.manager.FileManager;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.MessageManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.model.XWObject;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.TalkSpecific;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.model.message.specific.TalkImageSpecific;
import com.xinwei.talk.model.message.specific.TalkNameCardSpecific;
import com.xinwei.talk.model.message.specific.TalkResourceSpecific;
import com.xinwei.talk.model.message.specific.TalkTextSpecific;
import com.xinwei.talk.model.message.specific.TalkVoiceSpecific;
import com.xinwei.talk.ui.plugin.emoticon.EmoticonManager;
import javafx.event.Event;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javax.swing.*;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class MsgUtil {

	static TalkDao dao = TalkManager.getTalkDao();

	private static Tooltip toolTipObj;

	public static Image getAvatar(String jId, boolean isMember) {
		Image image = null;
		if (isMember) {
			image = new Image(new ByteArrayInputStream(getAvatarImages(jId)), 50, 50, true, true);
		} else {
			VCard vCard = TalkManager.getVCardManager().getVCard();

			if (vCard.getAvatar() != null) {
				image = new Image(new ByteArrayInputStream(vCard.getAvatar()), 50, 50, true, true);
			}
		}
		return image;
	}

	public static byte[] getAvatarImages(String jid) {

		byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
		return talkAvatar;
	}

	public static void addTalkMessage(TalkMessage message, boolean isUser, ChatRoom_Controller chatRoom_Controller,
			String currentjid, String name) {

		try {
			List<TalkSpecific> msgList = message.getSpecificList();
			Date time = message.getTalkBody().getTime();
			if (msgList != null) {
				msgList = new ArrayList<>(msgList);

				for (TalkSpecific body : msgList) {

					if (body instanceof TalkTextSpecific) {

						String msg = ((TalkTextSpecific) body).getMsg();
						if (msg == null) {
							continue;
						}

						TextChatView container = new TextChatView(msg, message, currentjid, isUser);

						// added by Nisal to change the chat bubble width
						container.viewHolder.viewContainer.setPrefWidth(Control.USE_COMPUTED_SIZE);
						container.viewHolder.viewContainer.maxWidthProperty()
								.bind(chatRoom_Controller.chatPane.widthProperty().multiply(0.85));

						chatRoom_Controller.setMultiMsg(container.viewHolder.viewContainer, isUser, name, time,
								message);

					} else if (body instanceof TalkImageSpecific) {

						final TalkImageSpecific imageMsg = (TalkImageSpecific) body;

						chatRoom_Controller.receivedImageMsgHandler(imageMsg, isUser, message.getReaded(),
								message.getId(), name, time, message);

					} else if (body instanceof TalkVoiceSpecific) {

						TalkVoiceSpecific msg = (TalkVoiceSpecific) body;

						chatRoom_Controller.receivedAudioMsgHandler(msg, isUser, message.getReaded(), message.getId(),
								name, time, message);

					} else if (body instanceof TalkFileSpecific) {

						TalkFileSpecific fileMsg = (TalkFileSpecific) body;

						chatRoom_Controller.setReceivedFileOperation(fileMsg, isUser, name, time, message);

					} else if (body instanceof TalkNameCardSpecific) {

						TalkNameCardSpecific nameCardMsg = (TalkNameCardSpecific) body;

						ChatRoom_Controller.setNameCardOnRoom(nameCardMsg, isUser, name, time,
								message.getTalkBody().getFrom(), message);

					}
				}
			}
			chatRoom_Controller.setScrollToEnd();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getTimeNow() {
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return dt.format(new Date());
	}

	public static Date getStartTime(Date endtime) {
		Integer key = 1000;
		Calendar cal = Calendar.getInstance();
		cal.setTime(endtime);
		cal.add(Calendar.MONTH, 0 - key);
		return cal.getTime();
	}

	public static byte[] handleResourceMsg(int isRead, long msgId, TalkResourceSpecific resourceMsg, int msgType) {
		FileManager fileManager = new FileManager();
		final AtomicInteger trueNumber = new AtomicInteger();
		final AtomicInteger falseNumber = new AtomicInteger();
		final int updownType = isRead == TalkMessage.STATUS_UNREAD
				? com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DOWNLOAD
				: com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DB;

		fileManager.asynDownloadFile(msgId, msgType, resourceMsg.getUrl(), new FileDownloadListener() {
			@Override
			public void call(long msgId, int type, String url, byte[] data) {
				if (data == null) {
					falseNumber.incrementAndGet();
				} else {
					if (type == com.xinwei.talk.common.Constants.MSG_TYPE_IMG) {
					}
					resourceMsg.setData(data);
				}
				trueNumber.incrementAndGet();
			}
		}, updownType == com.xinwei.talk.common.Constants.UPDOWN_TYPE_HISTORY_DOWNLOAD);

		SystemUtil.waitForSize(trueNumber, 1, 300 * 1000);
		return resourceMsg.getData();
	}

	public static String getNameByIds(String groupId, String jId) {
		Member member = dao.getMemberByJid(BackgroundStuff.getCurrentGroupId(), jId);
		return null != member ? member.getMemNick() : "";
	}

	public static Image getAvatar(String currentJid) {

		Image image = null;

		image = new Image(new ByteArrayInputStream(getAvatarImages(currentJid)), 50, 50, true, true);

		return image;
	}

	/**
	 * @param item                 Received MessageType
	 * @param recentChatMsg        recent message Text component
	 * @param messageIconContainer
	 * @return
	 * @author Vaasu to print recent massage on chatlist
	 */
	public static Label printOnChatList(XWObject item, Label recentChatMsg, StackPane messageIconContainer) {
		recentChatMsg.setGraphic(null);
		recentChatMsg.setText("");
		if (messageIconContainer.getChildren() != null && messageIconContainer.getChildren().size() > 0) {
			messageIconContainer.getChildren().clear();
		}

		String msg = "";
//		if(BackgroundStuff.getActiveNode().equals("C")){
		TalkMessage message = dao.getLastMessage(item.getJid());
		System.out.println("====== Item Jid =====" + item.getJid());

		if (message != null && message.getTalkBody().getSpecificList().size() > 0) {
			TalkSpecific talkSpecific = message.getTalkBody().getSpecificList().get(0);

			if (talkSpecific != null && talkSpecific instanceof TalkTextSpecific) {
				msg = ((TalkTextSpecific) talkSpecific).getMsg();
				List<Object> textAndEmoticon = EmoticonManager.getInstance().getTextAndEmoticon(msg);
				HBox x = new HBox();
				if (textAndEmoticon != null) {

					for (Object obj : textAndEmoticon) {
						if (obj instanceof String) {
							Label lbl = new Label();
							lbl.getStyleClass().add("recent_chat_msg");
							lbl.setText(obj.toString());
							x.getChildren().add(lbl);
						} else if (obj instanceof ImageIcon) {
							Image img = EmojiManager.getInstance().getEmoticon(obj.toString());
							ImageView p = new ImageView(img);
							p.setFitWidth(15);
							p.setFitHeight(15);
//                            recentChatMsg.setText(msg);
							x.getChildren().add(p);
							x.setAlignment(Pos.CENTER);
//                            recentChatMsg.setGraphic(x);

							// return recentChatMsg = new Label(msg, p);
						}
					}
				}
//                recentChatMsg.setText(msg);
				recentChatMsg.setGraphic(x);
				System.out.println("=====Last Text msg======" + msg);
			} else if (talkSpecific != null && talkSpecific instanceof TalkFileSpecific) {
				msg = "FILE ICON WILL BE REPLACED";
				recentChatMsg.setText(((TalkFileSpecific) talkSpecific).getFilename());
				messageIconContainer.getChildren().add(new MsgUtil().getAttachmentIcon());
			} else if (talkSpecific != null && talkSpecific instanceof TalkImageSpecific) {
				msg = "IMAGE ICON WILL BE REPLACED";
				recentChatMsg.setText("Photo Received " + ((TalkImageSpecific) talkSpecific).getFilename());
			} else if (talkSpecific != null && talkSpecific instanceof TalkVoiceSpecific) {
				msg = "AUDIO ICON WILL BE REPLACED";
				recentChatMsg.setText("Audio Message Received");
				messageIconContainer.getChildren().add(new MsgUtil().getAudioIcon());
			} else if (talkSpecific != null && talkSpecific instanceof TalkNameCardSpecific) {
				msg = "AUDIO ICON WILL BE REPLACED";
				recentChatMsg.setText("Name Card Received");
				messageIconContainer.getChildren().add(new MsgUtil().getNameCardIcon());
			}
		}
//		}
		return recentChatMsg;
	}

	public ImageView getAudioIcon() {
		Image image = new Image(getClass().getResourceAsStream("/images/mic.png"), 20, 20, true, true);
		ImageView imageView = new ImageView(image);
		imageView.setFitWidth(16);
		imageView.setFitHeight(16);
		return imageView;
	}

	public ImageView getNameCardIcon() {
		Image image = new Image(getClass().getResourceAsStream("/images/member_2.png"), 20, 20, true, true);
		ImageView imageView = new ImageView(image);
		imageView.setFitWidth(16);
		imageView.setFitHeight(16);
		return imageView;
	}

	public ImageView getAttachmentIcon() {
		Image image = new Image(getClass().getResourceAsStream("/images/attachment-small.png"), 20, 20, true, true);
		ImageView imageView = new ImageView(image);
		imageView.setFitWidth(16);
		imageView.setFitHeight(16);
		return imageView;
	}

	public static void updateChatRoom(String name, String id, ChatRoom_Controller chatRoom_Controller) {
		long maxId = 0, minId = 0;
		try {

			chatRoom_Controller.clearChatRoom();
			chatRoom_Controller.setChatMember(name);

			final MessageManager historyManager = TalkManager.getMessageManager();

			Date searchEndTime = TalkManager.getXmppService().getServerDate();

			Date searchStartTime = MsgUtil.getStartTime(searchEndTime);

			Map<String, Object> messageTimeInfo = historyManager.getMessageTimeInfo(id, null, searchStartTime,
					searchEndTime);

			Object maxObj = messageTimeInfo.get("MAXTID");
			Object minObj = messageTimeInfo.get("MINID");

			if (maxObj != null && minObj != null) {
				maxId = ((Number) maxObj).longValue();
				minId = ((Number) minObj).longValue();
			}

			VCard vCard = TalkManager.getVCardManager().getVCard();

			List<? extends TalkMessage> talkMessageList = historyManager.getMessageList(id, null, maxId + 1, 100,
					false);
			if (talkMessageList != null && !(talkMessageList.isEmpty())) {
				for (TalkMessage transcriptMessage : talkMessageList) {

					Platform.runLater(() -> {

						if ((transcriptMessage.getTalkBody().getFrom()).equals(vCard.getTel())) {
							MsgUtil.addTalkMessage(transcriptMessage, false, chatRoom_Controller,
									BackgroundStuff.getCurrentJid(), vCard.getNick());
						} else {

							MsgUtil.addTalkMessage(transcriptMessage, true, chatRoom_Controller,
									BackgroundStuff.getCurrentJid(), "");
						}
					});
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
