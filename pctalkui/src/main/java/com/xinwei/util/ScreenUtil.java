package com.xinwei.util;

import javafx.geometry.Bounds;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.stage.Screen;

import java.util.HashMap;

import com.xinwei.constants.ScreenSizes;

public class ScreenUtil {
	public static final String LOGIN_SCREEN_DIMENSIONS_WIDTH = "LOGIN_SCREEN_DIMENSIONS_WIDTH";
	public static final String LOGIN_SCREEN_DIMENSIONS_HEIGHT = "LOGIN_SCREEN_DIMENSIONS_HEIGHT";
	
	/**
	 * added by charith
	 */
	public static final String MAIN_SCREEN_DIMENTIONS_HEIGHT = "MAIN_SCREEN_DIMENTIONS_HEIGHT";
	public static final String MAIN_SCREEN_DIMENTIONS_WIDTH= "MAIN_SCREEN_DIMENTIONS_WIDTH";

	/**
	 * get width and height for login screen
	 * 
	 * @author nisal
	 **/
	public static HashMap<String, Double> getLoginScreenDimensions() {
		HashMap<String, Double> map = new HashMap<>();

		try {

			Screen screen = Screen.getPrimary();
			Rectangle2D bounds = screen.getVisualBounds();
			double width = bounds.getWidth() * 0.770;
			double height = bounds.getHeight() * 0.850;

			map.put(ScreenUtil.LOGIN_SCREEN_DIMENSIONS_HEIGHT, height);
			map.put(ScreenUtil.LOGIN_SCREEN_DIMENSIONS_WIDTH, width);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * get given UI component's x position in the current displaying screen
	 * 
	 * @author nisal
	 * @param node current selected UI component
	 **/
	public static double getScreenXPostionOfNode(Node node) {
		try {
			Bounds bounds = node.getBoundsInLocal();
			Bounds screenBounds = node.localToScreen(bounds);

			return screenBounds.getMinX();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	/**
	 * get given UI component's y position in the current displaying screen
	 * 
	 * @author nisal
	 * @param node current selected UI component
	 **/
	public static double getScreenYPostionOfNode(Node node) {
		try {
			Bounds bounds = node.getBoundsInLocal();
			Bounds screenBounds = node.localToScreen(bounds);

			return screenBounds.getMinY();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}
	
	
	/**
	 * get height and width of Main screen
	 * @author Charith
	 */
	public static HashMap<String, Double> getMainScreenDimentions(){
		HashMap<String, Double> map = new HashMap<>();
		
		Screen screen = Screen.getPrimary();
		Rectangle2D bounds = screen.getVisualBounds();
		
		double width = bounds.getWidth() * 0.770;
		if (width < ScreenSizes.INITIAL_WINDOWS_WIDTH.getValue())
			width = ScreenSizes.INITIAL_WINDOWS_WIDTH.getValue();
		double height = bounds.getHeight() * 0.850;
		
		map.put(ScreenUtil.MAIN_SCREEN_DIMENTIONS_HEIGHT, height);
		map.put(ScreenUtil.MAIN_SCREEN_DIMENTIONS_WIDTH, width);
		return map;
		
	}

}
