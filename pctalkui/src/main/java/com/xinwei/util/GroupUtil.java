package com.xinwei.util;

import java.util.List;

import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.Group;
import com.xinwei.talk.model.Member;
import com.xinwei.talk.model.message.TalkMessage;

public class GroupUtil {

	/**
	 * create valid group name from given group details
	 * 
	 * @author nisal
	 * @param group        current group object
	 * @param groupMembers all members in current selected group
	 **/
	public static String createValidGroupName(Group group, List<Member> groupMembers) {

		String groupName = "";

		try {

			groupName = group.getName();

			if ((groupName == null || groupName.isEmpty()) && groupMembers != null && groupMembers.size() > 0) {
				for (Member member : groupMembers) {
					if (groupName == null || groupName.isEmpty())
						groupName = member.getMemNick();
					else
						groupName = String.format("%s, %s", groupName, member.getMemNick());
				}
			}
			if (groupName == null || groupName.isEmpty())
				groupName = group.getJid();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return groupName;
	}

	/**
	 * add valid group name, new msg count and group member details to given group
	 * 
	 * @author nisal
	 * @param group current selected group object
	 **/
	public static void updateGroupWithData(Group group) {
		try {

			// added by Nisal to get all available members in given group
			List<Member> groupMembers = TalkManager.getTalkDao().getMemberList(group.getId());
			group.setGroupMembers(groupMembers);

			// added by Nisal to create a valid group name
			String groupName = GroupUtil.createValidGroupName(group, groupMembers);
			group.setName(groupName);

			// get the unread message count to show in the UI
			List<TalkMessage> unread_msgs = TalkManager.getTalkDao().getUnReadMessageList(group.getJid(), TalkMessage.STATUS_UNREAD);
			group.setNewMessageCount((unread_msgs == null) ? 0 : unread_msgs.size());
			group.setHasMsg(unread_msgs != null && unread_msgs.size() > 0);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
