package com.xinwei.util;

import com.xinwei.talk.manager.ImageManager;
import javafx.scene.image.Image;

import java.io.ByteArrayInputStream;

public class ImageUtils {
	public static Image getImg(String jid) {
        Image img = new Image(new ByteArrayInputStream(getAvatarImages(jid)), 50, 50, true, true);
        return img;
    }
	
	private static byte[] getAvatarImages(String jid) {
	        byte[] talkAvatar = ImageManager.getTalkAvatar(jid, true);
	        return talkAvatar;
	}
}
