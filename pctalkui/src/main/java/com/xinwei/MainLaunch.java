package com.xinwei;

import com.xinwei.constants.ScreenSizes;
import com.xinwei.loginwindow.LoginFrameController;
import com.xinwei.talk.common.Log;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainLaunch extends Application {

	LoginFrameController loginFrameController;

	private static Stage mainStage;
	Scene scene;
	public static Scene mainScene;

	public static Stage getStage() {
		return mainStage;
	}

	private double xOffset = 0;
	private double yOffset = 0;

	private CooTalkStartup engine = null;

	public static void main(String args[]) {
		launch();
	}

	@Override
	public void init() {
		/*// added by Nisal to create initial DB creation method call
		TalkManager.getAppDao();*/

		engine = new CooTalkStartup();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		mainStage = primaryStage;
		try {
			engine.startup();

			Platform.runLater(new Runnable() {

				@Override
				public void run() {

					try {
						primaryStage.initStyle(StageStyle.TRANSPARENT);
						FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/LoginFrame.fxml"));
						Parent root = (Parent) loader.load();
						loginFrameController = (LoginFrameController) loader.getController();
						// loginFrameController.titleImage();//removed by nisal

						Rectangle2D bounds = Screen.getPrimary().getVisualBounds();

						root.setOnMousePressed(new EventHandler<MouseEvent>() {

							@Override
							public void handle(MouseEvent event) {
								xOffset = event.getSceneX();
								yOffset = event.getSceneY();

							}
						});

						root.setOnMouseDragged(new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent event) {
								primaryStage.setX(event.getScreenX() - xOffset);
								primaryStage.setY(event.getScreenY() - yOffset);
							}
						});

						/*
						 * primaryStage.setWidth(bounds.getWidth()*0.650);
						 * primaryStage.setHeight(bounds.getHeight()*0.850);
						 */

						// added by nisal
						double width = ScreenSizes.INITIAL_WINDOWS_WIDTH.getValue();
						double height = ScreenSizes.INITIAL_WINDOWS_HEIGHT.getValue();

						primaryStage.setWidth(width);
						primaryStage.setHeight(height);

						scene = new Scene(root, 652, 550);
						// scene.getStylesheets().add("/styles/TitleBar.css");
						primaryStage.setScene(scene);
						primaryStage.show();

						primaryStage.setX((bounds.getWidth() - primaryStage.getWidth()) / 2);
						primaryStage.setY((bounds.getHeight() - primaryStage.getHeight()) / 2);
						primaryStage.getIcons()
								.add(new Image(this.getClass().getResourceAsStream("/images/logo3x.png")));

						System.out.println(" > FX-Launcher : initiation completed ");
					} catch (Exception e) {
						Log.error(e);
						e.printStackTrace();
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
