package com.xinwei.manager;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.xinwei.common.lang.StringUtil;

import java.util.Set;

import javafx.scene.image.Image;

public class EmojiManager {

	private static EmojiManager singleton;
	private Map<String, Image> emoticonMap = new LinkedHashMap<String, Image>();
	
	public static synchronized EmojiManager getInstance() {
		if (null == singleton) {
			singleton = new EmojiManager();
		}
		return singleton;
	}
	
	public Image getEmoticon(String description) {
		return emoticonMap.get(description);
	}
	
	public List<Object> getTextAndEmoticon(String content) {
		if (content == null)
			return null;
		Set<String> keywords = emoticonMap.keySet();

		StringBuffer regExp = new StringBuffer("(");
		for (String keyword : keywords) {
			regExp.append(StringUtil.escapeExprSpecialWord(keyword)).append("|");
		}
		regExp.deleteCharAt(regExp.length() - 1).append(")");

		Matcher matcher = Pattern.compile(regExp.toString(), Pattern.CASE_INSENSITIVE).matcher(content);

		List<Object> result = new ArrayList<Object>();
		int start = 0;
		while (matcher.find()) {
			int start1 = matcher.start();
			int end1 = matcher.end();

			if (start < start1) {
				result.add(content.substring(start, start1));
			}
			result.add(emoticonMap.get(matcher.group(1)));

			start = end1;
		}
		if (start < content.length()) {
			result.add(content.substring(start, content.length()));
		}

		return result;
	}

	private EmojiManager() {
		
		Map<String, String> map = new LinkedHashMap<>();
		map.put("talk/images/emoticon/ee_1.png", "[):]");
		map.put("talk/images/emoticon/ee_2.png", "[:D]");
		map.put("talk/images/emoticon/ee_3.png", "[),)]");
		map.put("talk/images/emoticon/ee_4.png", "[:-o]");
		map.put("talk/images/emoticon/ee_5.png", "[:p]");
		map.put("talk/images/emoticon/ee_6.png", "[(H)]");
		map.put("talk/images/emoticon/ee_7.png", "[:@]");
		map.put("talk/images/emoticon/ee_8.png", "[:s]");
		map.put("talk/images/emoticon/ee_9.png", "[:$]");
		map.put("talk/images/emoticon/ee_10.png", "[:(]");
		map.put("talk/images/emoticon/ee_11.png", "[:'(]");
		map.put("talk/images/emoticon/ee_12.png", "[:|]");
		map.put("talk/images/emoticon/ee_13.png", "[(a)]");
		map.put("talk/images/emoticon/ee_14.png", "[8o|]");
		map.put("talk/images/emoticon/ee_15.png", "[8-|]");
		map.put("talk/images/emoticon/ee_16.png", "[+o(]");
		map.put("talk/images/emoticon/ee_17.png", "[<o)]");
		map.put("talk/images/emoticon/ee_18.png", "[|-)]");
		map.put("talk/images/emoticon/ee_19.png", "[*-)]");
		map.put("talk/images/emoticon/ee_20.png", "[:-#]");
		map.put("talk/images/emoticon/ee_21.png", "[:-*]");
		map.put("talk/images/emoticon/ee_22.png", "[^o)]");
		map.put("talk/images/emoticon/ee_23.png", "[8-)]");
		map.put("talk/images/emoticon/ee_24.png", "[(|)]");
		map.put("talk/images/emoticon/ee_25.png", "[(u)]");
		map.put("talk/images/emoticon/ee_26.png", "[(S)]");
		map.put("talk/images/emoticon/ee_27.png", "[(*)]");
		map.put("talk/images/emoticon/ee_28.png", "[(#)]");
		map.put("talk/images/emoticon/ee_29.png", "[(R)]");
		map.put("talk/images/emoticon/ee_30.png", "[({)]");
		map.put("talk/images/emoticon/ee_31.png", "[(})]");
		map.put("talk/images/emoticon/ee_32.png", "[(k)]");
		map.put("talk/images/emoticon/ee_33.png", "[(F)]");
		map.put("talk/images/emoticon/ee_34.png", "[(W)]");

		Set<Entry<String, String>> entrySet = map.entrySet();
		for (Entry<String, String> entry : entrySet) {
			emoticonMap.put(entry.getValue(), new Image(entry.getKey()));
		}
	}
	
	
	
}
