package com.xinwei.manager;

import com.xinwei.common.lang.SwingUtil;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.http.service.HttpFileService;
import com.xinwei.http.service.file.OutFileTransfer;
import com.xinwei.http.service.file.TransferStatus;
import com.xinwei.spark.TaskEngine;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.MessageUtil;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.manager.TransferManager;
import com.xinwei.talk.model.message.TalkBody;
import com.xinwei.talk.model.message.TalkMessage;
import com.xinwei.talk.model.message.specific.TalkFileSpecific;
import com.xinwei.talk.ui.chat.room.TalkChatRoom;

import java.io.File;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by VAASU on 7/26/2018. TO SEND FILE
 *
 */
public class FxTransferManager extends TransferManager {

	public boolean isGroup = false;

	public FxTransferManager(boolean isGroup) {
		this.isGroup = isGroup;
	}

	public String sendFileFromFx(File file, String jid) {
		long maxsize = Res.getLong("file.transfer.maximum.size", -1L);
		long warningsize = Res.getLong("file.transfer.warning.size", -1L);
		if (file.length() >= maxsize && maxsize != -1) {
			String maxsizeString = SwingUtil.getAppropriateByteWithSuffix(maxsize);
			String yoursizeString = SwingUtil.getAppropriateByteWithSuffix(file.length());
			String output = Res.getMessage("error.chatroom.fileupload.toobig", maxsizeString, yoursizeString);
			return output;
		}
		if (file.length() >= warningsize && warningsize != -1) {
			return "FX-Engine : WARNING - file legnth ";
		}
		// Create the outgoing file transfer
		final OutFileTransfer transfer = new OutFileTransfer(file);
		try {
			this.sendFile(transfer, jid);
		} catch (NullPointerException e) {
			Log.error(e);
		}
		return "FX-Engine : File is processing ";
	}

	public String sendFile(OutFileTransfer transfer, String jid) {
		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				try {
					Object obj = TalkUtil.getObject(jid);
					System.out.println("FX-Engine : New TaskEngine instance created to upload files into the server ");
					HttpFileService fileService = TalkManager.getFileService();
					HttpResponseInfo httpResponseInfo = fileService.uploadChatFile(obj, transfer);
					String fileAbsolutePath = fileService.getFileUrl(httpResponseInfo);
					System.out.println("Fx-Engine : Upload comepleted - Url " + fileAbsolutePath);
					doFinish(transfer, fileAbsolutePath, jid);
				} catch (Exception e) {
					transfer.setStatus(TransferStatus.error);
					Log.error(e);
					return;
				}
			}
		});

		// removed by Nisal (trying to update old swing components)
//        SwingWorker worker = new SwingWorker() {
//            public Object construct() {
//                while (true) {
//                    try {
//                        long starttime = System.currentTimeMillis();
//                        long startbyte = transfer.getBytesSent();
//                        Thread.sleep(500);
//                        TransferStatus status = transfer.getStatus();
//                        if (status == TransferStatus.error || status == TransferStatus.complete || status == TransferStatus.cancelled) {
//                            break;
//                        }
//                        long endtime = System.currentTimeMillis();
//                        long endbyte = transfer.getBytesSent();
//
//                        long timediff = endtime - starttime;
//                        long bytediff = endbyte - startbyte;
//
//                        long leftbyte = transfer.getContentLength() - transfer.getBytesSent();
//                        updateBar(transfer, leftbyte, bytediff, timediff,jid);
//                    } catch (InterruptedException e) {
//                        Log.error("Unable to sleep thread.", e);
//                    }
//                }
//                return "";
//            }
//            public void finished() {
//                updateBar(transfer,0, 0, 0,jid);
//            }
//        };
//        worker.start();
		return "FX-Engine : File is still processing ";
	}

	private String updateBar(final OutFileTransfer transfer, long leftbyte, long bytediff, long timediff, String jid) {
		System.out.println("FX-Engine : Update bar method is executing ");
		String updateResponse = null;
		TransferStatus status = transfer.getStatus();
		if (status == TransferStatus.initial) {
			// titleLabel.setText(Res.getString("message.negotiation.file.transfer",
			// nickname));
		} else if (status == TransferStatus.in_progress) {
			updateResponse = "IN PROGRESS";
		} else if (status == TransferStatus.cancelled) {
			doFinish(transfer, null, jid);
		} else if (status == TransferStatus.error) {
			if (transfer.getException() != null) {
				Log.error("Error occured during file transfer.", transfer.getException());
			}
			doFinish(transfer, null, jid);
		}
		return updateResponse;
	}

	protected void doFinish(final OutFileTransfer transfer, final String url, String jid) {
		com.xinwei.spark.SwingWorker writeImageThread = new com.xinwei.spark.SwingWorker() {
			public Object construct() {
				return true;
			}

			public void finished() {
				if (transfer.getStatus() == TransferStatus.complete) {
					doSendComplete(transfer, url, jid);
				} else if (transfer.getStatus() == TransferStatus.cancelled) {
					doSendCancel(transfer, url, jid);
				} else if (transfer.getStatus() == TransferStatus.error) {
					doSendCancel(transfer, url, jid);
				}
			}

			private void doSendCancel(final OutFileTransfer transfer, final String url, String jid) {
				sendFile(url, transfer.getFile(), false, jid);
			}

			private void doSendComplete(final OutFileTransfer transfer, final String url, String jid) {
				sendFile(url, transfer.getFile(), true, jid);
			}
		};
		writeImageThread.start();
	}

	public void sendFile(String url, File file, boolean complete, String jid) {
		TalkChatRoom talkroom = new TalkChatRoom(jid, "name", "destination");
		TalkBody bodyMessage = new TalkBody(talkroom.getMessageFrom(), talkroom.getMessageTo());
//        setSendParam(bodyMessage);
		TalkFileSpecific fileMsg = new TalkFileSpecific(url, file.getName(), file.length());
		fileMsg.setFileAbsolutePath(file.getAbsolutePath());
		if (complete) {
			fileMsg.setFileType(TalkFileSpecific.SEND_COMPLETE);
		} else {
			fileMsg.setFileType(TalkFileSpecific.SEND_CANCEL);
		}
		bodyMessage.addSpecific(fileMsg);
		final TalkMessage talkMessage = getTalkMessage(null, bodyMessage, TalkMessage.TYPE_SEND, null, jid);

		if (complete) {
			doSendTalkMessage(talkMessage, jid);
		} /*
			 * else { insertRealTimeMessage(talkMessage); }
			 */
	}

	private TalkMessage getTalkMessage(String packetId, TalkBody bodyMessage, int transcriptMessageType, Date time,
			String jid) {
		TalkMessage m = new TalkMessage();
		m.setId(TalkManager.getMessageManager().nextMessageId());
		m.setJid(jid);
		m.setSendOrReceive(transcriptMessageType);
		m.setReaded(TalkMessage.STATUS_READED);
		m.setIsGroup(isGroup);
		m.setTalkBody(bodyMessage);
		bodyMessage.setTime(time);
		bodyMessage.setPacketId(packetId);
		return m;
	}

	private void doSendTalkMessage(TalkMessage talkMessage, String jid) {
		String packetId = talkMessage.getTalkBody().getPacketId();
		if (packetId == null) {
			packetId = TalkManager.getMessageManager().nextPackageId();
			talkMessage.getTalkBody().setPacketId(packetId);
		}
		fireMessageSent(talkMessage);
		final AtomicInteger sendAtomicInteger = new AtomicInteger();
		TaskEngine.getInstance().submit(new Runnable() {
			@Override
			public void run() {
				boolean result = MessageUtil.sendBalloonMessage(talkMessage, TalkManager.getSessionManager().getJID(),
						jid);
				if (result) {
					System.out.println("send file ok");
				} else {
					System.out.println("send file error");
				}
				sendAtomicInteger.incrementAndGet();
			}
		});

	}

	private void fireMessageSent(TalkMessage message) {
		ListenerManager.fireMessageSent(message.getJid(), message);
	}

}
