package com.xinwei.screenshot;

import java.awt.EventQueue;
import java.awt.Robot; 
import java.awt.image.BufferedImage; 
import java.io.File; 
import java.util.logging.Level; 
import java.util.logging.Logger; 
import javafx.application.Application; 
import javafx.application.Platform; 
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor; 
import javafx.scene.Group; 
import javafx.scene.Node; 
import javafx.scene.Scene; 
import javafx.scene.input.MouseEvent; 
import javafx.scene.paint.Color; 
import javafx.scene.shape.Rectangle; 
import javafx.stage.Screen; 
import javafx.stage.Stage; 
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;

import com.xinwei.photoalbum.MainClass;
 
public class ScreenCapture extends Application { 
 
 private static ScreenCapture screenCapture; 
 private Stage stage; 
 public Rectangle screenBounds; 
 private Rebounder rebounder; 
 private GlassPane glassPane; 
 
 public static ScreenCapture getScreenCapture() { 
 
  return screenCapture; 
 } 
 
 @Override 
 public void start(Stage primaryStage) { 
 
  screenCapture = this; 
 
  stage = primaryStage; 
  Group group = new Group(); 
  Scene scene = new Scene(group); 
  scene.setFill(Color.TRANSPARENT); 
  scene.setCursor(Cursor.CROSSHAIR); 
 
  primaryStage.initStyle(StageStyle.TRANSPARENT); 
  //primaryStage.setFullScreen(true); 
  primaryStage.setScene(scene); 
  Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

  //set Stage boundaries to visible bounds of the main screen
  primaryStage.setX(primaryScreenBounds.getMinX());
  primaryStage.setY(primaryScreenBounds.getMinY());
  primaryStage.setWidth(primaryScreenBounds.getWidth());
  primaryStage.setHeight(primaryScreenBounds.getHeight());
  primaryStage.show(); 
 
  screenBounds = new Rectangle( 
    Screen.getPrimary().getBounds().getWidth(), Screen.getPrimary() 
      .getBounds().getHeight()); 
 
  KeyPane keyPane = new KeyPane(primaryStage, rebounder = new Rebounder()); 
  glassPane = new GlassPane(); 
  glassPane.setShape(rebounder.shapeBuilder(null), true); 
 
  group.getChildren().addAll( 
    new Node[] { (Node) keyPane, (Node) glassPane, 
      (Node) rebounder.getLasso() }); 
 
  scene.setOnMousePressed(new EventHandler<MouseEvent>() { 
 
   @Override 
   public void handle(MouseEvent me) { 
 if(me.isDragDetect()==false) {
	 System.out.println("Not detected drag area");
 }
    glassPane.setShape( 
      rebounder.shapeBuilder(rebounder.start(me.getX(), 
        me.getY())), false); 
   } 
  }); 
 
  scene.setOnMouseDragged(new EventHandler<MouseEvent>() { 
 
   @Override 
   public void handle(MouseEvent me) { 
	   if(me.isDragDetect()==false) {
			 System.out.println("Not detected drag area after drag");
		 }
    glassPane.setShape(rebounder.shapeBuilder(rebounder.rebound( 
      me.getX() + 1, me.getY() + 1)), false); 
   } 
  }); 
 
  scene.setOnMouseReleased(new EventHandler<MouseEvent>() { 
 
   @Override 
   public void handle(MouseEvent me) { 
 
    if (!rebounder.isStopped()) { 
    	if(me.isDragDetect()==false) {
			 System.out.println("Not detected drag area after drag release");
		 }
     capture(rebounder.stop(me.getX() + 1, me.getY() + 1)); 
    } 
   } 
  }); 
 } 
 
 public void capture(final Rectangle finished) { 
 
  stage.setWidth(1); 
  stage.setHeight(1); 
  EventQueue.invokeLater(new Runnable() { 
 
   @Override 
   public void run() { 
 
    if ((((finished.getWidth() - 1) * (finished.getHeight() - 1)) > 0.0d)) { 
    	
     try { 
      Robot robot = new Robot(); 
      BufferedImage img = robot.createScreenCapture(new java.awt.Rectangle((int) finished.getX(), (int) finished.getY(), 
    		              (int) finished.getWidth() - 1, (int) finished.getHeight() - 1));
      
      File folder = new File(System.getProperty("user.home")+ "/AppData/Local/CamTalk/log", "screenshots");
     
      folder.mkdirs(); 
      File file = File.createTempFile("CooTalkPC_img", ".jpg", folder); 
      ImageIO.write(img, "jpg", file); 
      
      Platform.runLater(new Runnable() { 
     	 
          @Override 
          public void run() { 
      try {
    	  
     	 stage.hide();
     	 new MainClass().start(new Stage());
     	 
     } catch (Exception e) {
     	
     	e.printStackTrace();
     }
           glassPane.setShape(rebounder.shapeBuilder(null), true); 
           stage.setWidth(screenBounds.getWidth()); 
           stage.setHeight(screenBounds.getHeight()); 
          } 
         });
     
     } catch (Exception ex) { 
    	 
      Logger.getLogger(ScreenCapture.class.getName()).log(Level.SEVERE, null, ex);
      
     } 
 
    }else {
    	
        Platform.runLater(new Runnable() {
    		
    		@Override
    		public void run() {
    			stage.hide();
    			new ScreenCapture().start(new Stage());
    		}
    	});
    }  
   } 
  }); 
 } 
 
 public void screenShot() { 
 
  launch(); 
 } 
}

