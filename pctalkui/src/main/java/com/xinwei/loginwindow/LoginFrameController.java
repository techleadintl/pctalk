package com.xinwei.loginwindow;

/**
 * @author Srinath
 * 
 **/

import java.awt.Desktop;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXListCell;
import com.xinwei.MainLaunch;
import com.xinwei.common.lang.SystemUtil;
import com.xinwei.common.lookandfeel.component.McWillCheckLabel;
import com.xinwei.constants.ScreenSizes;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.Res;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.model.VCard;
import com.xinwei.talk.ui.login.LoginDialog;
import com.xinwei.uiutils.ComboBox;
import com.xinwei.util.ScreenUtil;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

public class LoginFrameController implements Initializable {
	
	static LoadingFrameController loadingFrameController;

	@FXML
	private BorderPane login_frame, loading_frame;

	@FXML
	private TextField countryCodeText;

	@FXML
	private HBox signLabelPane;

	@FXML
	private Button signIn_loging, minimize, close, forgot_my_password, create_one;

	@FXML
	JFXComboBox<String>  cmb,country_cmb,mobile_number_loging;

	@FXML
	private PasswordField password_loging;

	@FXML
	private CheckBox save_password, auto_login;

	@FXML
	private Label signLabel;

	public static Scene resetPwScene;

	@FXML
	private StackPane loading/* , background */;

	@FXML
	private Label errorMsg; // Added to replace Alert - Sachini

	/**
	 * @author nisal
	 **/
	@FXML
	private VBox centerContainer, centerMainContainer, footer_container;
	@FXML
	private StackPane login_header;
	@FXML
	private ImageView header_img,img;

	public static Stage mainStage = new Stage();

	LoginFrameController loginFrameController;

	Scene scene;
	ComboBox comboBoxUtil = new ComboBox();

	private McWillCheckLabel savePasswordCheck, autoLoginCheck;

	public static Stage getStage() {
		return mainStage;
	}

	@FXML
	public void handleCloseMinimize(ActionEvent event) {

		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {
				if (!Res.getBoolean("disable.exit"))
					System.exit(1);

			} else if (button.equals(minimize)) {

				Stage stage = (Stage) minimize.getScene().getWindow();
				stage.setIconified(true);

			}
		}
	}

	public void shutdown() {

		if (!Res.getBoolean("disable.exit"))
			System.exit(1);
	}

	@FXML
	public void registerButton(ActionEvent event) {

		try {
			URI uri = new URI(Res.getProperty("talk.register"));
			Desktop.getDesktop().browse(uri);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * multiple users login
	 * 
	 * Srinath
	 * 
	 **/
	@FXML
	public void signInButton(ActionEvent e) throws Exception {

		if (country_cmb.getSelectionModel().isEmpty() || mobile_number_loging.getValue().isEmpty()
				|| password_loging.getText().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Login");
			alert.setHeaderText(null);
			alert.setContentText("Please Fill All Fields !");
			ButtonType buttonTypeOne = new ButtonType("Ok");

			alert.getButtonTypes().setAll(buttonTypeOne);
			alert.showAndWait();

		} else {

			try {
				startLoginProcess();

				Platform.runLater(new Runnable() {
					@Override
					public void run() {

						getLoadingImage();
					}
				});

			} catch (Exception e1) {
				Log.error(e1);
				e1.printStackTrace();
			}
		}
	}

	public void startLoginProcess() {

		Runnable task = new Runnable() {
			public void run() {
				startLogin();
			}
		};

		Thread backgroundThread = new Thread(task);
		backgroundThread.setDaemon(true);
		backgroundThread.start();
	}

	public void backToLoginFrame(int response, FileOutputStream lOCK, String tel) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {

				try {
					

					FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/LoginFrame.fxml"));
					Parent root = (Parent) loader.load();

					loginFrameController = loader.getController();
					// loginFrameController.titleImage();//removed by nisal

					ComponentsOrganizing.windowMove(root); //added by Srinath to move around window

					MainLaunch.mainScene = new Scene(root, 652, 550);
					MainLaunch.getStage().setScene(MainLaunch.mainScene);

					//Changed by Srinath
					if (lOCK != null && response == 1) {
						
						String errorMsg =  Res.getMessage("message.login.invalid.username");								
						System.out.println(errorMsg);
						loginFrameController.errorMsg.setText(errorMsg);
						
					}else if(lOCK != null && response == 2){
						
						String errorMsg =  Res.getMessage("message.login.invalid.password");
						System.out.println(errorMsg);
						loginFrameController.errorMsg.setText(errorMsg);
						
						
					}else if(lOCK != null && response == 3){
						
						String errorMsg =  Res.getMessage("message.login.unrecoverable.error");
						System.out.println(errorMsg);
						loginFrameController.errorMsg.setText(errorMsg);
						
						
					}else {
						String errorMsg = Res.getMessage("message.login.relogin.error");
						loginFrameController.errorMsg.setText(MessageFormat.format(errorMsg, tel));
					}

					
				} catch (Exception e) {
					Log.error(e);
					e.printStackTrace();
				}
			}
		});
	}

	@FXML
	public void startLogin() {
			Log.info("start login");

			String countryCode = countryCodeText.getText().trim();
			String tel = mobile_number_loging.getValue().trim();
			String username = "00" + countryCode + tel;

			final FileOutputStream LOCK = SystemUtil.lockFile(TalkUtil.getUserHome(username).getAbsolutePath() + "/lock");

			TalkManager.LOCK = LOCK;
			System.out.println(LOCK);
			//Changed by Srinath
			if (TalkManager.LOCK == null) {

				int response=1;
				backToLoginFrame(response, LOCK, tel);

			}else {
				try {
					if (auto_login.isSelected()) {
						save_password.setSelected(true);

					} else if (!save_password.isSelected()) {
						auto_login.setSelected(false);
					}
					LoginDialog log = new LoginDialog();
					int response = log.loginFx(countryCodeText.getText(), mobile_number_loging.getValue(),
							password_loging.getText(), save_password.isSelected(), auto_login.isSelected(), LOCK);

					if (response == 0) {
						try {
							loadMainUi();

						} catch (Exception e) {
							e.printStackTrace();
							Log.error(e);
						}

						System.out.println("Loading MainUi");

					} else {
						try {
							backToLoginFrame(response, LOCK, tel);
						} catch (Exception e) {
							Log.error(e.getMessage());
						}
					}
				} catch (Exception e) {
					Log.error(e.getMessage());
				}
			}


	}

	public void comboBoxSuggestion(KeyEvent e) {
		ComboBoxAutoComplete cb = new ComboBoxAutoComplete(cmb);
		cb.handleOnKeyPressed(e);
		
	}

	public void countrySuggestion(KeyEvent e) {

		ComboBoxAutoCompleteCountry cb = new ComboBoxAutoCompleteCountry(country_cmb);
		
		cb.handleOnKeyPressed(e);
		

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		organizeUIComponents();// organize the sizes of components

		Map<String, VCard> vCardMap = TalkManager.getVCardMap();

		String lastUsername = TalkManager.getAppConfig().getLastUsername();

		VCard personalVCard = vCardMap.get(lastUsername);

		if (personalVCard == null && !vCardMap.isEmpty()) {
			personalVCard = new ArrayList<>(vCardMap.values()).get(0);
		}

		// added by Nisal to reset error message
		errorMsg.setText("");
		setVCard(personalVCard);
		//added by charith to modularize getting country list in a common place.
		comboBoxUtil.getCountryListComboBox(country_cmb);

		if (personalVCard != null) {
			setCountryFx(personalVCard.getCountryCode(), null);
		}

		if (auto_login.isSelected() && save_password.isSelected()) {

			try {

				startLoginProcess();

				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						getLoadingImage();
					}
				});

			} catch (Exception e1) {
				Log.error(e1);
				e1.printStackTrace();
			}
		}
		forgot_my_password.setOnMouseClicked(event -> {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("fxml/ResetPassword.fxml"));

			StackPane root = null;
			try {
				root = loader.load();

			} catch (IOException e) {
				e.printStackTrace();
			}
			ComponentsOrganizing.windowMove(root); //added by Srinath to move around window
			
			mainStage.initStyle(StageStyle.TRANSPARENT);
			Scene resetPwScene = new Scene(root);
			mainStage.setScene(resetPwScene);
			/*Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
			mainStage.setWidth(primScreenBounds.getWidth() * 0.475);
			mainStage.setHeight(primScreenBounds.getHeight() * 0.750);*/
			double width = ScreenSizes.INITIAL_WINDOWS_WIDTH.getValue();
			double height = ScreenSizes.INITIAL_WINDOWS_HEIGHT.getValue();
			mainStage.setWidth(width);
			mainStage.setHeight(height);
			mainStage.show();
		});
	}

	public void alignmentComponents() {
		signLabelPane.setAlignment(Pos.CENTER);
	}
	
	@FXML
	public void clearFields() {
		try {
			mobile_number_loging.getItems().clear();
			mobile_number_loging.setValue("");
			password_loging.setText(null);
			countryCodeText.setText(null);
			country_cmb.setValue("");
		}catch(Exception e) {
			Log.error(e);
		}
		
	}
	
	protected void setCountryFx(int i, String name) {
		CountryCode countryCode = new CountryCode();    // edited by Srinath 23/10/2018 to get last user's country from CountryCode map
		Map<String, String> map = countryCode.getCountryCode();
		String code = Integer.toString(i);
		if(map.containsKey(code)) {
			country_cmb.setValue(map.get(code));		
		}
		countryCodeText.setText(code);

	}

	public void selectCountry(ActionEvent e) {
		
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				
				CountryCode countryCode = new CountryCode();
				Map<String, String> map = countryCode.getCountryCode();
				for (Entry<String, String> entry : map.entrySet()) {
					if (Objects.equals(country_cmb.getValue(), entry.getValue())) {
						countryCodeText.setText(entry.getKey());
					}
				}
				
			}
		});
	
	}

	@FXML
	public void setCountry(ActionEvent e) {

		CountryCode countryCode = new CountryCode();
		Map<String, String> map = countryCode.getCountryCode();

		if (!countryCodeText.getText().equals(map.get(countryCodeText.getText()))) {
			country_cmb.setValue(map.get(countryCodeText.getText()));
			// Label country = new Label(map.get(countryCodeText.getText()));
			// country_cmb.setValue(country);
			// System.out.println(map.get(countryCodeText.getText()));
		}
	}

	@FXML
	public void autoSelectCountry() {
		
	}
	
	public void setVCard(VCard vcard) {

		if (vcard == null) {
			return;
		}

		int countryCode = vcard.getCountryCode();
		String code = String.valueOf(countryCode);
		String tel = vcard.getTel().substring(("00" + code).length());
		// cmb.setValue(code);
		countryCodeText.setText(code);
		mobile_number_loging.setValue(tel);

		if (vcard.isSavePassword()) {
			String password = vcard.getPassword();
			if (password != null) {
				password_loging.setText(password);
			}
			save_password.setSelected(true);
		}

		if (vcard.isAutoLogin()) {
			auto_login.setSelected(vcard.isAutoLogin());
		}
	}

	@FXML
	public void actionPerformed(ActionEvent e) {

		// SwingUtil.openBrowseURL(Res.getProperty("talk.register"));

	}

	@FXML
	protected void handleButtonAction(ActionEvent event) {

		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {

				Platform.exit();

			} else if (button.equals(minimize)) {

				Stage stage = (Stage) minimize.getScene().getWindow();
				stage.setIconified(true);

			}
		}
	}

	@FXML
	public boolean isSavePassword() {
		return savePasswordCheck.isSelected();
	}

	@FXML
	public boolean isAutoLogin() {
		return autoLoginCheck.isSelected();
	}

	@FXML
	public void getPassword() {
		new String(password_loging.getText());
	}

	public void loadMainUi() throws Exception {

		Platform.runLater(new Runnable() {

			@Override
			public void run() {

				try {
					MainLaunch.getStage().hide();

					Parent loader = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/MainWindow.fxml"));
					// mainStage.initStyle(StageStyle.TRANSPARENT);
					Screen screen = Screen.getPrimary();
					Rectangle2D bounds = screen.getVisualBounds();

					ComponentsOrganizing.windowMove(loader); //added by Srinath to move around window
					
					HashMap<String, Double> mainScreenHWMap = ScreenUtil.getMainScreenDimentions();
					mainStage.setWidth(mainScreenHWMap.get(ScreenUtil.MAIN_SCREEN_DIMENTIONS_WIDTH));
					mainStage.setHeight(mainScreenHWMap.get(ScreenUtil.MAIN_SCREEN_DIMENTIONS_HEIGHT));
					JFXDecorator decorator = new JFXDecorator(mainStage, loader);
					HBox hBox = new HBox();
					HBox inner = new HBox();
					Image image = new Image(getClass().getResourceAsStream("/images/logo-small1.png"));
					Image dots = new Image(getClass().getResourceAsStream("/images/loading1.png"));

					decorator.setGraphic(new ImageView(dots));
					hBox = (HBox) decorator.getChildren().get(0);
					hBox.getChildren().remove(1);
					inner = (HBox) hBox.getChildren().get(0);
					Region r1 = new Region();
					Region r2 = new Region();
					inner.getChildren().add(r1);

					hBox.setPrefHeight(27);// change decorator size
					hBox.setMinHeight(27);
					hBox.setMaxHeight(27);
					hBox.setPadding(new Insets(4, 5, 4, 3));// change decorator padding

					inner.getChildren().add(new ImageView(image));

					inner.getChildren().add(r2);
					inner.setHgrow(r1, Priority.ALWAYS);
					inner.setHgrow(r2, Priority.ALWAYS);

					addCustomImagesToDecorator(decorator);// add custom images to controller buttons in the decorator

					scene = new Scene(decorator, 1000, 600);
					// scene.getStylesheets().add("/styles/TitleBar.css");
					scene.getStylesheets().add(getClass().getResource("/styles/Theme.css").toExternalForm());
					mainStage.setScene(scene);

					mainStage.show();

					mainStage.setX((bounds.getWidth() - mainStage.getWidth()) / 2);
					mainStage.setY((bounds.getHeight() - mainStage.getHeight()) / 2);
					mainStage.getIcons().add(new Image("/images/logo3x.png"));

				} catch (Exception e) {
					Log.error(e);
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Enter press loging
	 * 
	 * Srinath
	 * 
	 **/
	@FXML
	public void keyReleased(KeyEvent e) {

		Node source = (Node) e.getSource();
		if (source == mobile_number_loging || source == password_loging) {
			if (e.getCode() == KeyCode.ENTER) {

				try {

					startLoginProcess();

					Platform.runLater(new Runnable() {

						@Override
						public void run() {

							getLoadingImage();
						}
					});

				} catch (Exception e1) {
					Log.error(e1);
					e1.printStackTrace();
				}
			}

		} else if (source == cmb) {

			String text = cmb.getPromptText().trim();
			cmb.setValue(text);
		}
	}

	// removed by nisal
	// @FXML
	// public void titleImage() {
	// ImageView img = new ImageView("/images/background.png");
	// ImageViewPane viewPane = new ImageViewPane(img);
	//
	// background.getChildren().add(viewPane);
	//
	// }

	public void getLoadingImage() {

		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/LoadingFrame.fxml"));
			Parent root = (Parent) loader.load();

			loadingFrameController = (LoadingFrameController) loader.getController();
			loadingFrameController.setBgImage();
			MainLaunch.mainScene = new Scene(root, 652, 550);
			MainLaunch.getStage().setScene(MainLaunch.mainScene);

			/*
			 * Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
			 * MainLaunch.getStage().setWidth(primScreenBounds.getWidth() * 0.650);
			 * MainLaunch.getStage().setHeight(primScreenBounds.getHeight() * 0.850);
			 */

			// added by nisal
			double width = ScreenSizes.INITIAL_WINDOWS_WIDTH.getValue();
			double height = ScreenSizes.INITIAL_WINDOWS_HEIGHT.getValue();
			MainLaunch.getStage().setWidth(width);
			MainLaunch.getStage().setHeight(height);

			ComponentsOrganizing.windowMove(root); //added by Srinath to move around window
			
		} catch (Exception e) {
			Log.error(e);
			e.printStackTrace();
		}
	}

	/**
	 * add and fix the dynamic sizes for all UI components
	 * 
	 * @author nisal
	 **/
	private void organizeUIComponents() {

		/*DoubleProperty font_size_2 = FontsUtil.getDynamicFontSize(signIn_loging.widthProperty(),
				signIn_loging.heightProperty(), 15, 15);
		DoubleProperty font_size_3 = FontsUtil.getDynamicFontSize(signLabel.widthProperty(), signLabel.heightProperty(),
				28, 10);

		double radius = (signIn_loging.getMaxHeight() / 2.0);
		signIn_loging.styleProperty()
				.bind(Bindings.concat(signIn_loging.styleProperty().get(), "-fx-font-size: ", font_size_2.asString(),
						";", "-fx-background-radius: ", String.valueOf((int) radius), ";", "-fx-font-weight: bold;"));
		signLabel.styleProperty().bind(Bindings.concat(signLabel.styleProperty().get(), "-fx-font-size: ",
				font_size_3.asString(), ";", "-fx-font-weight: bold;"));*/

		double width = ScreenSizes.INITIAL_WINDOWS_WIDTH.getValue();
		double height = ScreenSizes.INITIAL_WINDOWS_HEIGHT.getValue();
		double devide_factor_h = 2.0;
		double devide_factor_w = 2.9;

		header_img.setFitHeight(height / 3.8);
		header_img.setFitWidth(width);

		centerMainContainer.setPrefWidth((width / devide_factor_w));
		centerMainContainer.setMaxWidth((width / devide_factor_w));

		footer_container.setPrefWidth((width / devide_factor_w));
		footer_container.setMaxWidth((width / devide_factor_w));

		centerContainer.setPrefHeight((height / devide_factor_h));
		centerContainer.setMaxHeight((height / devide_factor_h));
		centerContainer.setPrefWidth((width / devide_factor_w));
		centerContainer.setMaxWidth((width / devide_factor_w));

	}

	/**
	 * add custom images to controll buttons in decorator
	 * 
	 * @author nisal
	 * @param decorator current window's custom decorator object
	 **/
	private void addCustomImagesToDecorator(JFXDecorator decorator) {

		ImageView max = new ImageView(new Image(getClass().getResourceAsStream("/images/Max_1.png")));
		max.setFitWidth(15);
		decorator.setDecoratorButtonImages(null, null, max, max, max);

	}
	
	/**
	 * multiple users login
	 * 
	 * Srinath
	 * 
	 **/

	@FXML
	public void getUserListFx() {
		mobile_number_loging.getItems().clear();
		ObservableList<Image> avatarsList = FXCollections.observableArrayList();
		ObservableList<String> nickNameList = FXCollections.observableArrayList();
		List<VCard> loginList = TalkManager.getAppDao().getLoginList();
		
		for (final VCard getLoginList : loginList) {
			
			String nickName = getLoginList.getNick();
			nickNameList.add(nickName); 	
			byte[] byteAvatar = getLoginList.getAvatar();
			Image avatar = new Image(new ByteArrayInputStream(byteAvatar));
			avatarsList.add(avatar);
		}
		mobile_number_loging.getItems().addAll(nickNameList);
		mobile_number_loging.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {

			@Override
			public ListCell<String> call(ListView<String> param) {

				return new JFXListCell<String>() {

					private ImageView imgView;
					{
						imgView = new ImageView();
						imgView.setFitWidth(30);
						imgView.setFitHeight(25);
					}

					@Override
					protected void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);

						if (empty) {
							setGraphic(null);
						} else {

								imgView.setImage(avatarsList.get(nickNameList.indexOf(item)));
								setGraphic(imgView); 
							
						}
						
					}

				};

			}
		});

	}
	
	/**
	 * multiple users login
	 * 
	 * Srinath
	 * 
	 **/
	public void selectUser(ActionEvent e) {
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				try {
					ObservableList<String> nickNameList = FXCollections.observableArrayList();
					ObservableList<String> telList = FXCollections.observableArrayList();
					ObservableList<String> passwordList = FXCollections.observableArrayList();
					ObservableList<String> countryCodeList = FXCollections.observableArrayList();
					List<VCard> loginList = TalkManager.getAppDao().getLoginList();
					String nickName = null;
					String tel = null;
					String password = null;
					int countryCode = 0;
					String countryCodes = null;
					String telNumber = null;
					
					for (final VCard getLoginList : loginList) {
						nickName = getLoginList.getNick();
						tel = getLoginList.getTel();
						password = getLoginList.getPassword();
						countryCode = getLoginList.getCountryCode();
						countryCodes = Integer.toString(countryCode);
						nickNameList.add(nickName);
						telList.add(tel);
						passwordList.add(password);
						countryCodeList.add(countryCodes);

					}	
				
					if (telList.get(mobile_number_loging.getSelectionModel().getSelectedIndex()) != null) {
						
						telNumber= telList.get(mobile_number_loging.getSelectionModel().getSelectedIndex()).replace("00"+countryCodeList.get(mobile_number_loging.getSelectionModel().getSelectedIndex()), "");
						countryCodeText.setText(countryCodeList.get(mobile_number_loging.getSelectionModel().getSelectedIndex()));
						password_loging.setText(passwordList.get(mobile_number_loging.getSelectionModel().getSelectedIndex()));
						mobile_number_loging.setValue(telNumber);


						CountryCode countryCodeNew = new CountryCode();
						Map<String, String> map = countryCodeNew.getCountryCode();

						if (!countryCodeText.getText().equals(map.get(countryCodeText.getText()))) {
							country_cmb.setValue(map.get(countryCodeText.getText()));
						}
						
					}
				}catch (Exception ex) {
					Log.warn("selectUser   "+ex.toString());
				}
				
			}
		});

  }
}
