package com.xinwei.loginwindow;

import java.util.stream.Stream;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Window;

public class ComboBoxAutoCompleteCountry {
	
	@FXML
	private ComboBox<String> country_cmb;
	@FXML
	java.lang.String filter = "";
	@FXML
	private ObservableList<String> originalItems;

	public ComboBoxAutoCompleteCountry(ComboBox<String> country_cmb) {
		this.country_cmb = country_cmb;
		originalItems = FXCollections.observableArrayList(country_cmb.getItems());
		country_cmb.setTooltip(new Tooltip());
		country_cmb.setOnKeyPressed(this::handleOnKeyPressed);
		country_cmb.setOnHidden(this::handleOnHiding);
	}
	
	@FXML
	public void handleOnKeyPressed(KeyEvent e) {
		ObservableList<String> filteredList = FXCollections.observableArrayList();
		KeyCode code = e.getCode();

		if (code.isLetterKey()) {
			filter += e.getText();
			
		}
		if (code == KeyCode.BACK_SPACE && filter.length() > 0) {
			filter = filter.substring(0, filter.length() - 1);
			country_cmb.getItems().setAll(originalItems);
		}
		if (code == KeyCode.ESCAPE) {
			filter = "";
		}
		if (filter.length() == 0) {
			filteredList = originalItems;
			country_cmb.getTooltip().hide();
		} else {
			Stream<String> itens = country_cmb.getItems().stream();
			java.lang.String txtUsr = filter.toString().toLowerCase();
			itens.filter(el -> el.toString().toLowerCase().contains(txtUsr)).forEach(filteredList::add);
			country_cmb.getTooltip().setText(txtUsr);
			Window stage = country_cmb.getScene().getWindow();
			double posX = stage.getX() + country_cmb.getBoundsInParent().getMinX();
			double posY = stage.getY() + country_cmb.getBoundsInParent().getMinY();
			country_cmb.getTooltip().show(stage, posX, posY);
			country_cmb.show();
		}
		//edited by Srinath 17/10/2018
		country_cmb.getItems().setAll(filteredList);
		country_cmb.setValue(country_cmb.getItems().iterator().next());
		country_cmb.getTooltip().hide();
	}

	@FXML
	public void handleOnHiding(Event e) {
		filter = "";
		country_cmb.getTooltip().hide();
		String s = country_cmb.getSelectionModel().getSelectedItem();
		country_cmb.getItems().setAll(originalItems);
		country_cmb.getSelectionModel().select(s);
	}

}
