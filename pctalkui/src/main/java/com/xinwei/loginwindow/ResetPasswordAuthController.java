package com.xinwei.loginwindow;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

import com.xinwei.common.lang.StringUtil;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.talk.http.service.TalkServiceHelper;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.uiutils.PopUpUtil;
import com.xinwei.util.ProgressUtil;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class ResetPasswordAuthController extends TalkServiceHelper implements Initializable{

	@FXML
	private BorderPane login_frame;

	@FXML
	private StackPane background;

	@FXML
	private Label signLabel;

	@FXML
	private Label lblAuthNumb;

	@FXML
	private TextField authNumber;

	@FXML
	private Button minimize, close,btnSubmit;

	// added by Nisal
	@FXML
	private StackPane base_container_view;
	
	// added by Srinath
	@FXML
	private VBox container;
	// added by Srinath
	@FXML
	private ImageView header_img;

	Map<String, String> data = null;
	private String authCode;
	public static String sessionId = null;
	HttpResponseInfo responseInfo;
	// result code is always 0 or 1, initialized as 2 to avoid nullpointerexception
	// in if(resultCode == 0)
	Integer resultCode = 2;

	@FXML
	void btnSubmit(ActionEvent event) {
		if (StringUtil.isEmpty(authNumber.getText()))
			PopUpUtil.showAlert("Authentication code field can not be empty !", AlertType.ERROR);
		else {
			Task<Void> serverRequest = new Task<Void>() {
				@Override
				protected Void call() throws Exception {
					processcheckAuth();
					return null;
				}
			};
			serverRequest.setOnRunning(e -> {
				showProgressView();// show progress view until current process finished
			});
			serverRequest.setOnSucceeded(e -> {
				drawModifyPw();
				hideProgressView();// hide progress view after process finished
			});
			serverRequest.setOnCancelled(e -> {
				hideProgressView();// hide progress view after process finished
			});
			serverRequest.setOnFailed(e -> {
				hideProgressView();// hide progress view after process finished
			});

			Thread serverReqThread = new Thread(serverRequest);
			serverReqThread.setDaemon(true);
			serverReqThread.start();
		}
	}

	public void processcheckAuth() {
		try {
			authCode = authNumber.getText();
			responseInfo = TalkManager.getHttpPasswordService().checkAuthCode(ResetPasswordController.mobileNumb,
					authCode, ResetPasswordController.seqCode);
			resultCode = getResult(responseInfo);
		} catch (Exception e) {
			System.out.println("-> Fx Engine : Invalid authentication code ");
		}
	}

	public void drawModifyPw() {
		if (resultCode == 0) {
			data = (Map<String, String>) responseInfo.get("data");
			sessionId = data.get("sessionId");
			data = null;
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/fxml/ResetPasswordModify.fxml"));
			Parent root;
			try {
				root = loader.load();
				LoginFrameController.resetPwScene = new Scene(root);
				LoginFrameController.getStage().setScene(LoginFrameController.resetPwScene);
				ComponentsOrganizing.windowMove(root); //added by Srinath to move around window
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			PopUpUtil.showAlert("Invalid authentication code !", AlertType.ERROR);
		}
	}

	/**
	 * create new view with progress view to show while server call in progress
	 * 
	 * @author nisal
	 **/
	private void showProgressView() {

		VBox pop_up = ProgressUtil.getProgressView();

		// show pop-up dialog in UI
		if (base_container_view != null && pop_up != null
				&& base_container_view.getChildren().get(base_container_view.getChildren().size() - 1) != pop_up) {
			base_container_view.setAlignment(Pos.CENTER);
			base_container_view.getChildren().add(pop_up);
		}
	}

	/**
	 * hide progress view from UI
	 * 
	 * @author nisal
	 **/
	private void hideProgressView() {
		ProgressUtil.hideProgressView(base_container_view);
	}
	
	/**
	 * window close or minimize actions 
	 * 
	 * @author Srinath
	 **/
	@FXML
	public void handleCloseMinimize(ActionEvent event) {
		ComponentsOrganizing.closeAndMinimize(event,close,minimize);
	}
	
	
	/**
	 * To call Components Organizing on UI
	 * 
	 * @author Srinath
	 **/
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ComponentsOrganizing.componentsOrganize(container,header_img); //To call Components Organizing on UI
		
	}
}
