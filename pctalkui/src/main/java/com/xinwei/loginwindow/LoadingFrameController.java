package com.xinwei.loginwindow;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class LoadingFrameController implements Initializable {
	/*@FXML
	private BorderPane loading;*/
	@FXML
	private ImageView loading_img;
	@FXML
	private StackPane loading,logo;

	@FXML
	public void setBgImage() {
		ImageView img = new ImageView("/images/loading-bg.png");
		ImageViewPane viewPane = new ImageViewPane(img);
		ImageView img1 = new ImageView("/images/logo-white.png");
		//ImageViewPane viewPane = new ImageViewPane(img);
		//img.fitWidthProperty().bind(MainLaunch.getStage().widthProperty());
		loading.getChildren().add(viewPane);
		logo.getChildren().add(img1);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		
	}
	
	
}
