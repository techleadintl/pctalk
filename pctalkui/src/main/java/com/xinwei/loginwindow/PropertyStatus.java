package com.xinwei.loginwindow;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class PropertyStatus {

	private SimpleBooleanProperty isAutoLogin;

	public final Boolean getIsSelected() {
		if (isAutoLogin!=null) {
			return isAutoLogin.getValue();
		}
		return null;
		
		
	}

	public final void setIsSelected(boolean isSelected) {
		this.selectStatus().set(isSelected); 
	}
	
	
	public final BooleanProperty selectStatus() {
		if(isAutoLogin==null) {
			isAutoLogin = new SimpleBooleanProperty(false);
		}
		return isAutoLogin;
		
	}
}
