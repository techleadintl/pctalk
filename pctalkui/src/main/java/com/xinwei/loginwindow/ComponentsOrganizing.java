package com.xinwei.loginwindow;

import com.xinwei.constants.ScreenSizes;

/**
 * Components Organizing on UI
 * 
 * @author Srinath
 **/

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ComponentsOrganizing {
	
	public static void componentsOrganize(VBox container, ImageView header_img) {
		double width = ScreenSizes.INITIAL_WINDOWS_WIDTH.getValue();
		double height = ScreenSizes.INITIAL_WINDOWS_HEIGHT.getValue();
		double devide_factor_h = 2.0;
		double devide_factor_w = 2.9;

		header_img.setFitHeight(height / 3.8);
		header_img.setFitWidth(width);
		
		container.setPrefHeight((height / devide_factor_h));
		container.setMaxHeight((height / devide_factor_h));
		container.setPrefWidth((width / devide_factor_w));
		container.setMaxWidth((width / devide_factor_w));

	}
	
	public static void closeAndMinimize(ActionEvent event, Button close, Button minimize) {
		Stage stage = (Stage) minimize.getScene().getWindow();
		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();
			if (button.equals(close)) {
				stage.close();

			} else if (button.equals(minimize)) {

				
				stage.setIconified(true);

			}
		}
	}
	
	private static double xOffset = 0;
	private static double yOffset = 0;
	public static void windowMove(Parent root) {
	
		root.setOnMousePressed(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				xOffset = event.getSceneX();
				yOffset = event.getSceneY();

			}
		});

		root.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				LoginFrameController.getStage().setX(event.getScreenX() - xOffset);
				LoginFrameController.getStage().setY(event.getScreenY() - yOffset);
			}
		});
	}
}
