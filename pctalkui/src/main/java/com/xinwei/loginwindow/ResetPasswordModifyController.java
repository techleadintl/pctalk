package com.xinwei.loginwindow;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXPasswordField;
import com.xinwei.common.lang.DESCoderUtil;
import com.xinwei.http.model.HttpResponseInfo;
import com.xinwei.talk.http.service.TalkServiceHelper;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.uiutils.PopUpUtil;
import com.xinwei.util.ProgressUtil;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ResetPasswordModifyController extends TalkServiceHelper implements Initializable{
	@FXML
	private BorderPane login_frame;

	@FXML
	private StackPane background;

	@FXML
	private Label signLabel;

	@FXML
	private Label lblNewPassword;

	@FXML
	private JFXPasswordField txtNewPassword;

	@FXML
	private Label lblRetypePassword;

	@FXML
	private JFXPasswordField txtRetypePassword;

	@FXML
	private Button minimize, close,btnReset;

	// added by Nisal
	@FXML
	private StackPane base_container_view;
	
	// added by Srinath
	@FXML
	private VBox container;
	// added by Srinath
	@FXML
	private ImageView header_img;

	HttpResponseInfo responseInfo;

	@FXML
	void keyReleased(KeyEvent event) {

	}

	// result code is always 0 or 1, initialized as 2 to avoid nullpointerexception
	// in if(resultCode == 0)
	Integer resultCode = 2;

	@FXML
	void handleBtnReset(ActionEvent event) {
		if (!txtNewPassword.getText().trim().equals(txtRetypePassword.getText().trim())) {
			PopUpUtil.showAlert("Passwords does not match", AlertType.ERROR);
		} else {
			processModifyPassword();
		}
	}

	public void processModifyPassword() {
		String password = txtNewPassword.getText();
		try {
			Task<Void> serverRequest = new Task<Void>() {
				@Override
				protected Void call() throws Exception {
					responseInfo = TalkManager.getHttpPasswordService().modifyPassword(
							ResetPasswordController.mobileNumb, DESCoderUtil.encryptToStr(password),
							ResetPasswordAuthController.sessionId);
					resultCode = getResult(responseInfo);
					System.out.println(responseInfo);
					return null;
				}
			};
			serverRequest.setOnRunning(e -> {
//    			fixme: add processing notification UI | impl in PopUtils class
				showProgressView();// show progress view until current process finished
			});
			serverRequest.setOnSucceeded(e -> {
				hideProgressView();// hide progress view after process finished
				if (resultCode == 0) {
					PopUpUtil.showAlert("Password change successfull !", AlertType.INFORMATION);
					Stage stage = (Stage) login_frame.getScene().getWindow();
					stage.close();
				}
			});
			serverRequest.setOnCancelled(e -> {
				hideProgressView();// hide progress view after process finished
			});
			serverRequest.setOnFailed(e -> {
				hideProgressView();// hide progress view after process finished
			});
			Thread serverReqThread = new Thread(serverRequest);
			serverReqThread.setDaemon(true);
			serverReqThread.start();
		} catch (Exception e) {
			PopUpUtil.showAlert("Process did not complete", AlertType.WARNING);
		}
	}

	/**
	 * create new view with progress view to show while server call in progress
	 * 
	 * @author nisal
	 **/
	private void showProgressView() {

		VBox pop_up = ProgressUtil.getProgressView();

		// show pop-up dialog in UI
		if (base_container_view != null && pop_up != null
				&& base_container_view.getChildren().get(base_container_view.getChildren().size() - 1) != pop_up) {
			base_container_view.setAlignment(Pos.CENTER);
			base_container_view.getChildren().add(pop_up);
		}
	}

	/**
	 * hide progress view from UI
	 * 
	 * @author nisal
	 **/
	private void hideProgressView() {
		ProgressUtil.hideProgressView(base_container_view);
	}
	
	/**
	 * window close or minimize actions 
	 * 
	 * @author Srinath
	 **/
	@FXML
	public void handleCloseMinimize(ActionEvent event) {
		ComponentsOrganizing.closeAndMinimize(event,close,minimize);
	}
	
	/**
	 * To call Components Organizing on UI
	 * 
	 * @author Srinath
	 **/
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		ComponentsOrganizing.componentsOrganize(container,header_img); //To call Components Organizing on UI
		
	}
}
