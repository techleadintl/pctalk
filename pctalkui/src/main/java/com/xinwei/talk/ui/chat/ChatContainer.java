/*************************************************************
 * * Copyright (c) 2016-2050  信威通信
 *
 * All Rights Reserved
 * 
 * This is the proprietary source code of XinWei company product
 *
 * @author liyong
 *
 * 2016年9月20日 上午10:54:54
 * 
 ***************************************************************/
package com.xinwei.talk.ui.chat;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.reflect.Method;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JSplitPane;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;

import com.jtattoo.plaf.BaseRootPaneUI;
import com.xinwei.common.lang.StringUtil;
import com.xinwei.common.lang.SwingUtil;
import com.xinwei.common.lookandfeel.McWillTheme;
import com.xinwei.common.lookandfeel.ThemeColor;
import com.xinwei.common.lookandfeel.component.McWillList;
import com.xinwei.common.lookandfeel.component.McWillSplitPane;
import com.xinwei.common.lookandfeel.component.McWillTitlePane;
import com.xinwei.talk.common.LAF;
import com.xinwei.talk.common.Log;
import com.xinwei.talk.common.TalkUtil;
import com.xinwei.talk.manager.ImageManager;
import com.xinwei.talk.manager.ListenerManager;
import com.xinwei.talk.manager.TalkManager;
import com.xinwei.talk.ui.chat.room.ChatRoom;
import com.xinwei.talk.ui.util.WindowDragListener1;
import com.xinwei.talk.ui.util.component.XWScrollPane;

public class ChatContainer extends JPanel {
	private ChatFrame chatFrame;

	private McWillList<ChatRoom> list;

	private DefaultListModel<ChatRoom> listModel;

	private JPanel roomPanel;

	private JPanel rightPanel;

	private WindowDragListener1 dragListener;

	public ChatContainer() {
		setLayout(new BorderLayout());
		setOpaque(false);

		dragListener = new WindowDragListener1() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() != 2)
					return;
				JRootPane root = getRootPane();
				McWillTitlePane titlePane = (McWillTitlePane) ((BaseRootPaneUI) root.getUI()).getTitlePane();
				if (chatFrame.getState() == Frame.MAXIMIZED_BOTH) {
					chatFrame.setState(~Frame.MAXIMIZED_BOTH);
					titlePane.doIconify();
				} else {
					titlePane.doMax();
				}
			}
		};

		McWillSplitPane horizontalSplit = new McWillSplitPane(JSplitPane.HORIZONTAL_SPLIT) {
			@Override
			public Color getSplitLineColor() {
				return LAF.getChatSplitPaneColor();
			}
		};
		horizontalSplit.setBorder(BorderFactory.createEmptyBorder());
		horizontalSplit.setDividerLocation(145);
		horizontalSplit.setContinuousLayout(false);

		listModel = new DefaultListModel<ChatRoom>();
		list = new McWillList<ChatRoom>(listModel);
		list.setCellRenderer(new ChatRoomListCellRenderer());

		XWScrollPane scrollPane = new XWScrollPane(list);
		scrollPane.setPreferredSize(new Dimension(140, Short.MAX_VALUE));

		scrollPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		rightPanel = new JPanel(new BorderLayout());

		roomPanel = new JPanel();

		roomPanel.setLayout(new CardLayout());

		rightPanel.add(roomPanel, BorderLayout.CENTER);

		//Add Chat Panel to Split Pane
		horizontalSplit.setLeftComponent(scrollPane);
		horizontalSplit.setRightComponent(rightPanel);

		add(horizontalSplit, BorderLayout.CENTER);

		//		add(scrollPane, BorderLayout.WEST);
		//		add(rightPanel, BorderLayout.CENTER);
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Point mousePoint = e.getPoint();
				int locationToIndex = list.getValidIndex(mousePoint);
				if (locationToIndex == -1)
					return;
				ChatRoom chatRoom = list.getModel().getElementAt(locationToIndex);
				if (chatRoom == null) {
					return;
				}
				ChatContactPanel chatContactPanel = (ChatContactPanel) chatRoom.getClientProperty(chatRoom);
				if (!SwingUtil.contains(mousePoint, chatContactPanel.getDeleteRec())) {//不是点击关闭按钮
					activateChatRoom(chatRoom);
				}
			}
		});

		addMouseListener(dragListener);
		addMouseMotionListener(dragListener);

		list.addMouseListener(dragListener);
		list.addMouseMotionListener(dragListener);
		rightPanel.addMouseListener(dragListener);
		rightPanel.addMouseMotionListener(dragListener);
	}

	public ChatFrame getChatFrame() {
		return chatFrame;
	}

	public void addChatRoom(final ChatRoom chatRoom) {
		createFrameIfNeeded();

		if (containChatRoom(chatRoom)) {
			return;
		}

		ChatContactPanel chatContact = new ChatContactPanel(this, chatRoom);

		//		chatRoom.addClosingListener(new MyChatRoomClosingListener(chatContact, chatRoom));

		//		chatContact.addChatContactListener(new MyChatContactListener(chatContact, chatRoom));

		chatRoom.putClientProperty(chatRoom, chatContact);

		listModel.insertElementAt(chatRoom, 0);

		// Notify users that the chat room has been opened.
		ListenerManager.fireChatRoomOpened(chatRoom);

		chatRoom.registeredToFrame(chatFrame);

	}

	public void updateList() {
		list.updateUI();
	}

	public void activateChatRoom(final ChatRoom room) {
		if (room == null) {
			return;
		}
		if (list.getSelectedValue() != room) {
			list.setSelectedValue(room, true);
		}

		System.out.println("activateChatRoom..............................." + room.getRoomTitle());
		final String roomId = room.getRoomId();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				TalkManager.getMessageManager().clearUnreadMessageCount(roomId);
			}
		});
		
		
		roomPanel.removeAll();

		roomPanel.add(room);
		

		String displayName = TalkUtil.getDisplayName(roomId);
		chatFrame.setTitle(displayName);

		chatFrame.setIconImage(ImageManager.getAvatarImage(roomId, 25, 25, true));

//		chatFrame.bringFrameIntoFocus();
	}

	private void createFrameIfNeeded() {
		if (chatFrame != null) {
			return;
		}

		chatFrame = new ChatFrame(this, rightPanel);

		dragListener.setWindow(chatFrame);
		
	}

	public boolean closeAllChatRoom(boolean force) {
		if (force) {//强制关闭
			clearChatRoomList();
			return true;
		} else {
			ChatRoom[] rooms = new ChatRoom[listModel.size()];
			for (int i = 0; i < listModel.size(); i++) {
				rooms[i] = listModel.getElementAt(i);
			}
			return TalkManager.getChatManager().closeAllChatRoom(rooms);
		}
	}

	public void clearChatRoomList() {
		ChatRoom[] rooms = new ChatRoom[listModel.size()];
		for (int i = 0; i < listModel.size(); i++) {
			rooms[i] = listModel.getElementAt(i);
		}
		for (ChatRoom chatRoom : rooms) {
			chatRoom.close(true);
		}
		listModel.clear();
		list.updateUI();
	}

	public boolean isActiveChatRoom(ChatRoom chatRoom) {
		return chatRoom.equals(list.getSelectedValue());
	}

	/**
	 * Returns the Active Component.
	 */
	public ChatRoom getActiveChatRoom() {
		if (list.getModel().getSize() == 0) {
			return null;
		} else {
			return list.getSelectedValue();
		}
	}

	public ChatRoom getChatRoom(String roomId) {
		for (int i = 0; i < listModel.size(); i++) {
			ChatRoom talkRoom = listModel.getElementAt(i);
			if (talkRoom != null && StringUtil.equals(talkRoom.getRoomId(), roomId)) {
				return talkRoom;
			}
		}
		return null;
	}

	public ChatRoom[] getChatRooms() {
		ChatRoom[] rooms = new ChatRoom[listModel.size()];
		listModel.copyInto(rooms);
		return rooms;
	}

	public boolean containChatRoom(final ChatRoom room) {
		return listModel.contains(room);
	}

	public void chatRoomClosed(ChatRoom room) {
		try {
			ChatRoom selected = getActiveChatRoom();
			if (selected == room) {
				int index = list.getSelectedIndex();

				listModel.removeElement(room);

				if (listModel.isEmpty()) {
					chatFrame.setVisible(false);
				} else if (index == listModel.getSize()) {
					ChatRoom seletectRoom = listModel.getElementAt(listModel.getSize() - 1);
					activateChatRoom(seletectRoom);
				} else {
					ChatRoom seletectRoom = listModel.getElementAt(index);
					activateChatRoom(seletectRoom);
				}
			} else {
				listModel.removeElement(room);
			}
		} catch (Exception ex) {
			Log.error(ex);
		}
	}

	@SuppressWarnings("serial")
	public class ChatRoomListCellRenderer extends JPanel implements ListCellRenderer<ChatRoom> {
		@Override
		public Component getListCellRendererComponent(JList<? extends ChatRoom> list, ChatRoom chatRoom, int index, boolean isSelected, boolean cellHasFocus) {
			ChatContactPanel panel = (ChatContactPanel) chatRoom.getClientProperty(chatRoom);
			panel.setOpaque(true);
			ThemeColor themeColor = McWillTheme.getThemeColor();
			panel.setSelected(isSelected || cellHasFocus);
			panel.setRollovered(((McWillList) list).getRolloverIndex() == index);
			//			if (isSelected || cellHasFocus) {
			//				panel.setBackground(themeColor.getMiddleColor1());
			////				panel.setBorder(McWillBorders.getLabelBorder(themeColor.getMiddleColor1(), 20));
			//			} else {
			//				Color backgroundColor = ((McWillList) list).getRolloverIndex() == index ? ColorUtil.getAlphaColor(themeColor.getMiddleColor1(), 80) : themeColor.getSrcColor();
			//				panel.setBackground(backgroundColor);
			////				panel.setBorder(McWillBorders.getLabelBorder(backgroundColor, 20));
			//			}
			return panel;
		}
	}

}
